<?php

class Fire_model extends CI_Model {

 function preparePayment()
    {

        $token = $this->session->userdata('token');

        $BusinessID = $this->input->post('biz_id');
        $Year = $this->input->post('year');
        $PhoneNumber = $this->session->userdata('jpwnumber');

        $url = "http://192.168.6.10/JamboPayServices/api/payments/GetBusinessDetails?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);

        #var_dump($res); die();

        if (isset($res->ErrorCode)) {
            $sbpdata = array(
                'restext' => $res->Message,
                'rescode' => $res->ErrorCode
            );
        } else {
            $sbpdata = array(
                'approvalstatus' => $res->Status,
                'sbpnumber' => $res->BusinessID,
                'year' => $res->Year,
                'bizname' => $res->BusinessName,
                'pinno' => $res->Pin,
                'biztype' => "",
                'activitycode' => $res->ActivityCode,
                'paidfee' => $res->Amount,
                'inwords' => $res->AmountInWords,
                'pobox' => $res->POBox,
                'plotnumber' => $res->PlotNumber,
                'street' => $res->PhysicalAddress,
                'issuedate' => $res->DateIssued,
                'bizactivityname' => $res->ActivityDescription,
                'bizzid' => $res->BusinessID,
                'receiptno' => $res->ReceiptNumber,
                'phoneno' => $res->PhoneNumber,
                'restext' => "OK",
                'rescode' => 0
            );
        }
        // var_dump($sbpdata); die();
        $sbp_json = json_encode($sbpdata);
        return $sbp_json;
    }
function getFireBusinessDetailsBuilding(){
		$building = $this->input->post('building');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $PlotNo = $this->input->post('plotno');
        $LRNO = $this->input->post('lrno');
        $physicaladdress = $this->input->post('location');


        $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $serviceArgs = array(
        	"API_Id" =>$apiID,
			"Fullname"=>$fullnames,
			"Email"=>$email,
			"Tel_no"=>$Tel_no,
			"BusinessName"=>$building,//$Firm,
			"Owner"=>$fullnames,//$Occupation,
			"PlotNo"=> $PlotNo,
			"LRNO"=> $LRNO,
			"Form_Id"=> "FR",
			"Building"=>$physicaladdress,
			"category_id"=>$category,
			"subcounty"=>"dagoretti south",
			"zone"=>"ngando"
			);

        try {
        	$client = new SoapClient($url);

        	$result = $client->PostfireApplication($serviceArgs);

        	#var_dump($result); die();
        	$RefID = $result->PostfireApplicationResult->string["0"];
        	$InvoiceNo = $result->PostfireApplicationResult->string["3"];
	        } catch (Exception $e) {
	        	redirect('health/APIerror');
	        }

        if($RefID!="400"){

        	$serviceArguments2 = array(
        		"API_Id" =>$apiID,
        		"RefId"=>$RefID
        		#"category"=>$category
        		);
        	$result2 = $client->GetfireinvoiceDetails($serviceArguments2);

		    $Fullname = $result2->GetfireinvoiceDetailsResult->Name;
        	$Email = $result2->GetfireinvoiceDetailsResult->Email;
        	$Firm = $result2->GetfireinvoiceDetailsResult->BusinessName;
        	$InvoiceNo = $result2->GetfireinvoiceDetailsResult->InvoiceNum;
        	$Tel_no = $result2->GetfireinvoiceDetailsResult->Tel_no;
        	$Owner = $result2->GetfireinvoiceDetailsResult->Owner;
        	$Building = $result2->GetfireinvoiceDetailsResult->Building;
        	$Category = $result2->GetfireinvoiceDetailsResult->category;
        	$Status = $result2->GetfireinvoiceDetailsResult->status;
        	$RefID = $result2->GetfireinvoiceDetailsResult->RefId;
        	$Amount = $result2->GetfireinvoiceDetailsResult->amount;

		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$Tel_no,
			"Firm"=>$Firm,
			"InvoiceNo"=>$InvoiceNo,
			"Status"=>$Status,
			"Owner"=> $Owner,
			"Amount"=>$Amount,
			"Building"=>$Building,
			"Category"=>$category
			);

			try {
				$this->db->insert('fireinspection',$data);
			} catch (Exception $e) {

			}
	        #var_dump($data); die();

			return $data;
		}elseif($RefID=="400"){
			redirect('fire/error');
		}
}

function getFireBusinessDetails(){

		    $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));

        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=>$agentRef,
        	"businessId"=>$businessId,
        	"calendarYear"=>$year,
        	"custMobileNo"=>$Tel_no,
        	"pass"=> $pass

        	);

        try {
        	$client = new SoapClient($mainurl);
        } catch (Exception $e) {
        	redirect('/sbp/not_found');
        }

        $result = $client->GetIssuedSBPNCC($serviceArguments); #echo "<pre>"; var_dump($result); die();

        $sbpnumber = $result->GetIssuedSBPNCCResult->SBPNumber;
        $year = $result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname = $result->GetIssuedSBPNCCResult->BusinessName;
        $pinno = $result->GetIssuedSBPNCCResult->PINNumber;
        $biztype = $result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode = $result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee = $result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords = $result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox = $result->GetIssuedSBPNCCResult->POBox;
        $plotnumber = $result->GetIssuedSBPNCCResult->PlotNumber;
        $street = $result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate = $result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname = $result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid = $result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno = $result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress = $result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode = $result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building = $result->GetIssuedSBPNCCResult->Building;
        $approvalstatus = $result->GetIssuedSBPNCCResult->ApprovalStatus;
        $zone_name = $result->GetIssuedSBPNCCResult->ZoneName;

        if($resultcode == 0){

          $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
          $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

          $serviceArgs = array(
          	"API_Id" => $apiID,
      			"Fullname" => $fullnames,
      			"Email" => $email,
      			"Tel_no" => $Tel_no,
      			"BusinessName" => $bizname,//$Firm,
      			"Owner" => $fullnames,//$Occupation,
      			"PlotNo" => $plotnumber,
      			"LRNO" => $plotnumber,
            "Building" => $physicaladdress,
            "subcounty" => $this->input->post('subcounty'),
      			"Form_Id" => "FR",
            "activity_code" => $activitycode,
            "B_id" => $bizzid,
      			"zone" => $this->input->post('zone')
      		);

          #echo "<pre>";var_dump($serviceArgs);die();

          try {
          	$client = new SoapClient($url);

          	$result = $client->PostfireApplication($serviceArgs);

            #echo "<pre>";var_dump($result);die();
          	$RefID = $result->PostfireApplicationResult->string["0"];
          	$InvoiceNo = $result->PostfireApplicationResult->string["3"]; #echo "<pre>";var_dump($InvoiceNo);die();

  	        } catch (Exception $e) {
  	        	redirect('health/APIerror');
  	        }

            if($RefID != "400"){
            	$serviceArguments2 = array(
            		"API_Id" =>$apiID,
            		"RefId"=>$RefID
            		#"category"=>$category
            		);
            	$result2 = $client->GetfireinvoiceDetails($serviceArguments2);

              #echo "<pre>"; var_dump($result2); die();

    		      $Fullname = $result2->GetfireinvoiceDetailsResult->Name;
            	$Email = $result2->GetfireinvoiceDetailsResult->Email;
            	$Firm = $result2->GetfireinvoiceDetailsResult->BusinessName;
            	$InvoiceNo = $result2->GetfireinvoiceDetailsResult->InvoiceNum;
            	$Tel_no = $result2->GetfireinvoiceDetailsResult->Tel_no;
            	$Owner = $result2->GetfireinvoiceDetailsResult->Owner;
            	$Building = $result2->GetfireinvoiceDetailsResult->Building;
            	$Category = $result2->GetfireinvoiceDetailsResult->category;
            	$Status = $result2->GetfireinvoiceDetailsResult->status;
            	$RefID = $result2->GetfireinvoiceDetailsResult->RefId;
            	$Amount = $result2->GetfireinvoiceDetailsResult->amount;

          		$data = array(
          			"RefID"=>$RefID,
          			"issueDate"=>date("Y/m/d"),
          			"Fullname"=>$Fullname,
          			"Email"=>$Email,
          			"TelNumber"=>$Tel_no,
          			"Firm"=>$Firm,
          			"InvoiceNo"=>$InvoiceNo,
          			"Status"=>$Status,
          			"Owner"=> $Owner,
          			"Amount"=>$Amount,
          			"Building"=>$Building,
          			"Category"=>$category
          			);

          			try {
          				$this->db->insert('fireinspection',$data);
          			} catch (Exception $e) {

          			}
	              #var_dump($data); die();

            			return $data;
            		} elseif($RefID == "400"){
            			redirect('fire/error');
            		}
            	}else{
            		redirect('fire/InvalidBid');
            	}
}


	function printFirePermitreceipt($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/fireinvoice.pdf';
        $refid = $this->uri->segment(3);
        $query="SELECT * FROM `fireinspection` WHERE `RefID`='$refid' ORDER BY `id` DESC LIMIT 1";
        $result=$this->db->query($query);
        $result=$result->row();//var_dump($result); die();

        $Businessname = $result->Firm;
		$Owner = $result->Fullname;
		$Refid = $result->RefID;
		$InvoiceNo = $result->InvoiceNo;
		$FHAmount =$result->Amount;
		$Status = $result->status;
		if($Status=="0"){
			$Status = "WAITING FOR APPROVAL";
			}else{
			$Status = "APPROVED";
			}

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
        $page->drawText($Businessname, 250, 528);
        $page->drawText($Owner, 250, 480);
        $page->drawText($Refid, 250, 440);
        $page->drawText($InvoiceNo, 250, 400);
        $page->drawText(number_format($FHAmount,2), 320, 357);


        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=FireInspectionPermitInvoice.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
	}

	function payFireDetails(){

		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

		$RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
		$serviceArguments2 = array(
        		"API_Id" =>$apiID,
        		"RefId"=>$RefID
        		#"category"=>$category
        		);
			$client2 = new SoapClient($url);
        	$result2 = $client2->GetfireinvoiceDetails($serviceArguments2);

		    @$rescode = $result2->GetfireinvoiceDetailsResult->Response_Code;
		    if (!isset($rescode) ){
		    $Fullname = $result2->GetfireinvoiceDetailsResult->Name;
        	$Email = $result2->GetfireinvoiceDetailsResult->Email;
        	$Firm = $result2->GetfireinvoiceDetailsResult->BusinessName;
        	$InvoiceNo = $result2->GetfireinvoiceDetailsResult->InvoiceNum;
        	$Tel_no = $result2->GetfireinvoiceDetailsResult->Tel_no;
        	$Owner = $result2->GetfireinvoiceDetailsResult->Owner;
        	$Building = $result2->GetfireinvoiceDetailsResult->Building;
        	$Category = $result2->GetfireinvoiceDetailsResult->category;
        	$Status = $result2->GetfireinvoiceDetailsResult->status;
        	$RefID = $result2->GetfireinvoiceDetailsResult->RefId;
        	$Amount = $result2->GetfireinvoiceDetailsResult->amount;

        	$data = array(
				"RefID"=>$RefID,
				"issueDate"=>date("Y/m/d"),
				"Fullname"=>$Fullname,
				"Email"=>$Email,
				"TelNumber"=>$Tel_no,
				"Firm"=>$Firm,
				"InvoiceNo"=>$InvoiceNo,
				"Status"=>$Status,
				"Owner"=> $Owner,
				"Amount"=>$Amount,
				"Building"=>$Building,
				"Category"=>$Category
				);
        	try {
        	} catch (Exception $e) {

        	}
        	#var_dump($data); die();
        	return $data;
        }
        else{
        redirect('fire/invalidRefid');
        }
    }

    function prepareFirePayment(){

    	$token = $this->session->userdata('token');
    	$refid = $this->input->post('refid');
    	//$refid = str_replace(" ", "", $refid);

        $tosend = array(
            'Stream'=>"fireinvoice",
            'InvoiceReferenceNumber'=>$refid,
            'Year'=>date('Y'),//"2014",
            'PhoneNumber' =>$this->session->userdata('jpwnumber'),
            'PaidBy'=>$this->session->userdata('name'),
            'PaymentTypeID' => "1"
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        //echo "<pre>";var_dump($result);exit;
        $res = json_decode($result);
        @$rescode = $res->ResponseCode;
        //echo "<pre>";var_dump($res); die();
        if(isset($rescode)){
        	return $data = array(
        	"BusinessName"=>$res->BusinessName1,
        	"ContactPerson"=>$res->Names,
        	"Email"=>$res->Email,
        	"Telephone"=>$res->TelephoneNumber,
        	"Refid"=>$res->InvoiceReferenceNumber,
        	"TransactionID"=>$res->TransactionID,
        	"InvoiceNo"=>$res->InvoiceNumber,
        	"Amount"=>$res->Amount,
        	"rescode"=>$res->ResponseCode,
        	"message"=>"ok"
        	);
        }elseif(!isset($rescode)){
        	return $data=array(
        		"rescode"=>$res->ErrorCode,
        		"message"=>$res->Message,
        		);
        }

	}

	function completeFirePayment(){
	  	$token =$this->session->userdata('token');
	  	$LicenseID = $this->input->post('refid');
	      $tosend = array(
	            'Stream'=>"fireinvoice",
	            'TransactionID'=>$this->input->post('transid'),
	            'Pin'=>$this->input->post('jp_pin'),
	            'PhoneNumber' =>$this->session->userdata('jpwnumber'),
	            'PaymentTypeID' => '1',
	            'PaidBy'=>$this->session->userdata('name')
	          );

	      foreach ( $tosend as $key => $value) {
	      $post_items[] = $key . '=' . $value;
	      }
	      $post_string = implode ('&', $post_items);

	      $url = "http://192.168.6.10/JamboPayServices/api/payments/put";
	      $ch = curl_init($url);
	      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	      curl_setopt($ch, CURLOPT_POST, true);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	          'Authorization: bearer '.$token,
	          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
	      ));

	      $result = curl_exec($ch);
	      $res = json_decode($result);
	      // echo "<pre>";var_dump($res); die();
	      @$rescode = $res->ResponseCode;
        // echo "<pre>";var_dump($res); die();
        if(isset($rescode)){
        	return $data = array(
        	"BusinessName"=>$res->BusinessName1,
        	"ContactPerson"=>$res->Names,
        	"Email"=>$res->Email,
        	"Telephone"=>$res->TelephoneNumber,
        	"Refid"=>$LicenseID,
        	"ReceiptNo"=>$res->ReceiptNumber,
        	"InvoiceNo"=>$res->InvoiceNumber,
        	"Amount"=>$res->Amount,
        	"rescode"=>$res->ResponseCode,
        	"message"=>"ok"
        	);
        }elseif(!isset($rescode)){
        	return $data=array(
        		"rescode"=>@$res->ErrorCode,
        		"message"=>$res->Message,
        		);
        }
	}

    function completeFPPayment(){

		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

		$Refid=str_replace(" ","",$this->input->post('refid'));
		$InvoiceNum= $this->input->post('InvoiceNo');
		$Amount= $this->input->post('amount');

		$paymentinfo = array(
			"API_Id"=>$apiID,
			"Refid" => $Refid,
			"InvoiceNum"=>$InvoiceNum,
			"amount" => $Amount,
			"year"=> date("Y"),
			"form_id"=>"FR"
			);

		$client = new SoapClient($url);
		$result = $client->PostfirePayment($paymentinfo);
		$rescode = $result->PostfirePaymentResult; #var_dump($result); die();

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"InvoiceNo" => $this->input->post('InvoiceNo'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"rescode"=>$rescode
		);
		#var_dump($rescode); die();
		return $data;
	}

	function printFire(){
		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

		$Refid=strtoupper(str_replace(" ","",$this->input->post('refid'))) ;
		$data = array("RefId"=>$Refid,"API_Id"=>$apiID);

		try {
		$client = new SoapClient($url);
		$result = $client->printfirepermit($data);	#var_dump($result); die();
		} catch (Exception $e) {
			redirect('health/APIerror');
		}

		@$rescode = $result->printfirepermitResult->Response_Code;

		if (!isset($rescode) ){
		$result = $client->printfirepermit($data);
		$Fullname=$result->printfirepermitResult->Name;
		$Firm=$result->printfirepermitResult->BusinessName;
		$InvoiceNum=$result->printfirepermitResult->InvoiceNum;
		$Email=$result->printfirepermitResult->Email;
		$TelNumber=$result->printfirepermitResult->Tel_no;
		$Owner=$result->printfirepermitResult->Owner;
		$amount=$result->printfirepermitResult->amount;
		$Refid=$result->printfirepermitResult->RefId;
		$inspection_status=$result->printfirepermitResult->inspection_status;
		$Building=$result->printfirepermitResult->Building;
		$Category=$result->printfirepermitResult->category;

		$data = array(
			"RefID"=>$Refid,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$TelNumber,
			"Firm"=>$Firm,
			"Status"=>$inspection_status,
			"Amount"=>$amount,
			"Category"=>$Category,
			"Building"=>$Building
			);
		return $data;

		}
		else{
		redirect('fire/invalidRefid');
		}
	}

	function printFirePermit($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Firepermit.pdf';




        $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=$refid;
        $data = array("RefId"=>$Refid,"API_Id"=>$apiID);

        try {
        $client = new SoapClient($url);
        $result = $client->printfirepermit($data);  #var_dump($result); die();
        } catch (Exception $e) {
            redirect('health/APIerror');
        }

        @$rescode = $result->printfirepermitResult->Response_Code;

        if (!isset($rescode) ){
        $result = $client->printfirepermit($data);
        $Fullname=$result->printfirepermitResult->Name;
        $Firm=$result->printfirepermitResult->BusinessName;
        $InvoiceNum=$result->printfirepermitResult->InvoiceNum;
        $Email=$result->printfirepermitResult->Email;
        $TelNumber=$result->printfirepermitResult->Tel_no;
        $Owner=$result->printfirepermitResult->Owner;
        $amount=$result->printfirepermitResult->amount;
        $Refid=$result->printfirepermitResult->RefId;
        $inspection_status=$result->printfirepermitResult->inspection_status;
        $Building=$result->printfirepermitResult->Building;
        $Category=$result->printfirepermitResult->category;

        $data = array(
            "RefID"=>$Refid,
            "issueDate"=>date("Y/m/d"),
            "Fullname"=>$Fullname,
            "Email"=>$Email,
            "TelNumber"=>$TelNumber,
            "Firm"=>$Firm,
            "Status"=>$inspection_status,
            "Amount"=>$amount,
            "Category"=>$Category,
            "Building"=>$Building
            );
        }




       // $query="select * from fireinspection where RefID='$refid' order by RefID desc";

        //$result=$this->db->query($query);
       // $result=$result->row();#var_dump($result); die();

        $Fullname = $data["Fullname"];
		$Email = $data["Email"];
		$Tel_no = $data["TelNumber"];
		$Firm = $data["Firm"];
		$Category =$data["Category"];
		/*if($Category==14){
			$Category = "Medium Trader Shop/Retail Service";
		}elseif($Category==15){
			$Category = "Small Trader Shop/Retail Service";
		}elseif($Category==16){
			$Category = "Hyper-supermarket";
		}elseif($Category==17){
			$Category = "Other General Merchant Shop and Retail Service";
		}elseif($Category==22){
			$Category = "Plant Industry";
		}elseif($Category==23){
			$Category = "Medium Petrol Filling Station";
		}elseif($Category==24){
			$Category = "Small Petrol Filling Station";
		}elseif($Category==25){
			$Category = "Large Petrol Filling Station";
		}elseif($Category==26){
			$Category = "Commercial and industrial premises/offices";
		}elseif($Category==27){
			$Category = "Premises storing dangerous inflammable materials";
		}elseif($Category==29){
			$Category = "High Rise Buildings-Up to 10 th floor";
		}elseif($Category==30){
			$Category = "High Rise Buildings-Ten floors and above";
		}elseif($Category==31){
			$Category = "High Rise Buildings-Up to 4th floor";
		}elseif($Category==32){
			$Category = "Inspection of petrol tanker";
		}elseif($Category==33){
			$Category = "Sale & service of fire equipment 17500";
		}*/
		$Owner = $Owner;
		$PlotNo = $Building;
		$LRNO = $Building;
		#$Frontingon = $result->Frontingon;
		#$Building = $result->Building;
		#$FloorNo = $result->FloorNo;
		$Refid = $Refid;
		$Amount = $amount;
		$Receiptno = "111122333/00002588";
		$expiryDate = date('Y')."-12-31";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8);

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412);
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275);
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=Firepermit.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
        //redirect('en/land_rates');
	}
















}
