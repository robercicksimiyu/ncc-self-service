<?php

class Health_model extends CI_Model {

    // function sendMail(){
    //     $this->email->from('samsoft12@gmail.com', 'Samuel');
    //     $this->email->to('samsoft12@gmail.com');
    //     #$this->email->cc('another@another-example.com');
    //     #$this->email->bcc('them@their-example.com');
    //     $this->email->subject('Email Test');
    //     $this->email->message('Testing the email class.');
    //     $this->email->send();

    //     echo $this->email->print_debugger();
    // }

	function getSubcounties(){
		$result= $this->db->query("SELECT * FROM subcounty");
		if ($result->num_rows<0){
			return 0;
		}
		else{
			return $result->result();
		}
	}

	function getWards(){
		$subcounty = $this->input->post('subcounty');
		$result = $this->db->query("SELECT * FROM ward where sid ='$subcounty'");
		if ($result->num_rows<0){
			echo "<div class='alert alert-info'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                  No Ward for selected Sub county
                </div>";
		}
		else{
			#var_dump($result->result());die();
			$wards =  $result->result();
			echo "<select class='form-control chosen-select' id='ward' name='ward'>";
			foreach($wards as $key=>$value) {
				echo "<option value='$value->id'>$value->name</option>";
			}
			echo "</select>";
		}
	}

	function getHygieneBusinessDetails(){

		$mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));



        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=>$agentRef,
        	"businessId"=>$businessId,
        	"calendarYear"=>$year,
        	"custMobileNo"=>$Tel_no,
        	"pass"=> $pass

        	);

        try {
        	$client = new SoapClient($mainurl);
        } catch (Exception $e) {
        	redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

        $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
        $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";

        $serviceArgs = array(
        	"API_Id" =>$apiID,
			"Fullname"=>$fullnames,
			"mail"=>$email,
			"Mobile"=>$Tel_no,
			"Institution_name"=>$bizname,//$Firm,
			#"Occupation"=>$bizactivityname,//$Occupation,
			"Address"=>$pobox,//$Owner,
			"Plot_no"=> $plotnumber,
			"LRNO"=> $plotnumber,
			"form_id"=> "FH",
			"Purpose"=>$bizactivityname,
			"Nature"=>$bizactivityname,
			"category"=>$category,
			"subcounty"=>"dagoretti south",
			"zone"=>"ngando"
			);

        try {
        	$client = new SoapClient($url);
        	$result = $client->PostApplication($serviceArgs);
        	$RefID = $result->PostApplicationResult->string["0"];
        	$InvoiceNo = $result->PostApplicationResult->string["3"];
        } catch (Exception $e) {
        	redirect('health/APIerror');
        }#var_dump($RefID); die();

        if($RefID!="400"){

        	$serviceArguments2 = array(
        		"API_Id" =>$apiID,
        		"Refid"=>$RefID
        		#"category"=>$category
        		);
        	$client2 = new SoapClient($url);
        	$result2 = $client2->GetInvoiceDetails($serviceArguments2); #var_dump($result2); die();

		    $Fullname = $result2->GetInvoiceDetailsResult->Name;
        	$Email = $result2->GetInvoiceDetailsResult->Email;
        	$Firm = $result2->GetInvoiceDetailsResult->Institution_name;
        	$InvoiceNo = $result2->GetInvoiceDetailsResult->InvoiceNum;
        	$Mobile = $result2->GetInvoiceDetailsResult->Tel_no;
        	$status = $result2->GetInvoiceDetailsResult->status;
        	$RefID = $result2->GetInvoiceDetailsResult->RefId;
        	$certificate = $result2->GetInvoiceDetailsResult->certificate;
        	$Amount = $result2->GetInvoiceDetailsResult->amount;
        	$totalAmount = $certificate + $Amount;
		
		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$Mobile,
			"Firm"=>$Firm,
			"InvoiceNo"=>$InvoiceNo,
			"status"=>$status,
			"certificate"=> $certificate,
			"Amount"=>$Amount,
			"total"=>$totalAmount,
			"plotno"=>$plotnumber,
			"nature"=>$category
			);
#var_dump($this->db->last_query()); die();
		try {
			$this->db->insert('fhygiene',$data);	
		} catch (Exception $e) {

		}
        //$this->sendMail();
        // try {
            
        // } catch (Exception $e) {
            
        // }

		return $data;
	}elseif($RefID=="400"){
		redirect('health/error');
	}
	}else{
		redirect('health/invalidBID');
	}
}

function getInstitutionBusinessDetails(){

		$mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        #$category = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));



        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=>$agentRef,
        	"businessId"=>$businessId,
        	"calendarYear"=>$year,
        	"custMobileNo"=>$Tel_no,
        	"pass"=> $pass

        	);

        try {
        	$client = new SoapClient($mainurl);
        } catch (Exception $e) {
        	redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

        $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
        $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";

        $serviceArgs = array(
        	"API_Id" =>$apiID,
			"Fullname"=>$fullnames,
			"mail"=>$email,
			"Mobile"=>$Tel_no,
			"Institution_name"=>$bizname,//$Firm,
			"Address"=>$pobox,//$Owner,
			"Plot_no"=> $plotnumber,
			"LRNO"=> $plotnumber,
			"form_id"=> "ML",
			"Purpose"=>$bizactivityname,
			"Nature"=>$bizactivityname,
			"category"=>"Institution",
			"subcounty"=>"dagoretti south",
			"zone"=>"ngando"
			);

        try {
        	$client = new SoapClient($url);
        	$result = $client->PostApplication($serviceArgs);
        	$RefID = $result->PostApplicationResult->string["0"];
        	$InvoiceNo = $result->PostApplicationResult->string["3"];
        } catch (Exception $e) {
        	redirect('health/APIerror');
        }#var_dump($RefID); die();

        if($RefID!="400"){

        	$serviceArguments2 = array(
        		"API_Id" =>$apiID,
        		"Refid"=>$RefID
        		#"category"=>$category
        		);
        	$client2 = new SoapClient($url);
        	$result2 = $client2->GetInvoiceDetails($serviceArguments2); #var_dump($result2); die();

		    $Fullname = $result2->GetInvoiceDetailsResult->Name;
        	$Email = $result2->GetInvoiceDetailsResult->Email;
        	$Firm = $result2->GetInvoiceDetailsResult->Institution_name;
        	$InvoiceNo = $result2->GetInvoiceDetailsResult->InvoiceNum;
        	$Mobile = $result2->GetInvoiceDetailsResult->Tel_no;
        	$status = $result2->GetInvoiceDetailsResult->status;
        	$RefID = $result2->GetInvoiceDetailsResult->RefId;
        	$certificate = $result2->GetInvoiceDetailsResult->certificate;
        	$Amount = $result2->GetInvoiceDetailsResult->amount;
        	$totalAmount = $certificate + $Amount;
		
		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$Mobile,
			"Firm"=>$Firm,
			"InvoiceNo"=>$InvoiceNo,
			"status"=>$status,
			"certificate"=> $certificate,
			"Amount"=>$Amount,
			"total"=>$totalAmount,
			"plotno"=>$plotnumber,
			"nature"=>$bizactivityname
			);
#var_dump($this->db->last_query()); die();
		try {
			$this->db->insert('fhygiene',$data);	
		} catch (Exception $e) {

		}

		return $data;
	}elseif($RefID=="400"){
		redirect('health/error');
	}
	}else{
		redirect('health/invalidBID');
	}
}
	
function payHygieneDetails(){

		$url = 'http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
		$apiID = "d3775f2b-8fbe-4701-9dad-3db643021dd5";
		$RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
		$serviceArguments2 = array(
			"API_Id" =>$apiID,
			"Refid"=>$RefID
        		#"category"=>$category
			);
		$client2 = new SoapClient($url);
        	$result2 = $client2->GetInvoiceDetails($serviceArguments2); #var_dump($result2); die();

            @$rescode = $result2->GetInvoiceDetailsResult->Response_Code;
            if (!isset($rescode) ){
        	$Fullname = $result2->GetInvoiceDetailsResult->Name;
        	$Email = $result2->GetInvoiceDetailsResult->Email;
        	$Firm = $result2->GetInvoiceDetailsResult->Institution_name;
        	$InvoiceNo = $result2->GetInvoiceDetailsResult->InvoiceNum;
        	$Mobile = $result2->GetInvoiceDetailsResult->Tel_no;
        	$status = $result2->GetInvoiceDetailsResult->status;
        	$RefID = $result2->GetInvoiceDetailsResult->RefId;
        	$certificate = $result2->GetInvoiceDetailsResult->certificate;
        	$Amount = $result2->GetInvoiceDetailsResult->amount;
        	$total = $certificate+$Amount;

        	$data = array(
        		"RefID"=>$RefID,
        		"issueDate"=>date("Y/m/d"),
        		"Fullname"=>$Fullname,
        		"Email"=>$Email,
        		"TelNumber"=>$Mobile,
        		"Firm"=>$Firm,
        		"InvoiceNo"=>$InvoiceNo,
        		"status"=>$status,
        		"certificate"=> $certificate,
        		"Amount"=>$Amount,
        		"total"=>$total
        		);
        	try {
        	} catch (Exception $e) {

        	}

        	return $data;
        }
        else{
        redirect('health/invalidRefid');
        }
    }

    function payInstitutionDetails(){

		$url = 'http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
		$apiID = "d3775f2b-8fbe-4701-9dad-3db643021dd5";
		$RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
		$serviceArguments2 = array(
			"API_Id" =>$apiID,
			"Refid"=>$RefID
        		#"category"=>$category
			);
		try{
		$client2 = new SoapClient($url);
        	$result2 = $client2->GetInvoiceDetails($serviceArguments2); #var_dump($result2); die();
		} catch (Exception $e) {
			redirect('health/APIerror');
		}

        	$Fullname = $result2->GetInvoiceDetailsResult->Name;
        	$Email = $result2->GetInvoiceDetailsResult->Email;
        	$Firm = $result2->GetInvoiceDetailsResult->Institution_name;
        	$InvoiceNo = $result2->GetInvoiceDetailsResult->InvoiceNum;
        	$Mobile = $result2->GetInvoiceDetailsResult->Tel_no;
        	$status = $result2->GetInvoiceDetailsResult->status;
        	$RefID = $result2->GetInvoiceDetailsResult->RefId;
        	$certificate = $result2->GetInvoiceDetailsResult->certificate;
        	$Amount = $result2->GetInvoiceDetailsResult->amount;
        	$total = $certificate+$Amount;

        	$data = array(
        		"RefID"=>$RefID,
        		"issueDate"=>date("Y/m/d"),
        		"Fullname"=>$Fullname,
        		"Email"=>$Email,
        		"TelNumber"=>$Mobile,
        		"Firm"=>$Firm,
        		"InvoiceNo"=>$InvoiceNo,
        		"status"=>$status,
        		"certificate"=> $certificate,
        		"Amount"=>$Amount,
        		"total"=>$total
        		);
        	try {
        	} catch (Exception $e) {

        	}

        	return $data;
    }

	function completeFHPayment(){

		$url = 'http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
		$apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
		$Refid=str_replace(" ","",$this->input->post('refid'));
		$InvoiceNum= $this->input->post('InvoiceNo');
		$Amount= $this->input->post('amount');
		$Certificate= $this->input->post('certificate');
		$totalAmount = $Amount+$Certificate;
		$paymentinfo = array(
			"API_Id"=>$apiID,
			"Refid" => $Refid,
			"InvoiceNum"=>$InvoiceNum,
			"amount" => $Amount,
			"year"=> date("Y"),
			"certificate"=>$Certificate,
			"form_id"=>"FH"
			);

		$client = new SoapClient($url);
		$result = $client->PostPayment($paymentinfo);
		$rescode = $result->PostPaymentResult; 

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"PlotNo" => $this->input->post('plotno'),
		"LRNO" => $this->input->post('lrno'),
		"InvoiceNo" => $this->input->post('InvoiceNo'),
		"Certificate" => $this->input->post('certificate'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"total"=> $totalAmount,
		"rescode"=>$rescode
		);
		#var_dump($rescode); die();
		return $data;
	}

	function inputInstitutionFormDetails(){

		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";

		$Fullname = $this->input->post('full_names');
		$institutionname = $this->input->post('institution_name');
		$address = $this->input->post('address');
		$email = $this->input->post('email');
		$plotno = $this->input->post('plot_no');
		$road = $this->input->post('road');
		$mobile = $this->input->post('mobile');
		$purpose = $this->input->post('purpose');
		$nature = $this->input->post('nature');

		$serviceArguments = array(
			"Fullname"=>$Fullname,
			"Institution_name"=>$institutionname,
			"Address"=>$address,
			"mail"=>$email,
			"Plot_no"=>$plotno,
			"Road"=>$road,
			"Mobile"=> $purpose,
			"Nature"=> $nature
			);
		
		try {
		$client = new SoapClient($url);
		$result = $client->PostInstitutionLearning($serviceArguments); #var_dump($result); die();
		$RefID = $result->PostInstitutionLearningResult->string["0"];	
		} catch (Exception $e) {
		redirect('health/APIerror');
		}
		
		
		if($RefID!="400"){

		$serviceArguments2 = array(
			"Refid"=>$RefID
			);
		$client2 = new SoapClient($url);
		$result2 = $client2->GetInstituionDetails($serviceArguments2);

		#$Fullname = $result2->GetApplicationDetailsResult->Fullname;
		$Email = $result2->GetInstituionDetailsResult->Email;
		$Name = $result2->GetInstituionDetailsResult->Name;
		$Address = $result2->GetInstituionDetailsResult->Address;
		$Plot_no = $result2->GetInstituionDetailsResult->Plot_no;
		$Road = $result2->GetInstituionDetailsResult->Road;
		$Mobile = $result2->GetInstituionDetailsResult->Mobile;
		$Purpose = "BUSINESS";//$result2->GetInstituionDetailsResult->Purpose;
		$Nature = $result2->GetInstituionDetailsResult->Nature;
		$Status = $result2->GetInstituionDetailsResult->Status;
		$Amount = $result2->GetInstituionDetailsResult->Amount;
		
		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Name"=>$Name,
			"Email"=>$Email,
			"PlotNo"=> $Plot_no,
			"Mobile"=>$Mobile,
			"Purpose"=>$Purpose,
			"Nature"=>$Nature,
			"Address"=> $Address,
			"Status"=>$Status,
			"Amount"=>$Amount
			);
        
        try {
        $this->db->insert('healthinstitution',$data);	
        } catch (Exception $e) {
        	
        }
        return $data;
    }else{
    	redirect('health/error');
    }
	}

	function completeIHPayment(){

		$url = 'http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";
		$apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
		$Refid=str_replace(" ","",$this->input->post('refid'));
		$InvoiceNum= $this->input->post('InvoiceNo');
		$Amount= $this->input->post('amount');
		$Certificate= $this->input->post('certificate');
		$totalAmount = $Amount+$Certificate;
		$paymentinfo = array(
			"API_Id"=>$apiID,
			"Refid" => $Refid,
			"InvoiceNum"=>$InvoiceNum,
			"amount" => $Amount,
			"year"=> date("Y"),
			"certificate"=>$Certificate,
			"form_id"=>"FH"
			);

		$client = new SoapClient($url);
		$result = $client->PostPayment($paymentinfo);
		$rescode = $result->PostPaymentResult; #var_dump($result); die();

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"PlotNo" => $this->input->post('plotno'),
		"LRNO" => $this->input->post('lrno'),
		"InvoiceNo" => $this->input->post('InvoiceNo'),
		"Certificate" => $this->input->post('certificate'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"total"=> $totalAmount,
		"rescode"=>$rescode
		);
		#var_dump($rescode); die();
		return $data;
	}

	function printHealthPermit($refid){

        $url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";

        $Refid=$this->uri->segment(3);//strtoupper(str_replace(" ","",$this->input->post('refid'))) ;
        $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
        $data = array("RefId"=>$Refid,"API_Id"=>$apiID);

        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Healthpermit.pdf';

        try {
        $client = new SoapClient($url);
        $result = $client->PrintPermit($data);  #var_dump($result); die();
        } catch (Exception $e) {
            redirect('health/APIerror');
        }
        @$rescode = $result->PrintPermitResult->Response_Code;

        $result = $client->PrintPermit($data);
        $Fullname=$result->PrintPermitResult->Name;
        $Firm=$result->PrintPermitResult->Institution_name;
        $InvoiceNum=$result->PrintPermitResult->InvoiceNum;
        $Email=$result->PrintPermitResult->Email;
        $TelNumber=$result->PrintPermitResult->Tel_no;
        $certificate=$result->PrintPermitResult->certificate;
        $amount=$result->PrintPermitResult->amount;
        $Refid=$result->PrintPermitResult->RefId;
        $inspection_status=$result->PrintPermitResult->inspection_status;
        $totalAmount=$certificate+$amount;

		
        $query="select * from fhygiene where RefID='$Refid' order by RefID desc";
        $result=$this->db->query($query);
        $result=$result->row();#var_dump($result); die();

        //$Fullname = $result->Fullname;
		//$Email = $result->Email;
		//$Tel_no = $result->TelNumber;
		//$Firm = $result->Firm;
		$Nature =$result->nature;
		#$Owner = $result->Owner;
		$PlotNo = $result->plotno;
		$LRNO = $result->plotno;
		#$Frontingon = $result->Frontingon;
		#$Building = $result->Building;
		#$FloorNo = $result->FloorNo;
		//$Refid = $result->RefID;
		//$Amount = $result->total;
		$Receiptno = "111122333/00002588";
		$expiryDate = "2016-03-27";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        $page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Nature), 190, 398);
        $page->drawText(number_format($totalAmount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printHealthPermitreceipt($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/fhinvoice.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from fhygiene where RefID='$refid'";
        $result=$this->db->query($query); 
        $result=$result->row();#var_dump($result); die();

        $Businessname = $result->Firm;
		$Owner = $result->Fullname;
		$Refid = $result->RefID;
		$InvoiceNo = $result->InvoiceNo;
		$Certificate = $result->certificate;
		$FHAmount =$result->Amount;
		$TotalAmount = $result->total;
		$Status = $result->status;
		if($Status=="0"){
			$Status = "WAITING FOR APPROVAL";
			}else{
			$Status = "APPROVED";
			}

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $page->drawText(number_format($FHAmount,2), 320, 320);
        $page->drawText($Businessname, 250, 528);
        $page->drawText($Owner, 250, 480); 
        $page->drawText($Refid, 250, 440);
        $page->drawText($InvoiceNo, 250, 400); 
        $page->drawText(number_format($Certificate,2), 320, 357);
        $page->drawText(number_format($TotalAmount,2), 250, 280); 
        $page->drawText($Status, 200, 232);

        #$barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        // $rendererOptions = array(
        //     'topOffset' => 600,
        //     'leftOffset' =>295
        //     );
        #$pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=healthInvoice.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

    function printHealthPermitPayment($refid){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/healthreceipt.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from fhygiene where RefID='$refid' order by `id` desc";
        $result=$this->db->query($query); 
        $result=$result->row();#var_dump($result); die();

        $Businessname = $result->Firm;
        $date = $result->issueDate;
        $date = substr($date,0,10);
        $Owner = $result->Fullname;
        $Refid = $result->RefID;
        $InvoiceNo = $result->InvoiceNo;
        $Certificate = $result->certificate;
        $FHAmount =$result->Amount;
        $TotalAmount = $result->total;
        $receiptno = "11122334/000215875";
        if( is_numeric( $TotalAmount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($TotalAmount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";
        $description= "Health License ID - ".$Refid; 

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
          $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 480, 528);
        $page->drawText($Businessname, 250, 480); 
        $page->drawText(number_format($TotalAmount,2), 480, 480);
        $page->drawText($amount_in_words, 150, 437); 
        $page->drawText($description, 150, 390);
        $page->drawText($Owner, 127, 38);
        // $page->drawText(number_format($penalties,2), 450, 270);
        // $page->drawText(number_format($amount,2), 450, 250);
        // $page->drawText(number_format($amount,2), 450, 225);
        // $page->drawText(number_format($balance_due,2), 450, 200);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=healthreceipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

	function printInstitutionHealthPermit($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Healthpermit.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from healthinstitution where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$PlotNo = $result->PlotNo;
		$Purpose =$result->Purpose;
		$Nature = $result->Nature;
		$Mobile = $result->Mobile;
		$Address = $result->Address;
		#$Frontingon = $result->Frontingon;
		#$Building = $result->Building;
		#$FloorNo = $result->FloorNo;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); var_dump($refid); die();

        $page->drawText(strtoupper($Fullname), 210, 440);
        $page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($PlotNo), 190, 414);
        $page->drawText(strtoupper($Address), 400, 412); 
        $page->drawText(strtoupper($Purpose), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printInstitutionHealthPermitreceipt($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/healthreceipt.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from healthinstitution where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$Name = $result->Name;
		$Mobile = $result->Mobile;
		$Purpose = $result->Purpose;
		$PlotNo = $result->PlotNo;
		$Nature = $result->Nature;
		$Address = $result->Address;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";
		$issueDate = $result->issueDate;
		$description= "APPLICATION FOR $Fullname , PERMIT ID - ".$Refid;
		if( is_numeric( $Amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($Amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";
        $from = $this->session->userdata('name');

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
          $page->drawText($Receiptno, 150, 528);
        $page->drawText($expiryDate, 480, 528);
        $page->drawText($from, 250, 480);
        $page->drawText($from, 127, 38); 
        $page->drawText(number_format($Amount,2), 480, 480);
        $page->drawText($amount_in_words, 150, 437); 
        $page->drawText($description, 150, 390);
        // $page->drawText(number_format($sbpfee,2), 450, 270);
        // $page->drawText(number_format($regfees,2), 450, 250);
        // $page->drawText(number_format($amount,2), 450, 225);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printInstitution(){
		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";

		$Refid=strtoupper($this->input->post('refid')) ;
		$apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
		$data = array("Refid"=>$Refid,"API_Id"=>$apiID);

		try {
		$client = new SoapClient($url);
		$result = $client->PrintInstituionDetails($data);	#var_dump($result); die();
		} catch (Exception $e) {
			redirect('health/APIerror');
		}
		@$rescode = $result->PrintInstituionDetailsResult->Response_Code;

		if (!isset($rescode) ){
		$result = $client->PrintInstituionDetails($data);
		$Name=$result->PrintInstituionDetailsResult->Name;
		$Institution_name=$result->PrintInstituionDetailsResult->Institution_name;
		$Address=$result->PrintInstituionDetailsResult->Address;
		$Email=$result->PrintInstituionDetailsResult->Email;
		$Plot_no=$result->PrintInstituionDetailsResult->Plot_no;
		$Road=$result->PrintInstituionDetailsResult->Road;
		$Mobile=$result->PrintInstituionDetailsResult->Mobile;
		$Purpose=$result->PrintInstituionDetailsResult->Purpose;
		$Nature=$result->PrintInstituionDetailsResult->Nature;
		$Status=$result->PrintInstituionDetailsResult->Status;
		$Amount=$result->PrintInstituionDetailsResult->Amount;
		#return $result;

		$printdata = array(
		"Refid"=>$Refid,
		"Name"=>$Name,
		"Institution_name"=>$Institution_name,
		"Address"=>$Address,
		"Email"=>$Email,
		"Plot_no"=>$Plot_no,
		"Road"=>$Road,
		"Mobile"=>$Mobile,
		"Purpose"=>$Purpose,
		"Nature"=>$Nature,
		"Status"=>$Status,
		"Amount"=>$Amount
		);
		return $printdata;
		
		}
		else{
		redirect('health/invalidRefid');
		}
	}

	function printHygiene(){
		$url='http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?WSDL";

		$Refid=strtoupper(str_replace(" ","",$this->input->post('refid'))) ;
		$apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
		$data = array("RefId"=>$Refid,"API_Id"=>$apiID);

		try {
		$client = new SoapClient($url);
		$result = $client->PrintPermit($data);	#var_dump($result); die();
		} catch (Exception $e) {
			redirect('health/APIerror');
		}
		@$rescode = $result->PrintPermitResult->Response_Code;

		if (!isset($rescode) ){
		$result = $client->PrintPermit($data);
		$Fullname=$result->PrintPermitResult->Name;
		$Firm=$result->PrintPermitResult->Institution_name;
		$InvoiceNum=$result->PrintPermitResult->InvoiceNum;
		$Email=$result->PrintPermitResult->Email;
		$TelNumber=$result->PrintPermitResult->Tel_no;
		$certificate=$result->PrintPermitResult->certificate;
		$amount=$result->PrintPermitResult->amount;
		$Refid=$result->PrintPermitResult->RefId;
		$inspection_status=$result->PrintPermitResult->inspection_status;
		$totalAmount=$certificate+$amount;
		#return $result;

		$data = array(
			"RefID"=>$Refid,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$TelNumber,
			"Firm"=>$Firm,
			"Status"=>$inspection_status,
			"Amount"=>$totalAmount
			);
		return $data;
		
		}
		else{
		redirect('health/invalidRefid');
		}
	}

}
