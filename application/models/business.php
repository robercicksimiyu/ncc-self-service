<?php

class Business extends CI_Model
{
  private $jpwnumber;
  function __construct()
  {
    $this->jpwnumber=$this->session->userdata('jpwnumber');

  }
  function get_business_classes($activityId=NULL)
  {
    $results = curl_rest_client("GetSBPClasses?stream=sbp&PhoneNumber=".$this->session->userdata('jpwnumber')."&ActivityID=$activityId",'GET');
    return json_decode($results,true);
  }

  function get_business_details($BusinessID,$Year)
  {   
    $results = curl_rest_client("GetBusiness?stream=sbp&LicenseID=$BusinessID&Year=$Year&PhoneNumber=".$this->session->userdata('jpwnumber'),'GET');
    return json_decode($results,true);
  }

  function get_business_sub_classes($bizClassId)
  {
    $results = curl_rest_client("/getsbpsubclasses?stream=sbp&PhoneNumber=".$this->session->userdata('jpwnumber')."&id=$bizClassId",'GET');
    return json_decode($results,true);
  }

  function getSubActivityCharges()
  {

      $token = $this->session->userdata('token');
      $SubClassID = @trim($this->input->post('sub_classes'));        
      $PhoneNumber = $this->session->userdata('jpwnumber');
      $url = "http://192.168.6.10/JamboPayServices/api/payments/GetSBPActivityCharges?stream=sbp&SubClassID=$SubClassID&PhoneNumber=$PhoneNumber";
      $results = curl_rest_client("/getsbpsubclasses?stream=sbp&PhoneNumber=".$this->session->userdata('jpwnumber')."&id=$bizClassId",'GET');
      return json_decode($results,true);
        $results = curl_exec($ch);
      curl_close($ch);
      return $results;

  }

    function sbp_topup_init()
    {
        $token = $this->session->userdata('token');
        $url = "http://192.168.6.10/JamboPayServices/api/payments/getnewbusinessdetails?stream=sbp";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);


        if (isset($res->Message)) {
            return $data = array('rescode' => 55);
        } else {
            $bizactivity = $res->BusinessActivity;
            $relsize = $res->BusinessSizes;
            $idtype = $res->IDTypes;
            $zones = $res->Zones;
            $wards = $res->Wards;

            $data = array(
                'bizactivity' => $bizactivity,
                'relsize' => $relsize,
                'idtype' => $idtype,
                'zones' => $zones,
                'wards' => $wards,
                'rescode' => 0
            );


            return $data;
        }

    }

    // prepare topup for sbp
    function sbptopupprepare()
    {
        $token = $this->session->userdata('token');
        $PaidBy = $this->input->post('paidby');
        $ActivityCode = trim($this->input->post('businessactivity'));
        $Year = trim($this->input->post('year'));
        $BusinessId = $this->input->post('businessid');
        $PhoneNumber = $this->session->userdata('jpwnumber');

        $toSend = array(
            'Stream' => 'sbp',
            'PhoneNumber' => $PhoneNumber,
            'PaymentTypeID' => 2,
            'PaidBy' => $PaidBy,
            'BusinessID' => $BusinessId,
            'ActivityCode' => $ActivityCode,
            'SBPPaymentType' => 2,
            'Year' => $Year
        );

        // var_dump(http_build_query($toSend));
        $url = "http://192.168.6.10/JamboPayServices/api/payments/post";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($toSend));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);

        echo curl_error($ch) . '<br/>';
        curl_close($ch);

        // var_dump($result);
        // die();
        return json_decode($result);

    }


    function new_token()
    {
        $tosend = array(
            'grant_type' => "agency",
            'username' => "teller1.branch1@agency.com",
            'password' => 'p@ssw0rd'
        );

        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }

        $post_string = implode('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/token";
        $ch = curl_init($url);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        return $res->access_token;
    }

    function sbp_topup_change_api()
    {
        $token = $this->session->userdata('token');

        $biz_id = $this->input->post('biz_id');
        $activity_code = $this->input->post('businessactivity');

        $data = array(
            'Stream' => 'sbp',
            'PhoneNumber' => $this->session->userdata('jpwnumber'),
            'BusinessID' => $biz_id,
            'ActivityCode' => $activity_code
        );

        $url = "http://192.168.6.10/JamboPayServices/api/payments/putsbpactivitycode/";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public $serviceArgs;

    function insertReceiptDetails()
    {
        $businessid = $this->input->post('businessid');
        $annualamount = $this->input->post('annualamount');
        $date = $this->input->post('issuedate');
        $date1 = substr($date, 0, 10); //date("Y-m-d",$date);
        $receiptno = $this->input->post('receiptno');
        // $regno = $this->input->post('regno');
        // $regno = strtoupper($regno);
        $paidby = $this->input->post('paidby');
        $paidby = mysql_real_escape_string($paidby);
        $amount = $this->input->post('amount');
        $amountdue = $this->input->post('amount');
        $penalties = $this->input->post('penalties');
        $phoneno = $this->input->post('phonenumber');
        $channel = "Manual";
        #var_dump($date1); die();
        try {
            $data = array('receiptno' => $receiptno, 'issuedate' => $date1, 'from' => $paidby, 'amount' => $amount, 'businessid' => $businessid, 'penalties' => $penalties, 'annualamount' => $annualamount, 'amountdue' => $amountdue, 'username' => $phoneno, 'cashiername' => $paidby, 'channel' => $channel);

            // var_dump($data);


            $this->db->insert('sbp', $data);
            // $query = "insert into `sbp` (`receiptno`,`issuedate`,`paidby`,`amount`,`businessid`,`penalties`,`annualamount`,`amountdue`,`username`,`cashiername`,`channel`)";
            // $query.= " values ('".implode("','",$data)."')";
            // $this->db->query($query);
            //           var_dump($this->db->query($query));
            #echo $this->db->error_message();

            // var_dump(mysql_error());
            redirect('sbp/reprintSReceipt');
        } catch (Exception $e) {
            redirect('sbp/insertManualSbpReceipt');
        }
    }

    function preparePayment()
    {

        $url = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY; //"3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = @trim($this->input->post('biz_idd'));
        $year = $this->input->post('year');
        $number = $this->session->userdata['jpwnumber'];//"254".$num;
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $ck = $username . $number . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck));

        $serviceArguments = array(
            "userName" => $username,
            "agentRef" => $agentRef,
            "businessId" => $businessId,
            "calendarYear" => $year,
            "jpwMobileNo" => $number,
            "pass" => $pass

        );

        try {
            $client = new SoapClient($url);
            // var_dump($url);
            // var_dump($client);die();
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }

        $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
        $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
        $resultcode = $result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
        #$bill_status=$result->PreparePaymentSBPNCCResult->BillStatus;
        #if ($resultcode==0 && $resulttext=="OK" ){
        $trans_id = $result->PreparePaymentWalletSBPNCCResult->TransactionID;
        $bizna_id = $result->PreparePaymentWalletSBPNCCResult->BusinessId;
        $bizna_name = $result->PreparePaymentWalletSBPNCCResult->BusinessName;
        $physical_add = $result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
        $bill_status = $result->PreparePaymentWalletSBPNCCResult->BillStatus;
        $sbp_fee = $result->PreparePaymentWalletSBPNCCResult->SBPFee;
        $penalty = $result->PreparePaymentWalletSBPNCCResult->Penalty;
        $annualamount = $result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
        $billno = $result->PreparePaymentWalletSBPNCCResult->BillNumber;
        #$phone="254712633277";
        #if($bill_status=="NEW"){
        $data = array(
            'trans_id' => $trans_id,
            'biz_name' => $bizna_name,
            'physical_address' => $physical_add,
            'bill_stat' => $bill_status,
            'biz_id' => $bizna_id,
            'pass' => $pass,
            'mobile_no' => $number,
            'penalty' => $penalty,
            'annualamount' => $annualamount,
            'billno' => $billno,
            'sbpfee' => $sbp_fee,
            'year' => $year,
            'resultcode' => $resultcode,
            'resulttext' => $resulttext
        );

        $this->session->set_userdata($data);

        return $data; #}
    }


    function complete()
    {

        $url = MAIN_URL;
        $jpwMobileNo = $this->session->userdata('jpwnumber');
        $username = USER_NAME;
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->session->userdata('biz_id');
        $amount = $this->session->userdata('sbpfee');
        $businessname = $this->session->userdata('biz_name');
        $year = $this->session->userdata('year');
        $tranid = $this->session->userdata('trans_id');
        $channelRef = "45875";
        $ck = $username . $tranid . $key;
        $pass = sha1(utf8_encode($ck));
        $pin = $this->input->post('jp_pin');
        $name = $this->session->userdata('name');
        $name = mysql_real_escape_string($name);
        $penalty = $this->session->userdata('penalty');
        $annualamount = $this->session->userdata('annualamount');
        $channel = "SELFSERVICE";

        $data_complete = array(
            "userName" => $username,
            "transactionId" => $tranid,
            "jpPIN" => $pin,
            "CalenderYear" => $year,
            "amount" => $amount,
            "currency" => "KES",
            "channelRef" => "45875",
            "pass" => $pass

        );

        try {
            $client = new SoapClient($url);
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->CompletePaymentWalletSBPNCC($data_complete);
        $resulttext = $result->CompletePaymentWalletSBPNCCResult->Result->ResultText;
        $resultcode = $result->CompletePaymentWalletSBPNCCResult->Result->ResultCode;

        if ($resultcode == 0 && $resulttext == "OK") {
            $paid = $result->CompletePaymentWalletSBPNCCResult->Paid;
            $ReceiptNo = $result->CompletePaymentWalletSBPNCCResult->ReceiptNo;

            try {
                $data = array('receiptno' => $ReceiptNo, 'issuedate' => DATE('Y-m-d h:i:s', time()), 'from' => $name, 'amount' => $amount, 'businessid' => $businessId, 'penalties' => $penalty, 'annualamount' => $annualamount, 'amountdue' => $amount, 'username' => $this->session->userdata('jpwnumber'), 'cashiername' => $name, 'channel' => $channel);

                $query = "insert into sbp(`receiptno`,`issuedate`,`from`,`amount`,`businessid`,`penalties`,`annualamount`,`amountdue`,`username`,`cashiername`,`channel`)";
                $query .= " values ('" . implode("','", $data) . "')";
                $this->db->query($query);
            } catch (Exception $e) {

            }

            #sms
            try {
                $tel = $this->session->userdata('jpwnumber');
                $amnt = number_format($amount, 2, '.', ',');
                $msg = "Receipt Number  $ReceiptNo .Your payment of KES $amnt for Business ID $businessId has been received by Nairobi City County.Powered by:JamboPay";
                $key = "63bc8b3e-e674-4b08-879c-02e1aceedb8f";

                $APIKey = urlencode($key);
                $Phone = urlencode($tel);
                $relayCode = urlencode("WebTribe");
                $Message = urlencode($msg);
                $Shortcode = urlencode("700273");
                $CampaignId = urlencode("112623");
                $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://192.168.7.61/smsServer/SendSMS.aspx' . $qstr);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);

            } catch (Exception $e) {

            }

            #sms

            $data = array(
                "paidamount" => $paid,
                "receiptno" => $ReceiptNo,
                "businessname" => $businessname,
                "businessid" => $businessId,
                "for" => "SBP Renewal",
                "restext" => $resulttext,
                "rescode" => $resultcode
            );

            return $data;
        } else {
            return $data = array("restext" => $resulttext, "rescode" => $resultcode);
        }


        #return $result;

    }


    function confirmPlotCounty()
    {
        $token = $this->session->userdata['token'];

        $curl = curl_init();
        $ZoneCode = $this->input->post('zonecode');
        $PlotNumber = $this->input->post('plot');
        $PlotNumber = urlencode($PlotNumber);


        $post_url = "http://192.168.6.10/JamboPayServices/api/payments/GetValidSBPPlot?stream=sbp&SubCountyID=$ZoneCode&PlotNumber=$PlotNumber";


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $results = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($results);

        // if Message is present plot is not in county
        if (isset($res->Message)) {
            return False;
        } else {
            return True;
        }
    }


    

    function confirm()
    {
        
        $data=$this->testPreparePaymentNew();
        $bizname = $this->input->post('bizname');
        $amount = $data['Amount'];
        $transid = $data['TransactionID'];
        $bid = $data['BusinessID'];
        $year = $data['Year'];
        $penalty = $data['Penalty'];
        $ContactPersonName = $this->input->post('contactpersonname');
        $IDNumber = $this->input->post('idnumber');
        $Telephone1 = $this->input->post('telephone');
        $BuildingCategory = $this->input->post('buildingcategory');
        $Building = $this->input->post('building');
        $Floor = $this->input->post('floor');
        $RoomStallNumber = $this->input->post('room_stall_number');
        $ZoneName = $this->input->post('zonename');
        $WardName = $this->input->post('wardcode');
        $ZoneCode = $this->input->post('zonecode');
        $WardCode = $this->input->post('wardcode');
        $PlotNumber = $this->input->post('plot');
        //$details=$this->session->userdata('bid_prepared_details');        
        $data = array(
            'bizname' => $bizname,
            'amount' => $amount,
            'transid' => $transid,
            'bid' => $bid,
            'year' => $year,
            'penalty' => $penalty,
            'ContactPersonName' => $ContactPersonName,
            'IDNumber' => $IDNumber,
            'Telephone1' => $Telephone1,
            'Building' => $Building,
            'Floor' => $Floor,
            'RoomStallNumber' => $RoomStallNumber,
            'BuildingCategory' => $BuildingCategory,
            'ZoneCode' => $ZoneCode,
            'WardCode' => $WardCode,
            'ZoneName' => $ZoneName,
            'WardName' => $WardName,
            'PlotNumber' => $PlotNumber,
            'AcceptedTnC' => true,
            'SBPSubClassID'=>$data['SBPSubClassID'],
            'sub_streams'=>$data['LineItems'],           
        );       

        $this->traceAPI('POST','confirm-post',json_encode($data));       

        // var_dump($data);
        return $data;

    }

    function saveCompletedTransaction($data)
    {
        $data['transaction_date']=date('Y-m-d');
        $this->db->insert('business_transaction',$data);
        //echo json_encode($data);exit;

    }

    function getCompletedTransaction($param)
    {

        $this->db->where('transaction_id',$param);
        return $this->db->get('business_transaction')->row_array();
        
    }

    function completeNew()
    {     
        $bid_details=$this->session->userdata('bid_prepared_details');
        $token = $this->session->userdata['token'];
        $jpwMobileNo = $this->session->userdata('jpwnumber');
        $businessId = $this->input->post('bid');
        $amount = $this->input->post('amount');
        $transid = $this->input->post('transid');
        $pin = $this->input->post('jp_pin');
        $year = $this->input->post('year');
        $penalty = $this->input->post('penalty');
        $businessname = "";
        $ContactPersonName = $this->input->post('contactpersonname');
        $IDNumber = $this->input->post('idnumber');
        $Telephone1 = $this->input->post('telephone');
        $Building = $this->input->post('building');
        $Floor = $this->input->post('floor');
        $RoomStallNumber = $this->input->post('roomstallnumber');
        $ZoneName = $this->input->post('zonename');
        $WardName = $this->input->post('wardname');
        $ZoneCode = $this->input->post('zonecode');
        $WardCode = $this->input->post('wardcode');
        $PlotNumber = $this->input->post('plot');
        $this->session->set_userdata('sbp_amount',$amount);

        $tosend = array(
            'PhoneNumber' => $jpwMobileNo,
            'TransactionID' => $transid,//"36445",
            'Pin' => $pin,
            'Year' => $year,
            'Stream' => "sbp",
            'AcceptedTnC' => true,
            'ContactPersonName' => $ContactPersonName,
            'IDDocumentNumber' => $IDNumber,
            'Telephone1' => $Telephone1,
            'Building' => $Building,
            'Floor' => $Floor,
            'RoomStallNumber' => $RoomStallNumber,
            'ZoneCode' => $ZoneCode,
            'WardCode' => $WardCode,
            'ZoneName' => $ZoneName,
            'WardName' => $WardName,
            'PlotNumber' => $PlotNumber
        );

        //$transaction_streams=$this->get_streams_on_trans_id($transid);

        $this->traceAPI('POST','comfirm-post',json_encode($tosend));
        //var_dump($tosend);exit;
        

        $this->session->set_userdata('biz_details',$tosend);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://192.168.6.10/JamboPayServices/api/payments/put",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "Stream=sbp&Pin=$pin&SBPPaymentType=0&PhoneNumber=$jpwMobileNo&AcceptedTnC=true&Building=$Building&Floor=$Floor&RoomStallNumber=$RoomStallNumber&ZoneCode=$ZoneCode&ZoneName=$ZoneName&WardCode=$WardCode&WardName=$WardName&PlotNumber=$PlotNumber&IDDocumentNumber=$IDNumber&Telephone1=$Telephone1&ContactPersonName=$ContactPersonName&TransactionID=$transid",
            CURLOPT_HTTPHEADER => array(
                'Authorization: bearer ' . $token,
                'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
            ),
        ));
       
        $result = curl_exec($curl);
        
        curl_close($curl);
        $res = json_decode($result);
        if (!isset($res->ErrorCode)) {
            $data = array(
                'transaction_id'=>$res->TransactionID,
                "paidamount" => $res->Amount,
                "receiptno" => $res->ReceiptNumber,
                "businessname" => $res->BusinessName,
                "businessid" => $res->BusinessID,
                "year" => $res->Year,
                "for" => "SBP Renewal",
                "restext" => "OK",
                "rescode" => "0",
                
            );
            //var_dump($data);
            $this->saveCompletedTransaction($data);
            return $data;
        } else if (isset($res->ErrorCode)) {
            if($res->Message=="Transaction already completed") {                  
           

                $data=$this->getCompletedTransaction($transid);
               
            } else {
                $data = array('rescode' => $res->ErrorCode, 'restext' => $res->Message);
            }
            //var_dump($data);exit;
            return $data;

        } else if (is_null($res)) {
            return $data = array("rescode" => "5", "restext" => 'System Error');
        }
    }

    function print_sbp()
    {
        $token = $this->session->userdata('token');
        $BusinessID = @trim($this->input->post('biz_idd'));
        $Year = $this->input->post('year');
        $PhoneNumber = $this->session->userdata('jpwnumber');

        $url = "http://192.168.6.10/JamboPayServices/api/payments/GetBusinessDetails?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);

        #var_dump($res); die();

        if (isset($res->ErrorCode)) {
            $sbpdata = array(
                'restext' => $res->Message,
                'rescode' => $res->ErrorCode
            );
        } else {
            $sbpdata = array(
                'approvalstatus' => $res->Status,
                'sbpnumber' => $res->BusinessID,
                'year' => $res->Year,
                'bizname' => $res->BusinessName,
                'pinno' => $res->Pin,
                'biztype' => "",
                'activitycode' => $res->ActivityCode,
                'paidfee' => $res->Amount,
                'inwords' => $res->AmountInWords,
                'pobox' => $res->POBox,
                'plotnumber' => $res->PlotNumber,
                'street' => $res->PhysicalAddress,
                'issuedate' => $res->DateIssued,
                'bizactivityname' => $res->ActivityDescription,
                'bizzid' => $res->BusinessID,
                'receiptno' => $res->ReceiptNumber,
                'restext' => "OK",
                'rescode' => 0
            );
        }
        #var_dump($sbpdata); die();
        return $sbpdata;
    }


    function bussinessidcheck()
    {
        $bizId = $this->input->post("bizId");
        $result = $this->db->query("Select businessid,receiptno from sbp where businessid='$bizId' order by issuedate DESC limit 1 ");
        if ($result->num_rows() > 0) {
            $result = $result->row();
            return $result->businessid;
        } else {
            return redirect("sbp/reprintSReceipt/erro1");
        }

    }

    function bussinessidcheckNew()
    {
        $token = $this->session->userdata('token');
        $bizId = $this->input->post("bizId");

        $stream = "sbp";
        $key0 = "BusinessID";
        $value0 = $bizId;
        $key1 = "TransactionStatus";
        $value1 = "Completed";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);

        if ($res['0']->BusinessID == $bizId) {
            return $res['0']->BusinessID;
        } else {
            return redirect("sbp/reprintSReceipt/erro1");
        }

    }

    function reprintSbpreceipt($businessid)
    {
        $this->load->library('zend');
        $a = $this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        #$receiptno = "1111221334/000027570";
        $fileName = APPPATH . 'assets/back/receipts/sbp_receipt.pdf';
        #$receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
        #var_dump($businessid); die();
        $query = "select * from sbp where businessid='$businessid'";
        $result = $this->db->query($query);
        $result = $result->row();
        $receiptno = $result->receiptno;
        $date = substr($result->issuedate, 0, 10);
        $paidby = $result->from;
        $total_amount_due = $result->amountdue;
        $balance_due = "0";//($result->amountdue)-($result->amount);
        #$plot_owner=$result->plotowner;
        $cashier = $result->cashiername;
        $penalties = $result->penalties;
        $amount = $result->amount;
        $amount = str_replace(',', '', $amount);
        if (is_numeric($amount)) {
            $amount_in_words = "** " . strtoupper($this->amount_to_words->convert_number($amount)) . " SHILLINGS ONLY**";
        } else $amount_in_words = "NOT AVAILABLE";

        $description = " BUSINESS ID - " . $result->businessid;

        $pdf = Zend_Pdf::load($fileName);
        $page = $pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
        $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 480, 528);
        $page->drawText($cashier, 250, 480);
        $page->drawText(number_format($amount, 2), 480, 480);
        $page->drawText($amount_in_words, 150, 437);
        $page->drawText($description, 150, 390);
        $page->drawText(number_format($penalties, 2), 450, 270);
        $page->drawText(number_format($amount, 2), 450, 250);
        $page->drawText(number_format($amount, 2), 450, 225);
        $page->drawText(number_format($balance_due, 2), 450, 200);

        $barcodeOptions = array('text' => $receiptno, 'barHeight' => 40, 'factor' => 2.5, 'font' => APPPATH . 'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' => 295
        );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
        //redirect('en/land_rates');
    }


    function getLastPayment($businessid)
    {
         $token = $this->session->userdata('token');         
         $stream = "sbp";
         $key0 = "BusinessID";
         $value0 = $businessid;
         $key1 = "TransactionStatus";
         $value1 = "Completed";
         $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
        
         $ch = curl_init($url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
         #curl_setopt($ch, CURLOPT_POST, true);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
             'Authorization: bearer ' . $token,
             'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
         ));
         $result=curl_exec($ch);
        return $result;
         

    }

    function reprintSbpreceiptNew($businessid,$receiptno='')
    {
        $token = $this->session->userdata('token');
        //echo $businessid.' '.$receiptno;exit;
        $stream = "sbp";
        $key0 = "BusinessID";
        $value0 = $businessid;
        $key1 = "TransactionStatus";
        $value1 = "Completed";
        //if($receiptno!=''){
           /* $key2 = "ReceiptNumber";
            $value2 = $receiptno;
            $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1&[2].Key=$key2&[2].Value=$value2";*/
       // } else {
            $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
       // }
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);


        //echo $result;exit;

        $this->load->library('zend');
        $a = $this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');
        $year= $res['0']->Year;

        $bid_details=$this->session->userdata('bid_prepared_details');
       // var_dump( $res['0']);exit;
        if($year > 2016) {
             $fileName = APPPATH . 'assets/back/receipts/sbp_unified_receipt.pdf';
        } else {
             $fileName = APPPATH . 'assets/back/receipts/sbp_receipt.pdf';
        }
       // echo json_encode($this->session->all_userdata());exit;
       
        $receiptno = $res['0']->ReceiptNumber;
        $date = $res['0']->TransactionDate;
        $date = date('d-m-Y',strtotime(substr($date, 0, 10)));
        $paidby = $res['0']->ContactPersonName;
        $total_amount_due = $res['0']->Amount;
        $balance_due = "0";//($result->amountdue)-($result->amount);
        #$plot_owner=$result->plotowner;
        $cashier = $res['0']->ContactPersonName;
        $penalties = $res['0']->Penalty;
        $amount = $res['0']->Amount;
        if (is_numeric($amount)) {
            $amount_in_words = "** " . strtoupper($this->amount_to_words->convert_number($amount)) . " SHILLINGS ONLY**";
        } else $amount_in_words = "NOT AVAILABLE";

        $description = " BUSINESS ID - " . $res['0']->BusinessID;

        $pdf = Zend_Pdf::load($fileName);
        $page = $pdf->pages[0];

       

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
        $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 480, 528);
        $page->drawText($paidby, 250, 480);
        $page->drawText(number_format($amount, 2), 480, 480);
        $page->drawText($amount_in_words, 150, 437);
        $page->drawText($description, 150, 390);
        $trans_details=$this->getsubStreams($res['0']->BusinessID);
        $streams=$res['0']->LineItems;
        $health=0;
        $advert=0;
        $fire=0;
        $trade=0;
        $food=0;

        echo json_encode($streams);exit;
        if($year > 2016){
            foreach($streams as $key => $sub_streams){
               // var_dump($sub_streams);exit;
                if($sub_streams->Description=="Trade License") {
                    $trade=$sub_streams->Amount;
                } elseif($sub_streams->Description=="Fire Permit License") {
                    $fire=$sub_streams->Amount;//
                } elseif($sub_streams->Description=="Food Hygiene Certificate") {
                    $food+=$sub_streams->Amount;               
                } elseif($sub_streams->Description == "Health Certificate") {
                    $health+=$sub_streams->Amount;               
                } elseif($sub_streams->Description=="Advertisement") {
                     $advert=$sub_streams->Amount;//Advert Amount
                } elseif($sub_streams->Description=="Registration Fee") {
                    $penalties=$sub_streams->Amount;//Advert Amount
                    $fileName = APPPATH . 'assets/back/receipts/sbp_unified_receipt_reg.pdf';
                }
            }
        }

       

        if($year > 2016){
          $page->drawText(number_format($penalties, 2), 450, 278);//penalties
          $page->drawText(number_format($trade, 2), 450, 257);// Annual Amount         
          $page->drawText(number_format($advert, 2), 450, 235);//Advert Amount
          $page->drawText(number_format($fire, 2), 450, 213);//fire amount
          $page->drawText(number_format($food, 2), 450, 191);//Food Amount
          $page->drawText(number_format($health, 2), 450, 168);//Total Amount
          $page->drawText(number_format($amount, 2), 450, 146);//Amount Received
          $page->drawText(number_format($amount, 2), 450, 124);//Amount Due
          $page->drawText(number_format($balance_due, 2), 450, 101);//Amount Due

        } else {

            $page->drawText(number_format($penalties,2), 450, 102);
            $page->drawText(number_format($amount,2), 450, 250);
            $page->drawText(number_format($amount,2), 450, 225);
            $page->drawText(number_format($balance_due,2), 450, 205);
        }
        
        
       if($year > 2016) {
           /*$qrUrl = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.$res['0']->BusinessID;
                    $qrFileName = APPPATH.'assets/tmp/'.$receiptno.'.png';
                    copy($qrUrl, $qrFileName);
            $image = Zend_Pdf_Image::imageWithPath($qrFileName); 
            $page->drawImage($image, 150, 170, 250, 270);*/
       } else {
            $barcodeOptions = array('text' => $receiptno, 'barHeight' => 40, 'factor' => 2.5, 'font' => APPPATH . 'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 600,
                'leftOffset' => 113
            );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
       }
        
        


        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
        //redirect('en/land_rates');
    }

    function printnewsbpreceipt($businessid)
    {

        $token = $this->session->userdata('token');
        #$businessid = $this->uri->segment(3);
        $stream = "sbp";
        $key0 = "BusinessID";
        $value0 = $businessid;
        $key1 = "TransactionStatus";
        $value1 = "Completed";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        //$res = $res['0'];
        #var_dump($res->ReceiptNumber); die();

        $this->load->library('zend');
        $a = $this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');


        #$receiptno = str_replace('-', '/', $receiptno);
        //$fileName = APPPATH . '/assets/back/receipts/newsbp_receipt.pdf';
                $fileName = APPPATH . 'assets/back/receipts/sbp_unified_receipt_reg.pdf';
                $year=$res['0']->Year;
                $receiptno = $res['0']->ReceiptNumber;                
                $date = $res['0']->TransactionDate;
                $date = date('d-m-Y',strtotime(substr($date, 0, 10)));
                $paidby = $res['0']->ContactPersonName;
                $total_amount_due = $res['0']->Amount;
                $balance_due = "0";//($result->amountdue)-($result->amount);
                #$plot_owner=$result->plotowner;
                $cashier = $res['0']->ContactPersonName;
                //echo $cashier;exit;
                $penalties = $res['0']->Penalty;
                $amount = $res['0']->Amount;
                if (is_numeric($amount)) {
                    $amount_in_words = "** " . strtoupper($this->amount_to_words->convert_number($amount)) . " SHILLINGS ONLY**";
                } else $amount_in_words = "NOT AVAILABLE";

                $description = " BUSINESS ID - " . $res['0']->BusinessID;

                $pdf = Zend_Pdf::load($fileName);
                $page = $pdf->pages[0];

               

                $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
                $page->drawText($receiptno, 150, 528);
                $page->drawText($date, 480, 528);
                $page->drawText($paidby, 250, 480);
                $page->drawText(number_format($amount, 2), 480, 480);
                $page->drawText($amount_in_words, 150, 437);
                $page->drawText($description, 150, 390);
                //$trans_details=$this->getsubStreams($res['0']->BusinessID);
                $streams=$res['0']->LineItems;
                $health=0;
                $advert=0;
                $fire=0;
                $trade=0;
                if($year > 2016){
                    foreach($streams as $key => $sub_streams){
                       // var_dump($sub_streams);exit;
                        if($sub_streams->Description=="Trade License") {
                            $trade=$sub_streams->Amount;
                        } elseif($sub_streams->Description=="Fire Permit License") {
                            $fire=$sub_streams->Amount;//
                        } elseif($sub_streams->Description=="Food Hygiene Certificate") {
                            $food+=$sub_streams->Amount;               
                        } elseif($sub_streams->Description == "Health Certificate") {
                            $health+=$sub_streams->Amount;               
                        } elseif($sub_streams->Description=="Advertisement") {
                             $advert=$sub_streams->Amount;//Advert Amount
                        } elseif($sub_streams->Description=="Registration Fee") {
                             $penalties=$sub_streams->Amount;//Advert Amount
                        }
                    }
                }

                //echo $health.'  - '.$fire.' - '.$health.' - '.$trade;exit;

               

                if($year > 2016){
                 $page->drawText(number_format($penalties, 2), 450, 278);//penalties
                 $page->drawText(number_format($trade, 2), 450, 257);// Annual Amount         
                 $page->drawText(number_format($advert, 2), 450, 235);//Advert Amount
                 $page->drawText(number_format($fire, 2), 450, 213);//fire amount
                 $page->drawText(number_format($food, 2), 450, 191);//Food Amount
                 $page->drawText(number_format($health, 2), 450, 168);//Total Amount
                 $page->drawText(number_format($amount, 2), 450, 146);//Amount Received
                 $page->drawText(number_format($amount, 2), 450, 124);//Amount Due
                 $page->drawText(number_format($balance_due, 2), 450, 101);//Amount Due

                } else {

                    $page->drawText(number_format($penalties,2), 450, 270);
                    $page->drawText(number_format($amount,2), 450, 250);
                    $page->drawText(number_format($amount,2), 450, 225);
                    $page->drawText(number_format($balance_due,2), 450, 205);
                }
                
                
               if($year > 2016) {
                   /*$qrUrl = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.$res['0']->BusinessID;
                            $qrFileName = APPPATH.'assets/tmp/'.$receiptno.'.png';
                            copy($qrUrl, $qrFileName);
                    $image = Zend_Pdf_Image::imageWithPath($qrFileName); 
                    $page->drawImage($image, 150, 170, 250, 270);*/
               } else {
                    $barcodeOptions = array('text' => $receiptno, 'barHeight' => 40, 'factor' => 2.5, 'font' => APPPATH . 'assets/back/fonts/SWANSEBI.TTF');
                    $rendererOptions = array(
                        'topOffset' => 600,
                        'leftOffset' => 113
                    );
                    $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
               }
                
                


                $pdfData = $pdf->render();
                header("Content-Disposition: inline; filename=receipt.pdf");
                header("Content-type: application/x-pdf");
                echo $pdfData;
    }

     function print_sbp_preview($businessId)
      {
          // echo $businessId;exit;
          $token = $this->session->userdata('token');
          $Year=$this->uri->segment(4);
          $BusinessID = $this->uri->segment(3);
          
          $PhoneNumber = $this->session->userdata('jpwnumber');

          $url = "http://192.168.6.10/JamboPayServices/api/payments/GetBusinessDetails?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
          #curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Authorization: bearer ' . $token,
              'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
          ));

          $result = curl_exec($ch);
          //echo $result;exit;
          $this->traceAPI('API','GetBusinessDetails',$result);
          curl_close($ch);
          $res = json_decode($result);
          
          $biz_details=$this->session->userdata('biz_details');
          $sbpdata = array(
                  'approvalstatus' => $res->Status,
                  'sbpnumber' => $res->BusinessID,
                  'year' => $res->Year,
                  'bizname' => $res->BusinessName,
                  'pinno' => $res->Pin,
                  'biztype' => $res->Activity,
                  'activitycode' => $res->ActivityCode,
                  'paidfee' => $res->Amount,
                  'inwords' => $res->AmountInWords,
                  'pobox' => $res->POBox,
                  'plotnumber' => $res->PlotNumber,
                  'building' => $res->Building,
                  'floor' => $res->Floor,
                  'roomstallnumber' => $res->RoomStallNumber,
                  'street' => $res->PhysicalAddress,
                  'issuedate' => date('Y-m-d',strtotime($res->DateIssued)),
                  'bizactivityname' => $res->ActivityDescription,
                  'bizzid' => $res->BusinessID,
                  'receiptno' => $res->ReceiptNumber,
              );
              if($res->Year > 2016){
                    //$trans_details=$this->getsubStreams($res->BusinessID);
                   // $sbpdata['sub_streams']=$trans_details['sub_streams'];
              }
              //$this->traceAPI('RESCEIVED','sbp-data',json_encode($sbpdata));
              return $sbpdata;
      }

    /*function print_sbp_preview($businessId)
    {
        // echo $businessId;exit;
        $token = $this->session->userdata('token');
        $Year=$this->uri->segment(4);
        $BusinessID = $this->uri->segment(3);
        
        $PhoneNumber = $this->session->userdata('jpwnumber');

        $url = "http://192.168.6.10/JamboPayServices/api/payments/GetBusinessDetails?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        //var_dump($result);exit;
        curl_close($ch);
        $res = json_decode($result);
        
        
        $biz_details=$this->session->userdata('biz_details');
        $sbpdata = array(
                'approvalstatus' => '1',
                'sbpnumber' => $res->BusinessID,
                'year' => $res->Year,
                'bizname' => $res->BusinessName,
                'pinno' => $res->Pin,
                'biztype' => $res->Activity,
                'activitycode' => $res->ActivityCode,
                'paidfee' => $res->Amount,
                'inwords' => $res->AmountInWords,
                'pobox' => $res->POBox,
                'plotnumber' => $res->PlotNumber,
                'building' => $res->Building,
                'floor' => $res->Floor,
                'roomstallnumber' => $res->RoomStallNumber,
                'street' => $res->PhysicalAddress,
                'issuedate' => date('Y-m-d',strtotime($res->DateIssued)),
                'bizactivityname' => $res->ActivityDescription,
                'bizzid' => $res->BusinessID,
                'receiptno' => $res->ReceiptNumber,
            );
            if($res->Year > 2016){
                $trans_details=$this->getsubStreams($res->BusinessID);
                $sbpdata['sub_streams']=$trans_details['details'];
            }           
            
            return $sbpdata;
    }*/

    function getsubStreams($business_id)
    {
        $this->db->where('businessid',$business_id);
        return $this->db->get('business_transaction')->row_array();
    }


    function prepsaveinitial()
    {

        $url = MAIN_URL;
        $username = USER_NAME; //"97763838";
        #$key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
        #$username="97763838";
        $jp_key = JP_KEY; //'3637137f-9952-4eba-9e33-17a507a2bbb2';
        $timestamp = date("Y-m-d H:i:s");
        $ck = $username . $timestamp . $jp_key;
        $pass = sha1(utf8_encode($ck));

        $serviceArguments = array(
            "userName" => $username,
            "timestamp" => $timestamp,
            "pass" => $pass

        );
        try {
            $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }


        $result = $client->PrepareSaveBusinessInitDataSBPNCC($serviceArguments);
        $bizactivity = $result->PrepareSaveBusinessInitDataSBPNCCResult->BusinessActivity->{'BusinessPermit.BusinessActivity'};
        $relsize = $result->PrepareSaveBusinessInitDataSBPNCCResult->BusinessRelativeSize->{'BusinessPermit.BusinessRelativeSize'};
        $idtype = $result->PrepareSaveBusinessInitDataSBPNCCResult->IDTypesList->{'BusinessPermit.IDType'};
        $zones = $result->PrepareSaveBusinessInitDataSBPNCCResult->Zones->{'BusinessPermit.Zone'};
        $wards = $result->PrepareSaveBusinessInitDataSBPNCCResult->Wards->{'BusinessPermit.Ward'};

        $data = array(
            'bizactivity' => $bizactivity,
            'relsize' => $relsize,
            'idtype' => $idtype,
            'zones' => $zones,
            'wards' => $wards
        );

        return $data;
        //return $relsize;
    }

    function prepsaveinitialNew()
    {

        $token = $this->session->userdata('token');

        $url = "http://192.168.6.10/JamboPayServices/api/payments/getnewbusinessdetails?stream=sbp";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch); #echo "<pre>"; var_dump($res); die();

        if (isset($res->Message)) {
            return $data = array('rescode' => 55);
        } else {
            $bizactivity = $res->BusinessActivity;
            $relsize = $res->BusinessSizes;
            $idtype = $res->IDTypes;
            $zones = $res->Zones;
            $wards = $res->Wards;

            $data = array(
                'bizactivity' => $bizactivity,
                'relsize' => $relsize,
                'idtype' => $idtype,
                'zones' => $zones,
                'wards' => $wards,
                'rescode' => 0
            );

            // var_dump($data); die();
            return $data;
        }
        //return $relsize;
    }

    function step1()
    {

        $regdata = array(
            'biz_name' => $this->input->post('biz_name'),
            'doc_type' => $this->input->post('doc_type'),
            'doc_no' => $this->input->post('doc_no'),
            'pin_no' => $this->input->post('pin_no'),
            'vat_no' => $this->input->post('vat_no'),
            'box' => $this->input->post('box'),
            'postal_code' => $this->input->post('postal_code'),
            'town' => $this->input->post('town')
        );
        return $regdata;
    }

    function step2()
    {

        $regdata = array(
            'biz_name' => $this->input->post('biz_name'),
            'doc_type' => $this->input->post('doc_type'),
            'doc_no' => $this->input->post('doc_no'),
            'pin_no' => $this->input->post('pin_no'),
            'vat_no' => $this->input->post('vat_no'),
            'box' => $this->input->post('box'),
            'postal_code' => $this->input->post('postal_code'),
            'town' => $this->input->post('town'),
            'tel1' => $this->input->post('tel1'),
            'tel2' => $this->input->post('tel2'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'plot_number' => $this->input->post('plot_number'),
            'lr_number' => $this->input->post('lr_number'),
            'building' => $this->input->post('building'),
            'floor' => $this->input->post('floor'),
            'stall_room_no' => $this->input->post('stall_room_no')
        );
        return $regdata;
    }

    function step3()
    {

        $regdata = array(
            'biz_name' => $this->input->post('biz_name'),
            'doc_type' => $this->input->post('doc_type'),
            'doc_no' => $this->input->post('doc_no'),
            'pin_no' => $this->input->post('pin_no'),
            'vat_no' => $this->input->post('vat_no'),
            'box' => $this->input->post('box'),
            'postal_code' => $this->input->post('postal_code'),
            'town' => $this->input->post('town'),

            'tel1' => $this->input->post('tel1'),
            'tel2' => $this->input->post('tel2'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'plot_number' => $this->input->post('plot_number'),
            'lr_number' => $this->input->post('lr_number'),
            'building' => $this->input->post('building'),
            'floor' => $this->input->post('floor'),
            'stall_room_no' => $this->input->post('stall_room_no'),

            'full_names' => $this->input->post('full_names'),
            'owner_box' => $this->input->post('owner_box'),
            'owner_postal_code' => $this->input->post('owner_postal_code'),
            'owner_telephone' => $this->input->post('owner_telephone'),
            'owner_telephone2' => $this->input->post('owner_telephone2'),
            'owner_fax' => $this->input->post('owner_fax')
        );
        return $regdata;
    }

    function step4()
    {
        $regdata = array(
            'biz_name' => $this->input->post('biz_name'),
            'doc_type' => $this->input->post('doc_type'),
            'doc_no' => $this->input->post('doc_no'),
            'pin_no' => $this->input->post('pin_no'),
            'vat_no' => $this->input->post('vat_no'),
            'box' => $this->input->post('box'),
            'postal_code' => $this->input->post('postal_code'),
            'town' => $this->input->post('town'),

            'tel1' => $this->input->post('tel1'),
            'tel2' => $this->input->post('tel2'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'plot_number' => $this->input->post('plot_number'),
            'lr_number' => $this->input->post('lr_number'),
            'building' => $this->input->post('building'),
            'floor' => $this->input->post('floor'),
            'stall_room_no' => $this->input->post('stall_room_no'),

            'full_names' => $this->input->post('full_names'),
            'owner_box' => $this->input->post('owner_box'),
            'owner_postal_code' => $this->input->post('owner_postal_code'),
            'owner_telephone' => $this->input->post('owner_telephone'),
            'owner_telephone2' => $this->input->post('owner_telephone2'),
            'owner_fax' => $this->input->post('owner_fax'),

            'activity_dec' => $this->input->post('activity_dec'),
            'area' => $this->input->post('area'),
            'other_details' => $this->input->post('other_details'),
            'activity_code' => $this->input->post('activity_code'),
            'sbp_fee' => $this->input->post('sbp_fee'),
            'emp_no' => $this->input->post('emp_no'),
            'size' => $this->input->post('size'),
            'zone' => $this->input->post('zones'),
            'ward' => $this->input->post('ward'),
            'advert_fee' => 4200,
        );
        return $regdata;
    }

    function step5()
    {

        $regdata = array(
            'biz_name' => $this->input->post('biz_name'),
            'doc_type' => $this->input->post('doc_type'),
            'doc_no' => $this->input->post('doc_no'),
            'pin_no' => $this->input->post('pin_no'),
            'vat_no' => $this->input->post('vat_no'),
            'box' => $this->input->post('box'),
            'postal_code' => $this->input->post('postal_code'),
            'town' => $this->input->post('town'),

            'tel1' => $this->input->post('tel1'),
            'tel2' => $this->input->post('tel2'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'plot_number' => $this->input->post('plot_number'),
            'building' => $this->input->post('building'),
            'floor' => $this->input->post('floor'),
            'stall_room_no' => $this->input->post('stall_room_no'),

            'full_names' => $this->input->post('full_names'),
            'owner_box' => $this->input->post('owner_box'),
            'owner_postal_code' => $this->input->post('owner_postal_code'),
            'owner_telephone' => $this->input->post('owner_telephone'),
            'owner_telephone2' => $this->input->post('owner_telephone2'),
            'owner_fax' => $this->input->post('owner_fax'),

            'activity_dec' => $this->input->post('activity_dec'),
            'area' => $this->input->post('area'),
            'other_details' => $this->input->post('other_details'),
            'activity_code' => $this->input->post('activity_code'),
            'sbp_fee' => $this->input->post('sbp_fee'),
            'advert_fee' => $this->input->post('advert_fee'),
            'fire_fee' => $this->input->post('fire_fee'),
            'emp_no' => $this->input->post('emp_no'),
            'size' => $this->input->post('size'),
            'zone' => $this->input->post('zones'),
            'ward' => $this->input->post('ward'),
            'jppin' => $this->input->post('pin')
        );
        return $regdata;
    }

    function reg_prepare()
    {
        $url = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $jp_key = JP_KEY; //'3637137f-9952-4eba-9e33-17a507a2bbb2';
        $activityCode = $this->input->post('activity_code');
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $custPhone = $this->session->userdata['jpwnumber'];
        $timestamp = date("Y-m-d H:i:s");
        $ck = $username . $custPhone . $agentRef . $activityCode . $jp_key;
        $pass = sha1(utf8_encode($ck));

        $serviceArguments = array(
            "userName" => $username,
            "agentRef" => $agentRef,
            "activityCode" => $activityCode,
            "jpwMobileNo" => $custPhone,
            "pass" => $pass

        );
        try {
            $client = new SoapClient($url);
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->PrepareSaveBusinessWalletSBPNCC($serviceArguments);
        $resulttext = $result->PrepareSaveBusinessWalletSBPNCCResult->Result->ResultText;
        $resultcode = $result->PrepareSaveBusinessWalletSBPNCCResult->Result->ResultCode;
        $tran_id = $result->PrepareSaveBusinessWalletSBPNCCResult->TransactionID;
        return $data1 = array('resulttext' => $resulttext, 'tranidd' => $tran_id);
        #var_dump($resulttext);
    }

    function prepareRegistration()
    {
        $token = $this->session->userdata['token'];
        $number = $this->session->userdata['jpwnumber'];//"254".$num;
        $amount = $this->input->post('sbp_fee');
        $BusinessName = $this->input->post('biz_name');
        $IDDocumentType = $this->input->post('doc_type');
        $IDDocumentNumber = $this->input->post('doc_no');
        $PINNumber = $this->input->post('pin_no');
        $VATNumber = $this->input->post('vat_no');
        $SBPSubClassID=$this->input->post('sub_classes');
        $POBox = $this->input->post('box');
        $PostalCode = $this->input->post('postal_code');
        $AnnualSBPAmount = $this->input->post('sbp_fee') + $this->input->post('advert_fee');//'5000';
        $Town = $this->input->post('town');
        $Telephone1 = "254" . substr($this->input->post('tel1'), -9);
        $Telephone2 = "254" . substr($this->input->post('tel2'), -9);
        $FaxNumber = $this->input->post('fax');
        $email = $this->input->post('email');
        $PhysicalAddress = $this->input->post('address');
        $PlotNumber = $this->input->post('plot_number');
        $Building = $this->input->post('building');
        $BuildingCategory = ($this->input->post('floor')) ? 'Storey' : 'Non-Storey';
        $Floor = ($this->input->post('floor')) ? $this->input->post('floor') : 'n/a';
        $RoomStallNo = $this->input->post('stall_room_no');
        $ContactPersonName = $this->input->post('full_names');
        $ContactPersonPOBox = $this->input->post('owner_box');
        $ContactPersonPostalCode = $this->input->post('owner_postal_code');
        $ContactPersonTelephone1 = $this->input->post('owner_telephone');
        $ContactPersonTelephone2 = $this->input->post('owner_telephone2');
        $ContactPersonFaxNumber = $this->input->post('owner_fax');
        $BusinessClassificationDetails = $this->input->post('activity_dec');
        $PremisesArea = $this->input->post('area');
        $OtherBusinessClassificationDetails = $this->input->post('other_details');
        $NumberOfEmployees = $this->input->post('emp_no');
        $ActivityCode = $this->input->post('activity_code');//'205';//
        $ActivityName = $this->input->post('activity_dec');//'205';//
        $ZoneCode = $this->input->post('zones');
        $WardCode = $this->input->post('ward');
        $RelativeSize = $this->input->post('size');
        $pin = $this->session->userdata('jp_pin');//'1234';
        $Names = $this->input->post('full_names');
        $ContactPersonTown = '';
        $BusinessRegistrationNumber = '';
        $ContactPersonDesignation = '';
        $BusinessID = '';
        $Exception=$this->input->post('exemptions');

        // var_dump($this->session->all_userdata());exit;

        $tosend = array(
            "BusinessID" => $BusinessID,
            "Year" => '',
            "TransactionID" => '',
            "Names" => $Names,
            "BillNumber" => '',
            "BillStatus" => '',
            "Stream" => "sbp",
            "Stream" => "sbp",
            "PhoneNumber" => $number,
            "PhonePin" => $pin,
            "IDNumber" => $IDDocumentType,
            "ActivityCode" => $ActivityCode,
            "ActivityName" => $ActivityName,
            "SBPSubClassID"=> $SBPSubClassID,
            "RelativeSize" => $RelativeSize,
            "WardName" => '',
            "ZoneName" => '',
            "AcceptedTnC" => "1",
            "WardCode" => "4",
            "ZoneCode" => "136",
            "BusinessName" => $BusinessName,
            "BusinessRegistrationNumber" => $BusinessRegistrationNumber,
            "Pin" => $PINNumber,
            "VAT" => $VATNumber,
            "IDDocumentNumber" => $IDDocumentNumber,
            "BusinessClassificationDetails" => $BusinessClassificationDetails,
            "OtherBusinessClassificationDetails" => $OtherBusinessClassificationDetails,
            "PremisesArea" => $PremisesArea,
            "NumberOfEmployees" => $NumberOfEmployees,
            "AnnualSBPAmount" => $AnnualSBPAmount,
            "AnnualSBPAmount" => $AnnualSBPAmount,
            "POBox" => $POBox,
            "PostalCode" => $PostalCode,
            "Town" => $Town,
            "Telephone1" => $Telephone1,
            "Telephone2" => $Telephone2,
            "FaxNumber" => $FaxNumber,
            "Email" => $email,
            "PhysicalAddress" => $PhysicalAddress,
            "PlotNumber" => $PlotNumber,
            "Building" => $Building,
            "BuildingCategory" => $BuildingCategory,
            "Floor" => $Floor,
            "RoomStallNumber" => $RoomStallNo,
            "ContactPersonName" => $ContactPersonName,
            "ContactPersonDesignation" => $ContactPersonDesignation,
            "ContactPersonPOBox" => $ContactPersonPOBox,
            "ContactPersonPostalCode" => $ContactPersonPostalCode,
            "ContactPersonTown" => $ContactPersonTown,
            "ContactPersonTelephone1" => $ContactPersonTelephone1,
            "ContactPersonTelephone2" => $ContactPersonTelephone2,
            "ContactPersonFaxNumber" => $ContactPersonFaxNumber,
            "PaidBy" => $this->session->userdata('name'),
            "ApprovalStatus" => "0",
            "PaymentTypeID" => "1",
            "SBPPaymentType" => "1",
            "ReceiptNumber" => "",
            "LocalReceiptNumber" => "",
            "ReferenceNumber" => "",
            "BankName" => "",
            "BranchName" => "Payee",
            "BranchName" => "",
            
        );

       // echo json_encode($tosend);exit;

        $business_details=$this->session->set_userdata('bid_prepared_details',$tosend);


        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);
        if(count($Exception)) {
            $exclusions="";
            foreach ($Exception as $key => $value) {
              $exclusions.="&Exclusions[".$key."].ID=".$value;
            }
        }
        //$post_string.=$exclusions;

        //echo $post_string;exit;
        //echo $this->testExclusions();exit;       


        $url = "http://192.168.6.10/JamboPayServices/api/payments/post";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        //echo $result;exit;
        //var_dump($result);exit;

        if (!isset($res->ErrorCode)) {
            $trans_id = $res->TransactionID;
            $data = array(
                'tranidd' => $trans_id,
                'rescode' => 0,
                'details' => json_decode($result,true),
            );

            return $data;
        } else {
            return $data = array('rescode' => $res->ErrorCode, 'restext' => $res->Message);
        }
    }

    function testExclusions()
    {
        $token = $this->session->userdata['token'];
        $number = $this->session->userdata['jpwnumber'];//"254".$num;
        $curl = curl_init();
         
        curl_setopt_array($curl, array(
          //CURLOPT_PORT => "60799",
          CURLOPT_URL => "http://192.168.6.10/JamboPayServices/api/payments/post",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "Stream=sbp&PhoneNumber=0733235400&SBPPaymentType=1&IDNumber=2&RelativeSize=1&RelativeSize=10&WardCode=25&ZoneCode=137&SBPSubClassID=41&BusinessName=My%20Business%202017-01&Pin=28359235&VAT=283529392&IDDocumentNumber=82395293582935&BusinessClassificationDetails=My%20Details%202017-01&OtherClassificationDetails=Others%202017-01&PremisesArea=10x10&NumberOfEmployees=4&POBox=232834&PostalCode=00100&Town=Nairobi&Telephone1=0733235400&Telephone2=0733235400&FaxNumber=0733235400&Email=charles.gathu2017%40jambopay.com&PhysicalAddress=This%20Address%202017-01&PlotNumber=BLOCK%20123%2F123&Building=My%20Building%202017-01&Floor=2&RoomStallNumber=45&ContactPersonName=Charles&ContactPersonDesignation=CEO&ContactPersonPOBox=823942&ContactPersonPostalCode=00100&ContactPersonTown=Nairobi&ContactPersonTelephone1=0733235400&ContactPersonTelephone2=0733235400&ContactPersonFaxNumber=0733235400&Exclusions%5B0%5D.ID=10&Exclusions%5B1%5D.ID=11&Exclusions%5B2%5D.ID=12",
          CURLOPT_HTTPHEADER => array(
            "app_key: A2868C59-67E2-E411-8285-B8EE657EAEBC",
            "authorization: bearer ".$token,
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 9dcb157c-c9e1-9364-9397-7eb923dd6ea9"
          ),
        ));
         
        $response = curl_exec($curl);
        $err = curl_error($curl);
         
        curl_close($curl);
         
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }

    function getFireBusinessDetailsBuilding()
    {
        $building = $this->input->post('building');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('full_names');
        $year = date("Y");
        $Tel_no = $this->session->userdata['jpwnumber'] + mt_rand(5, 15);
        $this->session->set_userdata('fire_phone', $Tel_no);
        $category = $this->input->post('activity_code');
        $PlotNo = $this->input->post('plot_number');
        $LRNO = $this->input->post('lr_number');
        $physicaladdress = $this->input->post('address');
        $ward = $this->input->post('ward');
        $zone = $this->input->post('zones');


        $url = 'http://52.24.24.25/mainsector.asmx?wsdl'; //"http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID = "bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $serviceArgs = array(
            "API_Id" => $apiID,
            "Fullname" => $fullnames . mt_rand(1000, 2500),
            "Email" => 'salmon' . mt_rand(10, 25) . '@gmail.com',
            "Tel_no" => $this->session->userdata('fire_phone'),
            "BusinessName" => $building . mt_rand(1000, 2500),//$Firm,
            "Owner" => $fullnames . mt_rand(1000, 2500),//$Occupation,
            "PlotNo" => mt_rand(1000, 2500),
            "LRNO" => mt_rand(1000, 2500),
            "Form_Id" => "FR",
            "Building" => $physicaladdress . mt_rand(1000, 2500),
            "category_id" => $category,
            "subcounty" => $ward,
            "zone" => $zone,
        );


        try {
            $client = new SoapClient($url);

            $result = $client->PostfireApplication($serviceArgs);
            //var_dump($result);exit;
            #var_dump($result); die();
            $RefID = $result->PostfireApplicationResult->string["0"];
            $InvoiceNo = $result->PostfireApplicationResult->string["3"];
        } catch (Exception $e) {
            redirect('health/APIerror');
        }

        if ($RefID != "400") {

            $serviceArguments2 = array(
                "API_Id" => $apiID,
                "RefId" => $RefID
                #"category"=>$category
            );
            $result2 = $client->GetfireinvoiceDetails($serviceArguments2);

            $Fullname = $result2->GetfireinvoiceDetailsResult->Name;
            $Email = $result2->GetfireinvoiceDetailsResult->Email;
            $Firm = $result2->GetfireinvoiceDetailsResult->BusinessName;
            $InvoiceNo = $result2->GetfireinvoiceDetailsResult->InvoiceNum;
            $Tel_no = $result2->GetfireinvoiceDetailsResult->Tel_no;
            $Owner = $result2->GetfireinvoiceDetailsResult->Owner;
            $Building = $result2->GetfireinvoiceDetailsResult->Building;
            $Category = $result2->GetfireinvoiceDetailsResult->category;
            $Status = $result2->GetfireinvoiceDetailsResult->status;
            $RefID = $result2->GetfireinvoiceDetailsResult->RefId;
            $Amount = $result2->GetfireinvoiceDetailsResult->amount;

            $data = array(
                "RefID" => $RefID,
                "issueDate" => date("Y/m/d"),
                "Fullname" => $Fullname,
                "Email" => $Email,
                "TelNumber" => $Tel_no,
                "Firm" => $Firm,
                "InvoiceNo" => $InvoiceNo,
                "Status" => $Status,
                "Owner" => $Owner,
                "Amount" => $Amount,
                "Building" => $Building,
                "Category" => $category
            );

            try {
                $this->db->insert('fireinspection', $data);
            } catch (Exception $e) {

            }
            #var_dump($data); die();

            return $data;
        } elseif ($RefID == "400") {
            $data = array(
                "fire_amount" => 5000
            );

            return $data;
        }
    }

    function prepareFirePayment()
    {

        $token = $this->session->userdata('token');
        $refid = $this->input->post('refid');
        //$refid = str_replace(" ", "", $refid);

        $tosend = array(
            'Stream' => "fireinvoice",
            'InvoiceReferenceNumber' => $refid,
            'Year' => date('Y'),//"2014",
            'PhoneNumber' => $this->session->userdata('jpwnumber') + mt_rand(5, 15),
            'PaidBy' => $this->session->userdata('name'),
            'PaymentTypeID' => "1"
        );

        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);

        $res = json_decode($result);
        @$rescode = $res->ResponseCode;
        //echo "<pre>";var_dump($res); die();
        if (isset($rescode)) {
            return $data = array(
                "BusinessName" => $res->BusinessName1,
                "ContactPerson" => $res->Names,
                "Email" => $res->Email,
                "Telephone" => $res->TelephoneNumber,
                "Refid" => $res->InvoiceReferenceNumber,
                "TransactionID" => $res->TransactionID,
                "InvoiceNo" => $res->InvoiceNumber,
                "Amount" => $res->Amount,
                "rescode" => $res->ResponseCode,
                "message" => "ok"
            );
        } elseif (!isset($rescode)) {
            return $data = array(
                "rescode" => $res->ErrorCode,
                "message" => $res->Message,
            );
        }

    }

    function completeRegistration()
    {
        $token = $this->session->userdata('token');
        $TransactionID = $this->input->post('trans_id');
        $jpPin = $this->input->post('pin');
        $jpwMobileNo = $this->session->userdata('jpwnumber');

        $tosend = array(
            'Stream' => "sbp",
            'TransactionID' => $TransactionID,
            'PhonePin' => $jpPin,
            'PhoneNumber' => $jpwMobileNo,
            'SBPPaymentType' => '1',
            'PaidBy' => $this->session->userdata('name')
        );

        //var_dump($tosend);exit;

        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments/put";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result,true);

        //echo $result;exit;
        //var_dump($res);exit;


        if (!isset($res['ErrorCode'])) {        

            try {
                $tel = $this->session->userdata('jpwnumber');
                // $amnt= number_format($amount, 2, '.', ',');
                $msg = "You have registered " . strtoupper($res['BusinessName']) . " successfully.Business ID:".$res['BusinessID']." Please await verification to get a fully approved Business Permit.Powered by JamboPay";
                $key = "63bc8b3e-e674-4b08-879c-02e1aceedb8f";

                $APIKey = urlencode($key);
                $Phone = urlencode($tel);
                $relayCode = urlencode("WebTribe");
                $Message = urlencode($msg);
                $Shortcode = urlencode("700273");
                $CampaignId = urlencode("112623");
                $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://192.168.7.61/smsserver/SendSMS.aspx' . $qstr);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);

            } catch (Exception $e) {

            }

            return $res;
        } else {
            return $data = array('rescode' => $res['ErrorCode'], 'restext' => $res['Message']);
        }

    }

    function register_sbpNew()
    {

        $token = $this->session->userdata['token'];
        $businessId = $this->input->post('biz_id');
        $year = $this->input->post('year');
        $number = $this->session->userdata['jpwnumber'];//"254".$num;

        $custPhone = $this->session->userdata('jpwnumber');
        $tran_id = $this->input->post('trans_id');
        $transactionId = $tran_id;

        $amount = number_format($this->input->post('sbp_fee') + 200, 2, '.', '');
        $currency = "KES";
        $channelRef = strtoupper(substr(md5(uniqid()), 25));
        $BusinessName = $this->input->post('biz_name');
        $IDDocumentType = $this->input->post('doc_type');
        $IDDocumentNumber = $this->input->post('doc_no');
        $PINNumber = $this->input->post('pin_no');
        $VATNumber = $this->input->post('vat_no');
        $POBox = $this->input->post('box');
        $PostalCode = $this->input->post('postal_code');
        $AnnualSBPAmount = $this->input->post('sbp_fee');//'5000';
        $Town = $this->input->post('town');
        $Telephone1 = "254" . substr($this->input->post('tel1'), -9);
        $Telephone2 = "254" . substr($this->input->post('tel2'), -9);
        $FaxNumber = $this->input->post('fax');
        $email = $this->input->post('email');
        $PhysicalAddress = $this->input->post('address');
        $PlotNumber = $this->input->post('plot_number');
        $Building = $this->input->post('building');
        $Floor = $this->input->post('floor');
        $RoomStallNo = $this->input->post('stall_room_no');
        $ContactPersonName = $this->input->post('full_names');
        $ContactPersonPOBox = $this->input->post('owner_box');
        $ContactPersonPostalCode = $this->input->post('owner_postal_code');
        $ContactPersonTelephone1 = $this->input->post('owner_telephone');
        $ContactPersonTelephone2 = $this->input->post('owner_telephone2');
        $ContactPersonFaxNumber = $this->input->post('owner_fax');
        $BusinessClassificationDetails = $this->input->post('activity_dec');
        $PremisesArea = $this->input->post('area');
        $OtherBusinessClassificationDetails = $this->input->post('other_details');
        $NumberOfEmployees = $this->input->post('emp_no');
        $ActivityCode = $this->input->post('activity_code');//'205';//
        $ZoneCode = $this->input->post('zone');
        $WardCode = $this->input->post('ward');
        $RelativeSize = $this->input->post('size');
        $pin = $this->input->post('pin');//'1234';
        $ContactPersonTown = '';
        $BusinessRegistrationNumber = '';
        $ContactPersonDesignation = '';


        $tosend = array(
            "Stream" => "sbp",
            "TransactionID" => $transactionId,
            "PaymentTypeID" => "1",
            "PhonePin" => $pin,
            "PhoneNumber" => $custPhone,
            "SBPPaymentType" => "1",
            "BusinessName" => $BusinessName,
            "BusinessRegistrationNumber" => $BusinessRegistrationNumber,
            "Pin" => $PINNumber,
            "VAT" => $VATNumber,
            "IDNumber" => $IDDocumentType,
            "IDDocumentNumber" => $IDDocumentNumber,
            "ZoneCode" => $ZoneCode,
            "WardCode" => $WardCode,
            "ActivityCode" => $ActivityCode,
            "BusinessClassificationDetails" => $BusinessClassificationDetails,
            "OtherBusinessClassificationDetails" => $OtherBusinessClassificationDetails,
            "PremisesArea" => $PremisesArea,
            "RelativeSize" => $RelativeSize,
            "NumberOfEmployees" => $NumberOfEmployees,
            "AnnualSBPAmount" => $AnnualSBPAmount,
            "POBox" => $POBox,
            "PostalCode" => $PostalCode,
            "Town" => $Town,
            "Telephone1" => $Telephone1,
            "Telephone2" => $Telephone2,
            "FaxNumber" => $FaxNumber,
            "Email" => $email,
            "PhysicalAddress" => $PhysicalAddress,
            "PlotNumber" => $PlotNumber,
            "Building" => $Building,
            "Floor" => $Floor,
            "RoomStallNumber" => $RoomStallNo,
            "ContactPersonName" => $ContactPersonName,
            "ContactPersonDesignation" => $ContactPersonDesignation,
            "ContactPersonPOBox" => $ContactPersonPOBox,
            "ContactPersonPostalCode" => $ContactPersonPostalCode,
            "ContactPersonTown" => $ContactPersonTown,
            "ContactPersonTelephone1" => $ContactPersonTelephone1,
            "ContactPersonTelephone2" => $ContactPersonTelephone2,
            "ContactPersonFaxNumber" => $ContactPersonFaxNumber

        );

        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments/put";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);

        var_dump($transactionId);
        var_dump($res);
        die();

        $result = $client->CompleteSaveBusinessWalletSBPNCC($serviceArguments);
        $resulttext = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultText;
        $resultcodecomplete = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultCode;
        $dataarray['rescomplete'] = $resultcodecomplete;
        #var_dump($result); die();
        if ($resultcodecomplete == 0 && $resulttext == "OK") {
            $BusinessId = $result->CompleteSaveBusinessWalletSBPNCCResult->BusinessID;
            $Commission = $result->CompleteSaveBusinessWalletSBPNCCResult->Commission;
            $ReceiptNo = $result->CompleteSaveBusinessWalletSBPNCCResult->ReceiptNo;
            $Paid = $result->CompleteSaveBusinessWalletSBPNCCResult->Paid;


            $businessId = $BusinessId;
            $agentRef = strtoupper(substr(md5(uniqid()), 25));
            $ck = $username . $custPhone . $agentRef . $businessId . $jp_key;
            $pass = sha1(utf8_encode($ck));

            $serviceArguments = array(
                "userName" => $username,
                "agentRef" => $agentRef,
                "businessId" => $businessId,
                "calendarYear" => "2016",
                "jpwMobileNo" => $custPhone,
                "pass" => $pass

            );

            try {
                $client = new SoapClient($url);
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }

            $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
            $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
            $resultcode = $result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
            if ($resultcode == 0 && $resulttext == "OK") {
                $trans_id2 = $result->PreparePaymentWalletSBPNCCResult->TransactionID;
                $biz_id = $result->PreparePaymentWalletSBPNCCResult->BusinessId;
                $bill_no = $result->PreparePaymentWalletSBPNCCResult->BillNumber;
                $biz_name = $result->PreparePaymentWalletSBPNCCResult->BusinessName;
                $physical_add = $result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
                $bill_status = $result->PreparePaymentWalletSBPNCCResult->BillStatus;
                $sbp_fee = $result->PreparePaymentWalletSBPNCCResult->SBPFee;
                $annualamount = $result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
                $penalty = $result->PreparePaymentWalletSBPNCCResult->Penalty;

                $data = array(
                    'biz_id' => $biz_id,
                    'bill_no' => $bill_no,
                    'trans_id' => $trans_id2,
                    'commission' => $Commission,
                    'receiptno' => $ReceiptNo,
                    'biz_name' => $biz_name,
                    'physical_address' => $physical_add,
                    'bill_stat' => $bill_status,
                    'sbpfee' => number_format($sbp_fee, 2, '.', ''),
                    //'result'=>$result,
                    'annualamount' => number_format($annualamount, 2),
                    'penalty' => $penalty,
                    'rescode' => $resultcode,
                    'restext' => $resulttext,
                    'message' => ''
                );

                try {
                    // $db=array('receiptno' =>$ReceiptNo,'issuedate' =>DATE('Y-m-d h:i:s',time()),'from'=>$this->session->userdata('name'),'amount'=>$Paid,'businessid'=>$BusinessId,'businessowner'=>$ContactPersonName,'businessname'=>$BusinessName,'username'=>$this->session->userdata('jpwnumber'),'cashiername'=>$this->session->userdata('name'));

                    //   #$query = "insert into sbp(`receiptno`,`issuedate`,`paidby`,`amount`,`businessid`,`penalties`,`annualamount`,`amountdue`,`username`,`cashiername`,`channel`)";
                    //     #$query.= " values ('".implode("','",$data)."')";
                    // $this->db->insert('newsbp',$db);
                } catch (Exception $e) {

                }

                try {

                    /*for sending confirmation sms*/
                    $tel = $this->session->userdata('jpwnumber');
                    // $amnt= number_format($amount, 2, '.', ',');
                    $msg = "You have registered " . strtoupper($biz_name) . " successfully.Business ID: $biz_id. Please await verification to get a fully approved Business Permit.Powered by JamboPay";
                    $key = "63bc8b3e-e674-4b08-879c-02e1aceedb8f";

                    $APIKey = urlencode($key);
                    $Phone = urlencode($tel);
                    $relayCode = urlencode("WebTribe");
                    $Message = urlencode($msg);
                    $Shortcode = urlencode("700273");
                    $CampaignId = urlencode("112623");
                    $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'http://192.168.7.61/smsserver/SendSMS.aspx' . $qstr);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($ch);
                    curl_close($ch);

                } catch (Exception $e) {

                }


                return $data;
                #}
            } else {
                return $data = array('rescode' => $resultcode, 'restext' => $resulttext);
            }
            //return $result;
        } else return $data = array('rescode' => $resultcodecomplete, 'restext' => $resulttext);

    }

    // function register_sbp(){

    //   $url = MAIN_URL;
    //   $username= USER_NAME;// "97763838";
    //   $jp_key= JP_KEY;// '3637137f-9952-4eba-9e33-17a507a2bbb2';
    //   $custPhone = $this->session->userdata('jpwnumber');// '254712633277';
    //   $tran_id = $this->input->post('trans_id');
    //   $ck = $username .$tran_id . $jp_key;
    //   $pass = sha1(utf8_encode($ck ));
    //   $transactionId=$tran_id;
    //   #$regfee='200';
    //   #$amnt=$this->input->post('sbp_fee');$regfee+$this->input->post('sbp_fee');//
    //   $amount=number_format($this->input->post('sbp_fee')+200,2,'.','');
    //   $currency="KES";
    //   $channelRef=strtoupper(substr(md5(uniqid()),25));
    //   $BusinessName=$this->input->post('biz_name');
    //   $IDDocumentType=$this->input->post('doc_type');
    //   $IDDocumentNumber=$this->input->post('doc_no');
    //   $PINNumber=$this->input->post('pin_no');
    //   $VATNumber=$this->input->post('vat_no');
    //   $POBox=$this->input->post('box');
    //   $PostalCode=$this->input->post('postal_code');
    //     $AnnualSBPAmount=$this->input->post('sbp_fee');//'5000';
    //     $Town=$this->input->post('town');
    //     $Telephone1="254".substr($this->input->post('tel1'),-9);
    //     $Telephone2="254".substr($this->input->post('tel2'),-9);
    //     $FaxNumber=$this->input->post('fax');
    //     $email=$this->input->post('email');
    //     $PhysicalAddress=$this->input->post('address');
    //     $PlotNumber=$this->input->post('plot_number');
    //     $Building=$this->input->post('building');
    //     $Floor=$this->input->post('floor');
    //     $RoomStallNo=$this->input->post('stall_room_no');
    //     $ContactPersonName=$this->input->post('full_names');
    //     $ContactPersonPOBox=$this->input->post('owner_box');
    //     $ContactPersonPostalCode=$this->input->post('owner_postal_code');
    //     $ContactPersonTelephone1=$this->input->post('owner_telephone');
    //     $ContactPersonTelephone2=$this->input->post('owner_telephone2');
    //     $ContactPersonFaxNumber=$this->input->post('owner_fax');
    //     $BusinessClassificationDetails=$this->input->post('activity_dec');
    //     $PremisesArea=number_format($this->input->post('area'),2);
    //     $OtherBusinessClassificationDetails=$this->input->post('other_details');
    //     $NumberOfEmployees=$this->input->post('emp_no');
    //     $ActivityCode=$this->input->post('activity_code');//'205';//
    //     $ZoneCode=$this->input->post('zone');
    //     $WardCode=$this->input->post('ward');
    //     $RelativeSize=$this->input->post('size');
    //     $pin=$this->input->post('pin');//'1234';
    //     $ContactPersonTown='';
    //     $BusinessRegistrationNumber='';
    //     $ContactPersonDesignation='';


    //     $serviceArguments = array(
    //       "userName"=>$username,
    //       "transactionId"=>$transactionId,
    //       "amount"=> $amount,
    //       "jpPIN"=>$pin,
    //       "currency"=>$currency,
    //       "channelRef"=>$channelRef,
    //       "pass"=> $pass,
    //       "BusinessName"=>$BusinessName,
    //       "BusinessRegistrationNumber"=>$BusinessRegistrationNumber,
    //       "PINNumber"=> $PINNumber,
    //       "VATNumber"=>$VATNumber,
    //       "IDDocumentType"=>$IDDocumentType,
    //       "IDDocumentNumber"=> $IDDocumentNumber,
    //       "ZoneCode"=>$ZoneCode,
    //       "WardCode"=>$WardCode,
    //       "ActivityCode"=> $ActivityCode,
    //       "BusinessClassificationDetails"=>$BusinessClassificationDetails,
    //       "OtherBusinessClassificationDetails"=>$OtherBusinessClassificationDetails,
    //       "PremisesArea"=> $PremisesArea,
    //       "RelativeSize"=>$RelativeSize,
    //       "NumberOfEmployees"=>$NumberOfEmployees,
    //       "AnnualSBPAmount"=> $AnnualSBPAmount,
    //       "POBox"=>$POBox,
    //       "PostalCode"=>$PostalCode,
    //       "Town"=> $Town,
    //       "Telephone1"=>$Telephone1,
    //       "Telephone2"=>$Telephone2,
    //       "FaxNumber"=> $FaxNumber,
    //       "email"=>$email,
    //       "PhysicalAddress"=>$PhysicalAddress,
    //       "PlotNumber"=> $PlotNumber,
    //       "Building"=>$Building,
    //       "Floor"=>$Floor,
    //       "RoomStallNo"=> $RoomStallNo,
    //       "ContactPersonName"=>$ContactPersonName,
    //       "ContactPersonDesignation"=>$ContactPersonDesignation,
    //       "ContactPersonPOBox"=> $ContactPersonPOBox,
    //       "ContactPersonPostalCode"=>$ContactPersonPostalCode,
    //       "ContactPersonTown"=>$ContactPersonTown,
    //       "ContactPersonTelephone1"=> $ContactPersonTelephone1,
    //       "ContactPersonTelephone2"=>$ContactPersonTelephone2,
    //       "ContactPersonFaxNumber"=>$ContactPersonFaxNumber

    //       );

    // #var_dump($serviceArguments);die();
    // try {
    //   $client = new SoapClient($url);
    // } catch (Exception $e) {
    //   redirect('/sbp/not_found');
    // }

    // $result = $client->CompleteSaveBusinessWalletSBPNCC($serviceArguments);
    // $resulttext = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultText;
    // $resultcodecomplete = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultCode;
    // #$dataarray['rescomplete'] = $resultcodecomplete;
    //         #var_dump($result); die();
    // if($resultcodecomplete==0&&$resulttext=="OK"){
    //   $BusinessId = $result->CompleteSaveBusinessWalletSBPNCCResult->BusinessID;
    //   $Commission = $result->CompleteSaveBusinessWalletSBPNCCResult->Commission;
    //   $ReceiptNo = $result->CompleteSaveBusinessWalletSBPNCCResult->ReceiptNo;
    //   $Paid = $result->CompleteSaveBusinessWalletSBPNCCResult->Paid;


    //   $businessId = $BusinessId;
    //   $agentRef = strtoupper(substr(md5(uniqid()),25));
    //   $ck = $username . $custPhone . $agentRef . $businessId . $jp_key;
    //   $pass = sha1(utf8_encode($ck ));

    //   $serviceArguments = array(
    //     "userName"=>$username,
    //     "agentRef"=>$agentRef,
    //     "businessId"=>$businessId,
    //     "calendarYear"=>"2015",
    //     "jpwMobileNo"=>$custPhone,
    //     "pass"=> $pass

    //     );

    //   try {
    //     $client = new SoapClient($url);
    //   } catch (Exception $e) {
    //     redirect('/sbp/not_found');
    //   }

    //   $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
    //   $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
    //   $resultcode=$result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
    //   if ($resultcode==0 && $resulttext=="OK" ){
    //     $trans_id2=$result->PreparePaymentWalletSBPNCCResult->TransactionID;
    //     $biz_id=$result->PreparePaymentWalletSBPNCCResult->BusinessId;
    //     $bill_no=$result->PreparePaymentWalletSBPNCCResult->BillNumber;
    //     $biz_name=$result->PreparePaymentWalletSBPNCCResult->BusinessName;
    //     $physical_add=$result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
    //     $bill_status=$result->PreparePaymentWalletSBPNCCResult->BillStatus;
    //     $sbp_fee=$result->PreparePaymentWalletSBPNCCResult->SBPFee;
    //     $annualamount=$result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
    //     $penalty=$result->PreparePaymentWalletSBPNCCResult->Penalty;

    //     $data = array(
    //       'biz_id'=>$biz_id,
    //       'bill_no'=>$bill_no,
    //       'trans_id'=>$trans_id2,
    //       'commission' =>$Commission,
    //       'receiptno' =>$ReceiptNo,
    //       'biz_name'=>$biz_name,
    //       'physical_address'=>$physical_add,
    //       'bill_stat'=>$bill_status,
    //       'sbpfee'=>number_format($sbp_fee,2,'.',''),
    //       //'result'=>$result,
    //       'annualamount'=>number_format($annualamount,2),
    //       'penalty'=>$penalty,
    //       'rescode'=>$resultcode,
    //       'restext'=>$resulttext,
    //       'message'=>''
    //       );

    //     try {
    //       $db=array('receiptno' =>$ReceiptNo,'issuedate' =>DATE('Y-m-d h:i:s',time()),'from'=>$this->session->userdata('name'),'amount'=>$Paid,'businessid'=>$BusinessId,'businessowner'=>$ContactPersonName,'businessname'=>$BusinessName,'username'=>$this->session->userdata('jpwnumber'),'cashiername'=>$this->session->userdata('name'));

    //         #$query = "insert into sbp(`receiptno`,`issuedate`,`paidby`,`amount`,`businessid`,`penalties`,`annualamount`,`amountdue`,`username`,`cashiername`,`channel`)";
    //           #$query.= " values ('".implode("','",$data)."')";
    //       $this->db->insert('newsbp',$db);
    //     } catch (Exception $e) {

    //     }

    //     try {

    //               /*for sending confirmation sms*/
    //               $tel = $this->session->userdata('jpwnumber');
    //               // $amnt= number_format($amount, 2, '.', ',');
    //               $msg ="You have registered ".strtoupper($biz_name)." successfully.Business ID: $biz_id. Please await verification to get a fully approved Business Permit.Powered by JamboPay";
    //               $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

    //               $APIKey = urlencode($key);
    //               $Phone = urlencode($tel);
    //               $relayCode = urlencode("WebTribe");
    //               $Message = urlencode($msg);
    //               $Shortcode = urlencode("700273");
    //               $CampaignId = urlencode("112623");
    //               $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
    //               $ch=curl_init();
    //               curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsserver/SendSMS.aspx'.$qstr);
    //               curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //               $result = curl_exec($ch);
    //               curl_close($ch);

    //               } catch (Exception $e) {

    //               }


    //               return $data;
    //               #}
    //             } else {
    //               return $data = array('rescode'=>$resultcode,'restext'=>$resulttext);
    //             }
    //           //return $result;
    //           }else return $data = array('rescode'=>$resultcodecomplete,'restext'=>$resulttext);

    //         }


    //       }

    function register_sbp()
    {

        $url = MAIN_URL;
        $username = USER_NAME;// "97763838";
        $jp_key = JP_KEY;// '3637137f-9952-4eba-9e33-17a507a2bbb2';
        $custPhone = $this->session->userdata('jpwnumber');// '254712633277';
        $tran_id = $this->input->post('trans_id');
        $ck = $username . $tran_id . $jp_key;
        $pass = sha1(utf8_encode($ck));
        $transactionId = $tran_id;
        #$regfee='200';
        #$amnt=$this->input->post('sbp_fee');$regfee+$this->input->post('sbp_fee');//
        $amount = $this->input->post('sbp_fee') + 200;
        $currency = "KES";
        $channelRef = strtoupper(substr(md5(uniqid()), 25));
        $BusinessName = $this->input->post('biz_name');
        $IDDocumentType = $this->input->post('doc_type');
        $IDDocumentNumber = $this->input->post('doc_no');
        $PINNumber = $this->input->post('pin_no');
        $VATNumber = $this->input->post('vat_no');
        $POBox = $this->input->post('box');
        $PostalCode = $this->input->post('postal_code');
        $AnnualSBPAmount = $this->input->post('sbp_fee');//'5000';
        $Town = $this->input->post('town');
        $Telephone1 = "254" . substr($this->input->post('tel1'), -9);
        $Telephone2 = "254" . substr($this->input->post('tel2'), -9);
        $FaxNumber = $this->input->post('fax');
        $email = $this->input->post('email');
        $PhysicalAddress = $this->input->post('address');
        $PlotNumber = $this->input->post('plot_number');
        $Building = $this->input->post('building');
        $Floor = $this->input->post('floor');
        $RoomStallNo = $this->input->post('stall_room_no');
        $ContactPersonName = $this->input->post('full_names');
        $ContactPersonPOBox = $this->input->post('owner_box');
        $ContactPersonPostalCode = $this->input->post('owner_postal_code');
        $ContactPersonTelephone1 = $this->input->post('owner_telephone');
        $ContactPersonTelephone2 = $this->input->post('owner_telephone2');
        $ContactPersonFaxNumber = $this->input->post('owner_fax');
        $BusinessClassificationDetails = $this->input->post('activity_dec');
        $PremisesArea = $this->input->post('area');
        $OtherBusinessClassificationDetails = $this->input->post('other_details');
        $NumberOfEmployees = $this->input->post('emp_no');
        $ActivityCode = $this->input->post('activity_code');//'205';//
        $ZoneCode = $this->input->post('zone');
        $WardCode = $this->input->post('ward');
        $RelativeSize = $this->input->post('size');
        $pin = $this->input->post('pin');//'1234';
        $ContactPersonTown = '';
        $BusinessRegistrationNumber = '';
        $ContactPersonDesignation = '';


        $serviceArguments = array(
            "userName" => $username,
            "transactionId" => $transactionId,
            "jpPIN" => $pin,
            "amount" => $amount,
            "currency" => $currency,
            "channelRef" => $channelRef,
            "pass" => $pass,
            "BusinessName" => $BusinessName,
            "BusinessRegistrationNumber" => $BusinessRegistrationNumber,
            "PINNumber" => $PINNumber,
            "VATNumber" => $VATNumber,
            "IDDocumentType" => $IDDocumentType,
            "IDDocumentNumber" => $IDDocumentNumber,
            "ZoneCode" => $ZoneCode,
            "WardCode" => $WardCode,
            "ActivityCode" => $ActivityCode,
            "BusinessClassificationDetails" => $BusinessClassificationDetails,
            "OtherBusinessClassificationDetails" => $OtherBusinessClassificationDetails,
            "PremisesArea" => $PremisesArea,
            "RelativeSize" => $RelativeSize,
            "NumberOfEmployees" => $NumberOfEmployees,
            "AnnualSBPAmount" => $AnnualSBPAmount,
            "POBox" => $POBox,
            "PostalCode" => $PostalCode,
            "Town" => $Town,
            "Telephone1" => $Telephone1,
            "Telephone2" => $Telephone2,
            "FaxNumber" => $FaxNumber,
            "email" => $email,
            "PhysicalAddress" => $PhysicalAddress,
            "PlotNumber" => $PlotNumber,
            "Building" => $Building,
            "Floor" => $Floor,
            "RoomStallNo" => $RoomStallNo,
            "ContactPersonName" => $ContactPersonName,
            "ContactPersonDesignation" => $ContactPersonDesignation,
            "ContactPersonPOBox" => $ContactPersonPostalCode,
            "ContactPersonPostalCode" => $ContactPersonPostalCode,
            "ContactPersonTown" => $ContactPersonTown,
            "ContactPersonTelephone1" => $ContactPersonTelephone1,
            "ContactPersonTelephone2" => $ContactPersonTelephone2,
            "ContactPersonFaxNumber" => $ContactPersonFaxNumber

        );

        //return
        try {

            $client = new SoapClient($url);

            //echo "<pre>"; var_dump($serviceArguments);die();
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }

        $result = $client->CompleteSaveBusinessWalletSBPNCC($serviceArguments);


        $resulttext = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultText;
        $resultcodecomplete = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultCode;
        $dataarray['rescomplete'] = $resultcodecomplete;
        #var_dump($result); die();
        if ($resultcodecomplete == 0 && $resulttext == "OK") {
            $BusinessId = $result->CompleteSaveBusinessWalletSBPNCCResult->BusinessID;
            $Commission = $result->CompleteSaveBusinessWalletSBPNCCResult->Commission;
            $ReceiptNo = $result->CompleteSaveBusinessWalletSBPNCCResult->ReceiptNo;
            $Paid = $result->CompleteSaveBusinessWalletSBPNCCResult->Paid;


            $businessId = $BusinessId;
            $agentRef = strtoupper(substr(md5(uniqid()), 25));
            $ck = $username . $custPhone . $agentRef . $businessId . $jp_key;
            $pass = sha1(utf8_encode($ck));

            $serviceArguments = array(
                "userName" => $username,
                "agentRef" => $agentRef,
                "businessId" => $businessId,
                "calendarYear" => "2016",
                "jpwMobileNo" => $custPhone,
                "pass" => $pass

            );

            try {
                $client = new SoapClient($url);
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }

            $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
            $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
            $resultcode = $result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
            if ($resultcode == 0 && $resulttext == "OK") {
                $trans_id2 = $result->PreparePaymentWalletSBPNCCResult->TransactionID;
                $biz_id = $result->PreparePaymentWalletSBPNCCResult->BusinessId;
                $bill_no = $result->PreparePaymentWalletSBPNCCResult->BillNumber;
                $biz_name = $result->PreparePaymentWalletSBPNCCResult->BusinessName;
                $physical_add = $result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
                $bill_status = $result->PreparePaymentWalletSBPNCCResult->BillStatus;
                $sbp_fee = $result->PreparePaymentWalletSBPNCCResult->SBPFee;
                $annualamount = $result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
                $penalty = $result->PreparePaymentWalletSBPNCCResult->Penalty;

                $data = array(
                    'biz_id' => $biz_id,
                    'bill_no' => $bill_no,
                    'trans_id' => $trans_id2,
                    'commission' => $Commission,
                    'receiptno' => $ReceiptNo,
                    'biz_name' => $biz_name,
                    'physical_address' => $physical_add,
                    'bill_stat' => $bill_status,
                    'sbpfee' => number_format($sbp_fee, 2, '.', ''),
                    //'result'=>$result,
                    'annualamount' => number_format($annualamount, 2),
                    'penalty' => $penalty,
                    'rescode' => $resultcode,
                    'restext' => $resulttext,
                    'message' => ''
                );

                try {
                    $db = array('receiptno' => $ReceiptNo, 'issuedate' => DATE('Y-m-d h:i:s', time()), 'from' => $this->session->userdata('name'), 'amount' => $Paid, 'businessid' => $BusinessId, 'businessowner' => $ContactPersonName, 'businessname' => $BusinessName, 'username' => $this->session->userdata('jpwnumber'), 'cashiername' => $this->session->userdata('name'));

                    #$query = "insert into sbp(`receiptno`,`issuedate`,`paidby`,`amount`,`businessid`,`penalties`,`annualamount`,`amountdue`,`username`,`cashiername`,`channel`)";
                    #$query.= " values ('".implode("','",$data)."')";
                    $this->db->insert('newsbp', $db);
                } catch (Exception $e) {

                }

                try {

                    /*for sending confirmation sms*/
                    $tel = $this->session->userdata('jpwnumber');
                    // $amnt= number_format($amount, 2, '.', ',');
                    $msg = "You have registered " . strtoupper($biz_name) . " successfully.Business ID: $biz_id. Please await verification to get a fully approved Business Permit.Powered by JamboPay";
                    $key = "63bc8b3e-e674-4b08-879c-02e1aceedb8f";

                    $APIKey = urlencode($key);
                    $Phone = urlencode($tel);
                    $relayCode = urlencode("WebTribe");
                    $Message = urlencode($msg);
                    $Shortcode = urlencode("700273");
                    $CampaignId = urlencode("112623");
                    $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'http://192.168.7.61/smsserver/SendSMS.aspx' . $qstr);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($ch);
                    curl_close($ch);

                } catch (Exception $e) {

                }


                return $data;
                #}
            } else {
                return $data = array('rescode' => $resultcode, 'restext' => $resulttext);
            }
            //return $result;
        } else return $data = array('rescode' => $resultcodecomplete, 'restext' => $resulttext);

    }

    /* 
    Adding funtions to the fire module
       Prepare Business Payment   
    1. Fire Permit Included on renewal of businesses
    2. Fire Permit added to bussiness registration

    */


    function preparePaymentNew()
    {
        $token = $this->session->userdata['token'];
        $businessId = $this->input->post('biz_id');
        $year = $this->input->post('year');
        $number = $this->session->userdata['jpwnumber'];//"254".$num;
        $agentRef = strtoupper(substr(md5(uniqid()), 25));

        $tosend = array(
            'Stream' => "sbp",
            'BusinessID' => $businessId,
            'Year' => $year,//"2014",
            'PhoneNumber' => $number,
            'PaymentTypeID' => '1'
        );

        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);
        //var_dump($res);exit;

        if (isset($res->ErrorCode)) {
            return $data = array('error' => $res->ErrorCode, 'message' => $res->Message);
        } elseif (isset($res->Message)) {
            return $data = array('resultcode' => 55);
        } elseif (!isset($res->ErrorCode) || $res->ErrorCode == 2030) {
            $data = array(
                'trans_id' => $res->TransactionID,
                'owners_name'=>$res->Names,
                'email'=>$res->Email,
                'biz_name' => $res->BusinessName,
                'physical_address' => $res->PhysicalAddress,
                'bill_stat' => $res->BillStatus,
                'biz_id' => $res->BusinessID,
                'mobile_no' => $number,
                'penalty' => $res->Penalty,
                'annual_amount' => $res->AnnualAmount,
                'bill_no' => $res->BillNumber,
                'sbpfee' =>$res->Amount,
                'year' => $res->Year,
                'ContactPersonName' => $res->ContactPersonName,
                'id_number' => $res->IDNumber,
                'Telephone1' => $res->Telephone1,
                'building_category' => $res->BuildingCategory,
                'room_stall_number' => $res->RoomStallNumber,
                'FloorNumber' =>  $res->Floor,
                'building' => $res->Building,
                'RelativeSizeName' => $res->RelativeSizeName,
                'zone_name' => $res->ZoneName,
                'ward_name' => $res->WardName,
                'plot_number' => $res->PlotNumber,
                'resultcode' => 0,
            );
            return $data;
        }
    }

    function getFireRefId($business_id)
    {
        $this->db->where('business_id', $business_id);
        return $this->db->get('sbp_fire_details');
    }

    function getHealthRefId($business_id)
    {
        $this->db->where('business_id', $business_id);
        return $this->db->get('sbp_health_details');
    }

    function getFireBusinessDetails($fire_details)
    {       
        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $fire_details['biz_id'];
        $email = '';
        $year = date("Y",strtotime("-1 year"));
        $Tel_no = $fire_details['mobile_no'];
        $category = $fire_details['BuildingCategory'];
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck));
        $fullnames=$this->session->userdata('name');
        $url = 'http://52.24.24.25/mainsector.asmx?wsdl';//"54.218.79.241/
        $apiID = "bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $activity_code='';
        $building='';
        $floor_no='';
        $activity_name='';
        $RefID='';

        $check_ref_id = $this->getFireRefId($businessId)->row_array();
        if (count($check_ref_id)) {
            $RefID = $check_ref_id['ref_id'];
        } else {
            $serviceArguments = array(
                "userName" => $username,
                "agentRef" => $agentRef,
                "businessId" => $businessId,
                "calendarYear" => $year,
                "custMobileNo" => $Tel_no,
                "pass" => $pass
            );
            try {
                $client = new SoapClient($mainurl);
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
            $result = $client->GetIssuedSBPNCC($serviceArguments);
            $ctivity_code=$result->GetIssuedSBPNCCResult->ActivityCode;
            $building = $result->GetIssuedSBPNCCResult->Building;
            $floor_no = $result->GetIssuedSBPNCCResult->Floor;
            $activity_name = $result->GetIssuedSBPNCCResult->BusinessActivityName;

            if ($result->GetIssuedSBPNCCResult->Result->ResultCode != 1) {
                try {
                    $serviceArgs = array(
                        "API_Id" => $apiID,
                        "Fullname" => $fire_details['owners_name'],
                        "Email" => $fire_details['email'],
                        "Tel_no" => $fire_details['mobile_no'],
                        "BusinessName" => $fire_details['biz_name'],//$Firm,
                        "Owner" => $fire_details['owners_name'],//$Occupation,
                        "PlotNo" => $fire_details['PlotNumber'],
                        "LRNO" => $fire_details['PlotNumber'],
                        "Building" => $fire_details['physical_address'],
                        "subcounty" => $fire_details['WardName'],
                        "Form_Id" => "FR",
                        "activity_code" =>$result->GetIssuedSBPNCCResult->ActivityCode,
                        "B_id" => $fire_details['biz_id'],
                        "zone" => $fire_details['ZoneName'],
                    );
                    $client = new SoapClient($url);                    
                    $result = $client->PostfireApplication($serviceArgs);
                    $RefID = $result->PostfireApplicationResult->string["0"];
                    if ($RefID != "400") {
                        $sbp_fire_data = array(
                            "phone_number" => $fire_details['mobile_no'],
                            "business_id" => $fire_details['biz_id'],
                            "ref_id" => $RefID,
                            "date_requested" => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('sbp_fire_details', $sbp_fire_data);
                    }
                } catch (Exception $e) {
                    redirect('health/APIerror');
                }
            }
           }

            if ($RefID != "400") {
                $client = new SoapClient($url);
                $serviceArguments2 = array(
                    "API_Id" => $apiID,
                    "RefId" => $RefID,
                );
                $result2 = $client->GetfireinvoiceDetails($serviceArguments2);
                $Fullname = $result2->GetfireinvoiceDetailsResult->Name;
                $Email = $result2->GetfireinvoiceDetailsResult->Email;
                $Firm = $result2->GetfireinvoiceDetailsResult->BusinessName;
                $InvoiceNo = $result2->GetfireinvoiceDetailsResult->InvoiceNum;
                $Tel_no = $result2->GetfireinvoiceDetailsResult->Tel_no;
                $Owner = $result2->GetfireinvoiceDetailsResult->Owner;
                $Building = $result2->GetfireinvoiceDetailsResult->Building;
                $Category = $result2->GetfireinvoiceDetailsResult->category;
                $Status = $result2->GetfireinvoiceDetailsResult->status;
                $RefID = $result2->GetfireinvoiceDetailsResult->RefId;
                $Amount = $result2->GetfireinvoiceDetailsResult->amount;

                $data = array(
                    "RefID" => $RefID,
                    "issueDate" => date("Y/m/d"),
                    "Fullname" => $Fullname,
                    "Email" => $Email,
                    "TelNumber" => $Tel_no,
                    "Firm" => $Firm,
                    "InvoiceNo" => $InvoiceNo,
                    "Status" => $Status,
                    "Owner" => $Owner,
                    "Amount" => $Amount,
                    "Building" => $Building,
                    "Category" => $category,
                    "activity_code"=>$activity_code,
                    "activity_name"=>$activity_name,
                    "building"=>$building,
                    "floor_no"=>$floor_no,
                );
                try {
                    $this->db->insert('fireinspection', $data);
                } catch (Exception $e) {

                }

                return $data;
            } elseif ($RefID == "400") {
                redirect('fire/error');
            } else {
                redirect('fire/InvalidBid');
            }
            
    }

    function getHygieneBusinessDetails($health_details,$fire_details)
    {
        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $health_details['biz_id'];
        $email = '';
        $fullnames = $this->session->userdata('name');
        $year = date("Y");
        $Tel_no =$health_details['mobile_no'];
        $category = $health_details['BuildingCategory'];
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck));
        //"http://54.218.79.241/mainsector.asmx?WSDL";
        $url = 'http://52.24.24.25/mainsector.asmx?wsdl';
        // $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
        $apiID = "bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $RefID='';

        $check_ref_id = $this->getHealthRefId($businessId)->row_array();
        
        if (count($check_ref_id)) {
            $RefID = $check_ref_id['ref_id'];
        } else {
            if($fire_details['activity_name'] != '') {
                $serviceArgs = array(
                        "API_Id" => $apiID,
                        "Fullname" => $health_details['owners_name'],
                        "Institution_name" => $health_details['biz_name'],//$Firm,
                        "owner" => $health_details['owners_name'],
                        "Mobile" => $health_details['mobile_no'],
                        "Address" => $health_details['physical_address'],//$Owner,
                        "mail" => $health_details['email'],
                        "Plot_no" => $health_details['PlotNumber'],
                        "Road" => $health_details['physical_address'],
                        "Frontingon" => $health_details['physical_address'],
                        "Building" => $fire_details['building'],
                        "FloorNo" => $fire_details['floor_no'],
                        "zone" => $health_details['ZoneName'],
                        "subcounty" => $health_details['WardName'],
                        "form_id" => "FH",
                        "activity_code" => strval($fire_details['activity_code']),
                        "Purpose" => $fire_details['activity_name'],
                        "Nature" => $fire_details['activity_name'],
                        "B_id" => $health_details['biz_id'],
                    );
            } else {
                $serviceArguments = array(
                "userName" => $username,
                "agentRef" => $agentRef,
                "businessId" => $businessId,
                "calendarYear" => $year,
                "custMobileNo" => $Tel_no,
                "pass" => $pass,
                );
                try {
                    $client = new SoapClient($mainurl);
                } catch (Exception $e) {
                    redirect('/sbp/not_found');
                }
                $result = $client->GetIssuedSBPNCC($serviceArguments);              
                $sbpnumber = $result->GetIssuedSBPNCCResult->SBPNumber;
                $year = $result->GetIssuedSBPNCCResult->CalenderYear;
                $bizname = $result->GetIssuedSBPNCCResult->BusinessName;
                $pinno = $result->GetIssuedSBPNCCResult->PINNumber;
                $biztype = $result->GetIssuedSBPNCCResult->BusinessActivityDescription;
                $activitycode = $result->GetIssuedSBPNCCResult->ActivityCode;
                $paidfee = $result->GetIssuedSBPNCCResult->AmountPaid;
                $inwords = $result->GetIssuedSBPNCCResult->AmountPaidInWords;
                $pobox = $result->GetIssuedSBPNCCResult->POBox;
                $plotnumber = $result->GetIssuedSBPNCCResult->PlotNumber;
                $street = $result->GetIssuedSBPNCCResult->PhysicalAddress;
                $issuedate = $result->GetIssuedSBPNCCResult->DateIssued;
                $bizactivityname = $result->GetIssuedSBPNCCResult->BusinessActivityName;
                $bizzid = $result->GetIssuedSBPNCCResult->BusinessID;
                $receiptno = $result->GetIssuedSBPNCCResult->REFReceiptNumber;
                $physicaladdress = $result->GetIssuedSBPNCCResult->PhysicalAddress;
                $resultcode = $result->GetIssuedSBPNCCResult->Result->ResultCode;
                $building = $result->GetIssuedSBPNCCResult->Building;
                $approvalstatus = $result->GetIssuedSBPNCCResult->ApprovalStatus;
                $floor_no = $result->GetIssuedSBPNCCResult->Floor;
           
                if ($resultcode != "1") {
                    $serviceArgs = array(
                        "API_Id" => $apiID,
                        "Fullname" => $health_details['owners_name'],
                        "Institution_name" => $health_details['biz_name'],//$Firm,
                        "owner" => $health_details['owners_name'],
                        "Mobile" => $health_details['mobile_no'],
                        "Address" => $health_details['physical_address'],//$Owner,
                        "mail" => $health_details['email'],
                        "Plot_no" => $health_details['PlotNumber'],
                        "Road" => $health_details['physical_address'],
                        "Frontingon" => $health_details['physical_address'],
                        "Building" => $building,
                        "FloorNo" => $floor_no,
                        "zone" => $health_details['ZoneName'],
                        "subcounty" => $health_details['WardName'],
                        "form_id" => "FH",
                        "activity_code" => strval($activitycode),
                        "Purpose" => $bizactivityname,
                        "Nature" => $bizactivityname,
                        "B_id" => $health_details['biz_id'],
                    );
                }
               
            
                try {
                    $client = new SoapClient($url);
                    $result = $client->PostApplication($serviceArgs);
                    $RefID = $result->PostApplicationResult->string["0"];
                    if ($RefID != "400") {
                        $sbp_health_data = array(
                            "phone_number" => $health_details['mobile_no'],
                            "email" => $health_details['email'],
                            "business_id" => $health_details['biz_id'],                            
                            "ref_id" => $RefID,
                            "date_requested" => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('sbp_health_details', $sbp_health_data);
                    }

                } catch (Exception $e) {
                    redirect('health/APIerror');
                }#var_dump($RefID); die();
            }
        }

        if ($RefID != "400") {
            $serviceArguments2 = array(
                "API_Id" => $apiID,
                "Refid" => $RefID,
            );
            //var_dump($serviceArguments2);exit;

          try{
              $client2 = new SoapClient($url);  
          } catch(SoapFault $fault) {
             trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
             redirect('fire/error');

          }
            
            $result2 = $client2->GetInvoiceDetails($serviceArguments2);
            if(property_exists($result2->GetInvoiceDetailsResult, 'Name')){
                  $Fullname = $result2->GetInvoiceDetailsResult->Name;
                  $Email = $result2->GetInvoiceDetailsResult->Email;
                  $Firm = $result2->GetInvoiceDetailsResult->Institution_name;
                  $InvoiceNo = $result2->GetInvoiceDetailsResult->InvoiceNum;
                  $Mobile = $result2->GetInvoiceDetailsResult->Tel_no;
                  $status = $result2->GetInvoiceDetailsResult->status;
                  $RefID = $result2->GetInvoiceDetailsResult->RefId;
                  $certificate = $result2->GetInvoiceDetailsResult->certificate;
                  $Amount = $result2->GetInvoiceDetailsResult->amount;
                  $totalAmount = $certificate + $Amount;

                  $data = array(
                      "RefID" => $RefID,
                      "issueDate" => date("Y/m/d"),
                      "Fullname" => $Fullname,
                      "Email" => $Email,
                      "TelNumber" => $Mobile,
                      "Firm" => $Firm,
                      "InvoiceNo" => $InvoiceNo,
                      "status" => $status,
                      "certificate" => $certificate,
                      "Amount" => $Amount,
                      "total" => $totalAmount,
                      /*"plotno" => $plotnumber,*/
                      "nature" => $category,
                  );
                  try {
                      $this->db->insert('fhygiene', $data);
                  } catch (Exception $e) {

                  }
                  return $data;
            } else {                
                //redirect('health/error/details');
            }
            
            
        } elseif ($RefID == "400") {
            redirect('health/error');
        } else {
            redirect('health/invalidBID');
        }
    
    }
    /*
        Test for preparing payments
    */
    function testPreparePaymentNew()
    {       
        $token = $this->session->userdata['token'];
        $business_id = $this->input->post('bid');
        $year = $this->input->post('year');
        $number = $this->session->userdata['jpwnumber'];//"254".$num;
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $advert_check=($this->input->post('signage')) ?  'true' : 'false';

        //echo $kitchen_check;exit;

        $tosend = array(
            'Stream' => "sbp",
            'BusinessID' => $business_id,
            'Year' => $year,//"2014",
            'PhoneNumber' => $number,
            'PaymentTypeID' => '1',            
            'SBPSubClassID' =>$this->input->post('sub_classes')           
        );

        //echo json_encode($tosend);exit;


        
        $this->traceAPI('POST','post-to-send',json_encode($tosend));


        foreach ($tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);

        $url = "http://192.168.11.22/JamboPayServices/api/payments/post";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);

       
        
        $this->traceAPI('API','prepareRenewal',$result);
        $res = json_decode($result,true);
        curl_close($ch);
        return $res;
       //echo isset($res['ErrorType']);exit;
        
        /*if(isset($res['ErrorType'])){
            return $data = array('error' => 500, 'message' => "Sorry! Service was ureachable. Please try again later");
        } elseif (isset($res['ErrorCode'])) {
            return $data = array('error' => $res['ErrorCode'], 'message' => $res['Message']);
        } elseif (isset($res['Message'])) {
            return $data = array('resultcode' => 55);
        } elseif (!isset($res['ErrorCode']) || $res['ErrorCode'] == 2030) {
            $data = array(
                'biz_id' => $res['BusinessID'],
                'trans_id' => $res['TransactionID'],
                'owners_name'=>$res['Names'],
                'email'=>$res['Email'],
                'biz_name' => $res['BusinessName'],
                'physical_address' => $res['PhysicalAddress'],
                'bill_stat' => $res['BillStatus'],
                'mobile_no' => $number,
                'penalty' => $res['Penalty'],
                'annual_amount' => $res['AnnualAmount'],
                'bill_no' => $res['BillNumber'],
                'license_fee' =>$res['Amount'],
                'year' => $res['Year'],
                'contact_person_name' => $res['ContactPersonName'],
                'id_number' => $res['IDNumber'],
                'telephone_one' => $res['Telephone1'],
                'building_category' => $res['BuildingCategory'],
                'room_stall_number' => $res['RoomStallNumber'],
                'floor_number' =>  $res['Floor'],
                'building' => $res['Building'],
                'relative_size_name' => $res['RelativeSizeName'],
                'zone_name' => $res['ZoneName'],
                'ward_name' => $res['WardName'],
                'plot_number' => $res['PlotNumber'],
                'result_code' => 0,
                'activity_code'=>$res['ActivityCode'],
                'po_box'=>$res['POBox'],
                'pin_no'=>$res['Pin'],
                'activity_name'=>$res['ActivityName'],
                'sub_streams'=>$this->sub_streams($res['LineItems']),
            );
            $this->checkHealthFire($data);
            $this->update_streams($data['trans_id'],$data['sub_streams']);

            $this->session->set_userdata('bid_prepared_details',$data);
            $this->traceAPI('DATA-CHECK','check-stream',json_encode($data)); 
            return $data;
        }*/
    }

    public function update_streams($trans_id,$streams)
    {
        $transaction_streams=array(
                "transaction_id"=>$trans_id,
                "streams"=>$streams,
                );
        $check_trans_status=$this->db->get_where('business_trans_stream', array('transaction_id' => $trans_id));
        if($check_trans_status->num_rows()){
            $this->db->where('transaction_id',$trans_id);
            $this->db->update('business_trans_stream',$transaction_streams);
        } else {
            $this->db->insert('business_trans_stream',$transaction_streams);
        }
    }

    public function get_streams_on_trans_id($trans_id)
    {
        $this->db->where('transaction_id',$trans_id);
        $stream_by_trans_id=$this->db->get('business_trans_stream')->row_array();
        return $stream_by_trans_id['streams'];
    }

    public function checkHealthFire(&$data)
    {
        $streams=json_decode($data['sub_streams'],true);
        foreach ($streams as $key => $stream) {            
            if($stream['Name']=='Fire Certificate') {                
                $data['fire_certificate']=true;
            } elseif(trim($stream['Name'])=='Food Hygiene Certificate') {
                $data['health_certificate'] =true;
            } elseif($stream['Name']=='Health Certificate') {
                $data['health_certificate'] =true;
            }
        }
    }

    public function sub_streams($data)
    {
        
        foreach ($data as $key => $stream) {            
            if($stream['Name']=='tradeLicensefee') {
                $data[$key]['Name']='Trade License';
            } elseif($stream['Name']=='FirePermitLicenseFee') {
                $data[$key]['Name']='Fire Certificate';
            } elseif(trim($stream['Name'])=='FoodHygiene') {
                $data[$key]['Name']='Food Hygiene Certificate';
            } elseif($stream['Name']=='Advertisement') {
                $data[$key]['Name']='Advertisement Permit';
            } elseif($stream['Name']=='healthCertificate') {
                $data[$key]['Name']='Health Certificate';
            }
        }
         
        //var_dump($data);exit;
        return json_encode($data);
    }

    
    /*
        Test save prepared payments
    */
    function testprepsaveinitialNew()
    {        
        $data = array(
            'biz_activity' => $this->getBusinessActivities(),
            'rel_size' => $this->getBusinessSizes(),
            'id_type' => $this->getIdTypes(),
            'zones' => $this->getBusinessZones(),
            'wards' => $this->getBusinessWards(),
            'registration_fee' => $this->getRegistrationFee(),
            'sub_county'=>$this->getSubCounty(),
            'res_code' => 0,
        );


        return $data;
    }
    /*
        Support the above function :
        - Get details for the above function
    */

    public function getBusinessActivities()
    {
        return $this->db->get('business_activities')->result_array();       
    }

    public function getBusinessSizes()
    {
        $this->db->get('business_sizes')->result_array();
    }

    public function getIdTypes()
    {
        $this->db->get('id_types')->result_array();
    }

    public function getRegistrationFee()
    {
        $this->db->where('name',"business");
        $reg_amount=$this->db->get('registration_fee')->row_array();
        return $reg_amount['amount'];
    }

    public function getBusinessZones()
    {
       return $this->db->get('zones')->result_array();
    }

    public function getBusinessWards()
    {
        return $this->db->get('wards')->result_array();
    }

    public function getSubCounty()
    {
        return $this->db->get('sub_county')->result_array();
    }

    function testgetFireBusinessDetails()
    {        
        $business_details=$this->session->userdata('bid_prepared_details');

        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $business_details['biz_id'];
        $email = '';
        $year = date("Y",strtotime("-1 year"));
        $Tel_no = $business_details['mobile_no'];
        $category = $business_details['activity_code'];
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck));
        $fullnames=$this->session->userdata('name');
        $url = 'http://52.24.24.25/mainsector.asmx?wsdl';//"54.218.79.241/
        $apiID = "bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $activity_code='';
        $building='';
        $floor_no='';
        $activity_name='';
        $RefID='';

        $check_ref_id = $this->getFireRefId($businessId)->row_array();
        if (count($check_ref_id)) {
            $RefID = $check_ref_id['ref_id'];
        } else {
            $serviceArguments = array(
                "userName" => $username,
                "agentRef" => $agentRef,
                "businessId" => $businessId,
                "calendarYear" => $year,
                "custMobileNo" => $Tel_no,
                "pass" => $pass
            );
            try {
                $client = new SoapClient($mainurl);
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
            $result = $client->GetIssuedSBPNCC($serviceArguments);

            if ($result->GetIssuedSBPNCCResult->Result->ResultCode != 1) {
                $business_details['activity_code']=$result->GetIssuedSBPNCCResult->ActivityCode;
                $business_details['activity_name']=$result->GetIssuedSBPNCCResult->BusinessActivityName;
                $business_details['floor_number']=$result->GetIssuedSBPNCCResult->Floor;
                $business_details['building']= $result->GetIssuedSBPNCCResult->Building;

                $this->session->unset_userdata('bid_prepared_details');
                $this->session->set_userdata('bid_prepared_details',$business_details);
                try {
                    $serviceArgs = array(
                        "API_Id" => $apiID,
                        "Fullname" => $business_details['owners_name'],
                        "Email" => $business_details['email'],
                        "Tel_no" => $business_details['mobile_no'],
                        "BusinessName" => $business_details['biz_name'],//$Firm,
                        "Owner" => $business_details['owners_name'],
                        "PlotNo" => $business_details['plot_number'],
                        "LRNO" => $business_details['plot_number'],
                        "Building" => $business_details['building'],
                        "subcounty" => $business_details['ward_name'],
                        "Form_Id" => "FR",
                        "activity_code" =>$business_details['activity_code'],
                        "B_id" => $business_details['biz_id'],
                        "zone" => $business_details['zone_name'],
                        );

                    $client = new SoapClient($url);                    
                    $result = $client->PostfireApplication($serviceArgs);
                    $RefID = $result->PostfireApplicationResult->string["0"];
                    if ($RefID != "400") {
                        $sbp_fire_data = array(
                            "phone_number" => $business_details['mobile_no'],
                            "business_id" => $business_details['biz_id'],
                            "ref_id" => $RefID,
                            "date_requested" => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('sbp_fire_details', $sbp_fire_data);
                    }
                } catch (Exception $e) {
                    redirect('health/APIerror');
                }
            }
           }
           if ($RefID != "400") {
                $client = new SoapClient($url);
                $serviceArguments2 = array(
                    "API_Id" => $apiID,
                    "RefId" => $RefID,
                );
                $result2 = $client->GetfireinvoiceDetails($serviceArguments2);
                $data = array(
                    "RefID" => $RefID,
                    "Amount" => $result2->GetfireinvoiceDetailsResult->amount,
                );
                return $data;
            } elseif ($RefID == "400") {
               $data = array(
                "RefID" => '',
                "Amount" => 4500,
                );
               return $data;
            } else {
                redirect('fire/InvalidBid');
            }
    }

    public function unified_print_permit($business_id,$year)
        {
          $token = $this->session->userdata('token');

          $BusinessID = @trim($business_id);
          $Year = $year;
          $PhoneNumber = $this->session->userdata('jpwnumber');

          $post_url = "http://192.168.6.10/JamboPayServices/api/payments/GetSBPPermit?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

              $fp = fopen (dirname(__FILE__) . '/localfile.tmp', 'w+');//This is the file where we save the    information

          //$post_url = REST_URL."GetSBPPermit?stream=sbp&PhoneNumber=$PhoneNumber&BusinessID=$business_id&year=$year";

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $post_url);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
            ));

         $file = curl_exec($ch);

             if (isset($res->ErrorCode)) {
                return $data = array('error' => $res->ErrorCode, 'message' => $res->Message);
             }
             curl_close($curl);

             header('Content-type: ' . 'application/octet-stream');
             header('Content-Disposition: ' . 'attachment; filename=permit.pdf');
             echo $file;

        }

    function testgetHygieneBusinessDetails()
    {
        $business_details=$this->session->userdata('bid_prepared_details');
        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $business_details['biz_id'];
        $year = date("Y",strtotime("-1 year"));
        $agentRef = strtoupper(substr(md5(uniqid()), 25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck));
        //"http://54.218.79.241/mainsector.asmx?WSDL";
        $url = 'http://52.24.24.25/mainsector.asmx?wsdl';
        // $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
        $apiID = "bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        //$RefID='';

        $check_ref_id = $this->getHealthRefId($businessId)->row_array();
        
        if (count($check_ref_id)) {
            $RefID = $check_ref_id['ref_id'];
        } else {
            if($business_details['activity_name'] == '') {
                $serviceArguments = array(
                    "userName" => $username,
                    "agentRef" => $agentRef,
                    "businessId" => $businessId,
                    "calendarYear" => $year,
                    "custMobileNo" => $business_details['mobile_no'],
                    "pass" => $pass,
                );
                try {
                    $client = new SoapClient($mainurl);
                } catch (Exception $e) {
                    redirect('/sbp/not_found');
                }
                $result = $client->GetIssuedSBPNCC($serviceArguments);
                if ($result->GetIssuedSBPNCCResult->Result->ResultCode != 1) {
                     $business_details['activity_code']=$result->GetIssuedSBPNCCResult->ActivityCode;
                     $business_details['activity_name']=$result->GetIssuedSBPNCCResult->BusinessActivityName;
                     $business_details['floor_number']=$result->GetIssuedSBPNCCResult->Floor;
                     $business_details['building']= $result->GetIssuedSBPNCCResult->Building;
                     $this->session->unset_userdata('bid_prepared_details');
                     $this->session->set_userdata('bid_prepared_details',$business_details);
                 }
             }

             try {
                $serviceArgs = array(
                    "API_Id" => $apiID,
                    "Fullname" => $business_details['owners_name'],
                    "mail" => $business_details['email'],
                    "Mobile" => $business_details['mobile_no'],
                    "Address" => $business_details['physical_address'],
                    "Institution_name" => $business_details['biz_name'],//$Firm,
                    "owner" => $business_details['owners_name'],
                    "Plot_no" => $business_details['plot_number'],
                    "Road" => $business_details['physical_address'],
                    "Frontingon" => $business_details['physical_address'],
                    "LRNO" => $business_details['plot_number'],
                    "Building" => $business_details['building'],
                    "FloorNo" => $business_details['floor_number'],
                    "subcounty" => $business_details['ward_name'],
                    "Form_Id" => "FH",
                    "activity_code" =>$business_details['activity_code'],
                    "Purpose" => $business_details['activity_name'],
                    "Nature" => $business_details['activity_name'],
                    "B_id" => $business_details['biz_id'],
                    "zone" => $business_details['zone_name'],
                    );
                $client = new SoapClient($url);
                $result = $client->PostApplication($serviceArgs);                
                $RefID = $result->PostApplicationResult->string["0"];
                if($RefID != "400") {
                   $sbp_health_data = array(
                    "phone_number" => $business_details['mobile_no'],
                    "email" => $business_details['email'],
                    "business_id" => $business_details['biz_id'],
                    "ref_id" => $RefID,
                    "date_requested" => date('Y-m-d H:i:s'),
                    );

                   $this->db->insert('sbp_health_details', $sbp_health_data);

                } elseif($RefID == 400) {
                    $data = array(
                        "RefID" => '',
                        "Amount" => 4500,
                        );
                    return $data;
                } else {
                    redirect('health/APIerror');
                }
            } catch (Exception $e) {
                redirect('health/APIerror');
            }
        }
        if ($RefID != "400") {
            $client = new SoapClient($url);
            $serviceArguments2 = array(
                "API_Id" => $apiID,
                "Refid" => $RefID,
            );            
            $result2 = $client->GetInvoiceDetails($serviceArguments2);            
            $data = array(
                "RefID" => $RefID,
                "Amount" => $result2->GetInvoiceDetailsResult->amount,
            );
            return $data;
        } elseif ($RefID == "400") {
            redirect('fire/error');
        } else {
            redirect('fire/InvalidBid');
        }
        
    }
}