<?php

class Land_rates_model extends CI_Model{

  function allLandrates(){
    $sql = "SELECT * FROM `landrates` ORDER BY `issuedate` desc";
    $result = $this->db->query($sql);
    return $result;
  }

	function preparePayment(){

    $url=MAIN_URL;
		$plot_no = $this->input->post('plot_no');//$_POST['plot_no'];
		$year = "2014";
		$no = $this->session->userdata['jpwnumber'];//$this->input->post('cust_phone');
    $num =  substr($no,-9);
    $number ="254".$num;
		$username = USER_NAME;
	  $key = JP_KEY;//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
	  $agentRef = strtoupper(substr(md5(uniqid()),25)); 
	  $ck = $username.$number.$agentRef.$plot_no.$key;
	  $pass = sha1(utf8_encode($ck));

	    $params = array(
	    	"userName"=>$username,
	    	"agentRef"=>$agentRef,
	    	"plotNo"=>$plot_no,
	    	#"calendarYear"=>"2014",
	    	"jpwMobileNo"=> $number,
	    	"pass"=> $pass

	    	);

	    try {
            $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
	    $result = $client->PreparePaymentWalletPPRNCC($params);
	    $DueDate = $result->PreparePaymentWalletPPRNCCResult->DueDate;
	    $PhysicalAddress = $result->PreparePaymentWalletPPRNCCResult->PPhysicalAddress;
	    $UserDescription = $result->PreparePaymentWalletPPRNCCResult->PUseDescription;
	    $PlotNumber = $result->PreparePaymentWalletPPRNCCResult->PlotNumber;
	    $TransactionID = $result->PreparePaymentWalletPPRNCCResult->TransactionID;
	    $UPN = $result->PreparePaymentWalletPPRNCCResult->UPN;
	    $ResultCode = $result->PreparePaymentWalletPPRNCCResult->Result->ResultCode;
	    $ResultText = $result->PreparePaymentWalletPPRNCCResult->Result->ResultText;
      $arrears = $result->PreparePaymentWalletPPRNCCResult->TotalArrears;
      $CurrentBalance = $result->PreparePaymentWalletPPRNCCResult->CurrentBalance;
      $AnnualAmount = $result->PreparePaymentWalletPPRNCCResult->TotalAnnualAmount;
      $Penalty = $result->PreparePaymentWalletPPRNCCResult->AccumulatedPenalty;
      $Name=$result->PreparePaymentWalletPPRNCCResult->CustomerName;

	    $data=array(
	    	'resultcode'=>$ResultCode,
	    	'resulttext'=>$ResultText,
	    	'trans_id'=>$TransactionID,
	    	'duedate'=>$DueDate,
	    	'address'=>$PhysicalAddress,
	    	'description'=>$UserDescription,
	    	'pno'=>$PlotNumber,
	    	'mobile_no'=>$number,
	    	'amount'=>$AnnualAmount,
        'penalty'=>$Penalty,
        'balance'=>$CurrentBalance,
        'arrears'=>$arrears,
	    	'upn'=>$UPN,
        'ownername'=>$Name
	    	);

	    $this->session->set_userdata($data);#var_dump($data);die();
	    return $data; 
	} 

  function preparePaymentNew(){

      $token = $this->session->userdata['token'] ;
      $plot_no = $this->input->post('plot_no');//$_POST['plot_no'];
      $no = $this->session->userdata['jpwnumber'];//$this->input->post('cust_phone');
      
      $tosend = array(
            'Stream'=>"landrate",
            'PlotNumber'=>$plot_no,
            'PhoneNumber' =>$no,
            'PaymentTypeID' => "1"    
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        #echo "<pre>"; var_dump($res); die();
      
      if(isset($res->TransactionID)){
      $DueDate = $res->DueDate;
      $PhysicalAddress = $res->PhysicalAddress;
      $Description = $res->Description;
      $PlotNumber = $res->PlotNumber;
      $TransactionID = $res->TransactionID;
      $UPN = $res->UPN;
      $GroundRent = $res->GroundRent;
      $Rate = $res->Rate;
      $RateNumber = $res->RateNumber;
      $OtherCharges = $res->OtherCharges;
      $arrears = $res->Arreas;
      $Balance = $res->Balance;
      $AnnualAmount = $res->AnnualAmount;
      $Penalty = $res->Penalty;
      $Adjustment = $res->Adjustment;
      $AmountDue=$res->AmountToPay;
      $Name=$res->CustomerNames;
      $Waiver = $res->Waiver;
      $WaiverPercentage = $res->WaiverPercentage;
      $EntitledWaiver = $res->EntitledWaiver;
      $MinimumAmountForWaiver = $res->MinimumAmountForWaiver;
      $Message = $res->Message;

      
      $data=array(
        'resultcode'=>"0",
        'trans_id'=>$TransactionID,
        'duedate'=>$DueDate,
        'address'=>$PhysicalAddress,
        'description'=>$Description,
        'pno'=>$PlotNumber,
        'adjustment'=>$Adjustment,
        'annualamount'=>$AnnualAmount,
        'amountdue'=>$AmountDue,
        'penalty'=>$Penalty,
        'balance'=>$Balance,
        'arrears'=>$arrears,
        'upn'=>$UPN,
        'ownername'=>$Name,
        'groundrent'=>$GroundRent,
        'rate'=>$Rate,
        'ratenumber'=>$RateNumber,
        'othercharges'=>$OtherCharges,
        'waiver'=>$Waiver,
        'WaiverPercentage'=>$WaiverPercentage,
        'EntitledWaiver'=>$EntitledWaiver,
        'MinimumAmountForWaiver'=>$MinimumAmountForWaiver,
        'Message'=>$Message

        );

      #$this->session->set_userdata($data);
      return $data;
    }else{#var_dump($data);die();
      // foreach ($res as $keys => $value) {
      //     #echo $keys;
      // }
      return $data = array('resultcode'=>$res->ErrorCode);
    }
  }


	function complete(){

    $url=MAIN_URL;
    $username = USER_NAME;// "72386895";
    $key = JP_KEY;// "26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
    $tranid = $this->session->userdata('trans_id');
    $channelRef = "45875";  
    $amount = $this->input->post('paid_amount');
    $amount = number_format($amount, 2,'.','');
    $currency = "KES";
    $ck = $username . $tranid . $key;
    $pass = sha1(utf8_encode($ck ));
    $pin = $this->session->userdata['jp_pin']; //$this->input->post('jp_pin');
    $plotno = $this->input->post('plotnum');
    $l_customer = $this->session->userdata('name');
    $l_customer = mysql_real_escape_string($l_customer);
    $user = $this->session->userdata('jpwnumber');
    $plotowner = $this->input->post('plotowner');
    $penalties = $this->input->post('penalty');
    $amountdue = $this->input->post('amountdue');;
    $issuedate = date('Y-m-d',time());

    $serviceArguments = array(
      		"userName"=>$username,
            "transactionId"=>$tranid,
            "jpPIN"=>$pin,
            "CalenderYear"=>"2014",
            "amount"=>$amount,
            "currency"=> "KES",
            "channelRef"=> "45875",
            "pass"=> $pass
      );

   try {
                $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }

   $result = $client->CompletePaymentWalletPPRNCC($serviceArguments);
   $ResultCode = $result->CompletePaymentWalletPPRNCCResult->Result->ResultCode;
   $ResultText = $result->CompletePaymentWalletPPRNCCResult->Result->ResultText;
   

   if($ResultCode==0 && $ResultText=="OK" ){
    $Receipt = $result->CompletePaymentWalletPPRNCCResult->ReceiptNo;
    $Paid = $result->CompletePaymentWalletPPRNCCResult->Paid;

    /*for inserting to db*/
    try {
      $data=array('receiptno'=>$Receipt,'issuedate'=>$issuedate,'paidby'=>$l_customer,'amount'=>str_replace(',','',$amount),'plotno'=>$plotno,'plotowner'=>$plotowner,'penalties'=>str_replace(',','',$penalties),'amountdue'=>str_replace(',','',$amountdue),'username'=>$user,'cashiername'=>$l_customer,'channel'=>'selfservice');
      #$this->db->insert('landratestest', $data);
        $query = "insert into landrates (`receiptno`,`issuedate`,`paidby`,`amount`,`plotno`,`plotowner`,`penalties`,`amountdue`,`username`,`cashiername`,`channel`)";
        $query.= " values (?,?,?,?,?,?,?,?,?,?,?)";
        
        $result=$this->db->query($query,$data) or die(mysql_error());
    } catch (Exception $e) {
      
    }
    /*for sending confirmation sms*/
    try {
          $tel = $this->session->userdata('jpwnumber');
          $amnt= $amount;
          $msg ="Receipt Number  $Receipt .Your payment of KES $amnt for Plot number $plotno has been received by Nairobi City County.Powered by: JamboPay";
          $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

          $APIKey = urlencode($key);
          $Phone = urlencode($tel);
          $relayCode = urlencode("WebTribe");
          $Message = urlencode($msg);
          $Shortcode = urlencode("700273");
          $CampaignId = urlencode("112623");
          $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
          $ch=curl_init();
          curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
          $result = curl_exec($ch);
          curl_close($ch);
    } catch (Exception $e) {}
            
      $data = array(
        "receiptnum"=>$Receipt,
        "paymentfrom"=>$this->session->userdata['name'],
        "mobile"=>$this->session->userdata['jpwnumber'],
        "kshs"=>$Paid,
        "amount_due"=>$this->input->post('amount_due'),
        "plotowner"=>$this->input->post('plotowner'),
        "plotnum"=>$this->input->post('plotnum'),
        "rescode"=>$ResultCode,
        "restext"=>$ResultText
      );

      return $data;        
   }
       else{
       $data=array('rescode'=>$ResultCode,'restext'=>$ResultText);
       return $data;
        }

}

  function completeNew(){

    $token = $this->session->userdata['token'] ;
    $transid = $this->input->post('transid');
    $amount = $this->input->post('amountentered');
    #$amount = number_format($amount, 2,'.','');
    $pin = $this->input->post('jp_pin');
    #$plotno = $this->input->post('plotnum');
    #$l_customer = $this->session->userdata('name');
    #$l_customer = mysql_real_escape_string($l_customer);
    $phone = $this->session->userdata('jpwnumber');
    #$plotowner = $this->input->post('plotowner');
    #$penalties = $this->input->post('penalty');
    #$amountdue = $this->input->post('amountdue');
    #$issuedate = date('Y-m-d',time());

    $tosend = array(
            'TransactionID'=>$transid,
            'Amount'=>$amount,
            'PhoneNumber' =>$phone,
            'Pin' =>$pin,
            'Stream'=>"landrate"    
          );

      foreach ( $tosend as $key => $value) {
      $post_items[] = $key . '=' . $value;
      }
      $post_string = implode ('&', $post_items);

      $url = "http://192.168.6.10/JamboPayServices/api/payments";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec($ch);
      $res = json_decode($result); 

      // foreach ($res as $key => $value) {
      //   echo $key;
      // }
      #echo "<pre>"; var_dump($res); die();

   

     if(isset($res->ReceiptNumber)){
        
      $data = array(
        "receiptnum"=>$res->ReceiptNumber,
        #"paymentfrom"=>$this->session->userdata['name'],
        #"mobile"=>$this->session->userdata['jpwnumber'],
        #"kshs"=>$amount,
        #"amount_due"=>$this->input->post('amount_due'),
        #"plotowner"=>$this->input->post('plotowner'),
        #"plotnum"=>$this->input->post('plotnum'),
        "rescode"=>"0",
        "restext"=>"OK"
      );
      return $data;        
      }
       else{
         #foreach ($res as $keys => $value) {
          #echo $keys;
          #}
       $data=array('rescode'=>$res->ErrorCode);
       return $data;
        }

}

function confirm_payment(){

  	$plotno = $this->input->post('plotnum');
    $transid = $this->input->post('transid');
    $amountentered = $this->input->post('paid_amount');
    $minimumamountforwaiver = $this->input->post('minimumamountforwaiver');
    $annualamount=$this->input->post('annualamount');
    $penalties=$this->input->post('penalties');
    $balance=$this->input->post('balance');
    $entitledwaiver=$this->input->post('entitledwaiver');
  	
    $data2=array(
      'plotno'=>$plotno,
      'transid'=>$transid,
      'amountentered'=>$amountentered,
      'minimumamountforwaiver'=>$minimumamountforwaiver,
      'annualamount'=>$annualamount,
      'penalties'=>$penalties,
      'balance'=>$balance,
      'entitledwaiver'=>$entitledwaiver
      );
  	return $data2;
  }

function PrepareSearchPayment(){

      $url = MAIN_URL;
      $username = USER_NAME; //"72386895";
       $key = JP_KEY;// "26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
       $receiptNo =$this->input->post('receipt_no');
       $no = $this->session->userdata['jpwnumber'];//$this->input->post('cust_phone');
       $num = substr($no,-9);
       $number = "254".$num;
       $jpwMobileNo =$number; //"254724972416";
       $agentRef = uniqid();
       $ck = $username.$jpwMobileNo.$agentRef.$receiptNo.$key;
       $pass = sha1(utf8_encode($ck ));
       #$url = "https://10.0.0.1:1501/agencyservices?WSDL";

       $params = array(
           "userName" => $username,
           "agentRef" => $agentRef,
           "receiptNo" => $receiptNo,            
           "jpwMobileNo" => $jpwMobileNo,
           "pass" => $pass
       );

       $client = new SoapClient($url);
       $result = $client->PreparePaymentSearchWalletPPRNCC($params);
       $ResultCode = $result->PreparePaymentSearchWalletPPRNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentSearchWalletPPRNCCResult->Result->ResultText;

       if($ResultCode==0 && $ResultText=="OK" ){
       $name = $result->PreparePaymentSearchWalletPPRNCCResult->NameJPW;
       $transid = $result->PreparePaymentSearchWalletPPRNCCResult->TransactionID;
       $fee = $result->PreparePaymentSearchWalletPPRNCCResult->SearchFee;

       $data1= array(
          "wname" =>$name,
          "transid1"=>$transid,
          "JPnumber"=>$jpwMobileNo,
          "sfee"=>$fee,
          "rescode"=>$ResultCode,
          "restext"=>$ResultText
        );
       #$this->session->set_userdata($data1);
       return $data1;
       }
       else{
        $data1=array('restext'=>$ResultText,'rescode'=>$ResultCode);
             return $data1;
       }

}

function CompleteSearchPayment(){

       $url = MAIN_URL;
       $username = USER_NAME; //"72386895";
       $key = JP_KEY; //"26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";      
       $custMobileNo = $this->session->userdata['jpwnumber']; //$this->input->post('no');
       $amount = "200";
       $amnt=number_format($amount, 2, '.', ',');
       $agentRef = uniqid();  
       $channelRef = "234536";
       $TransactionID = $this->input->post('trans');
       $jpPIN = $this->session->userdata['jp_pin']; //$this->input->post('jp_pin');
       $ck = $username.$TransactionID.$key;
       $pass = sha1(utf8_encode($ck ));
       #$url = "https://41.212.9.57:1501/agencyservices?WSDL";

       $params = array(
           "userName" => $username,
           "transactionId" => $TransactionID,
           "amount" => $amount,
           "currency" => 'KES',
           "jpPIN" => $jpPIN,
           "channelRef" => $channelRef,
           "pass" => $pass
       );

       $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
       $result = $client->CompletePaymentSearchWalletPPRNCC($params);

        $resulttext = $result->CompletePaymentSearchWalletPPRNCCResult->Result->ResultText;
        $resultcode = $result->CompletePaymentSearchWalletPPRNCCResult->Result->ResultCode;
        #$ReceiptNo = $result->CompletePaymentWalletSBPNCCResult->ReceiptNo;

       if ($resultcode==0 && $resulttext=="OK" ){
        $AccumulatedPenalty=$result->CompletePaymentSearchWalletPPRNCCResult->AccumulatedPenalty;
        $Adjustment=$result->CompletePaymentSearchWalletPPRNCCResult->Adjustment;
        $AmountToPay=$result->CompletePaymentSearchWalletPPRNCCResult->AmountToPay;
        $CurrentBalance=$result->CompletePaymentSearchWalletPPRNCCResult->CurrentBalance;
        $CustomerName=$result->CompletePaymentSearchWalletPPRNCCResult->CustomerName;
        $DueDate=$result->CompletePaymentSearchWalletPPRNCCResult->DueDate;
        $GroundRent=$result->CompletePaymentSearchWalletPPRNCCResult->GroundRent;
        $LRNumber=$result->CompletePaymentSearchWalletPPRNCCResult->LRNumber;
        $LandRates=$result->CompletePaymentSearchWalletPPRNCCResult->LandRates;
        $OtherCharges=$result->CompletePaymentSearchWalletPPRNCCResult->OtherCharges;
        $PhysicalAddress=$result->CompletePaymentSearchWalletPPRNCCResult->PPhysicalAddress;
        $PUseDescription=$result->CompletePaymentSearchWalletPPRNCCResult->PUseDescription;
        $PlotNumber=$result->CompletePaymentSearchWalletPPRNCCResult->PlotNumber;
        $TotalAnnualAmount=$result->CompletePaymentSearchWalletPPRNCCResult->TotalAnnualAmount;
        $TotalArrears=$result->CompletePaymentSearchWalletPPRNCCResult->TotalArrears;
        $TransactionID=$result->CompletePaymentSearchWalletPPRNCCResult->TransactionID;
        $UPN=$result->CompletePaymentSearchWalletPPRNCCResult->UPN;
        $Waiver=$result->CompletePaymentSearchWalletPPRNCCResult->Waiver;
        $Commission=$result->CompletePaymentSearchWalletPPRNCCResult->Commission;
        $ReceiptAmount=$result->CompletePaymentSearchWalletPPRNCCResult->ReceiptAmount;
        $ReceiptNo=$result->CompletePaymentSearchWalletPPRNCCResult->ReceiptNo;
        $TransactionDate=$result->CompletePaymentSearchWalletPPRNCCResult->TransactionDate;

              try {
                $phone=$this->session->userdata('jpwnumber');#"254726981598";
                  $ph=substr($phone,-9);
                  $gsm="254".$ph;
                  $msg="A Search Fee of KES $amnt for $CustomerName receipt number $ReceiptNo has been received by Nairobi City County.Powered by: JamboPay";
                  #sms
                  $X_HOST ="http1.uk.oxygen8.com";
                   $X_URL = "/WebTribe";
                   $X_PORT ="8080";
                   $X_USERNAME = "WebTribe";
                   $X_PASSWORD = "web@12";
                   $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
                   $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
                   $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
                   $s_POST_DATA .= "&MSISDN=$gsm"; // Phone
                   $s_POST_DATA .= "&Content=$msg";
                   $s_POST_DATA .= "&DataType=0"; //Data Type
                   $s_POST_DATA .= "&Premium=1"; //Premium
                   $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
                   $s_POST_DATA .= "&Multipart=1";
                   $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
                   $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
                   $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
                   $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
                   $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
                   $s_Request .="\r\n".$s_POST_DATA;

                   
                   $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
                   if(fputs ($fp, $s_Request)){
                       
                   };
                   ///while (!feof($fp)) { $s_GatewayResponse .= fgets ($fp, 128); }
                   fclose ($fp);
              } catch (Exception $e) {
                
              }
                  

                $search=array(
                      "AccumulatedPenalty"=>$AccumulatedPenalty,
                      "Adjustment"=>$Adjustment,
                      "AmountToPay"=>$AmountToPay,
                      "CurrentBalance"=>$CurrentBalance,
                      "CustomerName"=>$CustomerName,
                      "DueDate"=>$DueDate,
                      "GroundRent"=>$GroundRent,
                      "LRNumber"=>$LRNumber,
                      "LandRates"=>$LandRates,
                      "OtherCharges"=>$OtherCharges,
                      "PhysicalAddress"=>$PhysicalAddress,
                      "PUseDescription"=>$PUseDescription,
                      "PlotNumber"=>$PlotNumber,
                      "TotalAnnualAmount"=>$TotalAnnualAmount,
                      "TotalArrears"=>$TotalArrears,
                      "TransactionID"=>$TransactionID,
                      "UPN"=>$UPN,
                      "Waiver"=>$Waiver,
                      "Commission"=>$Commission,
                      "ReceiptAmount"=>$ReceiptAmount,
                      "ReceiptNo"=>$ReceiptNo,
                      "TransactionDate"=>$TransactionDate,
                      "rescode"=>$resultcode,
                      "restext"=>$resulttext
                  );
              return $search;//message="Transaction Has Successfully been completed";
            }else{ 
              return $search=array("rescode"=>$resultcode,"restext"=>$resulttext);
            }
       
       #return $result;
   }


   function printlandreceipt($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/LR_receipt.pdf';
        $receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
    
        $query="select * from landrates where receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();
        $receiptno=$result->receiptno;
        $date=substr($result->issuedate, 0,10); 
        $paidby=$result->paidby;
        $total_amount_due=$result->amountdue;
        $balance_due=($result->amountdue)-($result->amount);
        $plot_owner=$result->plotowner;
        $cashier=$result->cashiername;
        $penalties=$result->penalties;
        $amount=$result->amount;
        $amount = str_replace( ',', '', $amount);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";

        $description= " PLOT NUMBER ".$result->plotno; 


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 250, 478); 
        $page->drawText($amount, 480, 478);
        $page->drawText($amount_in_words, 150, 427); 
        $page->drawText($description, 150, 376);
        $page->drawText($plot_owner, 170, 325);
        $page->drawText($cashier, 127, 38);
        $page->drawText(number_format($penalties,2), 450, 274);
        $page->drawText(number_format($total_amount_due,2), 450, 252);
        $page->drawText(number_format($amount,2), 450, 230);
        $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

    function printlandreceiptNew($receiptno){

        $receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
        #var_dump($receiptno); die();
        $token = $this->session->userdata('token');

        $stream = "landrate";
        $key0 = "ReceiptNumber";
        $value0 = urlencode($receiptno);
        // $key1 = "StartDate";
        // $value1 = "2015-07-10";
        $key1 = "TransactionStatus";
        $value1 = "1";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
        // $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[2].Key=$key2&[2].Value=$value2";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);
        $var = $res['0'];
        #echo"<pre>" ;var_dump($var); die();
        
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/LR_receipt.pdf';
        #$receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
    
        #$query="select * from landrates where receiptno='$receiptno'";
        if($var->Amount>=$var->MinimumAmountForWaiver){
        $Balance = $var->Balance-($var->Amount+$var->EntitledWaiver);
        $waiver = $var->EntitledWaiver;
        }
        else
        {
         $Balance = $var->Balance-$var->Amount;
         $waiver = "0";
        }
        #$query="select * from landrates where receiptno='$receiptno'";
        #$result=$this->db->query($query);
        #$result=$result->row();
        $receiptno=$var->ReceiptNumber;
        $date=$var->TransactionDate; 
        $paidby=$var->Names;
        $total_amount_due=$Balance;//$var->Balance-$var->Amount;
        //$balance_due=$var->Balance;
        $plot_owner=$var->CustomerNames;
        $cashier=$var->Names;
        $penalties=$var->Penalty;
        //$waiver=$var->Waiver;
        //$penalties=$var->Penalty;
        $amount=$var->Amount;
        $amount = str_replace( ',', '', $amount);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";

        $description= " PLOT NUMBER ".$var->RateNumber; 


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 250, 478); 
        $page->drawText(number_format($amount,2), 480, 478);
        $page->drawText($amount_in_words, 150, 427); 
        $page->drawText($description, 150, 376);
        $page->drawText($plot_owner, 170, 325);
        #$page->drawText($cashier, 127, 38);
        $page->drawText(number_format($penalties,2), 450, 274);
        $page->drawText(number_format($waiver,2), 450, 252);
        $page->drawText(number_format($amount,2), 450, 230);
        $page->drawText(number_format($total_amount_due,2), 450, 202);
        //$page->drawText(number_format($balance_due,2), 450, 180);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 622,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

    function checkLprintReceipt(){
          $plotno = $this->input->post('plotno');
          $result= $this->db->query("Select * from landrates where plotno='$plotno' order by issuedate DESC limit 1 ");
            if($result->num_rows()>0){
              $result = $result->row();
              return $result->receiptno;
            }else{
              return redirect("lr/landRsearchdetails/erro1");
            }
    }

    function checkLprintReceiptNew(){
      $plotno = strtoupper($this->input->post('plotno')) ;
      $date = "2015-07-10 00:00:00";// $this->input->post('dateissued');

      $token=$this->session->userdata('token');
      $stream = "landrate";
      $key0 = "RateNumber";
      $value0 = urlencode($plotno);
      $key1 = "StartDate";
      $value1 = $date;
      $key2 = "TransactionStatus";
      $value2 = "1";

      $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      #curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec( $ch );
      $res = json_decode($result); #var_dump($plotno); die();


      if($res['0']->RateNumber==$plotno){
        return $res['0']->ReceiptNumber;
      }else{
        return redirect("lr/landRsearchdetails/erro1");
      }

  }

    function lprintlandreceipt($receiptno){

        $token = $this->session->userdata('token');

        $stream = "landrate";
        $key0 = "ReceiptNumber";
        $value0 = urlencode($receiptno);
        // $key1 = "StartDate";
        // $value1 = "2015-07-10";
        $key1 = "TransactionStatus";
        $value1 = "1";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";
        // $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[2].Key=$key2&[2].Value=$value2";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);
        $var = $res['0'];
        #var_dump($receiptno);die();
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/LR_receipt.pdf';
        // $receiptno = $this->uri->segment(3);
        // $receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
        #var_dump($receiptno); die();
        if($var->Amount>=$var->MinimumAmountForWaiver){
        $Balance = $var->Balance-($var->Amount+$var->EntitledWaiver);
        $waiver = $var->EntitledWaiver;
        }
        else
        {
         $Balance = $var->Balance-$var->Amount;
         $waiver = "0";
        }
        // $result=$result->row();
        #var_dump($Balance."|".$waiver);die();
        $receiptno=$var->ReceiptNumber;
        $date=$var->TransactionDate; 
        $paidby=$var->Names;
        $total_amount_due=$Balance;//$var->Balance-$var->Amount;
        //$balance_due=$var->Balance;
        $plot_owner=$var->CustomerNames;
        $cashier=$var->Names;
        $penalties=$var->Penalty;
        //$waiver=$var->Waiver;
        $plotno=$var->RateNumber;
        $amount=$var->Amount;
        $amount = str_replace( ',', '', $amount);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";

        $description= " PLOT NUMBER - ".$plotno; 


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 150, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 250, 478); 
        $page->drawText(number_format($amount,2), 480, 478);
        $page->drawText($amount_in_words, 150, 427); 
        $page->drawText($description, 150, 376);
        $page->drawText($plot_owner, 170, 325);
        #$page->drawText($cashier, 127, 38);
        $page->drawText(number_format($penalties,2), 450, 274);
        $page->drawText(number_format($waiver,2), 450, 252);
        $page->drawText(number_format($amount,2), 450, 230);
        $page->drawText(number_format($total_amount_due,2), 450, 202);
        //$page->drawText(number_format($balance_due,2), 450, 180);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 622,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

    // function lprintlandreceipt($receiptno){
    //     $this->load->library('zend');
    //     $a=$this->load->library('amount_to_words');
    //     $this->zend->load('Zend/Pdf');
    //     $this->zend->load('Zend/Barcode');

    //     $fileName =APPPATH.'assets/back/receipts/LR_receipt.pdf';
    //     // $receiptno = $this->uri->segment(3);
    //     // $receiptno=str_replace('-', '/',str_replace('_', ' ', $receiptno));
    //     #var_dump($receiptno); die();
    //     $query="select * from landratestest where receiptno='$receiptno'";
    //     $result=$this->db->query($query);
    //     $result=$result->row();
    //     $receiptno=$result->receiptno;
    //     $date=substr($result->issuedate, 0,10); 
    //     $paidby=$result->paidby;
    //     $total_amount_due=$result->amountdue;
    //     $balance_due=($result->amountdue)-($result->amount);
    //     $plot_owner=$result->plotowner;
    //     $cashier=$result->cashiername;
    //     $penalties=$result->penalties;
    //     $amount=$result->amount;
    //     $amount = str_replace( ',', '', $amount);
    //     if( is_numeric( $amount) ) {
    //         $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
    //     }else $amount_in_words="NOT AVAILABLE";

    //     $description= " PLOT NUMBER ".$result->plotno; 


    //     $pdf = Zend_Pdf::load($fileName);
    //     $page=$pdf->pages[0];

    //     $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

    //     $page->drawText($receiptno, 150, 528);
    //     $page->drawText($date, 450, 528);
    //     $page->drawText($paidby, 250, 478); 
    //     $page->drawText($amount, 480, 478);
    //     $page->drawText($amount_in_words, 150, 427); 
    //     $page->drawText($description, 150, 376);
    //     $page->drawText($plot_owner, 170, 325);
    //     $page->drawText($cashier, 127, 38);
    //     $page->drawText(number_format($penalties,2), 450, 274);
    //     $page->drawText(number_format($total_amount_due,2), 450, 252);
    //     $page->drawText(number_format($amount,2), 450, 230);
    //     $page->drawText(number_format($balance_due,2), 450, 209);

    //     $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
    //     $rendererOptions = array(
    //         'topOffset' => 600,
    //         'leftOffset' =>295
    //         );
    //     $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


    //     $pdfData = $pdf->render(); 
    //     header("Content-Disposition: inline; filename=receipt.pdf"); 
    //     header("Content-type: application/x-pdf"); 
    //     echo $pdfData;
    //     //redirect('en/land_rates');
    // }

    function insertReceiptDetails(){
    $plotnum = $this->input->post('plotnum');
    $plotowner = $this->input->post('plotowner');
    $date = $this->input->post('issuedate');
    $date1 =substr($date,0,10); //date("Y-m-d",$date); 
    $receiptno = $this->input->post('receiptno');
    // $regno = $this->input->post('regno');
    // $regno = strtoupper($regno);
    $paidby = $this->input->post('paidby');
    $paidby = mysql_real_escape_string($paidby);
    $amount = $this->input->post('amount');
    $amountdue = $this->input->post('amountdue');
    $penalties = $this->input->post('penalties');
    $phoneno = $this->input->post('phonenumber');
    $channel = "Manual";
    #var_dump($date1); die();
    try {
     $data=array('receiptno'=>$receiptno,'issuedate'=>$date1,'paidby'=>$paidby,'amount'=>$amount,'plotno'=>$plotnum,'plotowner'=>$plotowner,'penalties'=>$penalties,'amountdue'=>$amountdue,'username' =>$phoneno,'cashiername' =>$paidby,'channel' =>$channel);
    $query = "insert into landrates (`receiptno`,`issuedate`,`paidby`,`amount`,`plotno`,`plotowner`,`penalties`,`amountdue`,`username`,`cashiername`,`channel`)";
    $query.= " values ('".implode("','",$data)."')"; #var_dump($data);die();
    $this->db->query($query);
                redirect('lr/landRsearchdetails');
    } catch (Exception $e) {
      redirect('lr/ManualLandReceiptInsert');
    }
  }

}