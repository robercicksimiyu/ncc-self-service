<?php

class Health_model extends CI_Model {

	function inputFormDetails(){

		$url="http://54.218.79.241/mainsector.asmx?WSDL";

		$Fullname = $this->input->post('full_names');
		$mail = $this->input->post('email');
		$Tel_no = $this->input->post('custphone');
		$Firm = $this->input->post('applicant_name');
		$Occupation = $this->input->post('occupation');
		$Owner = $this->input->post('owner');
		$PlotNo = $this->input->post('plot_no');
		$LRNO = $this->input->post('LR_no');
		$Frontingon = $this->input->post('frontingon');
		$Building = $this->input->post('building');
		$FloorNo = $this->input->post('floor');

		$serviceArguments = array(
			"Fullname"=>$Fullname,
			"mail"=>$mail,
			"Tel_no"=>$Tel_no,
			"Firm"=>$Firm,
			"Occupation"=>$Occupation,
			"Owner"=>$Owner,
			"PlotNo"=> $PlotNo,
			"LRNO"=> $LRNO,
			"Frontingon"=> $Frontingon,
			"Building"=> $Building,
			"FloorNo"=> $FloorNo
			);
		
		try {
		$client = new SoapClient($url);
		$result = $client->PostFoodHygeneApplication($serviceArguments); #var_dump($result); die();
		$RefID = $result->PostFoodHygeneApplicationResult->string["0"];	
		} catch (Exception $e) {
		redirect('health/APIerror');
		}
		
		
		if($RefID!="400"){

		$serviceArguments2 = array(
			"Refid"=>$RefID
			);
		$client2 = new SoapClient($url);
		$result2 = $client2->GetApplicationDetails($serviceArguments2);

		#$Fullname = $result2->GetApplicationDetailsResult->Fullname;
		$Email = $result2->GetApplicationDetailsResult->Email;
		$Firm = $result2->GetApplicationDetailsResult->Firm;
		$Occupation = $result2->GetApplicationDetailsResult->Occupation;
		$Owner = $result2->GetApplicationDetailsResult->Owner;
		$PlotNo = $result2->GetApplicationDetailsResult->PlotNo;
		$LRNO = $result2->GetApplicationDetailsResult->LRNO;
		$Frontingon = $result2->GetApplicationDetailsResult->Frontingon;
		$Building = $result2->GetApplicationDetailsResult->Building;
		$FloorNo = $result2->GetApplicationDetailsResult->FloorNo;
		$Status = $result2->GetApplicationDetailsResult->Status;
		$Amount = $result2->GetApplicationDetailsResult->Amount;
		$TelNumber = $result2->GetApplicationDetailsResult->TelNumber;#var_dump($Amount); die();
		
		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Email"=>$Email,
			"TelNumber"=>$TelNumber,
			"Firm"=>$Firm,
			"Occupation"=>$Occupation,
			"Owner"=>$Owner,
			"PlotNo"=> $PlotNo,
			"LRNO"=> $LRNO,
			"Frontingon"=> $Frontingon,
			"Building"=> $Building,
			"FloorNo"=> $FloorNo,
			"Status"=>$Status,
			"Amount"=>$Amount
			);
        
        try {
        $this->db->insert('institutionhealth',$data);	
        } catch (Exception $e) {
        	
        }
        
        return $data;
    }else{
    	redirect('health/error');
    }
	}

	function completeHealthPayment(){

		//$url="http://54.218.79.241/mainsector.asmx?WSDL";

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"Occupation" => $this->input->post('occupation'),
		"Owner" => $this->input->post('owner'),
		"PlotNo" => $this->input->post('plotno'),
		"LRNO" => $this->input->post('lrno'),
		"Frontingon" => $this->input->post('frontingon'),
		"Building" => $this->input->post('building'),
		"FloorNo" => $this->input->post('floorno'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount')
		);
		
		// $serviceArguments = array(
		// 	"Refid"=> "NCC22"//$this->input->post('refid')
		// 	);
		// $client = new SoapClient($url);
		// $result = $client->GetApplicationDetails($serviceArguments);
		return $data;
	}

	function inputInstitutionFormDetails(){

		$url="http://54.218.79.241/mainsector.asmx?WSDL";

		$Fullname = $this->input->post('full_names');
		$institutionname = $this->input->post('institution_name');
		$address = $this->input->post('address');
		$email = $this->input->post('email');
		$plotno = $this->input->post('plot_no');
		$road = $this->input->post('road');
		$mobile = $this->input->post('mobile');
		$purpose = $this->input->post('purpose');
		$nature = $this->input->post('nature');

		$serviceArguments = array(
			"Fullname"=>$Fullname,
			"Institution_name"=>$institutionname,
			"Address"=>$address,
			"mail"=>$email,
			"Plot_no"=>$plotno,
			"Road"=>$road,
			"Mobile"=> $purpose,
			"Nature"=> $nature
			);
		
		try {
		$client = new SoapClient($url);
		$result = $client->PostInstitutionLearning($serviceArguments); #var_dump($result); die();
		$RefID = $result->PostInstitutionLearningResult->string["0"];	
		} catch (Exception $e) {
		redirect('health/APIerror');
		}
		
		
		if($RefID!="400"){

		$serviceArguments2 = array(
			"Refid"=>$RefID
			);
		$client2 = new SoapClient($url);
		$result2 = $client2->GetInstituionDetails($serviceArguments2);

		#$Fullname = $result2->GetApplicationDetailsResult->Fullname;
		$Email = $result2->GetInstituionDetailsResult->Email;
		$Name = $result2->GetInstituionDetailsResult->Name;
		$Address = $result2->GetInstituionDetailsResult->Address;
		$Plot_no = $result2->GetInstituionDetailsResult->Plot_no;
		$Road = $result2->GetInstituionDetailsResult->Road;
		$Mobile = $result2->GetInstituionDetailsResult->Mobile;
		$Purpose = "BUSINESS";//$result2->GetInstituionDetailsResult->Purpose;
		$Nature = $result2->GetInstituionDetailsResult->Nature;
		$Status = $result2->GetInstituionDetailsResult->Status;
		$Amount = $result2->GetInstituionDetailsResult->Amount;
		
		$data = array(
			"RefID"=>$RefID,
			"issueDate"=>date("Y/m/d"),
			"Fullname"=>$Fullname,
			"Name"=>$Name,
			"Email"=>$Email,
			"PlotNo"=> $Plot_no,
			"Mobile"=>$Mobile,
			"Purpose"=>$Purpose,
			"Nature"=>$Nature,
			"Address"=> $Address,
			"Status"=>$Status,
			"Amount"=>$Amount
			);
        
        try {
        $this->db->insert('healthinstitution',$data);	
        } catch (Exception $e) {
        	
        }
        return $data;
    }else{
    	redirect('health/error');
    }
	}

	function completeInstitutionHealthPayment(){

		//$url="http://54.218.79.241/mainsector.asmx?WSDL";

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"InstitutionName" => $this->input->post('name'),
		"Mobile" => $this->input->post('mobile'),
		"Purpose" => $this->input->post('purpose'),
		"PlotNo" => $this->input->post('plotno'),
		"Nature" => $this->input->post('nature'),
		"Address" => $this->input->post('address'),
		"Email" => $this->input->post('email'),
		"RefID" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"Status"=> $this->input->post('status')
		);
		
		// $serviceArguments = array(
		// 	"Refid"=> "NCC22"//$this->input->post('refid')
		// 	);
		// $client = new SoapClient($url);
		// $result = $client->GetApplicationDetails($serviceArguments);
		return $data;
	}

	function printHealthPermit($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Healthpermit.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from institutionhealth where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$Tel_no = $result->TelNumber;
		$Firm = $result->Firm;
		$Occupation =$result->Occupation;
		$Owner = $result->Owner;
		$PlotNo = $result->PlotNo;
		$LRNO = $result->LRNO;
		$Frontingon = $result->Frontingon;
		$Building = $result->Building;
		$FloorNo = $result->FloorNo;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Fullname), 210, 440);#var_dump($vals); die();
        $page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        $page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Occupation), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printHealthPermitreceipt($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/healthreceipt.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from institutionhealth where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$Tel_no = $result->TelNumber;
		$Firm = $result->Firm;
		$Occupation =$result->Occupation;
		$Owner = $result->Owner;
		$PlotNo = $result->PlotNo;
		$LRNO = $result->LRNO;
		$Frontingon = $result->Frontingon;
		$Building = $result->Building;
		$FloorNo = $result->FloorNo;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";
		$issueDate = $result->issueDate;
		$description= "APPLICATION FOR $Fullname , PERMIT ID - ".$Refid;
		if( is_numeric( $Amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($Amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";
        $from = $this->session->userdata('name');

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
          $page->drawText($Receiptno, 150, 528);
        $page->drawText($expiryDate, 480, 528);
        $page->drawText($from, 250, 480);
        $page->drawText($from, 127, 38); 
        $page->drawText(number_format($Amount,2), 480, 480);
        $page->drawText($amount_in_words, 150, 437); 
        $page->drawText($description, 150, 390);
        // $page->drawText(number_format($sbpfee,2), 450, 270);
        // $page->drawText(number_format($regfees,2), 450, 250);
        // $page->drawText(number_format($amount,2), 450, 225);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printInstitutionHealthPermit($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Healthpermit.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from healthinstitution where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$PlotNo = $result->PlotNo;
		$Purpose =$result->Purpose;
		$Nature = $result->Nature;
		$Mobile = $result->Mobile;
		$Address = $result->Address;
		#$Frontingon = $result->Frontingon;
		#$Building = $result->Building;
		#$FloorNo = $result->FloorNo;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); #var_dump($refid); die();

        $page->drawText(strtoupper($Fullname), 210, 440);
        $page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($PlotNo), 190, 414);
        $page->drawText(strtoupper($Address), 400, 412); 
        $page->drawText(strtoupper($Purpose), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printInstitutionHealthPermitreceipt($refid){
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/healthreceipt.pdf';
        $refid = $this->uri->segment(3);
        $query="select * from healthinstitution where RefID='$refid'";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
		$Email = $result->Email;
		$Name = $result->Name;
		$Mobile = $result->Mobile;
		$Purpose = $result->Purpose;
		$PlotNo = $result->PlotNo;
		$Nature = $result->Nature;
		$Address = $result->Address;
		$Refid = $result->RefID;
		$Amount = $result->Amount;
		$Receiptno = "111122333/002588";
		$expiryDate = "2016-02-02";
		$issueDate = $result->issueDate;
		$description= "APPLICATION FOR $Fullname , PERMIT ID - ".$Refid;
		if( is_numeric( $Amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($Amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";
        $from = $this->session->userdata('name');

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
          $page->drawText($Receiptno, 150, 528);
        $page->drawText($expiryDate, 480, 528);
        $page->drawText($from, 250, 480);
        $page->drawText($from, 127, 38); 
        $page->drawText(number_format($Amount,2), 480, 480);
        $page->drawText($amount_in_words, 150, 437); 
        $page->drawText($description, 150, 390);
        // $page->drawText(number_format($sbpfee,2), 450, 270);
        // $page->drawText(number_format($regfees,2), 450, 250);
        // $page->drawText(number_format($amount,2), 450, 225);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
	}

	function printInstitution(){
		$url="http://54.218.79.241/mainsector.asmx?WSDL";

		$Refid=strtoupper($this->input->post('refid')) ;
		$data = array("Refid"=>$Refid);

		try {
		$client = new SoapClient($url);
		$result = $client->PrintInstituionDetails($data);	
		} catch (Exception $e) {
			redirect('health/APIerror');
		}
		$result = $client->PrintInstituionDetails($data);
		$Name=$result->PrintInstituionDetailsResult->Name;
		$Institution_name=$result->PrintInstituionDetailsResult->Institution_name;
		$Address=$result->PrintInstituionDetailsResult->Address;
		$Email=$result->PrintInstituionDetailsResult->Email;
		$Plot_no=$result->PrintInstituionDetailsResult->Plot_no;
		$Road=$result->PrintInstituionDetailsResult->Road;
		$Mobile=$result->PrintInstituionDetailsResult->Mobile;
		$Purpose=$result->PrintInstituionDetailsResult->Purpose;
		$Nature=$result->PrintInstituionDetailsResult->Nature;
		$Status=$result->PrintInstituionDetailsResult->Status;
		$Amount=$result->PrintInstituionDetailsResult->Amount;
		#return $result;

		$printdata = array(
		"Refid"=>$Refid,
		"Name"=>$Name,
		"Institution_name"=>$Institution_name,
		"Address"=>$Address,
		"Email"=>$Email,
		"Plot_no"=>$Plot_no,
		"Road"=>$Road,
		"Mobile"=>$Mobile,
		"Purpose"=>"BUSINESS",//$Purpose,
		"Nature"=>$Nature,
		"Status"=>$Status,
		"Amount"=>$Amount
		);
		return $printdata;
		
	}

}
