<?php
class Common_model extends CI_Model{
    function __construct()
    {
        $this->load->library('amount_to_words');
        $this->load->library('upload');
    }

    public function get_wards($sub_county_id)
    {
        $this->db->where('ZoneID',$sub_county_id);
        return $this->db->get('wards')->result_array();
    }

    public function getZones()
    {
        return $this->db->get('zones')->result_array();
    }
    

    public function getWards()
    {
        return $this->db->get('wards')->result_array();
    }

    function getWardsAPI($countyId)
    {
        $results = curl_rest_client("getsbpwards?stream=sbp&id=$countyId",'GET');
        return json_decode($results,true);
    }

    function getSubCountiesAPI()
    {
        $results = curl_rest_client("getsbpsubcounties?stream=sbp",'GET');
        return json_decode($results,true);
    }

    public function upload_files($custom_upload_path,$field_name)
    {
        $upload_details=array();
        $path='./uploads/'.$custom_upload_path;
        $config = array();
        $config['upload_path']  = $path;
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;
        $namesArray = [];
        $files = $_FILES;

        $cpt = count($_FILES[$field_name]['name']);
        for($i=0; $i<$cpt; $i++) {
            $namesArray[$i] = $files[$field_name]['name'][$i];
            $_FILES[$field_name]['name']= $files[$field_name]['name'][$i];
            $_FILES[$field_name]['type']= $files[$field_name]['type'][$i];
            $_FILES[$field_name]['tmp_name']= $files[$field_name]['tmp_name'][$i];
            $_FILES[$field_name]['error']= $files[$field_name]['error'][$i];
            $_FILES[$field_name]['size']= $files[$field_name]['size'][$i];
            $this->upload->initialize($config);
            if($this->upload->do_upload($field_name)) {
                $upload_details[]=$this->upload->data();
            } else {
               return $this->upload->display_errors();
            }
        }
        return $upload_details;
    }

    public function login($credentails)
    {
        $res = json_decode(curl_rest_client(TOKEN_URL,'POST',$credentails,true,false,false),true);
        $details=array();
        if(isset($res['access_token'])){
            $details = array(
                "name"=>(string)$res['names'],
                "response"=>'OK',
                "balanc"=>(string)$res['balance'],
                "jp_pin"=>$credentails['password'],
                "RESPONSE_CODE"=>0,
                "jpwnumber"=>$credentails['username'],
                "token"=>$res['access_token'],
            );
            $this->session->set_userdata($details); #var_dump($details); die();

        }else{
            $details = array("response"=>$res->invalid_grant);
        }
        return $details;


    }

    function getBusinessDetails($BusinessID,$Year)
    {
        $token = $this->session->userdata('token');
        $PhoneNumber=$this->session->userdata('jpwnumber');
        $url = MIDDLEWARE."/GetBusiness?stream=sbp&LicenseID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));
        $result = curl_exec( $ch );
        return json_decode($result,true);
    }

}





