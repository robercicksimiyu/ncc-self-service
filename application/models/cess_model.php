<?php

class Cess_model extends CI_Model{

	function dcinitial(){
		$username = USER_NAME;
		$key = JP_KEY;
		$url = MAIN_URL;

		$timestamp = date("Y-m-d H:i:s"); 
		$ck = $username . $timestamp . $key;
		$pass = sha1(utf8_encode($ck ));

		$params = array(
			"userName" => $username,
			"timestamp" => $timestamp,         
			"pass" => $pass

			);

		try {
			$client = new SoapClient($url);
			$result = $client->PreparePaymentCESSInitDataCESSNCC($params);
		} catch (Exception $e) {
			redirect('sbp/not_found');
		}

		$ResultCode = $result->PreparePaymentCESSInitDataCESSNCCResult->Result->ResultCode;
		$ResultText = $result->PreparePaymentCESSInitDataCESSNCCResult->Result->ResultText;
		$cessCodes = $result->PreparePaymentCESSInitDataCESSNCCResult->Charges->{"NCC.CESSCharges"};
		#$cessDescription = $result->PreparePaymentCESSInitDataCESSNCCResult->Charges->{"NCC.CESSDescription"};
		#$cess=array('cessCodes'=>$cessCodes,'cessDescription'=>$cessDescription);

		return $cessCodes;
	}


	function dcprepare(){
		$username = USER_NAME;
		$key = JP_KEY;
		$url = MAIN_URL;

		$CESSCode = $this->input->post('cessCode');
		$cess = $this->input->post('cessDescription');
		$jpwMobileNo = $this->session->userdata('jpwnumber');
		$custNatId = $this->input->post('idno');
		$location = $this->input->post('location');
		$agentRef = strtoupper(substr(md5(uniqid()),25));  
		$ck = $username . $agentRef . $CESSCode . $jpwMobileNo . $key;
		$pass = sha1(utf8_encode($ck ));
		#$name = $this->session->userdata('name');

		$params = array(
			"userName"=>$username,
			"agentRef"=>$agentRef,
			"CESSCode"=>$CESSCode,      
			"jpwMobileNo"=>$jpwMobileNo,
			"custNatId"=>$custNatId,
			"location"=>$location,     
			"pass"=> $pass

			);

		try {
			$client = new SoapClient($url);
			$result = $client->PreparePaymentWalletDailyCESSNCC($params);
		} catch (Exception $e) {
			redirect('sbp/not_found');
		}

		$ResultCode = $result->PreparePaymentWalletDailyCESSNCCResult->Result->ResultCode;
		$ResultText = $result->PreparePaymentWalletDailyCESSNCCResult->Result->ResultText;
		
		$transactionid = $result->PreparePaymentWalletDailyCESSNCCResult->TransactionID;
        $name = $result->PreparePaymentWalletDailyCESSNCCResult->NameJPW;
        $fee = $result->PreparePaymentWalletDailyCESSNCCResult->CESSFee;

        $details = array(
        	'name'=>$name,
        	'phone'=>$jpwMobileNo,
        	'idno'=>$custNatId,
        	'location'=>$location,
        	'cess'=>$cess,
        	'amount'=>$fee,
        	'transactionid'=>$transactionid,
        	'rescode'=>$ResultCode,
        	'restext'=>$ResultText
        	);
		
		return $details;

	}


	function dccomplete(){
		$username = USER_NAME;
		$key = JP_KEY;
		$url = MAIN_URL;

	    $tranid = $this->input->post('transid'); //"13168"; //1284101
	    $channelRef = "458752344";  
	    $amount = $this->input->post('amount');
	    $currency = "KES";
	    $channel = "Selfservice";
	    $jpPIN = $this->input->post('jp_pin');
	    $cess = $this->input->post('cess');
	    $location = $this->input->post('location');
	    $idno = $this->input->post('idno');

	    $ck = $username . $tranid . $key;
	    $pass = sha1(utf8_encode($ck ));

	    $params = array(
	    	"userName"=>$username,
	    	"transactionId"=>$tranid,
	    	"jpPIN"=>$jpPIN,     
	    	"amount"=>$amount, 
	    	"currency"=> $currency,
	    	"channelRef"=> $channelRef,
	    	"pass"=> $pass

	    	);

	    try {
			$client = new SoapClient($url);
			$result = $client->CompletePaymentWalletDailyCESSNCC($params);
		} catch (Exception $e) {
			redirect('sbp/not_found');
		}

        $ResultCode = $result->CompletePaymentWalletDailyCESSNCCResult->Result->ResultCode;
        $ResultText = $result->CompletePaymentWalletDailyCESSNCCResult->Result->ResultText;

        if($ResultCode==0&&$ResultText=='OK'){
        $paid = $result->CompletePaymentWalletDailyCESSNCCResult->Paid;
        $receiptno = $result->CompletePaymentWalletDailyCESSNCCResult->ReceiptNo;

        #set in db
        // try {
        //       $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'regno'=>$carreg,'category'=>$type,'zone'=>$zone,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$customer,'channel'=>$channel);

        //         $query = "insert into `dailyparking` (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`,`channel`)";
        //         $query.= " values ('".implode("','",$data)."')";
        //         $this->db->query($query);
              
        //     } catch (Exception $e) {}
        #end set in db
        try {

          $ph= $this->session->userdata('jpwnumber');//substr($phone,-9);"254726981598"; //
            //$gsm="254".$ph;

            $amnt= number_format($amount, 2, '.', ',');
            $msg="Receipt Number $receiptno.Your payment of KES $amnt for $cess CESS has been received by Nairobi City County.Powered by: JamboPay";
            $X_HOST ="http1.uk.oxygen8.com";
            $X_URL = "/WebTribe";
            $X_PORT ="8080";
            $X_USERNAME = "WebTribe";
            $X_PASSWORD = "web@12";
                   $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
                   $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
                   $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
                   $s_POST_DATA .= "&MSISDN=$ph"; // Phone
                   $s_POST_DATA .= "&Content=$msg";
                   $s_POST_DATA .= "&DataType=0"; //Data Type
                   $s_POST_DATA .= "&Premium=1"; //Premium
                   $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
                   $s_POST_DATA .= "&Multipart=1";
                   $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
                   $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
                   $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
                   $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
                   $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
                   $s_Request .="\r\n".$s_POST_DATA;

                   
                   $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
                   if(fputs ($fp, $s_Request)){

                   };
                   ///while (!feof($fp)) { $s_GatewayResponse .= fgets ($fp, 128); }
                   fclose ($fp);
        } catch (Exception $e) {
          
        }

        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText,
          'paid'=>$paid,
          'receiptno'=>$receiptno,
          'customer'=>$this->session->userdata('name')
          );
        return $data;
      }
      else {
        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $data;
      }

}

























}