<?php

class Parking_model extends CI_Model{

    function alljpparkingtransactions(){
    $sql = "SELECT * FROM seasonalparking  ";
    $result = $this->db->query($sql);
    #var_dump($result);die();
    return $result;
}


  function prepareDailyParkingInit(){
    $username = USER_NAME;
    $key = JP_KEY;

    $url = MAIN_URL;

    $timestamp = date("Y-m-d H:i:s");
    $ck = $username.$timestamp.$key;
    $pass = sha1(utf8_encode($ck));

    $params = array(
     "userName" => $username,
     "timestamp" => $timestamp,
     "pass" => $pass
     );

    $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
    $result = $client->PreparePaymentDailyParkingInitDataPCKNCC($params);
    $ResultCode = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->Result->ResultCode;
    $ResultText = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->Result->ResultText;

    if ($ResultCode==0&&$ResultText=='OK') {
      $zones = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->ParkingZones->{"NCC.ZoneCodes"};
      $VehicleType = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->VehicleType->{"NCC.VehicleType"};
      $parking=array('zones'=>$zones,'VehicleType'=>$VehicleType);
      //  echo "<pre>";var_dump($parking); die();
      return $parking;
    }


  }

    function prepareDailyParkingInitNew(){

    $token = $this->session->userdata('token');
    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/GetDailyParkingItems?stream=parking");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer '.$token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));
    // execute the request
    $result = curl_exec($ch);
    curl_close($ch);
    $res = json_decode($result);
    $ParkingZones = $res->ParkingZones;
    $PaymentTypes = $res->PaymentTypes;
    return $parking=array('zones'=>$ParkingZones,'VehicleType'=>$PaymentTypes);
    #echo "<pre>";var_dump($ParkingZones);var_dump($PaymentTypes); die();
  }

    function prepareDailyParkingPayment(){

      $username = USER_NAME;
      $key = JP_KEY;
      $url = MAIN_URL;

       $reg = $this->input->post('regno');
       $regnum = str_replace(' ', '', $reg);
       $registrationNo = strtoupper($regnum);
       $vehicleTypeCode = $this->input->post('vehicleType');
       $zoneCode = $this->input->post('ZoneCode');
       $jpwMobileNo = $this->session->userdata('jpwnumber');
       $agentRef = strtoupper(substr(md5(uniqid()),25));
       $zone = $this->input->post('ZoneDescription');
       $type = $this->input->post('VehicleDescription');

       $ck = $username.$jpwMobileNo.$vehicleTypeCode.$zoneCode.$agentRef.$registrationNo.$key;
       $pass = sha1(utf8_encode($ck));

       $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         "zoneCode"=> $zoneCode,
         "jpwMobileNo"=> $jpwMobileNo,
         "pass"=> $pass

       );


       $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
       $result = $client->PreparePaymentDailyWalletPCKNCC($serviceArguments);
       $ResultCode = $result->PreparePaymentDailyWalletPCKNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentDailyWalletPCKNCCResult->Result->ResultText;

       #if($ResultCode==0&&$ResultText=='OK'){
        $transactionid = $result->PreparePaymentDailyWalletPCKNCCResult->TransactionID;
        $name = $result->PreparePaymentDailyWalletPCKNCCResult->NameJPW;
        $fee = $result->PreparePaymentDailyWalletPCKNCCResult->PCKFee;
        $regno = $result->PreparePaymentDailyWalletPCKNCCResult->RegistrationNo;

        $details = array(
          'zone'=>$zone,
          'type'=>$type,
          'transid'=>$transactionid,
          'name'=>$name,
          'fee'=>$fee,
          'regno'=>$regno,
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $details;
       #}


    }

    function prepareDailyParkingPaymentNew(){

       $token = $this->session->userdata('token');
       $reg = $this->input->post('regno');
       $regnum = str_replace(' ', '', $reg);
       $registrationNo = strtoupper($regnum);
       $vehicleTypeCode = $this->input->post('vehicleType');
       $zoneCode = $this->input->post('ZoneCode');
       $jpwMobileNo = $this->session->userdata('jpwnumber');
       $zone = $this->input->post('ZoneDescription');
       $type = $this->input->post('VehicleDescription');

       $tosend = array(
            'IsDaily'=>"true",
            'PlateNumber'=>$registrationNo,
            'ChargeID'=>$vehicleTypeCode,//"2014",
            'PhoneNumber' =>$jpwMobileNo,
            'ZoneCodeID' =>$zoneCode,
            'Stream' =>"parking",
            'PaymentTypeID' => "1"
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments/post";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch); #var_dump($res); die();

        if(isset($res->TransactionID)){
        $transactionid = $res->TransactionID;
        $name = $res->Names;
        $fee = $res->Amount;
        $regno = $res->PlateNumber;

        $details = array(
          'zone'=>$zone,
          'type'=>$type,
          'transid'=>$transactionid,
          'name'=>$name,
          'fee'=>$fee,
          'regno'=>$regno,
          'rescode'=>"0"
          #'restext'=>$ResultText
          );
        return $details;
      }else{#var_dump($data);die();
      // foreach ($res as $keys => $value) {
      //     #echo $keys;
      // }
      return $details = array('rescode'=>$res->ErrorCode);
    }
       #}


    }

    function completeDailyParking(){

      $username = USER_NAME;
      $key = JP_KEY;
      $url = MAIN_URL;

       $tranid = $this->input->post('transid');
       $amount = $this->input->post('amount');
       $zone = $this->input->post('zone');
       $type = $this->input->post('type');
       $carreg = $this->input->post('reg');
       $channelRef = strtoupper(substr(md5(uniqid()),25));
       $currency = "KES";
       $jpPIN = $this->input->post('jp_pin');
       $customer = $this->session->userdata('name');
       $customer = mysql_real_escape_string($customer);
       $channel = "selfservice";

       $ck = $username . $tranid . $key;
       $pass = sha1(utf8_encode($ck ));

       $serviceArguments = array(
         "userName"=>$username,
         "transactionId"=>$tranid,
         "jpPIN"=>$jpPIN,
         "amount"=>$amount,
         "currency"=> $currency,
         "channelRef"=> $channelRef,
         "pass"=> $pass
         );

        $client = new SoapClient($url);
        $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
        $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
        $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
        if($ResultCode==0&&$ResultText=='OK'){
        $paid = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Paid;
        $receiptno = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;
        #set in db
        try {
              $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'regno'=>$carreg,'category'=>$type,'zone'=>$zone,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$customer,'channel'=>$channel);

                $query = "insert into dailyparking (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`,`channel`)";
                $query.= " values ('".implode("','",$data)."')";
                $this->db->query($query);

            } catch (Exception $e) {}
        #end set in db
        try {

          $tel = $this->session->userdata('jpwnumber');
          $amnt= number_format($amount, 2, '.', ',');
          $msg ="Receipt Number $receiptno.Your payment of KES $amnt for Parking of $carreg has been received by Nairobi City County.Powered by: JamboPay";
          $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

          $APIKey = urlencode($key);
          $Phone = urlencode($tel);
          $relayCode = urlencode("WebTribe");
          $Message = urlencode($msg);
          $Shortcode = urlencode("700273");
          $CampaignId = urlencode("112623");
          $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
          $ch=curl_init();
          curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
          $result = curl_exec($ch);
          curl_close($ch);

        } catch (Exception $e) {

        }

        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText,
          'paid'=>$paid,
          'receiptno'=>$receiptno,
          'customer'=>$this->session->userdata('name')
          );
        return $data;
      }
      else {
        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $data;
      }
    }

    function completeDailyParkingNew(){

       $token = $this->session->userdata('token');
       $tranid = $this->input->post('transid');
       $amount = $this->input->post('amount');
       $zone = $this->input->post('zone');
       $type = $this->input->post('type');
       $carreg = $this->input->post('reg');
       $channelRef = strtoupper(substr(md5(uniqid()),25));
       $currency = "KES";
       $jpPIN = $this->input->post('jp_pin');
       $customer = $this->session->userdata('name');
       $jpwMobileNo = $this->session->userdata('jpwnumber');
       $customer = mysql_real_escape_string($customer);
       $channel = "selfservice";

       $tosend = array(
            'PhoneNumber' =>$jpwMobileNo,
            'TransactionID' =>$tranid,
            'Stream' =>"parking",
            'Pin' => $jpPIN
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch); #var_dump($res); die();

        if(isset($res->ReceiptNumber)){
        #set in db
        // try {
        //       $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'regno'=>$carreg,'category'=>$type,'zone'=>$zone,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$customer,'channel'=>$channel);

        //         $query = "insert into dailyparking (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`,`channel`)";
        //         $query.= " values ('".implode("','",$data)."')";
        //         $this->db->query($query);

        //     } catch (Exception $e) {}
        // #end set in db
        // try {

        //   $tel = $this->session->userdata('jpwnumber');
        //   $amnt= number_format($amount, 2, '.', ',');
        //   $msg ="Receipt Number $receiptno.Your payment of KES $amnt for Parking of $carreg has been received by Nairobi City County.Powered by: JamboPay";
        //   $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

        //   $APIKey = urlencode($key);
        //   $Phone = urlencode($tel);
        //   $relayCode = urlencode("WebTribe");
        //   $Message = urlencode($msg);
        //   $Shortcode = urlencode("700273");
        //   $CampaignId = urlencode("112623");
        //   $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
        //   $ch=curl_init();
        //   curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
        //   curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //   $result = curl_exec($ch);
        //   curl_close($ch);

        // } catch (Exception $e) {

        // }

        $data = array(
          'rescode'=>"0",
          'restext'=>"OK",
          'paid'=>$amount,
          'regno'=>$carreg,
          'receiptno'=>$res->ReceiptNumber,
          'customer'=>$this->session->userdata('name')
          );
        return $data;
      }else {
        // foreach ($res as $keys => $value) {
        //   #echo $keys;
        //   }
        $data = array('rescode'=>$res->ErrorCode);
        return $data;
      }
    }

    function prepareSeasonalParkingInit(){
    $username = USER_NAME;
    $key = JP_KEY;
    $url = MAIN_URL;
    $timestamp = date("Y-m-d H:i:s");
    $ck = $username.$timestamp.$key;
    $pass = sha1(utf8_encode($ck));

    $params = array(
     "userName" => $username,
     "timestamp" => $timestamp,
     "pass" => $pass
     );

    $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
    $result = $client->PreparePaymentSeasonalParkingInitDataPCKNCC($params);
    $ResultCode = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->Result->ResultCode;
    $ResultText = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->Result->ResultText;

    if ($ResultCode==0&&$ResultText=='OK') {
      $duration = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->ParkingDuration->{"NCC.ParkingDuration"};
      $VehicleType = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->VehicleType->{"NCC.VehicleType"};
      $parking=array('duration'=>$duration,'VehicleType'=>$VehicleType);
      return $parking;
    }


  }

    function prepareSeasonalParkingPayment(){

      $username = USER_NAME;
      $key = JP_KEY;
      $url = MAIN_URL;

      $durationCode = $this->input->post('duration');
       $reg = $this->input->post('regno');
       $regnum = str_replace(' ', '', $reg);
       $registrationNo = strtoupper($regnum);
       $vehicleTypeCode = $this->input->post('type');
       #$zoneCode = "10";
       $jpwMobileNo = $this->session->userdata('jpwnumber');
       $agentRef = strtoupper(substr(md5(uniqid()),25));

       $ck = $username.$jpwMobileNo.$vehicleTypeCode.$durationCode.$agentRef.$registrationNo.$key;
       $pass = sha1(utf8_encode($ck));

       $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         #"zoneCode"=> $zoneCode,
         "durationCode"=>$durationCode,
         "jpwMobileNo"=> $jpwMobileNo,
         "pass"=> $pass

       );

       $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
       $result = $client->PreparePaymentSeasonalWalletPCKNCC($serviceArguments);
       $ResultCode = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultText;

       #if($ResultCode==0&&$ResultText=='OK'){
        $transactionid = $result->PreparePaymentSeasonalWalletPCKNCCResult->TransactionID;
        $name = $result->PreparePaymentSeasonalWalletPCKNCCResult->NameJPW;
        $fee = $result->PreparePaymentSeasonalWalletPCKNCCResult->PCKFee;
        $regno = $result->PreparePaymentSeasonalWalletPCKNCCResult->RegistrationNo;

        $details = array(
          'transid'=>$transactionid,
          'name'=>$name,
          'fee'=>$fee,
          'regno'=>$regno,
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $details;
       #}


    }

    function completeSeasonalParking(){
      $username = USER_NAME;
      $key = JP_KEY;
      $url = MAIN_URL;

       $tranid = $this->input->post('transid');
       $amount = $this->input->post('amount');
       $channelRef = strtoupper(substr(md5(uniqid()),25));
       $currency = "KES";
       $jpPIN = $this->input->post('jp_pin');

       $ck = $username . $tranid . $key;
       $pass = sha1(utf8_encode($ck ));

       $serviceArguments = array(
         "userName"=>$username,
         "transactionId"=>$tranid,
         "jpPIN"=>$jpPIN,
         "amount"=>$amount,
         "currency"=> $currency,
         "channelRef"=> $channelRef,
         "pass"=> $pass
         );

        $client = new SoapClient($url);
        $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
        $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
        $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
        if($ResultCode==0&&$ResultText=='OK'){
        $paid = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Paid;
        $receiptno = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;

        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText,
          'paid'=>$paid,
          'receiptno'=>$receiptno
          );
        return $data;
      }
      else {
        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $data;
      }
    }


  function getVehiclesDetails(){
    $username = $this->session->userdata('jpwnumber');
    $query="select registrationNo,durationDesc,vehicleDesc,amount,id from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $result1=$this->db->query($query);
    $html="";
    if($result1->num_rows()>0){
      $html.="<thead><tr><th>Registration Number</th><th>Duration</th><th>Vehicle Type</th>";
            $html.="<th style='text-align:right'>Amount</th><th></th></tr></thead><tbody>";

      foreach ($result1->result() as $value) {
        $html.="<tr><td>";
              $html.="<div class='text-primary'><strong>".$value->registrationNo."</strong></div></td>";
              $html.="<td style='width:150px;'>".$value->durationDesc."</td><td>".$value->vehicleDesc."</td><td  style='text-align:right'>".number_format($value->amount,2)."</td><td class='del' style='width:10px;'><a href='#' id='".$value->id."' class='panel-edit removeR'><i class='fa fa-times'></i></a></td></tr>";
      }

      $html.="</tbody>";
    }else{
      $html.=" <div class='well'>No records added </div> ";
    }
    return print_r($html);
  }

  function getVehiclesSum(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select sum(amount) as sumamount from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $html="";
    $result2=$this->db->query($query2);
    $value=$result2->row();
    $html=number_format($value->sumamount,2);
    return print_r($html);

  }

  function countVehicles(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select count(registrationNo) as num from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $html="";
    $result2=$this->db->query($query2);
    if ($result2->result()>0) {
      $value=$result2->row();
      $html=$value->num;

      return print_r($html);
    }else {
      return print_r($html);
    }
  }

  function addVehicle(){
       $float_balance=str_replace(',','',$this->input->post('fbal'));
        $durationCode=$this->input->post('DurationCode');
        $DurationDesc=$this->input->post('DurationDesc');
        $vehicleTypeCode=$this->input->post('VehicleTypeCode');
        $VehicleTypeDesc=$this->input->post('VehicleTypeDesc');
        $registrationNo= str_replace(' ', '',strtoupper($this->input->post('registrationNo')));
        $time=date('Y-m-d h:i:s',time());
        //$time='2014-03-25 08:08:08';

        $user=$this->session->userdata('jpwnumber');
        $status='0';
        $complete='0';
        $username = USER_NAME; //$this->session->userdata('auser');
      $key = JP_KEY; //$this->session->userdata('key');
      $agentRef = strtoupper(substr(md5(uniqid()),25));
      $ck = $username.$user.$vehicleTypeCode.$durationCode.$agentRef.$registrationNo.$key;
      $pass = sha1(utf8_encode($ck));
        $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         "durationCode"=>$durationCode,
         "jpwMobileNo"=> $user,
         "pass"=> $pass
       );

     try {
       $client = new SoapClient(MAIN_URL, array('cache_wsdl' => WSDL_CACHE_NONE));
     } catch (Exception $e) {
        redirect('sbp/not_found');
     }
     $query="select registrationNo from epaymentsSeasonalParkingtemp where user='$user' and registrationNo='$registrationNo'";
     $result=$this->db->query($query);
     if($result->num_rows()>0){
        return print_r('Cannot add a duplicate vehicle registration number.');
     }else{
       $result = $client->PreparePaymentSeasonalWalletPCKNCC($serviceArguments);
       $ResultCode = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultText;
       $pckfee = $result->PreparePaymentSeasonalWalletPCKNCCResult->PCKFee;
       $TransactionID = $result->PreparePaymentSeasonalWalletPCKNCCResult->TransactionID;
       $query="select sum(amount) as sum_amount from epaymentsSeasonalParkingtemp where user='$user'";
       $result=$this->db->query($query);
       $result=$result->row();
       $sum=$result->sum_amount;
       if($ResultCode==0&&$ResultText=="OK"){
          //$float_balance='30000';
          $total=$sum+$pckfee;/*get the sum of the recently added vehicle plus the already added vehicles*/
          //return print_r($total);
          if($total<=$float_balance){
            $query="insert into epaymentsSeasonalParkingtemp (durationDesc,durationCode,registrationNo,vehicleDesc,vehicleCode,transid,amount,`time`,user,status,complete) values ('$DurationDesc','$durationCode','$registrationNo','$VehicleTypeDesc','$vehicleTypeCode','$TransactionID','$pckfee','$time','$user','$status','$complete')";
            $this->db->query($query);
          }else{
            $less=$total-$float_balance;
            return print_r("Sorry.You have insufficient funds in your wallet" .anchor('selfservice/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")));
          }
       }else{
        if($ResultCode=='2030'){
          $total=$sum+$pckfee;
          $less=number_format($total-$float_balance);
          return print_r("Sorry.You have insufficient funds in your wallet. You need an additional $less. " .anchor('selfservice/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")));
        }else{
          return print_r($ResultText);
        }

       }
     }
  }

    function addVehicleNew(){
       $float_balance=str_replace(',','',$this->input->post('fbal'));
        $durationCode=$this->input->post('DurationCode');
        $DurationDesc=$this->input->post('DurationDesc');
        $vehicleTypeCode=$this->input->post('VehicleTypeCode');
        $VehicleTypeDesc=$this->input->post('VehicleTypeDesc');
        $registrationNo= str_replace(' ', '',strtoupper($this->input->post('registrationNo')));
        $time=date('Y-m-d h:i:s',time());
        //$time='2014-03-25 08:08:08';
        $token = $this->session->userdata('token');

        $user=$this->session->userdata('jpwnumber');
        $status='0';
        $complete='0';

        $tosend = array(
            'IsDaily'=>"false",
            'PlateNumber'=>$registrationNo,
            'DurationID'=>$durationCode,//"2014",
            'PhoneNumber' =>$user,
            'ChargeID' =>$vehicleTypeCode,
            'Stream' =>"parking",
            'PaymentTypeID' => "1"
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments/post";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);
        $res = json_decode($result); #var_dump($res); die();

    // $query="select registrationNo from epaymentsSeasonalParkingtemp where user='$user' and registrationNo='$registrationNo'";
     $query="select registrationNo from epaymentsSeasonalParkingtemp where user='$user' ";
     $result=$this->db->query($query);
     if($result->num_rows()>0){
       // return print_r('Cannot add a duplicate vehicle registration number.');
      return print_r('Cannot add a more than 1 vehicle registration number for now.');
     }else{
       // $result = $client->PreparePaymentSeasonalWalletPCKNCC($serviceArguments);
       // $ResultCode = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultCode;
       // $ResultText = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultText;
       // $pckfee = $result->PreparePaymentSeasonalWalletPCKNCCResult->PCKFee;
       // $TransactionID = $result->PreparePaymentSeasonalWalletPCKNCCResult->TransactionID;
       $query="select sum(amount) as sum_amount from epaymentsSeasonalParkingtemp where user='$user'";
       $result=$this->db->query($query);
       $result=$result->row();
       $sum=$result->sum_amount;
       if(isset($res->TransactionID)){
          //$float_balance='30000';
          $total=$sum+$res->Amount;/*get the sum of the recently added vehicle plus the already added vehicles*/
          //return print_r($total);
          if($total<=$float_balance){
            $query="insert into epaymentsSeasonalParkingtemp (durationDesc,durationCode,registrationNo,vehicleDesc,vehicleCode,transid,amount,`time`,user,status,complete) values ('$DurationDesc','$durationCode','$registrationNo','$VehicleTypeDesc','$vehicleTypeCode','$res->TransactionID','$res->Amount','$time','$user','$status','$complete')";
            $this->db->query($query);
          }else{
           // return var_dump(expression)
            $less=$total-$float_balance;
            return print_r("Sorry.You have insufficient funds in your wallet." .anchor('selfservice/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")));
          }
       }else{
        if($res->ErrorCode=2030){
          #$total=$sum+$res->Amount;
          #$less=number_format($total-$float_balance);
          return print_r("Sorry.You have insufficient funds in your wallet." .anchor('selfservice/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")));
        }else{
          return print_r($ResultText);
        }

       }
     }
  }

  function resetVehicle(){
    $username = $this->session->userdata('jpwnumber');
    $query="delete from epaymentsSeasonalParkingtemp where user='$username'";
    $this->db->query($query);
  }

  function removeSRecord(){
    $username = $this->session->userdata('jpwnumber');
    $id=$this->uri->segment(3);
    $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$id'";
    $this->db->query($query);
  }

  function confirmSPParking(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select sum(amount) as sumamount from epaymentsSeasonalParkingtemp where user='$username' ";
    $html="";
    $result2=$this->db->query($query2);

    $query="select count(registrationNo) as num from epaymentsSeasonalParkingtemp where user='$username'";
    $result=$this->db->query($query);
    return array('res1'=>$result,'res2'=>$result2);
  }

  function completeSPPayment(){
    $username = $this->session->userdata('jpwnumber');
    $query="select * from epaymentsSeasonalParkingtemp where user='$username'";
    $result=$this->db->query($query);
    $pin=$this->input->post('jppin');
    $name=$this->session->userdata('name');
    $name=mysql_real_escape_string($name);
    $t_unique = strtoupper(substr(md5(uniqid()),25));
    $this->session->set_userdata('t_unique',$t_unique);
    $totalamount = $this->input->post('amount');
    $noofvehicles = $this->input->post('noofvehicles');
    $channel = "selfservice";

    foreach ($result->result() as $value) {
     $ck = USER_NAME. $value->transid .JP_KEY;
     $pass = sha1(utf8_encode($ck ));
     $channelRef = strtoupper(substr(md5(uniqid()),25));
     $serviceArguments = array(
       "userName"=>USER_NAME,
           "jpPIN"=>$pin,//"1111",
           "transactionId"=>$value->transid,
           "amount"=>$value->amount,
           "currency"=> "KES",
           "channelRef"=> $channelRef,
           "pass"=> $pass
           );
     try {
      $client = new SoapClient(MAIN_URL);
    } catch (Exception $e) {
      redirect('/sbp/not_found');
    }

    $lastday=date('t',strtotime(date('Y-m-d h:i:s',time())));
    $daycount=date('d',strtotime(date('Y-m-d h:i:s',time())));
    $interval=$lastday-$daycount;
    $offset="+".($value->durationCode)." months";
    $expiry_date = date('Y-m-d', strtotime($offset, strtotime($value->time)));

    $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
    $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
    $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
    $ReceiptNo = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;
    if ($ResultCode==0&&$ResultText=="OK") {
      $query="insert into seasonalparking (`receiptno`,`issuedate`,`from`,`regno`,`category`,`duration`,`amount`,`expirydate`,`username`,`cashiername`,`t_unique`,`channel`) values('$ReceiptNo','$value->time','$name','$value->registrationNo','$value->vehicleDesc','$value->durationDesc','$value->amount','$expiry_date','$value->user','$name','$t_unique','$channel')";
      $this->db->query($query);
      $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$value->id'";
      $this->db->query($query);
      return $data = array('customer'=>$this->session->userdata('name'),'result'=>$result,'restext'=>$ResultText,'rescode'=>$ResultCode);
    }else{
      return $data = array('restext'=>$ResultText,'rescode'=>$ResultCode);
    }

  }

}

function completeSPPaymentS(){
    $token = $this->session->userdata('token');
    $username = $this->session->userdata('jpwnumber');
    $query="select * from epaymentsSeasonalParkingtemp where user='$username'";
    $result=$this->db->query($query);
    echo $result->num_rows();//$rowcount=mysql_num_rows($result); die();
    $pin=$this->input->post('jppin');
    $name=$this->session->userdata('name');
    #$name=mysql_real_escape_string($name);
    $t_unique = strtoupper(substr(md5(uniqid()),25));
    $this->session->set_userdata('t_unique',$t_unique);
    $totalamount = $this->input->post('amount');
    $noofvehicles = $this->input->post('noofvehicles');
    $channel = "selfservice";
    echo "<pre>";
    $url = "http://192.168.6.10/JamboPayServices/api/payments";
    $count=0;
    foreach ($result->result() as $value1) {
      echo $count;
      if($count==0){
         $count++;
        continue;
      }
     $tranid = $value1->transid;
     $amount = $value1->amount;


     $tosend = array(
          'PhoneNumber' =>$username,
          'TransactionID' =>$tranid,
          'Stream' =>"parking",
          'Pin' => $pin
        );

      foreach ( $tosend as $key => $value) {
      $post_items[] = $key . '=' . $value;
      }
      $post_string = implode ('&', $post_items);


      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result2 = curl_exec($ch);
      $res = json_decode($result2);


      if(isset($res->ErrorCode)){
        var_dump($res->ErrorCode);
      }
      curl_close($ch);
      var_dump($result2);
      //if (isset($res->ReceiptNumber)) {
        // $query="insert into seasonalparking (`receiptno`,`issuedate`,`from`,`regno`,`category`,`duration`,`amount`,`expirydate`,`username`,`cashiername`,`t_unique`,`channel`) values('$ReceiptNo','$value->time','$name','$value->registrationNo','$value->vehicleDesc','$value->durationDesc','$value->amount','$expiry_date','$value->user','$name','$t_unique','$channel')";
        // $this->db->query($query);
      //   $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$value1->id'";
      //   $this->db->query($query);
      //   return $data = array('rescode'=>0);
      // }else{
      //   return $data = array('rescode'=>$res->ErrorCode);
      // }

    }


}

  //   function completeSPPaymentNew(){
  //     $token = $this->session->userdata('token');
  //     $username = $this->session->userdata('jpwnumber');
  //     $query="select * from epaymentsSeasonalParkingtemp where user='$username'";
  //     $result=$this->db->query($query);
  //     $pin=$this->input->post('jppin');
  //     $name=$this->session->userdata('name');
  //     $t_unique = strtoupper(substr(md5(uniqid()),25));
  //     $this->session->set_userdata('t_unique',$t_unique);
  //     $totalamount = $this->input->post('amount');
  //     $noofvehicles = $this->input->post('noofvehicles');
  //     $channel = "selfservice";
  //     $customer = $this->session->userdata('name');
  //     $carreg = $this->input->post('reg');
  //     //var_dump($result->result());die();
  //     $url = "http://192.168.6.10/JamboPayServices/api/payments";
  //     foreach ($result->result() as $value1) {

  //      $tranid = $value1->transid;
  //      $amount = $value1->amount;


  //      $tosend = array(
  //           'PhoneNumber' =>$username,
  //           'TransactionID' =>$tranid,
  //           'Stream' =>"parking",
  //           'Pin' => $pin
  //         );

  //       foreach ( $tosend as $key => $value) {
  //         $post_items[] = $key . '=' . $value;
  //       }
  //       $post_string = implode ('&', $post_items);


  //       $ch = curl_init($url);
  //       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
  //       curl_setopt($ch, CURLOPT_POST, true);
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //       curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
  //       curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //           'Authorization: bearer '.$token,
  //           'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
  //       ));

  //       $result2 = curl_exec($ch);
  //       $res = json_decode($result2);
  //       curl_close($ch);

  //       // echo "<pre>";
  //       // var_dump($res);

  //       if(isset($res->ReceiptNumber)) {
  //         $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$value1->id'";
  //         $this->db->query($query);

  //         $data = array('rescode'=>0);
  //         //return $data;

  //         try {
  //         /*for sending confirmation sms*/
  //         $tel = $this->session->userdata('jpwnumber');
  //         // $amnt= number_format($amount, 2, '.', ',');
  //         $msg ="Your payment of KES: ".number_format($totalamount,2)." for Seasonal Parking of $noofvehicles vehicles has been received by Nairobi City County.Powered by: JamboPay";
  //         $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

  //         $APIKey = urlencode($key);
  //         $Phone = urlencode($tel);
  //         $relayCode = urlencode("WebTribe");
  //         $Message = urlencode($msg);
  //         $Shortcode = urlencode("700273");
  //         $CampaignId = urlencode("112623");
  //         $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
  //         $ch=curl_init();
  //         curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsserver/SendSMS.aspx'.$qstr);
  //         curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  //         $result = curl_exec($ch);
  //         curl_close($ch);
  //         } catch (Exception $e) {
  //         }
  //       }
  //       else{
  //         $data = array('rescode'=>$res->ErrorCode);
  //         //return $data;
  //       }
  //     }#end the 1st for each
  //     //die();
  // }

      function completeSPPaymentNew(){
      $token = $this->session->userdata('token');
      $username = $this->session->userdata('jpwnumber');
      $query="select * from epaymentsSeasonalParkingtemp where user='$username'";
      $result=$this->db->query($query);
      $pin=$this->input->post('jppin');
      $name=$this->session->userdata('name');
      $t_unique = strtoupper(substr(md5(uniqid()),25));
      $this->session->set_userdata('t_unique',$t_unique);
      $totalamount = $this->input->post('amount');
      $noofvehicles = $this->input->post('noofvehicles');
      $channel = "selfservice";
      $customer = $this->session->userdata('name');
      $carreg = $this->input->post('reg');

      foreach ($result->result() as $value1) {

       $tranid = $value1->transid;
       $amount = $value1->amount;


       $tosend = array(
            'PhoneNumber' =>$username,
            'TransactionID' =>$tranid,
            'Stream' =>"parking",
            'Pin' => $pin
          );

        foreach ( $tosend as $key => $value) {
        $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/api/payments";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result2 = curl_exec($ch);
        $res = json_decode($result2);
        curl_close($ch);
        #var_dump($value1->id); die();

        if(isset($res->ReceiptNumber)) {
          $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$value1->id'";
          $this->db->query($query);

          $data = array('rescode'=>0);

          $expirydate = substr($res->ExpiryDate,0,10);
          //echo $msg = "Receipt Number $res->ReceiptNumber.Your payment of KES $amount for Seasonal Parking of $res->PlateNumber , Expiry date $expirydate has been received by Nairobi City County.Powered by: JamboPay";

          try {
          /*for sending confirmation sms*/
          $tel = $this->session->userdata('jpwnumber');
          // $amnt= number_format($amount, 2, '.', ',');
          //$msg ="Your payment of KES: ".number_format($totalamount,2)." for Seasonal Parking of $noofvehicles vehicles has been received by Nairobi City County.Powered by: JamboPay";
          $msg = "Receipt Number $res->ReceiptNumber.Your payment of KES $amount for Seasonal Parking of $res->PlateNumber , Expiry date $expirydate has been received by Nairobi City County.Powered by: JamboPay";
          $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

          $APIKey = urlencode($key);
          $Phone = urlencode($tel);
          $relayCode = urlencode("WebTribe");
          $Message = urlencode($msg);
          $Shortcode = urlencode("700273");
          $CampaignId = urlencode("112623");
          $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
          $ch=curl_init();
          curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsserver/SendSMS.aspx'.$qstr);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
          $result = curl_exec($ch);
          curl_close($ch);
          } catch (Exception $e) {
          }
          return $data;
        }
        else{
          $data = array('rescode'=>$res->ErrorCode);
          return $data;
        }
      } #var_dump($data); die(); #end the 1st for each
  }

  #print seasonal receipt
    function printSeasonal(){
      $this->load->library('zend');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/seasonalParking.pdf';
        $pdf = Zend_Pdf::load($fileName);


    $page=$pdf->pages[0];
    $template = $pdf->pages[0];

        $t_unique=$this->uri->segment(3);
        $query="select count(t_unique) as count from seasonalparking where t_unique='$t_unique'";
        $result=$this->db->query($query);
        $count=$result->row();
        $pages=ceil($count/6);
        $query="select * from seasonalparking where t_unique='$t_unique'";
        $result=$this->db->query($query);
        $records=$result->result();
        $pageindex=0;
        $i=0;
       # var_dump($result) die();
      foreach ($result->result() as  $index=>$records) {
        //$page=$pdf->pages[0];
        if($index %6==0&&$index!=0){
          $pageindex++;
          ${'page'.$pageindex} = new Zend_Pdf_Page($template);
        $pdf->pages[] = ${'page'.$pageindex};
        $page=$pdf->pages[$pageindex];
        $i=0;
        }
        if($i==0){



          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 320, 543);
        $page->drawText(strtoupper($cashier), 103, 415);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 513);
            $page->drawText(strtoupper($paid_by), 105, 501);
            $page->drawText($registration_number, 178, 489);
            $page->drawText($vehicle_category, 163, 475);
            $page->drawText($duration, 115, 461);
            $page->drawText(number_format($amount,2), 110, 446);
            $page->drawText($expiry_date, 128, 432);
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 45,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();
        }

        if($i==1){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 543);
        $page->drawText(strtoupper($cashier), 483, 415);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 513);
            $page->drawText(strtoupper($paid_by), 485, 501);
            $page->drawText($registration_number, 558, 489);
            $page->drawText($vehicle_category, 546, 475);
            $page->drawText($duration, 495, 461);
            $page->drawText(number_format($amount,2), 490, 446);
            $page->drawText($expiry_date, 508, 432);
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 45,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==2){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 320, 350);
        $page->drawText(strtoupper($cashier), 103, 223);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 321);
            $page->drawText(strtoupper($paid_by), 105, 309);
            $page->drawText($registration_number, 178, 297);
            $page->drawText($vehicle_category, 163, 283);
            $page->drawText($duration, 115, 269);
            $page->drawText(number_format($amount,2), 110, 254);
            $page->drawText($expiry_date, 128, 240);

            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 237,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==3){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 350);
        $page->drawText(strtoupper($cashier), 483, 223);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 321);
            $page->drawText(strtoupper($paid_by), 485, 309);
            $page->drawText($registration_number, 558, 297);
            $page->drawText($vehicle_category, 546, 283);
            $page->drawText($duration, 495, 269);
            $page->drawText(number_format($amount,2), 490, 254);
            $page->drawText($expiry_date, 508, 240);

            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 237,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==4){

          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

          $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 323, 155);
        $page->drawText(strtoupper($cashier), 103, 29);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 127);
            $page->drawText(strtoupper($paid_by), 105, 115);
            $page->drawText($registration_number, 178, 103);
            $page->drawText($vehicle_category, 163, 89);
            $page->drawText($duration, 115, 75);
            $page->drawText(number_format($amount,2), 110, 60);
            $page->drawText($expiry_date, 128, 46);


            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 430,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==5){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10);
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount;
        $expiry_date=substr($records->expirydate,0,10);
        $cashier=$records->cashiername;

          $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 157);
        $page->drawText(strtoupper($cashier), 483, 29);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 127);
            $page->drawText(strtoupper($paid_by), 485, 115);
            $page->drawText($registration_number, 558, 103);
            $page->drawText($vehicle_category, 546, 89);
            $page->drawText($duration, 495, 75);
            $page->drawText(number_format($amount,2), 490, 60);
            $page->drawText($expiry_date, 508, 46);


            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 430,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }
        $i++;
      }
    //unset($pdf->pages[$i]);

        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=seasonalParking.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;

  }

    function printDreceipt(){
      // try {
      //   $this->load->library('database');
      // } catch (Exception $e) {
      //   redirect('sbp/not_found');
      // }
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
        $username=$this->session->userdata('jpwnumber');
        $receiptno=str_replace('-','/',$this->uri->segment(3));
        $query="select * from dailyparking where username='$username' and receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();

        $date=substr($result->issuedate, 0,10);
        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $zone=$result->zone;
        $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
  }

  function printDreceiptNew(){
        $token=$this->session->userdata('token');
        $stream = "parking";
        $key = "PlateNumber";

        $receiptno=$this->uri->segment(3);
        $value = $receiptno;

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);
        //echo $res;
        //echo $var;

        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
        $username=$this->session->userdata('jpwnumber');
        $receiptno=$res['0']->ReceiptNumber;//str_replace('-','/',$this->uri->segment(3));

        $date=$res['0']->TransactionDate;
        $regno=$res['0']->PlateNumber;
        $vehicle_category=$res['0']->ChargeName;
        $zone=$res['0']->ZoneName;
        $amount=$res['0']->Amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($res['0']->Names);


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
  }

    function printCreceipt(){

        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/topUpTicket.pdf';
        $username=$this->session->userdata('jpwnumber');
        $receiptno=str_replace('-','/',$this->uri->segment(3));
        $query="select * from dailyparking where username='$username' and receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();

        $date=substr($result->issuedate, 0,10);
        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $zone=$result->zone;
        $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
  }

  function insertReceiptDetails(){
    $category = $this->input->post('vehiclecategory');
    $zone = $this->input->post('parkingzone');
    $date = $this->input->post('issuedate');
    $date1 =substr($date,0,10); //date("Y-m-d",$date);
    $receiptno = $this->input->post('receiptno');
    $regno = $this->input->post('regno');
    $regno = strtoupper($regno);
    $paidby = $this->input->post('paidby');
    $paidby = mysql_real_escape_string($paidby);
    $amount = $this->input->post('amount');
    $phoneno = $this->input->post('phonenumber');
    $channel = "Manual";
    #var_dump($date1); die();
    try {
      $data=array('receiptno'=>$receiptno,'issuedate'=>$date1,'from'=>$paidby,'amount'=>$amount,'regno'=>$regno,'category'=>$category,'zone'=>$zone,'username' =>$phoneno,'cashiername' =>$paidby);
                $query = "insert into dailyparking (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`)";
                $query.= " values ('".implode("','",$data)."')";
                $this->db->query($query);
                redirect('parking/reprintDailyreceipt');
    } catch (Exception $e) {
      redirect('parking/reprintManualDReceipt');
    }

  }

    function reprintDreceipt(){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
        $username= $this->session->userdata('jpwnumber');
        $parkingtype=$this->input->post('parkingtype');
        $reg=$this->input->post('regno');
        $regnum = str_replace(' ', '', $reg);
        $regnum = strtoupper($regnum);
        $issuedate = $this->input->post('issuedate');

        if($parkingtype=="1"){
        $query="SELECT *,date(issuedate) as issuedate FROM `dailyparking` WHERE `regno`='$regnum' AND date(issuedate)='$issuedate' ";
        $result=$this->db->query($query);
        $result=$result->row();
        $dbdate=substr($result->issuedate,0,10);
        #var_dump($result); die();
        if ($result->regno==$regnum && $dbdate==$issuedate){
        $date=substr($result->issuedate, 0,10);
        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $zone=$result->zone;
        $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
      }
      else{
        redirect('parking/reprintUnavailable');
      }
    }
    else{
        $query="SELECT *,date(issuedate) as issuedate FROM `seasonalparking` WHERE `regno`='$regnum' AND date(issuedate)='$issuedate'"; #var_dump($issuedate); die();
        $result=$this->db->query($query);
        $result=$result->row();
        #echo $query;
        #var_dump($result); die();
        $dbdate=substr($result->issuedate, 0,10);
        if ($result->regno==$regnum && $dbdate==$issuedate){

        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $duration=$result->duration;
        $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);

        $fileName =APPPATH.'assets/back/receipts/SingleseasonalParking.pdf';
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($duration, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
      }
      else{
        redirect('parking/reprintUnavailable');
      }
    }
  }

  function reprintDreceiptNew(){
        $token=$this->session->userdata('token');
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $username= $this->session->userdata('jpwnumber');
        #$type=$this->input->post('parkingtype');
        $issuedate=$this->input->post('issuedate');
        $regno=$this->input->post('regno');
        #var_dump($type); die();
        $stream = "parking";
        $key = "PlateNumber";
        $value = $regno;
        $key0 = "TransactionStatus";
        $value0 = "Completed";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key0&[1].Value=$value0";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);
        #echo "<pre>";var_dump($res); die();
        #$status = $res['0']->TransactionStatus;
        if(count($res) > 0){

        $isdaily = $res['0']->IsDailyParking;
        $chargename = $res['0']->ChargeName;

        if($isdaily==TRUE && $chargename!="CLAMPING_CHARGE"){
            $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
            $receiptno=$res['0']->ReceiptNumber;
            $date=$res['0']->TransactionDate;
            $date = substr($date,0,10);
            $regno=$res['0']->PlateNumber;
            $vehicle_category=$res['0']->ChargeName;
            $zone=$res['0']->ZoneName;
            $amount=$res['0']->Amount;
            #var_dump(substr($date,0,10)); die();
            $amount =number_format(str_replace( ',', '', $amount),2);
            $servedby=strtoupper($res['0']->Names);


            $pdf = Zend_Pdf::load($fileName);
            $page=$pdf->pages[0];

            $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
            $page->drawText($receiptno, 260, 300);
            $page->drawText($date, 632, 411);
            $page->drawText($regno, 260, 275);
            $page->drawText($vehicle_category, 260, 250);
            $page->drawText($zone, 260, 225);
            $page->drawText($amount, 260, 200);
            $page->drawText($servedby, 165, 135);/*new*/
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=receipt.pdf");
            header("Content-type: application/x-pdf");
            echo $pdfData;
    }
   if($isdaily==0 && $chargename!="CLAMPING_CHARGE"){
        //if ($result->regno==$regnum && $dbdate==$issuedate){
        $receiptno=$res['0']->ReceiptNumber;
        $issuedate=$res['0']->TransactionDate;
          $issuedate=substr($issuedate,0,10);
        $startdate=$res['0']->StartDate;
          $startdate=substr($startdate,0,10);
        $enddate=$res['0']->EndDate;
          $enddate=substr($enddate,0,10);
        $regno=$res['0']->PlateNumber;
        $vehicle_category=$res['0']->ChargeName;
        $zone=$res['0']->ZoneName;
        $amount=$res['0']->Amount;
        $duration=$res['0']->Duration;
        $paidby = strtoupper($res['0']->Names);

        $amount =number_format(str_replace( ',', '', $amount),2);
        #echo "<pre>";var_dump($res['0']->TransactionStatus); die();
        $fileName =APPPATH.'assets/back/receipts/SingleseasonalParking.pdf';
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 325);
        $page->drawText($paidby, 260, 300);
        $page->drawText($issuedate, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($duration, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($startdate, 165, 175);
        $page->drawText($enddate, 415, 175);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
      // }else{
      //   redirect('parking/reprintUnavailable');
      // }
    }
    else{
        redirect('parking/reprintUnavailable');
    }
  }else{
      #var_dump("expression3");die();
        redirect('parking/reprintUnavailable');
      }

  }

  function printPenaltyReceipt(){
        $token=$this->session->userdata('token');
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $username= $this->session->userdata('jpwnumber');
        #$type=$this->input->post('parkingtype');
        $issuedate=$this->input->post('issuedate');
        $regno=$this->input->post('regno');
        #var_dump($type); die();
        $stream = "parking";
        $key = "PlateNumber";
        $value = $regno;
        $key0 = "TransactionStatus";
        $value0 = "1";
        $key1 = "ChargeName";
        $value1 = "CLAMPING_CHARGE";

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key0&[1].Value=$value0&[2].Key=$key1&[2].Value=$value1";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);

        if(count($res) > 0){
        $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
        $receiptno=$res['0']->ReceiptNumber;
        $date=$res['0']->TransactionDate;
        $date = substr($date,0,10);
        $regno=$res['0']->PlateNumber;
        $vehicle_category=$res['0']->ChargeName;
        $zone=$res['0']->ZoneName;
        $amount=$res['0']->Amount;
        #var_dump(substr($date,0,10)); die();
        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($res['0']->Names);


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B);
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render();
        header("Content-Disposition: inline; filename=receipt.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
        }
        else{
        redirect('parking/CreprintUnavailable');
      }
  }

}
