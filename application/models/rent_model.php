<?php

class Rent_model extends CI_Model{

	function getEstates(){

     $url=MAIN_URL;
	   $username = USER_NAME; //72386895";
	   $key = JP_KEY;//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
	   $timestamp = date("Y-m-d H:i:s");  
	   $agentRef = strtoupper(substr(md5(uniqid()),25));

	   $ck = $username.$timestamp.$key;
	   $pass = sha1(utf8_encode($ck));


	   $serviceArguments = array(
	     "userName"=>$username,
	     "pass"=> $pass,
	     "timestamp"=> $timestamp
	           
	   );



	try {
	   $client = new SoapClient($url);
	   $result = $client->GetEstatesHSENCC($serviceArguments);
	  } catch (SoapFault $fault) {

  		redirect('/sbp/not_found');

    }

    $ResultCode = $result->GetEstatesHSENCCResult->Result->ResultCode;
    $EstateDesc = $result->GetEstatesHSENCCResult->Estate->{'Housing.Estate'};

     return $EstateDesc;
	}

  function getEstatesNew(){
    $token = $this->session->userdata('token');
    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/GetEstates?stream=rent");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer '.$token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));
    // execute the request
    $result = curl_exec($ch);
    curl_close($ch);
    

     return json_decode($result);
  }

  function getHouseTypes(){
    $token = $this->session->userdata('token');
    $id = $this->input->post('EstateID'); 
    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/GetResidenceTypes?stream=rent&id=$id");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer '.$token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));
    // execute the request
    $result = curl_exec($ch);
    curl_close($ch);
    #var_dump($result); die();
    $housetype=json_decode($result);
    #echo "<pre>";var_dump($housetype);die();
    if (is_array( $housetype)) {
       echo "<select class='form-control chosen-select' id='housetype' name='housetype'>";
      foreach ($housetype as $key => $value) {
          echo "<option value='$value->ID'>$value->Name</option>";
      }
       echo "</select>";
     }else{

      echo "<div class='alert alert-info'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                 $housetype->Message;
                </div>";
     }
    
  }

  function getResidence(){
    $token = $this->session->userdata('token');
    $TypeID =$this->input->post('TypeID');
    $id =$this->input->post('EstateID');//"38";  


    //var_dump($id.'|'.$TypeID);
    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/GetResidences?stream=rent&id=$id");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer '.$token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));
    // execute the request
    $result = curl_exec($ch);
    curl_close($ch);
    #echo "<pre>";var_dump($id); die();
    $residence=json_decode($result);
   
    $houseTypesdata=array();
    #print_r($residence); die();
    if (is_array( $residence)) {
       echo "<select class='form-control chosen-select' id='house' name='house'>";
      foreach ($residence as $key => $value) {
        if($value->HouseTypeID==$TypeID){
          echo "<option value='$value->ID'>$value->ID</option>";
        }
          // $houseTypesdata=array('ID'=>$value->ID,'UHN'=>$value->UHN,'HOUSETYPEID'=>$value->HouseTypeID);

          // echo "<option value='".base64_encode(json_encode($houseTypesdata))."'>".$value->UHN.$value->HouseTypeID."</option>";
        
      }
      // var_dump( $houseTypesdata);
      // die();
       echo "</select>";
     }else{

      echo "<div class='alert alert-info'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                 $residence->Message;
                </div>";
     }
    
  }

  function getEstatesOnly(){

     $url=MAIN_URL;
     $username = USER_NAME; //72386895";
     $key = JP_KEY;//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
     $timestamp = date("Y-m-d H:i:s");  
     $agentRef = strtoupper(substr(md5(uniqid()),25));

     $ck = $username.$timestamp.$key;
     $pass = sha1(utf8_encode($ck));


     $serviceArguments = array(
       "userName"=>$username,
       "pass"=> $pass,
       "timestamp"=> $timestamp
             
     );



  try {
     $client = new SoapClient($url);
     $result = $client->GetEstatesHSENCC($serviceArguments);
    } catch (SoapFault $fault) {

      redirect('/sbp/not_found');

    }

    $ResultCode = $result->GetEstatesHSENCCResult->Result->ResultCode;
    $EstateDesc = $result->GetEstatesHSENCCResult->Estate->{'Housing.Estate'};
    #$EstateOnly = $result->GetEstatesHSENCCResult->Result->HouseOrMarketStall;

        foreach($EstateDesc as $item)
        {
         $HouseOrMarketStall = $item->HouseOrMarketStall;

         if ($HouseOrMarketStall == 1) {
           $mArray = array();
           $mArray["estate_desc"] = $item->EstateDescription;
           $mArray["estate_id"] = $item->EstateID;

           // if ($i++ > 0) break;        
           //array_push($response["estate_details"], $mArray);

         }
       }
     return $mArray;
  }

	function getEstatesHouses(){

     $url=MAIN_URL;
	   $username = USER_NAME; //72386895";
     $key = JP_KEY;//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
	   $timestamp = date("Y-m-d H:i:s");
	   $estateID=$this->input->post('EstateID');  
	   $agentRef = strtoupper(substr(md5(uniqid()),25));

	   $ck = $username.$timestamp.$key;
	   $pass = sha1(utf8_encode($ck));


	   $serviceArguments = array(
	     "userName"=>$username,
	     "timestamp"=> $timestamp,
	     "estateID"=>$estateID,
	     "pass"=> $pass
	   );

	try {
	   $client = new SoapClient($url);
	   $result = $client->GetEstateHousesHSENCC($serviceArguments);
	  } catch (SoapFault $fault) {

  		redirect('/sbp/not_found');

    }

    $ResultCode = $result->GetEstateHousesHSENCCResult->Result->ResultCode;
    $ResultText = $result->GetEstateHousesHSENCCResult->Result->ResultText;
    if ($ResultCode==0&&$ResultText=="OK") {
    	
		   
		    $HouseDesc = $result->GetEstateHousesHSENCCResult->House->{'Housing.House'};
		     //return $HouseDesc;
		    if (is_array($HouseDesc)) {
		    	echo "<select class='form-control chosen-select' id='house' name='house'>";
			  foreach($HouseDesc as $key=>$value) {
			     echo "<option value='$value->HouseNumber'>$value->HouseNumber</option>";
			  }
			  	echo "</select>";
		    }else{

		    	 echo "<select class='form-control chosen-select' id='house' name='house'>";
		    	 echo "<option value='$HouseDesc->HouseNumber'>$HouseDesc->HouseNumber</option>";
		    	 echo "</select>";
		    }
     }else{

     	echo "<div class='alert alert-info'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                  $ResultText
                </div>";
     }
    	
                        
	}


	function prephousepayment(){

       $url=MAIN_URL;
	     $username = USER_NAME;// "97763838"; //72386895";
       $key = JP_KEY;//"3637137f-9952-4eba-9e33-17a507a2bbb2";//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";
       $timestamp = date("Y-m-d H:i:s");
       $estateID=$this->input->post('estate');//"108";  
       $agentRef = strtoupper(substr(md5(uniqid()),25));
       $houseNumber=$this->input->post('house');//"0041-00001";
       $custMobileNo= $this->session->userdata('jpwnumber'); //"254712633277";//254".substr($this->input->post('phone'),-9);
       $ck = $username.$custMobileNo.$agentRef.$houseNumber.$key;
       $pass = sha1(utf8_encode($ck));

       $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=>$agentRef,
         "estateID"=>$estateID,
         "houseNumber"=>$houseNumber,
         "jpwMobileNo"=>$custMobileNo,
         "pass"=> $pass
       );

    try {
       $client = new SoapClient($url);
       $result = $client->PreparePaymentWalletHSENCC($serviceArguments);
      } catch (SoapFault $fault) {

        redirect('/sbp/not_found');

    } #var_dump($result); die();

      $ResultCode = $result->PreparePaymentWalletHSENCCResult->Result->ResultCode;
      $ResultText = $result->PreparePaymentWalletHSENCCResult->Result->ResultText;

      $CurrentBalance = $result->PreparePaymentWalletHSENCCResult->CurrentBalance;
      $HouseNumber = $result->PreparePaymentWalletHSENCCResult->HouseNumber;
      $LastBillMonth = $result->PreparePaymentWalletHSENCCResult->LastBillMonth;
      $MonthlyOtherCharges = $result->PreparePaymentWalletHSENCCResult->MonthlyOtherCharges;
      $MonthlyRent = $result->PreparePaymentWalletHSENCCResult->MonthlyRent;
      $OtherArrears = $result->PreparePaymentWalletHSENCCResult->OtherArrears;
      $PhysicalAddress = $result->PreparePaymentWalletHSENCCResult->PhysicalAddress;
      $RentArrears = $result->PreparePaymentWalletHSENCCResult->RentArrears;
      $RentDueDate = $result->PreparePaymentWalletHSENCCResult->RentDueDate;
      $TransactionID = $result->PreparePaymentWalletHSENCCResult->TransactionID;
      $CustomerName = $result->PreparePaymentWalletHSENCCResult->CustomerName;

      $data=array(
          "CustName"=>$CustomerName,
      		"CurrentBalance"=>$CurrentBalance,
      		"HouseNumber"=>$HouseNumber,
      		"LastBillMonth"=>substr($LastBillMonth,0,10),
      		"MonthlyOtherCharges"=>$MonthlyOtherCharges,
      		"MonthlyRent"=>$MonthlyRent,
      		"OtherArrears"=>$OtherArrears,
      		"PhysicalAddress"=>$PhysicalAddress,
      		"RentArrears"=>$RentArrears,
      		"RentDueDate"=>substr($RentDueDate,0,10),
      		"TransactionID"=>$TransactionID,
      		"phone"=>"0".substr($custMobileNo,-9),
      		"ResultCode"=>$ResultCode,
      		"ResultText"=>$ResultText
      	);

      return $data;
	}

  function prephousepaymentNew(){

       $token = $this->session->userdata('token');
       $estateID=$this->input->post('estate');
       $housetypeID=$this->input->post('housetype');
       $houseNumber = $this->input->post('house');
      // var_dump($this->input->post('house'));
       $housearray = json_decode(base64_decode($this->input->post('house')));
       // var_dump(json_decode(base64_decode($this->input->post('house'))));
       // exit();
      // var_dump($estateID);
      // var_dump($housetypeID);
      // var_dump($houseNumber);
      // die();


       #$houseNumber=$housearray;
       $custMobileNo= $this->session->userdata('jpwnumber'); //"254712633277";//254".substr($this->input->post('phone'),-9);

       $tosend = array(
      'PhoneNumber'=>$custMobileNo,
      'ResidenceNumber'=>$houseNumber,
      'EstateID'=>$estateID,
      'HouseTypeID'=>$housetypeID,//$housearray->HOUSETYPEID,
      'PaymentTypeID' => "1",
      'Stream'=>"rent"
      );

      foreach ( $tosend as $key => $value) {
          $post_items[] = $key . '=' . $value;
      }
      $post_string = implode ('&', $post_items);

      $url = "http://192.168.6.10/JamboPayServices/api/payments/";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec($ch);
      curl_close($ch);
      $res = json_decode($result);
      #var_dump($tosend); die();

      #$ResultCode = $result->PreparePaymentWalletHSENCCResult->Result->ResultCode;
      #$ResultText = $result->PreparePaymentWalletHSENCCResult->Result->ResultText;
      if(isset($res->TransactionID)){
      $CurrentBalance = $res->CurrentBalance;
      $HouseNumber = $res->ResidenceNumber;
      $LastBillMonth = $res->LastBillMonth;
      $MonthlyOtherCharges = $res->MonthlyOtherCharges;
      $MonthlyRent = $res->MonthlyRent;
      $OtherArrears = $res->OtherArreas;
      $PhysicalAddress = $res->PhysicalAddress;
      $RentArrears = $res->RentArreas;
      $RentDueDate = $res->RentDueDate;
      $TransactionID = $res->TransactionID;
      $CustomerName = $res->CustomerNames;
      $UHN = $res->UHN;

      $data=array(
          "CustName"=>$CustomerName,
          "CurrentBalance"=>$CurrentBalance,
          "HouseNumber"=>$HouseNumber,
          "LastBillMonth"=>substr($LastBillMonth,0,10),
          "MonthlyOtherCharges"=>$MonthlyOtherCharges,
          "MonthlyRent"=>$MonthlyRent,
          "OtherArrears"=>$OtherArrears,
          "PhysicalAddress"=>$PhysicalAddress,
          "RentArrears"=>$RentArrears,
          "RentDueDate"=>substr($RentDueDate,0,10),
          "TransactionID"=>$TransactionID,
          "phone"=>$custMobileNo,
          "UHN"=>$UHN,
          "rescode"=>"0"
          // "ResultText"=>$ResultText
        );

      return $data;
    }else{
      return $data = array("rescode"=>$res->ErrorCode);
    }
  }

	function confirm2rent(){

		    $phone=$this->input->post('phone');
        $physicaladdress=$this->input->post('physicaladdress');
        $houseno=$this->input->post('houseno');
        $transactionId=$this->input->post('transid');
        $amountdue=$this->input->post('amountdue');
        $amountpaid=$this->input->post('amountpaid');
        $pin=$this->input->post('jp_pin');
        $tenant=$this->input->post('custname');
        $arrears = $this->input->post('arrears');

        $data=array(
          "phone"=>$phone,
          "physicaladdress"=>$physicaladdress,
          "houseno"=>$houseno,
          "transactionId"=>$transactionId,
          "amountdue"=>$amountdue,
          "amountpaid"=>$amountpaid,
          "pin"=>$pin,
          "custname"=>$tenant,
          "arrears"=>$arrears
          );
        return $data;
	}


    function completeNew(){

      $token = $this->session->userdata['token'] ;
      $phone = $this->session->userdata['jpwnumber'];
      $physicaladdress=$this->input->post('physicaladdress');
      $houseno=$this->input->post('houseno');
      $transactionId=$this->input->post('transid');
      $amountdue=$this->input->post('amountdue');
      $RentArrears=$this->input->post('arrears');
      $amountpaid=$this->input->post('amountpaid');
      $jpPIN =$this->input->post('pin');
      $tenant =$this->input->post('custname');
      $tenant = mysql_real_escape_string($tenant);
      $cashier = $this->session->userdata('name');
      $cashier = mysql_real_escape_string($cashier);
      $channelRef = strtoupper(substr(md5(uniqid()),25));
      // $ck = $username.$transactionId.$key;
      // $pass = sha1(utf8_encode($ck));

      $tosend = array(
              'TransactionID'=>$transactionId,
              'Amount'=>$amountpaid,
              'PhoneNumber' =>$phone,
              'Pin' =>$jpPIN,
              'Stream'=>"rent"    
       );

      foreach ( $tosend as $key => $value) {
      $post_items[] = $key . '=' . $value;
      }
      $post_string = implode ('&', $post_items);

      $url = "http://192.168.6.10/JamboPayServices/api/payments";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec($ch);
      $res = json_decode($result); 

      // foreach ($res as $key => $value) {
      //   echo $key;
      // }
      //echo "<pre>"; var_dump($res); die();

   

     if(isset($res->ReceiptNumber)){
        
      $data = array(
        "estatename"=>$res->PhysicalAddress,
        "estateid"=>$res->EstateID,
        "housenumber"=>$res->ResidenceNumber,
        "tenant"=>$res->CustomerNames,
        "amountpaid"=>$res->Amount,
        "receiptno"=>$res->ReceiptNumber,
        "rescode"=>"0"
      );
      return $data;        
      }
       else{
         #foreach ($res as $keys => $value) {
          #echo $keys;
          #}
       $data=array('rescode'=>$res->ErrorCode);
       return $data;
        }

}

	function completerentpayment(){

    $url=MAIN_URL;
	   $username = USER_NAME;// "97763838"; //72386895";
       $key = JP_KEY ;//"3637137f-9952-4eba-9e33-17a507a2bbb2";//26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4";

       $phone=$this->input->post('phone');
       $physicaladdress=$this->input->post('physicaladdress');
       $houseno=$this->input->post('houseno');
       $transactionId=$this->input->post('transid');
       $amountdue=$this->input->post('amountdue');
       $RentArrears=$this->input->post('arrears');
       $amountpaid=$this->input->post('amountpaid');
       $jpPIN =$this->input->post('pin');
       $tenant =$this->input->post('custname');
       $tenant = mysql_real_escape_string($tenant);
       $cashier = $this->session->userdata('name');
       $cashier = mysql_real_escape_string($cashier);
       $channelRef = strtoupper(substr(md5(uniqid()),25));
       $ck = $username.$transactionId.$key;
       $pass = sha1(utf8_encode($ck));
       

       $serviceArguments = array(
         "userName"=>$username,
         "transactionId"=>$transactionId,
         "amount"=>number_format($amountpaid, 2,'.',''),
         "jpPIN"=>$jpPIN,
         "currency"=>"KES",
         "channelRef"=>$channelRef,
         "pass"=> $pass
         );

       try {
         $client = new SoapClient($url);
         $result = $client->CompletePaymentWalletHSENCC($serviceArguments);
       } catch (SoapFault $fault) {

        redirect('/en/api_access_error');

      }

      $ResultCode = $result->CompletePaymentWalletHSENCCResult->Result->ResultCode;
      $ResultText = $result->CompletePaymentWalletHSENCCResult->Result->ResultText;
      if ($ResultText=="OK"&&$ResultCode==0) {
        $receipt = $result->CompletePaymentWalletHSENCCResult->ReceiptNo;
        $paid = $result->CompletePaymentWalletHSENCCResult->Paid;


        try {
              $data=array('receiptno'=>$receipt,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$tenant,'amount'=>$amountpaid,'estateid'=>$physicaladdress,'houseno'=>$houseno,'houseowner'=>$tenant,'arrears'=>$RentArrears,'amountdue'=>$amountdue,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$cashier);

                $query = "insert into hrents (`receiptno`,`issuedate`,`from`,`amount`,`estateid`,`houseno`,`houseowner`,`arrears`,`amountdue`,`username`,`cashiername`)";
                $query.= " values ('".implode("','",$data)."')";
                $this->db->query($query);
              
            } catch (Exception $e) {}


        try {
              $tel = $this->session->userdata('jpwnumber');
              $amnt= number_format($amountpaid, 2, '.', ',');
              $msg ="Receipt Number $receipt. A rent payment of $paid for house number $houseno occupied by $tenant  at $physicaladdress has been received by Nairobi City County.Powered by JamboPay.";
              $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

              $APIKey = urlencode($key);
              $Phone = urlencode($tel);
              $relayCode = urlencode("WebTribe");
              $Message = urlencode($msg);
              $Shortcode = urlencode("700273");
              $CampaignId = urlencode("112623");
              $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
              $ch=curl_init();
              curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
              $result = curl_exec($ch);
              curl_close($ch);
                  //   $ph= $this->session->userdata('jpwnumber'); //"254726981598";//substr($phone,-9);
                  //   #$gsm="254".$ph;
                  //   $msg="Receipt Number $receipt. A rent payment of $paid for house number $houseno occupied by $tenant  at $physicaladdress has been received by Nairobi City County.Powered by JamboPay.";
                  // #sms
                  //   $X_HOST ="http1.uk.oxygen8.com";
                  //   $X_URL = "/WebTribe";
                  //   $X_PORT ="8080";
                  //   $X_USERNAME = "WebTribe";
                  //   $X_PASSWORD = "web@12";
                  //  $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
                  //  $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
                  //  $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
                  //  $s_POST_DATA .= "&MSISDN=$ph"; // Phone
                  //  $s_POST_DATA .= "&Content=$msg";
                  //  $s_POST_DATA .= "&DataType=0"; //Data Type
                  //  $s_POST_DATA .= "&Premium=1"; //Premium
                  //  $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
                  //  $s_POST_DATA .= "&Multipart=1";
                  //  $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
                  //  $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
                  //  $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
                  //  $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
                  //  $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
                  //  $s_Request .="\r\n".$s_POST_DATA;

                   
                  //  $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
                  //  if(fputs ($fp, $s_Request)){

                  //  };
                  //  ///while (!feof($fp)) { $s_GatewayResponse .= fgets ($fp, 128); }
                  //  fclose ($fp);
                 } catch (Exception $e) {

                 }

                 $data = array(
                    "estatename"=>$physicaladdress,
                    "housenumber"=>$houseno,
                    "tenant"=>$tenant,
                    "amountpaid"=>$paid,
                    "receiptno"=>$receipt,
                    "rescode"=>$ResultCode
                  );

                 return $data;
               }
               else{
                return $data = array("rescode"=>$ResultCode);
              }
            }

        function printrentreceipt($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $receiptno = $this->uri->segment(3);
        $receiptno = str_replace('-', '/', $receiptno);
        $fileName =APPPATH.'/assets/back/receipts/houseRent.pdf';
        $query="select * from hrents where receiptno='$receiptno'";
        $result=$this->db->query($query);
        #var_dump($result);die();
        $result=$result->row();
        $receiptno=$result->receiptno;
        $date=$result->issuedate; 
        $paidby=$result->from;
        $amount=$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for='MONTHLY HOUSE RENT PAYMENT, HOUSE NUMBER '.$result->houseno;
        $house_owner=strtoupper($result->houseowner);
        $arrears=$result->arrears;
        $total_amount_due=$result->amountdue;
        $balance_due=$result->amountdue-$result->amount;
        $cashier=strtoupper($result->cashiername);
        $arrears=number_format(str_replace( ',', '', $arrears),2);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($house_owner, 170, 325);
        $page->drawText($cashier, 127, 38);
        $page->drawText($arrears, 450, 274);
        $page->drawText($total_amount_due, 450, 252);
        $page->drawText($amount, 450, 230);
        $page->drawText($balance_due, 450, 209);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

  function printrentreceiptNew($houseno){
        $token=$this->session->userdata('token');
        $houseno=str_replace('_', '/', $this->uri->segment(4));
        $estateid = $this->uri->segment(3);
        $stream = "rent";
        $key0 = "HouseNumber";
        $value0 = urlencode($houseno);
        $key1 = "TransactionStatus";
        $value1 = "1";
        $key2 = "EstateID";
        $value2 = $estateid;

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1&[2].Key=$key2&[2].Value=$value2";

        //$url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result); #echo"<pre>";var_dump($res); die();

        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $receiptno = $res['0']->ReceiptNumber;
        $fileName =APPPATH.'/assets/back/receipts/houseRent.pdf';
        $date=$res['0']->TransactionDate;
        $date = substr($date, 0,10); 
        $paidby=$res['0']->Names;
        $amount=number_format($res['0']->Amount,2);
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $for='HOUSE NUMBER '.$res['0']->HouseNumber." in ".$res['0']->PhysicalAddress." Estate";
        $house_owner=strtoupper($res['0']->CustomerNames);
        $arrears=$res['0']->RentArras;
        $total_amount_due=$res['0']->MonthlyRent + $res['0']->RentArras;
        //$balance_due=$res['0']->CurrentBalance;
        $balance_due=$total_amount_due-$amount;
        $cashier=$res['0']->Names;

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 110, 375); 
        $page->drawText($house_owner, 170, 325);
        //$page->drawText($cashier, 127, 38);
        $page->drawText($arrears, 450, 274);
        $page->drawText($total_amount_due, 450, 252);
        $page->drawText($amount, 450, 230);
        $page->drawText($balance_due, 450, 209);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

    function printrentreceiptmanual(){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'/assets/back/receipts/houseRent.pdf';
        $receiptno = "1111221334/000124802";// $this->uri->segment(3);
        $receiptno = str_replace('-', '/', $receiptno);
        #$query="select * from hrents where receiptno='$receiptno'";
        #$result=$this->db->query($query);
        #$result=$result->row();
        #$receiptno=$result->receiptno;
        $date= "2014-10-03";// substr($result->issuedate, 0,10);  
        $paidby="TOM OTIENO";//$result->from;
        $amount="7000";//$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for='MONTHLY HOUSE RENT PAYMENT, HOUSE NUMBER C32';//$result->houseno;
        $house_owner="ALBERT ANYANGO";//strtoupper($result->houseowner);
        $arrears="0";//$result->arrears;
        $total_amount_due="7000";//$result->amountdue;
        $balance_due="0";//$result->amountdue-$result->amount;
        $cashier="";//strtoupper($result->cashiername);
        $arrears=number_format(str_replace( ',', '', $arrears),2);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($house_owner, 170, 325);
        $page->drawText($cashier, 127, 38);
        $page->drawText($arrears, 450, 274);
        $page->drawText($total_amount_due, 450, 252);
        $page->drawText($amount, 450, 230);
        $page->drawText($balance_due, 450, 209);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

      function printrentreceiptsearch($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'/assets/back/receipts/houseRent.pdf';
        $receiptno=$result->receiptno;
        $date=substr($result->issuedate, 0,10);  
        $paidby=$result->from;
        $amount=$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for='MONTHLY HOUSE RENT PAYMENT, HOUSE NUMBER '.$result->houseno;
        $house_owner=strtoupper($result->houseowner);
        $arrears=$result->arrears;
        $total_amount_due=$result->amountdue;
        $balance_due=$result->amountdue-$result->amount;
        $cashier=strtoupper($result->cashiername);
        $arrears=number_format(str_replace( ',', '', $arrears),2);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

      //   $query="select transid from chequedetails where transid=? and type='hrents'";
      //   $res=$this->db->query($query,array($result->id));
      //   if($res->num_rows()>0){
      //     $ptype="CHEQUE";
      // }else{
      //     $ptype="KSH";
      // }
      $paymenttype="PAYMENT METHOD:  KES";


      $pdf = Zend_Pdf::load($fileName);
      $page=$pdf->pages[0];

      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

      $page->drawText($receiptno, 180, 528);
      $page->drawText($date, 450, 528);
      $page->drawText($paidby, 220, 478);
      $page->drawText($amount, 450, 478);
      $page->drawText($amount_in_words, 130, 427);
      $page->drawText($for, 150, 375); 
      $page->drawText($house_owner, 170, 325);
      #$page->drawText($cashier, 127, 38);
      $page->drawText($arrears, 450, 274);
      $page->drawText($total_amount_due, 450, 252);
      $page->drawText($amount, 450, 230);
      $page->drawText($balance_due, 450, 209);
      #$page->drawText($paymenttype, 73, 20);

      $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
      $rendererOptions = array(
        'topOffset' => 600,
        'leftOffset' =>295
        );
      $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

      $pdfData = $pdf->render(); 
      header("Content-Disposition: inline; filename=receipt.pdf"); 
      header("Content-type: application/x-pdf"); 
      echo $pdfData;
  }
  function get_receipt_details($receipts,$receipt_no)
  {
    $receipts=json_decode($receipts,true);
    foreach($receipts as $key => $receipt) {
      if($receipt['ReceiptNumber']==$receipt_no){
        return $receipts[$key];
      } else{
        return $receipts[0];
      }
    }
  }
  function printrentreceiptsearchNew($houseno,$estateid,$receipt_no=''){
        $token=$this->session->userdata('token');
        $stream = "rent";
        $key0 = "HouseNumber";
        $value0 = urlencode($houseno);
        $key1 = "TransactionStatus";
        $value1 = "1";
        $key2 = "EstateID";
        $value2 = $estateid;

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1&[2].Key=$key2&[2].Value=$value2";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $receipt_details=$this->get_receipt_details($result,$receipt_no);        
       
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'/assets/back/receipts/houseRent.pdf';
        $receiptno=$receipt_details['ReceiptNumber'];
        $date=$receipt_details['TransactionDate']; 
        $date=substr($date, 0,10);
        $paidby=$receipt_details['Names'];
        $amount=number_format($receipt_details['Amount'],2);
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $for='HOUSE NUMBER '.$receipt_details['HouseNumber']." in ".$receipt_details['PhysicalAddress']." Estate";
        $house_owner=strtoupper($receipt_details['CustomerNames']);
        $arrears=$receipt_details['RentArras'];
        $total_amount_due=$receipt_details['MonthlyRent'] + $receipt_details['RentArras'];
        //$balance_due=$res['0']->CurrentBalance;
        $balance_due=$total_amount_due-$amount;
        $cashier=$receipt_details['Names'];

        // $arrears=number_format(str_replace( ',', '', $arrears),2);
        // $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        // $balance_due=number_format(str_replace( ',', '', $balance_due),2);

      //   $query="select transid from chequedetails where transid=? and type='hrents'";
      //   $res=$this->db->query($query,array($result->id));
      //   if($res->num_rows()>0){
      //     $ptype="CHEQUE";
      // }else{
      //     $ptype="KSH";
      // }
      $paymenttype="PAYMENT METHOD:  KES";


      $pdf = Zend_Pdf::load($fileName);
      $page=$pdf->pages[0];

      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

      $page->drawText($receiptno, 180, 528);
      $page->drawText(date($date), 450, 528);
      $page->drawText($paidby, 220, 478);
      $page->drawText($amount, 450, 478);
      $page->drawText($amount_in_words, 130, 427);
      $page->drawText($for, 110, 375); 
      $page->drawText($house_owner, 170, 325);
      #$page->drawText($cashier, 127, 38);
      $page->drawText($arrears, 450, 274);
      $page->drawText($total_amount_due, 450, 252);
      $page->drawText($amount, 450, 230);
      $page->drawText($balance_due, 450, 209);
      #$page->drawText($paymenttype, 73, 20);

      $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
      $rendererOptions = array(
        'topOffset' => 600,
        'leftOffset' =>295
        );
      $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

      $pdfData = $pdf->render(); 
      header("Content-Disposition: inline; filename=receipt.pdf"); 
      header("Content-type: application/x-pdf"); 
      echo $pdfData;
  }

  function housecheck(){
    $estateid = $this->input->post('estate');
    $hno = $this->input->post('house');
    $username=$this->session->userdata('jpwnumber');
    $result= $this->db->query("Select estateid,houseno,receiptno from hrents where houseno='$hno'  order by issuedate DESC limit 1 ");
    if($result->num_rows()>0){
      $result = $result->row();
      return $result->receiptno;
    }else{
      return redirect("rents/reprintrent/erro1");
    }

  }

  function housecheckNew(){
    $estateid = $this->input->post('estate');
    $hno = $this->input->post('house');
    $token=$this->session->userdata('token');
    $stream = "rent";
    $key = "HouseNumber";
    $value = urlencode($hno);
    $key1 = "EstateID";
    $value1 = "$estateid";
    $key2 = "TransactionStatus";
    $value2 = "1";


    $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key1&[1].Value=$value1&[2].Key=$key2&[2].Value=$value2";
    echo $url;exit;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    #curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer '.$token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));

    $result = curl_exec( $ch );

    //echo $result;exit;
    $res = json_decode($result,true); #var_dump($res); die();

    //echo json_encode($res);exit;

    return $res;
    /*if($res['0']->HouseNumber==$hno){
      return array('hNo'=>$res['0']->HouseNumber,'eID'=>$res['0']->EstateID);
    }else{
      return redirect("rents/reprintrentNew/erro1");
    }*/

  }

  function insertReceiptDetails(){
    $estate = $this->input->post('estate');
    $house = $this->input->post('house');
    $date = $this->input->post('issuedate');
    $date1 =substr($date,0,10); //date("Y-m-d",$date); 
    $receiptno = $this->input->post('receiptno');
    // $regno = $this->input->post('regno');
    // $regno = strtoupper($regno);
    $paidby = $this->input->post('paidby');
    $paidby = mysql_real_escape_string($paidby);
    $amount = $this->input->post('amount');
    $amountdue = $this->input->post('amountdue');
    $arrears = $this->input->post('arrears');
    $houseowner = $this->input->post('houseowner');
    $phoneno = $this->input->post('phonenumber');
    $channel = "Manual";
    #var_dump($date1); die();
    try {
     $data=array('receiptno'=>$receiptno,'issuedate'=>$date1,'from'=>$paidby,'amount'=>$amount,'estateid'=>$estate,'houseno'=>$house,'houseowner'=>$houseowner,'arrears'=>$arrears,'amountdue'=>$amountdue,'username' =>$phoneno,'cashiername' =>$paidby,'channel' =>$channel);#var_dump($data); die();
     $this->db->insert('hrents',$data);
    // $query = "insert into hrents (`receiptno`,`issuedate`,`from`,`amount`,`estateid`,`houseno`,`houseowner`,`arrears`,`amountdue`,`username`,`cashiername`,`channel`)";
    // $query.= " values ('".implode("','",$data)."')";
    // $this->db->query($query);
                redirect('rents/reprintrent');
    } catch (Exception $e) {
      redirect('rents/ManualRentReceiptInsert');
    }
  }

}