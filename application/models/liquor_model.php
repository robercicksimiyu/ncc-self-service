<?php

class Liquor_model extends CI_Model {



	function getRetailLiquorDetails(){

		$mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');

        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));


        // UPLOAD CONFIG
        $config = array();
        $config['upload_path']  = './liquorfiles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';

        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];

        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {           
        	$namesArray[$i] = $files['userfile']['name'][$i];
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload();
        }



        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=>$agentRef,
        	"businessId"=>$businessId,
        	"calendarYear"=>$year,
        	"custMobileNo"=>$Tel_no,
        	"pass"=> $pass

        	);

        try {
        	$client = new SoapClient($mainurl);
        } catch (Exception $e) {
        	redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

        	$url="http://54.218.79.241/mainsector.asmx?wsdl";
        	$apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        	$serviceArgs = array(
        	"API_Id" =>$apiID,
        	"Fullname"=>$fullnames,
        	"Email"=>$email,
        	"Tel_no"=>$Tel_no,
			"BusinessName"=>$bizname,//$Firm,
			"Owner"=>$fullnames,//$Occupation,
			"PlotNo"=> $plotnumber,
			"LRNO"=> $plotnumber,
			"Form_Id"=> "LF01",
			"Building"=>$physicaladdress,
			"category_id"=>$category,
			"subcounty"=>"dagoretti south",
			"zone"=>"ngando",

            "vat_certificate"=>base_url().'liquorfiles/'.$namesArray[0],
			"pin_certificate"=>base_url().'liquorfiles/'.$namesArray[1],
			"health_certificate"=>base_url().'liquorfiles/'.$namesArray[2],
			"identification_document"=> base_url().'liquorfiles/'.$namesArray[3],
			"nema_certificate"=>base_url().'liquorfiles/'.$namesArray[4]
			);


            
        	try {
        		$client = new SoapClient($url);
        		$result = $client->PostRetailliquorApplication($serviceArgs);
        	//var_dump($result); die();
        		$RefID = $result->PostRetailliquorApplicationResult->string["0"];
        		$InvoiceNo = $result->PostRetailliquorApplicationResult->string["3"];
        	} catch (Exception $e) {
        		redirect('liquor/error');
        	}

        	if($RefID!="400"){

        		$serviceArguments2 = array(
        			"API_Id" =>$apiID,
        			"RefId"=>$RefID
        		#"category"=>$category
        			);
        		$result2 = $client->GetRetailInvoiceDetails($serviceArguments2); 
        		#var_dump($result2); die();
        		$Fullname = $result2->GetRetailInvoiceDetailsResult->Name;
        		$Email = $result2->GetRetailInvoiceDetailsResult->Email;
        		$Firm = $result2->GetRetailInvoiceDetailsResult->BusinessName;
        		$InvoiceNo = $result2->GetRetailInvoiceDetailsResult->InvoiceNum;
        		$Tel_no = $result2->GetRetailInvoiceDetailsResult->Tel_no;
        		$Owner = $result2->GetRetailInvoiceDetailsResult->Owner;
        		$Building = $result2->GetRetailInvoiceDetailsResult->Building;
        		$Category = $result2->GetRetailInvoiceDetailsResult->category;
        		$Status = $result2->GetRetailInvoiceDetailsResult->status;
        		$RefID = $result2->GetRetailInvoiceDetailsResult->RefId;
        		$Amount = $result2->GetRetailInvoiceDetailsResult->amount;
        		$certAmount = $result2->GetRetailInvoiceDetailsResult->certificate_amount;

        		$data = array(
        			"RefID"=>$RefID,
        			"issueDate"=>date("Y/m/d"),
        			"Fullname"=>$Fullname,
        			"Email"=>$Email,
        			"TelNumber"=>$Tel_no,
        			"Firm"=>$Firm,
        			"InvoiceNo"=>$InvoiceNo,
        			"Status"=>$Status,
        			"Owner"=> $Owner,
        			"Amount"=>$Amount,
        			"certAmount"=>$certAmount,
        			"Building"=>$Building,
        			"Category"=>$Category,
        			"form_id"=>"LF01"
        			);

        		try {
        			$this->db->insert('retailliquor',$data);	
        		} catch (Exception $e) {

        		}
	        #var_dump($data); die();

        		return $data;
        	}elseif($RefID=="400" || $RefID=="403"){
        		redirect('liquor/error');
        	}
        }else{
        	redirect('liquor/invalidBID');
        }
    }

    function completeRetailLiquorInspectionPayment(){

		$url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

		$Refid=str_replace(" ","",$this->input->post('refid'));
		$InvoiceNum= $this->input->post('InvoiceNo');
		$Amount= $this->input->post('amount');
		$certAmount = $this->input->post('certamount');
		
		$paymentinfo = array(
			"API_Id"=>$apiID,
			"Refid" => $Refid,
			"InvoiceNum"=>$InvoiceNum,
			"amount" => $Amount,
			"certificate_amount"=>"",
			"year"=> date("Y"),
			"form_id"=>"LF01"
			);

		$client  = new SoapClient($url);
		$result  = $client->$paymentinfo;
		@$rescode = $result->PostretailPaymentResult; #var_dump($result); die();
        if(isset($rescode)){
		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"InvoiceNo" => $this->input->post('InvoiceNo'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"rescode"=>$rescode
		);
		#var_dump($rescode); die();
		return $data;
        }
        else{
        redirect('liquor/error');
        }
	}

    function payRetailLiquorCertDetails(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
        $serviceArguments2 = array(
                "API_Id" =>$apiID,
                "RefId"=>$RefID
                #"category"=>$category
                );
            $client2 = new SoapClient($url);
            $result2 = $client2->GetRetailInvoiceDetails($serviceArguments2); 

            @$rescode = $result2->GetRetailInvoiceDetailsResult->Response_Code;
            if (!isset($rescode) ){
            $Fullname = $result2->GetRetailInvoiceDetailsResult->Name;
            $Email = $result2->GetRetailInvoiceDetailsResult->Email;
            $Firm = $result2->GetRetailInvoiceDetailsResult->BusinessName;
            $InvoiceNo = $result2->GetRetailInvoiceDetailsResult->InvoiceNum;
            $Tel_no = $result2->GetRetailInvoiceDetailsResult->Tel_no;
            $Owner = $result2->GetRetailInvoiceDetailsResult->Owner;
            $Building = $result2->GetRetailInvoiceDetailsResult->Building;
            $Category = $result2->GetRetailInvoiceDetailsResult->category;
            $Status = $result2->GetRetailInvoiceDetailsResult->status;
            $RefID = $result2->GetRetailInvoiceDetailsResult->RefId;
            $Amount = $result2->GetRetailInvoiceDetailsResult->amount;
            $certAmount = $result2->GetRetailInvoiceDetailsResult->certificate_amount;

            $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category
                    );
            try {
            } catch (Exception $e) {

            }
            #var_dump($data); die();
            return $data;
        }
        else{
        redirect('liquor/invalidRefid');
        }
    }

    function completeRetailLiquorCertPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => "",
            "certificate_amount"=>$certAmount,
            "year"=> date("Y"),
            "form_id"=>"LF01"
            );

        $client = new SoapClient($url);
        $result = $client->PostretailPayment($paymentinfo);
        $rescode = $result->PostretailPaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $InvoiceNum,
        "Refid" => $Refid,
        "certAmount" => $certAmount,
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }

    function getImportLiquorDetails(){

        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $Alcoholcontent = $this->input->post('alcoholcontent');
        $Transportmode = $this->input->post('transportmode');
        $Brand = $this->input->post('brand');
        $Unitcapacity = $this->input->post('unitcapacity');
        $Purpose = $this->input->post('purpose');
        $importcost = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));


        // UPLOAD CONFIG
        $config = array();
        $config['upload_path']  = './liquorfiles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {           
        	$namesArray[$i] = $files['userfile']['name'][$i];
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload();
        }



        $serviceArguments = array(
            "userName"=>$username,
            "agentRef"=>$agentRef,
            "businessId"=>$businessId,
            "calendarYear"=>$year,
            "custMobileNo"=>$Tel_no,
            "pass"=> $pass

            );

        try {
            $client = new SoapClient($mainurl);
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

            $url="http://54.218.79.241/mainsector.asmx?wsdl";
            $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

            $serviceArgs = array(
            "API_Id" =>$apiID,
            "Fullname"=>$fullnames,
            "Email"=>$email,
            "Tel_no"=>$Tel_no,
            "BusinessName"=>$bizname,//$Firm,
            "Owner"=>$fullnames,//$Occupation,
            "PlotNo"=> $plotnumber,
            "LRNO"=> $plotnumber,
            "Alcoholcontent"=>$Alcoholcontent,
            "Mode_transportation"=>$Transportmode,
            "Type_Brand"=>$Brand,//$Firm,
            "Unit_capacity"=>$Unitcapacity,//$Occupation,
            "Purpose_use"=> $Purpose,
            "import_cost"=> $importcost,
            "Form_Id"=> "LF02",
            "Building"=>$physicaladdress,
            "category_id"=>$category,
            "subcounty"=>"dagoretti south",
            "zone"=>"ngando",

            "vat_certificate"=>base_url().'liquorfiles/'.$namesArray[0],
			"pin_certificate"=>base_url().'liquorfiles/'.$namesArray[1],
			"health_certificate"=>base_url().'liquorfiles/'.$namesArray[2],
			"identification_document"=> base_url().'liquorfiles/'.$namesArray[3],
			"nema_certificate"=>base_url().'liquorfiles/'.$namesArray[4],
            );

            try {
                $client = new SoapClient($url);
                $result = $client->PostimportApplication($serviceArgs);
            #var_dump($result); die();
                $RefID = $result->PostimportApplicationResult->string["0"];
                $InvoiceNo = $result->PostimportApplicationResult->string["3"];
            } catch (Exception $e) {
                redirect('liquor/error');
            }

            if($RefID!="400"){

                $serviceArguments2 = array(
                    "API_Id" =>$apiID,
                    "RefId"=>$RefID
                #"category"=>$category
                    );
                $result2 = $client->GetImportInvoiceDetails($serviceArguments2); 
                #var_dump($result2); die();
                $Fullname = $result2->GetImportInvoiceDetailsResult->Name;
                $Email = $result2->GetImportInvoiceDetailsResult->Email;
                $Firm = $result2->GetImportInvoiceDetailsResult->BusinessName;
                $InvoiceNo = $result2->GetImportInvoiceDetailsResult->InvoiceNum;
                $Tel_no = $result2->GetImportInvoiceDetailsResult->Tel_no;
                $Owner = $result2->GetImportInvoiceDetailsResult->Owner;
                $Building = $result2->GetImportInvoiceDetailsResult->Building;
                $Category = $result2->GetImportInvoiceDetailsResult->category;
                $Status = $result2->GetImportInvoiceDetailsResult->status;
                $RefID = $result2->GetImportInvoiceDetailsResult->RefId;
                $Amount = $result2->GetImportInvoiceDetailsResult->amount;
                $certAmount = $result2->GetImportInvoiceDetailsResult->certificate_amount;

                $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category,
                    "form_id"=>"LF02"
                    );

                try {
                    $this->db->insert('retailliquor',$data);    
                } catch (Exception $e) {

                }
            #var_dump($data); die();

                return $data;
            }elseif($RefID=="400" || $RefID=="403"){
                redirect('liquor/error');
            }
        }else{
            redirect('liquor/invalidBID');
        }
    }

    function completeImportLiquorInspectionPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => $Amount,
            "certificate_amount"=>"",
            "year"=> date("Y"),
            "form_id"=>"LF02"
            );

        $client = new SoapClient($url);
        $result = $client->PostretailPayment($paymentinfo);
        $rescode = $result->PostretailPaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $this->input->post('InvoiceNo'),
        "Refid" => $this->input->post('refid'),
        "Amount" => $this->input->post('amount'),
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }

    function payImportLiquorCertDetails(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
        $serviceArguments2 = array(
                "API_Id" =>$apiID,
                "RefId"=>$RefID
                #"category"=>$category
                );
            $client2 = new SoapClient($url);
            $result2 = $client2->GetImportInvoiceDetails($serviceArguments2); 

            @$rescode = $result2->GetImportInvoiceDetailsResult->Response_Code;
            if (!isset($rescode) ){
            $Fullname = $result2->GetImportInvoiceDetailsResult->Name;
            $Email = $result2->GetImportInvoiceDetailsResult->Email;
            $Firm = $result2->GetImportInvoiceDetailsResult->BusinessName;
            $InvoiceNo = $result2->GetImportInvoiceDetailsResult->InvoiceNum;
            $Tel_no = $result2->GetImportInvoiceDetailsResult->Tel_no;
            $Owner = $result2->GetImportInvoiceDetailsResult->Owner;
            $Building = $result2->GetImportInvoiceDetailsResult->Building;
            $Category = $result2->GetImportInvoiceDetailsResult->category;
            $Status = $result2->GetImportInvoiceDetailsResult->status;
            $RefID = $result2->GetImportInvoiceDetailsResult->RefId;
            $Amount = $result2->GetImportInvoiceDetailsResult->amount;
            $certAmount = $result2->GetImportInvoiceDetailsResult->certificate_amount;

            $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category
                    );
            try {
            } catch (Exception $e) {

            }
            #var_dump($data); die();
            return $data;
        }
        else{
        redirect('liquor/invalidRefid');
        }
    }

    function completeImportLiquorCertPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => "",
            "certificate_amount"=>$certAmount,
            "year"=> date("Y"),
            "form_id"=>"LF01"
            );

        $client = new SoapClient($url);
        $result = $client->PostImportPayment($paymentinfo);
        $rescode = $result->PostImportPaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $InvoiceNum,
        "Refid" => $Refid,
        "certAmount" => $certAmount,
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }


    function getManufactureLiquorDetails(){

        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $Alcoholcontent = $this->input->post('alcoholcontent');
        $Transportmode = $this->input->post('transportmode');
        $Brand = $this->input->post('brand');
        $Unitcapacity = $this->input->post('unitcapacity');
        $Purpose = $this->input->post('purpose');
        $importcost = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));
        #$this->

        // UPLOAD CONFIG
        $config = array();
        $config['upload_path']  = './liquorfiles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {           
        	$namesArray[$i] = $files['userfile']['name'][$i];
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload();
        }



        $serviceArguments = array(
            "userName"=>$username,
            "agentRef"=>$agentRef,
            "businessId"=>$businessId,
            "calendarYear"=>$year,
            "custMobileNo"=>$Tel_no,
            "pass"=> $pass

            );

        try {
            $client = new SoapClient($mainurl);
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

            $url="http://54.218.79.241/mainsector.asmx?wsdl";
            $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

            $serviceArgs = array(
            "API_Id" =>$apiID,
            "Fullname"=>$fullnames,
            "Email"=>$email,
            "Tel_no"=>$Tel_no,
            "BusinessName"=>$bizname,//$Firm,
            "Owner"=>$fullnames,//$Occupation,
            "PlotNo"=> $plotnumber,
            "LRNO"=> $plotnumber,
            "Type_of_business" =>$biztype, 
            "Alcoholcontent"=>$Alcoholcontent,
            "Mode_transportation"=>$Transportmode,
            "Type_Brand"=>$Brand,//$Firm,
            "Unit_capacity"=>$Unitcapacity,//$Occupation,
            "Purpose_use"=> $Purpose,
            "import_cost"=> $importcost,
            "Form_Id"=> "LF03",
            "Building"=>$physicaladdress,
            "category_id"=>$category,
            "subcounty"=>"dagoretti south",
            "zone"=>"ngando",
            "vat_certificate"=>base_url().'liquorfiles/'.$namesArray[0],
			"pin_certificate"=>base_url().'liquorfiles/'.$namesArray[1],
			"health_certificate"=>base_url().'liquorfiles/'.$namesArray[2],
			"identification_document"=> base_url().'liquorfiles/'.$namesArray[3],
			"nema_certificate"=>base_url().'liquorfiles/'.$namesArray[4],
            "kebs_certificate"=>base_url().'liquorfiles/'.$namesArray[5],
            "excise_return"=>base_url().'liquorfiles/'.$namesArray[6],
            "Architectural_drawing"=>base_url().'liquorfiles/'.$namesArray[7]
            );


            try {
                $client = new SoapClient($url);
                $result = $client->PostManufactureApplication($serviceArgs);
            #var_dump($result); die();
                $RefID = $result->PostManufactureApplicationResult->string["0"];
                $InvoiceNo = $result->PostManufactureApplicationResult->string["3"];
            } catch (Exception $e) {
                redirect('liquor/error');
            }

            if($RefID!="400"){

                $serviceArguments2 = array(
                    "API_Id" =>$apiID,
                    "RefId"=>$RefID
                #"category"=>$category
                    );
                $result2 = $client->GetManufactureInvoiceDetails($serviceArguments2); 
                #var_dump($result2); die();
                $Fullname = $result2->GetManufactureInvoiceDetailsResult->Name;
                $Email = $result2->GetManufactureInvoiceDetailsResult->Email;
                $Firm = $result2->GetManufactureInvoiceDetailsResult->BusinessName;
                $InvoiceNo = $result2->GetManufactureInvoiceDetailsResult->InvoiceNum;
                $Tel_no = $result2->GetManufactureInvoiceDetailsResult->Tel_no;
                $Owner = $result2->GetManufactureInvoiceDetailsResult->Owner;
                $Building = $result2->GetManufactureInvoiceDetailsResult->Building;
                $Category = $result2->GetManufactureInvoiceDetailsResult->category;
                $Status = $result2->GetManufactureInvoiceDetailsResult->status;
                $RefID = $result2->GetManufactureInvoiceDetailsResult->RefId;
                $Amount = $result2->GetManufactureInvoiceDetailsResult->amount;
                $certAmount = $result2->GetManufactureInvoiceDetailsResult->certificate_amount;

                $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category,
                    "form_id"=>"LF03"
                    );

                try {
                    $this->db->insert('retailliquor',$data);    
                } catch (Exception $e) {

                }
            #var_dump($data); die();

                return $data;
            }elseif($RefID=="400" || $RefID=="403"){
                redirect('liquor/error');
            }
        }else{
            redirect('liquor/invalidBID');
        }
    }

    function completeManufactureLiquorInspectionPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => $Amount,
            "certificate_amount"=>"",
            "year"=> date("Y"),
            "form_id"=>"LF03"
            );

        $client = new SoapClient($url);
        $result = $client->PostManufacturePayment($paymentinfo);
        $rescode = $result->PostManufacturePaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $this->input->post('InvoiceNo'),
        "Refid" => $this->input->post('refid'),
        "Amount" => $this->input->post('amount'),
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }


    function payManufactureLiquorCertDetails(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
        $serviceArguments2 = array(
                "API_Id" =>$apiID,
                "RefId"=>$RefID
                #"category"=>$category
                );
            $client2 = new SoapClient($url);
            $result2 = $client2->GetManufactureInvoiceDetails($serviceArguments2); 

            @$rescode = $result2->GetManufactureInvoiceDetailsResult->Response_Code;
            if (!isset($rescode) ){
            $Fullname = $result2->GetManufactureInvoiceDetailsResult->Name;
            $Email = $result2->GetManufactureInvoiceDetailsResult->Email;
            $Firm = $result2->GetManufactureInvoiceDetailsResult->BusinessName;
            $InvoiceNo = $result2->GetManufactureInvoiceDetailsResult->InvoiceNum;
            $Tel_no = $result2->GetManufactureInvoiceDetailsResult->Tel_no;
            $Owner = $result2->GetManufactureInvoiceDetailsResult->Owner;
            $Building = $result2->GetManufactureInvoiceDetailsResult->Building;
            $Category = $result2->GetManufactureInvoiceDetailsResult->category;
            $Status = $result2->GetManufactureInvoiceDetailsResult->status;
            $RefID = $result2->GetManufactureInvoiceDetailsResult->RefId;
            $Amount = $result2->GetManufactureInvoiceDetailsResult->amount;
            $certAmount = $result2->GetManufactureInvoiceDetailsResult->certificate_amount;

            $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category
                    );
            try {
            } catch (Exception $e) {

            }
            #var_dump($data); die();
            return $data;
        }
        else{
        redirect('liquor/invalidRefid');
        }
    }

    function completeManufactureLiquorCertPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => "",
            "certificate_amount"=>$certAmount,
            "year"=> date("Y"),
            "form_id"=>"LF03"
            );

        $client = new SoapClient($url);
        $result = $client->PostManufacturePayment($paymentinfo);
        $rescode = $result->PostManufacturePaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $InvoiceNum,
        "Refid" => $Refid,
        "certAmount" => $certAmount,
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }

    function getTemporaryLiquorDetails(){

        $mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        //$category = $this->input->post('category');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));
        #$this->


           // UPLOAD CONFIG
        $config = array();
        $config['upload_path']  = './liquorfiles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        
        for($i=0; $i<$cpt; $i++)
        {           
        	$namesArray[$i] = $files['userfile']['name'][$i];
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload();
        }

        $serviceArguments = array(
            "userName"=>$username,
            "agentRef"=>$agentRef,
            "businessId"=>$businessId,
            "calendarYear"=>$year,
            "custMobileNo"=>$Tel_no,
            "pass"=> $pass

            );

        try {
            $client = new SoapClient($mainurl);
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

            $url="http://54.218.79.241/mainsector.asmx?wsdl";
            $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

            $serviceArgs = array(
            "API_Id" =>$apiID,
            "Fullname"=>$fullnames,
            "Email"=>$email,
            "Tel_no"=>$Tel_no,
            "BusinessName"=>$bizname,//$Firm,
            "Owner"=>$fullnames,//$Occupation,
            "PlotNo"=> $plotnumber,
            "LRNO"=> $plotnumber,
            "Form_Id"=> "LF05",
            "Building"=>$physicaladdress,
            "category_id"=>"58",
            "subcounty"=>"dagoretti south",
            "zone"=>"ngando",
            
            "vat_certificate"=>base_url().'liquorfiles/'.$namesArray[0],
			"pin_certificate"=>base_url().'liquorfiles/'.$namesArray[1],
			"health_certificate"=>base_url().'liquorfiles/'.$namesArray[2],
			"identification_document"=> base_url().'liquorfiles/'.$namesArray[3],
			"nema_certificate"=>base_url().'liquorfiles/'.$namesArray[4]
            );


            try {
                $client = new SoapClient($url);
                $result = $client->PostTemporaryApplication($serviceArgs);
            #var_dump($result); die();
                $RefID = $result->PostTemporaryApplicationResult->string["0"];
                $InvoiceNo = $result->PostTemporaryApplicationResult->string["3"];
            } catch (Exception $e) {
                redirect('liquor/error');
            }

            if($RefID!="400"){

                $serviceArguments2 = array(
                    "API_Id" =>$apiID,
                    "RefId"=>$RefID
                #"category"=>$category
                    );
                $result2 = $client->GettemporaryInvoiceDetails($serviceArguments2); 
                #var_dump($result2); die();
                $Fullname = $result2->GettemporaryInvoiceDetailsResult->Name;
                $Email = $result2->GettemporaryInvoiceDetailsResult->Email;
                $Firm = $result2->GettemporaryInvoiceDetailsResult->BusinessName;
                $InvoiceNo = $result2->GettemporaryInvoiceDetailsResult->InvoiceNum;
                $Tel_no = $result2->GettemporaryInvoiceDetailsResult->Tel_no;
                $Owner = $result2->GettemporaryInvoiceDetailsResult->Owner;
                $Building = $result2->GettemporaryInvoiceDetailsResult->Building;
                $Category = $result2->GettemporaryInvoiceDetailsResult->category;
                $Status = $result2->GettemporaryInvoiceDetailsResult->status;
                $RefID = $result2->GettemporaryInvoiceDetailsResult->RefId;
                $Amount = $result2->GettemporaryInvoiceDetailsResult->amount;
                $certAmount = $result2->GettemporaryInvoiceDetailsResult->certificate_amount;

                $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category,
                    "form_id"=>"LF05"
                    );

                try {
                    $this->db->insert('retailliquor',$data);    
                } catch (Exception $e) {

                }
            #var_dump($data); die();

                return $data;
            }elseif($RefID=="400" || $RefID=="403"){
                redirect('liquor/error');
            }
        }else{
            redirect('liquor/invalidBID');
        }
    }

    function completeTemporaryLiquorInspectionPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => "",
            "certificate_amount"=>$certAmount,
            "year"=> date("Y"),
            "form_id"=>"LF05"
            );

        $client = new SoapClient($url);
        $result = $client->PostretailPayment($paymentinfo);
        $rescode = $result->PostretailPaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $this->input->post('InvoiceNo'),
        "Refid" => $this->input->post('refid'),
        "Amount" => $this->input->post('amount'),
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }

    function getTransferLiquorDetails(){

		$mainurl = MAIN_URL;
        $username = USER_NAME; //"97763838";
        $key = JP_KEY;// "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->input->post('biz_id');
        $email = $this->input->post('email');
        $fullnames = $this->input->post('fullnames');
        $year = date("Y");
        $Tel_no = $this->input->post('phoneno');
        $category = $this->input->post('category');
        $transferadd = $this->input->post('transferadd');
        $agentRef = strtoupper(substr(md5(uniqid()),25));
        $ck = $username . $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));
     	
     	    // UPLOAD CONFIG
        $config = array();
        $config['upload_path']  = './liquorfiles/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        
        for($i=0; $i<$cpt; $i++)
        {           
        	$namesArray[$i] = $files['userfile']['name'][$i];
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload();
        }



        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=>$agentRef,
        	"businessId"=>$businessId,
        	"calendarYear"=>$year,
        	"custMobileNo"=>$Tel_no,
        	"pass"=> $pass

        	);

        try {
        	$client = new SoapClient($mainurl);
        } catch (Exception $e) {
        	redirect('/sbp/not_found');
        }
        $result = $client->GetIssuedSBPNCC($serviceArguments); #var_dump($result); die();
        $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
        $year=$result->GetIssuedSBPNCCResult->CalenderYear;
        $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
        $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
        $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
        $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
        $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
        $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
        $pobox=$result->GetIssuedSBPNCCResult->POBox;
        $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
        $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
        $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
        $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
        $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        $physicaladdress=$result->GetIssuedSBPNCCResult->PhysicalAddress;
        $resultcode=$result->GetIssuedSBPNCCResult->Result->ResultCode;
        $building=$result->GetIssuedSBPNCCResult->Building;
        $approvalstatus=$result->GetIssuedSBPNCCResult->ApprovalStatus; #var_dump($result); die();

        if($resultcode=="0"){

        	$url="http://54.218.79.241/mainsector.asmx?wsdl";
        	$apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        	$serviceArgs = array(
        	"API_Id" =>$apiID,
        	"Fullname"=>$fullnames,
        	"Email"=>$email,
        	"Tel_no"=>$Tel_no,
			"BusinessName"=>$bizname,//$Firm,
			"Owner"=>$fullnames,//$Occupation,
			"PlotNo"=> $plotnumber,
			"LRNO"=> $plotnumber,
			"Form_Id"=> "LF01",
			"Building"=>$physicaladdress,
			"category_id"=>$category,
			"subcounty"=>"dagoretti south",
			"zone"=>"ngando",
			"transferee_address"=>$transferadd,

			"vat_certificate"=>base_url().'liquorfiles/'.$namesArray[0],
			"pin_certificate"=>base_url().'liquorfiles/'.$namesArray[1],
			"health_certificate"=>base_url().'liquorfiles/'.$namesArray[2],
			"identification_document"=> base_url().'liquorfiles/'.$namesArray[3],
			"nema_certificate"=>base_url().'liquorfiles/'.$namesArray[4]
			);

        	try {
        		$client = new SoapClient($url);
        		$result = $client->PosttransferApplication($serviceArgs);
        	#var_dump($result); die();
        		$RefID = $result->PosttransferApplicationResult->string["0"];
        		$InvoiceNo = $result->PosttransferApplicationResult->string["3"];
        	} catch (Exception $e) {
        		redirect('liquor/error');
        	}

        	if($RefID!="400"){

        		$serviceArguments2 = array(
        			"API_Id" =>$apiID,
        			"RefId"=>$RefID
        		#"category"=>$category
        			);
        		$result2 = $client->GetTransferInvoiceDetails($serviceArguments2); 
        		#var_dump($result2); die();
        		$Fullname = $result2->GetTransferInvoiceDetailsResult->Name;
        		$Email = $result2->GetTransferInvoiceDetailsResult->Email;
        		$Firm = $result2->GetTransferInvoiceDetailsResult->BusinessName;
        		$InvoiceNo = $result2->GetTransferInvoiceDetailsResult->InvoiceNum;
        		$Tel_no = $result2->GetTransferInvoiceDetailsResult->Tel_no;
        		$Owner = $result2->GetTransferInvoiceDetailsResult->Owner;
        		$Building = $result2->GetTransferInvoiceDetailsResult->Building;
        		$Category = $result2->GetTransferInvoiceDetailsResult->category;
        		$Status = $result2->GetTransferInvoiceDetailsResult->status;
        		$RefID = $result2->GetTransferInvoiceDetailsResult->RefId;
        		$Amount = $result2->GetTransferInvoiceDetailsResult->amount;
        		$certAmount = $result2->GetTransferInvoiceDetailsResult->certificate_amount;

        		$data = array(
        			"RefID"=>$RefID,
        			"issueDate"=>date("Y/m/d"),
        			"Fullname"=>$Fullname,
        			"Email"=>$Email,
        			"TelNumber"=>$Tel_no,
        			"Firm"=>$Firm,
        			"InvoiceNo"=>$InvoiceNo,
        			"Status"=>$Status,
        			"Owner"=> $Owner,
        			"Amount"=>$Amount,
        			"certAmount"=>$certAmount,
        			"Building"=>$Building,
        			"Category"=>$Category,
        			"form_id"=>"LF04"
        			);

        		try {
        			$this->db->insert('retailliquor',$data);	
        		} catch (Exception $e) {

        		}
	        #var_dump($data); die();

        		return $data;
        	}elseif($RefID=="400" || $RefID=="403"){
        		redirect('liquor/error');
        	}
        }else{
        	redirect('liquor/invalidBID');
        }
    }

    function completeTransferLiquorInspectionPayment(){

		$url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

		$Refid=str_replace(" ","",$this->input->post('refid'));
		$InvoiceNum= $this->input->post('InvoiceNo');
		$Amount= $this->input->post('amount');
		$certAmount = $this->input->post('certamount');
		
		$paymentinfo = array(
			"API_Id"=>$apiID,
			"Refid" => $Refid,
			"InvoiceNum"=>$InvoiceNum,
			"amount" => $Amount,
			"certificate_amount"=>"",
			"year"=> date("Y"),
			"form_id"=>"LF04"
			);

		$client = new SoapClient($url);
		$result = $client->PostTransferPayment($paymentinfo);
		$rescode = $result->PostTransferPaymentResult; #var_dump($result); die();

		$data = array(
		"Fullname" => $this->input->post('fullname'),
		"Email" => $this->input->post('email'),
		"Tel_no" => $this->input->post('telnumber'),
		"Firm" => $this->input->post('firm'),
		"InvoiceNo" => $this->input->post('InvoiceNo'),
		"Refid" => $this->input->post('refid'),
		"Amount" => $this->input->post('amount'),
		"rescode"=>$rescode
		);
		#var_dump($rescode); die();
		return $data;
	}

	function payTransferLiquorCertDetails(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $RefID = strtoupper(str_replace(" ","",$this->input->post('refid')));
        $serviceArguments2 = array(
                "API_Id" =>$apiID,
                "RefId"=>$RefID
                #"category"=>$category
                );
            $client2 = new SoapClient($url);
            $result2 = $client2->GetTransferInvoiceDetails($serviceArguments2); 

            @$rescode = $result2->GetTransferInvoiceDetailsResult->Response_Code;
            if (!isset($rescode) ){
            $Fullname = $result2->GetTransferInvoiceDetailsResult->Name;
            $Email = $result2->GetTransferInvoiceDetailsResult->Email;
            $Firm = $result2->GetTransferInvoiceDetailsResult->BusinessName;
            $InvoiceNo = $result2->GetTransferInvoiceDetailsResult->InvoiceNum;
            $Tel_no = $result2->GetTransferInvoiceDetailsResult->Tel_no;
            $Owner = $result2->GetTransferInvoiceDetailsResult->Owner;
            $Building = $result2->GetTransferInvoiceDetailsResult->Building;
            $Category = $result2->GetTransferInvoiceDetailsResult->category;
            $Status = $result2->GetTransferInvoiceDetailsResult->status;
            $RefID = $result2->GetTransferInvoiceDetailsResult->RefId;
            $Amount = $result2->GetTransferInvoiceDetailsResult->amount;
            $certAmount = $result2->GetTransferInvoiceDetailsResult->certificate_amount;

            $data = array(
                    "RefID"=>$RefID,
                    "issueDate"=>date("Y/m/d"),
                    "Fullname"=>$Fullname,
                    "Email"=>$Email,
                    "TelNumber"=>$Tel_no,
                    "Firm"=>$Firm,
                    "InvoiceNo"=>$InvoiceNo,
                    "Status"=>$Status,
                    "Owner"=> $Owner,
                    "Amount"=>$Amount,
                    "certAmount"=>$certAmount,
                    "Building"=>$Building,
                    "Category"=>$Category
                    );
            try {
            } catch (Exception $e) {

            }
            #var_dump($data); die();
            return $data;
        }
        else{
        redirect('liquor/invalidRefid');
        }
    }

    function completeTransferLiquorCertPayment(){

        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";

        $Refid=str_replace(" ","",$this->input->post('refid'));
        $InvoiceNum= $this->input->post('InvoiceNo');
        $Amount= $this->input->post('amount');
        $certAmount = $this->input->post('certamount');
        
        $paymentinfo = array(
            "API_Id"=>$apiID,
            "Refid" => $Refid,
            "InvoiceNum"=>$InvoiceNum,
            "amount" => "",
            "certificate_amount"=>$certAmount,
            "year"=> date("Y"),
            "form_id"=>"LF04"
            );

        $client = new SoapClient($url);
        $result = $client->PostTransferPayment($paymentinfo);
        $rescode = $result->PostTransferPaymentResult; #var_dump($result); die();

        $data = array(
        "Fullname" => $this->input->post('fullname'),
        "Email" => $this->input->post('email'),
        "Tel_no" => $this->input->post('telnumber'),
        "Firm" => $this->input->post('firm'),
        "InvoiceNo" => $InvoiceNum,
        "Refid" => $Refid,
        "certAmount" => $certAmount,
        "rescode"=>$rescode
        );
        #var_dump($rescode); die();
        return $data;
    }


    // Check Liquor Details
    function checkLiquorStatus($refid){
        $url="http://54.218.79.241/mainsector.asmx?wsdl";
        $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
        $Refid = strtoupper($refid);
        $data  = array("RefId"=>$Refid,"API_Id"=>$apiID);

        try {
        $client = new SoapClient($url);
        $result = $client->PrintRetailPermit($data);  #var_dump($result); die();
        } catch (Exception $e) {
            redirect('health/APIerror');
        }
     

        if (!isset($rescode) ){
        $result = $client->PrintRetailPermit($data);
        $Fullname=$result->PrintRetailPermitResult->Name;
        $Firm=$result->PrintRetailPermitResult->BusinessName;
        $InvoiceNum=$result->PrintRetailPermitResult->InvoiceNum;
        $Email=$result->PrintRetailPermitResult->Email;
        $TelNumber=$result->PrintRetailPermitResult->Tel_no;
        $Owner=$result->PrintRetailPermitResult->Owner;
        $amount=$result->PrintRetailPermitResult->amount;
        $Refid=$result->PrintRetailPermitResult->RefId;
        $inspection_status=$result->PrintRetailPermitResult->inspection_status;
        $Building=$result->PrintRetailPermitResult->Building;
        $Category=$result->PrintRetailPermitResult->category;

        $data = array(
            "RefID"=>$Refid,
            "issueDate"=>date("Y/m/d"),
            "Fullname"=>$Fullname,
            "Email"=>$Email,
            "TelNumber"=>$TelNumber,
            "Firm"=>$Firm,
            "Status"=>$inspection_status,
            "Amount"=>$amount,
            "Category"=>$Category,
            "Building"=>$Building
            );
        return $data;
        
        }
        else{
        redirect('health/invalidRefid');
        }
    }

    // Print Retail Liquor Certificate
    function printLiquorRetailPermitreceipt($refid){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/RetailLiquor.pdf';
       
        $query="SELECT * FROM `retailliquor` WHERE `form_id` = 'LF01' and `RefID`='$refid' ORDER BY RefID desc";
        $result=$this->db->query($query);
        $result=$result->row();
  

        $Fullname = $result->Fullname;
        $Email = $result->Email;
        $Tel_no = $result->TelNumber;
        $Firm = $result->Firm;
        $Category =$result->Category;
        
        $Owner = $result->Owner;
        $PlotNo = $result->Building;
        $LRNO = $result->Building;
        #$Frontingon = $result->Frontingon;
        #$Building = $result->Building;
        #$FloorNo = $result->FloorNo;
        $Refid = $result->RefID;
        $Amount = $result->certAmount;
        $Receiptno = "111122333/00002588";
        $expiryDate = "2016-07-16";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);

        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=RetailLiquorReceipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

    // Print Import Permit Certificate
    function printLiquorImportPermitreceipt($refid){
        $this->load->library('zend');
        $a    = $this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/ImportLiquor.pdf';

        $query="SELECT * FROM `retailliquor` WHERE  `RefID`='$refid' ORDER BY RefID desc";
        $result=$this->db->query($query);
        $result=$result->row();



        var_dump($result);

        $Fullname = $result->Fullname;
        $Email = $result->Email;
        $Tel_no = $result->TelNumber;
        $Firm = $result->Firm;
        $Category =$result->Category;
        
        $Owner = $result->Owner;
        $PlotNo = $result->Building;
        $LRNO = $result->Building;
        #$Frontingon = $result->Frontingon;
        #$Building = $result->Building;
        #$FloorNo = $result->FloorNo;
        $Refid = $result->RefID;
        $Amount = $result->certAmount;
        $Receiptno = "111122333/00002588";
        $expiryDate = "2016-07-16";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=ImportLiqour.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

     // Print Manufacturer Certificate
    function printLiquorManufacturerPermitreceipt($refid){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/ManufactureLiquor.pdf';

        $query="SELECT * FROM `retailliquor` WHERE  `RefID`='$refid' ORDER BY RefID desc";
        $result=$this->db->query($query);
        $result=$result->row();

        $Fullname = $result->Fullname;
        $Email = $result->Email;
        $Tel_no = $result->TelNumber;
        $Firm = $result->Firm;
        $Category =$result->Category;
        
        $Owner = $result->Owner;
        $PlotNo = $result->Building;
        $LRNO = $result->Building;
        #$Frontingon = $result->Frontingon;
        #$Building = $result->Building;
        #$FloorNo = $result->FloorNo;
        $Refid = $result->RefID;
        $Amount = $result->certAmount;
        $Receiptno = "111122333/00002588";
        $expiryDate = "2016-07-16";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=ManufacturerCertificate.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

     // Print Temporary Certificate
    function printLiquorTemporaryPermitreceipt($refid){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/TemporaryPermit.pdf';
        // $refid = $this->uri->segment(3);
        
        $query="SELECT * FROM `retailliquor` WHERE  `RefID`='$refid' ORDER BY RefID desc";
        $result=$this->db->query($query);
        $result=$result->row(); 

        $Fullname = $result->Fullname;
        $Email = $result->Email;
        $Tel_no = $result->TelNumber;
        $Firm = $result->Firm;
        $Category =$result->Category;
        
        $Owner = $result->Owner;
        $PlotNo = $result->Building;
        $LRNO = $result->Building;
        #$Frontingon = $result->Frontingon;
        #$Building = $result->Building;
        #$FloorNo = $result->FloorNo;
        $Refid = $result->RefID;
        $Amount = $result->certAmount;
        $Receiptno = "111122333/00002588";
        $expiryDate = "2016-07-16";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=TemporaryPermit.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }

     // Print Transfer Certificate
    function printLiquorTransferPermitreceipt($refid){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/Transfer.pdf';
        
        $query="SELECT * FROM `retailliquor` WHERE  `RefID`='$refid' ORDER BY RefID desc";
        $result=$this->db->query($query);
        $result=$result->row(); 

        $Fullname = $result->Fullname;
        $Email = $result->Email;
        $Tel_no = $result->TelNumber;
        $Firm = $result->Firm;
        $Category =$result->Category;
        
        $Owner = $result->Owner;
        $PlotNo = $result->Building;
        $LRNO = $result->Building;
        #$Frontingon = $result->Frontingon;
        #$Building = $result->Building;
        #$FloorNo = $result->FloorNo;
        $Refid = $result->RefID;
        $Amount = $result->certAmount;
        $Receiptno = "111122333/00002588";
        $expiryDate = "2016-07-16";

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText(strtoupper($Firm), 210, 440);
        #$page->drawText(strtoupper($PlotNo), 500, 428);
        $page->drawText(strtoupper($LRNO), 190, 414);
        #$page->drawText(strtoupper($Building), 400, 412); 
        $page->drawText(strtoupper($Category), 190, 398);
        $page->drawText(number_format($Amount,2), 150, 275); 
        $page->drawText($Receiptno, 150, 255);
        $page->drawText($expiryDate, 170, 230);
        $page->drawText($Refid, 510, 630);
        // $page->drawText(number_format($penalties,2), 450, 274);
        // $page->drawText(number_format($total_amount_due,2), 450, 252);
        // $page->drawText(number_format($amount,2), 450, 230);
        // $page->drawText(number_format($balance_due,2), 450, 209);

        $barcodeOptions = array('text' => $Receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=TransferCertificate.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //redirect('en/land_rates');
    }
    


}