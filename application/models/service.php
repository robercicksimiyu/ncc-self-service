<?php
class Common_model extends CI_Model{

  public function traceAPI($type,$method,$log)
  {
      $log_data=array(
          "type"=>$type,
          "method"=>$method,
          "log"=>$log,
          );
      $this->db->insert('econstruction_logs',$log_data);
  }

  public function get_business_details($bid)
  {
    $this->db->where('bid',$bid);
    return $this->db->get('bid_gabage_info')->row_array();
  }

  public function save_business_details($bid)
  {    
    $details=array(); 
    $this->load->model('sbp_model'); 
    //$bid_details=json_decode($this->sbp_model->getLastPayment($bid),true);   
   // var_dump(count($bid_details)) ;exit;
    $business_details=$this->check_bid_exitance($bid);
    //echo count($business_details)   ;exit;
    if(count($business_details)) {
        $details=$business_details;
        $details['invoice_number']=$this->create_invoices($bid);        
        $details['amount_paid']=$this->get_payment_type_amount($details['payment_type_id']);  
    } else {
      $category=($this->input->post('sub_category')) ? $this->input->post('sub_category') : $this->input->post('category');
      $bid_details=$this->sbp_model->getBusinessDetails($bid,2017);          
      $details=array(        
          'contact_person'=>$this->input->post('contact_person'),
          'phone_number'=>$this->input->post('phone_number'),
          'id_number'=>$this->input->post('id_number'),
          'bid_name'=>$bid_details['Name'],
          'bid'=>$bid,
          'bid_location'=>$bid_details['PhysicalAddress'],
          'zone'=>$bid_details['ZoneID'],
          'ward'=>$bid_details['WardID'],
          'building'=>$bid_details['PremisesArea'],
          'bid_lr_no'=>$bid_details['PlotNumber'],
          'bid_activity'=>$bid_details['ActivityName'],
          'bid_activity_id'=>$bid_details['ActivityID'],
          'payment_type_id '=> $category,
      );      
      $this->db->insert('bid_gabage_info',$details);
      
      $details['invoice_number']=$this->create_invoices($bid,$category);
      $details['amount_paid']=$this->get_payment_type_amount($category);      
    }

    /*if(count($bid_details) < 1) {
       $details=array(        
        'contact_person'=>$this->input->post('contact_person'),
        'phone_number'=>$this->input->post('phone_number'),
        'id_number'=>$this->input->post('id_number'),
        'bid_name'=>$bid_details[0]['BusinessName'],
        'bid'=>$bid,
        'bid_location'=>$bid_details[0]['PhysicalAddress'],
        'zone'=>$bid_details[0]['ZoneCode'],
        'ward'=>$bid_details[0]['WardCode'],
        'building'=>$bid_details[0]['Building'],
      );

       // /var_dump($details);exit;
      

      //echo $this->get_payment_type_amount($category);exit;

      $business_details=$this->check_bid_exitance($bid);     
      var_dump($business_details);exit;
      if(!count($business_details)) {
        //echo "sdd";echo json_encode($details);exit;exit;
        //$this->db->reset();
        $this->db->insert('bid_gabage_info',$details);
        //
        //echo $this->db->last_query();exit;
        $details['invoice_number']=$this->create_invoices($bid);
        $details['amount_paid']=$this->get_payment_type_amount($category);
      } else {
        $details=$business_details;
        $details['invoice_number']=$this->create_invoices($bid);        
        $details['amount_paid']=$this->get_payment_type_amount($category);        
      }      
      
    }*/
    
    return $details;
  }



  public function check_bid_exitance($bid)
  {
    $this->db->where('bid',$bid);
    $check_bid=$this->db->get('bid_gabage_info');
    if($check_bid->num_rows()){
      return $check_bid->row_array();
    } else {
      return array();
    }
  }

  public function get_categories()
  {
    $this->db->where('parent',0);
    return $this->db->get('gabage_categories')->result_array();
  }

  public function get_sub_categories($category_id)
  {
    $this->db->where('parent',$category_id);
    return $this->db->get('gabage_categories')->result_array();
  }



  public function check_business_invoices($bid)
  {
    $this->db->where('bid',$bid);
    $this->db->where('status',0);
    $check_unpaid_invoices=$this->db->get('gabage_invoices');
    return $check_unpaid_invoices->num_rows();
  }

  public function get_payment_type_amount($id)
  {
    $this->db->where('id',$id);
    $type_details=$this->db->get('gabage_categories')->row_array();
   
    return $type_details['amount'];
  }

  public function create_invoices($bid,$category=1)
  {    
    $invoices=$this->get_invoices($bid);
    //$this->db->reset_query();
    if(count($invoices)){
      return $invoices;
    } else {
        $invoice_details=array(
        'invoice_number'=>$this->generate_ref_number(3,$bid),
        'bid'=>$bid,
        'ref_number'=>$this->generate_random_invoice_numbers(),
        'payment_type_id'=>$category,
        'status'=>0
      );      
      $this->db->insert('gabage_invoices',$invoice_details);      
      return $invoice_details;
    }
    
  }

  public function renewal()
  {
    //$details['invoice_number']=$this->create_invoices($this->input->post('bizId'));
    $business_details=$this->check_bid_exitance($this->input->post('bizId')); 
    if(!count($business_details)) {
      redirect('garbage');
    }    
    if(!count($business_details)) {
      //echo "sdd";echo json_encode($details);exit;exit;
      //$this->db->reset();
      $this->db->insert('bid_gabage_info',$details);
      //
      //echo $this->db->last_query();exit;
      $details['invoice_number']=$this->create_invoices($this->input->post('bizId'));
      $details['amount_paid']=$this->get_payment_type_amount(1);
    } else {
      $details=$business_details;
      $details['invoice_number']=$this->create_invoices($this->input->post('bizId'));        
      $details['amount_paid']=$this->get_payment_type_amount(1);        
    }

    return $details;
  }

  public function get_invoices($bid,$paid=false,$invoice=false)
  {
    if($paid) {
      if($invoice) {
        $data=array(
          'ref_number'=>$invoice,
          'status'=>1
          );
      } else {
        $data=array(
          'bid'=>$bid,
          'status'=>1
          );
      }
      $invoices=$this->db->get_where('gabage_invoices',$data)->row_array();
    } else {
      $invoices=$this->db->get_where('gabage_invoices',array(
      'bid'=>$bid,
      'status'=>0
      ))->row_array();
    }  
  
    return $invoices;
  }



  function generate_ref_number($length = 6,$bid) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return 'GBCOL'.$bid.$randomString;
  }

  function generate_random_invoice_numbers()
  {
    return 'GB'.rand(0, 99999);
  } 


  public function business_information() {
    return $this->save_business_details($this->input->post('bid'));    
  }

  public function check_status($invoice_number)
  {
    $this->db->where('invoice_number',$invoice_number);
    return $this->db->get('gabage_payment')->num_rows();
  }



  public function make_payments()
  {    
     $message=array();
     $post_data=$this->input->post();
     $jpw_pin=$post_data['jp_pin'];
     $message['invoice']=$post_data['invoice_number'];
     $post_data['zone']=$post_data['zones'];
     unset($post_data['zones']);
     unset($post_data['jp_pin']);
     unset($post_data['invoice_number']);
     $invoice_details=$this->update_invoice_status($message['invoice']);
    
     if(count($invoice_details)) {
      if(count($post_data)) {
        $this->db->where('bid',$invoice_details['bid']);
        $this->db->update('bid_gabage_info',$post_data);

        //echo $this->db->last_query();exit;
        //echo "An update should be going on";
      }
      $tosend = array(
          'Stream' => "merchantpayment",
          'PhoneNumber' => $this->input->post('jpwnumber'),          
          'PaymentTypeID' => 1,//"2014",
          'MerchantCode' => '123456',//"2014",
          'ProductName' => 'Gabage',//"2014",
          'Amount' => $this->get_payment_type_amount($invoice_details['payment_type_id']),
          'PaymentTypeID' => '1',
          'InvoiceNumber' => $message['invoice'],
          'Items' => 1,
          'PaidBy' => $this->session->userdata('name'),

          ); 

      foreach ($tosend as $key => $value) {
          $post_items[] = $key . '=' . $value;
      }
      $post_string = implode('&', $post_items);

      //echo $post_string;exit;

      $url = "http://192.168.11.22/JamboPayServices/api/payments/post";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer ' . $this->session->userdata['token'],
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec($ch);

      echo $result;exit;

      
      $message['code']=200;
      $message['info']="Payment has been successfully";
           
     } else {
        $message['code']=404;
        $message['info']="Invoice was not found";
     }

     return $message;
  }


  

  function update_business_location($bid,$location)
  {
    $this->db->where('bid',$bid);
    return $this->db->update('bid_gabage_info',array('bid_location'=>$location));
    
  }

  function update_invoice_status($ref_number)
  {   
    $this->db->where('ref_number',$ref_number);
    $this->db->update('gabage_invoices',array('status'=>1)); 

  
    $this->db->where('ref_number',$ref_number);  
    $invoice_details=$this->db->get('gabage_invoices')->row_array();    
    $this->create_payments($invoice_details);
    return $invoice_details;    
   
  }

  function create_payments($invoice_details) {
    //var_dump($invoice_details);exit;
     $data=array(
      'amount_paid'=>$this->get_payment_type_amount($invoice_details['payment_type_id']),
      'invoice_number'=>$invoice_details['ref_number'],
      'paid_by'=>$this->session->userdata('name'),
      'receipt_number'=>$this->generate_ref_number(17,$invoice_details['bid']),      
      );
     //var_dump($data);exit;
     $this->db->insert('gabage_payment',$data);

  }

    function printReceipt($invoice_number=null){
        $bid=null;
        if($this->input->post('bid')) {
          $bid=$this->input->post('bid');
        }
        
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');
        if($invoice_number!=null) {
          $invoice_details=$this->get_invoices($bid,true,$invoice_number);         
        } else {
          $invoice_details=$this->get_invoices($bid,true);
        }


        //echo $invoice_details['bid'];exit;
        
        
        $business_details=$this->get_business_details($invoice_details['bid']);

        

        $payment_details=$this->db->get_where('gabage_payment',array('invoice_number'=>$invoice_details['ref_number']))->row_array();
        

        $fileName =APPPATH.'/assets/back/receipts/gabbage_collection.pdf';
        
        $username=$payment_details['paid_by'];;#var_dump($fileName);die();
        $receiptno=$payment_details['receipt_number'];
        $date=substr($payment_details['transaction_time'], 0,10); 
        $paidby=strtoupper($payment_details['paid_by']);
        $amount=$payment_details['amount_paid'];
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($payment_details['amount_paid']))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for="INVOICE NUMBER: ".$payment_details['invoice_number'];
        $total_amount_due=$payment_details['amount_paid'];
        $balance_due='0.00';
        $cashier=strtoupper($payment_details['paid_by']);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

      //   $query="select transid from chequedetails where transid=? and type='hrents'";
      //   $res=$this->db->query($query,array($result->id));
      //   if($res->num_rows()>0){
      //     $ptype="CHEQUE";
      // }else{
      //     $ptype="KSH";
      // }
      $paymenttype="PAYMENT METHOD:  KES";


      $pdf = Zend_Pdf::load($fileName);
      $page=$pdf->pages[0];

      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

      $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($cashier, 127, 38);
        $page->drawText($total_amount_due, 450, 300);
        $page->drawText($amount, 450, 278);
        $page->drawText($balance_due, 450, 257);

     /* $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
      $rendererOptions = array(
        'topOffset' => 600,
        'leftOffset' =>295
        );
      $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();*/

      $pdfData = $pdf->render(); 
      header("Content-Disposition: inline; filename=receipt.pdf"); 
      header("Content-type: application/x-pdf"); 
      echo $pdfData;
  }


	function prepareConstruction(){

		$username = USER_NAME;
        $key = JP_KEY;
    	$timestamp = date("Y-m-d H:i:s");
        $invoiceNumber = $this->input->post('invoice') ;
        $invoiceNumber = str_replace(' ', '', $invoiceNumber);
        $jpwMobileNo = $this->session->userdata('jpwnumber');
        $agentRef = rand(0, 1000);// $timestamp;
        $ck = $username.$jpwMobileNo.$agentRef.$invoiceNumber.$key;
        
         //pass = SHA1(userName+jpwMobileNo+billNumber+agentRef+key)
                 
        $pass = sha1(utf8_encode($ck));

        $serviceArguments = array(
          "userName"=>$username,
          "agentRef"=> $agentRef,
          "invoiceNumber"=>$invoiceNumber,
          "jpwMobileNo"=> $jpwMobileNo,
          "pass"=> $pass

        );


	    try {
	    	
	    		$client = new SoapClient(MAIN_URL, array('cache_wsdl' => WSDL_CACHE_NONE));
	    	
	    } catch (Exception $e) {
	    	redirect('sbp/not_found');
	    }
	    
	    $result = $client->PreparePaymentWalletECNNCC($serviceArguments);
	    $resultcode=$result->PreparePaymentWalletECNNCCResult->Result->ResultCode;
	    $resulttext=$result->PreparePaymentWalletECNNCCResult->Result->ResultText;
	    $invoiceDate=$result->PreparePaymentWalletECNNCCResult->DateOfInvoice;
	    $invoiceNo=$result->PreparePaymentWalletECNNCCResult->InvoiceNumber;
	    $invoiceStatus=$result->PreparePaymentWalletECNNCCResult->InvoiceStatus;
	    $fullName=$result->PreparePaymentWalletECNNCCResult->UserFullName;
	    $email=$result->PreparePaymentWalletECNNCCResult->UserEmail;
	    $transactionId=$result->PreparePaymentWalletECNNCCResult->TransactionID;
	    $totalAmount=$result->PreparePaymentWalletECNNCCResult->TotalAmount;
	    $userMobile=$result->PreparePaymentWalletECNNCCResult->UserMobile;
	    if ($resulttext=="OK" && $resultcode==0) {
	    	$data=array(
	    		'invoiceDate'=>$invoiceDate,
	    		'invoiceNo'=>$invoiceNo,
	    		'invoiceStatus'=>$invoiceStatus,
	    		'fullName'=>$fullName,
	    		'email'=>$email,
	    		'transactionId'=>$transactionId,
	    		'totalAmount'=>$totalAmount,
	    		'userMobile'=>$userMobile,
	    		'rescode'=>$resultcode,
	    		'restext'=>$resulttext
	    		);
	    	return $data; }
        else {
	    	return $data = array('restext'=>$resulttext,'rescode'=>$resultcode);
	       }
	    
	}

  function prepareConstructionNew(){

    $token = $this->session->userdata['token'] ;
    $invoiceNumber = $this->input->post('invoice') ; #var_dump($invoiceNumber);
    #$invoiceNumber = str_replace(' ', '', $invoiceNumber);
    $jpwMobileNo = $this->session->userdata('jpwnumber');

    $tosend = array(
      'InvoiceNumber'=>$invoiceNumber,
      'PhoneNumber'=>$jpwMobileNo,
      'Stream'=>"econstruction",
      'PaymentTypeID' => "1",
      'PaidBy'=>$this->session->userdata('name'),    
    );
  //var_dump($tosend);exit;

  foreach ( $tosend as $key => $value) {
    $post_items[] = $key . '=' . $value;
  }
  $post_string = implode ('&', $post_items);

  $url = REST_URL."/post";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: bearer '.$token,
    'app_key:'.APP_KEY
    ));

  $result = curl_exec($ch);
  $this->traceAPI('POST','confirm-post-prepare',$result);
  $res = json_decode($result);

  //echo $result;exit;

  if (!isset($res->ErrorCode)) {        
    $data=array(
      'invoiceDate'=>$res->InvoiceDate,
      'invoiceNo'=>$res->InvoiceNumber,
      'invoiceStatus'=>$res->InvoiceStatus,
      'fullName'=>$res->FullNames,
      'email'=>$res->Email,
      'transactionId'=>$res->TransactionID,
      'totalAmount'=>$res->Amount,
      'userMobile'=>$res->PhoneNumber,
      'rescode'=>0,
      'restext'=>'OK'
      );
    return $data; 
  } else {
    return $data = array('restext'=>$res->Message,'rescode'=>$res->ErrorCode);
  }

  }



  function completeConstructionPayment(){    
    $token = $this->session->userdata['token'] ;
    $invoiceNumber=$this->input->post('invoiceno');    
    $tranid =$this->input->post('transid');
    $amount = $this->input->post('amount');

    $tosend = array(
      'Stream' => 'econstruction',
      'TransactionID'=>$tranid,
      'Pin' => $this->input->post('jp_pin'),
      'PhoneNumber' =>$this->session->userdata('jpwnumber'),
      'PaymentTypeID' => '1',
      );
    try {  
      $url = REST_URL;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer ' . $token,
        'app_key: '.APP_KEY
        ));
    } catch (Exception $e) {
      redirect('sbp/not_found');
    }
    $result = curl_exec($ch);
    $res = json_decode($result);
    curl_close($ch);

    if(!isset($res->ErrorCode)){   
      try {
        $data=array(
          'receiptno'=>$res->ReceiptNumber,
          'issuedate'=>date('Y-m-d h:i:s',time()),
          'from'=>$res->FullNames,
          'amount'=>$res->Amount,
          'invoiceno'=>$res->InvoiceNumber,
          'username' =>$res->PhoneNumber,
          'cashiername' =>$res->FullNames,
          'channel'=>'E-Payments',
          );
        $query = "insert into `econstruction` (`receiptno`,`issuedate`,`from`,`amount`,`invoiceno`,`username`,`cashiername`,`channel`)";
        $query.= " values ('".implode("','",$data)."')";
        $this->db->query($query);
      } catch (Exception $e) {}
      #end set in db
     /* try {
        $tel = $this->session->userdata('jpwnumber');
        $amnt= number_format($paid, 2, '.', ',');
        $msg ="Receipt Number $receiptno.Your e-Construction payment of KES $amnt for $invoiceNumber has been received by Nairobi City County.Powered by: JamboPay";
        $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

        $APIKey = urlencode($key);
        $Phone = urlencode($tel);
        $relayCode = urlencode("WebTribe");
        $Message = urlencode($msg);
        $Shortcode = urlencode("700273");
        $CampaignId = urlencode("112623");
        $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $result = curl_exec($ch);
        curl_close($ch);
      } catch (Exception $e) {

      }
*/
      $data = array(
        'rescode'=>$ResultCode,
        'restext'=>$ResultText,
        'paid'=>$paid,
        'receiptno'=>$receiptno,
        'customer'=>$this->session->userdata('name')
        );
      return $data;
    }
    else {
      $data = array(
        'rescode'=>$res->ErrorCode,
        'restext'=>$res->Message,
        );
      return $data;
    }


  }
	/*function completeConstructionPayment(){
		$invoiceNumber=$this->input->post('invoiceno');
		$username = USER_NAME;
		$key = JP_KEY;
		$tranid =$this->input->post('transid');
		$amount = $this->input->post('amount');
		$channelRef = strtoupper(substr(md5(uniqid()),25)); 
		$currency = "KES";
		$jpPIN = $this->input->post('jp_pin');
		$customer = $this->session->userdata('name');
        $phone = $this->session->userdata('jpwnumber');
        $channel = "SELFSERVICE";


		$ck = $username .$tranid . $key;
		$pass = sha1(utf8_encode($ck ));

		$params = array(
			"userName"=>$username,
			"transactionId"=>$tranid,
			"jpPIN"=>$jpPIN,     
			"amount"=>$amount, 
			"currency"=> $currency,
			"channelRef"=> $channelRef,
			"pass"=> $pass

			);

		try {
			$client = new SoapClient(MAIN_URL);
			$result = $client->CompletePaymentWalletECNNCC($params);
		} catch (Exception $e) {
			redirect('sbp/not_found');
		}

		$ResultCode = $result->CompletePaymentWalletECNNCCResult->Result->ResultCode;
		$ResultText = $result->CompletePaymentWalletECNNCCResult->Result->ResultText;

		if($ResultCode==0&&$ResultText=='OK'){
			$paid = $result->CompletePaymentWalletECNNCCResult->Paid;
			$receiptno = $result->CompletePaymentWalletECNNCCResult->ReceiptNo;

        #set in db
        try {
              $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'invoiceno'=>$invoiceNumber,'username' =>$phone,'cashiername' =>$customer,'channel'=>$channel);

        			$query = "insert into `econstruction` (`receiptno`,`issuedate`,`from`,`amount`,`invoiceno`,`username`,`cashiername`,`channel`)";
        			$query.= " values ('".implode("','",$data)."')";
        			$this->db->query($query);

            } catch (Exception $e) {}
        #end set in db
			try {

          $tel = $this->session->userdata('jpwnumber');
              $amnt= number_format($paid, 2, '.', ',');
              $msg ="Receipt Number $receiptno.Your e-Construction payment of KES $amnt for $invoiceNumber has been received by Nairobi City County.Powered by: JamboPay";
              $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

              $APIKey = urlencode($key);
              $Phone = urlencode($tel);
              $relayCode = urlencode("WebTribe");
              $Message = urlencode($msg);
              $Shortcode = urlencode("700273");
              $CampaignId = urlencode("112623");
              $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
              $ch=curl_init();
              curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
              $result = curl_exec($ch);
              curl_close($ch);
               } catch (Exception $e) {

               }

               $data = array(
               	'rescode'=>$ResultCode,
               	'restext'=>$ResultText,
               	'paid'=>$paid,
               	'receiptno'=>$receiptno,
               	'customer'=>$this->session->userdata('name')
               	);
               return $data;
           }
           else {
           	$data = array(
           		'rescode'=>$ResultCode,
           		'restext'=>$ResultText
           		);
           	return $data;
           }


       }

       function PrintEConstruction($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/E-Construction.pdf';
        $receiptno=str_replace('-', '/', $this->uri->segment(3));

        $query="select * from econstruction where receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();
        $username=$result->username;#var_dump($fileName);die();
        $receiptno=$result->receiptno;
        $date=substr($result->issuedate, 0,10); 
        $paidby=strtoupper($result->from);
        $amount=$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for="INVOICE NUMBER: ".$result->invoiceno;
        $total_amount_due=$result->amount;
        $balance_due='0.00';
        $cashier=strtoupper($result->cashiername);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

        

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($cashier, 127, 38);
        $page->drawText($total_amount_due, 450, 300);
        $page->drawText($amount, 450, 278);
        $page->drawText($balance_due, 450, 257);
        #$page->drawText($paymenttype, 73, 20);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 552,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=E-Construction.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;

    }

    function checkECprintReceipt(){
      $invoiceno = $this->input->post('invoiceno');
      $query="Select * from econstruction where invoiceno='$invoiceno' order by issuedate DESC limit 1";
      $result= $this->db->query($query);
      if($result->num_rows()>0){
          $result = $result->row();
          return $result->receiptno;
      }else{
          return redirect("econstruction/reprintECReceipt/erro1");
      }
  }*/

    function checkECprintReceiptNew(){
      $invoiceno = $this->input->post('invoiceno');
      $token=$this->session->userdata('token');
      $stream = "econstruction";
      $key = "InvoiceNumber";
      $value = $invoiceno;
      $key1 = "TransactionStatus";
      $value1 = "1";

      $url = REST_URL."gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key1&[1].Value=$value1";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      #curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: '.APP_KEY
      ));

      $result = curl_exec( $ch );
      $res = json_decode($result);

      if($res['0']->InvoiceNumber==$invoiceno){
      return $res['0']->ReceiptNumber;
      }else{
        return redirect("econstruction/reprintECReceipt/erro1");
      }
    }

    function printEreceiptsearchNew($receiptno){
        $token=$this->session->userdata('token');

        $stream = "econstruction";
        $key = "ReceiptNumber";
        $value = $receiptno;
        $key1 = "TransactionStatus";
        $value1 = "1";

        $url = REST_URL."gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key1&[1].Value=$value1";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: '.APP_KEY
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);

        $fileName =APPPATH.'/assets/back/receipts/E-Construction.pdf';
        $receiptno=$res['0']->ReceiptNumber;
        $date=$res['0']->TransactionDate; 
        $date=substr($date, 0,10); 
        $paidby=$res['0']->Names;
        $amount=$res['0']->Amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for="INVOICE NUMBER: ".$res['0']->InvoiceNumber;
        $total_amount_due=$amount;
        $balance_due='0.00';
        $cashier=strtoupper($paidby);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

      
        $paymenttype="PAYMENT METHOD:  KES";


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

          $page->drawText($receiptno, 180, 528);
          $page->drawText($date, 450, 528);
          $page->drawText($paidby, 220, 478);
          $page->drawText($amount, 450, 478);
          $page->drawText($amount_in_words, 130, 427);
          $page->drawText($for, 150, 375); 
          $page->drawText($cashier, 127, 38);
          $page->drawText($total_amount_due, 450, 300);
          $page->drawText($amount, 450, 278);
          $page->drawText($balance_due, 450, 257);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
          'topOffset' => 600,
          'leftOffset' =>295
          );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

    
}





