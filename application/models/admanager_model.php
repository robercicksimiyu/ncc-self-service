<?php
class Admanager_model extends CI_Model{

  function prepareAD(){
    $username = USER_NAME;
    $key = JP_KEY;
    $timestamp = date("Y-m-d H:i:s");
    $invoiceNumber = $this->input->post('billno') ;
    $invoiceNumber = str_replace(' ', '', $invoiceNumber);
    $jpwMobileNo = $this->session->userdata('jpwnumber');
    $agentRef = rand(0, 1000);// $timestamp;
    $ck = $username.$agentRef.$invoiceNumber.$jpwMobileNo.$key;
    
     //pass = SHA1(userName+jpwMobileNo+billNumber+agentRef+key)

    $pass = sha1(utf8_encode($ck));

    $serviceArguments = array(
      "userName"=>$username,
      "agentRef"=> $agentRef,
      "invoiceNumber"=>$invoiceNumber,
      "jpwMobileNo"=> $jpwMobileNo,
      "pass"=> $pass

      );

    try {
      $client = new SoapClient(MAIN_URL);
    } catch (Exception $e) {
      redirect('sbp/not_found');
    }
    
    $result = $client->PreparePaymentWalletADMNCC($serviceArguments);
    #var_dump($result); die();
    $ResultCode = $result->PreparePaymentWalletADMNCCResult->Result->ResultCode;
    $ResultText = $result->PreparePaymentWalletADMNCCResult->Result->ResultText;
    $Client = $result->PreparePaymentWalletADMNCCResult->Client;
    $ClientAccountNumber = $result->PreparePaymentWalletADMNCCResult->ClientAccountNumber;
    $FeeType = $result->PreparePaymentWalletADMNCCResult->FeeType;
    $InvoiceAmount = $result->PreparePaymentWalletADMNCCResult->InvoiceAmount;
    $InvoiceBlocked = $result->PreparePaymentWalletADMNCCResult->InvoiceBlocked;
    $InvoiceNumber = $result->PreparePaymentWalletADMNCCResult->InvoiceNumber;
    $InvoiceStatus = $result->PreparePaymentWalletADMNCCResult->InvoiceStatus;
    $SignageType = $result->PreparePaymentWalletADMNCCResult->SignageType;
    $SiteNumber = $result->PreparePaymentWalletADMNCCResult->SiteNumber;
    $TransactionId = $result->PreparePaymentWalletADMNCCResult->TransactionID;

    $data = array(
    	"rescode"=>$ResultCode,
    	"restext"=>$ResultText,
    	"client"=>$Client,
    	"clientaccountnumber"=>$ClientAccountNumber,
    	"feetype"=>$FeeType,
    	"invoiceamount"=>$InvoiceAmount,
    	"invoiceblocked"=>$InvoiceBlocked,
    	"invoicenumber"=>$InvoiceNumber,
    	"invoicestatus"=>$InvoiceStatus,
    	"signagetype"=>$SignageType,
    	"sitenumber"=>$SiteNumber,
    	"TransactionId"=>$TransactionId
    	);


    return $data;
  }
	// function completeConstructionPayment(){
	// 	$invoiceNumber=$this->input->post('invoiceno');
	// 	$username = USER_NAME;
	// 	$key = JP_KEY;
	// 	$tranid =$this->input->post('transid');
	// 	$amount = $this->input->post('amount');
	// 	$channelRef = strtoupper(substr(md5(uniqid()),25)); 
	// 	$currency = "KES";
	// 	$jpPIN = $this->input->post('jp_pin');


	// 	$ck = $username .$tranid . $key;
	// 	$pass = sha1(utf8_encode($ck ));

	// 	$params = array(
	// 		"userName"=>$username,
	// 		"transactionId"=>$tranid,
	// 		"jpPIN"=>$jpPIN,     
	// 		"amount"=>$amount, 
	// 		"currency"=> $currency,
	// 		"channelRef"=> $channelRef,
	// 		"pass"=> $pass

	// 		);

	// 	try {
	// 		$client = new SoapClient(MAIN_URL);
	// 		$result = $client->CompletePaymentWalletECNNCC($params);
	// 	} catch (Exception $e) {
	// 		redirect('sbp/not_found');
	// 	}

	// 	$ResultCode = $result->CompletePaymentWalletECNNCCResult->Result->ResultCode;
	// 	$ResultText = $result->CompletePaymentWalletECNNCCResult->Result->ResultText;

	// 	if($ResultCode==0&&$ResultText=='OK'){
	// 		$paid = $result->CompletePaymentWalletECNNCCResult->Paid;
	// 		$receiptno = $result->CompletePaymentWalletECNNCCResult->ReceiptNo;

 //        #set in db
 //        // try {
 //        //       $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'regno'=>$carreg,'category'=>$type,'zone'=>$zone,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$customer,'channel'=>$channel);

 //        //         $query = "insert into `dailyparking` (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`,`channel`)";
 //        //         $query.= " values ('".implode("','",$data)."')";
 //        //         $this->db->query($query);

 //        //     } catch (Exception $e) {}
 //        #end set in db
	// 		try {

 //          $ph= $this->session->userdata('jpwnumber');//"254726981598"; 
 //            //$gsm="254".$ph;

 //          $amnt= number_format($amount, 2, '.', ',');
 //          $msg="Receipt Number $receiptno.Your payment of KES $amount for $invoiceNumber has been received by Nairobi City County.Powered by: JamboPay";
 //          $X_HOST ="http1.uk.oxygen8.com";
 //          $X_URL = "/WebTribe";
 //          $X_PORT ="8080";
 //          $X_USERNAME = "WebTribe";
 //          $X_PASSWORD = "web@12";
 //                   $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
 //                   $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
 //                   $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
 //                   $s_POST_DATA .= "&MSISDN=$ph"; // Phone
 //                   $s_POST_DATA .= "&Content=$msg";
 //                   $s_POST_DATA .= "&DataType=0"; //Data Type
 //                   $s_POST_DATA .= "&Premium=1"; //Premium
 //                   $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
 //                   $s_POST_DATA .= "&Multipart=1";
 //                   $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
 //                   $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
 //                   $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
 //                   $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
 //                   $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
 //                   $s_Request .="\r\n".$s_POST_DATA;

                   
 //                   $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
 //                   if(fputs ($fp, $s_Request)){

 //                   };
 //                   ///while (!feof($fp)) { $s_GatewayResponse .= fgets ($fp, 128); }
 //                   fclose ($fp);
 //               } catch (Exception $e) {

 //               }

 //               $data = array(
 //               	'rescode'=>$ResultCode,
 //               	'restext'=>$ResultText,
 //               	'paid'=>$paid,
 //               	'receiptno'=>$receiptno,
 //               	'customer'=>$this->session->userdata('name')
 //               	);
 //               return $data;
 //           }
 //           else {
 //           	$data = array(
 //           		'rescode'=>$ResultCode,
 //           		'restext'=>$ResultText
 //           		);
 //           	return $data;
 //           }


 //       }
}





