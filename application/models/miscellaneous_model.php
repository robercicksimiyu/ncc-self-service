<?php
class Miscellaneous_model extends CI_Model{

	function preparemiscellaneous(){

      $token = $this->session->userdata['token'];
      $billNumber = $this->input->post('billno');
      $jpwMobileNo = $this->session->userdata('jpwnumber');
      
      $post_string = array(
            'BillNumber'=>$billNumber,
            'PhoneNumber'=>$jpwMobileNo,
            'Stream'=>"misc",
            'PaymentTypeID' => "1"    
       );

      $url = "http://192.168.6.10/JamboPayServices/api/payments/";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_string));
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec($ch);

	  if (curl_errno($ch)) {
	    // this would be your first hint that something went wrong
	    return array('ErrorCode'=>curl_errno($ch),'Message'=>'Couldn\'t send request');
	  } else {
	  	// check the HTTP status code of the request
	    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    if ($resultStatus != 200) {
	       //$data = array('ErrorCode'=>$resultStatus,'Message'=>'Request failed: HTTP status code: ' . $resultStatus);
	    }
	  }
      
      $res = json_decode($result,true);
    /*   echo "<pre>";
      var_dump($res); die();     */
     
      if (!isset($res['ErrorCode'])) {
        $data= array(
          'BillNumber'=>$res['BillNumber'],
          'TransactionID'=>$res['TransactionID'],
          'Names'=>$res['Names'],
          'Amount'=>$res['Amount'],
          'BillDetail'=>$res['BillDetail'],
          'BillPayer'=>$res['BillPayer'],
          'BillStatus'=>$res['BillStatus'],
          'IssueDate'=>$res['IssueDate'],
          'AccountDescription'=>$res['AccountDescription']
          );
        
        return $data;
        }else {
          return array('ErrorType'=>$res['ErrorType'],'ErrorCode'=>$res['ErrorCode'],'Message'=>$res['Message']);
        }
      
  }


  function completeMiscellaneousPayment(){

	    $token = $this->session->userdata['token'] ;
	    $transid = $this->input->post('TransactionID');
	    $amount = $this->input->post('Amount');
	    $billno = $this->input->post('BillNumber');
	    $pin = $this->input->post('jp_pin');
	    $phone = $this->session->userdata('jpwnumber');
	   
	    $post_string = array(
	            'TransactionID'=>$transid,
	            'Amount'=>$amount,
	            'PhoneNumber' =>$phone,
	            'Pin' =>$pin,
	            'Stream'=>"misc"    
	          );


	      $url = "http://192.168.6.10/JamboPayServices/api/payments";
	      $ch = curl_init($url);
	      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	      curl_setopt($ch, CURLOPT_POST, true);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_string));
	      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	          'Authorization: bearer '.$token,
	          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
	      ));

	      $result = curl_exec($ch);
	      $res = json_decode($result); 
	     
	     if(!isset($res->ErrorCode)){
	        
	      $data = array(
	        "receiptnum"=>$res->ReceiptNumber,
	        "billno"=>$billno,
	        "rescode"=>"0",
	        "restext"=>"OK"
	      );


	      return $data;        
	      }
	      else {
          return array('ErrorType'=>$res->ErrorType,'ErrorCode'=>$res->ErrorCode,'Message'=>$res->Message);
        }

	}

  	 

  function printmiscellaneousreceiptNew($BillNumber){
        $token = $this->session->userdata('token');
        $stream = "misc";
        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&BillNumber=$BillNumber&Index=0";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));

        $result = curl_exec( $ch );
        $res = json_decode($result);
        foreach ($res as $key => $value) {
        	if ($value->TransactionDetail==$BillNumber) {
        		$trans_details=$value;
        		break;
        	}
        }
        
        if (!isset($trans_details)) {
        	 return redirect("miscellaneous/printReceipt/erro1");
        }
        
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/miscellaneous.pdf';
       
        $receiptno=$trans_details->ReceiptNumber;
        $date=substr($trans_details->TransactionDate, 0,10); 
        $paidby=$trans_details->BillPayer;
        $description=strtoupper($trans_details->AccountDescription);
        $amount=$trans_details->Amount;
        $amount = str_replace( ',', '', $amount);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 218, 478); 
        $page->drawText(number_format($amount,2), 480, 478);
        $page->drawText($amount_in_words, 150, 427); 
        $page->drawText($description, 150, 376);
        
        //$page->drawText($payer, 127, 38);
        $page->drawText(number_format($amount,2), 450, 296);
        $page->drawText(number_format($amount,2), 450, 274);
        $page->drawText(number_format(0,2), 450, 252);
       

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 622,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
    }




}