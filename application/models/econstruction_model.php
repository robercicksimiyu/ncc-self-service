<?php
class Econstruction_model extends CI_Model{

  function prepareConstruction(){
    $username = USER_NAME;
    $key = JP_KEY;
    $timestamp = date("Y-m-d H:i:s");
    $invoiceNumber = $this->input->post('invoice') ;
    $invoiceNumber = str_replace(' ', '', $invoiceNumber);
    $jpwMobileNo = $this->session->userdata('jpwnumber');
    $agentRef = rand(0, 1000);// $timestamp;
    $ck = $username.$jpwMobileNo.$agentRef.$invoiceNumber.$key;
    $pass = sha1(utf8_encode($ck));
    $serviceArguments = array(
      "userName"=>$username,
      "agentRef"=> $agentRef,
      "invoiceNumber"=>$invoiceNumber,
      "jpwMobileNo"=> $jpwMobileNo,
      "pass"=> $pass,
      );
    try {
      $client = new SoapClient(MAIN_URL, array('cache_wsdl' => WSDL_CACHE_NONE));
    } catch (Exception $e) {
      redirect('sbp/not_found');
    }
    $result = $client->PreparePaymentWalletECNNCC($serviceArguments);
    $resultcode=$result->PreparePaymentWalletECNNCCResult->Result->ResultCode;
    $resulttext=$result->PreparePaymentWalletECNNCCResult->Result->ResultText;
    $invoiceDate=$result->PreparePaymentWalletECNNCCResult->DateOfInvoice;
    $invoiceNo=$result->PreparePaymentWalletECNNCCResult->InvoiceNumber;
    $invoiceStatus=$result->PreparePaymentWalletECNNCCResult->InvoiceStatus;
    $fullName=$result->PreparePaymentWalletECNNCCResult->UserFullName;
    $email=$result->PreparePaymentWalletECNNCCResult->UserEmail;
    $transactionId=$result->PreparePaymentWalletECNNCCResult->TransactionID;
    $totalAmount=$result->PreparePaymentWalletECNNCCResult->TotalAmount;
    $userMobile=$result->PreparePaymentWalletECNNCCResult->UserMobile;
    if ($resulttext=="OK" && $resultcode==0) {
      $data=array(
        'invoiceDate'=>$invoiceDate,
        'invoiceNo'=>$invoiceNo,
        'invoiceStatus'=>$invoiceStatus,
        'fullName'=>$fullName,
        'email'=>$email,
        'transactionId'=>$transactionId,
        'totalAmount'=>$totalAmount,
        'userMobile'=>$userMobile,
        'rescode'=>$resultcode,
        'restext'=>$resulttext,
        );
      return $data;
    }
    else {
      return $data = array('restext'=>$resulttext,'rescode'=>$resultcode);
    }
  }

  function prepareConstructionNew(){

    $token = $this->session->userdata['token'] ;
  $invoiceNumber = $this->input->post('invoice') ; #var_dump($invoiceNumber);
  #$invoiceNumber = str_replace(' ', '', $invoiceNumber);
  $jpwMobileNo = $this->session->userdata('jpwnumber');

  $tosend = array(
    'InvoiceNumber'=>$invoiceNumber,
    'PhoneNumber'=>$jpwMobileNo,
    'Stream'=>"econstruction",
    'PaymentTypeID' => "1",
    'PaidBy'=>$this->session->userdata('name'),    
    );
  //var_dump($tosend);exit;

  foreach ( $tosend as $key => $value) {
    $post_items[] = $key . '=' . $value;
  }
  $post_string = implode ('&', $post_items);

  $url = "http://192.168.6.10/JamboPayServices/api/payments/post";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: bearer '.$token,
    'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));

  $result = curl_exec($ch);
  $res = json_decode($result);

  echo $result;exit;

  if (!isset($res->ErrorCode)) {        
    $data=array(
      'invoiceDate'=>$res->InvoiceDate,
      'invoiceNo'=>$res->InvoiceNumber,
      'invoiceStatus'=>$res->InvoiceStatus,
      'fullName'=>$res->FullNames,
      'email'=>$res->Email,
      'transactionId'=>$res->TransactionID,
      'totalAmount'=>$res->Amount,
      'userMobile'=>$res->PhoneNumber,
      'rescode'=>0,
      'restext'=>'OK'
      );
    return $data; 
  } else {
    return $data = array('restext'=>$res->Message,'rescode'=>$res->ErrorCode);
  }

  }

  function completeEconstructions(){
    $token = $this->session->userdata('token');
    $number = $this->session->userdata('jpwnumber');
    $name = $this->session->userdata('name');
    $Refid=str_replace(" ","",$this->input->post('refid'));
    $InvoiceNum= $this->input->post('InvoiceNo');
    $Amount= $this->input->post('amount');
    $Certificate= $this->input->post('certificate');
    $totalAmount = $Amount+$Certificate;
    $agentRef = strtoupper(substr(md5(uniqid()), 25));
    $tosend = array(
      'Stream' => "healthinvoice",
      'PhoneNumber' => $number,
      'InvoiceReferenceNumber' => $Refid,
      'PaidBy' => $this->session->userdata('name'),
      'Year' => date("Y"),
      'PaymentTypeID' => '1'
      );
    $url = "http://192.168.6.10/JamboPayServices/api/payments";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: bearer ' . $token,
      'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));
    $result = curl_exec($ch);
    $res = json_decode($result);
    curl_close($ch);
    @$rescode = $res->ResponseCode;
    if(!isset($rescode)){
      return array(
  "rescode" =>'404',//@res->ErrorCode,
  "message" => 'Invalid License ID ' //@$res->Message,
  );
    }elseif(isset($rescode)){
      $pay_send = array(
        'Stream' => 'healthinvoice',
        'TransactionID'=>(isset($res->TransactionID))?$res->TransactionID:'',
        'Pin' => $this->input->post('jp_pin'),
        'PhoneNumber' => (isset($res->PhoneNumber))?$res->PhoneNumber:'',
        'PaymentTypeID' => '1',
        );
      $url = "http://192.168.6.10/JamboPayServices/api/payments";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($pay_send));
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: bearer ' . $token,
        'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
        ));
      $pay_result = curl_exec($ch);
      $pay_res=json_decode($pay_result);
      @$rescode = $pay_res->ResponseCode;
  // Remove this exception once the api is fixed
      @$exception = isset($pay_res->ErrorCode) && $pay_res->ErrorCode == 404;
      if(isset($rescode) || $exception){
        $data1 = array(
          "Fullname" => $this->input->post('fullname'),
          "Email" => $this->input->post('email'),
          "Tel_no" => $this->input->post('telnumber'),
          "Firm" => $this->input->post('firm'),
          "Plot_no" => $this->input->post('plotno'),
          "LRNO" => $this->input->post('lrno'),
          "Address" => "Address",
          "InvoiceNo" => $this->input->post('InvoiceNo'),
          "certificate" => $this->input->post('certificate'),
          "Refid" => $this->input->post('refid'),
          "Amount" => $this->input->post('amount'),
          "total"=> $totalAmount,
          "status" => 0,
  "rescode"=> '200'//$rescode
  );
        return $data1;
      }elseif(!isset($rescode)){
        return array(
          "rescode" =>@$pay_res->ErrorCode,
          "message" =>@$pay_res->Message,
          );
      }
    }
  }

function completeConstructionPayment(){    
  $token = $this->session->userdata['token'] ;
  $invoiceNumber=$this->input->post('invoiceno');    
  $tranid =$this->input->post('transid');
  $amount = $this->input->post('amount');

  $tosend = array(
    'Stream' => 'econstruction',
    'TransactionID'=>$tranid,
    'Pin' => $this->input->post('jp_pin'),
    'PhoneNumber' =>$this->session->userdata('jpwnumber'),
    'PaymentTypeID' => '1',
    );
  try {  
    $url = "http://192.168.6.10/JamboPayServices/api/payments";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: bearer ' . $token,
      'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));
  } catch (Exception $e) {
    redirect('sbp/not_found');
  }
  $result = curl_exec($ch);
  $res = json_decode($result);
  curl_close($ch);

  if(!isset($res->ErrorCode)){   
    try {
      $data=array(
        'receiptno'=>$res->ReceiptNumber,
        'issuedate'=>date('Y-m-d h:i:s',time()),
        'from'=>$res->FullNames,
        'amount'=>$res->Amount,
        'invoiceno'=>$res->InvoiceNumber,
        'username' =>$res->PhoneNumber,
        'cashiername' =>$res->FullNames,
        'channel'=>'E-Payments',
        );
      $query = "insert into `econstruction` (`receiptno`,`issuedate`,`from`,`amount`,`invoiceno`,`username`,`cashiername`,`channel`)";
      $query.= " values ('".implode("','",$data)."')";
      $this->db->query($query);
    } catch (Exception $e) {}
    #end set in db
    try {
      $tel = $this->session->userdata('jpwnumber');
      $amnt= number_format($paid, 2, '.', ',');
      $msg ="Receipt Number $receiptno.Your e-Construction payment of KES $amnt for $invoiceNumber has been received by Nairobi City County.Powered by: JamboPay";
      $key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

      $APIKey = urlencode($key);
      $Phone = urlencode($tel);
      $relayCode = urlencode("WebTribe");
      $Message = urlencode($msg);
      $Shortcode = urlencode("700273");
      $CampaignId = urlencode("112623");
      $qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
      $ch=curl_init();
      curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
    } catch (Exception $e) {

    }

    $data = array(
      'rescode'=>$ResultCode,
      'restext'=>$ResultText,
      'paid'=>$paid,
      'receiptno'=>$receiptno,
      'customer'=>$this->session->userdata('name')
      );
    return $data;
  }
  else {
    $data = array(
      'rescode'=>$res->ErrorCode,
      'restext'=>$res->Message,
      );
    return $data;
  }


}



function printEreceiptsearch($receiptno){
  $this->load->library('zend');
  $a=$this->load->library('amount_to_words');
  $this->zend->load('Zend/Pdf');
  $this->zend->load('Zend/Barcode');

  $fileName =APPPATH.'/assets/back/receipts/E-Construction.pdf';
  $query="select * from econstruction where receiptno='$receiptno'";
  $result=$this->db->query($query);
  $result=$result->row();
  $username=$result->username;#var_dump($fileName);die();
  $receiptno=$result->receiptno;
  $date=substr($result->issuedate, 0,10); 
  $paidby=strtoupper($result->from);
  $amount=$result->amount;
  $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
  $amount=number_format(str_replace( ',', '', $amount),2);
  $for="INVOICE NUMBER: ".$result->invoiceno;
  $total_amount_due=$result->amount;
  $balance_due='0.00';
  $cashier=strtoupper($result->cashiername);
  $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
  $balance_due=number_format(str_replace( ',', '', $balance_due),2);

//   $query="select transid from chequedetails where transid=? and type='hrents'";
//   $res=$this->db->query($query,array($result->id));
//   if($res->num_rows()>0){
//     $ptype="CHEQUE";
// }else{
//     $ptype="KSH";
// }
$paymenttype="PAYMENT METHOD:  KES";


$pdf = Zend_Pdf::load($fileName);
$page=$pdf->pages[0];

$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

$page->drawText($receiptno, 180, 528);
$page->drawText($date, 450, 528);
$page->drawText($paidby, 220, 478);
$page->drawText($amount, 450, 478);
$page->drawText($amount_in_words, 130, 427);
$page->drawText($for, 150, 375); 
$page->drawText($cashier, 127, 38);
$page->drawText($total_amount_due, 450, 300);
$page->drawText($amount, 450, 278);
$page->drawText($balance_due, 450, 257);

$barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
$rendererOptions = array(
  'topOffset' => 600,
  'leftOffset' =>295
  );
$pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

$pdfData = $pdf->render(); 
header("Content-Disposition: inline; filename=receipt.pdf"); 
header("Content-type: application/x-pdf"); 
echo $pdfData;
}

function printEreceiptsearch2($receiptno){
  $this->load->library('zend');
  $a=$this->load->library('amount_to_words');
  $this->zend->load('Zend/Pdf');
  $this->zend->load('Zend/Barcode');

  $fileName =APPPATH.'/assets/back/receipts/E-Construction.pdf';
  $query="select * from miscellaneous where receiptno='$receiptno'";
  $result=$this->db->query($query);
  $result=$result->row();
  $username=$result->username;
  $receiptno=$result->receiptno;
  $date=substr($result->issuedate, 0,10); 
  $paidby=strtoupper($result->from);
  $amount=$result->amount;
  $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
  $amount=number_format(str_replace( ',', '', $amount),2);
  $for="INVOICE NUMBER: ".$result->invoiceno;
  $total_amount_due=$result->amount;
  $balance_due='0.00';
  $cashier=strtoupper($result->cashiername);
  $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
  $balance_due=number_format(str_replace( ',', '', $balance_due),2);


  $paymenttype="PAYMENT METHOD:  KES";


  $pdf = Zend_Pdf::load($fileName);
  $page=$pdf->pages[0];

  $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

  $page->drawText($receiptno, 180, 528);
  $page->drawText($date, 450, 528);
  $page->drawText($paidby, 220, 478);
  $page->drawText($amount, 450, 478);
  $page->drawText($amount_in_words, 130, 427);
  $page->drawText($for, 150, 375); 
  $page->drawText($cashier, 127, 38);
  $page->drawText($total_amount_due, 450, 300);
  $page->drawText($amount, 450, 278);
  $page->drawText($balance_due, 450, 257);

  $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
  $rendererOptions = array(
    'topOffset' => 600,
    'leftOffset' =>295
    );
  $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

  $pdfData = $pdf->render(); 
  header("Content-Disposition: inline; filename=receipt.pdf"); 
  header("Content-type: application/x-pdf"); 
  echo $pdfData;
}
function checkECprintReceipt(){
      $invoiceno = $this->input->post('invoiceno');
      $token=$this->session->userdata('token');
      $stream = "econstruction";
      $key = "InvoiceNumber";
      $value = $invoiceno;
      $key1 = "TransactionStatus";
      $value1 = "1";

       $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?stream=$stream&[0].Key=$key&[0].Value=$value&[1].Key=$key1&[1].Value=$value1";

       //echo $token;exit;
      
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      #curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
      ));

      $result = curl_exec( $ch );
     
      $res = json_decode($result);

      if($res['0']->InvoiceNumber==$invoiceno){
      return $res['0']->ReceiptNumber;
      }else{
        return redirect("econstruction/reprintECReceipt/erro1");
      }
    }

}





