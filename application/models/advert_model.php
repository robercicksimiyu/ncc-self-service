<?php

class Advert_model extends CI_Model
{

  function __construct()
  {
    $this->load->model('sbp_model');
    $this->load->model('common_model');
   
  }

  public function get_categories()
  {
    $results = curl_rest_client(ADVERT_API_URL."category/get-categories",'POST',"{}","external","json");
    return json_decode($results,true);
  }

  public function get_category_fees()
  {
    $results = curl_rest_client(ADVERT_API_URL."category/get-categories",'POST',"{}","external","json");
    return json_decode($results,true);
  }

  function get_category_fees_by_cat_id($cat_id)
  {
    $category_fees=array();
    $results = json_decode(curl_rest_client(ADVERT_API_URL."categoryfees/get-category-fees",'POST',"{}","external","json"),true);
    if(!empty($results)) {

       foreach($results as $key=>$value) {        
        if(!is_null($value)) {
          if(isset($value['categoryID']['id'])) {
            if($value['categoryID']['id'] == $cat_id) {
              array_push($category_fees, array(
                "id" => $value['id'],
                "base_fees" => $value['baseFee'],
                "multiple_fee" => $value['multipleFee'],
                "narrative" => $value['narrative'],
                "renewal_period_id" => $value['renewalPeriodID']['id'],
                "renewal_period" => $value['renewalPeriodID']['period'],
                ));
            }
          }          
        }
        //var_dump($category);
      }
    }
    return $category_fees;
   
  }
  public function get_category_fee($category_id)
  {
    $category=array(
      "id"=>$category_id,
      );
    $results = curl_rest_client(ADVERT_API_URL."categoryfees/get-category-fee",'POST',json_encode($category),"external","json");

    return json_decode($results,true);
  }

  public function get_category_dimension_id($category_id)
  {
    $category=array(
      "categoryID"=>array(
        "id"=>$category_id,
      ));
    $results = json_decode(curl_rest_client(ADVERT_API_URL."categorydimensions/get-category-dimension-by-category-id",'POST',json_encode($category),"external","json"),true);
    
    if(!empty($results)) {
       return  $results[0]['id'];
    }

    return  1;
  }

  public function get_dimensions()
  {
    $results = curl_rest_client(ADVERT_API_URL."dimensionstypes/get-dimension-types",'POST',"{}","external","json");
    return json_decode($results,true);
  }

  public function format_invoice_details($invoice_inforamtion)
  {
    $invoice_details=array(
      "id"=> isset($invoice_inforamtion['id']) ? $invoice_inforamtion['id'] :  0,
      "invoice_name"=> isset($invoice_inforamtion['advertID']['alias']) ? $invoice_inforamtion['advertID']['alias'] :  " ",
      "applicant_name"=> isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['firstName']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['firstName'] : "" ." ".isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['middleName']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['middleName'] : " ",
      "invoice_name"=>isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['emailAddress']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['emailAddress'] :  " ",
      "category"=> isset($invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category']) ? $invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category'] : " ",
      "category"=> isset($invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category']) ? $invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category'] : " ",
      "measurement_unit"=>isset($invoice_inforamtion['advertID']['dimensionID']['dimensionTypeID']['dimension']) ? $invoice_inforamtion['advertID']['dimensionID']['dimensionX']['dimension'] :  " ",
      "dimensions"=>isset($invoice_inforamtion['advertID']['dimensionID']['dimensionX']) ? $invoice_inforamtion['advertID']['dimensionID']['dimensionX']:" "." ".isset($invoice_inforamtion['advertID']['dimensionID']['dimensionY']) ? $invoice_inforamtion['advertID']['dimensionID']['dimensionY'] :  " " ,
      "amount"=>isset($invoice_inforamtion['amountToPay']) ? $invoice_inforamtion['amountToPay'] :  " ",
      );
      if(isset($invoice_inforamtion['isPaid'])){
        $invoice_details['isPPaid']=($invoice_inforamtion['isPaid']) ? "PAID" : "PENDING";
      } 
      return $invoice_details;
  }


  public function get_invoice_details($invoice_number)
  {
    $invoice=array(
      "id"=>$invoice_number,
      );
    $results = curl_rest_client(ADVERT_API_URL."invoice/get-invoice",'POST',json_encode($invoice),"external","json");
    return json_decode($results,true);
  }

  public function create_vehicle($vehicle_no)
  { 
    $create_vehicle=array(
      "numberPlate"=>$vehicle_no,
      );   
    return array(
      "id"=>json_decode(curl_rest_client(ADVERT_API_URL."vehicle/create-vehicle",'POST',json_encode($create_vehicle),"external","json"))->id,
    );
  }

  public function small_format_application()
  {
    $names=explode(' ',$this->input->post('company_name'));
    $applicant=array(
      "firstName"=>$names[0],
      "middleName"=>isset($names[1]) ? $names[1] : "",
      "surname"=>isset($names[2]) ? $names[2] : " ",
      "genderID" => array(
        "id"=>3,
        ),
      );

    $details = json_decode(curl_rest_client(ADVERT_API_URL."applicant/create-applicant",'POST',json_encode($applicant),"external","json"),true);

    $applicant_contact=array(
       "applicantID"=> array(
          "id"=>$details['id'],
        ),
       "emailAddress"=>$this->input->post('email'),
       "phoneNumber"=> $this->input->post('tel_no'),
      );

    $contact_details = json_decode(curl_rest_client(ADVERT_API_URL."contacts/create-contact",'POST',json_encode($applicant_contact),"external","json"),true);

    $application=array(
      "applicantContactID"=> array( 
        "id"=>$contact_details['id'], 
      ), 
      "periodID"=>array(
        "id"=> 6,
        )
      );

    $application_details=json_decode(curl_rest_client(ADVERT_API_URL."application/apply",'POST',json_encode($application),"external","json"),true);
    $measure=number_format(sqrt($this->input->post('area')),2,'.','');
    $create_dimension=array(
      "dimensionTypeID"=>array(
        "id"=>4,
      ),
      "dimensionX"=> $measure,
      "dimensionY"=> $measure,
      );

    $dimension_details=json_decode(curl_rest_client(ADVERT_API_URL."dimensions/create-dimension",'POST',json_encode($create_dimension),"external","json"),true);

    
    $vehicle_details=($this->input->post('vehicle_reg_no')=="") ? null : $this->create_vehicle($this->input->post('vehicle_reg_no'));  

    //var_dump($dimension_details);exit;

    //create advert
    $create_advert=array(
      "alias"=>$this->input->post('company_name'),
      "applicationID"=>array(
        "id"=>$application_details['id'],
        ),
      "categoryDimensionID"=>array(
        "id"=>$this->get_category_dimension_id($this->input->post('category_id')),//$this->input->post('dimension_unit'),
        ),
      "categoryFeeID"=>array(
        "id"=>$this->input->post('category_fee'),
        ),
      "dimensionID"=>array(
        "id"=>$dimension_details['id'],
        ),
      "vehicleData"=>$vehicle_details,
      );
   

    //var_dump($this->input->post());exit;
    

    $advert_details=json_decode(curl_rest_client(ADVERT_API_URL."advert/create-advert",'POST',json_encode($create_advert),"external","json"),true);

   
    $create_invoice=array(
      "advertID"=>array(
        "id"=>$advert_details['id'],
        ),
      );
    $invoice_details=json_decode(curl_rest_client(ADVERT_API_URL."invoice/create-invoice",'POST',json_encode($create_invoice),"external","json"),true);
     //var_dump($advert_details);exit;

    //$invoice_details['submitted']=$this->input->post();   
  
    return $invoice_details;
  }

  public function get_hire_parks_categories()
  {
    return $this->environment_model->get('hire_parks_category')->result_array();
  }

  public function complete_payment()
  {
    $payment_details = array(
           'Stream' => "merchantpayment",
           'PhoneNumber' => $this->session->userdata('jpwnumber'),          
           'PaymentTypeID' => 1,//"2014",
           'MerchantCode' => '123456',//"2014",
           'ProductName' => 'Advertisement',//"2014",
           'Amount' => $this->input->post('Invoice_amount'),
           'PaymentTypeID' => '1',
           'InvoiceNumber' =>$this->input->post('Invoice_number'),         
           'PaidBy' => $this->session->userdata('name'),           
           'Pin' => $this->session->userdata('jp_pin')           
           );

    $payment_result=json_decode(curl_rest_client("post","POST",$payment_details),true);

    if(isset($payment_result['TransactionID'])) {       	
      $payment_result['invoice_no']=$payment_details['InvoiceNumber'];
      $this->register_payment( $payment_result);
    }
    //var_dump($payment_result);exit;
    return $payment_result;  
  }

  function register_payment($data)
  { 
  	$register_details=array(
  		"datePaid"=>date('Y-m-d\TH:i:s\.0\0\0P'),
  		"middleWareID"=>$data['ID'],
  		  "paidBy"=>$this->session->userdata("name"),
  		  "receiptNumber"=>$data['OrderID'],
  		  "invoice"=>array(  
  		     "id"=>$data['invoice_no'],
  		  ),
  		);  	
  	return json_decode(curl_rest_client(ADVERT_API_URL."receipts/registerpayment",'POST',json_encode($register_details),"external","json"),true);
  }

  function printReceipt($ref_id)
  {
    $this->load->library('zend');
    $a=$this->load->library('amount_to_words');
    $this->zend->load('Zend/Pdf');
    $this->zend->load('Zend/Barcode');

    $fileName =APPPATH.'assets/back/receipts/advert.pdf';

      $invoice_details=$this->format_invoice_details($this->get_invoice_details($ref_id));
      //var_dump($invoice_details);exit;
      //var_dump($invoice_details);exit;
      
      $ownername = strtoupper($invoice_details['applicant_name']); //"Owners Name";
      $lrno = ''; //"LR NUMBER";
      $Refid = $invoice_details['id']; //"REFRENCE ID";
      $InvoiceNo = $invoice_details['id']; //"InvoiceNumber";
      $receiptno = $invoice_details['id']; //"InvoiceNumber";
      $title = "Advertisement"; //Regularization Type
      $amount = $invoice_details['amount'];//"2000000"; 
      if( is_numeric( $amount) ) {
          $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY **";
      } else {
        $amount_in_words="NOT AVAILABLE"; 
      } 
      $date=date('d-m-Y'); 
      $balance_due=0;       

      $pdf = Zend_Pdf::load($fileName);
      $page=$pdf->pages[0];

      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
      $page->drawText($receiptno, 170, 528);
      $page->drawText($date, 425, 528);
      $page->drawText($ownername, 250, 480);
      $page->drawText(number_format($amount, 2), 480, 480);
      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8);
      $page->drawText($amount_in_words, 150, 431);
      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
      $page->drawText('ADVERTINV'.'-'.$Refid, 150, 381);
      $page->drawText(number_format($amount,2), 450, 270);
      $page->drawText(number_format($amount,2), 450, 250);
      $page->drawText(number_format($balance_due,2), 450, 225);

      $pdfData = $pdf->render();
      header("Content-Disposition: inline; filename=Advertisement Receipt.pdf");
      header("Content-type: application/x-pdf");
      echo $pdfData;
    }

  function transport_types($form_id)
  { 
    $pattern="/TF[0-9]{2}/";
    $form=$this->getFormId($form_id);    
    $this->db->where('form_id',$form);
    $forms=$this->db->get('transport_forms')->row_array();
    if($forms) {
      return $forms['name'];
    } else {
      return '';
    }  
    
  }

  function getFormId($form_id)
  {
      $pattern="/TF[0-9]{2}/";
      $form=preg_match($pattern, substr($form_id,3), $matches, PREG_OFFSET_CAPTURE);
      if($form){
         return $matches[0][0];
      }  else {
        return '';
      }

  
  }

  function getInvoiceDetails($ref_id)
  {
    $get_invoices=array(
      ""
      );
  /*
     $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
     $api_id = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

     $serviceArguments=array(
      "API_Id"=>$api_id,
      "RefId"=>$ref_id,
      );
    
    $client = new SoapClient($url);
     $invoice_details=$client->GettransinvoiceDetails($serviceArguments);
     $invoice_result=(array)$invoice_details->GettransinvoiceDetailsResult;
     $invoice_result['RefId']=$ref_id;
     return $invoice_result;*/
  }


  function confirm_details()
  {
    $bid= @trim ($this->input->post('Bid')) ? $this->input->post('Bid') : $this->input->post('bid');
    $transaction_type=($this->input->post('format')=="renewal") ? 2 : 1;
    $form_id=$this->input->post('Form_Id');
    $bid_details=$this->sbp_model->getBusinessDetails($bid,2017);
    if(isset($bid_details['ErrorCode'])) {
      return array(
         "code"=>"E403",
         "message"=>"Business not found",
        );
    }
    $renewal_details=$this->get_renewal_bill();   
    if($form_id=="TF02") {
      $file_details=$this->upload_files_new('bannersfiles');     
      $artwork_file_path=base_url('bannersfiles/'.$file_details['file_name']);
    }
   // echo $artwork_file_path;exit;

    if(count($bid_details)) {     
      $details=array(        
          'Fullname'=>($this->input->post('contact_person')) ? $this->input->post('contact_person') : $this->input->post('Fullname') ,
          'Tel_no'=>($this->input->post('phone_number')) ? $this->input->post('phone_number') : $this->input->post('Tel_no'),
          'Email'=>($this->input->post('contact_email')) ? $this->input->post('contact_email') : $this->input->post('Email'),
          'bid_name'=>isset($bid_details['Name']) ? $bid_details['Name'] : "N/A" ,
          'Bid'=>$bid,
          'Location'=>isset($bid_details['PhysicalAddress']) ? $bid_details['PhysicalAddress'] : "N/A",
          'bid_lr_no'=>isset($bid_details['PlotNumber']) ? $bid_details['PlotNumber'] : "N/A",          
          'zone'=>isset($bid_details['ZoneID']) ? $bid_details['ZoneID'] : "N/A",
          'ward'=>isset($bid_details['WardID']) ? $bid_details['WardID'] : "N/A" ,
          'Parking_slots'=>($this->input->post('Parking_slots')) ? $this->input->post('Parking_slots') :$renewal_details['zones'],
          'Form_Id'=>$form_id,
          'category_id'=>$this->input->post('category_id'),
          'Start_date'=>$this->input->post('Start_date'),          
          'Description'=>isset($bid_details['ActivityName']) ? $bid_details['ActivityName'] : "N/A",         
          'trans_status'=>$transaction_type,         
      );

      if($this->input->post('HoistedLocation')) {
          $details['Location']=$this->input->post('HoistedLocation');
      }

      //var_dump($details);exit;
      if(isset($artwork_file_path)) {
        $details['artwork']=$artwork_file_path;
      }

    } else {
      $detials=array(
        "code"=>"E403",
        "message"=>"Business not found",
        );
    }

    return $details;
  }

  function get_hoisted_location()
  {
    return $this->db->get('banner_stands')->result_array();
  }

  function upload_files_new($filename)
  {
    // UPLOAD CONFIG    
        $config = array();
        $config['upload_path']  = "./$filename/";
        $config['allowed_types'] = 'jpg|jpeg|png|gif|doc|pdf|docx|odt';

        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $namesArray = [];


        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['Artwork']['name']);
       // var_dump($files);exit;
        //var_dump($config);exit;

       //echo $cpt;exit;
        for($i=0; $i<$cpt; $i++)
        {           
            $namesArray[$i] = $files['Artwork']['name'][$i];
            $_FILES['Artwork']['name']= $files['Artwork']['name'][$i];
            $_FILES['Artwork']['type']= $files['Artwork']['type'][$i];
            $_FILES['Artwork']['tmp_name']= $files['Artwork']['tmp_name'][$i];
            $_FILES['Artwork']['error']= $files['Artwork']['error'][$i];
            $_FILES['Artwork']['size']= $files['Artwork']['size'][$i];            
            $this->upload->initialize($config);            
            if($this->upload->do_upload('Artwork'))
            {
             return $this->upload->data();
            } else {
              $data = array('upload_data' => $this->upload->data());
              $this->load->view('upload_failed', $data);
            }           
        }
  }



  public function process_application($category='')
  {
    //var_dump($this->input->post());exit;
    $token = $this->session->userdata('token');
    $BusinessID = @trim($this->input->post('Bid'));
    $serviceArgs=$this->input->post();
    $serviceArgs['Subcounty']=$serviceArgs['ward'];
    $serviceArgs['zone']=$serviceArgs['zones'];
    $bid_details=$this->sbp_model->getBusinessDetails($BusinessID,2017);

    $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
    $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

    $data=array();

    $result=array(); 

    unset($serviceArgs['zones']); 
    unset($serviceArgs['ward']);
    //unset($serviceArgs['zone']);

    $serviceArgs['API_Id']=$apiID;
   
    try {
      $client = new SoapClient($url);

      if($serviceArgs['Form_Id']=="TF01") {
        $result = $client->PostLoadingZones($serviceArgs);
        @$RefID = $result->PostLoadingZonesResult->string;
      } if($serviceArgs['Form_Id']=="TF02") {
        $this->upload_files_new('liquorfiles');
        $result = $client->PostBanners($serviceArgs);
        @$RefID = $result->PostBannersResult->string;
      } if($serviceArgs['Form_Id']=="TF03") {
        $result = $client->PostBusShelters($serviceArgs);
        @$RefID = $result->PostBusSheltersResult->string;
      } 

      //var_dump($result);exit;
      //var_dump($result);exit;
      $save_data['bid']=$serviceArgs['Bid'];     
      $save_data['form_id']=$serviceArgs['Form_Id'];
      $save_data['trans_status']=$serviceArgs['trans_status'];           
      $save_data['year']=date('Y');        
      if($RefID[0]!=400){
        $serviceArguments=array(
          "API_Id"=>$apiID,
          "RefId"=>$RefID[0],
          );
        $save_data['ref_id']=$RefID[0];
        $this->createLoadingZoneRfId($save_data);                 
        $invoice_details=$client->GettransinvoiceDetails($serviceArguments);       
        $invoice_data=(array)$invoice_details->GettransinvoiceDetailsResult;
        $data=array_merge($save_data,$invoice_data);        
      } elseif($RefID[0]==400){        
        $bid_loading_zone_details=$this->getLoadingZoneRfId($serviceArgs['Bid']);
        if(count($bid_loading_zone_details)){
          //var_dump($bid_loading_zone_details);exit;
          $serviceArguments=array(
            "API_Id"=>$apiID,
            "RefId"=>$bid_loading_zone_details['ref_id'],
            );
          $save_data['form_id']=$bid_loading_zone_details['form_id'];
          $save_data['year']=$bid_loading_zone_details['year'];
          $save_data['ref_id']=$bid_loading_zone_details['ref_id'];


          $invoice_details=$client->GettransinvoiceDetails($serviceArguments);
          $invoice_data=(array)$invoice_details->GettransinvoiceDetailsResult;
          $data=array_merge($save_data,$invoice_data);
          //var_dump($data);exit;     
        } else {
          $data['ref_id']=$RefID[0];
          $data['message']=$RefID[1];
          
      }
        }
    }catch (Exception $e){
      redirect('rlb/Apierror');
    } 

   
    return $data;
 }

 function invoicePay()
 {
    $url = "http://35.164.42.112/mainsector.asmx?wsdl";
    $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
    $invoice_details=$this->getInvoiceDetails($this->input->post('RefId'));
    if(isset($invoice_details['InvoiceNum'])) {
      $_POST['invoice_number']=$invoice_details['InvoiceNum'];
      $_POST['amount']=$invoice_details['amount'];
      $_POST['ref_id']=$this->input->post('RefId');
      $_POST['year']=date('Y');
      $_POST['form_id']=$this->getFormId($this->input->post('RefId'));   
      return $this->complete_payment();
    } 
    return $invoice_details;   

 }

 function get_renew_businesses()
 {
   return $this->db->get('loading_zone_businesses')->result_array();
 }

 public function get_renewal_bill($bid=null)
 {   
   $this->db->where('client',$this->input->post('client'));
   return $this->db->get('loading_zone_businesses')->row_array();
 }

 public function generate_random_invoice_numbers()
  {
    return 'LZ'.rand(0, 99999);
  } 

 function createLoadingZoneRfId($details)
 {
  $insert_record=array(
    "bid"=>$details['bid'],
    "ref_id"=>$details['ref_id'],
    'form_id'=>$details['form_id'],
    'year'=>$details['year'],
    );
    $this->db->insert('loading_zones_refid',$insert_record);    
 }

 function getLoadingZoneRfId($bid)
 {
    $this->db->where('bid',$bid);
    return $this->db->get('loading_zones_refid')->row_array();
 }

 function check_expiry_date($date) 
 {
  if(strtotime($date) >= strtotime(date('Y-m-d'))) {
    return true;
  }
    return false;
 }

 public function add_payment($data,$ref_id)
 {
  if(isset($data['OrderID'])){
    $insert_data=array(
    'ref_id'=>$ref_id,
    'receipt_no'=>$data['OrderID'],
     );


    $this->db->insert('loading_zone_payment',$insert_data);  
  }
  
   

 }

 

    public function get_receipt_number($ref_id)
    {
      $this->db->where('ref_id',$ref_id);
      $details=$this->db->get('loading_zone_payment')->row_array();
      return $details['receipt_no'];

    }

  function changeUserStep1()
  {
            //'applicationNumber' => $this->input->post('applicationNumber'),
    $regdata = array(
      'applicantNames' => $this->input->post('applicantNames'),
      'applicantAddress' => $this->input->post('applicantAddress'),
      'applicantProfession' => $this->input->post('applicantProfession'),
      'ownerNames' => $this->input->post('ownerNames'),
      'ownerAddress' => $this->input->post('ownerAddress'),
      'applicantInterest' => $this->input->post('applicantInterest'),
      'physicalAddress' => $this->input->post('physicalAddress'),
      'zone' => $this->input->post('zone'),
      'zone_id' => $this->input->post('zone_id'),
      'lrNo' => str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
      'acreage' => $this->input->post('acreage')
      );

    return $regdata;
  }



  public function traceAPI($type,$method,$log)
  {
    $log_details=array(
      "type"=>$type,
      "method"=>$method,
      "log"=>$log,
      );
    $this->db->insert('rlb_logs',$log_details);
  }
  function changeUserStep2()
  {
            //'applicationNumber' => $this->input->post('applicationNumber'),
    $regdata = array(
      'applicantNames' => $this->input->post('applicantNames'),
      'applicantAddress' => $this->input->post('applicantAddress'),
      'applicantProfession' => $this->input->post('applicantProfession'),
      'ownerNames' => $this->input->post('ownerNames'),
      'ownerAddress' => $this->input->post('ownerAddress'),
      'applicantInterest' => $this->input->post('applicantInterest'),
      'physicalAddress' => $this->input->post('physicalAddress'),
      'zone' => $this->input->post('zone'),
      'zone_id' => $this->input->post('zone_id'),
      'lrNo' => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
      'acreage' => $this->input->post('acreage'),
      'subdivisionInvolved' => @($this->input->post('subdivisionInvolved')=='0')?'No':'Yes',
      'permissionApplied' => @($this->input->post('permissionApplied')=='0')?'No':'Yes',
      'subdivisionApplicationNo' => $this->input->post('subdivisionApplicationNo'),
      'proposedDevelopment' => $this->input->post('proposedDevelopment'),
      'currentUse' => $this->input->post('currentUse'),
      'lastUseDate' => $this->input->post('lastUseDate'),
      'natureProposedDevelopment' => $this->input->post('natureProposedDevelopment'),
      'meansOfAccessContruction' => @($this->input->post('meansOfAccessContruction')=='0')?'No':'Yes',
      );

return $regdata;
}

function structuresStep1()
{
  $regdata = array(
    'ownerNames' => $this->input->post('ownerNames'),
    'ownerEmail' => $this->input->post('ownerEmail'),
    'ownerPhoneNo' => $this->input->post('ownerPhoneNo'),
    'ownerPostalAddress' => $this->input->post('ownerPostalAddress'),
    'architectRegNo' => $this->input->post('architectRegNo'),
    'architectNames' => $this->input->post('architectNames'),
    'architectEmail' => $this->input->post('architectEmail'),
    'architectPhoneNo' => $this->input->post('architectPhoneNo'),
    'architectPostalAddress' => $this->input->post('architectPostalAddress'),
    'engineerRegNo' => $this->input->post('engineerRegNo'),
    'engineerNames' => $this->input->post('engineerNames'),
    'engineerEmail' => $this->input->post('engineerEmail'),
    'engineerPhoneNo' => $this->input->post('engineerPhoneNo'),
    'engineerPostalAddress' => $this->input->post('engineerPostalAddress'),
    );
  $this->session->set_userdata('regularization_details',json_encode($regdata));
  return $regdata;
}

function structuresStep2()
{
  $regdata=json_decode($this->session->userdata('regularization_details'),true);

  $regdata['currentLandUse']=$this->input->post('currentLandUse');
  $regdata['zone'] = $this->input->post('zone');
  $regdata['zone_id'] = $this->input->post('zone_id');
  $regdata['projectDetailedDescription'] = $this->input->post('projectDetailedDescription');
  $regdata['landTenure'] = $this->input->post('landTenure');
  $regdata['numberofUnits'] = $this->input->post('numberofUnits');
  $regdata['lrNo'] = @str_replace(' ', '', strtoupper($this->input->post('lrNo')));
  $regdata['plotSize'] =  $this->input->post('plotSize');
  $regdata['nearestRoad'] = $this->input->post('nearestRoad');
  $regdata['estate'] = $this->input->post('estate');
  $regdata['subcounty'] = $this->input->post('subcounty');
  $regdata['ward'] = $this->input->post('ward');
  $regdata['soilType'] = $this->input->post('soilType');

  $this->session->set_userdata('regularization_details',json_encode($regdata));
  return $regdata;
}
function subdivisionStep1()
{
  $regdata=$this->session->userdata('regularization_details');
  $regdata['applicantNames'] = $this->input->post('applicantNames');
  $regdata['applicantAddress'] = $this->input->post('applicantAddress');
  $regdata['applicantProfession'] = $this->input->post('applicantProfession');
  $regdata['ownerNames'] = $this->input->post('ownerNames');
  $regdata['ownerAddress'] = $this->input->post('ownerAddress');
  $regdata['applicantInterest'] = $this->input->post('applicantInterest');
  $regdata['lrNo'] = @str_replace(' ', '', strtoupper($this->input->post('lrNo')));
  $regdata['physicalAddress'] = $this->input->post('physicalAddress');
  $regdata['zone'] = $this->input->post('zone');
  $regdata['zone_id'] = $this->input->post('zone_id');
  $regdata['acreage'] = $this->input->post('acreage');      
  $this->session->set_userdata('regularization_details',$regdata);
  return $regdata;
}
function subdivisionStep2()
{
            //'applicationNumber' => $this->input->post('applicationNumber'),
  $regdata = array(
    'applicantNames' => $this->input->post('applicantNames'),
    'applicantAddress' => $this->input->post('applicantAddress'),
    'applicantProfession' => $this->input->post('applicantProfession'),
    'ownerNames' => $this->input->post('ownerNames'),
    'ownerAddress' => $this->input->post('ownerAddress'),
    'applicantInterest' => $this->input->post('applicantInterest'),
    'lrNo' => str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
    'physicalAddress' => $this->input->post('physicalAddress'),
    'zone' => $this->input->post('zone'),
    'zone_id' => $this->input->post('zone_id'),
    'acreage' => $this->input->post('acreage'),

    'subdivisionInvolved' => ($this->input->post('subdivisionInvolved')=='0')?'No':'Yes',
    'permissionApplied' => ($this->input->post('permissionApplied')=='0')?'No':'Yes',
    'subdivisionApplicationNo' => $this->input->post('subdivisionApplicationNo'),
    'proposedSubdivision' => $this->input->post('proposedSubdivision'),
    'currentUse' => $this->input->post('currentUse'),
    'lastUseDate' => $this->input->post('lastUseDate'),
    'noofSubPlots' => $this->input->post('noofSubPlots'),
    'meansOfAccessContruction' => ($this->input->post('meansOfAccessContruction')=='0')?'No':'Yes',
    );
return $regdata;
}

function fieldsNames(){
 return $fields = array(
  "LANDAFFECTED"=>"Affected Land",
  "BUILDINGSCOVER"=>"Building Cover",
  "CURRENTSITECOVER"=>"Current Site Cover",
  "PROPOSEDSITECOVER"=>"Proposed Site Cover",
  "WATERSUPPLIER"=>"Water Supplier",
  "WATERSUPPLY"=>"Water Supply",
  "SURFACEWATERDISPOSAL"=>"Surface Water Disposal",
  "WALLSDETAILS"=>"Walls Details (If applicable)",
  "SEWERAGEDISPOSAL"=>"Sewerage Disposal",
  "REFUSEDISPOSAL"=>"Refuse Disposal Details",
  "EASEMENTDETAILS"=>"Easement Details",
  "APPLICATIONNUMBER"=>"Application Number",
  "OWNERNAMES"=>"Owner's Names",
  "OWNEREMAIL"=>"Owner's Email",
  "OWNERPHONENO"=>"Owner's Phone Number",
  "OWNERPOSTALADDRESS"=>"Owner's Postal Address",
  "ARCHITECTREGNO"=>"Architect's Registration No.",
  "ARCHITECTNAMES"=>"Architect's Names",
  "ARCHITECTEMAIL"=>"Architect's Email",
  "ARCHITECTPHONENO"=>"Architect's Phone Number",
  "ARCHITECTPOSTALADDRESS"=>"Architect's Postal Address",
  "ENGINEERREGNO"=>"Engineer's Registration No.",
  "ENGINEERNAMES"=>"Engineer's Names",
  "ENGINEEREMAIL"=>"Engineer's Email",
  "ENGINEERPHONENO"=>"Engineer's Phone Number",
  "ENGINEERPOSTALADDRESS"=>"Engineer's Postal Address",
  "ZONE"=>"Zone",
  "ZONE_ID"=>"Zone ID",
  "OWNERADDRESS"=>"Owner's Address",
  "APPLICANTNAMES"=>"Applicant/Agent's Names",
  "APPLICANTPROFESSION"=>"Applicant/Agent's Profession",
  "APPLICANTADDRESS"=>"Applicant/Agent's Address",
  "APPLICANTINTEREST"=>"Applicant Interest",
  "LRNO"=>"Land Rate Number",
  "PROPOSEDDEVELOPMENT"=>"Current Development",
  "PHYSICALADDRESS"=>"Physical Address",
  "CURRENTUSE"=>"Original Land Use",
  "LASTUSEDATE"=>"Current Use Date",
  "ACREAGE"=>"Acreage (hectares)",
  "NATUREPROPOSEDDEVELOPMENT"=>"Nature of Current Development",
  "MEANSOFACCESSCONTRUCTION"=>"Access to construction area",
  "SUBDIVISIONINVOLVED"=>"Is Subdivision Involved",
  "PERMISSIONAPPLIED"=>"Is Permission Applied",
  "SUBDIVISIONAPPLICATIONNO"=>"Subdivision Application Number (Conditional)",
  "NOOFSUBPLOTS"=>"Number of Subplots",
  "PROPOSEDSUBDIVISION"=>"Proposed Subdivision Description",
  "SEWERAGEDISPOSALMETHOD"=>"Sewerage Disposal method",
  "BUILDINGCATEGORY"=>"Building Category",
  "BASEMENTAREA"=>"Basement Floor Area",
  "MEZZANINEFLOORAREA"=>"Zone Floor Area",
  "FLOOR1AREA"=>"1st Floor Area",
  "FLOOR2AREA"=>"2nd Floor Area",
  "FLOOR3AREA"=>"3rd Floor Area",
  "FLOOR4AREA"=>"4th Floor Area",
  "OTHERS"=>"Other Floor Area",
  "TOTALAREA"=>"Sum Total Floor Area",
  "PROJECTCOST"=>"Project Cost",
  "INSPECTIONFEES"=>"Inspection Fees",
  "FOUNDATION"=>"Foundation Details",
  "EXTERNALWALLS"=>"External Walls Details",
  "MORTAR"=>"Mortar Details",
  "ROOFCOVER"=>"Roof % Cover",
  "DAMPPROOFCOURSE"=>"Damp Proof Course",
  "CURRENTLANDUSE"=>"Current Land Use",
  "PROJECTDETAILEDDESCRIPTION"=>"Project Detailed Description",
  "LANDTENURE"=>"Land Tenure choice",
  "PLOTSIZE"=>"Plot Size",
  "NEARESTROAD"=>"Nearest Road",
  "ESTATE"=>"Estate",
  "SUBCOUNTY"=>"Sub-county",
  "WARD"=>"Ward",
  "SOILTYPE"=>"Soil Type",
  "NUMBEROFUNITS"=>"Number of units"
  );
}

function getZoneList()
{
        //  $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
        //  try{
        //      $client = new SoapClient($url);
        //      $serviceArguments = array(
        //                'API_Id' => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
        //         );
        //      $Response = $client->GetregDetails($serviceArguments);
        //  }catch (Exception $e){
        //         redirect('rlb/Apierror');
        // }
        // echo "<pre>"; var_dump($Response); die();
        // var_dump($Response->GetregDetailsResult->anyType);
        //  $data1 = @array_chunk($Response->GetregDetailsResult->anyType, 2); //array of values zoneDetails

  $data2 = array(
    '0' => "1",
    '1' => "CBD",
    '2' => "2",
    '3' => "upperhill",
    '4' => "3",
    '5' => "eastleigh",
    '6' => "4",
    '7' => "pumwani",
    '8' => "5",
    '9' => "califonia",
    '10' => "6",
    '11' => "ziwani starehe",
    '12' => "7",
    '13' => "Ngara",
    '14' => "8",
    '15' => "Parklands City Park",
    '16' => "9",
    '17' => "Kilimani",
    '18' => "10",
    '19' => "Kileleshwa",
    '20' => "11",
    '21' => "Westlands",
    '22' => "12",
    '23' => "Woodley",
    '24' => "13",
    '25' => "Lavington",
    '26' => "14",
    '27' => "Kyuna",
    '28' => "15",
    '29' => "Loresho",
    '30' => "16",
    '31' => "Upper Spring Valley",
    '32' => "17",
    '33' => "Muthaiga",
    '34' => "18",
    '35' => "Mathare",
    '36' => "19",
    '37' => "Huruma",
    '38' => "20",
    '39' => "Dandora",
    '40' => "21",
    '41' => "Kariobangi",
    '42' => "23",
    '43' => "Old Eastlands â€“ NCC Estates",
    '44' => "24",
    '45' => "Donholm",
    '46' => "25",
    '47' => "Buruburu",
    '48' => "26",
    '49' => "Komorock",
    '50' => "27",
    '51' => "Kayole",
    '52' => "28",
    '53' => "Industrial Area",
    '54' => "29",
    '55' => "Kariobangi Light Industry",
    '56' => "30",
    '57' => "Mathare North Light Industry",
    '58' => "31",
    '59' => "Ruaraka Light Industry",
    '60' => "32",
    '61' => "Embakasi Light Industry",
    '62' => "33",
    '63' => "Langata",
    '64' => "34",
    '65' => "South B & C",
    '66' => "35",
    '67' => "Villa Franca",
    '68' => "36",
    '69' => "Embakasi Village",
    '70' => "37",
    '71' => "Kibera",
    '72' => "38",
    '73' => "Karen",
    '74' => "39",
    '75' => "Kitisuru",
    '76' => "40",
    '77' => "Gigigiri",
    '78' => "41",
    '79' => "Runda Ridgeways",
    '80' => "42",
    '81' => "Garden Estate",
    '82' => "43",
    '83' => "Roysambu",
    '84' => "44",
    '85' => "Thome",
    '86' => "45",
    '87' => "Marurui",
    '88' => "46",
    '89' => "Dagoretti",
    '90' => "47",
    '91' => "Baba Dogo",
    '92' => "48",
    '93' => "Ruaraka",
    '94' => "49",
    '95' => "Ngumba Estate",
    '96' => "50",
    '97' => "Kahawa West",
    '98' => "51",
    '99' => "Zimmerman",
    '100' => "52",
    '101' => "Githurai",
    '102' => "53",
    '103' => "Kasarani",
    '104' => "54",
    '105' => "Njiru",
    '106' => "55",
    '107' => "Clayworks",
    '108' => "56",
    '109' => "Mwiki",
    '110' => "57",
    '111' => "Ruai",
    '112' => "58",
    '113' => "Embakasi"
    );

        // $data2 = array(
        //     '0' => array(
        //                 "0" => "1",
        //                 "1" => "CBD",
        //                  ),
        //     '1' => array(
        //                 "0" => "1",
        //                 "1" => "upperhill",
        //                  ),
        //     '2' => array(
        //                 "0" => "2",
        //                 "1" => "eastleigh",
        //                  ),
        //     '3' => array(
        //                 "0" => "2",
        //                 "1" => "pumwani",
        //                  ),
        //     '4' => array(
        //                 "0" => "2",
        //                 "1" => "califonia",
        //                  ),
        //     '5' => array(
        //                 "0" => "2",
        //                 "1" => "ziwani starehe",
        //                  ),
        //     '6' => array(
        //                 "0" => "2",
        //                 "1" => "Ngara",
        //                  ),
        //     '7' => array(
        //                 "0" => "3",
        //                 "1" => "Parklands City Park",
        //                  ),
        //     '8' => array(
        //                 "0" => "4",
        //                 "1" => "Kilimani",
        //                  ),
        //     '9' => array(
        //                 "0" => "4",
        //                 "1" => "Kileleshwa",
        //                  ),
        //     '10' => array(
        //                 "0" => "4",
        //                 "1" => "Westlands",
        //                  ),
        //     '11' => array(
        //                 "0" => "4",
        //                 "1" => "Woodley",
        //                  ),
        //     '12' => array(
        //                 "0" => "5",
        //                 "1" => "Lavington",
        //                  ),
        //     '13' => array(
        //                 "0" => "5",
        //                 "1" => "Kyuna",
        //                  ),
        //     '14' => array(
        //                 "0" => "5",
        //                 "1" => "Loresho",
        //                  ),
        //     '15' => array(
        //                 "0" => "5",
        //                 "1" => "Upper Spring Valley",
        //                  ),
        //     '16' => array(
        //                 "0" => "6",
        //                 "1" => "Muthaiga",
        //                  ),
        //     '17' => array(
        //                 "0" => "7",
        //                 "1" => "Mathare",
        //                  ),
        //     '18' => array(
        //                 "0" => "7",
        //                 "1" => "Huruma",
        //                  ),
        //     '19' => array(
        //                 "0" => "7",
        //                 "1" => "Dandora",
        //                  ),
        //     '20' => array(
        //                 "0" => "7",
        //                 "1" => "Kariobangi",
        //                  ),
        //     '21' => array(
        //                 "0" => "8",
        //                 "1" => "Old Eastlands – NCC Estates",
        //                  ),
        //     '22' => array(
        //                 "0" => "8",
        //                 "1" => "Donholm",
        //                  ),
        //     '23' => array(
        //                 "0" => "8",
        //                 "1" => "Buruburu",
        //                  ),
        //     '24' => array(
        //                 "0" => "8",
        //                 "1" => "Komorock",
        //                  ),
        //     '25' => array(
        //                 "0" => "8",
        //                 "1" => "Kayole",
        //                  ),
        //     '26' => array(
        //                 "0" => "9",
        //                 "1" => "Industrial Area",
        //                  ),
        //     '27' => array(
        //                 "0" => "9",
        //                 "1" => "Kariobangi Light Industry",
        //                  ),
        //     '28' => array(
        //                 "0" => "9",
        //                 "1" => "Mathare North Light Industry",
        //                  ),
        //     '29' => array(
        //                 "0" => "9",
        //                 "1" => "Ruaraka Light Industry",
        //                  ),
        //     '30' => array(
        //                 "0" => "9",
        //                 "1" => "Embakasi Light Industry",
        //                  ),
        //     '31' => array(
        //                 "0" => "10",
        //                 "1" => "Langata",
        //                  ),
        //     '32' => array(
        //                 "0" => "10",
        //                 "1" => "South B &amp; C",
        //                  ),
        //     '33' => array(
        //                 "0" => "10",
        //                 "1" => "Villa Franca",
        //                  ),
        //     '34' => array(
        //                 "0" => "10",
        //                 "1" => "Embakasi Village",
        //                  ),
        //     '35' => array(
        //                 "0" => "11",
        //                 "1" => "Kibera",
        //                  ),
        //     '36' => array(
        //                 "0" => "12",
        //                 "1" => "Karen",
        //                  ),
        //     '37' => array(
        //                 "0" => "13",
        //                 "1" => "Kitisuru",
        //                  ),
        //     '38' => array(
        //                 "0" => "13",
        //                 "1" => "Gigigiri",
        //                  ),
        //     '39' => array(
        //                 "0" => "13",
        //                 "1" => "Runda Ridgeways",
        //                  ),
        //     '40' => array(
        //                 "0" => "13",
        //                 "1" => "Garden Estate",
        //                  ),
        //     '41' => array(
        //                 "0" => "14",
        //                 "1" => "Roysambu",
        //                  ),
        //     '42' => array(
        //                 "0" => "14",
        //                 "1" => "Thome",
        //                  ),
        //     '43' => array(
        //                 "0" => "14",
        //                 "1" => "Marurui",
        //                  ),
        //     '44' => array(
        //                 "0" => "15",
        //                 "1" => "Dagoretti",
        //                  ),
        //     '45' => array(
        //                 "0" => "16",
        //                 "1" => "Baba Dogo",
        //                  ),
        //     '46' => array(
        //                 "0" => "16",
        //                 "1" => "Ruaraka",
        //                  ),
        //     '47' => array(
        //                 "0" => "16",
        //                 "1" => "Ngumba Estate",
        //                  ),
        //     '48' => array(
        //                 "0" => "17",
        //                 "1" => "Kahawa West",
        //                  ),
        //     '49' => array(
        //                 "0" => "17",
        //                 "1" => "Zimmerman",
        //                  ),
        //     '50' => array(
        //                 "0" => "17",
        //                 "1" => "Githurai",
        //                  ),
        //     '51' => array(
        //                 "0" => "18",
        //                 "1" => "Kasarani",
        //                  ),
        //     '52' => array(
        //                 "0" => "18",
        //                 "1" => "Njiru",
        //                  ),
        //     '53' => array(
        //                 "0" => "18",
        //                 "1" => "Clayworks",
        //                  ),
        //     '54' => array(
        //                 "0" => "18",
        //                 "1" => "Mwiki",
        //                  ),
        //     '55' => array(
        //                 "0" => "18",
        //                 "1" => "Ruai",
        //                  ),
        //     '56' => array(
        //                 "0" => "18",
        //                 "1" => "Embakasi"
        //                 )
        //   );
        //print_r($data1); print_r($data2);exit();

return $data2;
}

function GetbuildzoneDetails()
{
        // $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
        // try{
        //      $client = new SoapClient($url);
        //      $serviceArguments = array(
        //                'API_Id' => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
        //         );
        //      $Response = $client->GetbuildzoneDetails($serviceArguments);
        //  }catch (Exception $e){
        //         redirect('rlb/Apierror');
        //     }
        //  $data1 = @array_chunk($Response->GetbuildzoneDetailsResult->anyType, 2); //array of values buildingzone details

 $data2 = Array(
  "0" => array
  (
    "0" => "low rise",
    "1" => "24000",
    ),

  "1" => array
  (
    "0" => "high rise-with lifts",
    "1" => "30000",
    ),

  "2" => array
  (
    "0" => "factories",
    "1" => "23000",
    ),

  "3" => array
  (
    "0" => "Warehouses",
    "1" => "21000",
    ),

  "4" => array
  (
    "0" => "RETAIL OUTLETS-Small Scale Shopping Centres",
    "1" => "23000",
    ),

  "5" => array
  (
    "0" => "RETAIL OUTLETS-Shopping Malls ",
    "1" => "30000",
    ),

  "6" => array
  (
    "0" => "RESIDENTIAL -Low cost, low rise flats( 5 levels & below)",
    "1" => "21000",
    ),

  "7" => array
  (
    "0" => "RESIDENTIAL -Low Cost , High rise Flats-Six Levels & above)",
  "1" => "24000",
  ),

  "8" => array
  (
    "0" => "RESIDENTIAL -Low Class Single Units (Maisonettes & Bungalows )",
    "1" => "24000",
    ),

  "9" => array
  (
    "0" => "RESIDENTIAL -High Class Single Units (Maisonettes & Bungalows)",
    "1" => "30000",
    ),

  "10" => array
  (
    "0" => "RESIDENTIAL -High Class, Low Rise Flats ( 5 levels & below)",
    "1" => "30000",
    ),

  "11" => array
  (
    "0" => "RESIDENTIAL -High Class, High Rise Flats (Six Levels & above)",
    "1" => "33000",
    ),

  "12" => array
  (
    "0" => "SOCIAL CENTRES-Social Clubs",
    "1" => "24000",
    ),

  "13" => array
  (
    "0" => "SOCIAL CENTRES-Community Centres ",
    "1" => "27000",
    ),

  "14" => array
  (
    "0" => "SOCIAL CENTRES-Churches (Normal Height)",
    "1" => "28000",
    ),

  "15" => array
  (
    "0" => "SOCIAL CENTRES-Churches (Double Volume)",
    "1" => "32000",
    ),

  "16" => array
  (
    "0" => "EDUCATION FACILITIES -Hostels",
    "1" => "25000",
    ),

  "17" => array
  (
    "0" => "EDUCATION FACILITIES -Nursery schools",
    "1" => "21000",
    ),

  "18" => array
  (
    "0" => "EDUCATION FACILITIES -Primary schools",
    "1" => "24000",
    ),

  "19" => array
  (
    "0" => "EDUCATION FACILITIES -Secondary schools",
    "1" => "27000",
    ),

  "20" => array
  (
    "0" => "EDUCATION FACILITIES -Colleges & Universities",
    "1" => "30000",
    ),

  "21" => array
  (
    "0" => "Urban Low Rise (5 levels & below)",
    "1" => "24000",
    ),

  "22" => array
  (
    "0" => "HOTELS-Urban High Rise (with lifts)",
    "1" => "30000",
    ),

  "23" => array
  (
    "0" => "HOTELS-Tented Camps",
    "1" => "25000",
    ),

  "24" => array
  (
    "0" => "HOTELS-Games Lodges",
    "1" => "32000",
    ),

  "25" => array
  (
    "0" => "HEALTH FACILITATES-Simple Clinics",
    "1" => "18000",
    ),

  "26" => array
  (
    "0" => "HEALTH FACILITATES-Dispensaries",
    "1" => "20000",
    ),

  "27" => array
  (
    "0" => "HEALTH FACILITATES-Urban Areas Clinics",
    "1" => "24000",
    ),

  "28" => array
  (
    "0" => "HEALTH FACILITATES-Large Â Referral Hospitals",
    "1" => "27000",
    ),

  "29" => array
  (
    "0" => "SPORTS FACILITIES-Simple Arenas",
    "1" => "22000",
    ),

  "30" => array
  (
    "0" => "SPORTS FACILITIES-Stadiums",
    "1" => "27000",
    ),

  "31" => array
  (
    "0" => "SPORTS FACILITIES-Theatres (Double Volume/ Height)",
    "1" => "35000",
    ),

  "32" => array
  (
    "0" => "SPORTS FACILITIES-Health clubs",
    "1" => "38000",
    ),

  "33" => array
  (
    "0" => "SPORTS FACILITIES-Playing Fields",
    "1" => "13000",
    )
  );
return $data2;
}
function getRtList()
{
 $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
 try{
   $client = new SoapClient($url);
   $serviceArguments = array(
     'API_Id' => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
     );
   $Response = $client->GetRtDetails($serviceArguments);
 }catch (Exception $e){
  redirect('rlb/Apierror');
}
         return $regDetails = $Response->GetRtDetailsResult->anyType; //array of values RtDetails
       }

    // GET SUB COUNTIES
       function getSubCounties()
       {
         $token = $this->session->userdata['token'];
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/getsbpsubcounties?stream=sbp");
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
          ));

         $results = curl_exec($ch);

         curl_close($ch);

         return json_decode($results);
       }

    // GET SUB WARDS
       function getWards($countyId)
       {
        $token = $this->session->userdata['token'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://192.168.6.10/JamboPayServices/api/payments/getsbpwards?stream=sbp&id=$countyId");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer '.$token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
          ));

        $results = curl_exec($ch);
        curl_close($ch);

        return json_decode($results);
      }

      function getRegInvoiceDetails($client, $RefID, $ownerNames = ''){
        $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
        try{
            //Get invoice details
          $serviceArgs = array(
            "API_Id" => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
            "RefId" => $RefID,
            );
            //$client = new SoapClient($url);
            // pass the client instance to avoid overloading the server
          $result2 =  $client->GetreginvoiceDetails($serviceArgs);
        }catch (Exception $e){
          redirect('rlb/Apierror');
        }
        if(isset($result2)){
          $data = array(
            "RefId" => @str_replace('/', '', $RefID),
            "InvoiceNo" => @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->InvoiceNum),
            "ownerNames" => @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->ownerNames),
            "ownerAddress" => @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->ownerAddress),
            "lrNo" =>  @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->lrNo),
            "physicalAddress" => @$result2->GetreginvoiceDetailsResult->physicalAddress,
            "amount" => @str_replace('/', '', $result2->GetreginvoiceDetailsResult->amount),
            "form_id" => @substr($RefID, 3, 4),
            "form_status" => @$result2->GetreginvoiceDetailsResult->form_status,
            );
          return $data;
        }else {
          redirect('rlb/invalidRefid');
        }
      }

      function postAmalgamation()
      {
        // POST Data
        $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
          "API_Id" => $apiID,
          "ownerNames" => $this->input->post('ownerNames'),
          "ownerAddress" => $this->input->post('ownerAddress'),
          "applicantNames" => $this->input->post('applicantNames'),
          "applicantAddress" => $this->input->post('applicantAddress'),
          "applicantInterest" => $this->input->post('applicantInterest'),
          "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
          "physicalAddress" => $this->input->post('physicalAddress'),
          "acreage" => $this->input->post('acreage'),
          "applicationNumber" => $this->input->post('applicationNumber'),
          "zone" => $this->input->post('zone'),
          "Form_Id" => "RF07",
          "subdivisionInvolved" => $this->input->post('subdivisionInvolved'),
          "permissionApplied" => $this->input->post('permissionApplied'),
          "subdivisionApllicationNo" => $this->input->post('subdivisionApllicationNo'),
          "proposedDevelopment" => $this->input->post('proposedDevelopment'),
          "currentUse" => $this->input->post('currentUse'),
          "lastUseDate" => $this->input->post('lastUseDate'),
          "meansofAccessConstruction" => $this->input->post('meansofAccessConstruction'),
          "natureProposedDevelopment" => $this->input->post('natureProposedDevelopment'),
          "wallsDetails" => $this->input->post('wallsDetails'),
          "waterSupply" => $this->input->post('waterSupply'),
          "sewerageDisposal" => $this->input->post('sewerageDisposal'),
          "refuseDisposal" => $this->input->post('refuseDisposal'),
          "surfaceWaterDisposal" => $this->input->post('surfaceWaterDisposal'),
          "easementDetails" => $this->input->post('easementDetails'),
          "zoneid" => $this->input->post('zone_id'),
          "Rt" => "category a",
          "Rtid" => "6",
          "ward" => $this->input->post('zone'),
          "subcounty" => $this->input->post('zone'),
          "noofSubPlots" => $this->input->post('noofSubPlots')
          );
        //$this->session->set_userdata($serviceArgs);
        //$this->session->set_userdata('applicantProfession',$this->input->post('applicantProfession'));
try {
  $client = new SoapClient($url);
  $result = $client->postamalgamation($serviceArgs);
  @$RefID = $result->postamalgamationResult->string[0];

}catch (Exception $e){
  redirect('rlb/Apierror');
}
$isReg = @substr($RefID, 3,3) == 'RF0';
if($RefID!="400" && $isReg){
  @$ownerNames = $result->postamalgamationResult->string[1];
  @$InvoiceNo = $result->postamalgamationResult->string[3];
            // Get invoice details
            // pass the client instance to avoid overloading the server
  return $this->getRegInvoiceDetails($client, $RefID, $ownerNames);
} elseif($RefID == '400'){
  redirect('rlb/error');
}else {
  redirect('rlb/Apierror');
}
}

function postBuildingStructures()
{
  $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
  $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';
  $serviceArguments = array(
    'API_Id' => $apiID,
    "ownerNames" => $this->input->post('ownerNames'),
    "ownerEmail" => $this->input->post('ownerEmail'),
    "ownerPhoneNo" => $this->input->post('ownerPhoneNo'),
    "ownerAddress" => $this->input->post('ownerPostalAddress'),
    "architectRegNo" => $this->input->post('architectRegNo'),
    "architectNames" => $this->input->post('architectNames'),
    "architectEmail" => $this->input->post('architectEmail'),
    "architectPhoneNo" =>$this->input->post('architectPhoneNo'),
    "architectPostalAddress" => $this->input->post('architectPostalAddress'),
    "engineerRegNo" => $this->input->post('engineerRegNo'),
    "engineerNames" => $this->input->post('engineerNames'),
    "Form_Id" => "RF08",
    "engineerEmail" => $this->input->post('engineerEmail'),
    "engineerPhoneNo" => $this->input->post('engineerPhoneNo'),
    "engineerPostalAddress" => $this->input->post('engineerPostalAddress'),
    "currentLandUse" => $this->input->post('currentLandUse'),
    "zone" => $this->input->post('zone'),
    "projectDetailedDescription" => $this->input->post('projectDetailedDescription'),
    "landTenure" => $this->input->post('landTenure'),
    "numberofUnits" => $this->input->post('numberofUnits'),
    "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
    "plotSize" => $this->input->post('plotSize'),
    "nearestRoad" => $this->input->post('nearestRoad'),
    "estate" => $this->input->post('estate'),
    "subcounty" => $this->input->post('subcounty'),
    "ward" => $this->input->post('ward'),
    "soilType" => $this->input->post('soilType'),
    "waterSupplier" => $this->input->post('waterSupplier'),
    "sewerageDisposalMethod" => $this->input->post('sewerageDisposalMethod'),
    "projectCost" => $this->input->post('projectCost'),
    "inspectionFees" => $this->input->post('inspectionFees'),
    "Foundation" => $this->input->post('Foundation'),
    "externalWalls" => $this->input->post('externalWalls'),
    "mortar" => $this->input->post('mortar'),
    "roofCover" => $this->input->post('roofCover'),
    "dampProofCourse" => $this->input->post('dampProofCourse'),
    "zoneid" => $this->input->post('zone_id'),
    "Rt" => "category a",
    "Rtid" => "3",
    "category_id" => "152",
    "basementArea" => $this->input->post('basementArea'),
    "mezzaninefloorArea" => $this->input->post('mezzaninefloorArea'),
    "floorArea" => $this->input->post('totalArea'),
    "floorArea1" => $this->input->post('floorArea1'),
    "floorArea2" => $this->input->post('floorArea2'),
    "floorArea3" => $this->input->post('floorArea3'),
    "floorArea4" => $this->input->post('floorArea4'),
    "Others" => $this->input->post('Others'),
    );

$this->session->set_userdata($serviceArguments);
$this->traceAPI('API:Arguments','postbuildingstructures',json_encode($serviceArguments));
try{
  $client = new SoapClient($url);
  $Response1 = $client->postbuildingstructures($serviceArguments);
  $this->traceAPI('API:Response','postbuildingstructures',json_encode($Response1));
  @$RefID = $Response1->postbuildingstructuresResult->string[0];
}catch (Exception $e){
  redirect('rlb/Apierror');
}
$isReg = @substr($RefID, 3,3) == 'RF0';
if($RefID!="400" && $isReg){
  @$ownerEmail = $Response1->postbuildingstructuresResult->string[2];
  @$InvoiceNo = $Response1->postbuildingstructuresResult->string[3];
            //Get invoice details
  $serviceArgs = array(
   "API_Id" => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
   "RefId" => $RefID
   );
  $result2 =  $client->GetbuildinginvoiceDetails($serviceArgs);
  $this->traceAPI('API:Arguments','GetbuildinginvoiceDetails',json_encode($serviceArguments));
  if(isset($result2)){
    $data = array(
     "RefId" => @str_replace('/', '', $RefID),
     "InvoiceNo" => @str_replace('/', '_', $InvoiceNo),
     "ownerNames" => @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->ownerNames),
     "ownerAddress" => @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->ownerAddress),
     "lrNo" =>  @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->lrNo),
     "physicalAddress" => $result2->GetbuildinginvoiceDetailsResult->physicalAddress,
     "amount" => @str_replace('/', '', $result2->GetbuildinginvoiceDetailsResult->amount),
     "architectNames" => $result2->GetbuildinginvoiceDetailsResult->architectNames,
     "architectEmail" => $result2->GetbuildinginvoiceDetailsResult->architectEmail,
     "engineerNames" => $result2->GetbuildinginvoiceDetailsResult->engineerNames,
     "engineerEmail" => $result2->GetbuildinginvoiceDetailsResult->engineerEmail,
     "form_id" => 'RF08',
     "form_status" => $result2->GetbuildinginvoiceDetailsResult->form_status,
     );

return $data;
}else {
  redirect('rlb/Apierror');
}
} elseif($RefID == '400'){
  redirect('rlb/error');
} else {
  redirect('rlb/Apierror');
}
}

function postChangeOfUsers()
{
        // POST Data
  $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
  $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

  $serviceArgs = array(
    "API_Id" => $apiID,
    "ownerNames" => $this->input->post('ownerNames'),
    "ownerAddress" => $this->input->post('ownerAddress'),
    "applicantNames" => $this->input->post('applicantNames'),
    "applicantAddress" => $this->input->post('applicantAddress'),
    "applicantInterest" => $this->input->post('applicantInterest'),
    "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
    "physicalAddress" => $this->input->post('physicalAddress'),
    "acreage" => $this->input->post('acreage'),
    "applicationNumber" => $this->input->post('applicationNumber'),
    "zone" => $this->input->post('zone'),
    "Form_Id" => "RF01",
    "subdivisionInvolved" => $this->input->post('subdivisionInvolved'),
    "permissionApplied" => $this->input->post('permissionApplied'),
    "subdivisionApllicationNo" => $this->input->post('subdivisionApllicationNo'),
    "proposedDevelopment" =>  $this->input->post('proposedDevelopment'),
    "currentUse" => $this->input->post('currentUse'),
    "lastUseDate" => $this->input->post('lastUseDate'),
    "meansofAccessConstruction" => $this->input->post('meansofAccessConstruction'),
    "natureProposedDevelopment" => $this->input->post('natureProposedDevelopment'),
    "wallsDetailswallsDetails" => $this->input->post('wallsDetails'),
    "waterSupply" => $this->input->post('waterSupply'),
    "sewerageDisposal" => $this->input->post('sewerageDisposal'),
    "refuseDisposal" => $this->input->post('refuseDisposal'),
    "surfaceWaterDisposal" =>$this->input->post('surfaceWaterDisposal'),
    "easementDetails" => $this->input->post('easementDetails'),
    "landAffected" => $this->input->post('landAffected'),
    "buildingsCover" => $this->input->post('buildingsCover'),
    "currentSiteCover" => $this->input->post('currentSiteCover'),
    "proposedSiteCover" => $this->input->post('proposedSiteCover'),
    "wallsDetails" => $this->input->post('wallsDetails'),
    "zoneid" =>$this->input->post('zone_id'),
    "Rt" => "category a",
    "Rtid" => "3",
    "ward" => $this->input->post('zone'),
    "subcounty" => $this->input->post('zone'),
    );
       //$this->session->set_userdata($serviceArgs);
       // $this->session->set_userdata('applicantProfession',$this->input->post('applicantProfession'));

try {
  $client = new SoapClient($url);
  $result = $client->Postchangeofuser($serviceArgs);
  @$RefID = $result->PostchangeofuserResult->string[0];
}catch (Exception $e){
  redirect('rlb/Apierror');
}
$isReg = @substr($RefID, 3,3) == 'RF0';
if($RefID!="400" && $isReg){
 @$ownerNames = $result->PostchangeofuserResult->string[1];
 @$InvoiceNo = $result->PostchangeofuserResult->string[3];
            // Get invoice details
            // pass the client instance to avoid overloading the server
 return $this->getRegInvoiceDetails($client, $RefID, $ownerNames);
} elseif($RefID == '400'){
  redirect('rlb/error');
} else {
  redirect('rlb/Apierror');
}
}

function postExtentionOfUsers()
{
  $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
  $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

  $serviceArgs = array(
    "API_Id" => $apiID,
    "ownerNames" => $this->input->post('ownerNames'),
    "ownerAddress" => $this->input->post('ownerAddress'),
    "applicantNames" => $this->input->post('applicantNames'),
    "applicantAddress" => $this->input->post('applicantAddress'),
    "applicantInterest" => $this->input->post('applicantInterest'),
    "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
    "physicalAddress" => $this->input->post('physicalAddress'),
    "acreage" => $this->input->post('acreage'),
    "applicationNumber" => $this->input->post('applicationNumber'),
    "zone" => $this->input->post('zone'),
    "Form_Id" => "RF02",
    "subdivisionInvolved" => $this->input->post('subdivisionInvolved'),
    "permissionApplied" => $this->input->post('permissionApplied'),
    "subdivisionApllicationNo" => $this->input->post('subdivisionApllicationNo'),
    "proposedDevelopment" =>  $this->input->post('proposedDevelopment'),
    "currentUse" => $this->input->post('currentUse'),
    "lastUseDate" => $this->input->post('lastUseDate'),
    "meansofAccessConstruction" => $this->input->post('meansofAccessConstruction'),
    "natureProposedDevelopment" => $this->input->post('natureProposedDevelopment'),
    "wallsDetailswallsDetails" => $this->input->post('wallsDetails'),
    "waterSupply" => $this->input->post('waterSupply'),
    "sewerageDisposal" => $this->input->post('sewerageDisposal'),
    "refuseDisposal" => $this->input->post('refuseDisposal'),
    "surfaceWaterDisposal" =>$this->input->post('surfaceWaterDisposal'),
    "easementDetails" => $this->input->post('easementDetails'),
    "landAffected" => $this->input->post('landAffected'),
    "buildingsCover" => $this->input->post('buildingsCover'),
    "currentSiteCover" => $this->input->post('currentSiteCover'),
    "proposedSiteCover" => $this->input->post('proposedSiteCover'),
    "wallsDetails" => $this->input->post('wallsDetails'),
    "zoneid" =>$this->input->post('zone_id'),
    "Rt" => "category a",
    "Rtid" => "4",
    "ward" => $this->input->post('zone'),
    "subcounty" => $this->input->post('zone'),
    );
        //$this->session->set_userdata($serviceArgs);
       // $this->session->set_userdata('applicantProfession',$this->input->post('applicantProfession'));

try {
  $client = new SoapClient($url);
  $result = $client->postExtensionofuse($serviceArgs);
  @$RefID = $result->postExtensionofuseResult->string[0];
}catch (Exception $e){
  redirect('rlb/Apierror');
}
$isReg = @substr($RefID, 3,3) == 'RF0';
if($RefID!="400" && $isReg){
  @$ownerNames = $result->postExtensionofuseResult->string[1];
  @$InvoiceNo = $result->postExtensionofuseResult->string[3];
            // Get invoice details
            // pass the client instance to avoid overloading the server
  return $this->getRegInvoiceDetails($client, $RefID, $ownerNames);
}elseif($RefID=="400"){
            redirect('rlb/error'); // error it already exists
          }else {
            redirect('rlb/Apierror');
          }
        }

        function postLandSubdivison()
        {
          $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
          $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

          $serviceArgs = array(
            "API_Id" => $apiID,
            "ownerNames" => $this->input->post('ownerNames'),
            "ownerAddress" => $this->input->post('ownerAddress'),
            "applicantNames" => $this->input->post('applicantNames'),
            "applicantAddress" => $this->input->post('applicantAddress'),
            "applicantInterest" => $this->input->post('applicantInterest'),
            "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
            "physicalAddress" => $this->input->post('physicalAddress'),
            "acreage" => $this->input->post('acreage'),
            "applicationNumber" => $this->input->post('applicationNumber'),
            "zone" => $this->input->post('zone'),
            "Form_Id" => "RF04",
            "subdivisionInvolved" => $this->input->post('subdivisionInvolved'),
            "permissionApplied" => $this->input->post('permissionApplied'),
            "subdivisionApllicationNo" => $this->input->post('subdivisionApplicationNo'),
            "proposedDevelopment" => $this->input->post('proposedDevelopment'),
            "currentUse" => $this->input->post('currentUse'),
            "lastUseDate" => $this->input->post('lastUseDate'),
            "meansofAccessConstruction" => $this->input->post('meansofAccessConstruction'),
            "natureProposedDevelopment" => $this->input->post('natureProposedDevelopment'),
            "wallsDetails" => $this->input->post('wallsDetails'),
            "waterSupply" => $this->input->post('waterSupply'),
            "sewerageDisposal" => $this->input->post('sewerageDisposal'),
            "refuseDisposal" => $this->input->post('refuseDisposal'),
            "surfaceWaterDisposal" => $this->input->post('surfaceWaterDisposal'),
            "easementDetails" => $this->input->post('easementDetails'),
            "zoneid" =>$this->input->post('zone_id'),
            "Rt" => "category a",
            "Rtid" => "5",
            "ward" => $this->input->post('zone'),
            "subcounty" => $this->input->post('zone'),
            "noofSubPlots" => $this->input->post('noofSubPlots')
            );
       // $this->session->set_userdata($serviceArgs);
       // $this->session->set_userdata('applicantProfession',$this->input->post('applicantProfession'));
try {
  $client = new SoapClient($url);
  $result = $client->PostLandsubdivision($serviceArgs);
  @$RefID = $result->PostLandsubdivisionResult->string[0];
}catch (Exception $e){
  redirect('rlb/Apierror');
}
$isReg = @substr($RefID, 3,3) == 'RF0';
if($RefID!="400" && $isReg){
  @$ownerNames = $result->PostLandsubdivisionResult->string[1];
  @$InvoiceNo = $result->PostLandsubdivisionResult->string[3];
            // Get invoice details
            // pass the client instance to avoid overloading the server
  return $this->getRegInvoiceDetails($client, $RefID, $ownerNames);
}elseif($RefID=="400"){
            redirect('rlb/error'); // error it already exists
          }else {
            redirect('rlb/Apierror');
          }
        }


        function getLoadingZoneDetails()
        {
          $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
          $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

          $serviceArgs = array(
            "API_Id" => $apiID,
            "RefId" => $this->input->post('biz_id'),                   
            );

          try {
            $client = new SoapClient($url);
            $result = $client->PostLoadingZones($serviceArgs);
            @$RefID = $result->PostLoadingZonesResult->string[0];

            if($RefID!="400" && $isReg){

            }          

          } catch (Exception $e){
            redirect('rlb/Apierror');
          }

        }

        function print_sbp()
        {
          $token = $this->session->userdata('token');
          $BusinessID = @trim($this->input->post('biz_idd'));
          $Year = $this->input->post('year');
          $PhoneNumber = $this->session->userdata('jpwnumber');

          $url = "http://192.168.6.10/JamboPayServices/api/payments/GetBusinessDetails?stream=sbp&BusinessID=$BusinessID&Year=$Year&PhoneNumber=$PhoneNumber";

          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer ' . $token,
            'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
            ));

          $result = curl_exec($ch);
          curl_close($ch);
          $res = json_decode($result);

        #var_dump($res); die();

          if (isset($res->ErrorCode)) {
            $sbpdata = array(
              'restext' => $res->Message,
              'rescode' => $res->ErrorCode
              );
          } else {
            $sbpdata = array(
              'approvalstatus' => $res->Status,
              'sbpnumber' => $res->BusinessID,
              'year' => $res->Year,
              'bizname' => $res->BusinessName,
              'pinno' => $res->Pin,
              'biztype' => "",
              'activitycode' => $res->ActivityCode,
              'paidfee' => $res->Amount,
              'inwords' => $res->AmountInWords,
              'pobox' => $res->POBox,
              'plotnumber' => $res->PlotNumber,
              'street' => $res->PhysicalAddress,
              'issuedate' => $res->DateIssued,
              'bizactivityname' => $res->ActivityDescription,
              'bizzid' => $res->BusinessID,
              'receiptno' => $res->ReceiptNumber,
              'restext' => "OK",
              'rescode' => 0
              );
          }
        #var_dump($sbpdata); die();
          return $sbpdata;
        }

        function postSubdivisionLargeSchemes()
        {
          $url = 'http://35.164.42.112/mainsector.asmx?wsdl';
          $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';



          $serviceArgs = array(
            "API_Id" => $apiID,
            "ownerNames" => $this->input->post('ownerNames'),
            "ownerAddress" => $this->input->post('ownerAddress'),
            "applicantNames" => $this->input->post('applicantNames'),
            "applicantAddress" => $this->input->post('applicantAddress'),
            "applicantInterest" => $this->input->post('applicantInterest'),
            "lrNo" => @str_replace(' ', '', strtoupper($this->input->post('lrNo'))),
            "physicalAddress" => $this->input->post('physicalAddress'),
            "acreage" => $this->input->post('acreage'),
            "applicationNumber" => $this->input->post('applicationNumber'),
            "zone" => $this->input->post('zone'),
            "Form_Id" => "RF05",
            "subdivisionInvolved" => $this->input->post('subdivisionInvolved'),
            "permissionApplied" => $this->input->post('permissionApplied'),
            "subdivisionApllicationNo" => $this->input->post('subdivisionApplicationNo'),
            "proposedDevelopment" => $this->input->post('proposedDevelopment'),
            "currentUse" => $this->input->post('currentUse'),
            "lastUseDate" => $this->input->post('lastUseDate'),
            "meansofAccessConstruction" => $this->input->post('meansofAccessConstruction'),
            "natureProposedDevelopment" => $this->input->post('natureProposedDevelopment'),
            "wallsDetails" => $this->input->post('wallsDetails'),
            "waterSupply" => $this->input->post('waterSupply'),
            "sewerageDisposal" => $this->input->post('sewerageDisposal'),
            "refuseDisposal" => $this->input->post('refuseDisposal'),
            "surfaceWaterDisposal" => $this->input->post('surfaceWaterDisposal'),
            "easementDetails" => $this->input->post('easementDetails'),
            "zoneid" =>$this->input->post('zone_id'),
            "Rt" => "category a",
            "Rtid" => "3",
            "ward" => $this->input->post('zone'),
            "subcounty" => $this->input->post('zone'),
            );
        //$this->session->set_userdata($serviceArgs);
        //$this->session->set_userdata('applicantProfession',$this->input->post('applicantProfession'));

try {
  $client = new SoapClient($url);
  $result = $client->postsubdivisonlargeschemes($serviceArgs);
            @$RefID = $result->postsubdivisonlargeschemesResult->string[0];//var_dump($result);exit();
          }catch (Exception $e){
            redirect('rlb/Apierror');
          }
          $isReg = @substr($RefID, 3,3) == 'RF0';
          if($RefID!="400" && $isReg){
           @$ownerNames = $result->postsubdivisonlargeschemesResult->string[1];
           @$InvoiceNo = $result->postsubdivisonlargeschemesResult->string[3];
            // Get invoice details
            // pass the client instance to avoid overloading the server
           return $this->getRegInvoiceDetails($client, $RefID, $ownerNames);
         }elseif($RefID=="400"){
            redirect('rlb/error'); // error it already exists
          }else {
            redirect('rlb/Apierror');
          }
        }

        function preparePayment($InvoiceReferenceNumber=NULL,$year_check=NULL)
        {
          $token = $this->session->userdata('token');
          $number = $this->session->userdata('jpwnumber');
          $name = $this->session->userdata('name');
          $agentRef = strtoupper(substr(md5(uniqid()), 25));
          $tosend = array(
            'Stream' => "regularizationinvoice",
            'RegularizationType' => ($this->input->post('form_id') == 'RF08')?'0':'1',
            'PhoneNumber' => $number,
            'InvoiceReferenceNumber' => $this->input->post('RefID'),
            'PaidBy' => $this->session->userdata('name'),
            'PaymentTypeID' => '1'
            );
        $url = "http://192.168.6.10/JamboPayServices/api/payments"; //REST_URL;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer ' . $token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
          ));
        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);
        @$rescode = $res->ResponseCode;
        if(!isset($rescode)){
          if(isset($res->ErrorCode) && $res->ErrorCode == 404){
      redirect('rlb/invalidRefid');  // invalid reference id
    }else{
      return json_decode($result,TRUE); // Pass Message $res->Message
    }
  }elseif(isset($rescode)){
    $pay_send = array(
      'Stream' => 'regularizationinvoice',
      'TransactionID'=>(isset($res->TransactionID))?$res->TransactionID:'',
                //'InvoiceNumber' => $InvoiceNo,
      'Pin' => $this->input->post('pin'),
      'PhoneNumber' => (isset($res->PhoneNumber))?$res->PhoneNumber:'',
      'PaymentTypeID' => '1',
      );
        $url = "http://192.168.6.10/JamboPayServices/api/payments"; //REST_URL;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($pay_send));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: bearer ' . $token,
          'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
          ));
        $pay_result = curl_exec($ch);
        $pay_res=json_decode($pay_result);
        @$rescode = $pay_res->ResponseCode;
        @$exception = isset($pay_res->ErrorCode) && $pay_res->ErrorCode == 404;
        if(isset($rescode) ){ //|| $exception
          return $data = array(
            "Reference_ID" => $this->input->post('RefID'),
            "Invoice_Number"=>$this->input->post('InvoiceNo'),
            "Payee" => $this->session->userdata('name'),
            "ReceiptNo"=> @$pay_res->ReceiptNumber,
            "amount" => $this->input->post('amount'),
            "year"=> date("Y"),
            "form_id"=> $this->input->post('form_id'),
            );
        }elseif(!isset($rescode)){
          return json_decode($pay_result,TRUE); // Pass Message $res->Message
        }
      }

    }

    function completeRegulationPayment(){
      $url = "http://35.164.42.112/mainsector.asmx?wsdl";
      $apiID="d3775f2b-8fbe-4701-9dad-3db643021dd5";
      $Refid=str_replace(" ","",$this->input->post('RefId'));
      $InvoiceNum= $this->input->post('InvoiceNo');
      $Amount= $this->input->post('amount');
      $paymentinfo = array(
        "API_Id"=>$apiID,
          "Refid" => $this->input->post('RefId'), //'NCCRF08195'
          "InvoiceNum"=>$this->input->post('InvoiceNo'), //'840195'
          "amount" => $this->input->post('amount'), //'250000'
          "year"=> date("Y"),
          "form_id"=> $this->input->post('form_id'),//'0',
        //"certificate" => $this->input->post('amount')
          );
      try {
        $client = new SoapClient($url);
        $result = $client->PostPayment($paymentinfo);
        $rescode = $result->PostPaymentResult;
      }catch (Exception $e){
        redirect('rlb/Apierror');
      }
      return $data = array(
       "Reference_ID" => $this->input->post('RefId'),
       "Invoice_Number"=>$this->input->post('InvoiceNo'),
       "Payee" => $this->session->userdata('name'),
       "amount" => $this->input->post('amount'),
       "year"=> date("Y"),
       "form_id"=> $this->input->post('form_id'),
       );
    }

    function payRegDetails($check = ''){

     $url="http://35.164.42.112/mainsector.asmx?wsdl";
     $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
     $RefID = strtoupper(str_replace(" ","",$this->input->post('RefId')));
     $serviceArgs = array(
       "API_Id" => $apiID,
       "RefId" => $RefID
       );
     try{
       $client = new SoapClient($url);
       $result2 = $client->GetreginvoiceDetails($serviceArgs);
     }catch (Exception $e){
      redirect('rlb/Apierror');
    }
    @$lrNo = $result2->GetreginvoiceDetailsResult->lrNo;
    @$rescode = $result2->GetreginvoiceDetailsResult->Response_Code;
    if (!isset($rescode) && isset($lrNo)){
     $data = array(
      "RefID"=>@str_replace('/', '', $RefID),
      "ownerNames" => @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->ownerNames),
      "ownerAddress" =>  @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->ownerAddress),
      "lrNo" =>  @str_replace('/', '_', $result2->GetreginvoiceDetailsResult->lrNo),
      "physicalAddress" =>  @$result2->GetreginvoiceDetailsResult->physicalAddress,
      "amount" =>  @str_replace('/', '', $result2->GetreginvoiceDetailsResult->amount),
      "form_id" => @substr($RefID, 3, 4),
      "form_status" =>  @$result2->GetreginvoiceDetailsResult->form_status,
      "InvoiceNo" =>  @str_replace('/', '', $result2->GetreginvoiceDetailsResult->InvoiceNum),
      );
     return $data;
   }
   else{
    redirect('rlb/invalidRefid');
  }
}

function payRegBuildingDetails($check = ''){
  $url="http://35.164.42.112/mainsector.asmx?wsdl";
  $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
  $RefID = strtoupper(str_replace(" ","",$this->input->post('RefId')));
  $serviceArgs = array(
   "API_Id" => $apiID,
           "RefId" =>  $RefID //NCCRF0851
          );
  try{
    $client = new SoapClient($url);
      $result2 =  $client->GetbuildinginvoiceDetails($serviceArgs); //var_dump($result2);exit();
    }catch (Exception $e){
      redirect('rlb/Apierror');
    }
    @$lrNo = $result2->GetbuildinginvoiceDetailsResult->lrNo;
    @$Response_Code = $result2->GetbuildinginvoiceDetailsResult->Response_Code;
    if(!isset($Response_Code) && isset($lrNo)){
      $data = array(
        "RefID" => @str_replace('/', '', $RefID),
        "ownerNames" =>  @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->ownerNames),
        "ownerAddress" =>  @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->ownerAddress),
        "lrNo" =>  @str_replace('/', '_', $result2->GetbuildinginvoiceDetailsResult->lrNo),
        "physicalAddress" =>  @$result2->GetbuildinginvoiceDetailsResult->physicalAddress,
        "amount" =>  @str_replace('/', '', $result2->GetbuildinginvoiceDetailsResult->amount),
        "architectNames" =>  @$result2->GetbuildinginvoiceDetailsResult->architectNames,
        "architectEmail" =>  @$result2->GetbuildinginvoiceDetailsResult->architectEmail,
        "engineerNames" =>  @$result2->GetbuildinginvoiceDetailsResult->engineerNames,
        "engineerEmail" =>  @$result2->GetbuildinginvoiceDetailsResult->engineerEmail,
        "InvoiceNo" =>  @str_replace('/', '', $result2->GetbuildinginvoiceDetailsResult->InvoiceNum),
        "form_status" =>  @$result2->GetbuildinginvoiceDetailsResult->form_status,
        "form_id" => @substr($RefID, 3, 4),
        );
return $data;
}else {
  redirect('rlb/invalidRefid');
}
}

function printRegDoc($refid){
  $url="http://35.164.42.112/mainsector.asmx?wsdl";
  $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
  $serviceArgs = array(
   "API_Id" => $apiID,
           "RefId" =>  $refid //NCCRF0851
           );
  try{
    $client = new SoapClient($url);
    $result2 =  $client->Printregdoc($serviceArgs);
    $res = @json_decode($result2);
  }catch (Exception $e){
        redirect('rlb/invalidRefid'); //redirect("rlb/Apierror");
      }
      @$responseCode = $result2->PrintbuildingdocResult->Response_Code;
      @$lrNo = $result2->PrintregdocResult->lrNo;
      if(!isset($responseCode) && isset($lrNo)){
        $data = array(
            "ownerNames" => @$result2->PrintregdocResult->ownerNames,  //"grfgvserg"
            "ownerAddress" => @$result2->PrintregdocResult->ownerAddress,  //"0"
            "InvoiceNum" => @$result2->PrintregdocResult->InvoiceNum,  //"21001"
            "applicantNames" => @$result2->PrintregdocResult->applicantNames,  //"raila odi"
            "applicantAddress" => @$result2->PrintregdocResult->applicantAddress,  // "0"
            "applicantInterest" => @$result2->PrintregdocResult->applicantInterest,  // "adsfas"
            "lrNo" => @$result2->PrintregdocResult->lrNo,  //"433333323"
            "physicalAddress" => @$result2->PrintregdocResult->physicalAddress,  //"2132"
            "zone" => @$result2->PrintregdocResult->zone,  // "upperhill"
            "acreage" => @$result2->PrintregdocResult->acreage,  // "12"
            "amount" => @$result2->PrintregdocResult->amount,  //"120000"
            "subdivisionInvolved" => @((($result2->PrintregdocResult->subdivisionInvolved)=='0')?'No':'Yes'),  //"0"
            "permissionApplied" => @((($result2->PrintregdocResult->permissionApplied)=='0')?'No':'Yes'),  //"0"
            "proposedDevelopment" => @$result2->PrintregdocResult->proposedDevelopment,  //"sdfds"
            "currentUse" => @$result2->PrintregdocResult->currentUse,  //"sdf"
            "lastUseDate" => @$result2->PrintregdocResult->lastUseDate,  //"2/16/2012 12:00:00 AM"
            "meansofAccessConstruction" => @((($result2->PrintregdocResult->meansofAccessConstruction)=='0')?'No':'Yes'),//"0"
            "natureProposedDevelopment" => @$result2->PrintregdocResult->natureProposedDevelopment,  // "bgf"
            "wallsDetails" => @$result2->PrintregdocResult->wallsDetails,  //"fghfghgf"
            "waterSupply" => @$result2->PrintregdocResult->waterSupply,  //"xfg"
            "sewerageDisposal" => @$result2->PrintregdocResult->sewerageDisposal,  //"dfgdf"
            "refuseDisposal" => @$result2->PrintregdocResult->refuseDisposal,  //"dfgdf"
            "surfaceWaterDisposal" => @$result2->PrintregdocResult->surfaceWaterDisposal,  //"dfgdf"
            "landAffected" => @$result2->PrintregdocResult->landAffected,  //"dfgdf"
            "buildingsCover" => @$result2->PrintregdocResult->buildingsCover,  //"dfgdf"
            "currentSiteCover" => @$result2->PrintregdocResult->currentSiteCover,  //"dfgdf"
            "easementDetails" => @$result2->PrintregdocResult->easementDetails,  //"dfsbdfb"
            "form_status" => @$result2->PrintregdocResult->form_status,  //"0"
            );
$this->session->set_userdata($data);
       //print_r($this->session->all_userdata());
}else {
  redirect('rlb/invalidRefid');
}
}

function printBuildingDoc($refid){
  $url="http://35.164.42.112/mainsector.asmx?wsdl";
  $apiID="bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3";
  $serviceArgs = array(
   "API_Id" => $apiID,
           "RefId" =>  $refid //NCCRF0851
           );
  try{
    $client = new SoapClient($url);
    $result2 =  $client->Printbuildingdoc($serviceArgs);
  }catch (Exception $e){
    redirect('rlb/Apierror');
  }
  @$responseCode = $result2->PrintbuildingdocResult->Response_Code;
  @$lrNo = $result2->PrintbuildingdocResult->lrNo;
  if(!isset($responseCode) && isset($lrNo)){
    $data = array(
            "InvoiceNum" => @$result2->PrintbuildingdocResult->InvoiceNum , //4604
            "lrNo" => @$result2->PrintbuildingdocResult->lrNo , //545
            "physicalAddress" => @$result2->PrintbuildingdocResult->physicalAddress , //0
            "amount" => @$result2->PrintbuildingdocResult->amount , //5050000
            "currentLandUse" => @$result2->PrintbuildingdocResult->currentLandUse , //ghgfhgh
            "ownerAddress" => @$result2->PrintbuildingdocResult->ownerAddress , //0
            "ownerEmail" => @$result2->PrintbuildingdocResult->ownerEmail , //gfrtdh@gmail.com
            "ownerNames" => @$result2->PrintbuildingdocResult->ownerNames , //erftewftwegftr ter
            "ownerPhoneNo" => @$result2->PrintbuildingdocResult->ownerPhoneNo , //45645645
            "architectRegNo" => @$result2->PrintbuildingdocResult->architectRegNo , //456
            "architectNames" => @$result2->PrintbuildingdocResult->architectNames , //thgfhtrh
            "architectEmail" => @$result2->PrintbuildingdocResult->architectEmail , //tfghtrhy@gmail.com
            "architectPhoneNo" => @$result2->PrintbuildingdocResult->architectPhoneNo , //7567567
            "architectPostalAddress" => @$result2->PrintbuildingdocResult->architectPostalAddress , //56565656567
            "engineerNames" => @$result2->PrintbuildingdocResult->engineerNames , //vdsvds
            "engineerEmail" => @$result2->PrintbuildingdocResult->engineerEmail , //cvbc@gmail.com
            "engineerPhoneNo" => @$result2->PrintbuildingdocResult->engineerPhoneNo , //5646456
            "engineerPostalAddress" => @$result2->PrintbuildingdocResult->engineerPostalAddress , //7656756
            "zone" => @$result2->PrintbuildingdocResult->zone , //fghgfhgfh
            "projectDetailedDescription" => @$result2->PrintbuildingdocResult->projectDetailedDescription , //
            "landTenure" => @$result2->PrintbuildingdocResult->landTenure , //2
            "numberofUnits" => @$result2->PrintbuildingdocResult->numberofUnits , //45
            "plotSize" => @$result2->PrintbuildingdocResult->plotSize , //55
            "nearestRoad" => @$result2->PrintbuildingdocResult->nearestRoad , //tyujytjty
            "estate" => @$result2->PrintbuildingdocResult->estate , //ytjuy
            "subcounty" => @$result2->PrintbuildingdocResult->subcounty , //ytjyryr
            "ward" => @$result2->PrintbuildingdocResult->ward , //tyjuyr
            "soilType" => @$result2->PrintbuildingdocResult->soilType , //xcvxzvx</soilType>
            "waterSupplier" => @$result2->PrintbuildingdocResult->waterSupplier , //jytttttttttttttttttttttttttttt
            "sewerageDisposalMethod" => @$result2->PrintbuildingdocResult->sewerageDisposalMethod , //yjhntytytytytyj
            "projectCost" => @$result2->PrintbuildingdocResult->projectCost , //1000000000
            "inspectionFees" => @$result2->PrintbuildingdocResult->inspectionFees , //567567
            "externalWalls" => @$result2->PrintbuildingdocResult->externalWalls , //fdbfvdb fdb
            "mortar" => @$result2->PrintbuildingdocResult->mortar , //fdbfdb
            "roofCover" => @$result2->PrintbuildingdocResult->roofCover , //dbfd
            "dampProofCourse" => @$result2->PrintbuildingdocResult->dampProofCourse , //dsbdfb
            "basementArea" => @$result2->PrintbuildingdocResult->basementArea , //45
            "mezzaninefloorArea" => @$result2->PrintbuildingdocResult->mezzaninefloorArea , //55
            "floorArea" => @$result2->PrintbuildingdocResult->floorArea , //55
            "floorArea1" => @$result2->PrintbuildingdocResult->floorArea1 , //55
            "floorArea2" => @$result2->PrintbuildingdocResult->floorArea2 , //55
            "floorArea3" => @$result2->PrintbuildingdocResult->floorArea3 , //55
            "floorArea4" => @$result2->PrintbuildingdocResult->floorArea4 , //55
            "Others" => @$result2->PrintbuildingdocResult->Others , //55
            "form_status" => @$result2->PrintbuildingdocResult->form_status , //0
            );
$this->session->set_userdata($data);
}else {
  redirect('rlb/invalidRefid');
}
}

function prepareRegPayment($RefID='', $InvoiceNum=''){

  $token = $this->session->userdata('token');
  $number = $this->session->userdata('jpwnumber');
  $name = $this->session->userdata('name');
  $InvoiceNum= $this->input->post('InvoiceNo');
  $RefID = $this->input->post('RefID');
  $tosend = array(
    'Stream' => "regularizationinvoice",
    'RegularizationType' =>  $this->input->post('Regtype'),
    'PhoneNumber' => $number,
        //'InvoiceNumber' => $InvoiceNum,
        'InvoiceReferenceNumber' => $RefID, //'NCCRF0852'
        'PaidBy' => $this->session->userdata('name'),
        'PaymentTypeID' => '1'
        );
  $url = "http://192.168.6.10/JamboPayServices/api/payments";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: bearer ' . $token,
    'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
    ));
  $result = curl_exec($ch);
    $res = json_decode($result); #echo "<pre>"; var_dump($res); die();
    $this->traceAPI('API:Response','Prepare Payment',$result);
    curl_close($ch);

    @$rescode = $res->ResponseCode;
    if(isset($rescode)){
     $data = array(
      "Refid"=>@$res->InvoiceReferenceNumber,
      "OwnersName"=>@$res->RegularizationInvoiceDetails[49]->Description,
      "ArchitectsName"=>@$res->RegularizationInvoiceDetails[5]->Description,
      "EngineersName"=>@$res->RegularizationInvoiceDetails[19]->Description,
      "LrNo"=>@str_replace('/', '_', $res->RegularizationInvoiceDetails[38]->Description),
      "InvoiceStatus"=> @$res->InvoiceStatus,
      "InvoiceNo"=> @$res->InvoiceNumber,
      "Amount" => @$res->Amount,
      "name"=>@$res->PaidBy,
      "PhoneNumber"=> @$res->PhoneNumber,
      "TransactionID"=>@$res->TransactionID,
      "rescode"=> @$res->ResponseCode,
      "message"=>"ok"
      );
     return $data;
   }elseif(!isset($rescode)){
     $data=array(
      "rescode"=>$res->ErrorCode,
      "message"=>$res->Message,
      );
     if($data['rescode'] == 2030){
      redirect('rlb/topup');
    }else {
      return $data;
    }
  }

}

function completeRegPayment($TransactionID='',$InvoiceNo=''){

  $token =$this->session->userdata('token');
  $amount = $this->input->post('amount');
  $InvoiceNo = $this->input->post('InvoiceNo');
  $LicenseID = $this->input->post('Refid');
  $TransactionID = $this->input->post('transid');
  $pin = $this->input->post('jp_pin');
  $number = $this->session->userdata('jpwnumber');

  $tosend = array(
   'Stream' => 'regularizationinvoice',
   'TransactionID'=> $TransactionID,
   'InvoiceNumber' => $InvoiceNo,
   'Pin' => $pin,
   'PhoneNumber' => $number,
   'PaymentTypeID' => '1',
   );
  $url = "http://192.168.6.10/JamboPayServices/api/payments";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($tosend));
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   'Authorization: bearer ' . $token,
   'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
   ));
  $pay_result = curl_exec($ch);
  $pay_res=json_decode($pay_result);
  @$rescode = $pay_res->ResponseCode;
  if(isset($rescode)){
        //var_dump($pay_res);
   $data = array(
    "Reference_ID" => $LicenseID,
    "Invoice_Number"=> $pay_res->InvoiceNumber,
    "Payee" => $this->session->userdata('name'),
    "amount" => $pay_res->Amount,
    "year"=> date("Y"),
    "form_id"=> @substr($LicenseID, 3, 4),
    "ReceiptNo"=> @$pay_res->ReceiptNumber,
          "rescode"=> $pay_res->ResponseCode, // "200",
          "message"=>"ok"
          );
   return $data;
 }elseif(!isset($rescode)){
  return $data=array(
    "rescode"=>@$pay_res->ErrorCode,
    "message"=>$pay_res->Message,
    );
}
}

function printRlbinvoice($ownername = "", $refid = "", $lrno ="", $invoice="", $regType="", $amount=0,$form_id="0"){
  $this->load->library('zend');
  $a=$this->load->library('amount_to_words');
  $this->zend->load('Zend/Pdf');
  $this->zend->load('Zend/Barcode');

  $fileName =APPPATH.'assets/back/receipts/rlbinvoice.pdf';
    $ownername = $ownername; //"Owners Name";
    $lrno = $lrno; //"LR NUMBER";
    $Refid = $refid; //"REFRENCE ID";
    $InvoiceNo = $invoice; //"InvoiceNumber";
    $regType = $regType; //Regularization Type
    $amount = $amount; //"2000000";
    $Status = $form_id; //'0';
    if($Status=="0"){
      $Status = "WAITING FOR APPROVAL";
    }else{
      $Status = "APPROVED";
    }

    $pdf = Zend_Pdf::load($fileName);
    $page=$pdf->pages[0];

    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
    $page->drawText($ownername, 250, 528);
    $page->drawText($lrno, 250, 480);
    $page->drawText($Refid, 250, 440);
    $page->drawText($InvoiceNo, 250, 400);
    // $page->drawText($regType, 250, 360);
    $page->drawText(number_format($amount,2), 320, 357);


    $pdfData = $pdf->render();
    header("Content-Disposition: inline; filename=Regularization_invoice.pdf");
    header("Content-type: application/x-pdf");
    echo $pdfData;
  }

  function reprintRlbreceipt($refid='', $receipt='',$payee='',$amount='0.00',$form_id=null)
  {
    //var_dump($refid);var_dump($receipt);var_dump($payee);var_dump($amount);var_dump($form_id);exit;
    $this->load->library('zend');
    $a = $this->load->library('amount_to_words');
    $this->zend->load('Zend/Pdf');
    $this->zend->load('Zend/Barcode');

    $fileName = APPPATH . 'assets/back/receipts/rlb_receipt.pdf';
    //var_dump($fileName);exit();
    $receiptno = $receipt;//"RECEIPTNUMBER"; //Refid/InvoiceNumber
    $date = date("Y-m-d");
    $total_amount_due = "0.00";
    $balance_due = "0.00";
    $cashier = $payee; //"Owner Name"; // ownerName
    $penalties ="0";
    $amount = $amount; //is_double($amount)?$amount:'0.00';//"20000.00"; // amount

    $amount = str_replace(',', '', $amount);
    if (is_numeric($amount)) {
      $amount_in_words = "** " . strtoupper($this->amount_to_words->convert_number($amount)) . " SHILLINGS ONLY**";
    } else $amount_in_words = "NOT AVAILABLE";

    $description = $this->getRegularizationType($refid)." PAYMENT (LICENSE No. ".$refid." )";

    $pdf = Zend_Pdf::load($fileName);
    $page = $pdf->pages[0];

    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
    $page->drawText($receiptno, 150, 528);
    $page->drawText($date, 480, 528);
    $page->drawText($cashier, 250, 480);
    $page->drawText(number_format($amount, 2), 480, 480);

    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8);
    $page->drawText($amount_in_words, 150, 437);

    $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
    $page->drawText($description, 150, 390);
    $page->drawText(number_format($penalties, 2), 450, 270);
    $page->drawText(number_format($amount, 2), 450, 250);
    $page->drawText(number_format($amount, 2), 450, 225);
    $page->drawText(number_format($balance_due, 2), 450, 200);

    $barcodeOptions = array('text' => $receiptno, 'barHeight' => 40, 'factor' => 2.5, 'font' => APPPATH . 'assets/back/fonts/SWANSEBI.TTF');
    $rendererOptions = array(
      'topOffset' => 600,
      'leftOffset' => 295
      );
    $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


    $pdfData = $pdf->render();
    header("Content-Disposition: inline; filename=receipt.pdf");
    header("Content-type: application/x-pdf");
    echo $pdfData;
    //redirect('en/land_rates');
  }

  function getRegularizationType($Invoice_number)
  {
    if(preg_match('/^NCCRF01/', $Invoice_number)) {
      return 'CHANGE OF USE';
    } elseif(preg_match('/^NCCRF02/', $Invoice_number)) {
      return 'EXTENSION OF USE';
    } elseif(preg_match('/^NCCRF04/', $Invoice_number)) {
      return 'SUB DIVISION';
    } elseif(preg_match('/^NCCRF07/', $Invoice_number)) {
      return 'AMALGAMATION';
    } elseif(preg_match('/^^NCCRF08/', $Invoice_number)) {
      return 'BUILDING STRUCTURES';
    }
  }

  public function saveBuildingStructure($data)
  {
    $details=array(
      "phone_number"=>$this->session->userdata('jpwnumber'),
      "details"=>$data,        
      );
    $this->db->insert('building_structure',$details);
  }

  public function getBuildingStructureDetails()
  {
    $this->db->where('phone_number',$this->session->userdata('jpwnumber'));
    $this->db->limit('1');
    return $this->db->get('building_structure');
  }

  
}
