<?php

class Rlb_model extends CI_Model
{
    public function changeUserStep1()
    {
        $regdata = array(
            'applicationNumber' => $this->input->post('applicationNumber'),
            'zone' => explode('____', $this->input->post('zone'))[1],
            'zone_id' => explode('____', $this->input->post('zone'))[0],
            'ownerNames' => $this->input->post('ownerNames'),
            'ownerAddress' => $this->input->post('ownerAddress'),
            'applicantNames' => $this->input->post('applicantNames'),
            'applicantAddress' => $this->input->post('applicantAddress'),
            'applicantInterest' => $this->input->post('applicantInterest'),
            'lrNo' => $this->input->post('lrNo'),
        );

        return $regdata;
    }
    public function changeUserStep2()
    {
        $regdata = array(
            'applicationNumber' => $this->input->post('applicationNumber'),
            'ownerNames' => $this->input->post('ownerNames'),
            'ownerAddress' => $this->input->post('ownerAddress'),
            'applicantNames' => $this->input->post('applicantNames'),
            'applicantAddress' => $this->input->post('applicantAddress'),
            'applicantInterest' => $this->input->post('applicantInterest'),
            'lrNo' => $this->input->post('lrNo'),

            'proposedDevelopment' => $this->input->post('proposedDevelopment'),
            'physicalAddress' => $this->input->post('physicalAddress'),
            'currentUse' => $this->input->post('currentUse'),
            'lastUseDate' => $this->input->post('lastUseDate'),
            'acreage' => $this->input->post('acreage'),
            'natureProposedDevelopment' => $this->input->post('natureProposedDevelopment'),
            'meansOfAccessContruction' => $this->input->post('meansOfAccessContruction'),
            'subdivisionInvolved' => $this->input->post('subdivisionInvolved'),
            'permissionApplied' => $this->input->post('permissionApplied'),
            'subdivisionApplicationNo' => $this->input->post('subdivisionApplicationNo'),
        );

        return $regdata;
    }
    public function structuresStep1()
    {
        $regdata = array(
            'ownerNames' => $this->input->post('ownerNames'),
            'ownerEmail' => $this->input->post('ownerEmail'),
            'ownerPhoneNo' => $this->input->post('ownerPhoneNo'),
            'ownerPostalAddress' => $this->input->post('ownerPostalAddress'),
            'architectRegNo' => $this->input->post('architectRegNo'),
            'architectNames' => $this->input->post('architectNames'),
            'architectEmail' => $this->input->post('architectEmail'),
            'architectPhoneNo' => $this->input->post('architectPhoneNo'),
            'architectPostalAddress' => $this->input->post('architectPostalAddress'),
            'engineerRegNo' => $this->input->post('engineerRegNo'),
            'engineerNames' => $this->input->post('engineerNames'),
            'engineerEmail' => $this->input->post('engineerEmail'),
            'engineerPhoneNo' => $this->input->post('engineerPhoneNo'),
            'engineerPostalAddress' => $this->input->post('engineerPostalAddress'),
        );

        return $regdata;
    }
    public function structuresStep2()
    {
        $regdata = array(
            'currentLandUse' => $this->input->post('currentLandUse'),
            'zone' => explode('____', $this->input->post('zone'))[1],
            'zone_id' => explode('____', $this->input->post('zone'))[0],
            'projectDetailedDescription' => $this->input->post('projectDetailedDescription'),
            'landTenure' => $this->input->post('landTenure'),
            'numberofUnits' => $this->input->post('numberofUnits'),
            'lrNo' => $this->input->post('lrNo'),
            'plotSize' => $this->input->post('plotSize'),
            'nearestRoad' => $this->input->post('nearestRoad'),
            'estate' => $this->input->post('estate'),
            'subcounty' => $this->input->post('subcounty'),
            'ward' => $this->input->post('ward'),
            'soilType' => $this->input->post('soilType'),

            'ownerNames' => $this->input->post('ownerNames'),
            'ownerEmail' => $this->input->post('ownerEmail'),
            'ownerPhoneNo' => $this->input->post('ownerPhoneNo'),
            'ownerPostalAddress' => $this->input->post('ownerPostalAddress'),
            'architectRegNo' => $this->input->post('architectRegNo'),
            'architectNames' => $this->input->post('architectNames'),
            'architectEmail' => $this->input->post('architectEmail'),
            'architectPhoneNo' => $this->input->post('architectPhoneNo'),
            'architectPostalAddress' => $this->input->post('architectPostalAddress'),
            'engineerRegNo' => $this->input->post('engineerRegNo'),
            'engineerNames' => $this->input->post('engineerNames'),
            'engineerEmail' => $this->input->post('engineerEmail'),
            'engineerPhoneNo' => $this->input->post('engineerPhoneNo'),
            'engineerPostalAddress' => $this->input->post('engineerPostalAddress'),
        );

        return $regdata;
    }
    public function subdivisionStep1()
    {
        $regdata = array(
            'applicationNumber' => $this->input->post('applicationNumber'),
            'zone' => explode('____', $this->input->post('zone'))[1],
            'zone_id' => explode('____', $this->input->post('zone'))[0],
            'ownerNames' => $this->input->post('ownerNames'),
            'ownerAddress' => $this->input->post('ownerAddress'),
            'applicantNames' => $this->input->post('applicantNames'),
            'applicantAddress' => $this->input->post('applicantAddress'),
            'applicantInterest' => $this->input->post('applicantInterest'),
            'lrNo' => $this->input->post('lrNo'),

        );

        return $regdata;
    }
    public function subdivisionStep2()
    {
        $regdata = array(
            'applicationNumber' => $this->input->post('applicationNumber'),
            'ownerNames' => $this->input->post('ownerNames'),
            'ownerAddress' => $this->input->post('ownerAddress'),
            'applicantNames' => $this->input->post('applicantNames'),
            'applicantAddress' => $this->input->post('applicantAddress'),
            'applicantInterest' => $this->input->post('applicantInterest'),
            'lrNo' => $this->input->post('lrNo'),

            'proposedSubdivision' => $this->input->post('proposedSubdivision'),
            'physicalAddress' => $this->input->post('physicalAddress'),
            'currentUse' => $this->input->post('currentUse'),
            'lastUseDate' => $this->input->post('lastUseDate'),
            'noofSubPlots' => $this->input->post('noofSubPlots'),
            'acreage' => $this->input->post('acreage'),
            'meansOfAccessContruction' => $this->input->post('meansOfAccessContruction'),

        );

        return $regdata;
    }
    public function getZoneList()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $client = new SoapClient($url);
        $serviceArguments = array(
               'API_Id' => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
        );
        $Response = $client->GetregDetails($serviceArguments);
    //  var_dump($Response->GetregDetailsResult->anyType);
     return @array_chunk($Response->GetregDetailsResult->anyType, 2); //array of values zoneDetails
    }
    public function getRtList()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $client = new SoapClient($url);
        $serviceArguments = array(
               'API_Id' => 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3',
        );
        $Response = $client->GetRtDetails($serviceArguments);

        return $regDetails = $Response->GetRtDetailsResult->anyType; //array of values RtDetails
    }
    // Used by most
    function getRegInvoiceDetails($apiID, $RefID, $client)
    {
        $serviceArgs = array(
            'API_Id' => $apiID,
            'RefId' => $RefID,
        );
        $result2 = $client->GetreginvoiceDetails($serviceArgs);
        //var_dump($result2);
        $rescode = $result2->GetreginvoiceDetailsResult->Response_Code;
        //var_dump($rescode);
        if (isset($rescode)) {
            $data = array(
                'RefId' => $RefID,
                // 'ownerNames' => $result2->GetreginvoiceDetailsResult->ownerNames,
                // 'ownerAddress' => $result2->GetreginvoiceDetailsResult->ownerAddress,
                // 'lrno' => $result2->GetreginvoiceDetailsResult->lrno,
                // 'physicalAddress' => $result2->GetreginvoiceDetailsResult->physicalAddress,
                // 'amount' => $result2->GetreginvoiceDetailsResult->amount,
            );
            return $data;
        } else {
            redirect('rlb/invalidRefid');
        }
    }
    // Used by building only
    function getBuildingInvoice($apiID, $RefID, $client)
    {
        $serviceArgs = array(
            'API_Id' => $apiID,
            'RefId' => $RefID,
        );
        $result2 = $client->GetbuildinginvoiceDetails($serviceArgs);
        $rescode = $result2->GetbuildinginvoiceDetailsResult->Response_Code;
        if (isset($rescode)) {
            $data = array(
             'RefId' => $RefID,
             'ownerNames' => $result2->GetbuildinginvoiceDetailsResult->ownerNames,
             'ownerAddress' => $result2->GetbuildinginvoiceDetailsResult->ownerAddress,
             'InvoiceNum' => $result2->GetbuildinginvoiceDetailsResult->InvoiceNum,
             'applicantNames' => $result2->GetbuildinginvoiceDetailsResult->applicantNames,
             'applicantAddress' => $result2->GetbuildinginvoiceDetailsResult->applicantAddress,
             'applicantInterest' => $result2->GetbuildinginvoiceDetailsResult->applicantInterest,
             'lrNo' => $result2->GetbuildinginvoiceDetailsResult->lrNo,
             'physicalAddress' => $result2->GetbuildinginvoiceDetailsResult->physicalAddress,
             'acreage' => $result2->GetbuildinginvoiceDetailsResult->acreage,
             'category_id' => $result2->GetbuildinginvoiceDetailsResult->category_id,
             'form_amount' => $result2->GetbuildinginvoiceDetailsResult->form_amount,
             'category' => $result2->GetbuildinginvoiceDetailsResult->category,
             'amount' => $result2->GetbuildinginvoiceDetailsResult->amount,
             'status' => $result2->GetbuildinginvoiceDetailsResult->status,
             'RefId' => $result2->GetbuildinginvoiceDetailsResult->RefId,
             'inspection_status' => $result2->GetbuildinginvoiceDetailsResult->inspection_status,
             'form_id' => $result2->GetbuildinginvoiceDetailsResult->form_id,
             'applicationNumber' => $result2->GetbuildinginvoiceDetailsResult->applicationNumber,
             'subdivisionInvolved' => $result2->GetbuildinginvoiceDetailsResult->subdivisionInvolved,
             'permissionApplied' => $result2->GetbuildinginvoiceDetailsResult->permissionApplied,
             'subdivisionApllicationNo' => $result2->GetbuildinginvoiceDetailsResult->subdivisionApllicationNo,
             'proposedDevelopment' => $result2->GetbuildinginvoiceDetailsResult->proposedDevelopment,
             'currentUse' => $result2->GetbuildinginvoiceDetailsResult->currentUse,
             'currentLandUse' => $result2->GetbuildinginvoiceDetailsResult->currentLandUse,
             'lastUseDate' => $result2->GetbuildinginvoiceDetailsResult->lastUseDate,
             'meansofAccessConstruction' => $result2->GetbuildinginvoiceDetailsResult->meansofAccessConstruction,
             'natureProposedDevelopment' => $result2->GetbuildinginvoiceDetailsResult->natureProposedDevelopment,
             'wallsDetails' => $result2->GetbuildinginvoiceDetailsResult->wallsDetails,
             'waterSupply' => $result2->GetbuildinginvoiceDetailsResult->waterSupply,
             'sewerageDisposal' => $result2->GetbuildinginvoiceDetailsResult->sewerageDisposal,
             'refuseDisposal' => $result2->GetbuildinginvoiceDetailsResult->refuseDisposal,
             'surfaceWaterDisposal' => $result2->GetbuildinginvoiceDetailsResult->surfaceWaterDisposal,
             'easementDetails' => $result2->GetbuildinginvoiceDetailsResult->easementDetails,
             'landAffected' => $result2->GetbuildinginvoiceDetailsResult->landAffected,
             'buildingsCover' => $result2->GetbuildinginvoiceDetailsResult->buildingsCover,
             'currentSiteCover' => $result2->GetbuildinginvoiceDetailsResult->currentSiteCover,
             'proposedSiteCover' => $result2->GetbuildinginvoiceDetailsResult->proposedSiteCover,
             'noofSubPlots' => $result2->GetbuildinginvoiceDetailsResult->noofSubPlots,
             'ownerEmail' => $result2->GetbuildinginvoiceDetailsResult->ownerEmail,
             'architectRegNo' => $result2->GetbuildinginvoiceDetailsResult->architectRegNo,
             'architectNames' => $result2->GetbuildinginvoiceDetailsResult->architectNames,
             'architectEmail' => $result2->GetbuildinginvoiceDetailsResult->architectEmail,
             'architectPhoneNo' => $result2->GetbuildinginvoiceDetailsResult->architectPhoneNo,
             'architectPostalAddress' => $result2->GetbuildinginvoiceDetailsResult->architectPostalAddress,
             'engineerRegNo' => $result2->GetbuildinginvoiceDetailsResult->engineerRegNo,
             'engineerNames' => $result2->GetbuildinginvoiceDetailsResult->engineerNames,
             'engineerEmail' => $result2->GetbuildinginvoiceDetailsResult->engineerEmail,
             'engineerPhoneNo' => $result2->GetbuildinginvoiceDetailsResult->engineerPhoneNo,
             'engineerPostalAddress' => $result2->GetbuildinginvoiceDetailsResult->engineerPostalAddress,
             'zone' => $result2->GetbuildinginvoiceDetailsResult->zone,
             'projectDetailedDescription' => $result2->GetbuildinginvoiceDetailsResult->projectDetailedDescription,
             'landTenure' => $result2->GetbuildinginvoiceDetailsResult->landTenure,
             'numberofUnits' => $result2->GetbuildinginvoiceDetailsResult->numberofUnits,
             'plotSize' => $result2->GetbuildinginvoiceDetailsResult->plotSize,
             'nearestRoad' => $result2->GetbuildinginvoiceDetailsResult->nearestRoad,
             'estate' => $result2->GetbuildinginvoiceDetailsResult->estate,
             'subcounty' => $result2->GetbuildinginvoiceDetailsResult->subcounty,
             'ward' => $result2->GetbuildinginvoiceDetailsResult->ward,
             'soilType' => $result2->GetbuildinginvoiceDetailsResult->soilType,
             'waterSupplier' => $result2->GetbuildinginvoiceDetailsResult->waterSupplier,
             'sewerageDisposalMethod' => $result2->GetbuildinginvoiceDetailsResult->sewerageDisposalMethod,
             'projectCost' => $result2->GetbuildinginvoiceDetailsResult->projectCost,
             'inspectionFees' => $result2->GetbuildinginvoiceDetailsResult->inspectionFees,
             'Foundation' => $result2->GetbuildinginvoiceDetailsResult->Foundation,
             'externalWalls' => $result2->GetbuildinginvoiceDetailsResult->externalWalls,
             'mortar' => $result2->GetbuildinginvoiceDetailsResult->mortar,
             'roofCover' => $result2->GetbuildinginvoiceDetailsResult->roofCover,
             'dampProofCourse' => $result2->GetbuildinginvoiceDetailsResult->dampProofCourse,
             'Form_Id' => $result2->GetbuildinginvoiceDetailsResult->Form_Id,
             'ownerPhoneNo' => $result2->GetbuildinginvoiceDetailsResult->ownerPhoneNo,
             'penalty' => $result2->GetbuildinginvoiceDetailsResult->penalty,
             'zoneid' => $result2->GetbuildinginvoiceDetailsResult->zoneid,
             'zone_no' => $result2->GetbuildinginvoiceDetailsResult->zone_no,
             'Rtid' => $result2->GetbuildinginvoiceDetailsResult->Rtid,
             'Rt' => $result2->GetbuildinginvoiceDetailsResult->Rt,
             'ownerPostalAddress' => $result2->GetbuildinginvoiceDetailsResult->ownerPostalAddress,
             'basementArea' => $result2->GetbuildinginvoiceDetailsResult->basementArea,
             'mezzaninefloorArea' => $result2->GetbuildinginvoiceDetailsResult->mezzaninefloorArea,
             'floorArea' => $result2->GetbuildinginvoiceDetailsResult->floorArea,
             'floorArea1' => $result2->GetbuildinginvoiceDetailsResult->floorArea1,
             'floorArea2' => $result2->GetbuildinginvoiceDetailsResult->floorArea2,
             'floorArea3' => $result2->GetbuildinginvoiceDetailsResult->floorArea3,
             'floorArea4' => $result2->GetbuildinginvoiceDetailsResult->floorArea4,
             'Others' => $result2->GetbuildinginvoiceDetailsResult->Others,
             'form_status' => $result2->GetbuildinginvoiceDetailsResult->form_status,

            );
            //var_dump($data);
            return $data;
        } else {
            redirect('rlb/invalidRefid');
        }
    }
    public function postChangeOfUsers()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
            'API_Id' => $apiID,
            'ownerNames'=> $this->input->post('ownerNames'),
            'ownerAddress'=> $this->input->post('ownerAddress'),
            'applicantNames'=> $this->input->post('applicantNames'),
            'applicantAddress'=> $this->input->post('applicantAddress'),
            'applicantInterest'=> $this->input->post('applicantInterest'),
            'lrNo'=> $this->input->post('lrNo'),
            'physicalAddress'=> $this->input->post('physicalAddress'),
            'acreage'=> $this->input->post('acreage'),
            'applicationNumber'=> $this->input->post('applicationNumber'),
            'zone'=> $this->input->post('zone'),
            'Form_Id' => 'RF01',
            'subdivisionInvolved'=> $this->input->post('subdivisionInvolved'),
            'permissionApplied'=> $this->input->post('permissionApplied'),
            'subdivisionApllicationNo'=> $this->input->post('subdivisionApllicationNo'),
            'proposedDevelopment'=> $this->input->post('proposedDevelopment'),
            'currentUse'=> $this->input->post('currentUse'),
            'lastUseDate'=> $this->input->post('lastUseDate'),
            'meansofAccessConstruction'=> $this->input->post('meansofAccessConstruction'),
            'natureProposedDevelopment'=> $this->input->post('natureProposedDevelopment'),
            'wallsDetailswallsDetails'=> $this->input->post('wallsDetails'),
            'waterSupply'=> $this->input->post('waterSupply'),
            'sewerageDisposal'=> $this->input->post('sewerageDisposal'),
            'refuseDisposal'=> $this->input->post('refuseDisposal'),
            'surfaceWaterDisposal'=> $this->input->post('surfaceWaterDisposal'),
            'easementDetails'=> $this->input->post('easementDetails'),
            'landAffected'=> $this->input->post('landAffected'),
            'buildingsCover'=> $this->input->post('buildingsCover'),
            'currentSiteCover'=> $this->input->post('currentSiteCover'),
            'proposedSiteCover'=> $this->input->post('proposedSiteCover'),
            'wallsDetails'=> $this->input->post('wallsDetails'),
            'zoneid'=> $this->input->post('zone_id'),
            'Rt' => 'category a',
            'Rtid' => '3',
        );

        try {
            $client = new SoapClient($url);
            $result = $client->Postchangeofuser($serviceArgs);
            $RefID = $result->PostchangeofuserResult->string[0];
            $name = $result->PostchangeofuserResult->string[1];
            $value = $result->PostchangeofuserResult->string[2];
            $InvoiceNo = $result->PostchangeofuserResult->string[3];
            //echo "ref:".$RefID." name:".$name." value:".$value." InvoiceNo:".$InvoiceNo;
        } catch (Exception $e) {
            redirect('rlb/Apierror');
        }
        if ($RefID != '400') {
            $this->getRegInvoiceDetails($apiID,$RefID, $client);
        } elseif ($RefID == '400') {
            redirect('rlb/error'); // error it already exists
        }
    }
    public function postAmalgamation()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
          'API_Id' => $apiID,
          'ownerNames'=> $this->input->post('ownerNames'),
          'ownerAddress'=> $this->input->post('ownerAddress'),
          'applicantNames'=> $this->input->post('applicantNames'),
          'applicantAddress'=> $this->input->post('applicantAddress'),
          'applicantInterest'=> $this->input->post('applicantInterest'),
          'lrNo'=> $this->input->post('lrNo'),
          'physicalAddress'=> $this->input->post('physicalAddress'),
          'acreage'=> $this->input->post('acreage'),
          'applicationNumber'=> $this->input->post('applicationNumber'),
          'zone'=> $this->input->post('zone'),
          'Form_Id' => 'RF07',
          'subdivisionInvolved'=> $this->input->post('subdivisionInvolved'),
          'permissionApplied'=> $this->input->post('permissionApplied'),
          'subdivisionApllicationNo'=> $this->input->post('subdivisionApllicationNo'),
          'proposedDevelopment'=> $this->input->post('proposedDevelopment'),
          'currentUse'=> $this->input->post('currentUse'),
          'lastUseDate'=> $this->input->post('lastUseDate'),
          'meansofAccessConstruction'=> $this->input->post('meansofAccessConstruction'),
          'natureProposedDevelopment'=> $this->input->post('natureProposedDevelopment'),
          'wallsDetails'=> $this->input->post('wallsDetails'),
          'waterSupply'=> $this->input->post('waterSupply'),
          'sewerageDisposal'=> $this->input->post('sewerageDisposal'),
          'refuseDisposal'=> $this->input->post('refuseDisposal'),
          'surfaceWaterDisposal'=> $this->input->post('surfaceWaterDisposal'),
          'easementDetails'=> $this->input->post('easementDetails'),
          'zoneid'=> $this->input->post('zone_id'),
          'Rt' => 'category a',
          'Rtid' => '3',
        );
        try {
            $client = new SoapClient($url);
            $result = $client->postamalgamation($serviceArgs);
            $RefID = $result->postamalgamationResult->string[0];
            $name = $result->postamalgamationResult->string[1];
            $value = $result->postamalgamationResult->string[2];
            $InvoiceNo = $result->postamalgamationResult->string[3];
        } catch (Exception $e) {
            redirect('rlb/Apierror');
        }
        if ($RefID != '400') {
            $this->getRegInvoiceDetails($apiID,$RefID, $client);
        } elseif ($RefID == '400') {
            redirect('rlb/error'); // error it already exists
        }
    }
    public function postBuildingStructures()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
          'API_Id' => $apiID,
          'ownerNames'=> $this->input->post('ownerNames'),
          'ownerEmail'=> $this->input->post('ownerEmail'),
          'ownerPhoneNo'=> $this->input->post('ownerPhoneNo'),
          'ownerPostalAddress'=> $this->input->post('ownerPostalAddress'),
          'architectRegNo'=> $this->input->post('architectRegNo'),
          'architectNames'=> $this->input->post('architectNames'),
          'architectEmail'=> $this->input->post('architectEmail'),
          'architectPhoneNo'=> $this->input->post('architectPhoneNo'),
          'architectPostalAddress'=> $this->input->post('architectPostalAddress'),
          'engineerRegNo'=> $this->input->post('engineerRegNo'),
          'engineerNames'=> $this->input->post('engineerNames'),
          'Form_Id' => 'RF08',
          'engineerEmail'=> $this->input->post('engineerEmail'),
          'engineerPhoneNo'=> $this->input->post('engineerPhoneNo'),
          'engineerPostalAddress'=> $this->input->post('engineerPostalAddress'),
          'currentLandUse'=> $this->input->post('currentLandUse'),
          'zone'=> $this->input->post('zone'),
          'projectDetailedDescription'=> $this->input->post('projectDetailedDescription'),
          'landTenure'=> $this->input->post('landTenure'),
          'numberofUnits'=> $this->input->post('numberofUnits'),
          'lrNo'=> $this->input->post('lrNo'),
          'plotSize'=> $this->input->post('plotSize'),
          'nearestRoad'=> $this->input->post('nearestRoad'),
          'estate'=> $this->input->post('estate'),
          'subcounty'=> $this->input->post('subcounty'),
          'ward'=> $this->input->post('ward'),
          'soilType'=> $this->input->post('soilType'),
          'waterSupplier'=> $this->input->post('waterSupplier'),
          'sewerageDisposalMethod'=> $this->input->post('sewerageDisposalMethod'),
          'projectCost'=> $this->input->post('projectCost'),
          'inspectionFees'=> $this->input->post('inspectionFees'),
          'Foundation'=> $this->input->post('Foundation'),
          'externalWalls'=> $this->input->post('externalWalls'),
          'mortar'=> $this->input->post('mortar'),
          'roofCover'=> $this->input->post('roofCover'),
          'dampProofCourse'=> $this->input->post('dampProofCourse'),
          'zoneid'=> $this->input->post('zone_id'),
          'Rt' => 'category a',
          'Rtid' => '3',

        );
        try {
            $client = new SoapClient($url);
            $result = $client->postbuildingstructures($serviceArgs);
            $RefID = $result->postbuildingstructuresResult->string[0];
            $name = $result->postbuildingstructuresResult->string[1];
            $value = $result->postbuildingstructuresResult->string[2];
            $InvoiceNo = $result->postbuildingstructuresResult->string[3];
        } catch (Exception $e) {
            redirect('rlb/Apierror');
        }
        if ($RefID != '400') {
            //GET INVOICE DETAILS
            $this->getBuildingInvoice($apiID, $RefID, $client);

        } elseif ($RefID == '400') {
            redirect('rlb/error'); // error it already exists
        }
    }
    public function postSubdivisionLargeSchemes()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
          'API_Id' => $apiID,
          'ownerNames'=> $this->input->post('ownerNames'),
          'ownerAddress'=> $this->input->post('ownerAddress'),
          'applicantNames'=> $this->input->post('applicantNames'),
          'applicantAddress'=> $this->input->post('applicantAddress'),
          'applicantInterest'=> $this->input->post('applicantInterest'),
          'lrNo'=> $this->input->post('lrNo'),
          'physicalAddress'=> $this->input->post('physicalAddress'),
          'acreage'=> $this->input->post('acreage'),
          'applicationNumber'=> $this->input->post('applicationNumber'),
          'zone'=> $this->input->post('zone_id'),
          'Form_Id' => 'RF05',
          'subdivisionInvolved'=> $this->input->post('subdivisionInvolved'),
          'permissionApplied'=> $this->input->post('permissionApplied'),
          'subdivisionApllicationNo'=> $this->input->post('subdivisionApllicationNo'),
          'proposedDevelopment'=> $this->input->post('proposedDevelopment'),
          'currentUse'=> $this->input->post('currentUse'),
          'lastUseDate'=> $this->input->post('lastUseDate'),
          'meansofAccessConstruction'=> $this->input->post('meansofAccessConstruction'),
          'natureProposedDevelopment'=> $this->input->post('natureProposedDevelopment'),
          'wallsDetails'=> $this->input->post('wallsDetails'),
          'waterSupply'=> $this->input->post('waterSupply'),
          'sewerageDisposal'=> $this->input->post('sewerageDisposal'),
          'refuseDisposal'=> $this->input->post('refuseDisposal'),
          'surfaceWaterDisposal'=> $this->input->post('surfaceWaterDisposal'),
          'easementDetails'=> $this->input->post('easementDetails'),
          'zoneid'=> $this->input->post('zone_id'),
          'Rt' => 'category a',
          'Rtid' => '3',
        );
        try {
            $client = new SoapClient($url);
            $result = $client->postsubdivisonlargeschemes($serviceArgs);
            $RefID = $result->postsubdivisonlargeschemesResult->string[0];
            $name = $result->postsubdivisonlargeschemesResult->string[1];
            $value = $result->postsubdivisonlargeschemesResult->string[2];
            $InvoiceNo = $result->postsubdivisonlargeschemesResult->string[3];
        } catch (Exception $e) {
            redirect('rlb/Apierror');
        }
        if ($RefID != '400') {
            $this->getRegInvoiceDetails($apiID,$RefID, $client);
        } elseif ($RefID == '400') {
            redirect('rlb/error'); // error it already exists
        }
    }

    public function postLandSubdivison()
    {
        $url = 'http://54.218.79.241/mainsector.asmx?wsdl';
        $apiID = 'bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3';

        $serviceArgs = array(
          'API_Id' => $apiID,
          'ownerNames'=> $this->input->post('ownerNames'),
          'ownerAddress'=> $this->input->post('ownerAddress'),
          'applicantNames'=> $this->input->post('applicantNames'),
          'applicantAddress'=> $this->input->post('applicantAddress'),
          'applicantInterest'=> $this->input->post('applicantInterest'),
          'lrNo'=> $this->input->post('lrNo'),
          'physicalAddress'=> $this->input->post('physicalAddress'),
          'acreage'=> $this->input->post('acreage'),
          'applicationNumber'=> $this->input->post('applicationNumber'),
          'zone'=> $this->input->post('zone'),
          'Form_Id' => 'RF04',
          'subdivisionInvolved'=> $this->input->post('subdivisionInvolved'),
          'permissionApplied'=> $this->input->post('permissionApplied'),
          'subdivisionApllicationNo'=> $this->input->post('subdivisionApllicationNo'),
          'proposedDevelopment'=> $this->input->post('proposedDevelopment'),
          'currentUse'=> $this->input->post('currentUse'),
          'lastUseDate'=> $this->input->post('lastUseDate'),
          'meansofAccessConstruction'=> $this->input->post('meansofAccessConstruction'),
          'natureProposedDevelopment'=> $this->input->post('natureProposedDevelopment'),
          'wallsDetails'=> $this->input->post('wallsDetails'),
          'waterSupply'=> $this->input->post('waterSupply'),
          'sewerageDisposal'=> $this->input->post('sewerageDisposal'),
          'refuseDisposal'=> $this->input->post('refuseDisposal'),
          'surfaceWaterDisposal'=> $this->input->post('surfaceWaterDisposal'),
          'easementDetails'=> $this->input->post('easementDetails'),
          'zoneid'=> $this->input->post('zone_id'),
          'Rt' => 'category a',
          'Rtid' => '3',
        );
        try {
            $client = new SoapClient($url);
            $result = $client->PostLandsubdivision($serviceArgs);
            $RefID = $result->PostLandsubdivisionResult->string[0];
            $name = $result->PostLandsubdivisionResult->string[1];
            $value = $result->PostLandsubdivisionResult->string[2];
            $InvoiceNo = $result->PostLandsubdivisionResult->string[3];
        } catch (Exception $e) {
            redirect('rlb/Apierror');
        }
        if ($RefID != '400') {
            $this->getRegInvoiceDetails($apiID,$RefID, $client);
        } elseif ($RefID == '400') {
            redirect('rlb/error'); // error it already exists
        }
    }
}
