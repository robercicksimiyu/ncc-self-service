<?php

class Parking_model extends CI_Model{

	#var $bizID;

  function prepareDailyParkingInit(){
    $username = USER_NAME;//"97763838"; //72386895"; 
    $key = JP_KEY;//"3637137f-9952-4eba-9e33-17a507a2bbb2";

    $url = MAIN_URL;
    //$liveurl = "https://192.168.7.10:1501/agencyservices?WSDL";

    $timestamp = date("Y-m-d H:i:s");
    $ck = $username.$timestamp.$key;
    $pass = sha1(utf8_encode($ck));

    $params = array(
     "userName" => $username,
     "timestamp" => $timestamp,
     "pass" => $pass
     );

    $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
    $result = $client->PreparePaymentDailyParkingInitDataPCKNCC($params);
    $ResultCode = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->Result->ResultCode;
    $ResultText = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->Result->ResultText;

    if ($ResultCode==0&&$ResultText=='OK') {
      $zones = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->ParkingZones->{"NCC.ZoneCodes"};
      $VehicleType = $result->PreparePaymentDailyParkingInitDataPCKNCCResult->VehicleType->{"NCC.VehicleType"};
      $parking=array('zones'=>$zones,'VehicleType'=>$VehicleType);
      return $parking;
    }

    
  }

    function prepareDailyParkingPayment(){

      $username = USER_NAME;//"97763838"; //72386895"; 
      $key = JP_KEY;//"3637137f-9952-4eba-9e33-17a507a2bbb2";
      $url = MAIN_URL;

       $reg = $this->input->post('regno');// "KBX111X";
       $regnum = str_replace(' ', '', $reg);
       $registrationNo = strtoupper($regnum);
       $vehicleTypeCode = $this->input->post('vehicleType');// "10";
       $zoneCode = $this->input->post('ZoneCode');//"10";
       $jpwMobileNo = $this->session->userdata('jpwnumber'); //"254724213205";
       $agentRef = strtoupper(substr(md5(uniqid()),25));
       $zone = $this->input->post('ZoneDescription');
       $type = $this->input->post('VehicleDescription'); 

       $ck = $username.$jpwMobileNo.$vehicleTypeCode.$zoneCode.$agentRef.$registrationNo.$key;
       $pass = sha1(utf8_encode($ck));

       $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         "zoneCode"=> $zoneCode,
         "jpwMobileNo"=> $jpwMobileNo,
         "pass"=> $pass
               
       );


       $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
       $result = $client->PreparePaymentDailyWalletPCKNCC($serviceArguments);
       $ResultCode = $result->PreparePaymentDailyWalletPCKNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentDailyWalletPCKNCCResult->Result->ResultText;

       #if($ResultCode==0&&$ResultText=='OK'){
        $transactionid = $result->PreparePaymentDailyWalletPCKNCCResult->TransactionID;
        $name = $result->PreparePaymentDailyWalletPCKNCCResult->NameJPW;
        $fee = $result->PreparePaymentDailyWalletPCKNCCResult->PCKFee;
        $regno = $result->PreparePaymentDailyWalletPCKNCCResult->RegistrationNo;

        $details = array(
          'zone'=>$zone,
          'type'=>$type,
          'transid'=>$transactionid,
          'name'=>$name,
          'fee'=>$fee,
          'regno'=>$regno,
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $details;
       #}

       
    }

    function completeDailyParking(){
      
      $username = USER_NAME; //72386895"; 
      $key = JP_KEY; //"3637137f-9952-4eba-9e33-17a507a2bbb2";
      $url = MAIN_URL;// "https://192.168.1.196:1602/agencyservices?wsdl";
      
       $tranid = $this->input->post('transid');// "6017";// $_POST['tranid'];
       $amount = $this->input->post('amount');// "300";
       $zone = $this->input->post('zone');
       $type = $this->input->post('type');
       $carreg = $this->input->post('reg');
       $channelRef = strtoupper(substr(md5(uniqid()),25)); 
       $currency = "KES";
       $jpPIN = $this->input->post('jp_pin');
       $customer = $this->session->userdata('name');
              
       $ck = $username . $tranid . $key;
       $pass = sha1(utf8_encode($ck ));

       $serviceArguments = array(
         "userName"=>$username,
         "transactionId"=>$tranid,
         "jpPIN"=>$jpPIN, 
         "amount"=>$amount,      
         "currency"=> $currency,
         "channelRef"=> $channelRef,
         "pass"=> $pass
         );

        $client = new SoapClient($url);
        $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
        $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
        $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
        if($ResultCode==0&&$ResultText=='OK'){
        $paid = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Paid;
        $receiptno = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;
        #set in db
        try {
              $data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'regno'=>$carreg,'category'=>$type,'zone'=>$zone,'username' =>$this->session->userdata('jpwnumber'),'cashiername' =>$this->session->userdata('name'));

                $query = "insert into dailyparking (`receiptno`,`issuedate`,`from`,`amount`,`regno`,`category`,`zone`,`username`,`cashiername`)";
                $query.= " values ('".implode("','",$data)."')";
                $this->db->query($query);
              
            } catch (Exception $e) {}
        #end set in db
        try {

          $ph=$this->session->userdata('jpwnumber');//substr($phone,-9);
            //$gsm="254".$ph;

            $amnt= number_format($amount, 2, '.', ',');
            $msg="Receipt Number $receiptno.Your payment of KES $amnt for Daily Parking of $carreg at $zone has been received by Nairobi City County.Powered by: JamboPay";
            $X_HOST ="http1.uk.oxygen8.com";
            $X_URL = "/WebTribe";
            $X_PORT ="8080";
            $X_USERNAME = "WebTribe";
            $X_PASSWORD = "web@12";
                   $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
                   $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
                   $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
                   $s_POST_DATA .= "&MSISDN=$ph"; // Phone
                   $s_POST_DATA .= "&Content=$msg";
                   $s_POST_DATA .= "&DataType=0"; //Data Type
                   $s_POST_DATA .= "&Premium=1"; //Premium
                   $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
                   $s_POST_DATA .= "&Multipart=1";
                   $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
                   $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
                   $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
                   $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
                   $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
                   $s_Request .="\r\n".$s_POST_DATA;

                   
                   $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
                   if(fputs ($fp, $s_Request)){

                   };
                   ///while (!feof($fp)) { $s_GatewayResponse .= fgets ($fp, 128); }
                   fclose ($fp);
        } catch (Exception $e) {
          
        }

        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText,
          'paid'=>$paid,
          'receiptno'=>$receiptno,
          'customer'=>$this->session->userdata('name')
          );
        return $data;
      }
      else {
        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $data;
      }
    }

    function prepareSeasonalParkingInit(){
    $username = USER_NAME;//"97763838"; //72386895"; 
    $key = JP_KEY;//"3637137f-9952-4eba-9e33-17a507a2bbb2";
    $url = MAIN_URL;
    //$liveurl = "https://192.168.7.10:1501/agencyservices?WSDL";
    $timestamp = date("Y-m-d H:i:s");
    $ck = $username.$timestamp.$key;
    $pass = sha1(utf8_encode($ck));

    $params = array(
     "userName" => $username,
     "timestamp" => $timestamp,
     "pass" => $pass
     );

    $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
    $result = $client->PreparePaymentSeasonalParkingInitDataPCKNCC($params);
    $ResultCode = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->Result->ResultCode;
    $ResultText = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->Result->ResultText;

    if ($ResultCode==0&&$ResultText=='OK') {
      $duration = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->ParkingDuration->{"NCC.ParkingDuration"};
      $VehicleType = $result->PreparePaymentSeasonalParkingInitDataPCKNCCResult->VehicleType->{"NCC.VehicleType"};
      $parking=array('duration'=>$duration,'VehicleType'=>$VehicleType);
      return $parking;
    }

    
  }

    function prepareSeasonalParkingPayment(){

      $username = USER_NAME;//"97763838"; //72386895"; 
      $key = JP_KEY;//"3637137f-9952-4eba-9e33-17a507a2bbb2";
      $url = MAIN_URL;

      $durationCode = $this->input->post('duration');
       $reg = $this->input->post('regno');// "KBX111X";
       $regnum = str_replace(' ', '', $reg);
       $registrationNo = strtoupper($regnum);
       $vehicleTypeCode = $this->input->post('type');
       #$zoneCode = "10";
       $jpwMobileNo = $this->session->userdata('jpwnumber'); //"254712633277";
       $agentRef = strtoupper(substr(md5(uniqid()),25)); 

       $ck = $username.$jpwMobileNo.$vehicleTypeCode.$durationCode.$agentRef.$registrationNo.$key;
       $pass = sha1(utf8_encode($ck));

       $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         #"zoneCode"=> $zoneCode,
         "durationCode"=>$durationCode,
         "jpwMobileNo"=> $jpwMobileNo,
         "pass"=> $pass
               
       );

       $client = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
       $result = $client->PreparePaymentSeasonalWalletPCKNCC($serviceArguments);
       $ResultCode = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultCode;
       $ResultText = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultText;

       #if($ResultCode==0&&$ResultText=='OK'){
        $transactionid = $result->PreparePaymentSeasonalWalletPCKNCCResult->TransactionID;
        $name = $result->PreparePaymentSeasonalWalletPCKNCCResult->NameJPW;
        $fee = $result->PreparePaymentSeasonalWalletPCKNCCResult->PCKFee;
        $regno = $result->PreparePaymentSeasonalWalletPCKNCCResult->RegistrationNo;

        $details = array(
          'transid'=>$transactionid,
          'name'=>$name,
          'fee'=>$fee,
          'regno'=>$regno,
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $details;
       #}

       
    }

    function completeSeasonalParking(){
      $username = USER_NAME; //72386895"; 
      $key = JP_KEY; //"3637137f-9952-4eba-9e33-17a507a2bbb2";
      $url = MAIN_URL;// "https://192.168.1.196:1602/agencyservices?wsdl";
      
       $tranid = $this->input->post('transid');// "6017";// $_POST['tranid'];
       $amount = $this->input->post('amount');// "300";
       $channelRef = strtoupper(substr(md5(uniqid()),25)); 
       $currency = "KES";
       $jpPIN = $this->input->post('jp_pin');
              
       $ck = $username . $tranid . $key;
       $pass = sha1(utf8_encode($ck ));

       $serviceArguments = array(
         "userName"=>$username,
         "transactionId"=>$tranid,
         "jpPIN"=>$jpPIN, 
         "amount"=>$amount,      
         "currency"=> $currency,
         "channelRef"=> $channelRef,
         "pass"=> $pass
         );

        $client = new SoapClient($url);
        $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
        $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
        $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
        if($ResultCode==0&&$ResultText=='OK'){
        $paid = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Paid;
        $receiptno = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;

        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText,
          'paid'=>$paid,
          'receiptno'=>$receiptno
          );
        return $data;
      }
      else {
        $data = array(
          'rescode'=>$ResultCode,
          'restext'=>$ResultText
          );
        return $data;
      }
    }


  function getVehiclesDetails(){
    $username = $this->session->userdata('jpwnumber');
    $query="select * from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $result1=$this->db->query($query);
    $html="";
    if($result1->num_rows()>0){
      $html.="<thead><tr><th>Registration Number</th><th>Duration</th><th>Vehicle Type</th>";
            $html.="<th>Amount</th></tr></thead><tbody>";

      foreach ($result1->result() as $value) {
        $html.="<tr><td>";
              $html.="<div class='text-primary'><strong>".$value->registrationNo."</strong></div></td>";
              $html.="<td>".$value->durationDesc."</td><td>".$value->vehicleDesc."</td><td>".$value->amount."</td></tr>";
      }

      $html.="</tbody>";
    }else{
      $html.=" <div class='well'>No records added </div> ";
    }
    return print_r($html);
  }

  function getVehiclesSum(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select sum(amount) as sumamount from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $html="";
    $result2=$this->db->query($query2);
    if ($result2->result()>0) {
      $value=$result2->row();
      $html=$value->sumamount;

      return print_r($html);
    }else {
      return print_r($html);
    }
    
  }

  function countVehicles(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select count(registrationNo) as num from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $html="";
    $result2=$this->db->query($query2);
    if ($result2->result()>0) {
      $value=$result2->row();
      $html=$value->num;

      return print_r($html);
    }else {
      return print_r($html);
    }
  }

  function addVehicle(){
        $durationCode=$this->input->post('DurationCode');
        $DurationDesc=$this->input->post('DurationDesc');
        $vehicleTypeCode=$this->input->post('VehicleTypeCode');
        $VehicleTypeDesc=$this->input->post('VehicleTypeDesc');
        $registrationNo= str_replace(' ', '',strtoupper($this->input->post('registrationNo')));
        $time=date('Y-m-d h:i:s',time());
        $user=$this->session->userdata('jpwnumber');
        $status='0';
        $complete='0';
        $username = USER_NAME;// $this->session->userdata('jpwnumber');
      $key = JP_KEY;// $this->session->userdata('key');
      $agentRef = strtoupper(substr(md5(uniqid()),25)); 
      $ck = $username.$user.$vehicleTypeCode.$durationCode.$agentRef.$registrationNo.$key;
      $pass = sha1(utf8_encode($ck));
        $serviceArguments = array(
         "userName"=>$username,
         "agentRef"=> $agentRef,
         "registrationNo"=>$registrationNo,
         "vehicleTypeCode"=> (int)$vehicleTypeCode,
         #"zoneCode"=> $zoneCode,
         "durationCode"=>$durationCode,
         "jpwMobileNo"=> $user,
         "pass"=> $pass
               
       );

     try {
       $client = new SoapClient(MAIN_URL, array('cache_wsdl' => WSDL_CACHE_NONE));
     } catch (Exception $e) {
        redirect('/sbp/not_found');
     }

     $result = $client->PreparePaymentSeasonalWalletPCKNCC($serviceArguments);
     $ResultCode = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultCode;
     $ResultText = $result->PreparePaymentSeasonalWalletPCKNCCResult->Result->ResultText;
     if($ResultCode==0&&$ResultText=="OK"){
      $pckfee = $result->PreparePaymentSeasonalWalletPCKNCCResult->PCKFee;
      $TransactionID = $result->PreparePaymentSeasonalWalletPCKNCCResult->TransactionID;

      $query="insert into epaymentsSeasonalParkingtemp (durationDesc,durationCode,registrationNo,vehicleDesc,vehicleCode,transid,amount,`time`,user,status,complete) values ('$DurationDesc','$durationCode','$registrationNo','$VehicleTypeDesc','$vehicleTypeCode','$TransactionID','$pckfee','$time','$user','$status','$complete')";

        $this->db->query($query);
     }else{
     return print_r($ResultText); # var_dump($result);
     }

        
  }

  function resetVehicle(){
    $username = $this->session->userdata('jpwnumber');
    $query="delete from epaymentsSeasonalParkingtemp where user='$username'";
    $this->db->query($query);
  }

  function confirmSPParking(){
    $username = $this->session->userdata('jpwnumber');
    $query2="select sum(amount) as sumamount from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $html="";
    $result2=$this->db->query($query2);

    $query="select count(registrationNo) as num from epaymentsSeasonalParkingtemp where user='$username' and complete='0'";
    $result=$this->db->query($query);

    return array('res1'=>$result,'res2'=>$result2);
  }

  function completeSPPayment(){
      $username = $this->session->userdata('jpwnumber');
      $query="select * from epaymentsSeasonalParkingtemp where user='$username'";
      $result=$this->db->query($query);
      $pin=$this->input->post('jppin');
      $name=$this->session->userdata('name');
      $t_unique = strtoupper(substr(md5(uniqid()),25));
      $this->session->set_userdata('t_unique',$t_unique);
      $totalamount = $this->input->post('amount');

      foreach ($result->result() as $value) {

         $ck = USER_NAME. $value->transid .JP_KEY;
         $pass = sha1(utf8_encode($ck ));
         $channelRef = strtoupper(substr(md5(uniqid()),25)); 
         $serviceArguments = array(
           "userName"=>USER_NAME,
           "jpPIN"=>$pin,//"1111",
           "transactionId"=>$value->transid,
           "amount"=>$value->amount,      
           "currency"=> "KES",
           "channelRef"=> $channelRef,
           "pass"=> $pass
           );
          try {
            $client = new SoapClient(MAIN_URL);
          } catch (Exception $e) {
            redirect('/sbp/not_found');
          }
          
          $result = $client->CompletePaymentSeasonalOrDailyWalletPCKNCC($serviceArguments);
          $ResultCode = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultCode;
          $ResultText = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->Result->ResultText;
          $ReceiptNo = $result->CompletePaymentSeasonalOrDailyWalletPCKNCCResult->ReceiptNo;
          if ($ResultCode==0&&$ResultText=="OK") {
            $query="insert into seasonalparking (`receiptno`,`issuedate`,`from`,`regno`,`category`,`duration`,`amount`,`expirydate`,`username`,`cashiername`,`t_unique`) values('$ReceiptNo','$value->time','$name','$value->registrationNo','$value->vehicleDesc','$value->durationDesc','$value->amount','$value->time','$value->user','$name','$t_unique')";
            $this->db->query($query);
            

            $query="delete from epaymentsSeasonalParkingtemp where user='$username' and id='$value->id'";
            $this->db->query($query);
          }



      }
             if ($ResultCode==0&&$ResultText=="OK") {
          try {
          /*for sending confirmation sms*/
              $phone=$this->session->userdata('jpwnumber');// "254726981598";
              $ph=substr($phone,-9);
              $gsm="254".$ph;
                
                $msg="Receipt Number $ReceiptNo.Your payment of KES $totalamount for Seasonal Parking has been received by Nairobi City County.Powered by: JamboPay";
                $X_HOST ="http1.uk.oxygen8.com";
                $X_URL = "/WebTribe";
                $X_PORT ="8080";
                $X_USERNAME = "WebTribe";
                $X_PASSWORD = "web@12";
                $s_POST_DATA = "Channel=KENYA.SAFARICOM"; // Channel
                $s_POST_DATA .= "&Shortcode=700273"; // Shortcode
                $s_POST_DATA .= "&SourceReference=3456"; // Source Reference
                $s_POST_DATA .= "&MSISDN=$gsm"; // Phone
                $s_POST_DATA .= "&Content=$msg";
                $s_POST_DATA .= "&DataType=0"; //Data Type
                $s_POST_DATA .= "&Premium=1"; //Premium
                $s_POST_DATA .= "&CampaignID=105409"; // CampaignID
                $s_POST_DATA .= "&Multipart=1";
                $s_Request = "POST ".$X_URL." HTTP/1.0\r\n";
                $s_Request .="Host: ".$X_HOST.":".$X_PORT."\r\n";
                $s_Request .="Authorization: Basic ".base64_encode($X_USERNAME.":".$X_PASSWORD)."\r\n";
                $s_Request .="Content-Type: application/x-www-form-urlencoded\r\n";
                $s_Request .="Content-Length: ".strlen($s_POST_DATA)."\r\n";
                $s_Request .="\r\n".$s_POST_DATA;

                
                $fp = fsockopen ($X_HOST, $X_PORT, $errno, $errstr, 30) or die("Error!!!");
                if(fputs ($fp, $s_Request)){
                    
                };
                fclose ($fp);
        } catch (Exception $e) {
          
        }
      }
      $query="delete from epaymentsSeasonalParkingtemp where user='$username'";
      //$this->db->query($query);
      #$cash=$this->input->post('cash');
      #$customer=$this->input->post('customer');
      $amount=$this->input->post('amount');

      return array('customer'=>$this->session->userdata('name'),'result'=>$result,'restext'=>$ResultText,'rescode'=>$ResultCode);
    
         
  }

  #print seasonal receipt
    function printSeasonal(){

    $this->load->library('zend');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/seasonalParking.pdf';
        $pdf = Zend_Pdf::load($fileName);

    
    $page=$pdf->pages[0];
    $template = $pdf->pages[0];

        $t_unique=$this->session->userdata('t_unique');
        $query="select count(t_unique) as count from seasonalparking where t_unique='$t_unique'";
        $result=$this->db->query($query);
        $count=$result->row();
        $pages=ceil($count/6);
        $query="select * from seasonalparking where t_unique='$t_unique'";
        $result=$this->db->query($query);
        $records=$result->result();
        $pageindex=0;
        $i=0;
        
      foreach ($result->result() as  $index=>$records) {
        //$page=$pdf->pages[0];
        if($index %6==0&&$index!=0){
          $pageindex++;
          ${'page'.$pageindex} = new Zend_Pdf_Page($template);
        $pdf->pages[] = ${'page'.$pageindex};
        $page=$pdf->pages[$pageindex];
        $i=0;
        }
        if($i==0){

          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration); 
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 320, 543); 
        $page->drawText($cashier, 103, 415);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 513);
            $page->drawText($paid_by, 105, 501); 
            $page->drawText($registration_number, 178, 489);
            $page->drawText($vehicle_category, 163, 475); 
            $page->drawText($duration, 115, 461);
            $page->drawText($amount, 110, 446);
            $page->drawText($expiry_date, 128, 432);
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 45,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();
        }

        if($i==1){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 543); 
        $page->drawText($cashier, 483, 415);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 513);
            $page->drawText($paid_by, 485, 501); 
            $page->drawText($registration_number, 558, 489);
            $page->drawText($vehicle_category, 546, 475); 
            $page->drawText($duration, 495, 461);
            $page->drawText($amount, 490, 446);
            $page->drawText($expiry_date, 508, 432);
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 45,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==2){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 320, 350); 
        $page->drawText($cashier, 103, 223);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 321);
            $page->drawText($paid_by, 105, 309); 
            $page->drawText($registration_number, 178, 297);
            $page->drawText($vehicle_category, 163, 283); 
            $page->drawText($duration, 115, 269);
            $page->drawText($amount, 110, 254);
            $page->drawText($expiry_date, 128, 240);

            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 237,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==3){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 350); 
        $page->drawText($cashier, 483, 223);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 321);
            $page->drawText($paid_by, 485, 309); 
            $page->drawText($registration_number, 558, 297);
            $page->drawText($vehicle_category, 546, 283); 
            $page->drawText($duration, 495, 269);
            $page->drawText($amount, 490, 254);
            $page->drawText($expiry_date, 508, 240);

            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 237,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();

        }

        if($i==4){

          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

          $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 323, 155); 
        $page->drawText($cashier, 103, 29);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 146, 127);
            $page->drawText($paid_by, 105, 115); 
            $page->drawText($registration_number='KBH456Y', 178, 103);
            $page->drawText($vehicle_category, 163, 89); 
            $page->drawText($duration, 115, 75);
            $page->drawText($amount, 110, 60);
            $page->drawText($expiry_date, 128, 46);

            
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 430,
                'leftOffset' =>360
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();
        
        }

        if($i==5){
          $receiptno=$records->receiptno;
        $issue_date=substr($records->issuedate,0,10); 
        $registration_number=$records->regno;
        $paid_by=$records->from;
        $vehicle_category=$records->category;
        $duration=str_replace('_', ' ', $records->duration);
        $amount=$records->amount; 
        $expiry_date=substr($records->expirydate,0,10); 
        $cashier=$records->cashiername;

          $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 7);
        $page->drawText($issue_date, 700, 157); 
        $page->drawText($cashier, 483, 29);
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10);
            $page->drawText($receiptno, 526, 127);
            $page->drawText($paid_by, 485, 115); 
            $page->drawText($registration_number, 558, 103);
            $page->drawText($vehicle_category, 546, 89); 
            $page->drawText($duration, 495, 75);
            $page->drawText($amount, 490, 60);
            $page->drawText($expiry_date, 508, 46);

            
            $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 30,'factor'=>1.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 430,
                'leftOffset' =>740
                );
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf,$pageindex)->draw();
        
        }
        $i++; 
      }
    unset($pdf->pages[$i]);

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=SeasonalTicket.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
    
  }

    function printDreceipt(){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/dailyParking.pdf';
        $username=$this->session->userdata('jpwnumber');
        $receiptno=str_replace('-','/',$this->uri->segment(3));
        $query="select * from dailyparking where username='$username' and receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();

        $date=substr($result->issuedate, 0,10);
        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $zone=$result->zone;
        $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);

       
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B); 
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

  function printCreceipt(){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/parkingCharges.pdf';
        $username=$this->session->userdata('auser');
        $receiptno=str_replace('-','/',$this->uri->segment(3));
        $query="select * from dailyparking where username='$username' and receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();

        $date=substr($result->issuedate, 0,10);
        $receiptno=$result->receiptno;
        $regno=$result->regno;
        $vehicle_category=$result->category;
        $zone=$result->zone;
    $amount=$result->amount;

        $amount =number_format(str_replace( ',', '', $amount),2);
        $servedby=strtoupper($result->cashiername);

       
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14, B); 
        $page->drawText($receiptno, 260, 300);
        $page->drawText($date, 632, 411);
        $page->drawText($regno, 260, 275);
        $page->drawText($vehicle_category, 260, 250);
        $page->drawText($zone, 260, 225);
        $page->drawText($amount, 260, 200);
        $page->drawText($servedby, 165, 135);/*new*/
        $barcodeOptions = array('orientation'=>-90,'text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 205,
            'leftOffset' =>680
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
  }

}