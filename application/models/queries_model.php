<?php

class Queries_model extends CI_Model{

	function allqueries(){
		$sql = "SELECT * FROM `queries` ORDER BY `time` desc";
	    $result = $this->db->query($sql);
	    return $result;
	}

	function receiptQueryDetails(){
		$token = $this->session->userdata('token');

		$receiptno = preg_replace('/\s+/', '',$this->input->post('receiptno'));
        $key0 = "ReceiptNumber";
        $value0 = $receiptno;

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?[0].Key=$key0&[0].Value=$value0";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Authorization: bearer '.$token,
		    'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
		));

		$result = curl_exec( $ch );
		$res = json_decode($result);//var_dump($res); die();
		if (!$res){
		return $data = array("rescode"=>"0");
		}else{
		return $data = array(
			"rescode"=>"1",
			"amount"=>$res['0']->Amount,
			"stream"=>$res['0']->StreamDescription,
			"receiptno"=>$res['0']->ReceiptNumber,
			"date"=>$res['0']->TransactionDate,
			"transactiondetail"=>$res['0']->TransactionDetail,
			"transactionid"=>$res['0']->TransactionID
			);
		}
	}

	function printReceiptQueryDetails($transid){
		$token = $this->session->userdata('token');
		$this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');
        $fileName =APPPATH.'assets/back/receipts/ReceiptDetails.pdf';
        
        $transid = $this->uri->segment(3);

        $key0 = "TransactionID";
        $value0 = $transid;

        $url = "http://192.168.6.10/JamboPayServices/api/payments/gettransactions?[0].Key=$key0&[0].Value=$value0";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Authorization: bearer '.$token,
		    'app_key: a2868c59-67e2-e411-8285-b8ee657eaebc'
		));

		$result = curl_exec( $ch );
		$res = json_decode($result);
		

        $amount=$res['0']->Amount;
		$stream=$res['0']->StreamDescription;
		$receiptno=$res['0']->ReceiptNumber;
		$date=$res['0']->TransactionDate;
		$transactiondetail=$res['0']->TransactionDetail;
		$transactionid=$res['0']->TransactionID;

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $page->drawText(number_format($amount,2), 420, 528);
        $page->drawText($receiptno, 200, 528);
        $page->drawText($date, 200, 480); 
        $page->drawText($transactiondetail, 200, 430);
        $page->drawText($stream, 450, 480); 
        $page->drawText($transactionid, 200, 380);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 600,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=ReceiptDetails.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
	}

}