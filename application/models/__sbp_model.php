<?php

class Sbp_model extends CI_Model{

	#var $bizID;

    function prepare(){
        $jpwMobileNo = "254712633277";
        $username = "97763838";
        $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = "1294345";
        $agentRef = strtoupper(substr(md5(uniqid()),25));  
        $ck = $username .$jpwMobileNo. $agentRef . $businessId . $key;
        $pass = sha1(utf8_encode($ck ));



        $params = array(
            "userName"=>$username,
            "businessId"=>$businessId,            
            "calendarYear"=>"2014",        
            "jpwMobileNo"=> $jpwMobileNo,
            "agentRef"=> $agentRef,
            "pass"=> $pass                  
            );

        $client = new SoapClient("https://41.212.9.57:1501/agencyservices?WSDL");
        $result = $client->PreparePaymentWalletSBPNCC($params);

        return $result;
    }

    function preparePayment(){

      $username = "97763838";
      $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
      $businessId = $this->input->post('biz_id');
      $no = $this->input->post('cust_phone');
      $num = substr($no,-9);
      $number="254".$num;
      $agentRef = strtoupper(substr(md5(uniqid()),25));
      $ck = $username. $number. $agentRef . $businessId . $key;
      $pass = sha1(utf8_encode($ck ));

      $serviceArguments = array(
        "userName"=>$username,
        "agentRef"=>$agentRef,
        "businessId"=>$businessId,
        "calendarYear"=>"2014",
        "jpwMobileNo"=>$number,
        "pass"=> $pass

        );

      try {
        $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
    } catch (Exception $e) {
        redirect('/sbp/not_found');
    }
    $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
    $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
    $resultcode=$result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
        #$bill_status=$result->PreparePaymentSBPNCCResult->BillStatus;
    if ($resultcode==0 && $resulttext=="OK" ){
        $trans_id=$result->PreparePaymentWalletSBPNCCResult->TransactionID;
        $bizna_id=$result->PreparePaymentWalletSBPNCCResult->BusinessId;
        $bizna_name=$result->PreparePaymentWalletSBPNCCResult->BusinessName;
        $physical_add=$result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
        $bill_status=$result->PreparePaymentWalletSBPNCCResult->BillStatus;
        $sbp_fee=$result->PreparePaymentWalletSBPNCCResult->SBPFee;
        $penalty=$result->PreparePaymentWalletSBPNCCResult->Penalty;
        $annualamount=$result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
        $billno=$result->PreparePaymentWalletSBPNCCResult->BillNumber;
        #$phone="254712633277";
        #if($bill_status=="NEW"){
        $data = array(
            'trans_id'=>$trans_id,
            'biz_name'=>$bizna_name,
            'physical_address'=>$physical_add,
            'bill_stat'=>$bill_status,
            'biz_id'=>$bizna_id,
            'pass'=>$pass,
            'mobile_no'=>$number,
            'penalty'=>$penalty,
            'annualamount'=>$annualamount,
            'billno'=>$billno,
            'sbpfee'=>$sbp_fee
            );

        $this->session->set_userdata($data);

        return $data; } 
    }

    function complete(){

        $jpwMobileNo =$this->session->userdata('mobile_no');
        $username = "97763838";
        $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
        $businessId = $this->session->userdata('biz_id');
        $amount = $this->session->userdata('sbpfee');

        $tranid =$this->session->userdata('trans_id');
        $channelRef = "45875";  
        $ck = $username . $tranid. $key;
        $pass = sha1(utf8_encode($ck ));
        $pin = $this->input->post('jp_pin');

        $data_complete = array(
            "userName"=>$username,
            "transactionId"=>$tranid,
            "jpPIN"=>$pin,
            "CalenderYear"=>"2014",
            "amount"=>$amount,
            "currency"=> "KES",
            "channelRef"=> "45875",
            "pass"=> $pass

            );

        try {
            $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
        } catch (Exception $e) {
            redirect('/sbp/not_found');
        }
        $result = $client->CompletePaymentWalletSBPNCC($data_complete);
        $resulttext = $result->CompletePaymentWalletSBPNCCResult->Result->ResultText;
        $resultcode = $result->CompletePaymentWalletSBPNCCResult->Result->ResultCode;
        $ReceiptNo = $result->CompletePaymentWalletSBPNCCResult->ReceiptNo;
        if ($resultcode==0 && $resulttext=="OK" ){

                 #send mail
            $to      = 'samuel@deveint.com';
            $subject = 'Nairobi City County - Payment Received';
            $message = 'Your trasaction of KES'." ".number_format($amount, 2, '.', ',')." ".' for Single Business Permit ID '." ". $businessId." ".'  Was Successfully completed, your Receipt Number is'." ". $ReceiptNo." ".'Please download the permit from the web portal!';
                    // $message = 'hello';
                    // $message = 'hello';
            $headers = 'From: test@webtribe.co.ke' . "\r\n" .
                        #'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);
                  #mail

                  $phone=$this->session->userdata('mobile_no');#"254712633277";
                  $ph=substr($phone,-9);
                  $gsm="254".$ph;
                  $postUrl = "http://api2.infobip.com/api/sendsms/xml";
                  $user="JumaMusyoka";
                  $pass="IV3fuK6w";
                  $sender="JAMBOPAY";
                  $msg="Receipt Number  $ReceiptNo .Your payment of KES $amount for Business ID $businessId has been received by Nairobi City County.Powered by: JamboPay";
                  
                  $xmlString =
                  "<SMS>
                  <authentification>
                      <username>$user</username>
                      <password>$pass</password>
                  </authentification>
                  <message>
                      <sender>$sender</sender>
                      <text>$msg</text>
                  </message>
                  <recipients>
                      <gsm>$gsm</gsm>
                  </recipients>
              </SMS>";
                  // previously formatted XML data becomes value of "XML" POST variable
              $fields = "XML=" . urlencode($xmlString);
                  // in this example, POST request was made using PHP's CURL
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $postUrl);
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                  // response of the POST request
              $response = curl_exec($ch);
              curl_close($ch);






              return $message="Transaction Has Successfully been completed";
            }else return $resulttext;//="Some Errors Occured during the process please try again!";


            return $result;

        }

        function jp_checkout(){
          $username = "97763838";
          $currency = "KES";
		$channel = "Agency"; //ask munyaga
		$channelRef = "3456";
		$calendarYear = "2014";
		$tranid =432; //$this->session->userdata['trans_id'];
		$jp_key = "jp"; //confirm with munyaga

		$ck = $username . $tranid . $jp_key;
		$pass = sha1(utf8_encode($ck ));
		#var_dump($pass);
			#$JP_TRANID = $this->session->userdata['trans_id'];  //confusion
            $JP_MERCHANT_ORDERID = 432; //confusion
            $JP_ITEM_NAME = "SBP";
            $JP_AMOUNT = 95000;
            $JP_CURRENCY = 'KES';
            $JP_TIMESTAMP = date('Y-m-d H:i:s');
            $JP_PASSWORD = $pass;
            $JP_CHANNEL = $channel;

//$sharedkey, IS ONLY SHARED BETWEEN THE MERCHANT AND JAMBOPAY. THE KEY SHOULD BE SECRET ********************

//Make sure you get the key from JamboPay Support team
            $sharedkey = 'BF025795-6927-464C-B0FC-93F108B153E9';

            $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;

            $serviceArguments = array(
            	"userName"=>$username,
            	"transactionId"=>432,#$this->session->userdata['trans_id'],
            	"amount"=>95000,//$this->session->userdata['sbpfee'],
            	"calendarYear"=> $calendarYear,
            	"pass"=> $pass,
            	"currency"=> $currency,
            	"channel"=> $channel,
            	"channelRef"=> $channelRef

            	);

//**************** VERIFY *************************
            if (md5(utf8_encode($str)) == $JP_PASSWORD) {

            	try {
                    $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
                } catch (Exception $e) {
                    redirect('/sbp/not_found');
                }
                $result = $client->CompletePaymentSBPNCC($serviceArguments);
                $resulttext = $result->CompletePaymentSBPNCCResult->Result->ResultText;
                $resultcode = $result->CompletePaymentSBPNCCResult->Result->ResultCode;
                if ($resultcode==0 && $resulttext=="OK" ){
                  return $message="Transaction Has Successfully been completed";
              }else {
                  return $message="Some Errors Occured during the process please try again!";
              }


          }
      }

      function check_balance(){

            //set POST variables
        $url = 'http://10.0.0.1:1500/jpweb/ewallet/expresstatement.aspx';

        $phone=$this->input->post('phone');
        $phone=substr($phone, -9);
        $pin=$this->input->post('pin');
        $jp_business="test@webtribe.co.ke";
        $fields = array(
            'jp_customer_telephone' => urlencode($phone),
            'jp_customer_pin' => urlencode($pin),
            'jp_business' => urlencode($jp_business)
            );

            //url-ify the data for the POST
        $fields_string='';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

            //open connection
        $ch = curl_init($url);
        $path = base_url().'/cacert.pem';
            //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '\cacert.pem');
            //execute post
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
                // this would be your first hint that something went wrong
            print('Couldn\'t send request: ' . curl_error($ch));
        } else {
                // check the HTTP status code of the request
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus != 200) {
                print('Request failed: HTTP status code: ' . $resultStatus);
            }else {
                $xml=simplexml_load_string($result);
                    //echo $xml->RESPONSE_TEXT;
                echo $xml->BALANCE;
                    //echo $result;
            }
        }

            //close connection
        curl_close($ch);
            //return var_dump($result);

    }

    function mini_statement(){

            //set POST variables
        $url = 'http://10.0.0.1:1500/jpweb/ewallet/expresstatement.aspx';

        $phone=$this->input->post('phone');
        $phone=substr($phone, -9);
        $pin=$this->input->post('pin');
        $jp_business="test@webtribe.co.ke";
        $fields = array(
            'jp_customer_telephone' => urlencode($phone),
            'jp_customer_pin' => urlencode($pin),
            'jp_business' => urlencode($jp_business)
            );

            //url-ify the data for the POST
        $fields_string='';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

            //open connection
        $ch = curl_init($url);
        $path = base_url().'/cacert.pem';
            //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '/cacert.pem');
            //execute post
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
                // this would be your first hint that something went wrong
            print('Couldn\'t send request: ' . curl_error($ch));
        } else {
                // check the HTTP status code of the request
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus != 200) {
                print('Request failed: HTTP status code: ' . $resultStatus);
            }else {
                $xml=simplexml_load_string($result);
                    //echo $xml->RESPONSE_TEXT;
                if($xml->RESPONSE_CODE!=3){
                    $statement=$xml->STATEMENT;
                    $data = explode("|",$statement);
                    $value =  array($data);
                    $display = '';
                    foreach ($data as $key => $value) {
                        echo '<tr>';
                        $data2=explode(",", $value);
                        $value2=array($data2);
                        foreach ($data2 as $key => $value2) {
                            echo "<td>".str_replace('JAMBOPAY', 'Nairobi Pay',$value2)."</td>";
                                //echo "<td>".($xml->STATEMENT)."</td>";
                        }
                        echo '</tr>';

                    }
                }else{

                    echo "0";
                }
                    //var_dump($xml);
            }
        }

            //close connection
        curl_close($ch);
            //return var_dump($result);

    }

    function checkout(){
        $url = 'http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx';
        $xml=simplexml_load_file($url);
        $statement=$xml->url;
        #echo $statement;
        header("location: $statement");
    }


    function submit_checkout(){
        	//set POST variables
        $url = "http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx";

        $jp_item_type="cart";
        $jp_item_name="Single Business Permit";
        $order_id="1";#$this->session->userdata('biz_id');
        $jp_business="test@webtribe.co.ke";
        $jp_amount_1="0";#$this->session->userdata('sbpfee');
        $jp_amount_2="0";
        $jp_amount_5="0";
             $jp_payee="samuel@deveint.com";
             $jp_shipping="Name";#$this->session->userdata('biz_name');
             $jp_rurl="http://208.109.88.195:1700/web/index.php/sbp/success";
             $jp_furl="http://208.109.88.195:1700/web/index.php/sbp/return_result";
             $jp_curl="http://208.109.88.195:1700/web/index.php/sbp/return_result";
            // $pin=$this->input->post('pin');
            // $jp_business="test@webtribe.co.ke";
             $fields = array(
                'jp_item_type' => urlencode($jp_item_type),
                'jp_item_name' => urlencode($jp_item_name),
                'order_id' => urlencode($order_id),
                'jp_business' => urlencode($jp_business),
                'jp_amount_1' => urlencode($jp_amount_1),
                'jp_amount_2' => urlencode($jp_amount_2),
                'jp_amount_5' => urlencode($jp_amount_5),
                'jp_payee' => urlencode($jp_payee),
                'jp_shipping' => urlencode($jp_shipping),
                'jp_rurl' => urlencode($jp_rurl),
                'jp_furl' => urlencode($jp_furl),
                'jp_curl' => urlencode($jp_curl)

                );

            //url-ify the data for the POST
             $fields_string='';
             foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
             rtrim($fields_string, '&');

            //open connection
             $ch = curl_init($url);
             $path = base_url().'/cacert.pem';
            //set the url, number of POST vars, POST data
             curl_setopt($ch,CURLOPT_URL, $url);
             curl_setopt($ch,CURLOPT_POST, count($fields));
             curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
             curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '/cacert.pem');
            //execute post
             $result = curl_exec($ch);

             if (curl_errno($ch)) {
                // this would be your first hint that something went wrong
                print('Couldn\'t send request: ' . curl_error($ch));
            } else {
                // check the HTTP status code of the request
                $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($resultStatus != 200) {
                    print('Request failed: HTTP status code: ' . $resultStatus);
                }else {
                    $xml=simplexml_load_string($result);
                    //echo $xml->RESPONSE_TEXT;
                    	  $statement=$xml->urlp;
                          return $statement;
                    //$data = explode("|",$statement);
                    //$value =  array($data);
                    // $display = '';
                    // foreach ($data as $key => $value) {


                    //         #echo '<tr><td>'.$value.'</td></tr>';

                    // }
                    	#redirect('sbp/h/$statement');
                    #header("location: $statement");
                   #echo $statement; # anchor($statement,'Click Here');
                    #return $xml->url('$xml->url');
                }
            }

            //close connection
            curl_close($ch);
            #return $result;

           #redirect('jp_redirect');
        }

        function print_sbp(){

            $username = "97763838";
            $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
            $businessId = $this->input->post('biz_idd');
            $agentRef = strtoupper(substr(md5(uniqid()),25));
            $ck = $username . $agentRef . $businessId . $key;
            $pass = sha1(utf8_encode($ck ));



            $serviceArguments = array(
                "userName"=>$username,
                "agentRef"=>$agentRef,
                "businessId"=>$businessId,
                "calendarYear"=>"2014",
                "custMobileNo"=>"254724972416",
                "pass"=> $pass

                );

            try {
                $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
            $result = $client->GetIssuedSBPNCC($serviceArguments);
            $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
            $year=$result->GetIssuedSBPNCCResult->CalenderYear;
            $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
            $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
            $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
            $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
            $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
            $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
            $pobox=$result->GetIssuedSBPNCCResult->POBox;
            $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
            $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
            $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
            $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
            $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
            $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        #$bill=$result->GetIssuedSBPNCCResult->BillStatus;


            $sbpdata = array(
                'sbpnumber'=>$sbpnumber,
                'year'=>$year,
                'bizname'=>$bizname,
                'pinno'=>$pinno,
                'biztype'=>$biztype,
                'activitycode'=>$activitycode,
                'paidfee'=>$paidfee,
                'inwords'=>$inwords,
                'pobox'=>$pobox,
                'plotnumber'=>$plotnumber,
                'street'=>$street,
                'issuedate'=>$issuedate,
                'bizactivityname'=>$bizactivityname,
                'bizzid'=>$bizzid,
            #'bill'=>$bill,
                'receiptno'=>$receiptno
                );

        // $usersession=array(
        //  'username' => $username,
        //  'businessId' => $businessId,
        //  'password' => $pass,
        //  'trans_id'=>$trans_id
        //  );
        #$this->session->set_userdata($data);

            return $sbpdata;
        }

        function print_sbp_preview($businessId){

            $username = "97763838";
            $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
            $businessId = $this->uri->segment(3);
            $agentRef = strtoupper(substr(md5(uniqid()),25));
            $ck = $username . $agentRef . $businessId . $key;
            $pass = sha1(utf8_encode($ck ));



            $serviceArguments = array(
                "userName"=>$username,
                "agentRef"=>$agentRef,
                "businessId"=>$businessId,
                "calendarYear"=>"2014",
                "custMobileNo"=>"254724972416",
                "pass"=> $pass

                );

            try {
                $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
            $result = $client->GetIssuedSBPNCC($serviceArguments);
            $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
            $year=$result->GetIssuedSBPNCCResult->CalenderYear;
            $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
            $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
            $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
            $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
            $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
            $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
            $pobox=$result->GetIssuedSBPNCCResult->POBox;
            $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
            $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
            $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
            $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
            $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
            $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        #$bill=$result->GetIssuedSBPNCCResult->REFReceiptNumber;
        #$expirydate=$result->GetIssuedSBPNCCResult->;


            $sbpdata = array(
                'sbpnumber'=>$sbpnumber,
                'year'=>$year,
                'bizname'=>$bizname,
                'pinno'=>$pinno,
                'biztype'=>$biztype,
                'activitycode'=>$activitycode,
                'paidfee'=>$paidfee,
                'inwords'=>$inwords,
                'pobox'=>$pobox,
                'plotnumber'=>$plotnumber,
                'street'=>$street,
                'issuedate'=>$issuedate,
                'bizactivityname'=>$bizactivityname,
                'bizzid'=>$bizzid,
                'receiptno'=>$receiptno
                );

        // $usersession=array(
        //  'username' => $username,
        //  'businessId' => $businessId,
        //  'password' => $pass,
        //  'trans_id'=>$trans_id
        //  );
        #$this->session->set_userdata($data);

            return $sbpdata;
        }

        function register_user(){
            $url = 'http://10.0.0.1:1500/jpweb/ewallet/expressregister.aspx';

            $first_name=$this->input->post('first_name');
            $last_name=$this->input->post('last_name');
            $email=$this->input->post('email');
            $express_phone=$this->input->post('express_phone');
            $express_phone=substr($express_phone, -9);
            $express_pin=$this->input->post('express_pin');
            $national_id=$this->input->post('national_id');
            $jp_business=$this->input->post('jp_business');
            $fields = array(
                                    'first_name' => urlencode($first_name),
                                    'last_name' => urlencode($last_name),
                                    'email' => urlencode($email),
                                    'express_phone' => urlencode($express_phone),
                                    'express_pin' => urlencode($express_pin),
                                    'national_id' => urlencode($national_id),
                                    'jp_business' => urlencode($jp_business)
                            );

            //url-ify the data for the POST
            $fields_string='';
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');

            //open connection
            $ch = curl_init($url);
            $path = base_url().'/cacert.pem';
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '\cacert.pem');
            //execute post
            $result = curl_exec($ch);

            if (curl_errno($ch)) {
                // this would be your first hint that something went wrong
                print('Couldn\'t send request: ' . curl_error($ch));
            } else {
                // check the HTTP status code of the request
                $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($resultStatus != 200) {
                    print('Request failed: HTTP status code: ' . $resultStatus);
                }else {
                    $xml=simplexml_load_string($result);
                    $response=array('1' =>$xml->RESPONSE_CODE,'2'=>$xml->RESPONSE_TEXT);
                    if($xml->RESPONSE_TEXT=="OK"){
                        print($xml->RESPONSE_CODE);
                    }else{
                        print($xml->RESPONSE_TEXT);
                    }
                    
                    //print_r($response);
                }
            }

            //close connection
            curl_close($ch);

            //return var_dump($result);
	}

       function submit_register(){
        $data['title'] = 'Submit Registration';
       // $data['content'] = 'en/register';
        $data['xml'] =$this->sbp_model->register_user();
        //$this->load->view('include/back_template', $data);
    } 



 

       

       

        function top(){
            $url = 'http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx';

         // $url = "http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx";

        $jp_item_type="cart";
        $jp_item_name="Single Business Permit";
        $order_id="1";#$this->session->userdata('biz_id');
        $jp_business="test@webtribe.co.ke";
        $jp_amount_1="10";#$this->session->userdata('sbpfee');
        $jp_amount_2="0";
        $jp_amount_5="0";
         $jp_payee="samuel@deveint.com";
         $jp_shipping="Name";#$this->session->userdata('biz_name');
         $jp_rurl="http://208.109.88.195:1700/web/index.php/sbp/success";
         $jp_furl="http://208.109.88.195:1700/web/index.php/admin/indexx";
         $jp_curl="http://208.109.88.195:1700/web/index.php/sbp/return_result";

         /*<form method="post" action="http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx" >
        <input type="hidden" name="jp_item_type" value="kahara"/>
  <input type="hidden" name="jp_item_name" value="Single Business Permit"/>
  <input type="hidden" name="order_id" value="100"/>
  <input type="hidden" name="jp_business" value="demo@webtribe.co.ke"/>
  <input type="hidden" name="jp_amount_1" value="1"/>
  <input type="hidden" name="jp_amount_2" value="0"/>
  <input type="hidden" name="jp_amount_5" value="0"/>
  <input type="hidden" name="jp_payee" value="samuel@deveint.com"/>
  <input type="hidden" name="jp_shipping" value="kahara"/><br>
  <input type="hidden" name="jp_rurl" value="http://208.109.88.195:1700/web/index.php/sbp/success"/>
  <input type="hidden" name="jp_furl" value="http://208.109.88.195:1700/web/index.php/sbp/return_result"/>
  <input type="hidden" name="jp_curl" value="http://208.109.88.195:1700/web/index.php/sbp/return_result"/>
  <input id="" id="top" type="submit" src="" value="Top up Now" style="padding-left: 100px;padding-right: 100px;padding-top: 20px;" />
    </form>*/

          $fields = array(
                'jp_item_type' => urlencode($jp_item_type),
                'jp_item_name' => urlencode($jp_item_name),
                'order_id' => urlencode($order_id),
                'jp_business' => urlencode($jp_business),
                'jp_amount_1' => urlencode($jp_amount_1),
                'jp_amount_2' => urlencode($jp_amount_2),
                'jp_amount_2' => urlencode($jp_amount_2),
                'jp_customer_telephone' =>"254724213205",
                'jp_customer_pin' =>"1234",
                'jp_payee' => urlencode($jp_payee),
                'jp_shipping' => urlencode($jp_shipping),
                'jp_rurl' => urlencode($jp_rurl),
                'jp_furl' => urlencode($jp_furl),
                'jp_curl' => urlencode($jp_curl)

                );

            //url-ify the data for the POST
        $fields_string='';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

            //open connection
        $ch = curl_init($url);
        $path = base_url().'\cacert.pem';
            //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '\cacert.pem');
            //execute post
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
                // this would be your first hint that something went wrong
            print('Couldn\'t send request: ' . curl_error($ch));
        } else {
                // check the HTTP status code of the request
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus != 200) {
                print('Request failed: HTTP status code: ' . $resultStatus);
            }else {
                $xml=simplexml_load_string($result);
                    //echo $xml->RESPONSE_TEXT;
                $url=$xml->url ;
                header("Location:$url");
                    //echo $result;
            }
        }

            //close connection
        curl_close($ch);
            //return var_dump($result);

        }


function prepsaveinitial(){
          $username="97763838";
          $jp_key= '3637137f-9952-4eba-9e33-17a507a2bbb2';
          $timestamp = date("Y-m-d H:i:s"); 
          $ck = $username .$timestamp . $jp_key;
          $pass = sha1(utf8_encode($ck ));

            $serviceArguments = array(
              "userName"=>$username,
              "timestamp"=>$timestamp,
              "pass"=> $pass
                   
            );
            try {
                 $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL", array('cache_wsdl' => WSDL_CACHE_NONE));
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }


            $result = $client->PrepareSaveBusinessInitDataSBPNCC($serviceArguments);
            $bizactivity=$result->PrepareSaveBusinessInitDataSBPNCCResult->BusinessActivity->{'BusinessPermit.BusinessActivity'};
            $relsize=$result->PrepareSaveBusinessInitDataSBPNCCResult->BusinessRelativeSize->{'BusinessPermit.BusinessRelativeSize'};
            $idtype=$result->PrepareSaveBusinessInitDataSBPNCCResult->IDTypesList->{'BusinessPermit.IDType'};
            $zones=$result->PrepareSaveBusinessInitDataSBPNCCResult->Zones->{'BusinessPermit.Zone'};
            $wards=$result->PrepareSaveBusinessInitDataSBPNCCResult->Wards->{'BusinessPermit.Ward'};

            $data=array(
                'bizactivity' =>$bizactivity,
                'relsize' =>$relsize,
                'idtype' =>$idtype,
                'zones' =>$zones,
                'wards' =>$wards
                );

            return $data;
            //return $relsize;
        }

        function register_sbp(){

          $username="97763838";
          $jp_key= '3637137f-9952-4eba-9e33-17a507a2bbb2';
          $activityCode =$this->input->post('activity_code'); 
          $agentRef =  strtoupper(substr(md5(uniqid()),25));
          $custPhone = "254".substr($this->input->post('tel1'),-9);

          $ck = $username.$custPhone.$agentRef.$activityCode. $jp_key;
          $pass = sha1(utf8_encode($ck ));

            $serviceArguments = array(
              "userName"=>$username,
              "agentRef"=>$agentRef,
              "activityCode"=>$activityCode,
              "jpwMobileNo"=>$custPhone,
              "pass"=> $pass
                    
            );
            try {
                $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
            } catch (Exception $e) {
                redirect('/sbp/not_found');
            }
            $result = $client->PrepareSaveBusinessWalletSBPNCC($serviceArguments);
            $resulttext = $result->PrepareSaveBusinessWalletSBPNCCResult->Result->ResultText;
            $resultcode = $result->PrepareSaveBusinessWalletSBPNCCResult->Result->ResultCode;

            //var_dump($result); die();
            if($resulttext=="OK"&&$resultcode==0){

              $tran_id = $result->PrepareSaveBusinessWalletSBPNCCResult->TransactionID;
              //$agentRef=strtoupper(substr(md5(uniqid()),25));
              $ck = $username .$tran_id . $jp_key;
              $pass = sha1(utf8_encode($ck ));
              $transactionId=$tran_id;
              //$amount=number_format($amount, 2,'.','');
              $amount=number_format($this->input->post('sbp_fee'),2,'.','');
              $currency="KES";
              $channelRef=strtoupper(substr(md5(uniqid()),25));
              $BusinessName=$this->input->post('biz_name');
              $IDDocumentType=$this->input->post('doc_type');
              $IDDocumentNumber=$this->input->post('doc_no');
              $PINNumber=$this->input->post('pin_no');
              $VATNumber=$this->input->post('vat_no');
              $POBox=$this->input->post('box');
              $PostalCode=$this->input->post('postal_code');
              $AnnualSBPAmount=$this->input->post('sbp_fee');
              $Town=$this->input->post('town');
              $Telephone1="254".substr($this->input->post('tel1'),-9);
              $Telephone2="254".substr($this->input->post('tel2'),-9);
              $FaxNumber=$this->input->post('fax');
              $email=$this->input->post('email');
              $PhysicalAddress=$this->input->post('address');
              $PlotNumber=$this->input->post('plot_number');
              $Building=$this->input->post('building');
              $Floor=$this->input->post('floor');
              $RoomStallNo=$this->input->post('stall_room_no');
              $ContactPersonName=$this->input->post('full_names');
              $ContactPersonPOBox=$this->input->post('owner_box');
              $ContactPersonPostalCode=$this->input->post('owner_postal_code');
              $ContactPersonTelephone1=$this->input->post('owner_telephone');
              $ContactPersonTelephone2=$this->input->post('owner_telephone2');
              $ContactPersonFaxNumber=$this->input->post('owner_fax');
              $BusinessClassificationDetails=$this->input->post('activity_dec');
              $PremisesArea=number_format($this->input->post('area'),2);
              $OtherBusinessClassificationDetails=$this->input->post('other_details');
              $NumberOfEmployees=$this->input->post('emp_no');
              $ActivityCode=$this->input->post('activity_code');
              $ZoneCode=$this->input->post('zone');
              $WardCode=$this->input->post('ward');
              $RelativeSize=$this->input->post('size');
              $pin=$this->input->post('pin');
              $ContactPersonTown='';
              $BusinessRegistrationNumber='';
              $ContactPersonDesignation='';



                $serviceArguments = array(
                  "userName"=>$username,
                  "transactionId"=>$transactionId,
                  "amount"=> $amount,
                  "currency"=>$currency,
                  "channelRef"=>$channelRef,
                  "pass"=> $pass,
                  "BusinessName"=>$BusinessName,
                  "BusinessRegistrationNumber"=>$BusinessRegistrationNumber,
                  "PINNumber"=> $PINNumber,
                  "VATNumber"=>$VATNumber,
                  "IDDocumentType"=>$IDDocumentType,
                  "IDDocumentNumber"=> $IDDocumentNumber,
                  "ZoneCode"=>$ZoneCode,
                  "WardCode"=>$WardCode,
                  "ActivityCode"=> $ActivityCode,
                  "BusinessClassificationDetails"=>$BusinessClassificationDetails,
                  "OtherBusinessClassificationDetails"=>$OtherBusinessClassificationDetails,
                  "PremisesArea"=> $PremisesArea,
                  "RelativeSize"=>$RelativeSize,
                  "NumberOfEmployees"=>$NumberOfEmployees,
                  "AnnualSBPAmount"=> $AnnualSBPAmount,
                  "POBox"=>$POBox,
                  "PostalCode"=>$PostalCode,
                  "Town"=> $Town,
                  "Telephone1"=>$Telephone1,
                  "Telephone2"=>$Telephone2,
                  "FaxNumber"=> $FaxNumber,
                  "email"=>$email,
                  "PhysicalAddress"=>$PhysicalAddress,
                  "PlotNumber"=> $PlotNumber,
                  "Building"=>$Building,
                  "Floor"=>$Floor,
                  "RoomStallNo"=> $RoomStallNo,
                  "ContactPersonName"=>$ContactPersonName,
                  "ContactPersonDesignation"=>$ContactPersonDesignation,
                  "ContactPersonPOBox"=> $ContactPersonPOBox,
                  "ContactPersonPostalCode"=>$ContactPersonPostalCode,
                  "ContactPersonTown"=>$ContactPersonTown,
                  "ContactPersonTelephone1"=> $ContactPersonTelephone1,
                  "ContactPersonTelephone2"=>$ContactPersonTelephone2,
                  "pin"=>$pin,
                  "ContactPersonFaxNumber"=>$ContactPersonFaxNumber
                                        
                );
                try {
                    $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
                } catch (Exception $e) {
                    redirect('/sbp/not_found');
                }
                
                $result = $client->CompleteSaveBusinessWalletSBPNCC($serviceArguments);
                $resulttext = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultText;
                $resultcode = $result->CompleteSaveBusinessWalletSBPNCCResult->Result->ResultCode;

                if($resultcode==0&&$resulttext=="OK"){
                    $BusinessId = $result->CompleteSaveBusinessWalletSBPNCCResult->BusinessID;
                    $Commission = $result->CompleteSaveBusinessWalletSBPNCCResult->Commission;
                    $ReceiptNo = $result->CompleteSaveBusinessWalletSBPNCCResult->ReceiptNo;
                    $Paid = $result->CompleteSaveBusinessWalletSBPNCCResult->Paid;
                    $trans=$result->CompleteSaveBusinessWalletSBPNCCResult->TransactionID;


                    $businessId = $BusinessId;
                    $agentRef = strtoupper(substr(md5(uniqid()),25));
                    $custPhone = "254".substr($this->input->post('tel1'),-9);
                    $ck = $username .$custPhone.$agentRef . $businessId . $jp_key;
                    $pass = sha1(utf8_encode($ck ));

                    $serviceArguments = array(
                        "userName"=>$username,
                        "agentRef"=>$agentRef,
                        "businessId"=>$businessId,
                        "calendarYear"=>"2014",
                        "jpwMobileNo"=>$custPhone,
                        "pass"=> $pass

                        );

                    try {

                        $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
                        
                    } catch (Exception $e) {
                        
                        redirect('/sbp/not_found');
                    }
                    
                    $result = $client->PreparePaymentWalletSBPNCC($serviceArguments);
                    $resulttext = $result->PreparePaymentWalletSBPNCCResult->Result->ResultText;
                    $resultcode=$result->PreparePaymentWalletSBPNCCResult->Result->ResultCode;
                    if ($resultcode==0 && $resulttext=="OK" ){
                        $trans_id2=$result->PreparePaymentWalletSBPNCCResult->TransactionID;
                        $biz_id=$result->PreparePaymentWalletSBPNCCResult->BusinessId;
                        $bill_no=$result->PreparePaymentWalletSBPNCCResult->BillNumber;
                        $biz_name=$result->PreparePaymentWalletSBPNCCResult->BusinessName;
                        $physical_add=$result->PreparePaymentWalletSBPNCCResult->PhysicalAddress;
                        $bill_status=$result->PreparePaymentWalletSBPNCCResult->BillStatus;
                        $sbp_fee=$result->PreparePaymentWalletSBPNCCResult->SBPFee;
                        $annualamount=$result->PreparePaymentWalletSBPNCCResult->AnnualAmount;
                        $penalty=$result->PreparePaymentWalletSBPNCCResult->Penalty;

                            $data = array(
                                'biz_id'=>$biz_id,
                                'bill_no'=>$bill_no,
                                'trans_id'=>$trans_id2,
                                'commission' =>$Commission,
                                'receiptno' =>$ReceiptNo,
                                'biz_name'=>$biz_name,
                                'physical_address'=>$physical_add,
                                'bill_stat'=>$bill_status,
                                'sbpfee'=>number_format($sbp_fee,2,'.',''),
                                'result'=>$result,
                                'annualamount'=>number_format($annualamount,2),
                                'penalty'=>$penalty,
                                'message'=>''
                            );

                            $data=json_encode($data);

                        return print_r($data);
                        
                    }
                }else return var_dump($result);
            }
            

        }

        function pay_sbp(){

           $amount=$this->input->post('amount');
           $tranid=$this->input->post('trans_id');
           $businessId=$this->input->post('businessId');

            $username = "97763838";
            $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
            $businessId = $this->input->post('biz_idd');
            $agentRef = strtoupper(substr(md5(uniqid()),25));
            $ck = $username . $agentRef . $businessId . $key;
            $pass = sha1(utf8_encode($ck));

            $serviceArguments = array(
                "userName"=>$username,
                "agentRef"=>$agentRef,
                "businessId"=>$businessId,
                "calendarYear"=>"2014",
                "custMobileNo"=>'254715302571',
                "pass"=> $pass

                );

            try {
                $client = new SoapClient("https://10.0.0.1:1501/agencyservices?WSDL");
            } catch (Exception $e) {
                redirect('/en/api_access_error');
            }

            $result = $client->GetIssuedSBPNCC($serviceArguments);
            $sbpnumber=$result->GetIssuedSBPNCCResult->SBPNumber;
            $year=$result->GetIssuedSBPNCCResult->CalenderYear;
            $bizname=$result->GetIssuedSBPNCCResult->BusinessName;
            $pinno=$result->GetIssuedSBPNCCResult->PINNumber;
            $biztype=$result->GetIssuedSBPNCCResult->BusinessActivityDescription;
            $activitycode=$result->GetIssuedSBPNCCResult->ActivityCode;
            $paidfee=$result->GetIssuedSBPNCCResult->AmountPaid;
            $inwords=$result->GetIssuedSBPNCCResult->AmountPaidInWords;
            $pobox=$result->GetIssuedSBPNCCResult->POBox;
            $plotnumber=$result->GetIssuedSBPNCCResult->PlotNumber;
            $street=$result->GetIssuedSBPNCCResult->PhysicalAddress;
            $issuedate=$result->GetIssuedSBPNCCResult->DateIssued;
            $bizactivityname=$result->GetIssuedSBPNCCResult->BusinessActivityName;
            $bizzid=$result->GetIssuedSBPNCCResult->BusinessID;
            $receiptno=$result->GetIssuedSBPNCCResult->REFReceiptNumber;


            $sbpdata = array(
                'sbpnumber'=>$sbpnumber,
                'year'=>$year,
                'bizname'=>$bizname,
                'pinno'=>$pinno,
                'biztype'=>$biztype,
                'activitycode'=>$activitycode,
                'paidfee'=>$paidfee,
                'inwords'=>$inwords,
                'pobox'=>$pobox,
                'plotnumber'=>$plotnumber,
                'street'=>$street,
                'issuedate'=>$issuedate,
                'bizactivityname'=>$bizactivityname,
                'bizzid'=>$bizzid,
                'receiptno'=>$receiptno,
                'result'=>$result
                );

            $data=json_encode($sbpdata);
            return print_r($data);
            
        }


}
