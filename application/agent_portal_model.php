<?php  header("access-control-allow-origin: *"); ?>

<?php

/**
* 
*/
class Agent_portal_model extends CI_Model
{
	
	function confirm_topup(){
		$username="72386895"; //You get this from Jambopay
		$jp_key= '26e22682-37b1-4bf7-ba0b-a1bf76fa1ef4'; //You get this from Jambopay

		$agentReference = strtoupper(substr(md5(uniqid()),25)); 
		$phone=$this->input->post('phoneno');
		$phone=substr($phone,-9);
		$amount=$this->input->post('amount');
		$amount=number_format($amount, 2,'.','');
	    $ck = $username . $agentReference . $amount . $phone . $jp_key;
	    $pass =sha1(utf8_encode($ck));

		$serviceArguments = array(
		      "userName"=>$username,
		      "agentReference"=>$agentReference,
		      "amount"=>$amount,
		      "phone"=>$phone,
		      "pass"=> $pass         
		    );

		//$client = new SoapClient("");
		try {
			$client = new SoapClient("https://www.jambopay.com/agencyservices?WSDL");
		} catch (Exception $e) {
			redirect('/en/api_access_error');
		}
		$result = $client->WalletInitiateDeposit($serviceArguments);
        $tran_id=$result->WalletInitiateDepositResult->TransactionID;
        $rescode=$result->WalletInitiateDepositResult->Result->ResultCode;
        $restext=$result->WalletInitiateDepositResult->Result->ResultText;
        
        if ($rescode==0 && $restext=="OK"){
		    $ck = $username . $tran_id . $jp_key;
		    $pass = sha1(utf8_encode($ck));
		    $confirm = array('1'=>$phone,'2'=>$amount,'3'=>$tran_id,'4'=>$result);
		    $data=array(
		    	'transactionID'=>$tran_id,
		    	'password'=>$pass,
		    	'phone'=>$phone,
		    	'amount'=>$amount
		    	);
		    $this->session->set_userdata($data);
		    return $confirm;
		    #return $result;
		   
		}
		 //return $result;
	}
	/*$serviceArguments = array(
      "userName"=>$username,
      "transactionID"=>$tran_id,
      "currency"=>'KES',
      "phone"=>$phone,
      "pass"=> $pass
            
    );*/
	function complete_topup(){
		$username="72386895"; //You get this from Jambopay
		$serviceArguments = array(
		      "userName"=>$username,
		      "transactionID"=>$this->session->userdata('transactionID'),
		      "currency"=>'KES',
		      "amount"=>$this->session->userdata('amount'),
		      "phone"=>$this->session->userdata('phone'),
		      "pass"=> $this->session->userdata('password')
		    );

			try {
				$client = new SoapClient("https://www.jambopay.com:1450/agencyservices?WSDL");
			} catch (Exception $e) {
				redirect('/en/api_access_error');
			}
		   $result = $client->WalletCompleteDeposit($serviceArguments);
		   $resulttext = $result->WalletCompleteDepositResult->Result->ResultText;
		   $resultcode=$result->WalletCompleteDepositResult->Result->ResultCode;
		   $message='';
		    if ($resultcode==0 && $resulttext=="OK"){
		    	return $message="The transaction was completed successfully";
		    }else {
		    	return $message="Errors Occured";
		    	//return $result;
		    }
		    	
	}


		function preparePayment(){

			$username = "97763838";
	        $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
	        $businessId = $this->input->post('biz_no');
	        $agentRef = strtoupper(substr(md5(uniqid()),25));
	        $ck = $username . $agentRef . $businessId . $key;
	        $pass = sha1(utf8_encode($ck ));

	        $serviceArguments = array(
	            "userName"=>$username,
	            "agentRef"=>$agentRef,
	            "businessId"=>$businessId,
	            "calendarYear"=>"2014",
	            "custMobileNo"=>"254724972416",
	            "pass"=> $pass

	            );


	        try {

	        	$client = new SoapClient("https://41.212.9.57:1501/agencyservices?WSDL");
	        	
	        } catch (Exception $e) {
	        	
	        	redirect('/en/api_access_error');
	        }
	        
	        $result = $client->PreparePaymentSBPNCC($serviceArguments);
	        $resulttext = $result->PreparePaymentSBPNCCResult->Result->ResultText;
		    $resultcode=$result->PreparePaymentSBPNCCResult->Result->ResultCode;
	        $bill_status=$result->PreparePaymentSBPNCCResult->BillStatus;

			if ($resultcode==0 && $resulttext=="OK" ){
		        $trans_id2=$result->PreparePaymentSBPNCCResult->TransactionID;
		        $biz_id=$result->PreparePaymentSBPNCCResult->BusinessId;
		        $bill_no=$result->PreparePaymentSBPNCCResult->BillNumber;
		        $biz_name=$result->PreparePaymentSBPNCCResult->BusinessName;
		        $physical_add=$result->PreparePaymentSBPNCCResult->PhysicalAddress;
		        $bill_status=$result->PreparePaymentSBPNCCResult->BillStatus;
		        $sbp_fee=$result->PreparePaymentSBPNCCResult->SBPFee;
                #if($bill_status=="NEW"){

                	$data = array(
		                'biz_id'=>$biz_id,
		                'bill_no'=>$bill_no,
		                'trans_id'=>$trans_id2,
		                'biz_name'=>$biz_name,
		                'physical_address'=>$physical_add,
		                'bill_stat'=>$bill_status,
		                'sbpfee'=>$sbp_fee
		            );
		        $this->session->set_userdata($data);

	        	return $data;

                #}else return $message="5000";
		        
	        }


		}

		function confirm_payment(){
			$email=$this->input->post('email');
			$phone=$this->input->post('phone');
			$amount=$this->session->userdata('sbpfee');
			$data=array('amount'=>$amount,'email'=>$email,'phone'=>$phone);
			$this->session->set_userdata($data);
			return $data;
		}


		function complete_payment(){

		   $username = "97763838";
		   $key = "3637137f-9952-4eba-9e33-17a507a2bbb2";
		   $tranid = $this->session->userdata('trans_id');
		   $channelRef = "45875";  
		   $amount = $this->session->userdata('amount');
		   $currency = "KES";
		   $channel = "Agency";
		       
		    $ck = $username . $tranid . $key;
		    $pass = sha1(utf8_encode($ck ));

		   $serviceArguments = array(
		     "userName"=>$username,
		     "transactionId"=>$tranid,
		     "amount"=>$amount,      
		     "currency"=> $currency,
		     "channelRef"=> $channelRef,
		     "pass"=> $pass
		     
		   );

		   try {
				$client = new SoapClient("https://41.212.9.57:1501/agencyservices?WSDL");
			} catch (Exception $e) {
				redirect('/en/api_access_error');
			}
		    $result = $client->CompletePaymentSBPNCC($serviceArguments);
		    $resulttext = $result->CompletePaymentSBPNCCResult->Result->ResultText;
		    $resultcode = $result->CompletePaymentSBPNCCResult->Result->ResultCode;
		    if ($resultcode==0 && $resulttext=="OK" ){
		    	  $phone=$this->session->userdata('phone');
		    	  $ph=substr($phone,-9);
		          $gsm="254".$ph;
		          $postUrl = "http://api2.infobip.com/api/sendsms/xml";
		          $user="JumaMusyoka";
		          $pass="IV3fuK6w";
		          $sender="JAMBOPAY";
		          $msg="Your trasaction Was Successfully completed. Please download the permit from your email!";
		          
		          $xmlString =
		          "<SMS>
		          <authentification>
		          <username>$user</username>
		          <password>$pass</password>
		          </authentification>
		          <message>
		          <sender>$sender</sender>
		          <text>$msg</text>
		          </message>
		          <recipients>
		          <gsm>$gsm</gsm>
		          </recipients>
		          </SMS>";
		          // previously formatted XML data becomes value of "XML" POST variable
		          $fields = "XML=" . urlencode($xmlString);
		          // in this example, POST request was made using PHP's CURL
		          $ch = curl_init();
		          curl_setopt($ch, CURLOPT_URL, $postUrl);
		          curl_setopt($ch, CURLOPT_POST, 1);
		          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		          // response of the POST request
		          $response = curl_exec($ch);
		          curl_close($ch);



		     return $message="Transaction Has Successfully been completed";
		 	}else return $message="Some Errors Occured during the process please try again!";

				
		}


		function register_user(){

			//set POST variables
			$url = 'https://www.jambopay.com/ewallet/expressregister.aspx';

			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$email=$this->input->post('email');
			$express_phone=$this->input->post('express_phone');
			$express_phone=substr($express_phone, -9);
			$express_pin=$this->input->post('express_pin');
			$national_id=$this->input->post('national_id');
			$jp_business=$this->input->post('jp_business');
			$fields = array(
			                        'first_name' => urlencode($first_name),
			                        'last_name' => urlencode($last_name),
			                        'email' => urlencode($email),
			                        'express_phone' => urlencode($express_phone),
			                        'express_pin' => urlencode($express_pin),
			                        'national_id' => urlencode($national_id),
			                        'jp_business' => urlencode($jp_business)
			                );

			//url-ify the data for the POST
			$fields_string='';
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			//open connection
			$ch = curl_init($url);
			$path = base_url().'/cacert.pem';
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '/cacert.pem');
			//execute post
			$result = curl_exec($ch);

			if (curl_errno($ch)) {
			    // this would be your first hint that something went wrong
			    print('Couldn\'t send request: ' . curl_error($ch));
			} else {
			    // check the HTTP status code of the request
			    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if ($resultStatus != 200) {
			        print('Request failed: HTTP status code: ' . $resultStatus);
			    }else {
			    	$xml=simplexml_load_string($result);
			    	$response=array('1' =>$xml->RESPONSE_CODE,'2'=>$xml->RESPONSE_TEXT);
			    	if($xml->RESPONSE_TEXT=="OK"){
			    		print($xml->RESPONSE_CODE);
			    	}else{
			    		print($xml->RESPONSE_TEXT);
			    	}
			    	
			    	//print_r($response);
			    }
			}

			//close connection
			curl_close($ch);
			//return var_dump($result);
		}


		function check_balance(){

			//set POST variables
			$url = 'https://www.jambopay.com/ewallet/expresstatement.aspx';

			$phone=$this->input->post('phone');
			$phone=substr($phone, -9);
			$pin=$this->input->post('pin');
			$jp_business="test@webtribe.co.ke";
			$fields = array(
		                        'jp_customer_telephone' => urlencode($phone),
		                        'jp_customer_pin' => urlencode($pin),
		                        'jp_business' => urlencode($jp_business)
			                );

			//url-ify the data for the POST
			$fields_string='';
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			//open connection
			$ch = curl_init($url);
			$path = base_url().'/cacert.pem';
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '/cacert.pem');
			//execute post
			$result = curl_exec($ch);

			if (curl_errno($ch)) {
			    // this would be your first hint that something went wrong
			    print('Couldn\'t send request: ' . curl_error($ch));
			} else {
			    // check the HTTP status code of the request
			    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if ($resultStatus != 200) {
			        print('Request failed: HTTP status code: ' . $resultStatus);
			    }else {
			    	$xml=simplexml_load_string($result);
			    	//echo $xml->RESPONSE_TEXT;
			    	echo $xml->BALANCE;
			    	//echo $result;
			    }
			}

			//close connection
			curl_close($ch);
			//return var_dump($result);

		}


		function mini_statement(){

			//set POST variables
			$url = 'https://www.jambopay.com/ewallet/expresstatement.aspx';

			$phone=$this->input->post('phone');
			$phone=substr($phone, -9);
			$pin=$this->input->post('pin');
			$jp_business="test@webtribe.co.ke";
			$fields = array(
		                        'jp_customer_telephone' => urlencode($phone),
		                        'jp_customer_pin' => urlencode($pin),
		                        'jp_business' => urlencode($jp_business)
			                );

			//url-ify the data for the POST
			$fields_string='';
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			//open connection
			$ch = curl_init($url);
			$path = base_url().'/cacert.pem';
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt ($ch, CURLOPT_CAINFO,dirname(__FILE__) . '/cacert.pem');
			//execute post
			$result = curl_exec($ch);

			if (curl_errno($ch)) {
			    // this would be your first hint that something went wrong
			    print('Couldn\'t send request: ' . curl_error($ch));
			} else {
			    // check the HTTP status code of the request
			    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			    if ($resultStatus != 200) {
			        print('Request failed: HTTP status code: ' . $resultStatus);
			    }else {
			    	$xml=simplexml_load_string($result);
			    	//echo $xml->RESPONSE_TEXT;
			    	$statement=$xml->STATEMENT;
			    	$data = explode("|",$statement);
					$value =  array($data);
					$display = '';
					foreach ($data as $key => $value) {
						echo '<tr>';
						$data2=explode(",", $value);
						$value2=array($data2);
						foreach ($data2 as $key => $value2) {

							echo "<td>".$value2."</td>";

						}
						echo '</tr>';
						
					}
			    	//echo $result;
			    }
			}

			//close connection
			curl_close($ch);
			//return var_dump($result);

			
		}


	}

?>