<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);


/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('MAIN_URL','https://192.168.6.195:1602/agencyservices?wsdl');
define('WALLET_URL','https://192.168.6.195:1600/jpweb/ewallet/expresstatement.aspx');
define('REG_URL', 'https://192.168.6.195:1600/jpweb/ewallet/expressregister.aspx');
define('USER_NAME','97763838');
define('JP_KEY','3637137f-9952-4eba-9e33-17a507a2bbb2');
define('MIDDLEWARE','http://192.168.11.22/JamboPayServices/api/payments/');
define('TOKEN_URL','http://192.168.11.22/JamboPayServices/token');
define('APP_KEY','a2868c59-67e2-e411-8285-b8ee657eaebc');
define('EMOMENTUM_WSDL',"http://35.164.42.112/mainsector.asmx?wsdl");
define('EMOMENTUM_APP_ID',"bb9ffb33-eb3b-4d8c-a0d9-3b6a2da132d3");
define('ADVERT_API_URL',"http://192.168.11.22:8080/CAd/api/");


define('TRANSPORT_FILES',"./assets/temp/uploads/transport");

define('ERROR_PREFIX','<li>');
define('ERROR_SUFFIX','</li>');
define('BANNERS_PATH','uploads/transport/banners/');