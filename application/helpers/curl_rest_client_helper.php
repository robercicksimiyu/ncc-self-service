<?php
if(!function_exists('curl_rest_client'))    
{
	function curl_rest_client($url,$method,$params=false,$external=false,$data_type=false,$has_header=true)
	{
		$CI =& get_instance();
		$rest_url=(!$external) ? MIDDLEWARE.$url : $url;
		$headers=array(
			'Authorization: bearer ' .  $CI->session->userdata('token'),
            'app_key: '.APP_KEY
        );
        if($data_type=="json") {
        	$headers=array(
			'Content-Type: application/json'           
           	);      	
        }

        $ch = curl_init();
	    curl_setopt($ch,CURLOPT_URL,$rest_url);
	    curl_setopt($ch,CURLOPT_MAXREDIRS,3);
	    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,0);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    curl_setopt($ch,CURLOPT_VERBOSE,0);
	    curl_setopt($ch,CURLOPT_HEADER,0);
	    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);

	    if($has_header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }


	    switch (strtoupper($method)) {
	        case 'POST':
	        	if(!$data_type) {
	        		foreach ($params as $key => $value) {
	        		  $post_items[] = $key . '=' . $value;
	        		}
	        		$params = implode('&', $post_items);
	        	}	        	
	            curl_setopt($ch,CURLOPT_POST,true);
	            curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
	            break;
	        case 'DELETE':
	            curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'DELETE');
	            break;
	        case 'PUT':
	            curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'PUT');
	            curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
	            break;
	    }
	    // Execute CURL
	    $json = curl_exec($ch);


	   
	    $code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	    
	    curl_close($ch);
	    if ($code < 200 || $code > 299) {
	        $_tmp = json_decode($json,true);
	        return 'ERROR: '. $_tmp['message'];
	    }
	    return $json;
	}
}
?>