<!-- <div class="mainpanel"> -->
 <div class="pageheader">
      <h2><i class="fa fa-home"></i> e-Wallet Mini Statement <span>county wallet mini statement</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Nairobi County e-Wallet</a></li>
          <li class="active">View Mini Statement</li>
        </ol>
      </div>
    </div>
   
  <div class="contentpanel" >
      
      <div class="row">
        <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" class="form-horizontal">
             <div class="panel panel-default" >
              <div class="panel-heading">
                <div class="panel-btns">
                  <a href="#" class="minimize"></a>
                </div>
                <h4 class="panel-title panelx">Account Receipts</h4>
                <p>NCC Personal Account Receipts</p>
              </div>
              <div class="panel-body" style="">
              
              <table class="table table-striped mb30">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Receipt No.</th>                    
                    <th>Amount</th>
                  </tr>
                </thead>
                <?php  if(count($receipts)):?>
                  <?php foreach($receipts as $receipt):?>
                      <tr>
                        <td><?=date('d-m-Y',strtotime($receipt["TransactionDate"]))?></td>
                        <td><?=$receipt["ReceiptNumber"]?></td>
                        <td><?=$receipt["Amount"]?></td>
                        <td> <a class="btn btn-primary" href="<?=base_url('rents/receipts/').'/'.$receipt['HouseNumber'].'/'.$receipt['EstateID'].'/'.$receipt['ReceiptNumber']?>">Print</a></td>
                      </tr>
                    <?php endforeach;?>              
                                   
                <?php  endif;?>
              </table>
                 <div class="err-box" style="padding:30px;"></div> 
              </div>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
<!-- panel -->
       </div>
  </div><!-- contentpanel -->

    
</div><!-- mainpanel -->



