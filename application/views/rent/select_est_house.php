<!--   <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> House Rent <span>Pay your House Rent Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">House Rent</a></li>
          <li class="active">Select House</li>
        </ol>
      </div>
    </div>
	
<div class="pageheader" style="height:60px">
    <a href="<?php echo base_url();?>index.php/sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>index.php/sbp/view_balance"><i class="fa fa-credit-card"></i>Check Wallet Bal</a>
    </div> 
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        <?php //$hses = explode(',',$esthouse); ?>
          <h4 class="panel-title">Select your House</h4>
          <p>Cross check to make sure you have selected your house</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('sbp/display_business_details',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-6">
                      <label class="col-sm-6 control-label">House Number</label>
                       <select class="form-control" name="hse" id="hse" required>
                            <option value="">Select House No.</option>
                            <?php #foreach($esthouse->result() as $value) { ?>
                                 <option value="<?php #echo $value->HouseNumber; ?>"><?php #echo $value->Description; ?></option>
                            <?php #} ?>
                          </select>
                          
                      </div>
                      <!-- <div class="col-sm-6">
                      <label class="col-sm-6 control-label"></label>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        </div> -->
                      
            </div>
            <input type="submit" class="btn btn-primary" value="Submit"/>
            <!-- <button type="reset" class="btn btn-primary">Reset</button> -->
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Select your Estate</p></li>
            <li><p>Click Submit to Load Search for your House Number</p></li>
          </ol>
        </div>

      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

