<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Receipts Portal - Rents</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Landrates</a></li>
          <li class="active">Print receipt</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-10" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Fill in the transaction Details below    <?php #echo $dates="2015-01-06 00:00:00"; echo date("Y-m-d",$dates) ;?></h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('rents/insertManualReceipt',array('class' =>"form-block")) ?>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Estates</h4>
              <select class="form-control chosen-select" id="estate" name="estate" data-placeholder="Select Estate...">
                <option value=""></option>
                <?php
                foreach($estates as $key=>$value) {
                  ?>
                  <?php if($value->HouseOrMarketStall==1): ?>
                  <option value="<?php echo $value->EstateID; ?>"><?php echo $value->EstateDescription; ?></option>
                  <?php endif;   
                }
                ?>
              </select></div>
            
            <div class="col-sm-4">
              <h4 class="subtitle mb5">House numbers</h4>
                  <div id="houseno">
                      <select class="form-control chosen-select" id="house" name="house" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select></div>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Date Paid</h4>
              <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="issuedate" required/>
              <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> -->
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Receipt Number</h4>
              <input type="text" value="" id="receiptno" name="receiptno" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Arrears</h4>
              <input type="text" value="" id="arrears" name="arrears" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Paid By</h4>
              <input type="text" value="" id="paidby" name="paidby" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Amount Paid</h4>
              <input type="text" value="" id="amount" name="amount" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Amount Due</h4>
              <input type="text" value="" id="amountdue" name="amountdue" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-4">
              <h4 class="subtitle mb5">House Owner.</h4>
              <input type="text" value="" id="houseowner" name="houseowner" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Phone No.</h4>
              <input type="text" value="" id="phonenumber" name="phonenumber" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
          </div>
            <div id="proc">
              <input type="submit" class="btn btn-primary" value="Save">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->

    </div><!-- contentpanel --> 
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

        // Date Picker
        jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
        
        jQuery('#datepicker-inline').datepicker();
        
        jQuery('#datepicker-multiple').datepicker({
          numberOfMonths: 3,
          showButtonPanel: true
        });

          $('#loader').hide();
          $('#estate').on('change',function(e){
              $('#loader').show();
              $.post('<?php echo base_url();?>index.php/house_rent/get_houses',
              {
                EstateID:$('#estate').val()
              },
              function(data){
                $('#loader').hide();
                $('#houseno').html(data);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

          });

      });
    </script>
