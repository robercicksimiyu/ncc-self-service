<div class="pageheader">
      <h2><i class="fa fa-inbox"></i> HR <span>House Rent</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/rent/house_rent">House Rent</a></li>
          <li class="active">Search House Details</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Enter House Details</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php #var_dump($housetypes); ?>
          <?php echo form_open('house_rent/preparehousepayment',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Estates</h4>
                  <select class="form-control chosen-select" id="estate" name="estate" data-placeholder="Select Estate...">
                        <option value=""></option>
                        <?php
                          foreach($estates as $key=>$value) {
                            ?>
                            <?php if($value->HouseOrStall==1): ?>
                             <option value="<?php echo $value->ID; ?>"><?php echo $value->Description; ?></option>
                           <?php endif;   
                          }
                        ?>
                      </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">House Type</h4>
                  <div id="housetypeDiv">
                      <select class="form-control chosen-select" id="housetype" name="housetype" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select>
                  </div>
              </div>
              <div class="col-sm-3" id="loader2">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">House numbers</h4>
                  <div id="housenoDiv">
                      <select class="form-control chosen-select" id="house" name="house" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select>
                      <input type="hidden" id='htype' name="htype" value="">
                  </div>
              </div>
              <div class="col-sm-3" id="loader">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>

            <div id="proc">
              <!-- <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Phone Number</h4>
                  <input type="text" value="" id="phone" name="phone" class="form-control" />
                </div>

              </div> -->
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->
          
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li>
            <p>Choose the name of your Estate</p></li>
            <li>
              <p>Select the House Number</p></li>
              </ol>
            </div>
            
          </div>
    </div><!-- contentpanel --> 



   <script type="text/javascript">
      $(document).ready(function(){
          jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          $('#loader').hide();
          $('#loader2').hide();
          $('#estate').on('change',function(e){
              $('#loader').show();
              $.post('<?php echo base_url();?>index.php/house_rent/houseTypes',
              {
                EstateID:$('#estate').val()
              },
              function(data){
                $('#loader').hide();
                $('#housetypeDiv').html('');
                $('#housetypeDiv').append(data);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }
                $.post('<?php echo base_url();?>index.php/house_rent/getResidence',
              {
                TypeID:$('#housetype').val(),
                EstateID:$('#estate').val()
              },
              function(data){
                $('#loader').hide();
                $('#housenoDiv').html(data).trigger("chosen:updated");
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

              });

          });

          $('body').on('change','#housetype',function(){
            console.log( $('#housetype').val());
             /* $('#loader').show();*/
              $.post('<?php echo base_url();?>index.php/house_rent/getResidence',
              {
                TypeID:$('#housetype').val(),
                EstateID:$('#estate').val()
              },
              function(data){
                $('#loader').hide();
                $('#housenoDiv').html(data).trigger("chosen:updated");
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

          });

      });
    </script>