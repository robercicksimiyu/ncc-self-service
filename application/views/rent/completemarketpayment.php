     <div class="pageheader">
      <h2><i class="fa fa-inbox"></i> Market / Stall <?php #echo $message['rescode'];?> <span>Complete Payment</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/en/house_rent">Market/Stall</a></li>
          <li class="active">Payment Status</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default col-md-8">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize"></a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                <?php $response = $message['rescode'];?>
               
                <?php if($response=="0"): ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Transaction was successfully completed.".anchor('mrents/printdirect/'.str_replace('/','-',$message["housenumber"]),'Print Receipt',array('class'=>'btn btn-primary','style'=>'float:right;'));
                  ?>
              </div>
            <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry you entered a wrong PIN".anchor('marketstall/select_market','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;"));
                  ?>
              </div>
            <?php elseif($response=="1053"): ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry the transaction is already complete.".anchor('marketstall/select_market','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;"));
                  ?>
              </div>
            <?php elseif($response=="2030"): ?>
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "You have insufficient funds to make this payment.Your wallet balance is Ksh: $bal ".anchor('selfservice/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;"));
                  ?>
              </div>
              <?php endif; ?>
              </div>
            </div><!-- panel-body -->
          </div>
        </div>
      
    </div><!-- contentpanel -->

   