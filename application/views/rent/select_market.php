<!--   <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Markets <span>Pay your Market Rates Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Markets</a></li>
          <li class="active">Select Market</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
            <div class="panel panel-default col-md-3" style="width:70%;margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="minimize">&minus;</a>
          </div>
          <h4 class="panel-title">Enter House Details</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php //var_dump($houses); ?>
          <?php echo form_open('rent/preparehousepayment',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Market</h4>
                  <select class="form-control chosen-select" id="estate" name="estate" data-placeholder="Select Estate...">
                        <option value=""></option>
                       <?php
                          foreach($estates as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->EstateID; ?>"><?php echo $value->EstateDescription; ?></option>
                           <?php    
                          }
                        ?>
                      </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Market/Stall number</h4>
                  <div id="houseno">
                      <select class="form-control chosen-select" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select>
                  </div>
              </div>
            </div>
            <div id="proc">
              <div class="form-group">
                <!-- <div class="col-sm-8">
                  <h4 class="subtitle mb5">Phone Number</h4>
                  <input type="text" value="" id="phone" name="phone" class="form-control" />
                </div> -->

              </div>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Select your Estate</p></li>
            <li><p>Click Submit to Load Search for your House Number</p></li>
          </ol>
        </div>

      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          $('#estate').on('change',function(e){
              $.post('<?php echo base_url();?>index.php/rent/get_houses',
              {
                EstateID:$('#estate').val()
              },
              function(data){
                console.log(data);
                $('#houseno').html(data);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

          });
      });
    </script>
