   <div class="pageheader">
    <h2><i class="fa fa-inbox"></i> Market / Stall <span>Confirm Details</span></h2>
    <div class="breadcrumb-wrapper">
      <span class="label">You are here:</span>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>index.php/rent/house_rent">Market / Stall</a></li>
        <li class="active">Confirm Market / Stall Details <?php echo $this->session->userdata('jpwnumber'); ?></li>
      </ol>
    </div>
  </div>



  <div class="contentpanel" >
  <?php if($confirm['rescode']==0): ?>
    <div >
    <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="minimize">&minus;</a>
          </div>

        </div>
        <div class="panel-body">
          <div class="row">
            
            <div class="table-responsive">
              <table class="table table-striped mb30">
                <thead>
                  <tr>
                    <th colspan="2" style="text-align:center;">CONFIRM THE INFORMATION BELOW</th>
                  </tr>
                </thead>
                <?php echo form_open('marketstall/confirm2rent'); ?>
                <tbody>
                  <tr>
                    <td><b>OCCUPANT</b></td>
                    <td><?php echo $confirm['CustName']; ?></td>
                    <input type="hidden" value="<?php echo $confirm['CustName']; ?>" name="custname"/>
                  </tr>
                  <tr>
                    <td><b>PHYSICAL ADDRESS</b></td>
                    <td><?php echo $confirm['PhysicalAddress']; ?></td>
                    <input type="hidden" value="<?php echo $confirm['PhysicalAddress']; ?>" name="physicaladdress">
                  </tr>
                  <tr>
                    <td><b>MARKET/STALL NUMBER</b></td>
                    <td><?php echo $confirm['HouseNumber']; ?></td>
                    <input type="hidden" value="<?php echo $confirm['HouseNumber']; ?>" name="houseno">
                  </tr>
                  <tr>
                    <td><b>PHONE NUMBER</b></td>
                    <td><?php echo $confirm['phone']; ?></td>
                    <input type="hidden" value="<?php echo $confirm['phone']; ?>" name="phone">
                  </tr>
                  <tr>
                    <td><b>MONTHLY RENT</b></td>
                    <td><?php echo number_format($confirm['MonthlyRent'],2); ?></td>
                    <input type="hidden" value="<?php //echo $confirm['2'] ?>" name="">
                  </tr>
                  <tr>
                    <td><b>OTHER MONTHLY CHARGES</b></td>
                    <td><?php echo number_format($confirm['MonthlyOtherCharges'],2); ?></td>
                    <input type="hidden" value="<?php //echo $confirm['2'] ?>" name="">
                  </tr>
                  <tr>
                    <td><b>RENT ARREARS</b></td>
                    <td><?php echo number_format($confirm['RentArrears'],2); ?></td>
                    <input type="hidden" value="<?php echo $confirm['RentArrears'] ?>" name="arrears">
                  </tr>
                  <tr>
                    <td><b>OTHER RENT ARREAS</b></td>
                    <td><?php echo number_format($confirm['OtherArrears'],2); ?></td>
                    <input type="hidden" value="<?php //echo $confirm['2'] ?>" name="">
                  </tr>
                  <tr>
                    <td><b>CURRENT BALANCE</b></td>
                    <td><?php echo number_format($confirm['CurrentBalance'],2); ?></td>
                    <input type="hidden" value="<?php echo $confirm['CurrentBalance'] ?>" name="amountdue">
                    <input type="hidden" value="<?php echo $confirm['TransactionID'] ?>" name="transid">
                  </tr>
                  <tr>
                    <td><b>LAST BILL MONTH</b></td>
                    <td><?php echo $confirm['LastBillMonth']; ?></td>
                    <input type="hidden" value="<?php //echo $confirm['2'] ?>" name="">
                  </tr>

                  <tr>
                    <td><b>RENT DUE DATE</b></td>
                    <td><?php echo $confirm['RentDueDate']; ?></td>
                    <input type="hidden" value="<?php //echo $confirm['2'] ?>" name="">
                  </tr>
                  <?php //var_dump($confirm['4']) ?>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div>
        </div><!-- panel-body -->
        <div class="panel-footer">
              <?php  echo'<div class="col-sm-3">';
              echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                echo '<input type="password" id="jp_pin" placeholder="Enter Your Wallet Pin" name="jp_pin" class="form-control" required/>';
                echo'</div>';
                echo'<div class="col-sm-3">';
              echo '<h4 class="subtitle mb5">'."Enter Amount".'</h4>';
                echo '<input type="text" id="amount" placeholder="Enter Amount" name="amountpaid" class="form-control" required/>';
                echo'</div>';
                echo '<br/>';
                ?>
          <input type="submit" value="Confirm Transaction" class="btn btn-primary">
          <?php echo form_close(); ?>

          <?php echo anchor('marketstall/select_market','Cancel Transaction',array('class'=>"btn btn-default")); ?>
        </div>
      </div>
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter the Amount you want to Pay</p></li>
              <li>
                <p>Enter your County Wallet PIN</p></li>
                <li>
                  <p>Click confirm to continue with Payment or else Cancel</p></li>
                </ol>
              </div>

            </div>
          </div>

        <?php elseif($confirm['rescode']==2021): ?>
              <div class="alert alert-danger col-md-8">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "The Phone Number is Invalid.".anchor('marketstall/select_market','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;"));
                  ?>
              </div>
        <?php elseif($confirm['rescode']==1111): ?>
              <div class="alert alert-danger col-md-8">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "House charge statement not found.".anchor('marketstall/select_market','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;"));
                  ?>
              </div>
        <?php endif;?>
        </div><!-- contentpanel -->

