<div class="pageheader">
      <h2><i class="fa fa-inbox"></i> HR <span>House Rent</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/houserents/houserent">House Rent</a></li>
          <li class="active">Search House Details</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel " >
    <div class="panel panel-default ">
      <div class="panel-heading">
        <div class="panel-btns">
          <a href="#" class="minimize">&minus;</a>
        </div>
        <h4 class="panel-title rtitle ">Enter House Details</h4>
        <p>Cross check to make sure you have filled in the correct details</p>
      </div>
      <?php $err = $this->uri->segment(3);?>
      <div class="panel-body">
        <div class="col-md-3">
          <div class="rdio rdio-success">
            <input type="radio" name="radio" value="3" id="radioWarning" <?php if($err=='erro1'||empty($err)) echo 'checked';?>/>
            <label for="radioWarning">House Rent</label>
          </div>
          <!-- <div class="rdio rdio-success">
            <input type="radio" name="radio" value="4" id="radioSuccess" <?php  if($err=='erro2') echo 'checked';?>/>
            <label for="radioSuccess">Market Stalls</label>
          </div> -->
          <!-- <div class="rdio rdio-success">
            <input type="radio" name="radio" value="5" id="radioDanger" <?php  if($err=='erro3') echo 'checked';?>/>
            <label for="radioDanger">CESS</label>
          </div> -->
        </div>
        <div class="col-md-8">
          <div class="houserent">
            <?php echo form_open('rents/printreceiptcheck',array('class' =>"form-block")) ?>
              <div class="form-group">
                <div class="col-sm-8">
                    <h4 class="subtitle mb5">Estates</h4>
                    <select class="form-control chosen-select" id="estate" name="estate" data-placeholder="Select Estate...">
                          <option value=""></option>
                          <?php
                            foreach($estates as $key=>$value) {
                              ?>
                              <?php #if($value->HouseOrMarketStall==1): ?>
                               <option value="<?php echo $value->EstateID; ?>"><?php echo $value->EstateDescription; ?></option>
                             <?php #endif;   
                            }
                          ?>
                        </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-8">
                    <h4 class="subtitle mb5">House numbers</h4>
                    <div id="houseno">
                        <select class="form-control chosen-select" name="hno" data-placeholder="Choose Estate First...">
                          <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3" id="loader">
                  <h4 class="subtitle mb5">&nbsp</h4>
                  <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
                </div>
              </div>
              <!-- <div class="form-group">
                <div class="col-sm-3">
                <h4 class="subtitle mb5">Date of Payment</h4>
                  <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="issuedate" required/>
                </div>
              </div> -->
              <div id="proc">
                <input type="submit" class="btn btn-primary print_r" value="Print">
              </div>
            <?php echo form_close(); ?> <br>
            <?php 
              if(isset($err)&&!empty($err)&&$err=='erro1') :?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Receipt for the specified house does not exist.</strong> 
            </div>
          <?php endif;?>
          </div><!-- house -->
          <div class="stalls">
            <?php echo form_open('marketstalls/stallcheck',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Market Name</h4>
                  <select class="form-control chosen-select" id="stallestate" name="stallestate" data-placeholder="Select Estate...">
                        <option value=""></option>
                        <?php
                          foreach($estates as $key=>$value) {
                        ?>
                            <?php if($value->HouseOrMarketStall==2): ?>
                             <option value="<?php echo $value->EstateID; ?>"><?php echo $value->EstateDescription; ?></option>
                           <?php endif;   
                          }
                        ?>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Market Stall numbers</h4>
                  <div id="stallhouseno">
                      <select class="form-control chosen-select" name="stallno" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select>
                  </div>
              </div>
              <div class="col-sm-3" id="loader2">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>
            <div id="proc">
              <input type="submit" class="btn btn-primary print_s" value="Print">
            </div>
               <?php echo form_close(); ?><br>
            <?php  
              if($err=='erro2') : ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Receipt for the specified stall No does not exist.</strong> 
            </div>
          <?php endif;?>
          </div><!-- stall -->
          <!-- <div class="cess">
            <?php echo form_open('cess/cesscheck',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Select CESS Type</h4>
                  <select class="form-control chosen-select" id="cessCode" name="cesscode" data-placeholder="Select Type..." required><option value=""></option><?php foreach($cess as $key=>$value) { ?><option value="<?php echo $value->CESSCode; ?>"><?php echo str_replace('_',' ',str_replace('WKLY','WEEKLY',$value->CESSDescription)); ?></option><?php } ?></select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                <h4 class="subtitle mb5">National ID Number</h4>
                <input type="text" id="idno" name="idno" class="form-control" required/>
              </div>
              <div class="col-sm-3" id="loader3">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>
            <div id="proc">
              <input type="submit" class="btn btn-primary print_c" value="Print">
            </div>
               <?php echo form_close(); ?><br>
            <?php  
              if($err=='erro3') : ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Receipt for the specified bill number No does not exist.</strong> 
            </div>
          <?php endif;?>
          </div> --><!-- cess -->
        </div>
      </div><!-- panel body -->
      
    </div><!-- panel -->
  </div><!-- contentpanel --> 

    <script type="text/javascript">
      $(document).ready(function(){
          $('.stalls').hide();
          $('.print_r').attr("disabled", true);
          $('.print_s').attr("disabled", true);
          $('.print_c').attr("disabled", true);
          jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          $('#loader').hide();
          //$('radio').
          $('#loader2').hide();
          $('#loader3').hide();

          if($("#radioSuccess").is(":checked")){
            $('.houserent').hide();
            $('.cess').hide();
            $('.stalls').show();
            $('.rtitle').html('Enter Stall Details');
          }

           if($("#radioWarning").is(":checked")){
            $('.stalls').hide();
            $('.cess').hide();
            $('.houserent').show();
            $('.rtitle').html('Enter House Details');
          }

          if($("#radioDanger").is(":checked")){
            $('.stalls').hide();
            $('.houserent').hide();
            $('.cess').show();
            $('.rtitle').html('Enter CESS Details');
          }

          $("#radioSuccess").on('change',function(){ 
              if($(this).is(":checked") ){ 
                  $('.houserent').hide();
                  $('.stalls').show();
                  $('.cess').hide();
                  $('.rtitle').html('Enter Stall Details');
              }
            });
          $("#radioWarning").on('change',function(){ 
              if($(this).is(":checked") ){ 
                  $('.stalls').hide();
                  $('.cess').hide();
                  $('.houserent').show();
                  $('.rtitle').html('Enter House Details');
              }
            });
          $("#radioDanger").on('change',function(){ 
              if($(this).is(":checked") ){ 
                  $('.stalls').hide();
                  $('.cess').show();
                  $('.houserent').hide();
                  $('.rtitle').html('Enter CESS Details');
              }
            });

          $('#cessCode').on('change',function(e){
              $('.print_c').attr("disabled", false);
          });

          $('#estate').on('change',function(e){
              $('#loader').show();
              $.post('<?php echo base_url();?>index.php/house_rent/get_houses',
              {
                EstateID:$('#estate').val()
              },
              function(data){
                $('#loader').hide();
                $('#houseno').html(data);
                $('.print_r').attr("disabled", false);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }
              });
          });
          $('#stallestate').on('change',function(e){
            $('#loader2').show();
              $.post('<?php echo base_url();?>index.php/marketstall/get_houses',
              {
                EstateID:$('#stallestate').val()
              },
              function(data){
                $('#loader2').hide();
                $('#stallhouseno').html(data);
                $('.print_s').attr("disabled", false);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                  $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

          });

      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        // Date Picker
        jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
        
        jQuery('#datepicker-inline').datepicker();
        
        jQuery('#datepicker-multiple').datepicker({
          numberOfMonths: 3,
          showButtonPanel: true
        });
      });
    </script>