<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Small Formats advertisement<span>Pay for your e-Construction Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Small Formats Advertisement</a></li>
      <li class="active">Display Invoice Details</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      //$rescode= $construction['rescode'];
      #$construction['invoiceStatus'] = "Paid";

      //if($rescode=="0" && $construction['invoiceStatus']!="paid"):

      ?>      
       <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <?php if(!isset($invoice_inforamtion['status']['message'])):?>
                <div class="row">
                
                  <?=form_open('advertisement/pay')?>
                    <div class="table-responsive">
                       <table class="table table-striped mb30">
                         <thead>
                          <tr>
                            <th colspan="2" style="text-align:center;">INVOICE DETAILS</th>
                          </tr>
                        </thead>
                        <tbody>
                                     <tr>
                            <td><b>Company Name</b></td>
                            <td><?php echo isset($invoice_inforamtion['advertID']['alias']) ? $invoice_inforamtion['advertID']['alias'] :  " ";?></td>                      
                          </tr>
                          <!-- <tr>
                            <td><b>Applicant Name</b></td>
                            <td>
                            <p><?=isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['firstName']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['firstName'] : ""?> <?=isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['middleName']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['applicantID']['middleName'] : " "?>  </p>                 
                            </td>
                          </tr> -->
                          <tr>
                            <td><b>Email Address</b></td>
                            <td>
                            <p><?php echo isset($invoice_inforamtion['advertID']['applicationID']['applicantContactID']['emailAddress']) ? $invoice_inforamtion['advertID']['applicationID']['applicantContactID']['emailAddress'] :  " ";?></p>                        
                            </td>
                          </tr>
                          <tr>
                            <td><b>Category</b></td>
                            <td>
                            <p><?=isset($invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category']) ? $invoice_inforamtion['advertID']['categoryDimensionID']['categoryID']['category'] : " "?></p>                        
                            </td>
                          </tr>
                         <!--  <tr>
                            <td><b>Area</b></td>
                            <td>
                            <?=$invoice_inforamtion['submitted']['area']?></p>                        
                            </td>
                          </tr> -->
                          <tr>
                            <td><b>Amount</b></td>
                            <td>
                            <p> KES. <?=isset($invoice_inforamtion['amountToPay']) ? number_format($invoice_inforamtion['amountToPay'],2,'.',',') :  " ";?></p>                        
                            </td>
                          </tr>
                          
                          <tr>
                            <td><b>Status</b></td>
                            <td>
                           <p><?php if(isset($invoice_inforamtion['isPaid'])){
                                echo ($invoice_inforamtion['isPaid']) ? "PAID" : "PENDING";
                            } ?></p>                       
                            </td>
                          </tr>
                                
                        </tbody>
                       </table>
                    </div>
                
                </div>
            

             
              
            <div class="panel-footer">
              <div class="col-sm-6">
              <input type="hidden" name="Invoice_amount" value="<?=isset($invoice_inforamtion['amountToPay']) ? $invoice_inforamtion['amountToPay'] :  '';?>">
              <input type="hidden" name="Invoice_number" value="<?=isset($invoice_inforamtion['id']) ? $invoice_inforamtion['id'] :  ' ';?>">
              <input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>
              </div>
              <input class="btn btn-primary" type="submit" value="Make Payment">
              <input class="btn btn-primary" type="button" value="Back" class="backLink">
            </div>
              <?php else:?>
                    <div class="alert alert-danger">
                      <?=$invoice_inforamtion['status']['message']?>
                    </div>
                  <?php endif;?>
          
            
       
      
    </div> 
<script type="text/javascript">
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];               
                if(id=="<?=$invoice_details['Location']?>") {
                   $('#ward').append($('<option selected="selected">', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                 else {
                    $('#ward').append($('<option>', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                }
              
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
