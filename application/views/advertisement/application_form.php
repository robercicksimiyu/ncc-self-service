 <!--  <div class="mainpanel"> -->
 <div class="pageheader">
  <h2><i class="fa fa-home"></i> <?=$title?> <span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href=""><?=$title?></a></li>
      <li class="active">Check Status</li>
    </ol>
  </div>
</div>   

<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px">
    <div class="panel-heading">
      <h4 class="panel-title panelx">Fill in Your Application </h4>
      <p>Cross check to make sure you have filled in the correct application details</p>
    </div>
    <div class="panel-body">
      <?php echo form_open('advertisement/small',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm_application","enctype"=>"multipart/form-data")) ?>         
      <div class="row mb10"> 
       <div class="col-md-6 col-sm-12">
        <label class="col-md-6 col-sm-12 control-label">Company Name</label>
        <input type="text" class="form-control" id="env_tx_company_name" name="company_name" placeholder=" City Business XXX"  />
      </div>
      <div class="col-md-6 col-sm-12">
        <label class="col-sm-5 control-label">Business ID</label>
        <input type="text" class="form-control" id="env_tx_business_id" name="business_id" placeholder="eg. 23xxx" />
      </div>
    </div>
    <div class="row mb10">           
      <div class="col-md-6 col-sm-12">
        <label class="col-sm-12 control-label">Contact Phone No.</label>
        <input type="text" class="form-control" id="env_tx_phoneno" name="tel_no" placeholder="07xxxxxx" required />
      </div>  
      <div class="col-md-6 col-sm-12">
        <label class="col-sm-5 control-label">Contact Email</label>
        <input type="email" class="form-control" id="env_tx_email" name="email" placeholder="you@example.com" required/>
      </div>
    </div>
    <div class="row mb10"> 
     <div class="col-md-6 col-sm-12">
      <label class="col-sm-5 control-label">Sub Counties</label>
      <select class="form-control" name="zone" id="zone" required>
        <option value="">Select one ...</option>  
        <?php foreach($subcounties as $county): ?>
          <?php if($county['ID']==$invoice_details['zone']):?>
           <option data-bid="<?=$county['Bid']?>" selected="selected" value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
         <?php else:?>
           <option value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
         <?php endif;?>
       <?php endforeach;?> 
     </select>
   </div>
   <div class=" col-md-6 col-sm-12">
     <label class="col-sm-6 control-label">Wards</label>
     <select class="form-control" name="subcounty" id="ward" required>
      <option value="">Select Sub County</option>
    </select>
  </div>
</div>
<div class="row mb10">
 <div class="col-md-4 col-sm-12">
  <label class="col-sm-12 control-label">Physical Address</label>
  <input type="text" class="form-control" id="env_tx_address" name="location" placeholder="eg. Nairobi CBD" required/>
</div>
<div class="col-md-4 col-sm-12">
  <label class="col-sm-12 control-label">Street/Road</label>
  <input type="text" class="form-control" id="env_tx_root" name="road_street" placeholder="eg. Standard Street" required/>
</div>  
<div class="col-md-4 col-sm-12">
  <label class="col-sm-12 control-label">Building / LR </label>
  <input type="text" class="form-control" id="env_tx_building" name="building_lr" placeholder="eg. Building XXX " required/>
</div>
</div>
<div class="row mb10">
  <div class="col-md-4 col-sm-12">
    <label class="control-label">Sides (e.g. 2 ) </label>
    <input type="text" class="form-control" id="env_tx_sides" name="sides" placeholder="eg. 21" required/>      
  </div>

  <div class="col-md-4 col-sm-12">
    <label class="col-sm-5 control-label">Category</label>              
    <select class="form-control" id="category_id" name="category_id">
      <option>Select Category</option>
      <?php foreach($categories as $category): if(isset($category['category'])):?>
        <option value="<?=$category['id']?>"><?=$category['category']?></option>
      <?php endif; endforeach;?>
    </select>
      <small class="text-danger" id="sub-category-alert" style="display: none;"></small>
  </div>
  <div class=" col-md-4 col-sm-12 pull-right"  id="category_fee">
     <label class="col-sm-12 control-label">Sub Category</label>
     <select class="form-control" name="category_fee" id="category_fees" required>
      <option value="">Select One ...</option>
    </select>
    <p id="Fees"></p>
  </div>
    <div class="col-md-4 col-sm-12">
        <label class="control-label">Vehicle Reg. Number</label>
        <input type="text" class="form-control" id="env_tx_vehicle_reg_no" name="vehicle_reg_no" placeholder="KAA 1122A" />
        <small class="text-info"><i class="fa fa-info-circle"></i> Required for Vehicle branding</small>
    </div>
</div>
<div class="row mb10">

 <div class="col-md-6 col-sm-12">
   <label class="col-sm-7 control-label">Art Work </label>
   <input type="file" class="form-control" id="vatcert" multiple name="Artwork[]" placeholder="Upload Actual Drawings" required/>       
 </div>
 
 
</div>     
<div id="sub">
  <input type="submit" class="btn btn-primary" value="Submit" >
</div>
<?php echo form_close(); ?>
</div><!-- panel-body -->
</div><!-- panel -->
<div class="panel panel-default col-md-3">
  <div class="panel-heading">
    <div class="panel-btns">
      <!-- <a href="#" class="panel-close">&times;</a> -->
    </div>
    <h4 class="panel-title panelx">Follow these simple steps</h4>
    <ol>
      <li>
        <p>Fill in your application details</p>
      </li>
      <li>
        <p>Confirm Details</p>
      </li>
      <li>
        <p>Make Payments</p>
      </li>
      <li>
        <p>Print Receipt</p>
      </li>
    </ol>
  </div>

</div><!-- panel -->

</div><!-- contentpanel -->


</div><!-- mainpanel -->

<script type="text/javascript">
  $(document).ready(function(){
    $("#zone").change(function(event) {
      getWards("<?=base_url('services/get_wards')?>");
    });

    $('#category_id').on('change',function(){
      if(!$(this).val()==''){
        getCategoryFees("<?=base_url('advertisement/get_category_fee_cat_id')?>/"+$(this).val());
      } else {
        $('#category_fee').hide();
      }
    });

    $('#category_fee').hide();


    //validateForm('#frm_application');
  });
</script>
