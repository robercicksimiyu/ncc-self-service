<?php
class Econstruction_model extends CI_Model{

	function prepareConstruction(){

		$username = USER_NAME;
		$key = JP_KEY;
		$timestamp = date("Y-m-d H:i:s");
		$invoiceNumber = $this->input->post('invoice') ;
		$invoiceNumber = str_replace(' ', '', $invoiceNumber);
		$jpwMobileNo = $this->session->userdata('jpwnumber');
        $agentRef = rand(0, 1000);// $timestamp;
        $ck = $username.$jpwMobileNo.$agentRef.$invoiceNumber.$key;
        
         //pass = SHA1(userName+jpwMobileNo+billNumber+agentRef+key)

        $pass = sha1(utf8_encode($ck));

        $serviceArguments = array(
        	"userName"=>$username,
        	"agentRef"=> $agentRef,
        	"invoiceNumber"=>$invoiceNumber,
        	"jpwMobileNo"=> $jpwMobileNo,
        	"pass"=> $pass

        	);


        try {

        	$client = new SoapClient(MAIN_URL, array('cache_wsdl' => WSDL_CACHE_NONE));

        } catch (Exception $e) {
        	redirect('sbp/not_found');
        }

        $result = $client->PreparePaymentWalletECNNCC($serviceArguments);
        $resultcode=$result->PreparePaymentWalletECNNCCResult->Result->ResultCode;
        $resulttext=$result->PreparePaymentWalletECNNCCResult->Result->ResultText;
        $invoiceDate=$result->PreparePaymentWalletECNNCCResult->DateOfInvoice;
        $invoiceNo=$result->PreparePaymentWalletECNNCCResult->InvoiceNumber;
        $invoiceStatus=$result->PreparePaymentWalletECNNCCResult->InvoiceStatus;
        $fullName=$result->PreparePaymentWalletECNNCCResult->UserFullName;
        $email=$result->PreparePaymentWalletECNNCCResult->UserEmail;
        $transactionId=$result->PreparePaymentWalletECNNCCResult->TransactionID;
        $totalAmount=$result->PreparePaymentWalletECNNCCResult->TotalAmount;
        $userMobile=$result->PreparePaymentWalletECNNCCResult->UserMobile;
        if ($resulttext=="OK" && $resultcode==0) {
        	$data=array(
        		'invoiceDate'=>$invoiceDate,
        		'invoiceNo'=>$invoiceNo,
        		'invoiceStatus'=>$invoiceStatus,
        		'fullName'=>$fullName,
        		'email'=>$email,
        		'transactionId'=>$transactionId,
        		'totalAmount'=>$totalAmount,
        		'userMobile'=>$userMobile,
        		'rescode'=>$resultcode,
        		'restext'=>$resulttext
        		);
        	return $data;                                                                                                                                          }else {
        		return $data = array('restext'=>$resulttext,'rescode'=>$resultcode);
        	}

        }

        function completeConstructionPayment(){
        	$invoiceNumber=$this->input->post('invoiceno');
        	$username = USER_NAME;
        	$key = JP_KEY;
        	$tranid =$this->input->post('transid');
        	$amount = $this->input->post('amount');
        	$channelRef = strtoupper(substr(md5(uniqid()),25)); 
        	$currency = "KES";
        	$jpPIN = $this->input->post('jp_pin');
        	$customer = $this->session->userdata('name');
        	$phone = $this->session->userdata('jpwnumber');
        	$channel = "SELFSERVICE";

        	$ck = $username .$tranid . $key;
        	$pass = sha1(utf8_encode($ck ));

        	$params = array(
        		"userName"=>$username,
        		"transactionId"=>$tranid,
        		"jpPIN"=>$jpPIN,     
        		"amount"=>$amount, 
        		"currency"=> $currency,
        		"channelRef"=> $channelRef,
        		"pass"=> $pass

        		);

        	try {
        		$client = new SoapClient(MAIN_URL);
        		$result = $client->CompletePaymentWalletECNNCC($params);
        	} catch (Exception $e) {
        		redirect('sbp/not_found');
        	}

        	$ResultCode = $result->CompletePaymentWalletECNNCCResult->Result->ResultCode;
        	$ResultText = $result->CompletePaymentWalletECNNCCResult->Result->ResultText;

        	if($ResultCode==0&&$ResultText=='OK'){
        		$paid = $result->CompletePaymentWalletECNNCCResult->Paid;
        		$receiptno = $result->CompletePaymentWalletECNNCCResult->ReceiptNo;

        #set in db
        		try {
        			$data=array('receiptno'=>$receiptno,'issuedate'=>date('Y-m-d h:i:s',time()),'from'=>$customer,'amount'=>$amount,'invoiceno'=>$invoiceNumber,'username' =>$phone,'cashiername' =>$customer,'channel'=>$channel);

        			#$query = "insert into `econstruction` (`receiptno`,`issuedate`,`from`,`amount`,`invoiceno`,`username`,`cashiername`,`channel`)";
        			#$query.= " values ('".implode("','",$data)."')";
        			$this->db->insert('econstruction',$data);

        		} catch (Exception $e) {}
        #end set in db
        		try {
        			$tel = $this->session->userdata('jpwnumber');
        			$amnt= number_format($paid, 2, '.', ',');
        			$msg ="Receipt Number $receiptno.Your e-Construction payment of KES $amnt for $invoiceNumber has been received by Nairobi City County.Powered by: JamboPay";
        			$key ="63bc8b3e-e674-4b08-879c-02e1aceedb8f";

        			$APIKey = urlencode($key);
        			$Phone = urlencode($tel);
        			$relayCode = urlencode("WebTribe");
        			$Message = urlencode($msg);
        			$Shortcode = urlencode("700273");
        			$CampaignId = urlencode("112623");
        			$qstr = "?APIKey=$APIKey&Phone=$Phone&Message=$Message&Shortcode=$Shortcode&CampaignId=$CampaignId&relayCode=$relayCode";
        			$ch=curl_init();
        			curl_setopt($ch,CURLOPT_URL,'http://192.168.7.61/smsServer/SendSMS.aspx'.$qstr);
        			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        			$result = curl_exec($ch);
        			curl_close($ch);

        		} catch (Exception $e) {

        		}

        		$data = array(
        			'rescode'=>$ResultCode,
        			'restext'=>$ResultText,
        			'paid'=>$paid,
        			'receiptno'=>$receiptno,
        			'customer'=>$this->session->userdata('name')
        			);
        		return $data;
        	}
        	else {
        		$data = array(
        			'rescode'=>$ResultCode,
        			'restext'=>$ResultText
        			);
        		return $data;
        	}


        }

        function PrintEConstruction($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'assets/back/receipts/E-Construction.pdf';
        $receiptno=str_replace('-', '/', $this->uri->segment(3));

        $query="select * from econstruction where receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();
        $username=$result->username;#var_dump($fileName);die();
        $receiptno=$result->receiptno;
        $date=substr($result->issuedate, 0,10); 
        $paidby=strtoupper($result->from);
        $amount=$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for="INVOICE NUMBER: ".$result->invoiceno;
        $total_amount_due=$result->amount;
        $balance_due='0.00';
        $cashier=strtoupper($result->cashiername);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

        

        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

        $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($cashier, 127, 38);
        $page->drawText($total_amount_due, 450, 300);
        $page->drawText($amount, 450, 278);
        $page->drawText($balance_due, 450, 257);
        #$page->drawText($paymenttype, 73, 20);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 552,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=E-Construction.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;

    }

    function checkECprintReceipt(){
      $invoiceno = $this->input->post('invoiceno');
      $query="Select * from econstruction where invoiceno='$invoiceno' order by issuedate DESC limit 1";
      $result= $this->db->query($query);
      if($result->num_rows()>0){
          $result = $result->row();
          return $result->receiptno;
      }else{
          return redirect("econstruction/reprintECReceipt/erro1");
      }
  }

    function printEreceiptsearch($receiptno){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =APPPATH.'/assets/back/receipts/E-Construction.pdf';
        $query="select * from econstruction where receiptno='$receiptno'";
        $result=$this->db->query($query);
        $result=$result->row();
        $username=$result->username;#var_dump($fileName);die();
        $receiptno=$result->receiptno;
        $date=substr($result->issuedate, 0,10); 
        $paidby=strtoupper($result->from);
        $amount=$result->amount;
        $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        $amount=number_format(str_replace( ',', '', $amount),2);
        $for="INVOICE NUMBER: ".$result->invoiceno;
        $total_amount_due=$result->amount;
        $balance_due='0.00';
        $cashier=strtoupper($result->cashiername);
        $total_amount_due=number_format(str_replace( ',', '', $total_amount_due),2);
        $balance_due=number_format(str_replace( ',', '', $balance_due),2);

      //   $query="select transid from chequedetails where transid=? and type='hrents'";
      //   $res=$this->db->query($query,array($result->id));
      //   if($res->num_rows()>0){
      //     $ptype="CHEQUE";
      // }else{
      //     $ptype="KSH";
      // }
      $paymenttype="PAYMENT METHOD:  KES";


      $pdf = Zend_Pdf::load($fileName);
      $page=$pdf->pages[0];

      $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

      $page->drawText($receiptno, 180, 528);
        $page->drawText($date, 450, 528);
        $page->drawText($paidby, 220, 478);
        $page->drawText($amount, 450, 478);
        $page->drawText($amount_in_words, 130, 427);
        $page->drawText($for, 150, 375); 
        $page->drawText($cashier, 127, 38);
        $page->drawText($total_amount_due, 450, 300);
        $page->drawText($amount, 450, 278);
        $page->drawText($balance_due, 450, 257);

      $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
      $rendererOptions = array(
        'topOffset' => 600,
        'leftOffset' =>295
        );
      $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();

      $pdfData = $pdf->render(); 
      header("Content-Disposition: inline; filename=receipt.pdf"); 
      header("Content-type: application/x-pdf"); 
      echo $pdfData;
  }

}





