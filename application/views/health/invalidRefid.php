<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Invalid BID<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene Details</a></li>
      <li class="active">Error</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
    	<div class="row">
    		<div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "That License ID is invalid.";
                echo anchor('health/printInstitution','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
                </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 

