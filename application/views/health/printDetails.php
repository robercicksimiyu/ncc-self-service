<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Print Institution Health Permit<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Print Institution Health Permit</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>
<?php if($health['Email']!=NULL): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">INSTITUTION HEALTH PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo $health['Refid'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON</b></td>
                        <td><?php echo $health['Name'];?></td>
                      </tr>
                      <tr>
                        <td><b>INSTITUTION NAME</b></td>
                        <td><?php echo $health['Institution_name'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT EMAIL</b></td>
                        <td><?php echo $health['Email'];?></td>
                      </tr>
                      <tr>
                        <td><b>PLOT No.</b></td>
                        <td><?php echo $health['Plot_no']; ?></td>
                      </tr>
                      <tr>
                        <td><b>MOBILE</b></td>
                        <td><?php echo $health['Mobile'];?></td>
                      </tr>
                      <tr>
                        <td><b>PURPOSE</b></td>
                        <td><?php echo $health['Purpose'];?></td>
                      </tr>
                      <tr>
                        <td><b>NATURE</b></td>
                        <td><?php echo $health['Nature'];?></td>
                      </tr>
                      <tr>
                        <td><b>ADDRESS</b></td>
                        <td><?php echo $health['Address'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td><?php if($health['Status']=="Pending"){echo "PENDING";} elseif($health['Status']=="Approved"){echo "APPROVED";};?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $health['Amount'] ;?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title panelx">Print Recommendation</h4>
        <?php if($health['Status']=="Approved"): ?>
            <p></p>
            <p> <a href="#"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
        <?php elseif($health['Status']=="Pending"): ?>
            <p></p>
            <p>Status is Pending hence Recommendation Unprintable</p>
        <?php endif; ?>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
  <?php elseif($health['Email']=NULL):  ?>
    <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
  <?php endif; ?>


