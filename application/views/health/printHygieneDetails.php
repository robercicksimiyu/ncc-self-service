<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Print Health Permit<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Print Health Permit</a></li>
      <li class="active">Print</li>
    </ol>
  </div>
</div>
<?php if($health['Email']!=NULL): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">HEALTH PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $health['Firm']; ?></td>
                        <input type="hidden" name="firm" value="<?php echo $health['Firm'];?>" />
                      </tr>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo $health['RefID'];?></td>
                        <input type="hidden" name="refid" value="<?php echo $health['RefID'];?>" />
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON</b></td>
                        <td><?php echo strtoupper($health['Fullname']);?></td>
                        <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
                      </tr>
                      <tr>
                        <td><b>EMAIL</b></td>
                        <td><?php echo $health['Email'];?></td>
                        <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
                      </tr>
                      <tr>
                        <td><b>TELEPHONE No.</b></td>
                        <td><?php echo $health['TelNumber'];?></td>
                        <input type="hidden" name="telnumber" value="<?php echo $health['TelNumber'];?>" />
                      </tr>
                      <tr>
                        <td><b>APPROVAL STATUS</b></td>
                        <td><?php if($health['Status']=="0"){echo "PENDING";} elseif($health['Status']=="1"){echo "APPROVED";} ?></td>
                        <input type="hidden" name="address" value="<?php echo $health['Status'];?>" />
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $health['Amount'] ;?></td>
                        <input type="hidden" name="amount" value="<?php echo $health['Amount'];?>" />
                      </tr>
                        </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title panelx">Print Permit</h4>
        <?php if($health['Status']=="1"): ?>
            <p></p>
            <p> <a href="<?php echo base_url(); ?>health/printHealthPermit/<?php echo $health['RefID'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
        <?php elseif($health['Status']=="0"): ?>
            <p></p>
            <p>Status is Pending hence Permit Unprintable</p>
        <?php endif; ?>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
  <?php elseif($health['Email']=NULL):  ?>
    <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
  <?php endif; ?>


