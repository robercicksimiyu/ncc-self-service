<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Permit<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Permit</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>
<?php if($health['Email']!=NULL): ?>
  <div class="contentpanel" >
    <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
      <?php $refid = $health['RefID'] ;?>
      <div class="panel-body">
        <div class="row">
          <?php echo form_open('health/completeInstitutionHealthPermitDetails',array('class' =>"form-block")); ?>
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
                <tr>
                  <th colspan="2" style="text-align:center;">INSTITUTION PERMIT DETAILS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>BUSINESS NAME</b></td>
                  <td><?php echo $health['Firm']; ?></td>
                  <input type="hidden" name="firm" value="<?php echo $health['Firm'];?>" />
                </tr>
                <tr>
                  <td><b>OWNER</b></td>
                  <td><?php echo strtoupper($health['Fullname']);?></td>
                  <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
                  <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
                  <input type="hidden" name="telnumber" value="<?php echo $health['TelNumber'];?>" />
                  <input type="hidden" name="certificate" value="<?php echo $health['certificate'];?>" />
                            <!-- <input type="hidden" name="plotno" value="<?php echo $health['Plot_no'];?>" />
                            <input type="hidden" name="lrno" value="<?php echo $health['LRNO'];?>" /> -->
                          </tr>
                          <tr>
                            <td><b>LICENSE ID</b></td>
                            <td><?php echo $health['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $health['RefID'];?>" />
                          </tr>
                          <tr>
                            <td><b>INVOICE NUMBER</b></td>
                            <td><?php echo $health['InvoiceNo'];?></td>
                            <input type="hidden" name="InvoiceNo" value="<?php echo $health['InvoiceNo'];?>" />
                          </tr>
                          <!-- <tr>
                            <td><b>HEALTH CERTIFICATE AMOUNT</b></td>
                            <td><?php echo $health['certificate'] ;?></td>
                            <input type="hidden" name="certificate" value="<?php echo $health['certificate'];?>" />
                          </tr> -->
                          <tr>
                            <td><b>INSTITUTION PERMIT FEE</b></td>
                            <td><?php echo $health['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $health['Amount'];?>" />
                          </tr>
                          <tr>
                              <td><b>TOTAL AMOUNT PAYABLE</b></td>
                              <td><?php echo $health['total'] ;?></td>
                              <input type="hidden" name="total" value="<?php echo $health['total'];?>" />
                          </tr>
                          <tr>
                            <td><b>STATUS</b></td>
                            <td><?php if($health['status']=="0"){
                              echo "WAITING APPROVAL";
                            }else{
                              echo "APPROVED" ;
                            }
                            ?></td>
                            <input type="hidden" name="status" value="<?php echo $health['status'];?>" />
                          </tr>
                          <!-- <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            Your Application was successful,please wait for a notification after inspection.
                          </div> -->
                        </tbody>
                      </table>
                    </div><!-- table-responsive -->

                    <div class="panel-footer">
                      <?php if($health['status']=="1") : ?>
                      <div class="row">
                        <?php 
                        echo'<div class="col-sm-6">';
                        echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
                        echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
                            echo'</div>';
                        echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('health/completeHealthPermitDetails','Cancel',array('class'=>'btn btn-primary'));
                        ?>
                        <?php echo form_close(); ?>
                      </div>
                    <?php elseif($health['status']=="0"): ?>
                    <div class="row">
                    </div>
                    <?php endif;?>
                    </div>
                  </div>

                </div>

              </div><!-- contentpanel -->



              <div class="panel panel-default col-md-3" >
                <div class="panel-heading" style="text-align:center;">
                  <div class="panel-btns">

                  </div>
                  <h4 class="panel-title panelx">Notice</h4>
                  <?php if($health['status']=="1"): ?>
                  <p></p>
                  <p>Enter your e-Wallet PIN</p>
                  <p>Click to proceed with payment</p>
                <?php elseif($health['status']=="0"): ?>
                <p></p>
                <p>Approval Status is Pending hence you cannot Proceed to pay</p>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div><!-- mainpanel 1346861-->
    <?php elseif($health['Email']=NULL):  ?>
    <div class="alert alert-info">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
              <?php endif; ?>


