<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Food Hygiene<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
    <?php $refid = $health['RefID']; ?>
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php echo form_open('health/completeHealthPermitDetails',array('class' =>"form-block")); ?>
    		<div class="table-responsive">
    			<table class="table table-striped mb30">
    				<thead>
    					<tr>
    						<th colspan="2" style="text-align:center;">APPLICATION DETAILS</th>
    					</tr>
    				</thead>
    				<tbody>
                        <tr>
                            <td><b>BUSINESS NAME</b></td>
                            <td><?php echo $health['Firm']; ?></td>
                            <input type="hidden" name="firm" value="<?php echo $health['Firm'];?>" />
                        </tr>
                        <tr>
                            <td><b>CONTACT PERSON</b></td>
                            <td><?php echo strtoupper($health['Fullname']);?></td>
                            <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
                            <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
                            <input type="hidden" name="telnumber" value="<?php echo $health['TelNumber'];?>" />
                            <!-- <input type="hidden" name="plotno" value="<?php echo $health['Plot_no'];?>" />
                            <input type="hidden" name="lrno" value="<?php echo $health['LRNO'];?>" /> -->
                        </tr>
    					<tr>
    						<td><b>LICENSE ID</b></td>
    						<td><?php echo $health['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $health['RefID'];?>" />
    					</tr>
                        <tr>
                            <td><b>INVOICE NUMBER</b></td>
                            <td><?php echo $health['InvoiceNo'];?></td>
                            <input type="hidden" name="InvoiceNo" value="<?php echo $health['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                            <td><b>HEALTH CERTIFICATE AMOUNT</b></td>
                            <td><?php echo $health['certificate'] ;?></td>
                            <input type="hidden" name="certificate" value="<?php echo $health['certificate'];?>" />
                        </tr>
                        <tr>
                            <td><b>FOOD HYGIENE PERMIT FEE</b></td>
                            <td><?php echo $health['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $health['Amount'];?>" />
                        </tr>
                        <tr>
                            <td><b>TOTAL AMOUNT PAYABLE</b></td>
                            <td><?php echo $health['total'] ;?></td>
                            <input type="hidden" name="total" value="<?php echo $health['total'];?>" />
                        </tr>
    					<tr>
    						<td><b>STATUS</b></td>
    						<td><?php if($health['status']=="0"){
                                echo "WAITING APPROVAL";
                            }else{
                                echo "APPROVED" ;
                            }
                            ?></td>
                            <input type="hidden" name="status" value="<?php echo $health['status'];?>" />
    					</tr>
                        <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    Your Application was successful,please wait for a notification after inspection.
                  </div>
                </tbody>
            </table>
        </div><!-- table-responsive -->
    </div>
      
      <?php 
            // echo'<div class="col-sm-6">';
            // echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
            // echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
            //     echo'</div>';
            // echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
            // echo " ";
            // echo anchor('health/completeHealthPermitDetails','Cancel',array('class'=>'btn btn-primary'));
            ?>
      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
  <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <h4 class="panel-title panelx">Follow these steps</h4>
        <p>Click below to print the Invoice which you will use to pay</p>
        <p></p>
        <p><a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $refid;?>"><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a></p>
    </div>
</div>
</div><!-- contentpanel --> 

