 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Food Hygiene License <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Food Hygiene</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Enter Business ID </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('health/displayHealthPermitDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
            </div>
          </div>
          <div class="row mb10">
            
            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="Enter Full names" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter Phone no." required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required/>
            </div>            
            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Category</label>
              <select class="form-control input-sm mb15 chosen-select" name="category" id="category" data-placeholder="Select Category...">
                <option value="Hotel">Hotel</option>
                <option value="Restaurant">Restaurant</option>
                <option value="Manufacturer">Manufacturer</option>
                <option value="Wholesaler/Wine and spirits">Wholesaler/Wine and spirits</option>
                <option value="Wholesaler Shop">Wholesale Shop</option>
                <option value="Retail grocery shop">Retail grocery shop</option>
                <option value="Supermarket">Supermarket</option>
                <option value="Distributor">Distributor</option>
                <option value="Packaging">Packaging</option>
                <option value="Fish and chips">Eating House / Fish & Chips</option>
                <option value="Cafes">Cafes</option>
                <option value="Members club">Proprietary Liquor / Members club</option>
              </select>
            </div>
            <!-- <div class="col-sm-5">
              <label class="col-sm-5 control-label">Sub-County</label>
              <select class="form-control chosen-select" id="subcounty" name="subcounty" data-placeholder="Select Sub County...">
                        <option value=""></option>
                        <?php foreach($subcounty as $key=>$value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                        <?php } ?>
                  </select>
            </div>
            <div class="col-sm-5">
              <label class="col-sm-5 control-label">Ward</label>
              <div id="ward">
                <select class="form-control chosen-select" id="ward" name="ward" data-placeholder="Choose Sub County First..." required>
                  <option selected="selected" disabled="disabled" value=""></option>
                </select>
              </div>
            </div> -->
            <!-- <div class="col-sm-2" id="loader">
              <br><label class="col-sm-6 control-label"></label>
              <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
            </div> -->
          </div>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    // $('#loader').hide();
    // $('#subcounty').on('change',function(e){
    //   $('#loader').show();
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     $('#loader').hide();
    //     $('#ward').html(data);
    //     jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    //     if(document.getElementById("ward") === null){
    //       $('#sub').hide();
    //     }else{
    //       $('#sub').show();
    //     }

    //   });

    // });
  });
  </script>



