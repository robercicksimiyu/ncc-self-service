<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Food Hygiene<span>Print Permit</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>
<?php $rescode = $health['rescode'];?>
<?php if($rescode=="200"): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;"><div class="alert alert-success" style="padding-top:10px;margin-left:20px;">
                          <button type="button" class="close" data-dismiss="success" aria-hidden="true"></button>
                          <?php echo "Permit has successfully been paid.";?>
                        </div>
                      </th>
                    </tr>
                      <tr>
                        <th colspan="2" style="text-align:center;">PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                <tr>
                  <td><b>BUSINESS NAME</b></td>
                  <td><?php echo $health['Firm']; ?></td>
                  <input type="hidden" name="firm" value="<?php echo $health['Firm'];?>" />
                </tr>
                <tr>
                  <td><b>CONTACT PERSON</b></td>
                  <td><?php echo strtoupper($health['Fullname']);?></td>
                  <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
                  <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
                  <input type="hidden" name="Tel_no" value="<?php echo $health['Tel_no'];?>" />
                            <!-- <input type="hidden" name="plotno" value="<?php echo $health['Plot_no'];?>" />
                            <input type="hidden" name="lrno" value="<?php echo $health['LRNO'];?>" /> -->
                          </tr>
                          <tr>
                            <td><b>LICENSE ID</b></td>
                            <td><?php echo $health['Refid'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $health['Refid'];?>" />
                          </tr>
                          <tr>
                            <td><b>INVOICE NUMBER</b></td>
                            <td><?php echo $health['InvoiceNo'];?></td>
                            <input type="hidden" name="InvoiceNo" value="<?php echo $health['InvoiceNo'];?>" />
                          </tr>
                          <!-- <tr>
                            <td><b>CERTIFICATE</b></td>
                            <td><?php echo $health['certificate'] ;?></td>
                            <input type="hidden" name="certificate" value="<?php echo $health['certificate'];?>" />
                          </tr> -->
                          <tr>
                            <td><b>AMOUNT PAID</b></td>
                            <td><?php echo $health['total'] ;?></td>
                            <input type="hidden" name="total" value="<?php echo $health['total'];?>" />
                          </tr>
                        <!-- <tr>
                            <td><b>ADDRESS</b></td>
                            <td><?php echo $health['Address'] ;?></td>
                            <input type="hidden" name="address" value="<?php echo $health['Address'];?>" />
                          </tr> -->
                          <!-- <tr>
                            <td><b>STATUS</b></td>
                            <td><?php if($health['status']=="0"){
                              echo "WAITING APPROVAL";
                            }else{
                              echo "APPROVED" ;
                            }
                            ?></td>
                            <input type="hidden" name="status" value="<?php echo $health['status'];?>" />
                          </tr> -->
                          <!-- <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            Your Application was successful,please wait for a notification after inspection.
                          </div> -->
                        </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Print Permit</h4>
            <p></p>
            <p> <a href="<?php echo base_url(); ?>health/printHealthPermit/<?php echo $health['Refid'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $health['Refid'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
<?php elseif($rescode=="401"): ?>
<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Permit is already Paid.";
                echo anchor('health/payHygiene','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
                </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 
<?php endif;?>


