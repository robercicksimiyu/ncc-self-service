<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Institution Health License<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Institution Health Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >

  <div class="panel panel-default col-md-10" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title">Fill in This Form</h4>
    </div>
    <div class="panel-body">
      <?php echo form_open('health/displayInstitutionHealthPermitDetails',array('class' =>"form-block")); ?>

      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Full Name</label>
          <input type="text" name="full_names" id="full_names" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Institution Name</label>
          <input type="text" name="institution_name" id="institution_name" class="form-control" placeholder="" />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Address</label>
          <input type="text" name="address" id="address" class="form-control" placeholder="" required />
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Email</label>
          <input type="text" name="email" id="email" class="form-control" placeholder="" />
        </div>

        <div class="col-sm-4">
          <label class="control-label">Plot No</label>
          <input type="text" name="plot_no" id="plot_no" class="form-control" placeholder="" />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Road</label>
          <input type="text" name="road" id="road" class="form-control" placeholder="" required />
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Mobile</label>
          <input type="text" name="mobile" id="mobile" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Purpose</label>
          <input type="text" name="purpose" id="purpose" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Nature</label>
          <input type="text" name="nature" id="nature" class="form-control" placeholder=""  />
        </div>
      </div>

      <div id="proc">
        <input type="submit" class="btn btn-primary" value="Submit">
      </div>
      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 

