<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Institution Health License<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Institution Health Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>
<?php $rescode = $health['rescode'];?>
<?php if($rescode=="200"): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;"><div class="alert alert-success" style="padding-top:10px;margin-left:20px;">
                          <button type="button" class="close" data-dismiss="success" aria-hidden="true"></button>
                          <?php echo "Permit has successfully been paid.";?>
                        </div>
                      </th>
                    </tr>
                      <tr>
                        <th colspan="2" style="text-align:center;">INSTITUTION HEALTH PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <tr>
                        <td><b>License ID</b></td>
                        <td><?php echo $health['RefID'];?></td>
                      </tr>
                      <!-- <tr>
                        <td><b>FULL NAMES</b></td>
                        <td><?php echo $health['Fullname'];?></td>
                      </tr> -->
                      <tr>
                        <td><b>INSTITUTION NAME</b></td>
                        <td><?php echo $health['InstitutionName'];?></td>
                      </tr>
                      <tr>
                        <td><b>EMAIL ADDRESS</b></td>
                        <td><?php echo $health['Email'];?></td>
                      </tr>
                      <tr>
                        <td><b>PLOT No.</b></td>
                        <td><?php echo $health['PlotNo']; ?></td>
                      </tr>
                      <tr>
                        <td><b>MOBILE</b></td>
                        <td><?php echo $health['Mobile'];?></td>
                      </tr>
                      <tr>
                        <td><b>PURPOSE</b></td>
                        <td><?php echo $health['Purpose'];?></td>
                      </tr>
                      <tr>
                        <td><b>NATURE</b></td>
                        <td><?php echo $health['Nature'];?></td>
                      </tr>
                      <tr>
                        <td><b>ADDRESS</b></td>
                        <td><?php echo $health['Address'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td><?php echo $health['Status'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $health['Amount'] ;?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Print Receipt</h4>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printInstitutionHealthPermit/<?php echo $health['RefID'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
            <p> <a href="<?php echo base_url(); ?>health/printInstitutionHealthPermitreceipt/<?php echo $health['RefID'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
  <?php elseif($rescode=="400"): ?>
<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
          <?php echo "Permit is already Paid.";
          echo anchor('health/InstitutionHealthApplicationForm','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
          ?>
        </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 
<?php endif;?>


