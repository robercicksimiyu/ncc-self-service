 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Institution Health License <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Institution Health</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
  

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Enter Business ID </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('health/displayInstitutionHealthPermitDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
          <div class="row mb10">
            <div class="col-sm-4">
              <label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
            </div>
            <div class="col-sm-7">
              <label class="col-sm-4 control-label">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" />
            </div>
            <div class="col-sm-4">
              <br><label class="col-sm-6 control-label">Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter Phone no." required />
            </div>
            <div class="col-sm-5">
              <br><label class="col-sm-7 control-label">Owner Full Names</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="Enter Full names" required />
            </div>
            <!-- <div class="col-sm-3">
              <br><label class="col-sm-6 control-label">Category</label>
              <select class="form-control input-sm mb15" name="category" id="category" data-placeholder="Select Category...">
                <option value="Hotel">Hotel</option>
                <option value="Restaurant">Restaurant</option>
                <option value="Manufacturer">Manufacturer</option>
                <option value="Wholesaler/Wine and spirits">Wholesaler/Wine and spirits</option>
                <option value="Wholesale Shop">Wholesale Shop</option>
                <option value="Retail grocery shop">Retail grocery shop</option>
                <option value="Supermarket">Supermarket</option>
                <option value="Distributor">Distributor</option>
                <option value="Packaging">Packaging</option>
                <option value="Fish and chips">Eating House / Fish & Chips</option>
                <option value="Cafes">Cafes</option>
                <option value="Members club">Proprietary Liquor / Members club</option>
              </select>
            </div> -->

          </div>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

