 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Fire Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Fire Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Fill in the form below </h4>
          <p>Cross check to make sure you have filled in the correct Location</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('fire/displayFirePermitDetailsBuilding',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")); ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Building Name</label>
              <input type="text" class="form-control" id="building" name="building" placeholder="Enter Building Name" required />
            </div>
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Physical Location</label>
              <input type="text" class="form-control" id="location" name="location" placeholder="Enter Location Name" required />
            </div>
          </div>
          <div class="row mb10">
            
            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="Enter Full names" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter Phone no." required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required/>
            </div>            
            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Category</label>
              <select class="form-control input-sm mb15 chosen-select" name="category" id="category" data-placeholder="Select Category...">
                <!-- <option value="14">Medium Trader Shop/Retail Service</option>
                <option value="15">Small Trader Shop/Retail Service</option>
                <option value="16">Hyper-supermarket</option>
                <option value="17">Other General Merchant Shop and Retail Service</option>
                <option value="22">Plant Industry</option>
                <option value="23">Medium Petrol Filling Station</option>
                <option value="24">Small Petrol Filling Station</option>
                <option value="25">Large Petrol Filling Station</option>
                <option value="26">Commercial and industrial premises/offices</option>
                <option value="27">Premises storing dangerous inflammable materials</option> -->
                <option value="29">High Rise Buildings-Up to 10 th floor</option>
                <option value="30">High Rise Buildings-Ten floors and above</option>
                <option value="31">High Rise Buildings-Up to 4th floor</option>
                <!-- <option value="32">Inspection of petrol tanker</option>
                <option value="33">Sale & service of fire equipment</option> -->
              </select>
            </div>

            <!-- <div class="col-sm-5">
              <label class="col-sm-5 control-label">Sub-County</label>
              <select class="form-control chosen-select" id="subcounty" name="subcounty" data-placeholder="Select Sub County...">
                        <option value=""></option>
                        <?php foreach($subcounty as $key=>$value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                        <?php } ?>
                  </select>
            </div>
            <div class="col-sm-5">
              <label class="col-sm-5 control-label">Ward</label>
              <div id="ward">
                <select class="form-control chosen-select" id="ward" name="ward" data-placeholder="Choose Sub County First..." required>
                  <option selected="selected" disabled="disabled" value=""></option>
                </select>
              </div>
            </div> -->
            <!-- <div class="col-sm-2" id="loader">
              <br><label class="col-sm-6 control-label"></label>
              <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
            </div> -->
          </div>
          <div class="row">
              <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Plot No.</label>
                <input type="text" class="form-control" id="plotno" name="plotno" placeholder="Enter Plot No." required />
              </div>
              <div class="col-md-6 col-sm-12">
                <br><label class="col-sm-5 control-label">Land Rate No.</label>
                <input type="text" class="form-control" id="lrno" name="lrno" placeholder="Enter LR No." required/>
              </div>
            </div>
            <br>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Location Details</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  });
  </script>



