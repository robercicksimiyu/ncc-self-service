  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Fire Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Fire Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>

<div class="contentpanel" >
    <?php $refid = $fire['RefID']; ?>
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php echo form_open('fire/prepareFirePayment',array('class' =>"form-block")); ?>
    		<div class="table-responsive">
    			<table class="table table-striped mb30">
    				<thead>
    					<tr>
    						<th colspan="2" style="text-align:center;">APPLICATION DETAILS</th>
    					</tr>
    				</thead>
    				<tbody>
                        <tr>
                            <td><b>BUSINESS NAME</b></td>
                            <td><?php echo $fire['Firm']; ?></td>
                            <input type="hidden" name="firm" value="<?php echo $fire['Firm'];?>" />
                        </tr>
                        <tr>
                            <td><b>CONTACT PERSON</b></td>
                            <td><?php echo strtoupper($fire['Fullname']);?></td>
                            <input type="hidden" name="fullname" value="<?php echo $fire['Fullname'];?>" />
                            <input type="hidden" name="email" value="<?php echo $fire['Email'];?>" />
                            <input type="hidden" name="telnumber" value="<?php echo $fire['TelNumber'];?>" />
                        </tr>
    					<tr>
    						<td><b>LICENSE ID</b></td>
    						<td><?php echo $fire['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $fire['RefID'];?>" />
    					</tr>
                        <tr>
                            <td><b>INVOICE NUMBER</b></td>
                            <td><?php echo $fire['InvoiceNo'];?></td>
                            <input type="hidden" name="InvoiceNo" value="<?php echo $fire['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                            <td><b>FIRE INSPECTION PERMIT FEE</b></td>
                            <td><?php echo $fire['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $fire['Amount'];?>" />
                        </tr>
                        <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    Your Application was successful, print your invoice and proceed to pay.
                  </div>
                </tbody>
            </table>
        </div><!-- table-responsive -->
        <div class="panel-footer">
                      <div class="row">
                        <?php
                        echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
                        echo "    ";
                        echo anchor('fire/firePermitApplicationForm','Cancel',array('class'=>'btn btn-primary'));
                        ?>
                        <?php echo form_close(); ?>
                      </div>
         </div>
    </div>

      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
  <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <h4 class="panel-title panelx">Follow these steps</h4>
        <p>Click below to print the Invoice which you will use to pay</p>
        <p></p>
        <p><a href="<?php echo base_url(); ?>fire/printFirePermitreceipt/<?php echo $refid;?>"><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a></p>
    </div>
</div>
</div><!-- contentpanel --> 

