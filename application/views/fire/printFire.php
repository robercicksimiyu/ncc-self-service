  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Print Fire Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Fire Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Enter License No. </h4>
          <p>Cross check to make sure you have filled in the correct License ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('fire/printFireDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-8">
                      <label class="col-sm-4 control-label">LICENSE ID</label>
                        <input type="text" class="form-control" id="refid" name="refid" placeholder="Enter License No" required />
                      </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit" >
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Enter your License Identification Number</p></li>
		   <li>
          <p>Check Submit</p></li>
		    </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

