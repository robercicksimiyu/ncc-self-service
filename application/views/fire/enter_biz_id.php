 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Fire Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Fire Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Fill in the form below </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('fire/displayFirePermitDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")); ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
            </div>
          </div>
          <div class="row mb10">

            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="Enter Full names" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter Phone no." required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Category</label>
              <select class="form-control input-sm mb15 chosen-select" name="category" id="category" data-placeholder="Select Category...">

                <?php foreach($categories['bizactivity'] as $key => $value): ?>
                  <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                <?php endforeach; ?>

              </select>
            </div>

            <!-- <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Subcounty</label>
              <input type="text" class="form-control" id="subcounty" name="subcounty" placeholder="Enter Subcounty" required/>
            </div> -->

            <div class="col-md-6 col-sm-12 <?=(form_error('subcounty')) ? 'has-error' : ''?>">
              <label class="col-md-12 col-sm-12 control-label" style="margin-top: 5%;">Sub County</label>
              <?php if(!isset($subcounties)){?>
              <input type="text" name="subcounty"  id="subcounty" class="form-control" placeholder="Sub-county" value="<?=set_value('subcounty')?>"  />
              <?php } else {
                  echo '<select class="form-control input-sm mb15 chosen-select" name="zonecode" id="zonecode">';
                  echo '<option value="">Select Subcounty</option>';
                    foreach($subcounties as $county): ?>
                    <option value="<?php echo $county->ID; ?>"><?php echo $county->Name; ?></option>
                    <?php endforeach;?>
                  <?php echo '</select>';
                  echo '<input type="hidden" id="subcounty" name="subcounty" class="form-control" required/>';
                }?>
              <small><?php echo form_error('subcounty'); ?></small>
            </div>

            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Zone</label>
              <select class="form-control input-sm mb15 chosen-select" id="zone_id" name="zone_id" required>
                <option value="">Select Zone</option>
                <?php
                  $incr = 0;
                  for($i = 0; $i <= 56; $i++) {
                    echo '<option value="' .$zones[$i + $incr] .'">' .$zones[$i + $incr + 1] .'</option>';
                    $incr ++;
                  }
                ?>
                <input type="hidden" id="zone" name="zone" value='' readonly />
              </select>
            </div>
            <!-- <div class="col-md-6 col-sm-12">
              <label class="col-md-12 col-sm-12 control-label">Sub-County</label>
              <select class="form-control chosen-select" id="subcounty" name="subcounty">
                        <option value="">Select Sub-County</option>
                        <?php foreach($subcounty as $key=>$value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                        <?php } ?>
                  </select>
            </div> -->

            <!-- <div class="col-md-6 col-sm-12">
               <label class="col-md-12 col-sm-12 control-label">Ward</label> -->
              <!-- <div id="ward">

              </div>
            </div> -->

          </div><!-- row -->
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
        <div id="biz_details"></div>
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){

    code=$("#zonecode").find('option:selected').text();
    $('#subcounty').val(code);

    $('#zonecode').on('change',function(){
    code=$(this).find('option:selected').text();
    $('#subcounty').val(code);

    $("#zone_id").on('change', function (){
      var selectedText = $("#zone_id option:selected").text();
      $("#zone").val(selectedText);
    });

    $('#biz_id').on('input',function(){
      //Category select box
      $.ajax({
        url:"<?=base_url('fire/getBusinessDetails')?>",
        type:'POST',
        data:{biz_id:$(this).val(),year:"<?=date('Y')?>"},

        success:function(data){
            // console.log(data);
            var obj = JSON.parse(data);
            // console.log(obj.rescode);
            if (obj.rescode == 0){
              $('select#category').val(obj.activitycode);
              $('select#category').children('option').hide();
              $('#phoneno').val(obj.phoneno);
            } else {
              $('select#category').children('option').show();
              $('#phoneno').val('');
            }
        }
      });
    });

    // $('#subcounty').on('change',function(e){
    //
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     // $('#loader').hide();
    //     $('#ward').html(data);
    //     // jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    //
    //     // if(document.getElementById("ward") === null){
    //     //   $('#sub').hide();
    //     // }else{
    //     //   $('#sub').show();
    //     // }
    //
    //   });
    //
    // });

  });

  </script>
