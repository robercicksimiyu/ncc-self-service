<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Permit<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Permit</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>
<?php if($fire['Email']!=NULL): ?>
  <div class="contentpanel" >
    <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
      <?php $refid = $fire['RefID'] ;?>
      <div class="panel-body">
        <div class="row">
          <?php echo form_open('fire/prepareFirePayment',array('class' =>"form-block")); ?>
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
                <tr>
                  <th colspan="2" style="text-align:center;">FIRE INSPECTION PERMIT DETAILS</th>
                </tr>
              </thead>
                      <tbody>
                        <tr>
                          <td><b>BUSINESS NAME</b></td>
                          <td><?php echo $fire['Firm']; ?></td>
                          <input type="hidden" name="firm" value="<?php echo $fire['Firm'];?>" />
                        </tr>
                        <tr>
                          <td><b>CONTACT PERSON</b></td>
                          <td><?php echo strtoupper($fire['Fullname']);?></td>
                          <input type="hidden" name="fullname" value="<?php echo $fire['Fullname'];?>" />
                          <input type="hidden" name="email" value="<?php echo $fire['Email'];?>" />
                          <input type="hidden" name="telnumber" value="<?php echo $fire['TelNumber'];?>" />
                        </tr>
                        <tr>
                          <td><b>LICENSE ID</b></td>
                          <td><?php echo $fire['RefID'];?></td>
                          <input type="hidden" name="refid" value="<?php echo $fire['RefID'];?>" />
                        </tr>
                        <tr>
                          <td><b>INVOICE NUMBER</b></td>
                          <td><?php echo $fire['InvoiceNo'];?></td>
                          <input type="hidden" name="InvoiceNo" value="<?php echo $fire['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                          <td><b>FIRE INSPECTION PERMIT FEE</b></td>
                          <td><?php echo $fire['Amount'] ;?></td>
                          <input type="hidden" name="amount" value="<?php echo $fire['Amount'];?>" />
                        </tr>
                      </tbody>
                      </table>
                    </div><!-- table-responsive -->

                    <div class="panel-footer">
                      <div class="row">
                        <?php
                        echo '<input type="submit" id="bill_status"  value="Proceed to Make Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('#','Cancel',array('class'=>'btn btn-primary','onClick'=>'goBack()'));
                        ?>
                        <?php echo form_close(); ?>
                      </div>
                    </div>
                  </div>

                </div>

              </div><!-- contentpanel -->



              <div class="panel panel-default col-md-3" >
                <div class="panel-heading" style="text-align:center;">
                  <h4 class="panel-title panelx">Follow these steps</h4>
                  <p>Enter your e-wallet PIN</p>
                  <p>Click Proceed to make payment</p>
                  <p></p>
                </div>
              </div>
        </div>
      </div><!-- mainpanel 1346861-->
    <?php elseif($fire['Email']=NULL):  ?>
    <div class="alert alert-info">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
              <?php endif; ?>

  <script>
function goBack() {
    window.history.back();
}
</script>


