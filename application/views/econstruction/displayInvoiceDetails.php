<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>e-Construction Fee<span>Pay for your e-Construction Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">e-Construction</a></li>
      <li class="active">Display Invoice Details</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      $rescode= $construction['rescode'];
      #$construction['invoiceStatus'] = "Paid";

      if($rescode=="0" && $construction['invoiceStatus']!="paid"):

      ?>
      
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('econstruction/completeConstructionPayment',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONSTRUCTION DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $construction['invoiceNo'];?></td>
                        <input type="hidden" id="transid" name="transid" value="<?php echo $construction['transactionId'];?>"/>
                        <input type="hidden" id="amount" name="amount" value="<?php echo $construction['totalAmount'];?>"/>
                        <input type="hidden" id="invoiceno" name="invoiceno" value="<?php echo $construction['invoiceNo'];?>"/>
                        <input type="hidden" id="fullname" name="fullname" value="<?php echo $construction['fullName'];?>"/>
                        <input type="hidden" id="email" name="email" value="<?php echo $construction['email'];?>"/>
                      </tr>
                      <tr>
                        <td><b>FULL NAME</b></td>
                        <td><?php echo strtoupper($construction['fullName']);?></td>
                      </tr>
                      <tr>
                        <td><b>EMAIL</b></td>
                        <td><?php echo $construction['email'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE DATE</b></td>
                        <td><?php echo $construction['invoiceDate'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE STATUS</b></td>
                        <td><?php echo strtoupper($construction['invoiceStatus']);?></td>
                      </tr>
                      <tr>
                        <td><b>TOTAL AMOUNT</b></td>
                        <td><?php echo number_format($construction['totalAmount'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('econstruction','Cancel',array('class'=>"btn btn-default")); ?>
                </div>


              </div>
        </div>
      
    </div><!-- contentpanel -->


    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed are correct</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
    
  </div><!-- mainpanel -->
<?php elseif($rescode=="0" && $construction['invoiceStatus']="paid" ): ?>

      <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('econstruction/completeConstructionPayment',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONSTRUCTION DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $construction['invoiceNo'];?></td>
                        <input type="hidden" id="transid" name="transid" value="<?php echo $construction['transactionId'];?>"/>
                        <input type="hidden" id="amount" name="amount" value="<?php echo $construction['totalAmount'];?>"/>
                        <input type="hidden" id="invoiceno" name="invoiceno" value="<?php echo $construction['invoiceNo'];?>"/>
                        <input type="hidden" id="fullname" name="fullname" value="<?php echo $construction['fullName'];?>"/>
                        <input type="hidden" id="email" name="email" value="<?php echo $construction['email'];?>"/>
                      </tr>
                      <tr>
                        <td><b>FULL NAME</b></td>
                        <td><?php echo strtoupper($construction['fullName']);?></td>
                      </tr>
                      <tr>
                        <td><b>EMAIL</b></td>
                        <td><?php echo $construction['email'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE DATE</b></td>
                        <td><?php echo $construction['invoiceDate'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE STATUS</b></td>
                        <td><?php echo strtoupper($construction['invoiceStatus']);?></td>
                      </tr>
                      <tr>
                        <td><b>TOTAL AMOUNT</b></td>
                        <td><?php echo number_format($construction['totalAmount'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <!-- <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('econstruction','Cancel',array('class'=>"btn btn-default")); ?>
                </div> -->
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The Permit for this Invoice Number is already Paid "; ?> <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <?php echo anchor('econstruction','Back',array('class'=>"btn btn-default")); ?>

              </div>
        </div>
      
    </div><!-- contentpanel -->

              
<?php elseif($rescode=="4440" || $rescode=="44405" || $rescode=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
<?php elseif($rescode=="1110"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid Invoice Number"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="1097"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Costing for the given Zone Code not found"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="2023"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry.your Account has been suspended"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="2030"): ?>
                      <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('construction/completePaymentDaily',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">constructionELLANEOUS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BILL NUMBER</b></td>
                        <td><?php echo $construction['BillNumber'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL PAYER</b></td>
                        <td><?php echo $construction['BillPayer'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL STATUS</b></td>
                        <td><?php echo $construction['BillStatus'];?></td>
                      </tr>
                      <tr>
                        <td><b>DATE ISSUED</b></td>
                        <td><?php echo $construction['DateIssued'];?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo number_format($construction['TotalAmount'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Sorry.you do not have sufficient funds in your wallet to continue with the transaction"; ?>
                    <a href="<?php echo base_url();?>index.php/selfservice/wallettopup" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-money"></i>Top Up Wallet</a>
                  </div>
                </div>


              </div>
        </div>
      
    </div> 
<?php endif; ?>