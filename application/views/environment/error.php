 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >
		<div class="panel panel-dark">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="" class="panel-close">×</a>
                <a href="" class="minimize">−</a>
              </div><!-- panel-btns -->
              <h3 class="panel-title">Panel Title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

<script type="text/javascript">
$(document).ready(function(){
  $("#zone").change(function(event) {
    getWards("<?=base_url('services/get_wards')?>");
  });
  validateForm('#frm_application');
});
</script>
