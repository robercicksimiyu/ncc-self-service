 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >
		<div class="col-md-9 col-sm-12 col-xs-12">
			<div class="panel panel-dark">
				<div class="panel-heading">				  
				  <h3 class="panel-title">Application Form</h3>
				</div>
				<div class="panel-body">
				  <?php echo form_open('environment/process',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm_application")) ?>         
					  <div class="row mb10">            
						<div class="col-md-6 col-sm-12">
						  <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
						  <input type="text" class="form-control" id="env_tx_fullname" name="Fullname" placeholder="John Doe" required />
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Contact Email</label>
						  <input type="email" class="form-control" id="env_tx_email" name="Email" placeholder="you@example.com" required/>
						</div> 
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-12 control-label">Contact Phone No.</label>
						  <input type="text" class="form-control" id="env_tx_phoneno" name="Tel_no" placeholder="0700000000" required />
						</div>   
						 <div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Category</label>
						  <select class="form-control" id="Category_id" name="category_id">
							<?php foreach($categories as $category):?>
							  <option value="<?=$category['category_id']?>"><?=$category['description']?></option>
							<?php endforeach;?>
						  </select>              
						</div> 
						</div>
						<div class="row mb10">            
						<div class=" col-md-12 col-sm-12">
						  <label class="col-sm-6 control-label">Description</label>
						  <textarea class="form-control" id="env_tx_description" name="Description" placeholder="Write a short description e.g Hiring ground for raising funds for the needy" required></textarea>
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Sub Counties</label>
						  <select class="form-control" name="zone" id="zone" required>
								<option value="">Select one ...</option>  
								<?php foreach($subcounties as $county): ?>

								  <?php if($county['ID']==$invoice_details['zone']):?>
									   <option data-bid="<?=$county['Bid']?>" selected="selected" value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
								  <?php else:?>
									   <option value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
								  <?php endif;?>
								 
								<?php endforeach;?> 
							</select>
						 </div>
						 <div class=" col-md-6 col-sm-12">
						   <label class="col-sm-6 control-label">Wards</label>
						   <select class="form-control" name="Subcounty" id="ward" required >
							<option value="">Select Sub County</option>
						   </select>
						 </div>
						 <div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Duration</label>
						  <input type="text" class="form-control" id="env_tx_duration" name="Duration" placeholder="eg. No. of Hours, Day, Week, Month, Year" required/>
						</div> 

					  
								 
					  <!--   <div class="col-md-6 col-sm-12">
							<label class=" col-sm-5 control-label">Start Date</label> -->
						   <!--  <input type="hidden" class="form-control" id="datepicker" name="Start_date" value="<?=date('Y-m-d')?>" placeholder="" required/> -->
					   <!--  </div> -->
					 
						<input type="hidden" name="Form_Id" value="<?=$form?>">
					</div>     
				  <div id="sub">
					<input type="submit" class="btn btn-primary" value="Submit" >
				  </div>
				 <?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12">
			<div class="panel panel-dark">
            <div class="panel-heading">             
              <h3 class="panel-title">Panel Title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
		</div>		

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

<script type="text/javascript">
$(document).ready(function(){
  $("#zone").change(function(event) {
    getWards("<?=base_url('services/get_wards')?>");
  });
  validateForm('#frm_application');
});
</script>
