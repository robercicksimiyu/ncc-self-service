<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Miscellaneous<span>Pay for your Miscellaneous online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Miscellaneous</a></li>
          <li class="active">Display Miscellaneous Details</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" >

      <?php 

      if(!isset($misc['ErrorCode'])):

      ?>
      
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('miscellaneous/completeMiscPayment',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">MISCELLANEOUS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>ACCOUNT NAME</b></td>
                        <td><?php echo $misc['Names'];?></td>
                        <input type="hidden" id="TransactionID" name="TransactionID" value="<?php echo $misc['TransactionID'];?>"/>
                        <input type="hidden" id="Amount" name="Amount" value="<?php echo $misc['Amount'];?>"/>
                        <input type="hidden" id="BillDetail" name="BillDetail" value="<?php echo $misc['BillDetail'];?>"/>
                        <input type="hidden" id="BillNumber" name="BillNumber" value="<?php echo $misc['BillNumber'];?>"/>
                      </tr>
                      <tr>
                        <td><b>BILL PAYER</b></td>
                        <td><?php echo $misc['BillPayer'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL STATUS</b></td>
                        <td><?php echo $misc['BillStatus'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL NUMBER</b></td>
                        <td><?php echo $misc['BillNumber'];?></td>
                      </tr>
                      <tr>
                        <td><b>TRANSACTION ID</b></td>
                        <td><?php echo $misc['TransactionID'];?></td>
                      </tr>
                      <tr>
                        <td><b>ISSUE DATE</b></td>
                        <td><?php echo $misc['IssueDate'];?></td>
                      </tr>
                      <tr>
                        <td><b>ACCOUNT DESCRIPTION</b></td>
                        <td><?php echo $misc['AccountDescription'];?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo number_format($misc['Amount'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('miscellaneous','Cancel',array('class'=>"btn btn-default")); ?>
                </div>


              </div>
        </div>
      
    </div><!-- contentpanel -->



	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Confirm that all the details displayed are correct</p></li>
		   <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
		    </ol>
        </div>
    
  </div><!-- mainpanel -->
<?php else: ?>
    <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      <?php echo $misc['ErrorCode'].": ".$misc['Message']; ?>
      <a href="<?php echo base_url();?>miscellaneous" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
    </div>

<?php endif; ?>