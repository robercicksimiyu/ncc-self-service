<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Miscellaneous<span>Pay for your Miscellaneous online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Miscellaneous</a></li>
          <li class="active">Complete Miscellaneous Payment</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="row">
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment</h4>
              </div>
              <?php $response = isset($misc['ErrorCode'])?$misc['ErrorCode']:'0';
              
                if (!isset($misc['ErrorCode'])): ?>
                  <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Your Transaction was Successful";
                    #$receiptno=str_replace('/','-',$complete['receiptno']);
                    echo anchor('miscellaneous/miscellaneousreceiptprint/'.$misc['billno'],'Print Receipt',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>

                <?php elseif($response=="4440" || $response=="44405" || $response=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
              
              <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered a Wrong Wallet PIN"; ?>
                <a href="<?php echo base_url();?>index.php/cess" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php elseif($response=="1053"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The transaction has already been completed."; ?>
                <a href="<?php echo base_url();?>index.php/cess" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php else: ?>
                <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo $misc['Message']; ?>
                <a href="<?php echo base_url();?>miscellaneous" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div><!-- contentpanel -->

    

