<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Miscellaneous Fees<span>Pay for your Miscellaneous Fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Miscellaneous</a></li>
          <li class="active">Select Bill No.</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Enter Bill No</h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('miscellaneous/displayMiscDetails',array('class' =>"form-block")) ?>
            
            <div class="form-group">
              <div class="col-sm-8">
                <h4 class="subtitle mb5">Bill Number</h4>
                <input type="text" id="billno" name="billno" class="form-control" required/>
              </div>
            </div>
            <div id="proc">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Enter the Bill Number</p></li>
            <li><p>Click Submit</p></li>
          </ol>
        </div>

      </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
      // $(document).ready(function(){
      //   jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

      //   $('#cessCode').on('change',function(){
      //     code=$(this).find('option:selected').text();
      //     $('#cessDescription').val(code);
      //         //alert(code);
      //       });
      // });
    </script>
