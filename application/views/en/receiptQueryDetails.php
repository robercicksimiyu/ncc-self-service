 <div class="pageheader">
      <h2><i class="fa fa-th"></i>Receipt Details<span>Receipt Query</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li class="active">Receipt Details</li>
        </ol>
      </div>
    </div>

<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php if($receiptDetails['rescode']=="0"):?>
            <div class="alert alert-danger col-md-8" style="margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Receipt Number Not Found!"; ?>
                <a href="<?php echo base_url();?>selfservice/receiptQuery" class="btn btn-primary receipt" style="float:right;margin-top:-5px;"><i class="fa fa-mail-reply"></i>Back</a>
          </div>
        <?php elseif($receiptDetails['rescode']=="1"):?>
                <div class="table-responsive">
                 <table class="table table-striped mb30">
                    <thead>
                       <tr>
                          <th colspan="2" style="text-align:center;">Transaction Details</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td><b>REVENUE STREAM</b></td>
                        <td><?php echo $receiptDetails['stream'];?></td>
                    </tr>
                    <tr>
                      <td><b>TRANSACTION DETAIL</b></td>
                      <td><?php echo $receiptDetails['transactiondetail'];?></td>
                    </tr>
                    <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $receiptDetails['amount']; ?></td>
                    </tr>
                    <tr>
                      <td><b>RECEIPT NUMBER</b></td>
                      <td><?php echo $receiptDetails['receiptno'];?></td>
                    </tr>
                    <tr>
                      <td><b>TRANSACTION DATE</b></td>
                      <td><?php echo $receiptDetails['date'];?></td>
                    </tr>
                    <tr>
                      <td><b>TRANSACTION ID</b></td>
                      <td><?php echo $receiptDetails['transactionid'];?></td>
                    </tr>
                  </tbody>
                 </table>
                </div><!-- table-responsive -->
    <?php endif;?>
    </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
  <div class="panel panel-default col-md-3" >
    <div class="panel-heading" style="text-align:center;">
      <?php if($receiptDetails['rescode']=="1"):?>
      <h4 class="panel-title panelx">Print Receipt Details</h4>
      <p></p>
      <p><a href="<?php echo base_url(); ?>selfservice/printReceiptQueryDetails/<?php echo $receiptDetails['transactionid'];?>" target="_blank"><input type="submit" value="Print" class="btn btn-primary btn-lg" style="line-height:normal"></a></p>
      <?php elseif($receiptDetails['rescode']=="0"):?>
      <h4 class="panel-title panelx">Thank you for using ejijipay!</h4>
      <?php endif;?>
    </div>
  </div>
</div><!-- contentpanel --> 

