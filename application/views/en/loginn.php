<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url();?>application/assets/back/images/favicon.ico" type="image/png">
  <title>NCC Self Service Portal</title>
  <link href="<?php echo base_url();?>application/assets/back/css/section.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/application.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/style.default.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/emomentum.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body onload="display_ct();">
  <div data-part="breadcrumbs-navbar" id="helper-navbar" class="row fixed-nav-bar top breadcrumbs-navbar">
    <div class="col-xs-12 welcome-nav">
      <div class="row">
        <div class="col-xs-offset-1 col-xs-12 col-sm-8 col-md-8 col-lg-8">
          <div class="container"> <small class="no-padding no-margin bread">
            <ol class="breadcrumb" bla="no-margin no-padding">
              <li><a href="http://epayments.nairobi.go.ke">Home</a> </li>
              <li class="active">ePayments</li>
              <li class="active">Log In</li>
            </ol>
          </small> </div>
        </div>
      </div>
    </div>
  </div>

  <div class="signin-info">
    <div class="logopanel" style="text-align:center">
      <h1> <img src="<?php echo base_url();?>application/assets/back/images/ncc.png" style="background:none;"><span>Nairobi County Self Service Portal</span></h1>
    </div>
  </div>
</div>

<!-- Preloader -->
<div id="preloader">
  <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<div class="container ">
  <div class='row'>
    <div class="sidebarpanel col-xs-12 col-md-6  hidden-xs hidden-sm " style="padding:0px;">
      <div class="row">
        <h2 class="panel-title">Download from Google Play</h2>
        <ol>
          <li style="">
            <p style="font-size: 14px; margin-left:30px;"> <a href="https://play.google.com/store/apps/details?id=labs.avanaya.jp.ncc" target="_blank">Android Phone App </a> <a style="margin-left:-30px;" href="https://play.google.com/store/apps/details?id=labs.avanaya.jp.ncc" target="_blank"><img src="<?php echo base_url();?>application/assets/back/images/google-play.jpg" width="190px;" /> </a></p>
          </li>
        </ol>
      </p>
      <h2 class="panel-title">Download from Windows Phone Store</h2>
        <ol>
          <li style="">
            <p style="font-size: 14px; margin-left:30px;"><a href="http://www.windowsphone.com/en-us/store/app/nairobi-city-county-epayments/bc082aa3-49bc-44fe-84d1-22fd5e371add" target="_blank">Windows Phone App </a> <a style="margin-left:-30px;" href="http://www.windowsphone.com/en-us/store/app/nairobi-city-county-epayments/bc082aa3-49bc-44fe-84d1-22fd5e371add" target="_blank"><img src="<?php echo base_url();?>application/assets/back/images/windows.png" width="190px;" /> </a></p>
          </li>
        </ol>
      <!-- <p>&nbsp;</p> --> 
      <h2 style="margin-top:0px;" class="panel-title">USSD available on Safaricom & Airtel on *217#</h2>
      <h2 style="margin-top:0px;" class="panel-title">Customer care - 0709920000</h2>
      <!-- <ol>
        <li>
          <p style="font-size: 14px;"><img src="<?php echo base_url();?>application/assets/back/images/ussd.jpg" width="" /></p>
        </li>
      </ol> -->
    </div>
  </div>
  <div class="sidebarpanel col-xs-12  col-md-6" style="padding:0px;">
    <div class="signinpanel ">
      <div class="row">
        <div class="col-xs-offset-0 col-xs-12 col-md-9 col-md-offset-1">
          <form method="post" action="<?php echo base_url();?>selfservice/userLogin">
            <h4 class="nomargin">Sign In</h4>
            <h5 style="text-align:center; color:red;">
              <?php if ($this->session->flashdata('msg') != ''): echo $this->session->flashdata('msg'); 
              endif; ?>
            </h5>
            <p class="mt5 mb20">Login to access your account.</p>
            <label class="col-sm-8 control-label">Mobile Phone No:</label>
            <input type="text" class="form-control uname" placeholder="Mobile Phone No." name="cust_phone" required />
            <label class="col-sm-4 control-label">PIN:</label>
            <input type="password" class="form-control pword" placeholder="PIN" name="password" required/>
            <button class="btn btn-success btn-block">Sign In</button>
            <!-- <h5 style="text-align:center;"><a href="#">Forgot Your Pin?</a></h5> -->
            <p>&nbsp;</p>
            <h5 style="text-align:center;"><strong>Not Registered? <a href="<?php echo base_url();?>selfservice/registerNew">Sign Up</a></strong><br/><br/><a href="<?php echo base_url();?>selfservice/forgotPassword">Forgot Password</a></h5>
          </form>
        </div>
        <!-- col-sm-7 --> 

      </div>
      <!-- row -->

      <div class="signup-footer" style="">
        <div> &copy; 2014. All Rights Reserved. Nairobi City County &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered By: <a href="http://jambopay.com/" target="_blank"><img src="<?php echo base_url();?>application/assets/back/images/jplogo.png"/></a> </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- signin -->

</div>
<div role="nav-menu" data-part="fixed-footer-nav-menu" id="navigation-bar" class="row fixed-nav-bar bottom"> 
  <!--The menu icon-->
  <div class="col-xs-1 hidden-xs hidden-sm text-align-center"> <a href="http://nairobi.go.ke" class="yellow"> <i class="fa fa-home fa-3x yellow"></i> </a> </div>
  <!-- Time and weather-->
  <div class="col-md-2 hidden-xs hidden-sm text-center yellow-bg">
    <h2 id="clockss" class="moment-in-time grey no-padding"></h2>
    <small class="grey"> Nairobi.</small> </div>
    <!-- The navigation menu-->
    <div class="col-sm-6 col-md-5 hidden-xs">
      <div class="row"> 
        <!-- Previous scroller-->
        <div class="col-xs-1">
          <p class="arrow"> <i id="nav-menu-prev" class="fa fa-angle-left fa-2x pull-left white-arrow">&nbsp;</i> </p>
        </div>
        <!-- The nav menu-->
        <div class="col-xs-10 uppercase">
          <div id="nav-menu" class="nav-menu text-align-center">
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/governors-profile/" cla> <i class="fa fa-star-o fa-2x"></i>
              <p style="font-size:12px;">Governor&#039;s Profile</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/services/" cla> <i class="fa fa-globe fa-2x"></i>
              <p style="font-size:12px;">Services</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/index.php/home/history/" cla> <i class="fa fa-file fa-2x"></i>
              <p style="font-size:12px;">History</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/explore-nairobi/" cla> <i class="fa fa-search fa-2x"></i>
              <p style="font-size:12px;">Explore</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/opportunities/" cla> <i class="fa fa-files-o fa-2x"></i>
              <p style="font-size:12px;">Tenders</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/common-city-laws-and-regulations/" cla> <i class="fa fa-check-square-o fa-2x"></i>
              <p style="font-size:12px;">City ByLaws</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/news-and-updates/" cla> <i class="fa fa-microphone fa-2x"></i>
              <p style="font-size:12px;">News</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/whats-happening/" cla> <i class="fa fa-calendar fa-2x"></i>
              <p style="font-size:12px;">Events</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/downloads/" cla> <i class="fa fa-money fa-2x"></i>
              <p style="font-size:12px;">Downloads</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/visitors/" cla> <i class="fa fa-share-square fa-2x"></i>
              <p style="font-size:12px;">Visitors</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/index.php/home/maps/" cla> <i class="fa fa-map-marker fa-2x"></i>
              <p style="font-size:12px;">Maps</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/sectors/" cla> <i class="fa fa-hospital-o fa-2x"></i>
              <p style="font-size:12px;">Sectors</p>
            </a> </div>
          </div>
        </div>
        <!-- Next Scroller-->
        <div class="col-xs-1">
          <p class="arrow"> <i id="nav-menu-next" class="fa fa-angle-right fa-2x pull-right white-arrow">&nbsp;</i> </p>
        </div>
      </div>
    </div>
    <!-- Emergency Services and social-icons-->
    <div class="col-sm-6 col-md-4">
      <div class="row inherit height">
        <div class="col-xs-6 col-sm-7 inherit height">
          <div id="social-icons">
            <div class="row text-center">
              <div class="col-xs-4"> <a class="blue" target="_blank" href="https://www.facebook.com/pages/Nairobi-City-County-Official/569756036404745"> <i class="fa fa-facebook-square fa-2x"></i> </a> </div>
              <div class="col-xs-4"> <a class="blue" target="_blank" href="https://twitter.com/county_nairobi"> <i class="fa fa-twitter fa-2x"></i> </a> </div>
              <div class="col-xs-4"> <a class="blue" target="_blank" href="#email-form"> <i class="fa fa-envelope fa-2x"></i> </a> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-5 no-padding no-margin emergency-services-bar">
          <a href="http://nairobi.go.ke/emergency-services"><div class="row">
            <div class="col-xs-4"> <i class="fa fa-bell fa-2x fa-borer">&nbsp;</i> </div>
            <div class="col-xs-8">
              <p class="uppercase no-margin text-justify"> <span>Emergency Services</span> </p>
            </div>
          </div></a>
        </div>
      </div>
    </div>
  </div>
  
<script src="<?php echo base_url();?>application/assets/back/js/jquery-1.10.2.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery-migrate-1.2.1.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/modernizr.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/retina.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/custom.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/application.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/execute.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery-ui-1.10.3.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.sparkline.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/toggles.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.cookies.js"></script>
<script type="text/javascript">
 function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
    }

    function display_ct() {
      var strcount
      var x = new Date()
      //var x1=x.getMonth() + "/" + x.getDate() + "/" + x.getYear(); 
      var x1 =x.getHours( )+ ":" +  (x.getMinutes()<10?'0':'') + x.getMinutes();
      document.getElementById('clockss').innerHTML = x1;

      tt=display_c();
    }
</script>
</body>
</html>
