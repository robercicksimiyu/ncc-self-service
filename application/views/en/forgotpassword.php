<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url();?>application/assets/back/images/favicon.ico" type="image/png">
  <title>NCC Self Service Portal</title>
  <link href="<?php echo base_url();?>application/assets/back/css/section.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/application.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/style.default.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/emomentum.css" rel="stylesheet">


</head>

<body onload="display_ct();">
  <div data-part="breadcrumbs-navbar" id="helper-navbar" class="row fixed-nav-bar top breadcrumbs-navbar">
    <div class="col-xs-12 welcome-nav">
      <div class="row">
        <div class="col-xs-offset-1 col-xs-12 col-sm-8 col-md-8 col-lg-8">
          <div class="container"> <small class="no-padding no-margin bread">
            <ol class="breadcrumb" bla="no-margin no-padding">
              <!-- <li>Home </li> -->
              <li class="active"><a href="<?php echo base_url();?>">ePayments</a></li>
              <li class="active">Forgot PIN</li>
            </ol>
          </small> </div>
        </div>
      </div>
    </div>
  </div>

  <div class="signin-info">
    <div class="logopanel" style="text-align:center">
      <h1> <img src="<?php echo base_url();?>application/assets/back/images/ncc.png" style="background:none;"><span>Nairobi County Self Service Portal</span></h1>
    </div>
  </div>
</div>

<!-- Preloader -->
<div id="preloader">
  <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<div class="container ">
  <div class='row'>
    <div class="sidebarpanel col-xs-12 col-md-6  hidden-xs hidden-sm ">
      <div class="row">
        <h2 class="panel-title">Follow the instructions below to reset your PIN</h2>
        <ol>
          <li style="">
            <p style="font-size: 14px;">Call Jambopay customer care through - <a href="#" target="_blank"> 0709920000 </a> <a style="margin-left:30px;" href="" target="_blank"></a></p>
            <p style="font-size: 14px;">Wait for a validation code from JamboPay</p>
            <p style="font-size: 14px;">Enter the Validation code in the right hand side of this form</p>
            <p style="font-size: 14px;">Enter the New PIN you want to set</p>
            <p style="font-size: 14px;">Confirm the new PIN and Click Reset</p>
          </li>
        </ol>
      </p>
    </div>
  </div>

  <div class="sidebarpanel col-xs-12  col-md-6">
    <div class="signinpanel ">
      <div class="row">
        <div class="col-xs-offset-0 col-xs-12 col-md-9 col-md-offset-1">
          <form method="post" action="">
            <div class="row mb10">
                      <div class="col-sm-8">
                      <label class="col-sm-8 control-label">Mobile Phone Number</label>
                        <input type="text" class="form-control" id="jpwnumber" name="jpwnumber" placeholder="Phone Number" required />
                      </div>
                      <div class="col-sm-8">
                      <label class="col-sm-8 control-label">Validation Code</label>
                        <input type="password" class="form-control" id="validationcode" name="validationcode" placeholder="Validation Code" required />
                      </div>
                      <div class="col-sm-8">
                      <label class="col-sm-8 control-label">New PIN</label>
                        <input type="password" class="form-control" id="newpin" name="newpin" placeholder="Enter New PIN" required />
                      </div>
                      <div class="col-sm-8">
                      <label class="col-sm-8 control-label">Confirm New PIN</label>
                        <input type="password" class="form-control" id="newpin2" name="newpin2" placeholder="Confirm New PIN" required />
                      </div>
            </div>
            <button type="submit" id="update" class="btn btn-success btn-block" style="width:150px;">Reset</button>
            <br/>
            <div class="alert alert-info aler">
            </div>
          </form>
        </div>
        <!-- col-sm-7 --> 

      </div>
      <!-- row -->

      <div class="signup-footer" style="">
        <div> &copy; 2014. All Rights Reserved. Nairobi City County &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered By: <a href="http://jambopay.com/" target="_blank"><img src="<?php echo base_url();?>application/assets/back/images/jplogo.png"/></a> </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- signin -->

</div>
<div role="nav-menu" data-part="fixed-footer-nav-menu" id="navigation-bar" class="row fixed-nav-bar bottom"> 
  <!--The menu icon-->
  <div class="col-xs-1 hidden-xs hidden-sm text-align-center"> <a href="http://nairobi.go.ke" class="yellow"> <i class="fa fa-home fa-3x yellow"></i> </a> </div>
  <!-- Time and weather-->
  <div class="col-md-2 hidden-xs hidden-sm text-center yellow-bg">
    <h2 id="clockss" class="moment-in-time grey no-padding"></h2>
    <small class="grey"> Nairobi.</small> </div>
    <!-- The navigation menu-->
    <div class="col-sm-6 col-md-5 hidden-xs">
      <div class="row"> 
        <!-- Previous scroller-->
        <div class="col-xs-1">
          <p class="arrow"> <i id="nav-menu-prev" class="fa fa-angle-left fa-2x pull-left white-arrow">&nbsp;</i> </p>
        </div>
        <!-- The nav menu-->
        <div class="col-xs-10 uppercase">
          <div id="nav-menu" class="nav-menu text-align-center">
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/governors-profile/" cla> <i class="fa fa-star-o fa-2x"></i>
              <p style="font-size:12px;">Governor&#039;s Profile</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/services/" cla> <i class="fa fa-globe fa-2x"></i>
              <p style="font-size:12px;">Services</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/index.php/home/history/" cla> <i class="fa fa-file fa-2x"></i>
              <p style="font-size:12px;">History</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/explore-nairobi/" cla> <i class="fa fa-search fa-2x"></i>
              <p style="font-size:12px;">Explore</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/opportunities/" cla> <i class="fa fa-files-o fa-2x"></i>
              <p style="font-size:12px;">Tenders</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/common-city-laws-and-regulations/" cla> <i class="fa fa-check-square-o fa-2x"></i>
              <p style="font-size:12px;">City ByLaws</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/news-and-updates/" cla> <i class="fa fa-microphone fa-2x"></i>
              <p style="font-size:12px;">News</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/whats-happening/" cla> <i class="fa fa-calendar fa-2x"></i>
              <p style="font-size:12px;">Events</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/downloads/" cla> <i class="fa fa-money fa-2x"></i>
              <p style="font-size:12px;">Downloads</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/visitors/" cla> <i class="fa fa-share-square fa-2x"></i>
              <p style="font-size:12px;">Visitors</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/index.php/home/maps/" cla> <i class="fa fa-map-marker fa-2x"></i>
              <p style="font-size:12px;">Maps</p>
            </a> </div>
            <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/sectors/" cla> <i class="fa fa-hospital-o fa-2x"></i>
              <p style="font-size:12px;">Sectors</p>
            </a> </div>
          </div>
        </div>
        <!-- Next Scroller-->
        <div class="col-xs-1">
          <p class="arrow"> <i id="nav-menu-next" class="fa fa-angle-right fa-2x pull-right white-arrow">&nbsp;</i> </p>
        </div>
      </div>
    </div>
    <!-- Emergency Services and social-icons-->
    <div class="col-sm-6 col-md-4">
      <div class="row inherit height">
        <div class="col-xs-6 col-sm-7 inherit height">
          <div id="social-icons">
            <div class="row text-center">
              <div class="col-xs-4"> <a class="blue" target="_blank" href="https://www.facebook.com/pages/Nairobi-City-County-Official/569756036404745"> <i class="fa fa-facebook-square fa-2x"></i> </a> </div>
              <div class="col-xs-4"> <a class="blue" target="_blank" href="https://twitter.com/county_nairobi"> <i class="fa fa-twitter fa-2x"></i> </a> </div>
              <div class="col-xs-4"> <a class="blue" target="_blank" href="#email-form"> <i class="fa fa-envelope fa-2x"></i> </a> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-5 no-padding no-margin emergency-services-bar">
          <a href="http://nairobi.go.ke/emergency-services"><div class="row">
            <div class="col-xs-4"> <i class="fa fa-bell fa-2x fa-borer">&nbsp;</i> </div>
            <div class="col-xs-8">
              <p class="uppercase no-margin text-justify"> <span>Emergency Services</span> </p>
            </div>
          </div></a>
        </div>
      </div>
    </div>
  </div>
  
<script src="<?php echo base_url();?>application/assets/back/js/jquery-1.10.2.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery-migrate-1.2.1.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/modernizr.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/retina.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/custom.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/application.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/execute.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery-ui-1.10.3.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.sparkline.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/toggles.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.cookies.js"></script>
 

<script type="text/javascript"> 
    function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
    }

    function display_ct() {
      var strcount
      var x = new Date()
      //var x1=x.getMonth() + "/" + x.getDate() + "/" + x.getYear(); 
      var x1 =x.getHours( )+ ":" +  (x.getMinutes()<10?'0':'') + x.getMinutes();
      document.getElementById('clockss').innerHTML = x1;

      tt=display_c();
    }
</script>

  <script type="text/javascript">
    $(document).ready(function(){
     $('.aler').hide()
      json='';
      $('#update').on('click',function(e){
         e.preventDefault(); 
         if ($('#newpin').val().length<4 && $('#newpin2').val().length<4) {
            $('.aler').show()
            $('.aler').html("Pin cannot be less than 4 characters")
         }else{
          //alert('done');
        $.post('<?php echo base_url();?>user/changePin',{
          jpwnumber:$('#jpwnumber').val(),
          validationcode:$('#validationcode').val(),
          newpin:$('#newpin').val(),
          newpin2:$('#newpin2').val()
        },function(data){
          json=JSON.parse(data);
                    if(json.err==0) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
                    if(json.err==1) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
                    if(json.err==2) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
        });}
      });
    });
  </script>

</body>
</html>
