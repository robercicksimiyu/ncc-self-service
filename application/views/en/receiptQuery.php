 <div class="pageheader">
      <h2><i class="fa fa-th"></i>Validate Receipt<span>Receipt Query</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li class="active">Receipt Query</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
          </div>
          <h4 class="panel-title">Kindly Enter your Receipt No.</h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('selfservice/receiptQueryDetails',array('class' =>"form-block")) ?>
            <div class="form-group">
              <label class="control-label">Receipt No.</label>
              <input type="text" size="100" class="form-control" id="receiptno" name="receiptno"  placeholder="Enter Receipt No" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Query">
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      
    </div><!-- contentpanel -->