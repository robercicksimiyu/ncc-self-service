  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Loading Zones <span>Pay your Loading Zones Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Loading Zones</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default col-md-8">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                              
                 
                  <div class="table-responsive">
                  <?php echo form_open('transport_infrustructure/payment'); ?>
                   <?php if($details['ref_id']!="400"):?>
                    <?php if($details['trans_status']!=1):?>
                    <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">PLEASE CONFIRM BEFORE COMPLETING THE TRANSACTION</th>
                      </tr>
                    </thead>
                    <tbody>                     
                      <tr>
                        <td><b>REFERENCE ID</b></td>
                        <td><?=$details['ref_id'];?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS ID</b></td>
                        <td><?=$details['Bid'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON NAME</b></td>
                        <td><?=$details['Fullname'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON EMAIL</b></td>
                        <td><?=$details['Email'];?></td>
                      </tr>

                      <tr>
                        <td><b>LOCATION</b></td>
                        <td><?=$details['Location'];?></td>
                      </tr>              
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?=$details['InvoiceNum'];?></td>
                      </tr>
                      <tr>
                        <td>                        
                          <input type="hidden" value="<?=$details['amount']?>" name="amount" id="amount" />
                          <input type="hidden" value="<?php echo $details['InvoiceNum'];?>" name="invoice_number" id="transid" />
                          <input type="hidden" value="<?php echo $details['ref_id'];?>" name="ref_id" id="transid" />
                          <input type="hidden" value="<?php echo $details['form_id'];?>" name="form_id" id="bid" />
                          <input type="hidden" value="<?php echo $details['year'];?>" name="year" id="year" />                         
                        </td>
                      </tr>
                      <tr>
                        <td><strong>TOTAL</strong></td>
                        <td>
                        <?php                                           
                            echo 'KES. '.number_format( $details['amount'], 2, '.', ',');
                            
                          ?>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                  <?php else:?>
                    <div class="alert alert-info">
                      <p>Thank you for submitting your application. You will be notified once the application is approved. Use <code><strong><?=strtoupper($details['ref_id'])?> </strong></code>as a reference number to make payments. <br> <a href="<?=base_url('transport_infrustructure/pay')?>" class="btn btn-primary btn-sm">Pay for Loading Zone</a></p>
                    </div>
                  <?php endif;?>

                  </div><!-- table-responsive -->
                  
              </div>
            </div><!-- panel-body -->
           
            <div class="panel-footer">
                  
                <?php if($details['trans_status']!=1):?>
                  <div class="col-sm-6">
                      <input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>
                  </div>
                  <input class="btn btn-primary" type="submit" value="Make Payment">
                  <input class="btn btn-primary" type="button" value="Back" class="backLink">
                  </div>
                <?php endif;?>
                <?php else: ?>
                    <div class="alert alert-warning"> 
                      <?=$details['message']?>
                    </div>
                  <?php endif;?>
             


           
            
            <?php echo form_close();?>
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->