 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Enter Details </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('transport_infrustructure/confirm/bus_shelter',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm","enctype"=>"multipart/form-data")) ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="Bid" placeholder="Enter Business ID" required />
            </div>
          </div>
          <div class="row mb10">

            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="Fullname" placeholder="eg. John Doe" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-12 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="Tel_no" placeholder="eg 0700000000" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="Email" placeholder="eg. you@example.com" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Bus Shelter Slots</label>
              <input type="text" class="form-control" id="Busshelter_slots" name="Busshelter_slots" placeholder="eg. 20" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Actual Drawings</label>
              <input type="file" class="form-control" id="vatcert" multiple name="userfile[]" placeholder="Upload Actual Drawings" required/>
              
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Drawing Design</label>
              <input type="file" class="form-control" id="vatcert" multiple name="userfile[]" placeholder="Upload Drawing Design" required/>
            </div>
            <div class="col-md-12 col-sm-12">
              <br><label class="col-sm-5 control-label">Description</label>
              <textarea class="form-control" id="description" name="Description" placeholder="Write a small description" required></textarea>   
            </div>
            
            <input type="hidden" name="category_id" value="<?=$category?>">

            <div class="col-sm-12 col-md-6">
              <label class="control-label">Sub County</label>
              <select class="form-control" name="zones" id="zones" required >               
                    <?php foreach($subcounties as $county): ?>
                            <option value="<?php echo $county->ID; ?>"><?php echo $county->Name; ?></option>
                    <?php endforeach;?>
            
              </select>
            </div>
            <div class="col-sm-12 col-md-6">
              <label class="control-label">Ward</label>
              <select class="form-control" name="ward" id="ward" required >
                
              </select>
            </div>

            
              <div class="col-sm-12 col-md-6">
                <label class="control-label">Mapped_location</label>
                <input class="form-control" name="Mapped_location" id="location" required >             
              </div> 

              <div class="col-sm-12 col-md-6">
                <label class="control-label">Location</label>
                <input class="form-control" name="Location" id="location" required >             
              </div>   

             
            

            
            <input type="hidden" name="Form_Id" value="<?=$form?>">
          
          </div>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
    $(document).ready(function(){
      jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
      // Date Picker
      jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
      
      jQuery('#datepicker-inline').datepicker();
      
      jQuery('#datepicker-multiple').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    });
  </script>

<script type="text/javascript">
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];

               $('#ward').append($('<option>', {
                "value": id
              }).text(name));
               console.log(id + " " + name);
             }
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
