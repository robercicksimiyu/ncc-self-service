 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Loading Zone <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Loading Zones</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
    <!-- API_Id:
Bid:
Location:
Fullname:
Email:
Tel_no:
Parking_slots:
Description:
Start_date:
Subcounty:
zone:
Form_Id:
category_id: -->

    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Enter Business ID </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('transport_infrustructure/confirm',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
            </div>
          </div>
          <div class="row mb10">

            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="eg. John Doe" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-12 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="eg 0700000000" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="eg. you@example.com" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Parking Slots</label>
              <input type="text" class="form-control" id="parking_slots" name="parking_slots" placeholder="eg. 20" required/>
            </div>
            <div class="col-md-12 col-sm-12">
              <br><label class="col-sm-5 control-label">Description</label>
              <textarea class="form-control" id="parking_slots" name="parking_slots" placeholder="eg. 20" required></textarea>   
            </div>
            
            <input type="hidden" name="category" value="<?=$category?>">
            <!-- <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Subcounty</label>
              <input type="text" class="form-control" id="subcounty" name="subcounty" placeholder="Enter Subcounty" required/>
            </div> -->

            <div class="col-md-6 col-sm-12 <?=(form_error('subcounty')) ? 'has-error' : ''?>">
              <label class="col-md-12 col-sm-12 control-label" style="margin-top: 5%;">Sub County</label>
              <?php if(!isset($subcounties)){?>
              <input type="text" name="subcounty"  id="subcounty" class="form-control" placeholder="Sub-county" value="<?=set_value('subcounty')?>"  />
              <?php } else {
                  echo '<select class="form-control input-sm mb15 chosen-select" name="zonecode" id="zonecode">';
                  echo '<option value="">Select Subcounty</option>';
                    foreach($subcounties as $county): ?>
                    <option value="<?php echo $county->ID; ?>"><?php echo $county->Name; ?></option>
                    <?php endforeach;?>
                  <?php echo '</select>';
                  echo '<input type="hidden" id="subcounty" name="subcounty" class="form-control" required/>';
                }?>
              <small><?php echo form_error('subcounty'); ?></small>
            </div>

            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Zone</label>
              <select class="form-control input-sm mb15 chosen-select" id="zone_id" name="zone_id" required>
                <option value="">Select Zone</option>
                <?php
                  $incr = 0;
                  for($i = 0; $i <= 56; $i++) {
                    echo '<option value="' .$zones[$i + $incr] .'">' .$zones[$i + $incr + 1] .'</option>';
                    $incr ++;
                  }
                ?>
                <input type="hidden" id="zone" name="zone" value='' readonly />
              </select>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Start Date</label>
              <input type="text" class="form-control" id="start_date" name="start_date" placeholder="eg. 20/12/2016" required/>
            </div>
            <input type="hidden" name="form" value="<?=$form?>">
           

          </div>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){

    $("#zone_id").on('change', function (){
      var selectedText = $("#zone_id option:selected").text();
      console.log(selectedText);
      $("#zone").val(selectedText);
    });

    

    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    $('#loader').hide();

    $('#subcounty').on('change',function(e){
      $('#loader').show();
      $.post('<?php echo base_url();?>health/getWards',
      {
        subcounty:$('#subcounty').val()
      },
      function(data){
        $('#loader').hide();
        $('#ward').html(data);
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        if(document.getElementById("ward") === null){
          $('#sub').hide();
        }else{
          $('#sub').show();
        }

      });

    });

  });
  </script>
