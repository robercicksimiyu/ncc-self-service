<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Loading Zones<span>Pay for your Loading Zones Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Loading Zones</a></li>
      <li class="active">Display Invoice Details</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      //$rescode= $construction['rescode'];
      #$construction['invoiceStatus'] = "Paid";

      //if($rescode=="0" && $construction['invoiceStatus']!="paid"):

      ?>      
       <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <?php  if(!isset($invoice_details['Fullname'])):?>
                <div class="alert alert-warning">
                  <?=$invoice_details['message']?>      <button class="btn btn-primary pull-right" onclick="goBack()">Back</button>
                </div>
              <?php else:?>
              <div class="row">
                  <?php echo form_open('transport_infrustructure/complete_payment',array('class' =>"form-block")) ?>
                  <?php foreach($invoice_details as $key=>$value):?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>">
                  <?php endforeach;?>
                  <div class="table-responsive">

                    <?php if(isset($invoice_details['form_status'])): if($invoice_details['form_status']!=0):?>
                    <table class="table table-striped mb30">
                      <thead>
                        <tr>
                          <th colspan="2" style="text-align:center;">INVOICE DETAILS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><b>Invoice Number</b></td>
                          <td><?=$invoice_details['InvoiceNum']?></td>
                        </tr>
                        <tr>
                          <td><b>Invoice Amount</b></td>
                          <td><?=$invoice_details['amount']?></td>
                        </tr>                      
                         <tr>
                          <td><b>Contact Person</b></td>
                          <td><?=$invoice_details['Fullname']?></td>
                        </tr>
                        
                        <tr>
                          <td><b>Contact Email</b></td>                       
                          <td>
                            <p><?=$invoice_details['Email']?></p>
                          </td>                   
                        </tr>
                        <tr>
                          <td><b>Business ID</b></td>
                          <td><?=$invoice_details['Bid'];?></td>                      
                        </tr>
                        <tr>
                          <td><b>Location</b></td>
                          <td>
                               <?=$invoice_details['Location'];?>                     
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <div class="panel-footer">
                       <div class="col-sm-6">
                       <input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>
                       </div>
                       <input class="btn btn-primary" type="submit" value="Make Payment">
                       <input class="btn btn-primary" type="button" value="Back" class="backLink">
                   </div>
                    <?php else: ?>
                      <div class="alert alert-warning">
                        <p>The application has not yet been approved</p>
                      </div>
                    <?php endif; else:?>
                       <div class="alert alert-warning">
                        <p>The invoice number does not exist.</p>
                      </div>
                    <?php endif;?>
                  </div><!-- table-responsive -->
        
              </div>         
            <?php endif;?>
          
            
       
      
    </div> 
<script type="text/javascript">
function goBack() {
    window.history.back();
}
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];               
                if(id=="<?=$invoice_details['Location']?>") {
                   $('#ward').append($('<option selected="selected">', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                 else {
                    $('#ward').append($('<option>', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                }
              
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
