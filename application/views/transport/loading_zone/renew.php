<!--   <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Loading Zones<span>Pay your Loading Zones</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">loading Zones</a></li>
          <li class="active">Select Estate</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
            <div class="panel panel-default col-md-3" style="width:70%;margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="minimize">&minus;</a>
          </div>
          <h4 class="panel-title">Loading Zone Renewal</h4>
          <p>Plase make sure you have picked the correct business name</p>
        </div>
        <div class="panel-body">
          <?php //var_dump($houses); ?>
          <?php echo form_open('transport_infrustructure/confirm',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Select the your business name</h4>
                  <select class="form-control chosen-select" id="client" name="client" data-placeholder="Select Bid...">
                        <option value=""></option>
                       <?php foreach($businesses as $key=>$value) { ?>
                             <option value="<?php echo $value['client']; ?>"><?php echo $value['client']; ?></option>
                           <?php  } ?>
                  </select>
                  
                    
              </div>
              <div class="form-group">
                <div class="col-md-8">
                  <h4>Enter your Business Number</h4>
                  <input type="text" id="bid" class="form-control" name="bid">
                </div>
              </div>

              <input type="hidden" name="format" value="renewal">
              <input type="hidden" name="Form_Id" value="TF01">
              <input type="hidden" name="category_id" value="197">
              <input type="hidden" name="Start_date" value="<?=date('Y')?>/01/01">


            </div>
            <div class="form-group">
              <div class="col-sm-8">
                <input type="submit" class="btn btn-primary" value="Submit">               
              </div>
            </div>
           <!--  <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">House numbers</h4>
                  <div id="houseno">
                      <select class="form-control chosen-select" data-placeholder="Choose Estate First...">
                        <option value=""></option>
                        
                      </select>
                  </div>
              </div>
            </div>
            <div id="proc">
              <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Phone Number</h4>
                  <input type="text" value="" id="phone" name="phone" class="form-control" />
                </div>

              </div>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div> -->
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Select your Estate</p></li>
            <li><p>Click Submit to Load Search for your House Number</p></li>
          </ol>
        </div>

      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          /*$('#estate').on('change',function(e){
              $.post('<?php echo base_url();?>index.php/rent/get_houses',
              {
                EstateID:$('#estate').val()
              },
              function(data){
                console.log(data);
                $('#houseno').html(data);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                if(document.getElementById("house") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }

              });

          });*/
      });
    </script>
