<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Loading Zones<span>Pay for your Loading Zones Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Loading Zones</a></li>
      <li class="active">Display Invoice Details</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      //$rescode= $construction['rescode'];
      #$construction['invoiceStatus'] = "Paid";

      //if($rescode=="0" && $construction['invoiceStatus']!="paid"):

      ?>      
       <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <?php  if(!isset($invoice_details['Fullname'])):?>
                <div class="alert alert-warning">
                  <?=$invoice_details['message']?>      <button class="btn btn-primary pull-right" onclick="goBack()">Back</button>
                </div>
              <?php else:?>
              <div class="row">
                  <?php echo form_open('transport_infrustructure/approve',array('class' =>"form-block")) ?>
                  <?php foreach($invoice_details as $key=>$value):?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>">
                  <?php endforeach;?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">TRANSPORT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>Contact Person</b></td>
                        <?php if($invoice_details['Fullname']):?>
                          <td><?=$invoice_details['Fullname']?></td>
                          <input type="hidden" value="<?=$invoice_details['Fullname']?>" name="Fullname">
                          <input type="hidden" value="<?=isset($invoice_details['transaction_type']) ? $invoice_details['transaction_type']: 1 ?>" name="trans_status">
                        <?php else:?>
                           <td><input type="text" class="form-control" value="<?=$invoice_details['Fullname']?>" name="Fullname"></td>
                        <?php endif;?>
                      </tr>
                      <tr>
                        <td><b>Contact Phone No</b></td> 
                         <td>
                         <?php if($invoice_details['Tel_no']):?> 
                          <p><?=$invoice_details['Tel_no']?></p>
                          <input type="hidden" value="<?=$invoice_details['Tel_no']?>" name="Fullname">
                          <?php else:?>   
                          <input type="text" class="form-control" name="Tel_no" required>                  
                          <?php endif;?>
                        </td>                   
                      </tr>
                      <tr>
                        <td><b>Contact Email</b></td>                       
                        <td>
                        <?php if($invoice_details['Email']):?> 
                         <p><?=$invoice_details['Email']?></p>
                         <input type="hidden" value="<?=$invoice_details['Email']?>" name="Fullname">
                         <?php else:?>   
                          <input type="email"  class="form-control" name="Email" required>            
                         <?php endif;?>
                       
                        </td>                   
                      </tr>
                      <tr>
                        <td><b>Business Name</b></td>
                        <td><?php echo $invoice_details['bid_name'];?></td>                      
                      </tr>
                      <tr>
                        <td><b>Location</b></td>
                        <td>
                        <?php if($invoice_details['Location']!=''):?>
                           <?=$invoice_details['Location']?>
                           <input type="hidden" name="Location" value="<?=$invoice_details['Location']?>" class="form-control" >
                        <?php else:?> 
                           <input type="text" name="Location" class="form-control" >
                        <?php endif;?>                          
                        </td>
                      </tr>
                      <!-- <tr>
                        <td><b>LR Number</b></td>
                        <td>
                        <?php if($invoice_details['bid_lr_no']!=''):?>
                           <?=$invoice_details['bid_lr_no']?>
                        <?php else:?> 
                           <input type="text" name="bid_lr_no" class="form-control" >
                        <?php endif;?>                          
                        </td>
                      </tr> -->

                      <tr>
                        <td><b>Sub Counties</b></td>
                        <td>
                        <select class="form-control" name="zones" id="zones">
                            <?php foreach($subcounties as $county): ?>
                              <?php if($county['ID']==$invoice_details['zone']):?>
                                   <option data-bid="<?=$county['Bid']?>" selected="selected" value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
                              <?php else:?>
                                   <option value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
                              <?php endif;?>
                             
                            <?php endforeach;?> 
                        </select>
                                                
                        </td>
                      </tr>
                      <tr>
                        <td><b>Wards</b></td>
                        <td>
                           <select class="form-control" name="ward" id="ward" >

                           </select>
                         </td>
                      </tr>               
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
             

             
              <div class="panel-footer">
                <div class="row">  

                  <input type="submit" value="Confirm Business Details" class="btn btn-primary ">
                  <?php echo form_close(); ?>
                  <?php echo anchor('transport_infrustructure/section/loading_zone','Cancel',array('class'=>"btn btn-default")); ?>
                </div>
              </div>
            <?php endif;?>
          
            
       
      
    </div> 
<script type="text/javascript">
function goBack() {
    window.history.back();
}
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];               
                if(id=="<?=$invoice_details['Location']?>") {
                   $('#ward').append($('<option selected="selected">', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                 else {
                    $('#ward').append($('<option>', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                }
              
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
