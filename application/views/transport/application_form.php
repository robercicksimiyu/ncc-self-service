 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Enter Details </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('transport_infrustructure/confirm',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>         
          <div class="row mb10">
            <div class=" col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="Bid" placeholder="Enter Business ID" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullname" name="contact_person" placeholder="John Doe" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-12 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phone_number" placeholder="0700000000" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="contact_email" placeholder="you@example.com" required/>
            </div>           
             <div class="col-md-6 col-sm-12">
              <label class="col-sm-5 control-label">Parking Slots</label>
              <input type="text" class="form-control" id="Parking_slots" name="Parking_slots" placeholder="20" required/>
            </div>            
            <div class="col-md-6 col-sm-12">
                <label class=" col-sm-5 control-label">Start Date</label>
                <input type="text" class="form-control" id="datepicker" name="Start_date" placeholder="" required/>
            </div>
            <input type="hidden" name="category_id" value="<?=$category?>">
            <input type="hidden" name="Form_Id" value="<?=$form?>">
        </div>     
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
            <li>
              <p>Enter your Email Address</p></li>
              <li>
                <p>Click Submit</p></li>
              </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
    $(document).ready(function(){
      jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
      // Date Picker
      jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
      
      jQuery('#datepicker-inline').datepicker();
      
      jQuery('#datepicker-multiple').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    });
  </script>

<script type="text/javascript">
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];

               $('#ward').append($('<option>', {
                "value": id
              }).text(name));
               console.log(id + " " + name);
             }
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
