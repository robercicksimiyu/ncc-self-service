<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Loading Zone<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Loading Zone</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>

    <div class="contentpanel" > 

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Enter Reference Number </h4>
          <p>Cross check to make sure you have filled in the correct invoice Number</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('transport_infrustructure/invoice',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-8">
                      <label class="col-sm-4 control-label">Reference Number</label>
                        <input type="text" class="form-control" id="RefId" name="RefId" placeholder="E.g NCCF0198" required />
                      </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit" >
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Enter your Reference Number</p></li>
		   <li>
          <p>Check Submit</p></li>
		    </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->
