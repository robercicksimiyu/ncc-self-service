<div class="pageheader">
      <h2><i class="fa fa-road"></i> Details <span>Change password</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li class="active">Change password</li>
        </ol>
      </div>
</div>



    <div class="contentpanel" >
      <div class="row">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize">&minus;</a>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <?php //if($cashier->num_rows()>0):?>
                <?php echo form_open(''); ?>
                <div id="fedit" style="padding-right:200px;">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">CURRENT PIN:</label>
                    <div class="col-sm-6">
                      <input type="password"  name="csr" id="csr" value="<?php //echo $cashier['empid']; ?>" id="empid" class="form-control" required/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">NEW PIN:</label>
                    <div class="col-sm-6">
                      <input type="password"  name="sr1" value="<?php //echo $cashier['fname']; ?>" id="sr1" class="form-control" required/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">CONFIRM NEW PIN:</label>
                    <div class="col-sm-6">
                      <input type="password" name="sr2" value="<?php //echo $cashier['lname']; ?>" id="sr2" class="form-control" required/>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-4">
                    </div>
                     <div class="col-sm-4">
                     <input type="submit" value="Update" class="btn btn-primary" id="update">
                    </div>

                  </div>
                  <div class="alert alert-info aler">
                  </div>
                  </div>
                 <!--  <div class="err-box"></div> -->
                </div><!-- panel-body -->
                <?php echo form_close(); ?>
              </div>
            </div><!-- contentpanel -->
          </div>
      </div>
  </div>
</div>

  <script type="text/javascript">
    $(document).ready(function(){
     $('.aler').hide()
      json='';
      $('#update').on('click',function(e){
         e.preventDefault(); 
         if ($('#sr1').val().length<4 && $('#sr2').val().length<4) {
            $('.aler').show()
            $('.aler').html("Pin cannot be less than 4")
         }else{
        $.post('<?php echo base_url();?>index.php/user/changePassword',{
          csr:$('#csr').val(),
          sr1:$('#sr1').val(),
          sr2:$('#sr2').val()
        },function(data){
          json=JSON.parse(data);
                    if(json.err==0) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
                    if(json.err==1) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
                    if(json.err==2) {
                      $('.aler').show()
                      $('.aler').html(json.x)
                    }
        });}
      });
    });
  </script>