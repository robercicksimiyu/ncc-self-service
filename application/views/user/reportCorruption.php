 <div class="pageheader">
      <h2><i class="fa fa-th"></i>Report Corruption<span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li class="active">Send Message</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
          </div>
          <h4 class="panel-title">Write us a message below</h4>
        </div>
        <div class="panel-body">
          <?php echo form_open(); ?>
            <div class="form-group">
              <label class="control-label">Message</label>
              <textarea id="message" name="message"  placeholder="Type your message here..." class="form-control" rows="5"></textarea>
            </div>
            <input type="submit" class="btn btn-primary" value="Send">
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      
    </div><!-- contentpanel -->