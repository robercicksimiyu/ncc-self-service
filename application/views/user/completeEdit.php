     <div class="pageheader">
      <h2><i class="fa fa-user"></i> User Profile <span>Account Profile</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php">Profile</a></li>
          <li class="active">Account details</li>
        </ol>
      </div>
    </div>



    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                 <?php $msg=$this->uri->segment(3); if ($msg==1) { ?>
                 <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                </div>
                   
                 <?php }elseif ($msg==2) { ?>
                   <div class="alert alert-info">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  </div>
                 <?php } ?>
                
                <div class="row">
                    
                  </div>
                <?php echo anchor('cashiers/profile','Go Back',array('class'=>'btn btn-primary'));?>
              </div>
            </div><!-- panel-body -->
          </div>
        </div>
      
    </div><!-- contentpanel -->
    

  