<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Parking Penalties<span>Pay your penalty online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Parking Penalties</a></li>
          <li class="active">Select Penalty</li>
        </ol> 
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Select Penalty Type</h4>
        </div>
        <div class="panel-body">
          <?php $penalty = $parkingpenalty['VehicleType']; ?>
          <?php echo form_open('parking/displayPenaltyDetails',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Select Penalty</h4>
                  <select class="form-control chosen-select" id="vehicleType" name="vehicleType" data-placeholder="Select Penalty..." required >
                        <option value=""></option>
                        <?php foreach($penalty as $key=>$value) { ?>
                        <?php if($value->ID==30 || $value->ID==31 || $value->ID==32): ?>
                        <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                        <?php endif;   
                          }
                        ?>
                  </select>
                  <input type="hidden" id='VehicleDescription' name="VehicleDescription" value="">
              </div>
            </div>
            <div id="proc">
              <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Vehicle Registration Number</h4>
                  <input type="text" value="" id="regno" name="regno" class="form-control" required />
                  <input type="hidden" id='ZoneCode' name="ZoneCode" value="10">
                </div>

              </div>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->
          
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li>
            <p>Choose the penalty type</p></li>
              <li>
                <p>Enter the vehicle registration number and click submit</p></li>
              </ol>
            </div>
            
          </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
       $(document).ready(function(){
         jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

        $('#ZoneCode').on('change',function(){
          code=$(this).find('option:selected').text();
          $('#ZoneDescription').val(code);
              //alert(code);
            });

        $('#vehicleType').on('change',function(){
          code=$(this).find('option:selected').text();
          $('#VehicleDescription').val(code);
              //alert(code);
            });
       });
    </script>
