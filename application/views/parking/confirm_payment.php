<div class="mainpanel">
    <div class="headerbar">
      
      
    </div><!-- headerbar -->
    
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i>Single Business Permit - Self Service</h2>
      <ul class="nav nav-pills nav-stacked nav-bracket" style="width: 60%;text-align: right;">
        <a href="<?php //echo base_url();?>index.php/sbp/topup"><span></span></a>&nbsp;&nbsp;
        <a href="<?php //echo base_url();?>index.php/sbp/view_balance"><span></span></a>&nbsp;&nbsp;
        <a href="#"><span></span></a>
      </ul>
    </div>
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default col-md-8">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                  
        
                  <div class="table-responsive">
                  <form method="POST" action="<?php echo base_url();?>index.php/sbp/submit_checkout">
                    <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">PLEASE CONFIRM BEFORE COMPLETING THE TRANSACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php #echo $this->session->userdata('biz_name');?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php #echo number_format($this->session->userdata('sbpfee'),2,'.',',');?></td>
                      </tr>
                      <tr>
                        <td><b>TRANSACTION ID</b></td>
                        <td><?php #echo $this->session->userdata('trans_id');?></td>
                      </tr>
                    </tbody>
                  </table>

                  </div><!-- table-responsive -->
        
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
            <input class="btn btn-primary" type="submit" value="Submit Details">
            <a href="<?php echo base_url();?>index.php/sbp/enter_acc_no"><input class="btn btn-primary" type="button" value="Cancel"></a>
            </div>
            </form>
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->