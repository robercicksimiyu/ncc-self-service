<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Print Parking Receipt<span>Print receipt</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>parking/reprintDailyreceipt">Parking</a></li>
          <li class="active">Print Receipt</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
     <div class="panel-body">
        <div class="row">
          <div class="alert alert-danger " style="margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "OOPS! Something went wrong.Try again."; ?>
                <a href="<?php echo base_url();?>parking/reprintManualDReceipt" class="btn btn-primary receipt" style="float:right;margin-top:-5px;"><i class="fa fa-mail-reply"></i>Print Now</a>
          </div>
        </div>
      </div>
    </div>
  </div><!-- contentpanel -->

    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Click back to try again  </p></li>
            <li><p>Make sure the vehicle registration number you entered is correct</p></li>
          </ol>
        </div>
  </div> --><!-- mainpanel -->
