<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Daily Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/rent/house_rent">Daily Parking</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="row">
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment <?php #echo $complete['rescode']; ?></h4>
              </div>
              <?php $response = $complete['rescode'];
              
                if ($response=="0"): ?>
                  <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Your Transaction was Successful";
                    $regno=$complete['regno'];
                    echo anchor('parking/printDreceipt/'.$regno,'Print Ticket',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>

                <?php elseif($response=="4440" || $response=="44405" || $response=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>

            <?php elseif($response=="1153"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Parking for this vehicle is already paid."; ?>
              </div>
              
              <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered a Wrong Wallet PIN"; ?>
                <a href="<?php echo base_url();?>index.php/parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
            <?php elseif($response=="1110"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Daily parking is free from 2 pm on Saturday and free the whole of Sunday"; ?>
                <a href="<?php echo base_url();?>index.php/parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php elseif($response=="1053"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The transaction has already been completed."; ?>
                <a href="<?php echo base_url();?>index.php/parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php elseif($response=="2026"): ?>
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "PIN Change is Required.".anchor('user/profile','Change Now',array('class'=>'btn btn-primary','style'=>'float:right;padding-top:1px;'));
                  ?>
              </div>
              <?php elseif($response==2021): ?>
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "The Phone Number Entered is Invalid"; ?><a href="<?php echo base_url();?>parking/daily_parking">  Go Back</a>
              </div>
              <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div><!-- contentpanel -->

    

