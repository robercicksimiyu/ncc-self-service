<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Daily Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/rent/house_rent">Daily Parking</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="row">
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment</h4>
              </div>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  
                  <?php echo $complete['restext'];?>
                  <a href="#" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i> Print Receipt</a>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div><!-- contentpanel -->

    

