<div class="contentpanel" >
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                  
                
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM PAYMENT</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php //var_dump($confirm['result']); ?>
                      <tr>
                        <td><b>NUMBER PLATE</b></td>
                        <td><?php echo $confirm['plateno'] ?></td>
                      </tr>
                      <tr>
                        <td><b>VEHICLE CATEGORY</b></td>
                        <td><?php echo $confirm['vehicle_cat'] ?></td>
                      </tr>
                      <tr>
                        <td><b>PARKING AREA</b></td>
                        <td><?php echo $confirm['parking_area'] ?></td>
                      </tr>
                      <tr>
                        <td><b>PERIOD</b></td>
                        <td><?php echo $confirm['period'] ?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $confirm['amount'] ?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
              </div>
              <?php echo form_open('en/complete_parking_payment'); ?>
                <input type="hidden" name="trans_id" value="<?php //echo $confirm['trans_id']; ?>">
                <input type="hidden" name="biz_name" value="<?php //echo $confirm['biz_name']; ?>">
                <input type="hidden" name="amount" value="<?php //echo $confirm['sbpfee']; ?>">
                <input type="hidden" name="biz_id" value="<?php //echo $confirm['biz_id'] ?>">
              <div class="row" id="input">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Phone</label>
                    <input type="text" name="phone" id="phone" class="form-control" required/>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Optional"/>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->

          </div><!-- panel-body -->
            <div class="panel-footer">
                <input type="submit"  value="Complete Payment" class="btn btn-primary">
            </div>
            <?php echo form_close();?>
              
          </div>
        </div>
    </div><!-- contentpanel -->