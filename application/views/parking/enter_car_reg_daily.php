<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Daily Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Daily Parking</a></li>
          <li class="active">Select Location & Car size</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Select Zone & Vehicle Type</h4>
        </div>
        <div class="panel-body">
          <?php //var_dump($houses); ?>
          <?php echo form_open('parking/display_details_daily',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Select Zone</h4>
                  <select class="form-control chosen-select" id="ZoneCode" name="ZoneCode" data-placeholder="Select Zone...">
                        <option value=""></option>
                        <?php
                          foreach($parking['zones'] as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                           <?php    
                          }
                        ?>
                  </select>
                  <input type="hidden" id='ZoneDescription' name="ZoneDescription" value="">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Select Vehicle Type</h4>
                  <div id="houseno">
                      <select class="form-control chosen-select" id="vehicleType" name="vehicleType" data-placeholder="Select Vehicle Category...">
                        <option value=""></option>
                        <?php
                          foreach($parking['VehicleType'] as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                           <?php    
                          }
                        ?>
                  </select>
                  <input type="hidden" id="VehicleDescription" name="VehicleDescription" value="" >
                  </div>
              </div>
            </div>
            <div id="proc">
              <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Vehicle Registration Number</h4>
                  <input type="text" value="" id="regno" name="regno" class="form-control" style="text-transform: uppercase;" pattern=".{5,10}" required title="5 to 10 characters"/>
                </div>

              </div>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->
          
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li>
            <p>Choose the Zone in which you want to park</p></li>
            <li>
              <p>Select the Vehicle type</p></li>
              <li>
                <p>Enter the vehicle registration number and Click Submit</p></li>
              </ol>
            </div>
            
          </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

        $('#ZoneCode').on('change',function(){
          code=$(this).find('option:selected').text();
          $('#ZoneDescription').val(code);
              //alert(code);
            });

        $('#vehicleType').on('change',function(){
          code=$(this).find('option:selected').text();
          $('#VehicleDescription').val(code);
              //alert(code);
            });
      });
    </script>
