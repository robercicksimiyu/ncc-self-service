<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Daily Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/rent/house_rent">Daily Parking</a></li>
          <li class="active">Display Parking Details <?php #echo $rescode= $prepare['restext']; ?></li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" >

      <?php 

      $rescode= $prepare['rescode'];

      if($rescode=="0"):

      ?>
      
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('parking/complete_payment_daily',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">PARKING DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>NAME</b></td>
                        <td><?php echo $prepare['name'];?></td>
                        <input type="hidden" id="transid" name="transid" value="<?php echo $prepare['transid'];?>"/>
                        <input type="hidden" id="amount" name="amount" value="<?php echo $prepare['fee'];?>"/>
                        <input type="hidden" id="type" name="type" value="<?php echo $prepare['type'];?>"/>
                        <input type="hidden" id="zone" name="zone" value="<?php echo $prepare['zone'];?>"/>
                        <input type="hidden" id="reg" name="reg" value="<?php echo $prepare['regno'];?>"/>
                      </tr>
                      <tr>
                        <td><b>ZONE</b></td>
                        <td><?php echo $prepare['zone'];?></td>
                      </tr>
                      <tr>
                        <td><b>VEHICLE TYPE</b></td>
                        <td><?php echo $prepare['type'];?></td>
                      </tr>
                      <tr>
                        <td><b>VEHICLE REGISTRATION NUMBER</b></td>
                        <td><?php echo $prepare['regno'];?></td>
                      </tr>
                      <tr>
                        <td><b>PARKING FEE</b></td>
                        <td><?php echo number_format($prepare['fee'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('parking/daily_parking','Cancel',array('class'=>"btn btn-default")); ?>
                </div>


              </div>
        </div>
      
    </div><!-- contentpanel -->



	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Confirm that all the details displayed are correct</p></li>
		   <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
		    </ol>
        </div>
    
  </div><!-- mainpanel -->
<?php elseif($rescode=="4440" || $rescode=="44405" || $rescode=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
<?php elseif($rescode=="1036"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid Vehicle Registration Number"; ?>
                <a href="<?php echo base_url();?>parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
<?php elseif($rescode=="1301"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Failed, pay CLAMPING charges first."; ?>
                <a href="<?php echo base_url();?>parking/parkingPenalties" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
<?php elseif($rescode=="1097"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Costing for the given Zone Code not found"; ?>
                <a href="<?php echo base_url();?>parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
<?php elseif($rescode=="2023"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry.your Account has been suspended"; ?>
                <a href="<?php echo base_url();?>parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
<?php elseif($rescode=="2021"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an Invalid Phone Number"; ?>
                <a href="<?php echo base_url();?>parking/daily_parking" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
<?php elseif($rescode=="2030"): ?>
          <div class="panel panel-default col-md-10" style="margin-right:20px">
            <div class="panel-body">
              <div class="panel-footer">
                <div class="row">
                  <div class="alert alert-danger col-md-11" style="margin-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Sorry. You do not have sufficient funds in your wallet to continue with the transaction."; ?>
                    <a href="<?php echo base_url();?>selfservice/wallettopup" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Top Up Wallet</a>
                  </div>
                </div>
              </div>
        </div>
      
    </div> 
<?php endif; ?>