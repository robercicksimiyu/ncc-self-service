<div class="pageheader">
      <h2><i class="fa fa-road"></i> Parking <span>Parking Fee</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/parking/prepparking">Parking</a></li>
          <li class="active">Vehicle Details</li>
        </ol>
      </div>
    </div>



    <div class="contentpanel" >
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="minimize">&minus;</a>
          </div>
          <h4 class="panel-title">Enter Vehicle Details</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('parking/confirmparkingdetails',array('class' =>"form-block")) ?>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Zones</h4>
                  <select class="form-control chosen-select" id="zones" name="zones" data-placeholder="Select Zone...">
                        <option value=""></option>
                        <?php
                          foreach($zones as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->ZoneWardCode; ?>"><?php echo $value->ZoneWardName; ?></option>
                           <?php    
                          }
                        ?>
                      </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-8">
                  <h4 class="subtitle mb5">Fees & Charges</h4>
                  <div id="feescharges">
                      <select class="form-control chosen-select" data-placeholder="Choose Zone First...">
                        <option value=""></option>
                        
                      </select>
                  </div>
              </div>
            </div>
            <div id="proc">
              <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Phone Number</h4>
                  <input type="text" value="" id="phone" name="phone" class="form-control" />
                </div>

              </div>
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- contentpanel -->

    <script type="text/javascript">
      $(document).ready(function(){
          $('#zones').on('change',function(e){
              $.post('<?php echo base_url();?>index.php/parking/getParkingFees',
              {
                ZoneID:$(this).val()
              },
              function(data){
                //console.log(data);
                $('#feescharges').html(data);
                jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

                /*if(document.getElementById("charges") === null){
                    $('#proc').hide();
                }else{
                  $('#proc').show();
                }
*/
              });

          });
      });
    </script>