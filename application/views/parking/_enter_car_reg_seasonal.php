<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Seasonal Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/selfservice/home">Seasonal Parking</a></li>
          <li class="active">Select Duration & Car size</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-invoice">
                
                </table>
            </div><!-- table-responsive -->
          
            <table class="table table-total">
                <tbody>
                    <tr>
                        <td><strong>Number Of Vehicles :</strong></td>
                        <td id="num"></td>
                    </tr>
                    <tr>
                        <td><strong>Total Amount :</strong></td>
                        <td id="tot"></td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
              <div class="col-sm-3">
                  <h4 class="subtitle mb5">Duration</h4>
                  <select class="form-control chosen-select" id="DurationCode" name="DurationCode" data-placeholder="Duration...">
                        <option value=""></option>
                        <?php
                          foreach($parking['duration'] as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->DurationCode; ?>"><?php echo $value->Description; ?></option>
                           <?php 
                          }
                        ?>
                  </select>
                  <span id="sduration" style="color:red"></span>
                  <input type="hidden" id="durationDesc" name="durationDesc" value="">
              </div>

              <div class="col-sm-3">
                  <h4 class="subtitle mb5">Vehicle Type</h4>
                  <select class="form-control chosen-select" id="VehicleTypeCode" name="VehicleTypeCode" data-placeholder="Category...">
                        <option value=""></option>
                        <?php
                          foreach($parking['VehicleType'] as $key=>$value) {
                            ?>
                            <?php if($value->VehicleTypeCode!=30&&$value->VehicleTypeCode!=31&&$value->VehicleTypeCode!=32): ?>
                             <option value="<?php echo $value->VehicleTypeCode; ?>"><?php echo $value->Description; ?></option>
                           <?php endif;   
                          }
                        ?>
                  </select>
                  <span id="svehicle" style="color:red"></span>
                  <input type="hidden" id="VehicleDescription" name="VehicleDescription" value="">
              </div>
              <div class="col-sm-4">
                <h4 class="subtitle mb5">Registration Number</h4>
                <div class="input-group mb15">
                  <input type="text" name="registrationNo" id="registrationNo" class="form-control">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary add">Add New</button>
                  </span>
                   <span id="sreg" style="color:red"></span>
                </div>
              </div>
              <div class="col-sm-2" id="loader">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>

            
            <div class="text-right btn-invoice">
                <button id="pay" class="btn btn-primary mr5"><i class="fa fa-dollar mr5"></i> Make Payment</button>
                <button class="btn btn-white reset"><i class="fa fa-icon-trash mr5"></i> Reset</button>
            </div>
            
            <div class="mb40"></div>
            
        </div><!-- panel-body -->
      </div><!-- panel -->
      
    </div><!-- contentpanel --> 

      <script type="text/javascript">
      $(document).ready(function(){
          $('#pay').hide();
          $('#loader').hide();
          $('.table-total').hide();
          jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          $.get('<?php echo base_url();?>index.php/parking/getVehiclesDetails',
            function(data){
                $('.table-invoice').html(data);
            });
          $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
            function(data){
                if (data.length>0) {
                    $('#tot').html(data);
                }else{
                    $('.table-total').hide();
                }
                
            });
          $.get('<?php echo base_url();?>index.php/parking/countVehicles',
            function(data){
                $('#num').html(data);
            });

          $('.add').on('click',function(){
                $('#loader').show();
                DurationCode=$('#DurationCode').find('option:selected').val();
                DurationDesc=$('#DurationCode').find('option:selected').text();
                VehicleTypeCode=$('#VehicleTypeCode').find('option:selected').val();
                VehicleTypeDesc=$('#VehicleTypeCode').find('option:selected').text();
                registrationNo=$('#registrationNo').val();


               $.post('<?php echo base_url();?>index.php/parking/addVehicle',
               {
                DurationCode:DurationCode,
                DurationDesc:DurationDesc,
                VehicleTypeCode:VehicleTypeCode,
                VehicleTypeDesc:VehicleTypeDesc,
                registrationNo:registrationNo
               },function(data){
                $('#pay').show();
                // if (data.length==0) {
                //       alert(data);
                //     };
                    updateVehicle();
                    $('#loader').hide();
               });


               

          });

          $('.reset').on('click',function(){
                $.get('<?php echo base_url();?>index.php/parking/resetVehicle',
                    function(data){
                      //$('.table-total').hide();
                      $('#pay').hide();
                        updateVehicle();
                    });
          });

          $('#pay').on('click',function(){
              $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
                function(data){
                    if (data.length>0) {
                        window.location.replace("<?php echo base_url() ?>index.php/parking/confirmSPParking");
                      
                    }else{
                       /* do nothing*/
                    }
                    
                });

          });

          function updateVehicle(){
            $.get('<?php echo base_url();?>index.php/parking/getVehiclesDetails',
                function(data){
                    $('.table-invoice').html(data);
                });
              $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
                function(data){
                    if (data.length>0) {
                        $('.table-total').show();
                        $('#tot').html(data);
                    }else{
                        $('.table-total').hide();
                    }
                    
                });
              $.get('<?php echo base_url();?>index.php/parking/countVehicles',
                function(data){
                    $('#num').html(data);
                });
           }

          /*a=$('#VehicleTypeCode').find('option:selected').val();
          alert(a);*/

      });
    </script>
