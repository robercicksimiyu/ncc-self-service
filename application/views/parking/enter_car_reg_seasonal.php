<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Seasonal Parking Payment<span>Pay your parking fee online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/selfservice/home">Seasonal Parking</a></li>
          <li class="active">Select Duration & Car size</li>
        </ol>
      </div>
    </div>
<?php $balz=$this->session->userdata('balanc');//$bal['balanc'];
$balance=str_replace(',','',$balz);
 ?>
    <div class="contentpanel">
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-invoice">
                
                </table>
            </div><!-- table-responsive -->
          
            <table class="table table-total" >
                <tbody>
                    <tr>
                        <td><strong>Number Of Vehicles :</strong></td>
                        <td id="num">0</td>
                    </tr>
                    <tr>
                        <td><strong>Total Amount :</strong></td>
                        <td id="tot">0.00</td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
              <div class="col-sm-3">
                  <h4 class="subtitle mb5">Duration</h4>
                  <select class="form-control chosen-select" id="DurationCode" name="DurationCode" data-placeholder="Duration...">
                        <option value=""></option>
                        <?php
                          foreach($parking['durations'] as $key=>$value) {
                            ?>
                             <option value="<?php echo $value->ID; ?>"><?php echo str_replace('_', ' ',$value->Description); ?></option>
                           <?php 
                          }
                        ?>
                  </select>
                  <span id="sduration" style="color:red"></span>
                  <input type="hidden" id="durationDesc" name="durationDesc" value="">
              </div>

              <div class="col-sm-3">
                  <h4 class="subtitle mb5">Vehicle Type</h4>
                  <select class="form-control chosen-select" id="VehicleTypeCode" name="VehicleTypeCode" data-placeholder="Category...">
                        <option value=""></option>
                        <?php
                          foreach($parking['VehicleType'] as $key=>$value) {
                            ?>
                            <?php if($value->ID!=30&&$value->ID!=31&&$value->ID!=32): ?>
                             <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                           <?php endif;   
                          }
                        ?>
                  </select>
                  <span id="svehicle" style="color:red"></span>
                  <input type="hidden" id="VehicleDescription" name="VehicleDescription" value="">
              </div>
              <div class="col-sm-4" style="margin-right:-20px;">
                <h4 class="subtitle mb5">Registration Number</h4>
                <!-- <div class="input-group mb15"> -->
                  <input type="text" name="registrationNo" id="registrationNo" class="form-control" style="text-transform: uppercase;"/>
                  
                   <span id="sreg" style="color:red"></span>
                <!-- </div> -->
              </div>
              <div class="col-sm-1">
              <h4 class="subtitle mb5">&nbsp</h4>
                <span class="input-group-btn">
                      <button type="button" class="btn btn-primary add" style="padding-bottom:10px;">Add</button>
                </span>
              </div>
              <div class="col-sm-1" id="loader" style="float:right;">
                <h4 class="subtitle mb5">&nbsp</h4>
                <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
              </div>
            </div>

            
            <div class="text-right btn-invoice">
                <div class="col-sm-6" id="loader2">
                  <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader14.gif" alt="" /> Processing, please wait...
                </div>
                <button id="pay" class="btn btn-primary mr5"><i class="fa fa-dollar mr5"></i> Make Payment</button>
                <button class="btn btn-white reset"><i class="fa fa-icon-trash mr5"></i> Reset</button>
            </div>
            
            <div class="mb40"></div>
            <div class="alert alert-danger errbox">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <span id="msg" style="color:red"></span>
            </div>
            
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li><p>Select Duration,Vehicle Type and enter Registration no. and click Add New</p></li>
            <li><p>Repeat step 1 above to add another vehicle</p></li>
            <li><p>Confirm that the vehicle details you have entered are correct</p></li>
            <li><p>Click Make Payment to proceed or Reset to clear the vehicle list</p></li>
          </ol>
            </div>

          </div>
    </div><!-- contentpanel --> 



  <script type="text/javascript">
      $(document).ready(function(){
          $('.errbox').hide();
          $('#loader').hide();
          $('#loader2').hide();
          jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
          $.get('<?php echo base_url();?>index.php/parking/getVehiclesDetails',
            function(data){
                $('.table-invoice').html(data);
                removeRecord();
          });
          $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
            function(data){
                if (data=='0.00') {
                  $('.table-total').hide();
                }else{
                  $('#tot').html(data); 
                }
                
          });
          $.get('<?php echo base_url();?>index.php/parking/countVehicles',
            function(data){
                $('#num').html(data);
          });

          $('.add').on('click',function(){
                DurationCode=$('#DurationCode').find('option:selected').val();
                DurationDesc=$('#DurationCode').find('option:selected').text();
                VehicleTypeCode=$('#VehicleTypeCode').find('option:selected').val();
                VehicleTypeDesc=$('#VehicleTypeCode').find('option:selected').text();
                registrationNo=$('#registrationNo').val();

                if (DurationCode.length==0) {
                    $('#msg').html('Please select parking duration');
                    $('.errbox').show();
                }else if(VehicleTypeCode.length==0){
                    $('#msg').html('Please select vehicle category');
                    $('.errbox').show();
                }else if(registrationNo.length==0){
                    $('#msg').html('Please enter vehicle registration number');
                    $('.errbox').show();
                }else if(DurationCode.length>0&&VehicleTypeCode.length>0&&registrationNo.length >0){
                    $('.errbox').hide();
                    $('#loader').show();
                    $(this).attr("disabled", true);
                    $.post('<?php echo base_url();?>index.php/parking/addVehicle',
                     {
                      fbal:'<?php echo $balance; ?>',
                      DurationCode:DurationCode,
                      DurationDesc:DurationDesc,
                      VehicleTypeCode:VehicleTypeCode,
                      VehicleTypeDesc:VehicleTypeDesc,
                      registrationNo:registrationNo
                     },function(data){
                          $('.add').attr("disabled", false);
                          data=data.trim();
                          if (data.length>0) {
                            $('#msg').html(data);
                            $('.errbox').show();
                          }else{
                            updateVehicle();
                            $('.errbox').hide();
                          };
                           
                          $('#loader').hide();
                    });
                }


               

          });

          $('.reset').on('click',function(){

                $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
                function(data){
                    data=data.trim();
                    if (data!='0.00') {
                        $('.errbox').hide();
                        $.get('<?php echo base_url();?>index.php/parking/resetVehicle',
                            function(data){
                            updateVehicle();
                        });
                    }else{
                        $('#msg').html('Please add records first');
                        $('.errbox').show();
                    }
                    
                });

                
                $('#loader2').hide();
          });

          $('#pay').on('click',function(){
              $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
                function(data){
                    data=data.trim();
                    if (data!='0.00') {
                        $('.errbox').hide();
                        $('#loader2').show();
                        window.location.replace("<?php echo base_url() ?>index.php/parking/confirmSPParking");
                    }else{
                       $('#msg').html('Please add atleast one vehicle to proceed');
                       $('.errbox').show();
                    }
                    
                });

          });

          function removeRecord(){
            $('a.removeR').on('click',function(event){
              id=$(this).attr('id');
              $.get('<?php echo base_url();?>index.php/parking/removeSRecord/'+id,
                function(data){
                  /*do something*/
                  updateVehicle();
                });
              event.prventDefault();
            });
          }

          function updateVehicle(){
            $.get('<?php echo base_url();?>index.php/parking/getVehiclesDetails',
                function(data){
                    $('.table-invoice').html(data);
                });
              $.get('<?php echo base_url();?>index.php/parking/getVehiclesSum',
                function(data){
                    data=data.trim();
                    if (data=='0.00') {
                        $('.table-total').hide();
                    }else{
                        $('.table-total').show();
                        $('#tot').html(data);
                        removeRecord();
                    }
                    
              });
              $.get('<?php echo base_url();?>index.php/parking/countVehicles',
                function(data){
                    $('#num').html(data);
              });
           }

      });
    </script>