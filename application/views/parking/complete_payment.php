<div class="mainpanel">
<div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/sbp/enter_acc_no">Single Business Permit</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default col-md-8" style="margin-right:20px">
            <div class="panel-heading">
              <div class="panel-btns">
                <!-- <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a> -->
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM PAYMENT</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php #echo $this->session->userdata('biz_name');?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS ID</b></td>
                        <td><?php #echo $this->session->userdata('biz_id');?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php #echo number_format($this->session->userdata('sbpfee'), 2, '.', ',') ;?></td>
                      </tr>
                      
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
            <?php echo form_open('sbp/complete_payment');
              echo'<div class="col-sm-6">';
              echo '<label class="col-sm-6 control-label">County Wallet PIN:</label>';
              echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
              echo'</div>';
              echo '<br/>';
             echo  '<input type="submit" class="btn btn-primary" value="Complete Payment">'; echo " ";
             echo '<a href="enter_acc_no" class="btn btn-primary">Cancel</a>';
             echo form_close();
             ?>
            </div>
          </div>

          <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li><p>Confirm that all the payment details are correct</p></li>
      <li><p>Enter your county wallet PIN</p></li>
      <li><p>Click Complete to complete payment or Cancel to abort process</p></li>
        </ol>
        </div>
    
  </div><!-- mainpanel -->
        </div>
      
    </div><!-- contentpanel -->

      
    
  </div><!-- mainpanel -->