<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Print Parking Receipt<span>Print receipt for previous transaction</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Parking</a></li>
          <li class="active">Print receipt</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Select type & enter vehicle registration number</h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('parking/reprintDreceipt',array('class' =>"form-block")) ?>
            <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Select Parking type</h4>
              <select class="form-control chosen-select" id="parkingtype" name="parkingtype" data-placeholder="Select Parking Type..." required>
                <option value=""></option>
                <option value="1">Daily Parking Ticket</option>
                <option value="2">Seasonal Parking Ticket</option>
              </select>
            </div>
            <div class="col-sm-5">
              <h4 class="subtitle mb5">Vehicle Registration Number</h4>
              <input type="text" value="" id="regno" name="regno" class="form-control" style="text-transform: uppercase;" pattern=".{5,10}" required title="5 to 10 characters" required/>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Select Date</h4>
              <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="issuedate" required/>
              <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> -->
            </div>
          </div>
            <div id="proc">
              <!-- <div class="form-group">
                <div class="col-sm-8">
                  <h4 class="subtitle mb5">Vehicle Registration Number</h4>
                  <input type="text" value="" id="regno" name="regno" class="form-control" style="text-transform: uppercase;" pattern=".{5,10}" required title="5 to 10 characters" required/>
                </div>
              </div> -->
              <input type="submit" class="btn btn-primary" value="Print Receipt">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li><p>Choose the type of parking you want to print a receipt for</p></li>
          <li><p>Enter the vehicle registration number and Click Print</p></li>
        </ol>
      </div>
            
          </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        // Date Picker
        jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
        
        jQuery('#datepicker-inline').datepicker();
        
        jQuery('#datepicker-multiple').datepicker({
          numberOfMonths: 3,
          showButtonPanel: true
        });
      });
    </script>
