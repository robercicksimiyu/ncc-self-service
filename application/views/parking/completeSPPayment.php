<div class="pageheader">
      <h2><i class="fa fa-road"></i> Seasonal Parking <span>Parking</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><?php echo anchor('parking/seasonalParking','Seasonal Parking');?></li>
          <li class="active">Vehicle Details</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      
          <div class="panel panel-default col-md-8" style="margin-right:20px">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize"></a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                <?php $rescode = $confirm['rescode']; ?>
                <?php if($rescode=="0"):?>
                 <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Your transaction has been successfully been completed.";
                  echo anchor('parking/reprintDailyReceipt/','Print Ticket(s)',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>
                <?php elseif($rescode=="2025"): ?>
                  <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry, you have entered the wrong PIN.";
                  echo anchor('parking/seasonal_parking','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>
                <?php elseif($rescode=="1053"): ?>
                  <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry, the transaction has already been completed.";
                  echo anchor('parking/seasonal_parking','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>
                <?php elseif($rescode=="4440" || $rescode=="44405" || $rescode=="44406"): ?>
                  <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry, service is currently unavailable but will be back soon.";
                  echo anchor('parking/seasonal_parking','Back',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>
                <?php endif; ?>
              </div>
            </div><!-- panel-body -->
          </div>

          <div class="panel panel-default col-md-3" >
            <div class="panel-heading">
              <div class="panel-btns">
              </div>
              <h4 class="panel-title panelx">Thank you for using Nairobi City County e-Payments service.</h4>
            </div>
          </div>
      
    </div><!-- contentpanel -->

