  <div class="mainpanel">
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Single Business Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
	
<div class="pageheader" style="height:60px">
    <a href="<?php echo base_url();?>index.php/sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>index.php/sbp/sbp_print"><i class="fa fa-print"></i>Print Permit</a>
    </div> 
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title">Enter Business ID & Phone No</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('parking/display_car_details',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-6">
                      <label class="col-sm-6 control-label">Registration No.:</label>
                        <input type="text" class="form-control" id="reg_no" name="reg_no" placeholder="Enter Car Registration Number" required />
                      </div>
                      <div class="col-sm-6">
                      <label class="col-sm-6 control-label">Phone Number:</label>
                        <input type="text" class="form-control" id="cust_phone" name="cust_phone" placeholder="Enter Your Phone No." required/>
                      </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Check Status">
            <button type="reset" class="btn btn-primary">Reset</button>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Enter your Business Business Identification Number</p></li>
		   <li>
          <p>Enter your Mobile Phone Number</p></li>
		   <li>
          <p>Check Status</p></li>
		    </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

