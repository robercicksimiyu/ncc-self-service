<div class="pageheader">
      <h2><i class="fa fa-road"></i> Seasonal Parking <span>Parking</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><?php echo anchor('parking/seasonalParking','Seasonal Parking');?></li>
          <li class="active">Vehicle Details</li>
        </ol>
      </div>
    </div>

    
    

    <div class="contentpanel" >
      
          <div class="panel panel-default col-md-8" style="margin-right:20px">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize"></a>
              </div>
              
            </div>
            <?php $balance=$bal; $walletbal=str_replace(",","",$balance);?>
            <?php #if(isset($parking)): ?>
            <div class="panel-body">
              <div class="row">
                <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM THE INFORMATION BELOW</th>
                      </tr>
                    </thead>
                    <?php echo form_open('parking/completeSPPayment'); ?>
                    <tbody>
                      <tr>
                        <td><b>TOTAL NUMBER OF VEHICLES</b></td>
                        <td><?php  echo $parking['res1']->row()->num; ?></td>
                      </tr>
                      <tr>
                        <td><b>TOTAL AMOUNT</b></td>
                        <td><?php echo number_format($parking['res2']->row()->sumamount,2,'.',','); ?></td>
                      </tr>
                      <?php //var_dump($confirm['4']) ?>
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div>
            </div><!-- panel-body -->
                        <?php $amt = $parking['res2']->row()->sumamount; $amnt=number_format($amt, 2, '.', ',');  
                        if($walletbal >$amt || $walletbal >=$amt):?>
                        <div class="panel-footer">
                          <input type="hidden" name="cash" id="cash">
                          <input type="hidden" name="noofvehicles" id="noofvehicles" value="<?php echo $parking['res1']->row()->num; ?>">
                          <input type="hidden" name="customer" id="customer">
                          <input type="hidden" name="amount" id="amount" value="<?php echo $parking['res2']->row()->sumamount; ?>">

                          <div class="col-md-3">
                          <label>County Wallet PIN</label>
                          <input type="password" name="jppin" id="jppin" placeholder="Wallet PIN" class="form-control" required />
                          </div>
                          <br/><input type="submit" id="" value="Complete Payment" class="btn btn-primary" />&nbsp;
                          <?php echo form_close(); ?>
                          <?php echo anchor('parking/seasonal_parking','Cancel',array('class'=>"btn btn-default")); ?>
                        </div>
                      <?php elseif($walletbal<$amt): ?>
                        <div class="panel-footer">
                          <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                   <?php echo "You do not have enough funds in your wallet. "; echo "The balance is: ".$balance;
                                   echo anchor('sbp/wallettopup','Top up Now',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                              </div>
                               <?php echo anchor('parking/seasonal_parking','Go Back',array('class'=>"btn btn-primary")); ?>
                        </div>
                      <?php endif; ?>
          <?php #endif; ?>
          </div>
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li><p>Confirm the transaction details</p></li>
            <li><p>If the details are correct, enter your wallet PIN to complete the transaction or else Cancel</p></li>
          </ol>
            </div>

          </div>
    </div><!-- contentpanel -->


    <!-- Modal -->
<!-- modal -->
<script type="text/javascript">
     
</script>

  