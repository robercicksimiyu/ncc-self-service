<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>e-Construction Fee<span>Pay for your e-Construction Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">e-Construction</a></li>
      <li class="active">Display Invoice Details</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      //$rescode= $construction['rescode'];
      #$construction['invoiceStatus'] = "Paid";

      //if($rescode=="0" && $construction['invoiceStatus']!="paid"):

      ?>      
       <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <?php echo form_open('garbage/payment',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">GABAGE COLLECTION DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>CONTACT PERSON</b></td>
                        <td><?=$invoice_details['contact_person']?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $invoice_details['bid_name'];?></td>                      
                      </tr>
                      <tr>
                        <td><b>LOCATION</b></td>
                        <td>
                        <?php if($invoice_details['bid_location']!=''):?>
                           <?=$invoice_details['bid_location']?>
                        <?php else:?> 
                           <input type="text" name="bid_location" class="form-control" >
                        <?php endif;?>                          
                        </td>
                      </tr>
                      <tr>
                        <td><b>LR Number</b></td>
                        <td>
                        <?php if($invoice_details['bid_lr_no']!=''):?>
                           <?=$invoice_details['bid_lr_no']?>
                        <?php else:?> 
                           <input type="text" name="bid_lr_no" class="form-control" >
                        <?php endif;?>                          
                        </td>
                      </tr>

                      <tr>
                        <td><b>Sub Counties</b></td>
                        <td>
                        <select class="form-control" name="zones" id="zones">
                            <?php foreach($subcounties as $county): ?>
                              <?php if($county['ID']==$invoice_details['zone']):?>
                                   <option selected="selected" value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
                              <?php else:?>
                                   <option value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
                              <?php endif;?>
                             
                            <?php endforeach;?> 
                        </select>
                                                
                        </td>
                      </tr>
                      <tr>
                        <td><b>Wards</b></td>
                        <td>
                           <select class="form-control" name="ward" id="ward" >

                           </select>
                         </td>
                      </tr>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $invoice_details['invoice_number']['ref_number'];?></td>
                        <input type="hidden" name="invoice_number" value="<?=$invoice_details['invoice_number']['ref_number']?>">
                      </tr>                     
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo number_format($invoice_details['amount_paid'], 2, '.', ',');?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>

              <div class="panel-footer">
              <?php if($invoice_details['invoice_number']['status']):?>
                <div class="panel-footer">

                                <div class="row">
                                  <div class="alert alert-info col-md-8" style="margin-top:10px;margin-left:20px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <?php echo "PAID"; ?>
                                    <!-- <a href="<?php echo base_url();?>index.php/selfservice/wallettopup" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-money"></i>Top Up Wallet</a> -->
                                  </div>
                                </div>


                              </div>
              <?php else:?>
                <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('gabage','Cancel',array('class'=>"btn btn-default")); ?>
                </div>


              </div>
          <?php endif;?>    
              <!-- <div class="panel-footer">

                <div class="row">
                  <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Sorry.you do not have sufficient funds in your wallet to continue with the transaction"; ?>
                    <a href="<?php echo base_url();?>index.php/selfservice/wallettopup" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-money"></i>Top Up Wallet</a>
                  </div>
                </div>


              </div> -->
        </div>
      
    </div> 
<script type="text/javascript">
jQuery(document).ready(function(){
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();
      console.log(dat);
    },
    success: function(data) {
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
        $('#ward').append($('<option>', {
          "value": id
        }).text(name));
      }
    },
    error: function(err) {
      console.log(err)
    }
  });

});
</script>



<script type="text/javascript">
$("#zones").change(function(event) {
  $.ajax({
    url: '<?php echo base_url(); ?>sbp/getWard2',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: $("#zones").serialize(),
    beforeSend: function() {
      console.log("Trying....");
      dat = $("#zones").serialize();            
      console.log(dat);
    },
    success: function(data) {
     
      $("#ward").empty();
      var optgroup = data;
      for (var i = 0; i < optgroup.length; i++) {
        var id = optgroup[i].ID;
        var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];               
                if(id=="<?=$invoice_details['bid_location']?>") {
                   $('#ward').append($('<option selected="selected">', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                } else {
                    $('#ward').append($('<option>', {
                     "value": id
                   }).text(name));
                    console.log(id + " " + name);
                  }
                }
              
           },
           error: function(err) {
            console.log(err)
          }
        });
});

$("#ward").change(function(event){
 var code = [120,195,215,210,220,205,200,546];
 var zone = $("#ward").text();

 console.log(zone);



});

</script>
