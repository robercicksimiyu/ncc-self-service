<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Garbage Collection Application<span>Pay for your Garbage Collection Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Gabage Collection</a></li>
      <li class="active">Fill the form below</li>
    </ol>
  </div>
</div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Enter your details</h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('garbage/confirm',array('class' =>"form-block"));  ?>
          <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Contact Person</label>
                 <input type="text" name="contact_person" id="contact_person" class="form-control" placeholder="" required />
                
              </div>
              <div class="col-sm-6">
                <label class="control-label">Phone Number</label>
                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="" required />
              </div>
            </div>
            <div class="form-group">              
              <div class="col-sm-12">
                <label class="control-label">ID Number</label>
                <input type="text" name="id_number" id="id_number" class="form-control" placeholder="" required />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Business ID</label>
                <input type="text" name="bid" id="bid" class="form-control" placeholder="" required />
              </div>            
            </div> 
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Garbage Categories</label>
                <select class="form-control" name="category" id="category_id">
                  <?php foreach($categories as $category):?>
                    <option data-name='<?=$category['name']?>' data-amount='<?=$category['amount']?>' data-id="<?=$category['id']?>" value="<?=$category['id']?>"><?=$category['name']?></option>
                  <?php endforeach;?>
                </select>                
               <!--  <input type="text" name="bid" id="bid" class="form-control" placeholder="" required /> -->
              
              </div>            
            </div> 
            <div class="form-group" id="sub_categories">
              <div class="col-sm-12">
                <label class="control-label" id="sub_category_title"></label>                
                <select id="sub_category" name="sub_category" class="form-control">
                  
                </select>              
              </div>            
            </div> 
            <div class="col-md-6 col-sm-12">
              <p> <a href="http://192.168.11.22/ncc/epaymentsLive/sbp/enter_acc_no"><input type="submit" value="Submit" class="btn btn-primary" style="line-height:normal"> </a> </p>
            </div>
            
            
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li><p>Enter Invoice Number</p></li>
            <li><p>Click Submit</p></li>
          </ol>
        </div>

      </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
      $(document).ready(function(){
        $('#category_id').on('change',function(){
          var $this=$(this);
          if($(this).find(':selected').data('amount')==1) { 
            //$('#sub_category').html();
            $.ajax({
              url:'<?=base_url("garbage/sub_categories")?>/'+$(this).find(':selected').data('id'),
              method:'GET',
              dataType:'json',
              success:function(data){
              $('#sub_category').html('');                
                $.each(data, function (i, item) {
                  //console.log(item.name);
                    $('#sub_category').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                    }));
                });
                //console.log();
                if($this.find(':selected').data('name')=="Hotels") {
                  $('#sub_category_title').html('No. of Rooms');
                } else {
                  $('#sub_category_title').html('No. of Staff');
                }
                $('#sub_categories').show();

              }
            });
          } else if(this.val=='Offices') {
           
          } else {
            $('#sub_categories').hide();
            //$('#sub_category').html();
          }
        });
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        $('#sub_categories').hide();
        var title='No. of rooms';

        $('#cessCode').on('change',function(){
          code=$(this).find('option:selected').text();
          $('#cessDescription').val(code);
              //alert(code);
            });
      });
    </script>
