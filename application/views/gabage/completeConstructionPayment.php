<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>e-Construction Fee<span>Pay for your e-Construction Fee online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">e-Construction</a></li>
      <li class="active">Complete Payment</li>
    </ol>
  </div>
</div>

  <div class="contentpanel" >
      
      <div class="row">
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment <?php #echo $response = $construction['restext'];?></h4>
              </div>
              <?php $response = $construction['rescode'];
              
                if ($response=="0"): ?>
                  <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Your Transaction was Successful";
                    $receiptno=str_replace('/','-',$construction['receiptno']);
                    echo anchor('econstruction/econstructionreceiptprint/'.$receiptno,'Print Receipt',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>

                <?php elseif($response=="4440" || $response=="44405" || $response=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
              
              <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered a Wrong Wallet PIN"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php elseif($response=="1053" ): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The transaction has already been completed."; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
            <?php elseif($response=="2030" ): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Insufficient funds in your wallet. Your balance is Ksh. $bal"; ?>
                <a href="<?php echo base_url();?>selfservice/wallettopup" target="_blank" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Topup Wallet</a>
              </div>
            <?php elseif($response=="1055" ): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Invalid Bill Status."; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" target="_blank" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-hdd-o"></i>Back</a>
              </div>
              <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div><!-- contentpanel -->

    

