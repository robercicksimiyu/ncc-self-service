<!-- <div class="mainpanel"> -->
 <div class="pageheader">
      <h2><i class="fa fa-home"></i> Gabbage Collection<span>Pay your Gabbage Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Gabbage Collection</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
    <div class="row">
    <?php $response = $trans_details['code'];?>
    <?php $restext = $trans_details['info'];?>

    
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment <?php #echo $response = $complete['rescode'];?></h4>
              </div>
              <?php if($response==200): ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Transaction was successfully completed.".anchor('garbage/printreceipt/'.$trans_details['invoice'],'Print Receipt',array('class'=>'btn btn-primary','style'=>'float:right;padding-top:1px;'));
                  ?>                 
              </div>
            <?php else: ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo  "Error Code ".$response. " : ".$restext; ?>
              </div>
            <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div>

  <!-- contentpanel -->


    
</div><!-- mainpanel -->

