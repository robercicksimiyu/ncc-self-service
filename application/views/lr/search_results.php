<!-- <div class="mainpanel"> -->
<div class="pageheader">
  <h2><i class="fa fa-home"></i> Land Rates <span>Search</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Land Rates</a></li>
      <li class="active">Search</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
  <?php 
   $res= $search['rescode'];
   if($res=="0"):
    ?>
   <div class="panel panel-default col-md-8" style="margin-right:20px">
    <div class="panel-body">
      <div class="row">
        <div class="table-responsive">
          <table class="table table-striped mb30">
            <thead>
              <tr>
                <th colspan="2" style="text-align:center;">SEARCH DETAILS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>CUSTOMER NAME</b></td>
                <td><?php echo $search['CustomerName'];?></td>
              </tr>
              <tr>
                <td><b>PLOT NUMBER</b></td>
                <td><?php echo $search['PlotNumber'];?></td>
              </tr>
              <tr>
                <td><b>PHYSICAL ADDRESS</b></td>
                <td><?php echo $search['PhysicalAddress'];?></td>
              </tr>
              <tr>
                <td><b>PLOT USE DESCRIPTION</b></td>
                <td><?php echo $search['PUseDescription'];?></td>
              </tr>
              <tr>
                <td><b>UPN</b></td>
                <td><?php echo $search['UPN'];?></td>
              </tr>
              <tr>
                <td><b>LR NUMBER</b></td>
                <td><?php echo $search['LRNumber'];?></td>
              </tr>
              <tr>
                <td><b>LAND RATES</b></td>
                <td><?php echo $search['LandRates'];?></td>
              </tr>
              <tr>
                <td><b>TOTAL ANNUAL AMOUNT</b></td>
                <td><?php echo $search['TotalAnnualAmount'];?></td>
              </tr>
              <tr>
                <td><b>ACCUMULATED PENALTY</b></td>
                <td><?php echo $search['AccumulatedPenalty'];?></td>
              </tr>
              <tr>
                <td><b>CURRENT BALANCE</b></td>
                <td><?php echo $search['CurrentBalance'];?></td>
              </tr>
              <tr>
                <td><b>ADJUSTMENT</b></td>
                <td><?php echo $search['Adjustment'];?></td>
              </tr>
              <tr>
                <td><b>GROUND RENT</b></td>
                <td><?php echo $search['GroundRent'] ;?></td>
              </tr>
              <tr>
                <td><b>OTHER CHARGES</b></td>
                <td><?php echo $search['OtherCharges'];?></td>
              </tr>
              <tr>
                <td><b>AMOUNT TO PAY</b></td>
                <td><?php echo $search['AmountToPay'];?></td>
              </tr>
              <tr>
                <td><b>DUE DATE</b></td>
                <td><?php echo $search['DueDate'] ;?></td>
              </tr>
              <tr>
                <td><b>TOTAL ARREARS</b></td>
                <td><?php echo $search['TotalArrears'];?></td>
              </tr>
              <tr>
                <td><b>TRANSACTION ID</b></td>
                <td><?php echo $search['TransactionID'];?></td>
              </tr>
              
              <tr>
                <td><b>WAIVER</b></td>
                <td><?php echo $search['Waiver'];?></td>
              </tr>
              <tr>
                <td><b>COMMISSION</b></td>
                <td><?php echo $search['Commission'];?></td>
              </tr>
              <tr>
                <td><b>RECEIPT AMOUNT</b></td>
                <td><?php echo $search['ReceiptAmount'];?></td>
              </tr>
              <tr>
                <td><b>RECEIPT NO</b></td>
                <td><?php echo $search['ReceiptNo'];?></td>
              </tr>
              <tr>
                <td><b>TRANSACTION DATE</b></td>
                <td><?php echo $search['TransactionDate'];?></td>
              </tr>
            </tbody>
          </table>
        </div><!-- table-responsive -->
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
              <?php elseif($res=="4440" || $res=="44405" || $res=="44406"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
                </div>
              <?php elseif($res=="2025"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "You have entered a Wrong Wallet PIN";
                  echo anchor('lr/search','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
                </div>
              <?php elseif($res=="1053"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "The transaction has already been completed."; 
                  echo anchor('lr/search','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
                </div>
              <?php endif; ?>
            </div>
          </div>
<!--   <div class="panel panel-default col-md-3" >
    <div class="panel-heading">
      <div class="panel-btns">
        <a href="#" class="panel-close">&times;</a>

      </div>
      <h4 class="panel-title">Thank you for using Nairobi City County e-Payments.</h4>
    </div>

  </div> -->
</div>
          <!-- contentpanel -->

