<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Land Rates <span>Search</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Land Rates</a></li>
          <li class="active">Search</li>
        </ol>
      </div>
    </div>
    
     <!-- <div class="pageheader" style="height:60px">
    <a href="<?php #echo base_url();?>sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>
    </div> -->
    <div class="contentpanel" >
    <?php 

       $res= $search['rescode'];

       if($res==0):

      ?>

      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  
              <?php echo form_open('lr/complete_search_payment'); ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM THE DETAILS BELOW</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>OWNER NAME</b></td>
                        <td><?php echo $search['wname']; ?></td>
                      </tr>
                      <!-- <tr>
                        <td><b>TRANSACTION ID</b></td>
                        <td><?php #echo $search['transid1']; ?></td>
                      </tr> -->
                      <tr>
                        <td><b>SEARCH FEE</b></td>
                        <td><?php echo number_format($search['sfee'],2); ?></td>
                        <input type="hidden" name="trans" value="<?php echo $search['transid1']; ?>">
                        <input type="hidden" name="no" value="<?php echo $search['JPnumber']; ?>">
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
              <div class="col-sm-6">
              <input class="form-control" type="password" name="jp_pin" id="jp_pin" placeholder="PIN" required />
              <!-- <input class="form-control" type="text" name="paid_amount" id="paid_amount" placeholder="Amount" required /> -->
              </div>
              <input type="submit" value="Confirm Transaction" class="btn btn-primary" style="line-height:normal">
              <?php echo form_close(); ?>
              <?php echo anchor('lr/search','Cancel Transaction',array('class'=>"btn btn-primary",'style'=>'line-height:21px')); ?>
            </div>
      
    </div><!-- contentpanel -->
   
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Confirm the Details </p></li>
		   <li>
          <p>Continue to make Payments</p></li>

		    </ol>
        </div>
    
  </div><!-- mainpanel -->
     <?php else: ?>
                <!-- <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  Error...<a href="<?php #echo base_url();?>lr/search">Go Back</a>
                </div> -->
                <?php endif; ?>