<!-- <div class="mainpanel"> -->
  <div class="pageheader">
    <h2><i class="fa fa-home"></i> Land Rates <span>Pay for your Plot Online</span></h2>
    <div class="breadcrumb-wrapper">
      <span class="label">You are here:</span>
      <ol class="breadcrumb">
        <li><a href="#">Land Rates</a></li>
        <li class="active">Check Status</li>
      </ol>
    </div>
  </div>
  
  <div class="contentpanel" >
    
    <?php 

     $state= $datax['resultcode'];

     if($state=="0"):

      ?>
     <div class="panel panel-default col-md-8" style="margin-right:20px">
       
      <div class="panel-body">
        <div class="row">

          
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
                <tr>
                  <th colspan="2" style="text-align:center;">PLOT DETAILS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>PLOT NUMBER</b></td>
                  <td><?php echo $datax['pno'];?></td>
                </tr>
                <tr>
                  <td><b>PLOT OWNER</b></td>
                  <td><?php echo $datax['ownername'];?></td>
                </tr>
                <tr>
                  <td><b>PHYSICAL ADDRESS</b></td>
                  <td><?php echo $datax['address'];?></td>
                </tr>
                <tr>
                  <td><b>PLOT USE DESCRIPTION</b></td>
                  <td><?php echo $datax['description'];?></td>
                </tr>
                <tr>
                  <td><b>UPN</b></td>
                  <td><?php echo $datax['upn'];?></td>
                </tr>
                <tr>
                  <td><b>PENALTY</b></td>
                  <td><?php echo number_format(($datax['penalty']), 2, '.', ',') ;?></td>
                </tr>
                <tr>
                  <td><b>ADJUSTMENT</b></td>
                  <td><?php echo number_format(($datax['adjustment']), 2, '.', ',') ;?></td>
                </tr>
                <tr>
                  <td><b>GROUND RENT</b></td>
                  <td><?php echo number_format(($datax['groundrent']), 2, '.', ',') ;?></td>
                </tr>
                <tr>
                  <td><b>RATE</b></td>
                  <td><?php echo number_format(($datax['rate']), 2, '.', ',') ;?></td>
                </tr>
                <tr>
                  <td><b>RATE NUMBER</b></td>
                  <td><?php echo $datax['ratenumber'] ;?></td>
                </tr>
                <tr>
                  <td><b>ANNUAL AMOUNT</b></td>
                  <td><?php echo number_format(($datax['annualamount']), 2, '.', ',') ;?></td>
                </tr>
                <tr>
                  <td><b>TOTAL ARREARS</b></td>
                  <td><?php echo number_format($datax['arrears'],2,'.',',');?></td>
                </tr>
                <tr>
                  <td><b>CURRENT BALANCE</b></td>
                  <td><?php echo number_format($datax['balance'],2,'.',',');?></td>
                </tr>
                <tr>
                  <td><b>OTHER CHARGES</b></td>
                  <td><?php echo number_format($datax['othercharges'],2,'.',',');?></td>
                </tr>
                <!-- <tr>
                  <td><b>WAIVER</b></td>
                  <td><?php echo number_format($datax['waiver'],2,'.',',');?></td>
                </tr> -->
                <tr>
                  <td><b>WAIVER %</b></td>
                  <td><?php echo number_format($datax['WaiverPercentage'],2,'.',',');?></td>
                </tr>
                <tr>
                  <td><b>ENTITLED WAIVER</b></td>
                  <td><?php echo number_format($datax['EntitledWaiver'],2,'.',',');?></td>
                </tr>
                <tr>
                  <td><b>MINIMUM AMOUNT FOR WAIVER</b></td>
                  <td><?php echo number_format($datax['MinimumAmountForWaiver'],2,'.',',');?></td>
                </tr>
                <tr>
                  <td><b>MESSAGE</b></td>
                  <td><?php if(empty($datax['Message'])){echo "N/A";}else{echo $datax['Message'];}?></td>
                </tr>
                <!-- <tr>
                  <td><b>AMOUNT TO PAY</b></td>
                  <td><?php echo number_format($datax['amountdue'],2,'.',',');?></td>
                </tr> -->
                <tr>
                  <td><b>DUE DATE</b></td>
                  <td><?php echo $datax['duedate'];?></td>
                </tr>
              </tbody>
            </table>
          </div><!-- table-responsive -->
          
        </div>
        
        <div class="panel-footer">
          
          <div class="row">
               <?php #if($datax['bill_stat']=="NEW"){
                echo form_open('lr/confirm_details',array('class' =>"form-block ",'name'=>"frmDisplay",'id'=>"frmDisplay"));
              echo'<div class="form-group">';
               echo '<label class="col-sm-3 control-label" style="text-align:right;">Amount to Pay</label>';
                echo '<div class="col-sm-4">';
                echo '<input type="text" id="paid_amount" placeholder="Enter Amount to Pay" name="paid_amount" class="form-control" required/>';
                echo '<input type="hidden" id="annualamount" value="'.$datax['annualamount'].'" name="annualamount" class="form-control" required/>';
                echo '<input type="hidden" id="plotnum" value="'.$datax['pno'].'" name="plotnum" class="form-control" required/>';
                echo '<input type="hidden" id="plotowner" value="'.$datax['ownername'].'" name="plotowner" class="form-control" required/>';
                echo '<input type="hidden" id="transid" value="'.$datax['trans_id'].'" name="transid" class="form-control" required/>';
                echo '<input type="hidden" id="minimumamountforwaiver" value="'.$datax['MinimumAmountForWaiver'].'" name="minimumamountforwaiver" class="form-control" required/>';
                echo '<input type="hidden" id="penalties" value="'.$datax['penalty'].'" name="penalties" class="form-control" required/>';
                echo '<input type="hidden" id="balance" value="'.$datax['balance'].'" name="balance" class="form-control" required/>';
                echo '<input type="hidden" id="entitledwaiver" value="'.$datax['EntitledWaiver'].'" name="entitledwaiver" class="form-control" required/>';
                echo '</div>';
                echo '<input type="submit" id=""  value="Proceed" class="btn btn-primary">';
                echo " ";
                echo anchor('lr/enter_plot_no','Cancel',array('class'=>"btn btn-danger"));
              echo '</div>';
                
                echo form_close();

                ?>
            </div>
            
            
          </div>
        </div>
        
      </div><!-- contentpanel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <h4 class="panel-title">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Confirm that the Plot details displayed are correct</p></li>
              <li>
                <p>Enter the amount you want to pay, it must be more than 50/=</p></li>
                <li>
                  <p>Enter your County Wallet Pin</p></li>
                  <li>
                    <p>Complete the Payment or Cancel to go back</p></li>
                  </ol>
                </div>
                
              </div>
            </div><!-- mainpanel -->
<?php elseif($datax['resultcode']=="4440" || $datax['resultcode']=="44405" || $datax['resultcode']=="44406"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon.";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
                </div>
              <?php elseif($datax['resultcode']=="1110"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid property number.";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
              <?php elseif($datax['resultcode']=="2021"): ?>
                <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid Phone Number.";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
                </div>
              <?php endif; ?>