  <!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Land Rates <span>Pay for your Plot Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Land Rates</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
  
<!-- <div class="pageheader" style="height:60px">
    </div> --> 
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Enter Plot Number</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('lr/display_plot_details',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-8">
                      <label class="col-sm-6 control-label">Plot Number</label>
                        <input type="text" class="form-control" id="plot_no" name="plot_no" placeholder="Enter Plot Number" required/>
                      </div>
                      <!-- <div class="col-sm-6">
                      <label class="col-sm-6 control-label">Phone Number</label>
                        <input type="text" class="form-control" id="cust_phone" name="cust_phone" placeholder="Enter Your Phone No." required/>
                      </div> -->
            </div>
            <input type="submit" class="btn btn-primary" value="Check Status">
            <button type="reset" class="btn btn-default">Reset</button>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Enter your Plot Number</p></li>
       <li>
          <p>Enter your Mobile Phone Number</p></li>
       <li>
          <p>Click Submit and Wait for your Plot Details</p></li>
        </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">

// $('#frm').submit(function() 
// {
//     if ($.trim($("#biz_id").val()) === "" || $.trim($("#cust_phone").val()) === "") {
//         alert('Please fill in the blank fields.');
//     return false;
//     }
// });

</script>
  