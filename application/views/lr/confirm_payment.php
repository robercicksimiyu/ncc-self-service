<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Land Rates <span>Pay for your Plot Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Land Rates</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >

      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
              <?php #$confirm['amountentered']=100;$confirm['minimumamountforwaiver']=99;
              if($confirm['amountentered']>=$confirm['minimumamountforwaiver']){
                $entitledwaiver = $confirm['entitledwaiver'];
              }elseif ($confirm['amountentered']<$confirm['minimumamountforwaiver']) {
                $entitledwaiver = "0";
              }
              //$amounttopay = $confirm['amountentered'] + $entitledwaiver;
              ?>
                  
              <?php echo form_open('lr/complete_payment'); ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM THE INFORMATION BELOW</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>PLOT NUMBER</b></td>
                        <td><?php echo $confirm['plotno']; ?></td>
                        <input type="hidden" id="plotno" value="<?php echo $confirm['plotno']; ?>" name="plotno" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>PENALTIES</b></td>
                        <td><?php echo $confirm['penalties']; ?></td>
                        <input type="hidden" id="penalties" value="<?php echo $confirm['penalties']; ?>" name="penalties" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>ANNUAL AMOUNT</b></td>
                        <td><?php echo $confirm['annualamount']; ?></td>
                        <input type="hidden" id="annualamount" value="<?php echo $confirm['annualamount']; ?>" name="annualamount" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>BALANCE</b></td>
                        <td><?php echo $confirm['balance']; ?></td>
                        <input type="hidden" id="balance" value="<?php echo $confirm['balance']; ?>" name="balance" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>MINIMUM AMOUNT FOR WAIVER</b></td>
                        <td><?php echo $confirm['minimumamountforwaiver']; ?></td>
                        <input type="hidden" id="minimumamountforwaiver" value="<?php echo $confirm['minimumamountforwaiver']; ?>" name="minimumamountforwaiver" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>AMOUNT YOU WANT TO PAY</b></td>
                        <td><?php echo $confirm['amountentered']; ?></td>
                        <input type="hidden" id="amountentered" value="<?php echo $confirm['amountentered']; ?>" name="amountentered" class="form-control"/>
                      </tr>
                      <tr>
                        <td><b>WAIVER AMOUNT AWARDED</b></td>
                        <td><?php echo $entitledwaiver; ?></td>
                        <input type="hidden" name="entitledwaiver" value="<?php echo $entitledwaiver; ?>">
                        <input type="hidden" id="transid" value="<?php echo $confirm['transid']; ?>" name="transid" class="form-control"/>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
              <?php
              echo'<div class="form-group">';
              echo '<label class="col-sm-3 control-label" style="text-align:right;">County Wallet PIN</label>';
                echo '<div class="col-sm-4">';
                echo '<input type="password" id="jp_pin" placeholder="Enter County Wallet PIN" name="jp_pin" class="form-control" required/>';
                echo '</div>';
                echo '<input type="submit" id="bill_status"  value="Complete Payment" class="btn btn-primary">';
                echo " ";
                echo anchor('lr/enter_plot_no','Cancel',array('class'=>"btn btn-danger"));
              echo '</div>';
                
              echo form_close(); ?>
            </div>
      
    </div><!-- contentpanel -->
	  <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->

        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li><p>Confirm your payment details</p></li>
          <li><p>Enter your e-Wallet PIN to complete payment</p></li>
          </ol>
      </div>
    
  </div><!-- mainpanel -->
    <script type="text/javascript">

// $('#frmDisplay').submit(function() 
// {
//     if ($.trim($("#jp_pin").val()) === "") {
//         alert('Please fill in your wallet PIN.');
//     return false;
//     }
// });

</script>