<div class="pageheader">
      <h2><i class="fa fa-inbox"></i>Receipts Portal - Land Rates</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Landrates</a></li>
          <li class="active">Print receipt</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="minimize">&minus;</a> -->
          </div>
          <h4 class="panel-title">Fill in the transaction Details below    <?php #echo $dates="2015-01-06 00:00:00"; echo date("Y-m-d",$dates) ;?></h4>
        </div>
        <div class="panel-body">
          <?php echo form_open('lr/insertManualReceipt',array('class' =>"form-block")) ?>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Plot Number</h4>
               <input type="text" value="" id="plotnum" name="plotnum" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Plot Owner</h4>
              <input type="text" value="" id="plotowner" name="plotowner" class="form-control" style="text-transform: uppercase;"  required/>
              </select>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Date Paid</h4>
              <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="issuedate" required/>
              <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> -->
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Receipt Number</h4>
              <input type="text" value="" id="receiptno" name="receiptno" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
            <div class="col-sm-5">
              <h4 class="subtitle mb5">Penalties</h4>
              <input type="text" value="" id="penalties" name="penalties" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Paid By</h4>
              <input type="text" value="" id="paidby" name="paidby" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Amount Paid</h4>
              <input type="text" value="" id="amount" name="amount" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-4">
              <h4 class="subtitle mb5">Amount Due</h4>
              <input type="text" value="" id="amountdue" name="amountdue" class="form-control" style="text-transform: uppercase;" required/>
            </div>
            <div class="col-sm-3">
              <h4 class="subtitle mb5">Phone No.</h4>
              <input type="text" value="" id="phonenumber" name="phonenumber" class="form-control" style="text-transform: uppercase;"  required/>
            </div>
          </div>
            <div id="proc">
              <input type="submit" class="btn btn-primary" value="Save">
            </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
          <!-- <a href="#" class="panel-close">&times;</a> -->
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li><p>Fill in all the details correctly</p></li>
          <li><p>Click save</p></li>
        </ol>
      </div>
            
          </div>
    </div><!-- contentpanel --> 
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

        // Date Picker
        jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
        
        jQuery('#datepicker-inline').datepicker();
        
        jQuery('#datepicker-multiple').datepicker({
          numberOfMonths: 3,
          showButtonPanel: true
        });
      });
    </script>
