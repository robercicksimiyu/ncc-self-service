<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Land Rates <span>Search</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Land Rates</a></li>
          <li class="active">Complete Search</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      
      <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment</h4>
              </div>
              <?php $response = $search['rescode'];
              
                if ($response=="0"): ?>
                  <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Your Transaction was Successful";
                    #$receiptno=str_replace('/','-',$complete['receiptno']);
                    #echo anchor('parking/printDreceipt/','Print Receipt',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>

                <?php elseif($response=="4440" || $response=="44405" || $response=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
              
              <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered a Wrong Wallet PIN";
                echo anchor('lr/search','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
              <?php elseif($response=="1053"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The transaction has already been completed."; 
                echo anchor('lr/search','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
              <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div>        
    </div>
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
         
          </div>
          <h4 class="panel-title">Thank you for using Nairobi City County e-Payments.</h4>
        </div>
    
  </div>
   </div>
      
    </div><!-- contentpanel -->
		
  </div><!-- mainpanel -->


</script>