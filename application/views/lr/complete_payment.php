<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Land Rates <span>Pay for your Plot Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Land Rates</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>
    
    <!--  <div class="pageheader" style="height:60px">
    <a href="<?php #echo base_url();?>sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>
    </div> -->
    <div class="contentpanel" >
      
      <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment <?php #echo $complete['rescode'];?></h4>
              </div>
              <?php $response = $complete['rescode'];
              
                if ($response=="0"): ?>
                  <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Your Transaction was Successfully completed.";
                    $receiptno=str_replace('/','-',$complete['receiptnum']);
                    echo anchor('lr/printdirect/'.$receiptno,'Print Receipt',array('class'=>"btn btn-primary",'style'=>"float:right;padding-top:1px;")); ?>
                  </div>

                <?php elseif($response=="4440" || $response=="44405" || $response=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
              <?php elseif($response=="1110"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid Property Number";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
            <?php elseif($response=="1061"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Amount is less minimum allowed of Ksh 50";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
              <?php elseif($response=="2025"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered a Wrong Wallet PIN";
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
              <?php elseif($response=="1053"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "The transaction has already been completed."; 
                echo anchor('lr/enter_plot_no','Back',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
            <?php elseif($response=="2030"): ?>
              <div class="alert alert-danger " style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry.You do not have enough funds in your wallet."; 
                echo anchor('selfservice/wallettopup','Topup',array('class'=>"btn btn-primary receipt",'style'=>"float:right;"));?>
              </div>
              <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div>
        
    </div>
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Thank you for using Nairobi City County e-Payments.</h4>
        </div>
    
  </div>
   </div>
      
    </div><!-- contentpanel -->
		
  </div><!-- mainpanel -->


</script>