 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >
	<div class="col-md-9 col-sm-12 col-xs-12">
		<div class="panel panel-dark">
            <div class="panel-heading">              
              <h3 class="panel-title">Environment Sector</h3>
            </div>
            <div class="panel-body">
              <div class="btn-demo">
				<a href="<?=base_url('environment/apply/'.$this->lang->line('hire_parks_form'))?>" class="btn btn-lg btn-warning-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_hire_parks')?></a>				
                <a class="btn btn-lg btn-lg btn-success-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_landscaping_fees')?></a>
                <a class="btn btn-lg btn-success-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_quarrying_permit')?></a>
                <a class="btn btn-lg btn-default-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_recycling_permit')?></a>
                <a class="btn btn-lg btn-default-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_tip_charges')?></a>
                <a class="btn btn-lg btn-success-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_saleof_flowers')?></a>
				 <a class="btn btn-lg btn-info-alt col-md-10 col-sm-12 col-xs-12"><?=$this->lang->line('text_construction_soil')?></a>
                <a class="btn btn-lg btn-default-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_tree_cutting')?></a>
                <a class="btn btn-lg btn-success-alt col-md-5 col-sm-12 col-xs-12"><?=$this->lang->line('text_waste_collection')?></a>
              </div>
            </div>
     </div>
	</div>	 
    <div class="col-md-3 col-sm-12 col-xs-12">
		<div class="panel panel-dark">
            <div class="panel-heading">              
              <h3 class="panel-title">Instructions</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
	</div>
   

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

<script type="text/javascript">
$(document).ready(function(){
  $("#zone").change(function(event) {
    getWards("<?=base_url('services/get_wards')?>");
  });
  validateForm('#frm_application');
});
</script>
