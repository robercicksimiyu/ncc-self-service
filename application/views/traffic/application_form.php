 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> <?=$title?> <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href=""><?=$title?></a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>   

    <div class="contentpanel" >
		<div class="col-md-9 col-sm-12 col-xs-12">
			<div class="panel panel-dark">
				<div class="panel-heading">				  
				  <h3 class="panel-title">Application Form</h3>
				</div>
				<div class="panel-body">
				  <?php echo form_open('traffic/process',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm_application")) ?>         
					  <div class="row mb10">
						<div class="col-md-6 col-sm-12">
						  <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
						  <input type="text" class="form-control" id="env_tx_fullname" name="Applicant_name" placeholder="John Doe" required />
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Contact Email</label>
						  <input type="email" class="form-control" id="env_tx_email" name="Address" placeholder="you@example.com" required/>
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-12 control-label">Contact Phone No.</label>
						  <input type="text" class="form-control" id="env_tx_phoneno" name="Tel_no" placeholder="0700000000" required />
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-12 control-label"><?=$this->lang->line('text_bid');?></label>
						  <input type="text" class="form-control" id="env_tx_phoneno" name="Bid" placeholder="0888667" required />
						</div>   						
						
						</div>
						<div class="row mb10">            
						<div class=" col-md-12 col-sm-12">
						  <label class="col-sm-6 control-label">Purpose</label>
						  <textarea class="form-control" id="env_tx_description" name="purpose" placeholder="Write a short description e.g Hiring ground for raising funds for the needy" required></textarea>
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label">Sub Counties</label>
						  <select class="form-control" name="zone" id="zone" required>
								<option value="">Select one ...</option>  
								<?php foreach($subcounties as $county): ?>

								  <?php if($county['ID']==$invoice_details['zone']):?>
									   <option data-bid="<?=$county['Bid']?>" selected="selected" value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
								  <?php else:?>
									   <option value="<?php echo $county['ID']; ?>"><?php echo $county['Name']; ?></option>
								  <?php endif;?>
								 
								<?php endforeach;?> 
							</select>
						 </div>
						 <div class=" col-md-6 col-sm-12">
						   <label class="col-sm-6 control-label">Wards</label>
						   <select class="form-control" name="Subcounty" id="ward" required >
							<option value="">Select Sub County</option>
						   </select>
						 </div>
										 
					  <div class="col-md-6 col-sm-12">
						<label class=" col-sm-8 control-label"><?=$this->lang->line('text_commencement_date');?></label>
						<input type="text" class="form-control" id="start_date" name="commencement_date" placeholder="2017-11-12" required/>
					  </div>					  
					  <div class="col-md-6 col-sm-12">
						<label class=" col-sm-8 control-label"><?=$this->lang->line('text_completion_date');?></label>
						<input type="text" class="form-control" id="end_date" name="completion_date" placeholder="2017-12-12" required/>
					  </div>
					   <div class="col-md-6 col-sm-12">
						  <label class="control-label"><?=$this->lang->line('text_lr');?></label>
						  <input class="form-control" name="lrNo" id="Location" required >             
						</div>
						<div class="col-md-6 col-sm-12">
						  <label class="col-sm-5 control-label"><?=$this->lang->line('text_road_name');?></label>
						  <input type="text" class="form-control" id="Week_no" name="road_name" placeholder="eg. Standard Street" required/>
						</div> 
						
						<div class="col-md-12 col-sm-12">
						  <label class="col-sm-12 control-label"><?=$this->lang->line('text_area_location');?></label>
						  <input type="text" class="form-control" id="Week_no" name="location_plan" placeholder="" required/>
						</div>
						
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"><?=$this->lang->line('text_carriageway')?> <?=$this->lang->line('text_width');?></label>
							  <input class="form-control" name="Carriageway_width" placeholder="0" id="Location" required >             
							</div>
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_carriageway')?> <?=$this->lang->line('text_length');?></label>
							  <input type="text" class="form-control" name="Carriageway_length" placeholder="0" required/>
							</div>							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_carriageway')?> <?=$this->lang->line('text_depth');?></label>
							  <input type="text" class="form-control" name="Carriageway_depth" placeholder="0" required/>
							</div>
						
						
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"><?=$this->lang->line('text_footpath')?> <?=$this->lang->line('text_width');?></label>
							  <input class="form-control" name="footpath_width"  placeholder="0"  required >             
							</div>
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_footpath')?> <?=$this->lang->line('text_length');?></label>
							  <input type="text" class="form-control" id="Week_no" name="footpath_length" placeholder="0" required/>
							</div>							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_footpath')?> <?=$this->lang->line('text_depth');?></label>
							  <input type="text" class="form-control" id="Week_no" name="footpath_depth" placeholder="0" required/>
							</div>
							
							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"><?=$this->lang->line('text_verge')?> <?=$this->lang->line('text_width');?></label>
							  <input class="form-control" name="verge_width"  placeholder="0" required >             
							</div>
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_verge')?> <?=$this->lang->line('text_length');?></label>
							  <input type="text" class="form-control"  name="verge_length" placeholder="0" required/>
							</div>							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_verge')?> <?=$this->lang->line('text_depth');?></label>
							  <input type="text" class="form-control"  name="verge_depth" placeholder="0" required/>
							</div>
							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"><?=$this->lang->line('text_other')?> <?=$this->lang->line('text_width');?></label>
							  <input class="form-control" name="other_width" placeholder="0"required >             
							</div>
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_other')?> <?=$this->lang->line('text_length');?></label>
							  <input type="text" class="form-control" id="Week_no" name="other_length" placeholder="0" required/>
							</div>							
							<div class="col-md-4 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_other')?> <?=$this->lang->line('text_depth');?></label>
							  <input type="text" class="form-control" name="other_depth" placeholder="0" required/>
							</div>

							<div class="col-md-6 col-sm-12">
							  <label class="control-label"> <?=$this->lang->line('text_surface_works')?> </label>
							  <input type="text" class="form-control"  name="surface_works" required/>
							</div> 							
					 
						<input type="hidden" name="Category_Id" value="<?=$category_id?>">
					</div>     
				  <div id="sub">
					<input type="submit" class="btn btn-primary" value="Submit" >
				  </div>
				 <?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12">
			<div class="panel panel-dark">
            <div class="panel-heading">             
              <h3 class="panel-title">Panel Title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
		</div>		

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->
 <script type="text/javascript">
    $(document).ready(function(){
      jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
      // Date Picker
      jQuery('#start_date').datepicker({ dateFormat: 'yy-mm-dd' });
	  
	  jQuery('#end_date').datepicker({ dateFormat: 'yy-mm-dd' });
      
      jQuery('#datepicker-inline').datepicker();
      
      jQuery('#datepicker-multiple').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(){
  jQuery('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
  $("#zone").change(function(event) {
    getWards("<?=base_url('services/get_wards')?>");
  });
  //validateForm('#frm_application');
});
</script>
