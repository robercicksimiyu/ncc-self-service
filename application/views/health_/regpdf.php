<?php

    include 'pdf/fpdf.php';
    include 'pdf/fpdi.php';

    // initiate FPDI
    $pdf = new FPDI();


    $date = date('Y-m-d');
    //$customer = substr($customerName, 0, 17);

    // BUILDING STRUCTURES ///////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');

    // import page 1 ////////////////////////////////
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    // //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244);

    //$customer = substr($customerName, 0, 17);
    $pdf->SetXY(36, 68);
    $pdf->Write(0, "REFNUMBER");
    $pdf->SetXY(36, 73);
    $pdf->Write(0, $date);
    $pdf->SetXY(52, 107);
    $pdf->Write(0, "REGISTERNUMBER");
    $pdf->SetXY(65, 120);
    $pdf->Write(0, "LRNUMBER");


    // import page 2 /////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(2);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

    $pdf->SetXY(143, 26);
    $pdf->Write(0, "PLANREGNO");

    $pdf->SetXY(70, 129);
    $pdf->Write(0, "Architect");
    $pdf->SetXY(161, 129);
    $pdf->Write(0, "REGNUMBER");
    $pdf->SetXY(67, 134);
    $pdf->Write(0, "Email");
    $pdf->SetXY(167, 134);
    $pdf->Write(0, "Mobile NUMBER");
    $pdf->SetXY(60, 139);
    $pdf->Write(0, "PO Box");

    $pdf->SetXY(73, 154);
    $pdf->Write(0, "Engineer");
    $pdf->SetXY(162, 154);
    $pdf->Write(0, "REGNUMBER");
    $pdf->SetXY(64, 160);
    $pdf->Write(0, "EMAIL");
    $pdf->SetXY(167, 160);
    $pdf->Write(0, "Mobile NUMBER");
    $pdf->SetXY(60, 165);
    $pdf->Write(0, "PO Box");

    $pdf->SetXY(65, 189);
    $pdf->Write(0, "Owner");
    $pdf->SetXY(143, 180);
    $pdf->Write(0, "DATE");
    $pdf->SetXY(155, 189);
    $pdf->Write(0, "EMAIL");
    $pdf->SetXY(165, 195);
    $pdf->Write(0, "Mobile NUMBER");
    $pdf->SetXY(60, 165);
    $pdf->Write(0, "PO Box");

    // import page 3 ///////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(3);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

    $pdf->SetXY(69, 28);
    $pdf->Write(0, "Current Land Use");
    $pdf->SetXY(137, 28);
    $pdf->Write(0, "Zone");

    $pdf->SetXY(88, 37);
    $pdf->Write(0, "Project Description XXXXXXXXXXXXXXXXXXXXXXXXXXX");
    $pdf->SetXY(40, 42);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    $pdf->SetXY(40, 46);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");


    $pdf->SetFont('Arial', '', 14);
    $pdf->SetXY(36, 62);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 67);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 72);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 77);
    $pdf->Write(0, "0");
    $pdf->SetFont('Arial', '', 11);


    $pdf->SetXY(133, 91);
    $pdf->Write(0, "Number Of Units");
    $pdf->SetXY(67, 96);
    $pdf->Write(0, "LR NUMBER");
    $pdf->SetXY(150, 96);
    $pdf->Write(0, "PLot Size");
    $pdf->SetXY(95, 105);
    $pdf->Write(0, "Nearest Road");
    $pdf->SetXY(78, 110);
    $pdf->Write(0, "Estate");
    $pdf->SetXY(144, 110);
    $pdf->Write(0, "Sub County");
    $pdf->SetXY(66, 114);
    $pdf->Write(0, "Type Of Soil");


    $pdf->SetXY(71, 124);
    $pdf->Write(0, "Water Supplier");
    $pdf->SetXY(157, 124);
    $pdf->Write(0, "Sewerage Disposal");
    $pdf->SetXY(104, 129);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

    $pdf->SetXY(93, 202);
    $pdf->Write(0, "Project Cost");
    $pdf->SetXY(84, 207);
    $pdf->Write(0, "Inspection Fees");

    $pdf->SetXY(76, 217);
    $pdf->Write(0, "Foundation");
    $pdf->SetXY(76, 222);
    $pdf->Write(0, "External Walls");
    $pdf->SetXY(76, 227);
    $pdf->Write(0, "Mortar");
    $pdf->SetXY(76, 232);
    $pdf->Write(0, "Roof Cover");
    $pdf->SetXY(82, 237);
    $pdf->Write(0, "Damp Proof");


    // INDEMNITY ////////////////////////////////////////

    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/idemity.pdf');
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
    // set the sourcefile

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);

    //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244); //rgb(66, 133, 244)

    $pdf->SetXY(22, 105);
    $pdf->Write(0, "REFNUMBER");
    $pdf->SetXY(22, 114);
    $pdf->Write(0, "12/12/2012");


    $pdf->SetXY(20, 121);
    $pdf->Write(0, "Architect");
    $pdf->SetFont('Arial', '', 8);
            $pdf->SetXY(117, 119);
        $pdf->Write(0, "REGISTERNUMBER");
        $pdf->SetFont('Arial', '', 11);
            $pdf->SetXY(152, 121);
            $pdf->Write(0, "PO. BOX");

    $pdf->SetXY(20, 130);
    $pdf->Write(0, "Structural Engineer");
    $pdf->SetFont('Arial', '', 8);
        $pdf->SetXY(132, 128);
        $pdf->Write(0, "REGISTERNUMBER");
        $pdf->SetFont('Arial', '', 11);
            $pdf->SetXY(162, 130);
            $pdf->Write(0, "PO. BOX");

    $pdf->SetXY(20, 139);
    $pdf->Write(0, "Owner");
        $pdf->SetXY(112, 139);
        $pdf->Write(0, "Owner PO Box");


    $pdf->SetFont('Arial', '', 9);
    $pdf->SetXY(70, 144);
    $pdf->Write(0, "PLANREGNO");
    $pdf->SetXY(120, 144);
    $pdf->Write(0, "LRNUMBER");
    $pdf->SetFont('Arial', '', 11);


        $pdf->SetXY(40, 221);
        $pdf->Write(0, "Developer Name");
        $pdf->SetXY(40, 230);
        $pdf->Write(0, "Architect Name");
        $pdf->SetXY(53, 239);
        $pdf->Write(0, "Engineer");


    $pdf->Output('sample.pdf', 'D');
