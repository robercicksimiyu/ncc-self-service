<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Institution Health License<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Institution Health Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
    <?php $refid = $health['RefID']; ?>
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php echo form_open('health/completeInstitutionHealthPermitDetails',array('class' =>"form-block")); ?>
    		<div class="table-responsive">
    			<table class="table table-striped mb30">
    				<thead>
    					<tr>
    						<th colspan="2" style="text-align:center;">APPLICATION DETAILS</th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<td><b>License ID</b></td>
    						<td><?php echo $health['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $health['RefID'];?>" />
    					</tr>
    					<tr>
    						<td><b>FULL NAMES</b></td>
    						<td><?php echo $health['Fullname'];?></td>
                            <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
    					</tr>
    					<tr>
    						<td><b>INSTITUTION NAME</b></td>
    						<td><?php echo $health['Name'];?></td>
                            <input type="hidden" name="name" value="<?php echo $health['Name'];?>" />
    					</tr>
    					<tr>
    						<td><b>EMAIL ADDRESS</b></td>
    						<td><?php echo $health['Email'];?></td>
                            <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
    					</tr>
    					<tr>
    						<td><b>PLOT No.</b></td>
    						<td><?php echo $health['PlotNo']; ?></td>
                            <input type="hidden" name="plotno" value="<?php echo $health['PlotNo'];?>" />
    					</tr>
    					<tr>
    						<td><b>MOBILE</b></td>
    						<td><?php echo $health['Mobile'];?></td>
                            <input type="hidden" name="mobile" value="<?php echo $health['Mobile'];?>" />
    					</tr>
    					<tr>
    						<td><b>PURPOSE</b></td>
    						<td><?php echo $health['Purpose'];?></td>
                            <input type="hidden" name="purpose" value="<?php echo $health['Purpose'];?>" />
    					</tr>
    					<tr>
    						<td><b>NATURE</b></td>
    						<td><?php echo $health['Nature'];?></td>
                            <input type="hidden" name="nature" value="<?php echo $health['Nature'];?>" />
    					</tr>
    					<tr>
    						<td><b>ADDRESS</b></td>
    						<td><?php echo $health['Address'] ;?></td>
                            <input type="hidden" name="address" value="<?php echo $health['Address'];?>" />
    					</tr>
    					<tr>
    						<td><b>Status</b></td>
    						<td><?php echo $health['Status'] ;?></td>
                            <input type="hidden" name="status" value="<?php echo $health['Status'];?>" />
    					</tr>
                        <tr>
                            <td><b>AMOUNT</b></td>
                            <td><?php echo $health['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $health['Amount'];?>" />
                        </tr>
                </tbody>
            </table>
        </div><!-- table-responsive -->
    </div>
      
      <?php echo'<div class="col-sm-6">';
            echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
            echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
                echo'</div>';
            echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
            echo " ";
            echo anchor('health/completeInstitutionHealthPermitDetails','Cancel',array('class'=>'btn btn-primary'));
            ?>
      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 

