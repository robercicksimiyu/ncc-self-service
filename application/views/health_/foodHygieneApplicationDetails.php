<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Food Hygiene<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
    <?php $refid = $health['RefID']; ?>
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php echo form_open('health/completeHealthPermitDetails',array('class' =>"form-block")); ?>
    		<div class="table-responsive">
    			<table class="table table-striped mb30">
    				<thead>
    					<tr>
    						<th colspan="2" style="text-align:center;">APPLICATION DETAILS</th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<td><b>Ref ID</b></td>
    						<td><?php echo $health['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $health['RefID'];?>" />
    					</tr>
    					<tr>
    						<td><b>FULL NAMES</b></td>
    						<td><?php echo $health['Fullname'];?></td>
                            <input type="hidden" name="fullname" value="<?php echo $health['Fullname'];?>" />
    					</tr>
    					<tr>
    						<td><b>EMAIL</b></td>
    						<td><?php echo $health['Email'];?></td>
                            <input type="hidden" name="email" value="<?php echo $health['Email'];?>" />
    					</tr>
    					<tr>
    						<td><b>TELEPHONE No.</b></td>
    						<td><?php echo $health['TelNumber'];?></td>
                            <input type="hidden" name="telnumber" value="<?php echo $health['TelNumber'];?>" />
    					</tr>
    					<tr>
    						<td><b>FIRM</b></td>
    						<td><?php echo $health['Firm']; ?></td>
                            <input type="hidden" name="firm" value="<?php echo $health['Firm'];?>" />
    					</tr>
    					<tr>
    						<td><b>OCCUPATION</b></td>
    						<td><?php echo $health['Occupation'];?></td>
                            <input type="hidden" name="occupation" value="<?php echo $health['Occupation'];?>" />
    					</tr>
    					<tr>
    						<td><b>OWNER</b></td>
    						<td><?php echo $health['Owner'];?></td>
                            <input type="hidden" name="owner" value="<?php echo $health['Owner'];?>" />
    					</tr>
    					<tr>
    						<td><b>PLOT No.</b></td>
    						<td><?php echo $health['PlotNo'];?></td>
                            <input type="hidden" name="plotno" value="<?php echo $health['PlotNo'];?>" />
    					</tr>
    					<tr>
    						<td><b>LR No.</b></td>
    						<td><?php echo $health['LRNO'] ;?></td>
                            <input type="hidden" name="lrno" value="<?php echo $health['LRNO'];?>" />
    					</tr>
    					<tr>
    						<td><b>FRONTING ON</b></td>
    						<td><?php echo $health['Frontingon'] ;?></td>
                            <input type="hidden" name="frontingon" value="<?php echo $health['Frontingon'];?>" />
    					</tr>
    					<tr>
    						<td><b>BUILDING</b></td>
    						<td><?php echo $health['Building'] ;?></td>
                            <input type="hidden" name="building" value="<?php echo $health['Building'];?>" />
    					</tr>
    					<tr>
    						<td><b>FLOOR No.</b></td>
    						<td><?php echo $health['FloorNo'] ;?></td>
                            <input type="hidden" name="floorno" value="<?php echo $health['FloorNo'];?>" />
    					</tr>
                        <tr>
                            <td><b>AMOUNT</b></td>
                            <td><?php echo $health['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $health['Amount'];?>" />
                        </tr>
                </tbody>
            </table>
        </div><!-- table-responsive -->
    </div>
      
      <?php echo'<div class="col-sm-6">';
            echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
            echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
                echo'</div>';
            echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
            echo " ";
            echo anchor('health/completeHealthPermitDetails','Cancel',array('class'=>'btn btn-primary'));
            ?>
      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 

