<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Food Hygiene<span>Print Permit</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene Details</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>Ref ID</b></td>
                        <td><?php echo $health['Refid'];?></td>
                      </tr>
                      <tr>
                        <td><b>FULL NAMES</b></td>
                        <td><?php echo $health['Fullname'];?></td>
                      </tr>
                      <tr>
                        <td><b>EMAIL</b></td>
                        <td><?php echo $health['Email'];?></td>
                      </tr>
                      <tr>
                        <td><b>TELEPHONE No.</b></td>
                        <td><?php echo $health['Tel_no'];?></td>
                      </tr>
                      <tr>
                        <td><b>FIRM</b></td>
                        <td><?php echo $health['Firm']; ?></td>
                      </tr>
                      <tr>
                        <td><b>OCCUPATION</b></td>
                        <td><?php echo $health['Occupation'];?></td>
                      </tr>
                      <tr>
                        <td><b>OWNER</b></td>
                        <td><?php echo $health['Owner'];?></td>
                      </tr>
                      <tr>
                        <td><b>PLOT No.</b></td>
                        <td><?php echo $health['PlotNo'];?></td>
                      </tr>
                      <tr>
                        <td><b>LR No.</b></td>
                        <td><?php echo $health['LRNO'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>FRONTING ON</b></td>
                        <td><?php echo $health['Frontingon'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>BUILDING</b></td>
                        <td><?php echo $health['Building'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>FLOOR No.</b></td>
                        <td><?php echo $health['FloorNo'] ;?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $health['Amount'] ;?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Print Receipt</h4>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printHealthPermit/<?php echo $health['Refid'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
            <p> <a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $health['Refid'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->


