<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Food Hygiene<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Food Hygiene</a></li>
      <li class="active">Fill Form</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >

  <div class="panel panel-default col-md-10" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title">Fill in This Form</h4>
    </div>
    <div class="panel-body">
      <?php echo form_open('health/displayHealthPermitDetails',array('class' =>"form-block")); ?>

      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Full Name</label>
          <input type="text" name="full_names" id="full_names" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Email</label>
          <input type="text" name="email" id="email" class="form-control" placeholder="" />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Telephone Number</label>
          <input type="text" name="custphone" id="custphone" class="form-control" placeholder="" required />
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Name of person/Firm/Company Issued with licence</label>
          <input type="text" name="applicant_name" id="applicant_name" class="form-control" placeholder="" />
        </div>

        <div class="col-sm-4">
          <label class="control-label">Nature of Occupation</label>
          <input type="text" name="occupation" id="occupation" class="form-control" placeholder="" />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Owner Of Premise</label>
          <input type="text" name="owner" id="owner" class="form-control" placeholder="" required />
        </div>
      </div>
      <div class="form-group">

        <div class="col-sm-4">
          <label class="control-label">Plot No</label>
          <input type="text" name="plot_no" id="plot_no" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">L.R.No</label>
          <input type="text" name="LR_no" id="LR_no" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Fronting on</label>
          <input type="text" name="frontingon" id="frontingon" class="form-control" placeholder=""  />
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-4">
          <label class="control-label">Building Name</label>
          <input type="text" name="building" id="building" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Floor Number</label>
          <input type="text" name="floor" id="floor" class="form-control" placeholder="" required />
        </div>
        <div class="col-sm-4">
          <label class="control-label">Physical Address</label>
          <input type="text" name="address" id="address" class="form-control" placeholder="" required />
        </div> 

      </div>
      <div id="proc">
        <input type="submit" class="btn btn-primary" value="Submit">
      </div>
      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 

