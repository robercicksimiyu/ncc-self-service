<?php

    include 'pdf/fpdf.php';
    include 'pdf/fpdi.php';

    // initiate FPDI
    $pdf = new FPDI();
    // Building Structures Variables /////////////////
    $refid  = "REFNUMBER";
    $planNo  = "PLANNUMBER";
    $ownerNames  ="Owner";
    $ownerEmail = "Email";
    $ownerPhoneNo ="Mobile NUMBER";
    $ownerPostalAddress = "PO Box";

    $architectRegNo = "REGNUMBER";
    $architectNames  = "Architect";
    $architectEmail = "Email";
    $architectPhoneNo  = "Mobile NUMBER";
    $architectPostalAddress  ="PO Box";

    $engineerRegNo  ="REGNUMBER";
    $engineerNames  = "Engineer";
    $engineerEmail  = "Email";
    $engineerPhoneNo  = "Mobile NUMBER";
    $engineerPostalAddress  = "PO Box";

    $currentLandUse  = "Current Land Use";
    $zone  = "Zone";

    $projectDetailedDescription = "Project Description XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    $projectDetailedDescription = substr($projectDetailedDescription, 0, 150);
    $project2 = $project3 = '';
    // $projectDetailedDescription $array = explode( "\n", wordwrap( $str, $x));
    // Split by 46 first
    // $array = split_str($projectDetailedDescription, 46);
    // if(sizeof($array) > 1){
    //     $project2 = substr($projectDetailedDescription,46,103);
    //     $project3 = substr($projectDetailedDescription,103,150);
    //     $projectDetailedDescription = substr($projectDetailedDescription,0, 46);
    // }
    // $landTenure <select> ***

    $numberofUnits  = "Number Of Units";
    $lrNo  = "LR NUMBER";
    $plotSize  = "PLot Size";
    $nearestRoad = "Nearest Road";
    $estate  = "Estate";
    $subcounty  = "Sub County";
    $ward  = "ward";

    $soilType  = "Type Of Soil";
    $waterSupplier  = "Water Supplier";

    // $sewerageDisposalMethod "Sewerage Disposal"

    $basementArea  = "basementArea";
    $mezzaninefloorArea = "mezzaninefloorArea";
    $floor1Area  = "floor1Area";
    $floor2Area = "floor2Area";
    $floor3Area  = "floor3Area";
    $floor4Area = "floor4Area";
    $Others = "Others";
    $totalArea = "totalArea";

    $projectCost = "Project Cost";
    $inspectionFees  = "Inspection Fees";

    $Foundation  = "Foundation";
    $externalWalls  = "External Walls";
    $mortar  = "Mortar";

    $roofCover  = "Roof Cover";
    $dampProofCourse  = "Damp Proof";
    //////////////////////////////////////////////////////

    $date = date('Y-m-d');
    //$customer = substr($customerName, 0, 17);

    // BUILDING STRUCTURES ///////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');

    // import page 1 ////////////////////////////////
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    // //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244);

    //$customer = substr($customerName, 0, 17);
    $pdf->SetXY(36, 68);
    $pdf->Write(0, $refid);
    $pdf->SetXY(36, 73);
    $pdf->Write(0, $date);
    $pdf->SetXY(52, 107);
    $pdf->Write(0, $planNo);
    $pdf->SetXY(65, 120);
    $pdf->Write(0, $lrNo);


    // import page 2 /////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(2);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

    $pdf->SetXY(143, 26);
    $pdf->Write(0, $planNo);

    $pdf->SetXY(70, 129);
    $pdf->Write(0, $architectNames);
    $pdf->SetXY(161, 129);
    $pdf->Write(0, $architectRegNo);
    $pdf->SetXY(67, 134);
    $pdf->Write(0, $architectEmail);
    $pdf->SetXY(167, 134);
    $pdf->Write(0, $architectPhoneNo);
    $pdf->SetXY(60, 139);
    $pdf->Write(0, $architectPostalAddress);

    $pdf->SetXY(73, 154);
    $pdf->Write(0, $engineerNames);
    $pdf->SetXY(162, 154);
    $pdf->Write(0, $engineerRegNo);
    $pdf->SetXY(64, 160);
    $pdf->Write(0, $engineerEmail);
    $pdf->SetXY(167, 160);
    $pdf->Write(0, $engineerPhoneNo);
    $pdf->SetXY(60, 165);
    $pdf->Write(0, $engineerPostalAddress);

    $pdf->SetXY(65, 189);
    $pdf->Write(0, $ownerNames);
    $pdf->SetXY(143, 180);
    $pdf->Write(0, $date);
    $pdf->SetXY(155, 189);
    $pdf->Write(0, $ownerEmail);
    $pdf->SetXY(165, 195);
    $pdf->Write(0, $ownerPhoneNo);
    $pdf->SetXY(60, 165);
    $pdf->Write(0, $ownerPostalAddress);

    // import page 3 ///////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(3);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

    $pdf->SetXY(69, 28);
    $pdf->Write(0, $currentLandUse);
    $pdf->SetXY(137, 28);
    $pdf->Write(0, $zone);

    $pdf->SetXY(88, 37);
    $pdf->Write(0, $projectDetailedDescription);
    $newline = 4;
    $start = 42;
    $pdf->SetXY(40, 42);
    $pdf->Write(0, $project2);
    $pdf->SetXY(40, 46);
    $pdf->Write(0, $project3);


    $pdf->SetFont('Arial', '', 14);
    $pdf->SetXY(36, 62);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 67);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 72);
    $pdf->Write(0, "0");
    $pdf->SetXY(36, 77);
    $pdf->Write(0, "0");
    $pdf->SetFont('Arial', '', 11);


    $pdf->SetXY(133, 91);
    $pdf->Write(0, $numberofUnits);
    $pdf->SetXY(67, 96);
    $pdf->Write(0, $lrNo);
    $pdf->SetXY(150, 96);
    $pdf->Write(0, $plotSize);
    $pdf->SetXY(95, 105);
    $pdf->Write(0, $nearestRoad);
    $pdf->SetXY(78, 110);
    $pdf->Write(0, $estate);
    $pdf->SetXY(144, 110);
    $pdf->Write(0, $subcounty);
    $pdf->SetXY(66, 114);
    $pdf->Write(0, $soilType);


    $pdf->SetXY(71, 124);
    $pdf->Write(0, $waterSupplier);
    $pdf->SetXY(157, 124);
    // $pdf->Write(0, "Sewerage Disposal");
    // $pdf->SetXY(104, 129);
    // $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");


    $pdf->SetXY(97, 158);
    $pdf->Write(0, $basementArea);
    $pdf->SetXY(97, 163);
    $pdf->Write(0, $mezzaninefloorArea);
    $pdf->SetXY(97, 168);
    $pdf->Write(0, $floor1Area);
    $pdf->SetXY(97, 173);
    $pdf->Write(0, $floor2Area);
    $pdf->SetXY(97, 178);
    $pdf->Write(0, $floor3Area);
    $pdf->SetXY(97, 183);
    $pdf->Write(0, $floor4Area);
    $pdf->SetXY(97, 188);
    $pdf->Write(0, $Others);
    $pdf->SetXY(97, 193);
    $pdf->Write(0, $totalArea);

    $pdf->SetXY(93, 202);
    $pdf->Write(0, $projectCost);
    $pdf->SetXY(84, 207);
    $pdf->Write(0, $inspectionFees);

    $pdf->SetXY(76, 217);
    $pdf->Write(0, $Foundation);
    $pdf->SetXY(76, 222);
    $pdf->Write(0, $externalWalls);
    $pdf->SetXY(76, 227);
    $pdf->Write(0, $mortar);
    $pdf->SetXY(76, 232);
    $pdf->Write(0, $roofCover);
    $pdf->SetXY(82, 237);
    $pdf->Write(0, $dampProofCourse);


    // INDEMNITY ////////////////////////////////////////

    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/idemity.pdf');
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
    // set the sourcefile

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);

    //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244); //rgb(66, 133, 244)

    $pdf->SetXY(22, 105);
    $pdf->Write(0, $refid);
    $pdf->SetXY(22, 114);
    $pdf->Write(0, $date);


    $pdf->SetXY(20, 121);
    $pdf->Write(0,$architectNames);
    $pdf->SetFont('Arial', '', 8);
            $pdf->SetXY(117, 119);
        $pdf->Write(0, $architectRegNo);
        $pdf->SetFont('Arial', '', 11);
            $pdf->SetXY(152, 121);
            $pdf->Write(0, $architectPostalAddress);

    $pdf->SetXY(20, 130);
    $pdf->Write(0, $engineerNames);
    $pdf->SetFont('Arial', '', 8);
        $pdf->SetXY(132, 128);
        $pdf->Write(0, $engineerRegNo);
        $pdf->SetFont('Arial', '', 11);
            $pdf->SetXY(162, 130);
            $pdf->Write(0, $engineerPostalAddress);

    $pdf->SetXY(20, 139);
    $pdf->Write(0,$ownerNames);
        $pdf->SetXY(112, 139);
        $pdf->Write(0, $ownerPostalAddress);


    $pdf->SetFont('Arial', '', 9);
    $pdf->SetXY(70, 144);
    $pdf->Write(0, $refid);
    $pdf->SetXY(120, 144);
    $pdf->Write(0, $lrNo);
    $pdf->SetFont('Arial', '', 11);


        $pdf->SetXY(40, 221);
        $pdf->Write(0, $ownerNames);
        $pdf->SetXY(40, 230);
        $pdf->Write(0, $architectNames);
        $pdf->SetXY(53, 239);
        $pdf->Write(0, $engineerNames);


    $pdf->Output('building_structures_and_indemnity.pdf', 'D');
