<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Extention Of Use</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">Extension of use</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
         <div class="panel panel-default col-md-10">
            <div class="panel-heading">
              <h4><em>Extension of User</em></h4>
            </div>
            <?php echo form_open('rlb/extention/step3'); ?>
            <?php if(isset($previousStepData)):?>
                <?php foreach($previousStepData as $key => $value){
                  echo "<input type='hidden' name='$key' id='$key' value='$value'/>";
                }?>
          <?php endif;?>
            <div class="panel-body">
              <div class="row">
                <fieldset class="fset">
                  <legend class="fset">SECTION C: EXTENSION OF USE </legend>
                  <div class="form-group">
                    <div class="col-sm-4 <?=(form_error('subdivisionInvolved')) ? 'has-error' : ''?>">
                      <label class="control-label">If Subdivision Involved</label>
                      <select  class="form-control" name="subdivisionInvolved" id="subdivisionInvolved" >
                        <option value="0"<?php echo set_select('subdivisionInvolved', '0'); ?>>No Subdivision Involved</option>
                        <option value="1"<?php echo set_select('subdivisionInvolved', '1'); ?>>Subdivision Involved</option>
                      </select>
                    </div>
                    <div id="permissions" class="col-sm-4 <?=(form_error('permissionApplied')) ? 'has-error' : ''?>">
                      <label class="control-label">Permissions Applied</label>
                      <select class="form-control" name="permissionApplied" id="permissionApplied" >
                        <option value="0"<?php echo set_select('permissionApplied', '0'); ?>>No Permissions Applied</option>
                        <option value="1"<?php echo set_select('permissionApplied', '1'); ?>>Permissions Applied</option>
                      </select>
                    </div>
                    <div id="sub_app_no" class="col-sm-4 <?=(form_error('subdivisionApplicationNo')) ? 'has-error' : ''?>">
                      <label class="control-label">Registered Application No</label>
                      <input type="text" name="subdivisionApplicationNo"  id="subdivisionApplicationNo" class="form-control"
                      placeholder="e.g N/A" value="<?=set_value('subdivisionApplicationNo')?>"/>
                      <small><?php echo form_error('subdivisionApplicationNo'); ?></small>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12 <?=(form_error('proposedDevelopment')) ? 'has-error' : ''?>">
                      <label class="control-label"> Current Development</label>
                      <textarea rows="2" type="text" name="proposedDevelopment"  id="proposedDevelopment" class="form-control"
                      placeholder="Brief description of current development including the purpose for which land and / or building are to be used"><?=set_value('proposedDevelopment')?></textarea>
                      <small><?php echo form_error('proposedDevelopment'); ?></small>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-4 <?=(form_error('currentUse')) ? 'has-error' : ''?>">
                    <label class="control-label">Original Land Use</label>
                    <select class="form-control" name="currentUse" id="currentUse">
                        <option value="<?=set_value('currentUse')?>">Select Original Land Use</option>
                        <option value="Residential/Office"<?php echo set_select('currentUse', 'Residential/Office'); ?>>Residential/Office</option>
                        <option value="Commercial"<?php echo set_select('currentUse', 'Commercial'); ?>>Commercial</option>
                        <option value="Industrial"<?php echo set_select('currentUse', 'Industrial'); ?>>Industrial</option>
                        <option value="Agricultural"<?php echo set_select('currentUse', 'Agricultural'); ?>>Agricultural</option>
                      </select>
                      <small><?php echo form_error('currentUse'); ?></small>
                    </div>
                    <div class="col-sm-4 <?=(form_error('lastUseDate')) ? 'has-error' : ''?>">
                      <label class="control-label">Current Use Date</label></label>
                      <input type="text" name="lastUseDate"  id="lastUseDate" class="form-control"
                      placeholder="e.g. 01/08/2016" value="<?=set_value('lastUseDate')?>"  />
                      <small><?php echo form_error('lastUseDate'); ?></small>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12 <?=(form_error('meansOfAccessContruction')) ? 'has-error' : ''?>">
                      <label class="control-label">Construction of means of access to or from a road</label>
                      <select class="form-control" name="meansOfAccessContruction" id="meansOfAccessContruction" >
                        <option value="">Select Means of Access</option>
                        <option value="0"<?php echo set_select('meansOfAccessContruction', '0'); ?>>No construction of new / alternative means of access to or from a road is involved </option>
                        <option value="1"<?php echo set_select('meansOfAccessContruction', '1'); ?>>Construction of new / alternative means of access to or from a road is involved</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12 <?=(form_error('natureProposedDevelopment')) ? 'has-error' : ''?>">
                      <label class="control-label">Nature Of Current Development</label>
                      <textarea rows="2" name="natureProposedDevelopment"  id="natureProposedDevelopment" class="form-control"
                      placeholder="State exact nature of the change, If the current development consists only of change of user and doesn't involve building operations.\n e.g Regularization of change of user"><?=set_value('natureProposedDevelopment')?></textarea>
                      <small><?php echo form_error('natureProposedDevelopment'); ?></small>
                    </div>
                  </div>
                </fieldset>
              </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
          </div><!-- contentpanel -->



     <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->
  <script type="text/javascript">
    $('document').ready(function(){
      if($('#subdivisionInvolved').val()==='0'){
        document.getElementById( 'permissions' ).style.display = 'none';
      }
      if($('#permissionApplied').val()==='1' && $('#subdivisionInvolved').val()==='1'){
        document.getElementById( 'sub_app_no' ).style.display = 'block';
      }else {
        document.getElementById( 'sub_app_no' ).style.display = 'none';
      }
      $('#subdivisionInvolved').on('change',function(){
        if($(this).val()==='0'){
          document.getElementById( 'permissions' ).style.display = 'none';
          document.getElementById( 'sub_app_no' ).style.display = 'none';
        }else{
          document.getElementById( 'permissions' ).style.display = 'block';
            document.getElementById( 'sub_app_no' ).style.display = 'block';
          if($('#permissionApplied').val()==='0'){
            document.getElementById( 'sub_app_no' ).style.display = 'none';
          }
        }
      });
      $('#permissionApplied').on('change',function(){
        if($(this).val()==='0'){
          document.getElementById( 'sub_app_no' ).style.display = 'none';
        }else{
          document.getElementById( 'sub_app_no' ).style.display = 'block';
        }
      });
    });
  </script>

  <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <!-- Javascript -->
  <script>
     $(function() {
        $( "#lastUseDate" ).datepicker({ dateFormat: 'dd/mm/yy', maxDate: '0' });
        //$( "#lastUseDate" ).datepicker("setDate", "10w+1");
     });
  </script>
