<div class="pageheader">
    <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Amalgamation</span></h2>
    <div class="breadcrumb-wrapper">
      <span class="label">You are here:</span>
      <ol class="breadcrumb">
        <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
        <li class="active">Amalgamation</li>
      </ol>
    </div>
  </div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-10">
           <div class="panel-heading">
             <h4><em>Amalgamation</em></h4>
           </div>
           <?php echo form_open('rlb/amalgamation/complete'); ?>
            <?php if(isset($previousStepData)):?>
                  <?php foreach($previousStepData as $key => $value){
                    echo "<input type='hidden' name='$key' id='$key' value='$value'/>";
                  }?>
            <?php endif;?>
            <div class="panel-body">
              <div class="row">
                <fieldset class="fset">
                  <legend class="fset">SECTION B: AMALGAMATION (Cont.)</legend>
                  <div class="form-group">
                      <div class="col-sm-6 <?=(form_error('waterSupply')) ? 'has-error' : ''?>">
                        <label class="control-label"> Water supply</label>
                        <select class="form-control" name="waterSupply" id="waterSupply">
                          <option value="<?=set_value('waterSupply')?>">Select one</option>
                          <option value="Borehole">Borehole</option>
                          <option value="Nairobi City Water and Sewerage Compan">Nairobi City Water and Sewerage Company</option>
                          <option value="Rain water">Rain water</option>
                          <option value="River">River</option>
                          <option value="Other Suppliers">Other Suppliers</option>
                        </select>
                        <small><?php echo form_error('waterSupply'); ?></small>
                      </div>
                      <div class="col-sm-6 <?=(form_error('surfaceWaterDisposal')) ? 'has-error' : ''?>">
                        <label class="control-label"> Surface Water Disposal</label>
                        <select name="surfaceWaterDisposal"  id="surfaceWaterDisposal" class="form-control">
                          <option value="<?=set_value('surfaceWaterDisposal')?>">Select One</option>
                          <option value="Nairobi County services">Nairobi County services</option>
                          <option value="Private arrangement">Private arrangement</option>
                        </select>
                        <small><?php echo form_error('surfaceWaterDisposal'); ?></small>
                      </div>
                   </div>
                  <div class="form-group">
                        <div class="col-sm-6 <?=(form_error('sewerageDisposal')) ? 'has-error' : ''?>">
                          <label class="control-label"> Sewerage Disposal</label>
                          <select name="sewerageDisposal"  id="sewerageDisposal" class="form-control">
                            <option value="<?=set_value('sewerageDisposal')?>">Select One</option>
                            <option value="Nairobi city water and sewerage company">Nairobi city water and sewerage company</option>
                            <option value="Conservative Tank">Conservative Tank</option>
                            <option value="Biodigester">Biodigester</option>
                            <option value="Pit Latrine">Pit Latrine</option>
                            <option value="Septic tank">Septic Tank</option>
                          </select>
                          <small><?php echo form_error('sewerageDisposal'); ?></small>
                        </div>
                      <div class="col-sm-6 <?=(form_error('refuseDisposal')) ? 'has-error' : ''?>">
                        <label class="control-label"> Refuse Disposal</label>
                          <select name="refuseDisposal"  id="refuseDisposal" class="form-control">
                            <option value="<?=set_value('refuseDisposal')?>">Select One</option>
                            <option value="Nairobi County services">Nairobi County services</option>
                            <option value="Private arrangement">Private arrangement</option>
                          </select>
                        <small><?php echo form_error('refuseDisposal'); ?></small>
                      </div>
                   </div>
                   <div class="form-group">
                     <div class="col-sm-12 <?=(form_error('easementDetails')) ? 'has-error' : ''?>">
                       <label class="control-label"> Easement Details</label>
                       <textarea type="text" name="easementDetails"  id="easementDetails" class="form-control"
                       placeholder="Details of any relevant easement affecting the proposals"><?=set_value('easementDetails')?></textarea>
                       <small><?php echo form_error('easementDetails'); ?></small>
                     </div>
                   </div>
                </fieldset>
              </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->
