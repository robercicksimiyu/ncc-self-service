<div class="pageheader">
    <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Subdivision</span></h2>
    <div class="breadcrumb-wrapper">
      <span class="label">You are here:</span>
      <ol class="breadcrumb">
        <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
        <li class="active">Subdivision</li>
      </ol>
    </div>
  </div>

    <div class="contentpanel" >
         <div class="panel panel-default col-md-10">
           <div class="panel-heading">
             <h4><em>Applicant Details &amp; General information</em></h4>
           </div>
           <?php echo form_open($callbackUrl.'/step2'); ?>
           <div class="panel-body">
             <div class="row">
               <fieldset class="fset">
                 <legend class="fset">Applicant or Agent Details</legend>
                 <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('applicantNames')) ? 'has-error' : ''?>">
                     <label class="control-label">Name</label>
                     <input type="text" name="applicantNames"  id="applicantNames" class="form-control"
                     placeholder="e.g. Jane Omolo" value="<?=set_value('applicantNames')?>"  />
                     <small><?php echo form_error('applicantNames'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('applicantAddress')) ? 'has-error' : ''?>">
                       <label class="control-label">Address</label>
                       <input type="text" name="applicantAddress"  id="applicantAddress" class="form-control"
                       placeholder="e.g. Box XXX - XXXXX" value="<?=set_value('applicantAddress')?>"  />
                       <small><?php echo form_error('applicantAddress'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('applicantProfession')) ? 'has-error' : ''?>">
                       <label class="control-label">Profession</label>
                       <input type="text" name="applicantProfession"  id="applicantProfession" class="form-control"
                       placeholder="e.g. Reg Planner" value="<?=set_value('applicantProfession')?>"  />
                       <small><?php echo form_error('applicantProfession'); ?></small>
                   </div>
                 </div>
                 <div class="form-group">
                   <!--<div class="col-sm-4 <?=(form_error('applicationNumber')) ? 'has-error' : ''?>">
                     <label class="control-label">Application Number</label>
                     <input type="text" name="applicationNumber"  id="applicationNumber" class="form-control" placeholder="" value="<?=set_value('applicationNumber')?>"  />
                     <small><?php echo form_error('applicationNumber'); ?></small>
                   </div>-->
                 </div>
               </fieldset>
               <fieldset class="fset">
                 <legend class="fset">SECTION A - GENERAL INFORMATION</legend>
                 <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('ownerNames')) ? 'has-error' : ''?>">
                     <label class="control-label">Owner Name</label>
                     <input type="text" name="ownerNames"  id="ownerNames" class="form-control"
                     placeholder="e.g. John Kamau" value="<?=set_value('ownerNames')?>"  />
                     <small><?php echo form_error('ownerNames'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('ownerAddress')) ? 'has-error' : ''?>">
                     <label class="control-label">Owner Address</label>
                     <input type="text" name="ownerAddress"  id="ownerAddress" class="form-control"
                     placeholder="e.g. Box XXX - XXXXX" value="<?=set_value('ownerAddress')?>"  />
                     <small><?php echo form_error('ownerAddress'); ?></small>
                   </div>
                 </div>
                 <div class="form-group">
                   <div class="col-sm-12 <?=(form_error('applicantInterest')) ? 'has-error' : ''?>">
                     <label class="control-label">Applicant Interest</label>
                     <textarea rows="2" type="text" name="applicantInterest"  id="applicantInterest" class="form-control"
                     placeholder="State interest in the land e.g lease, prospective purchaser, etc if applicant is not the owner and whether the consent of the owner to this application has been obtained."><?=set_value('applicantInterest')?></textarea>
                     <small><?php echo form_error('applicantInterest'); ?></small>
                   </div>
                 </div>
                 <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('lrNo')) ? 'has-error' : ''?>">
                     <label class="control-label">L.R. or parcel No.</label>
                     <input type="text" name="lrNo"  id="lrNo" class="form-control"
                     placeholder="e.g. Blk XXX/XXX" value="<?=set_value('lrNo')?>"  />
                     <small><?php echo form_error('lrNo'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('physicalAddress')) ? 'has-error' : ''?>">
                     <label class="control-label">Physical Address</label>
                     <input type="text" name="physicalAddress"  id="physicalAddress" class="form-control"
                     placeholder="e.g. Road, District and Town" value="<?=set_value('physicalAddress')?>"  />
                     <small><?php echo form_error('physicalAddress'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('zone_zoneid')) ? 'has-error' : ''?>">
                     <label class="control-label">Zone</label>
                     <select class="form-control" name="zone_id" id="zone_id" >
                       <option value="">Select Zone</option>
                       <?php

                       $incr = 0;
                       for($i = 0; $i <= 56; $i++) {
                         echo '<option value="' .$zones[$i + $incr] .'">' .$zones[$i + $incr + 1] .'</option>';
                         $incr ++;
                       }

                       ?>
                      <input type='hidden' id='zone' name='zone' value='' readonly/>
                     </select>
                   </div>
                 </div>
                 <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('acreage')) ? 'has-error' : ''?>">
                     <label class="control-label">Acreage (Ha)</label>
                     <input type="text" name="acreage"  id="acreage" class="form-control"
                     placeholder="e.g 0.2" value="<?=set_value('acreage')?>"  />
                     <small><?php echo form_error('acreage'); ?></small>
                   </div>
                 </div>
               </fieldset>
             </div>
           </div>
           <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->
  <script type="text/javascript">
  $(document).ready(function () {

    $("#zone_id").on('change', function (){
      var selectedText = $("#zone_id option:selected").text();
      $("#zone").val(selectedText);
    });

  });
  </script>
