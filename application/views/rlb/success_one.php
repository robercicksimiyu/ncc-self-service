<div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Success</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">Success</li>
        </ol>
      </div>
    </div>
    <div class="contentpanel" >
     <div class="panel panel-default col-md-8">
       <div class="panel-heading">
         <h4>Download Completed Forms</h4>
       </div>	
       <?php if($payment_status):?>
       <?php  echo form_open('rlb/printReceipt', array('id' => 'myform')); ?>
       <div class="panel-body">
          <div class="col-md-12">
            <div class="alert alert-success">Your Payment has been made successfully</div>
          </div>
	         <div class="col-md-12">
               <?php if(isset($complete)):?>
		        <div class="table-responsive">
		        <table class="table table-striped ">
		          <tbody>
                      <tr>
                        <td><b>NAME</b></td>
                        <td><?php echo isset($complete['Payee'])?$complete['Payee']:''; ?></td>
                        <input type="hidden" name="Payee" value="<?php echo isset($complete['Payee'])?$complete['Payee']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo isset($complete['Reference_ID'])?$complete['Reference_ID']:'';?></td>
                        <input type="hidden" name="Reference_ID" value="<?php echo isset($complete['Reference_ID'])?$complete['Reference_ID']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo isset($complete['Invoice_Number'])?$complete['Invoice_Number']:'';?></td>
                        <input type="hidden" name="Invoice_Number" value="<?php echo isset($complete['Invoice_Number'])?$complete['Invoice_Number']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>RECEIPT NUMBER</b></td>
                        <td><?php echo isset($complete['ReceiptNo'])?$complete['ReceiptNo']:'';?></td>
                        <input type="hidden" name="receipt" value="<?php echo isset($complete['ReceiptNo'])?$complete['ReceiptNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo isset($complete['Amount'])?$complete['Amount']:'0.00';?></td>
                        <input type="hidden" name="amount" value="<?php echo isset($complete['Amount'])?$complete['Amount']:'0.00';?>" />
                      </tr>
                      <tr>
                        <td><b>YEAR</b></td>
                        <td><?php echo isset($complete['year'])?$complete['year']:'';?></td>
                        <input type="hidden" name="year" value="<?php echo isset($complete['year'])?$complete['year']:'';?>" />
                      </tr>
		          </tbody>
		        </table>
	      	   </div><!-- table-responsive -->
            	<?php endif;?>
              </div>
             <a href="<?=($others)?base_url('rlb/printForm'):(($others_type == "sc")?base_url('rlb/printFormTwo'):base_url('rlb/printFormThree'))?>" class="btn btn-primary">Print Completed Forms</a>
             <input type="submit" id="next1"  value="Print Receipt" class="btn btn-primary"/>
           </div>
           <?php echo form_close();?>
	   <div class="panel panel-default col-md-8">
         <div class="panel-body">
    	   <?php elseif($complete['ErrorCode']==2025):?>
                  <div class="col-md-12">
                    <div class="alert alert-warning"><strong>Error!</strong> The Pin Entered is Invalid. </div>
                    <?php echo anchor('#','Back',array('class'=>"btn btn-primary",'onClick'=>"goBack()"));?>
                  </div>
    	    <?php elseif($complete['ErrorCode']==1110): ?>
                  <div class="col-md-12">
                    <div class="alert alert-warning"><strong>Error!</strong> The Reference ID Entered is Invalid. </div>
                    <?php echo anchor('#','Back',array('class'=>"btn btn-primary",'onClick'=>"goBack()"));?>
                  </div>
    	    <?php elseif($complete['ErrorCode']==44402): ?>
                  <div class="col-md-12">
                    <div class="alert alert-warning">Service currently unavailable but we will be back soon</div>
                  </div>
    	   <?php elseif($complete['ErrorCode']==2030):?>
                  <div class="col-md-12">
                    <div class="alert alert-warning"><strong>Error!</strong> <?=$complete['Message'];?></div>
                    <?php echo anchor('#','Back',array('class'=>"btn btn-primary",'onClick'=>"goBack()"));?>
                  </div>
         <?php else:?>
                  <div class="col-md-12">
                    <div class="alert alert-warning"><strong>Error!</strong> <?=$complete['Message'];?></div>
                    <?php echo anchor('#','Back',array('class'=>"btn btn-primary",'onClick'=>"goBack()"));?>
                  </div>
    	  <?php endif; ?>
        </div>
      </div>
         
    </div><!-- contentpanel -->

<script>
  function goBack() {
    window.history.back();
  }
</script>

  </div><!-- mainpanel -->
