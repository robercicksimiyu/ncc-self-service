<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Regularization<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Regularization</a></li>
      <li class="active">Complete</li>
    </ol>
  </div>
</div>
<?php $rescode = $reg['rescode'];?>
<?php if($rescode=="200"): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">

            <div class="panel-body">
              <div class="row">
                <?php  echo form_open('rlb/printReceipt', array('id' => 'myform')); ?>
                  <div class="table-responsive">
                    <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;"><div class="alert alert-success" style="padding-top:10px;margin-left:20px;">
                          <button type="button" class="close" data-dismiss="success" aria-hidden="true"></button>
                          <?php echo "Regularization has successfully been paid.";?>
                        </div>
                      </th>
                    </tr>
                      <tr>
                        <th colspan="2" style="text-align:center;">REGULARIZATION DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>NAME</b></td>
                        <td><?php echo isset($reg['Payee'])?$reg['Payee']:''; ?></td>
                        <input type="hidden" name="Payee" value="<?php echo isset($reg['Payee'])?$reg['Payee']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo isset($reg['Reference_ID'])?$reg['Reference_ID']:'';?></td>
                        <input type="hidden" name="Reference_ID" value="<?php echo isset($reg['Reference_ID'])?$reg['Reference_ID']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo isset($reg['Invoice_Number'])?$reg['Invoice_Number']:'';?></td>
                        <input type="hidden" name="Invoice_Number" value="<?php echo isset($reg['Invoice_Number'])?$reg['Invoice_Number']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>RECEIPT NUMBER</b></td>
                        <td><?php echo isset($reg['ReceiptNo'])?$reg['ReceiptNo']:'';?></td>
                        <input type="hidden" name="receipt" value="<?php echo isset($reg['ReceiptNo'])?$reg['ReceiptNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo isset($reg['amount'])?$reg['amount']:'0.00';?></td>
                        <input type="hidden" name="amount" value="<?php echo isset($reg['amount'])?$reg['amount']:'0.00';?>" />
                      </tr>
                      <tr>
                        <td><b>YEAR</b></td>
                        <td><?php echo isset($reg['year'])?$reg['year']:'';?></td>
                        <input type="hidden" name="year" value="<?php echo isset($reg['year'])?$reg['year']:'';?>" />
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                      <?php $refid = isset($reg['Reference_ID'])?$reg['Reference_ID']:'';?>
                       <a href="<?php echo base_url();?>rlb/printDoc/<?php echo $refid;?>" class="btn btn-primary">Print Completed Forms</a>
                       <input type="submit" id="next1"  value="Print Receipt" class="btn btn-primary"/>
                    </div>
                  </div>
                <?php echo form_close();?>
              </div>

        </div>

    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Payment Complete</h4>
            <p></p>
            <p>Kindly proceed to print the filled form and the receipt.</p>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $reg['Reference_ID'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
<?php elseif($rescode!="200"): ?>
<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo $reg['message'];?>
                <a href="#" onclick="goBack()" class="btn btn-primary receipt" style="float:right;margin-top:-5px;"><i class="fa fa-mail-reply"></i>Back</a>
                </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel -->
<?php endif;?>

<script>
function goBack() {
    window.history.back();
}
</script>
