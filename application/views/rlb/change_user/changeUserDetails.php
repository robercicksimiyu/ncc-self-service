<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span> Structures</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">PAYMENT DETAILS</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
  <?php $refid = $regDetails['RefId']; ?>
<div class="panel panel-default col-md-8" style="margin-right:20px;">
           <div class="panel-heading">
             <h4><em>PAYMENT INVOICE DETAILS</em></h4>
           </div>
           <?php echo form_open('rlb/completion/s'); ?>
           <div class="panel-body">
		<?php //var_dump ($regDetails);?>
		<table class="table table-striped mb30">
		  <thead>
		    <tr>
		      <th colspan="2" style="text-align:center;">APPLICATION DETAILS</th>
		    </tr>
		  </thead>
          	    <tbody>
		      <tr>
			  <td><b>OWNERS NAME</b></td>
			   <td><?php echo $regDetails['ownerNames']; ?></td>
			  <input type="hidden" name="ownerNames" value="<?php echo $regDetails['ownerNames'];?>" /> 
		      </tr>
		      <!--<tr>
			  <td><b>CONTACT DETAILS</b></td>
			  <td><?php echo strtoupper($regDetails['ownerAddress']);?></td>
			   <input type="hidden" name="ownerAddress" value="<?php echo $regDetails['ownerAddress'];?>" /> 
		      </tr>-->
		      <tr>
			  <td><b>LAND RATE NUMBER</b></td>
			  <td><?php echo strtoupper($regDetails['lrNo']);?></td>
			   <input type="hidden" name="lrNo" value="<?php echo $regDetails['lrNo'];?>" />
		      </tr>
		      <tr>
			<td><b>LICENSE ID</b></td>
			<td><?php echo $regDetails['RefId'];?></td>
			 <input type="hidden" name="RefId" value="<?php echo $regDetails['RefId'];?>" />
		      </tr>
		      <tr>
			  <td><b>INVOICE NUMBER</b></td>
			  <td><?php echo $regDetails['InvoiceNo'];?></td>
			   <input type="hidden" name="InvoiceNo" value="<?php echo $regDetails['InvoiceNo'];?>" />
		      </tr>
		      <tr>
			  <td><b>FORM STATUS</b></td>
			  <td><?php echo "PENDING"; //$regDetails['form_status'];?></td> 
			   <input type="hidden" name="form_status" value="<?php echo $regDetails['form_status'];?>" />
			   <input type="hidden" name="form_id" value="<?php echo isset($regDetails['form_id'])? $regDetails['form_id']:'RF01';?>" />
		      </tr>
		      <tr>
			  <td><b>AMOUNT</b></td>
			  <td><?php echo $regDetails['amount'] ;?></td>
			   <input type="hidden" name="amount" value="<?php echo $regDetails['amount'];?>" />
		      </tr>
		      <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
		  Your Application was successful, make payment and proceed to print your form.
		</div>
              </tbody>
          </table>
      </div><!-- table-responsive -->

           </div>
            <div class="panel-footer">
            <div class="row">
            <div class="form-group">
             <div class="col-sm-4">
                  <label class="control-label">Enter Wallet PIN</label>
                  <input type="password" name="pin" class="form-control" id="pin" placeholder="" required />
              </div>
              <div class="col-sm-4">
                <label class="control-label">&nbsp;</label><br>
                <input type="submit" id="next5"  value="Make Payments" class="btn btn-primary"/>
              </div>
             </div>
             <div class="col-sm-4">
                <?=anchor('rlb/building_structures/step1','Cancel',array('class'=>'btn btn-primary'))?>
              </div>
             </div>

              
            </div>
            <?php echo form_close();?>
    </div><!-- contentpanel -->



   <!-- <div class="panel panel-default col-md-3" >
    <div class="panel-heading" style="text-align:center;">
      <h4 class="panel-title panelx">Follow these steps</h4>
      <p>Click below to print the Invoice which you will use to pay</p>
      <p></p>
       <p><a href="<?php echo base_url(); ?>fire/printFirePermitreceipt/<?php echo $refid;?>"><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a></p> 
      <p><a href=""><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a></p>
  </div>
</div>-->

  </div><!-- mainpanel -->
