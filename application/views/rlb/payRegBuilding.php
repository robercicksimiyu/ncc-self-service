<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Regularization<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Regularization</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>

    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Enter License ID </h4>
          <p>Cross check to make sure you have filled in the correct License ID Number</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('rlb/payRegBuildingDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
                      <div class="col-sm-8">
                      <label class="col-sm-4 control-label">License ID</label>
                        <input type="text" class="form-control" id="RefId" name="RefId" placeholder="Enter License No" required />
                      </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit" >
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Enter your License Identification Number</p></li>
		   <li>
          <p>Check Submit</p></li>
		    </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->
