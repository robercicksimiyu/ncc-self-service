<?php

    include 'pdf/fpdf.php';
    include 'pdf/fpdi.php';

    // initiate FPDI
    $pdf = new FPDI();
    // Building Structures Variables /////////////////
    $refNumber  ="REFNUMBER";
    $PLANREGNO  ="REGISTERNUMBER";
    $ownerNames  = "Owner";
    $ownerEmail = "Email";
    $ownerPhoneNo = "Mobile NUMBER";
    $ownerPostalAddress = "PO Box";
    $architectRegNo =  "REGNUMBER";
    $architectNames  ="Architect";
    $architectEmail = "Email";
    $architectPhoneNo  ="Mobile NUMBER";
    $architectPostalAddress  ="PO Box";
    $engineerRegNo  ="REGNUMBER";
    $engineerNames  = "Engineer";
    $engineerEmail  =  "Email";
    $engineerPhoneNo  ="Mobile NUMBER";
    $engineerPostalAddress  ="PO Box";
    $currentLandUse  ="Current Land Use";
    $zone  ="Zone";
    // $projectDetailedDescription
    // $landTenure <select> ***
    $numberofUnits  ="Number Of Units";
    $lrNo  ="LR NUMBER";
    $plotSize  ="PLot Size";
    $nearestRoad = "Nearest Road";
    $estate  ="Estate";
    $subcounty  ="Sub County";
    $ward  ="ward";
    $soilType  ="Type Of Soil";
    $waterSupplier  ="Water Supplier";
    // $sewerageDisposalMethod "Sewerage Disposal"
    $basementArea  ="basementArea";
    $mezzaninefloorArea = "mezzaninefloorArea";
    $floor1Area  ="floor1Area";
    $floor2Area = "floor2Area";
    $floor3Area  ="floor3Area";
    $floor4Area = "floor4Area";
    $Others = "Others";
    $totalArea = "totalArea";
    $projectCost = "Project Cost";
    $inspectionFees  = "Inspection Fees";
    $Foundation  ="Foundation";
    $externalWalls  ="External Walls";
    $mortar  ="Mortar";
    $roofCover  ="Roof Cover";
    $dampProofCourse  ="Damp Proof";
    //////////////////////////////////////////////////////

    $date = date('Y-m-d');
    //$customer = substr($customerName, 0, 17);

    // BUILDING STRUCTURES ///////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');

    // import page 1 ////////////////////////////////
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    // //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244);

    //$customer = substr($customerName, 0, 17);
$page->drawText($refNumber, 36, 68);

$page->drawText($date,36, 73;

$page->drawText($PLANREGNO, 52, 107);

$page->drawText($lrNo, 65, 120);

    // import page 2 /////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(2);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

    $page->drawText($PLANREGNO, 143, 26);

    $page->drawText($architectName, 70, 129);

    $page->drawText($architectRegNo, 161, 129);

    $page->drawText($architectEmail, 67, 134);

    $page->drawText($architectPhoneNo, 167, 134);

    $page->drawText($architectPostalAddress, 160, 139);

    $page->drawText($engineerNames, 73, 139);

    $page->drawText($engineerRegNo, 162, 154);

    $page->drawText($engineerEmail, 64, 160);

    $page->drawText($engineerPhoneNo, 167, 160);

    $page->drawText($engineerPostalAddress, 60, 165);

    $page->drawText($ownerNames, 65, 189);

    $page->drawText($date, 143, 180);

    $page->drawText($ownerEmail, 155, 189);

    $page->drawText($ownerPhoneNo, 165, 195);

    $page->drawText($ownerPostalAddress, 60, 165);

    // import page 3 ///////////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/reg.pdf');
    $tplIdx = $pdf->importPage(3);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

$page->drawText($currentLandUse, 69, 28);

$page->drawText($zone, 137, 28);

    $pdf->SetXY(88, 37);
    $pdf->Write(0, "Project Description XXXXXXXXXXXXXXXXXXXXXXXXXXX");
$page->drawText($bal, 450, 200);

    $pdf->SetXY(40, 42);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
$page->drawText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 40, 42);

    $pdf->SetXY(40, 46);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
$page->drawText($bal, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 40, 46);



    $pdf->SetFont('Arial', '', 14);
$page->drawText("0", 36, 62);

$page->drawText("0", 36, 67);

$page->drawText("0", 36, 72);

$page->drawText("0", 36, 77);

    $pdf->SetFont('Arial', '', 11);

$page->drawText($numberofUnits, 133,91);

$page->drawText($lrNo, 67, 96);

$page->drawText($plotSize, 150,96);

$page->drawText($nearestRoad, 95, 105);

$page->drawText($estate, 78, 110);

$page->drawText($subcounty, 144, 110);

$page->drawText($soilType, 66, 114);

$page->drawText($waterSupplier, 71, 124);

    $pdf->SetXY(157, 124);
    $pdf->Write(0, "Sewerage Disposal");
$page->drawText("Sewerage Disposal",157, 124);

    $pdf->SetXY(104, 129);
    $pdf->Write(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
$page->drawText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 104, 129);

$page->drawText($basementArea, 97, 158);

$page->drawText($mezzaninefloorArea, 97, 158);

$page->drawText($floor1Area, 97, 168);

$page->drawText($floor2Area, 97, 173);

$page->drawText($floor3Area, 97, 178);

$page->drawText($floor4Area, 97, 183);

$page->drawText($Others, 97, 188);

$page->drawText($totalArea, 97, 193);

$page->drawText($projectCost, 93, 202);

$page->drawText($inspectionFees, 84, 207);

$page->drawText($Foundation, 76, 217);

$page->drawText($externalWalls, 76, 222);

$page->drawText($mortar, 76, 227);

$page->drawText($roofCover, 76, 232);

$page->drawText($dampProofCourse, 82,237);



    // INDEMNITY ////////////////////////////////////////

    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile('pdf/idemity.pdf');
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
    // set the sourcefile

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);

    //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244); //rgb(66, 133, 244)

$page->drawText("REFNUMBER", 22, 105);

$page->drawText("12/12/2012", 22, 114);

$page->drawText("Architect", 20, 121);

    $pdf->SetFont('Arial', '', 8);
$page->drawText("REGISTERNUMBER", 117,119);

        $pdf->SetFont('Arial', '', 11);
$page->drawText("PO. BOX", 152, 121);

$page->drawText("Structural Engineer", 20, 130)

    $pdf->SetFont('Arial', '', 8);
$page->drawText("REGISTERNUMBER", 132, 128);

        $pdf->SetFont('Arial', '', 11);
$page->drawText("PO. BOX", 162, 130));

$page->drawText("Owner", 20, 139);

$page->drawText("Owner PO Box", 112, 139);


    $pdf->SetFont('Arial', '', 9);
$page->drawText("PLANREGNO", 70, 144);
$page->drawText("LRNUMBER", 120, 144);

    $pdf->SetFont('Arial', '', 11);
$page->drawText("Developer Name", 40, 221);
$page->drawText("Architect Name", 40, 230);
$page->drawText("Engineer", 53, 239);




    $pdf->Output('building_structures_and_indemnity.pdf', 'D');
