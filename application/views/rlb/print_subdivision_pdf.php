<?php

    include 'pdf/fpdf.php';
    include 'pdf/fpdi.php';
    //var_dump($this->session->all_userdata());exit;
    // initiate FPDI
    $pdf = new FPDI();

    $date = date('Y-m-d');
    //$customer = substr($customerName, 0, 17);
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile(APPPATH . 'assets/back/receipts/HFWU16.pdf');
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
    // set the sourcefile

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);

    //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244); //rgb(66, 133, 244)

    $pdf->SetXY(20, 121);
    $pdf->Write(0,$date);

    $pdf->SetXY(45, 90);
    $pdf->Write(0," DIRECTOR OF URBAN PLANNING");

        $pdf->SetXY(119, 133);
        $pdf->Write(0,$this->session->userdata('applicantNames'));

            $pdf->SetXY(123, 142);
            $pdf->Write(0,$this->session->userdata('applicantAddress'));

                $pdf->SetXY(125, 151);
                $pdf->Write(0,$this->session->userdata('applicantProfession'));

    //Owners name and address
    $pdf->SetXY(70, 170);
    $pdf->Write(0,$this->session->userdata('ownerNames')."    ".$this->session->userdata('ownerAddress'));

    //As above field
    $pdf->SetXY(100, 175);
    $pdf->Write(0,"AS ABOVE");

        //Lr number
        $pdf->SetXY(76, 191);
        $pdf->Write(0,$this->session->userdata('lrNo'));

        //Road, District and Town (Zone)
        $pdf->SetXY(79, 196);
        $pdf->Write(0, $this->session->userdata('physicalAddress')." (".$this->session->userdata('zone').")");

        //acrage
        $pdf->SetXY(57, 201);
        $pdf->Write(0,$this->session->userdata('acreage')."  Hectares");

    //Subdivision involved & application number
    $pdf->SetXY(57, 215);
    $pdf->Write(0,$this->session->userdata('subdivisionInvolved'));//.". ".(isset($this->session->userdata('subdivisionApplicationNo'))?" Application number: ".$this->session->userdata('acreage'):""));

    //SECTION B - SUBDIVISION AND AMALGAMATION

    //description subdivision
    $pdf->SetXY(28, 243);
    $pdf->Write(0, $this->session->userdata('projectDetailedDescription'));

    //purposes of the land being used Date last used
    $pdf->SetXY(31, 259);
    $pdf->Write(0, "  Current Use:".$this->session->userdata('currentUse'));
    $pdf->SetXY(31, 264);
    $pdf->Write(0, "  Current Use Date:".$this->session->userdata('lastUseDate'));

    //means of access of construction
    //$pdf->SetXY(68, 276);
    //$pdf->Write(0,($this->session->userdata('meansOfAccessContruction')?'Available':'N/A'));


    // SECTION B (Cont.) & SECTION C  ///////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile(APPPATH . 'assets/back/receipts/HFWU15.pdf');

    // import page 1 ////////////////////////////////
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    $pdf->SetTextColor(66, 133, 244);

        //water supply
        $pdf->SetXY(81, 12);
        $pdf->Write(0,$this->session->userdata('waterSupply'));

        //sewerage disposal
        $pdf->SetXY(92, 17);
        $pdf->Write(0,$this->session->userdata('sewerageDisposal'));

        //surface waterdisposal
        $pdf->SetXY(99, 21);
        $pdf->Write(0,$this->session->userdata('surfaceWaterDisposal'));

        //refuse disposal
        $pdf->SetXY(87, 26);
        $pdf->Write(0,$this->session->userdata('refuseDisposal'));

        //easement details
        $pdf->SetXY(52, 44);
        $pdf->Write(0,$this->session->userdata('easementDetails'));

    // SECTION C - EXTENTION OF USE OR CHANGE OF USE  ///////////////////////////////

    //Subdividion is involved & application number
    $pdf->SetXY(48, 70);
    $pdf->Write(0,"N/A");

    //proposed development
    $pdf->SetXY(48, 93);
    $pdf->Write(0,"N/A");

    //purposes of the land being used
    $pdf->SetXY(48, 114);
    $pdf->Write(0, "N/A");

    //means of access
    $pdf->SetXY(48, 134);
    $pdf->Write(0,"N/A");

    //nature of change of user
    $pdf->SetXY(48, 158);
    $pdf->Write(0,"N/A");

    //walls details details
    $pdf->SetXY(48, 178);
    $pdf->Write(0,"N/A");

        //water supply
        $pdf->SetXY(89, 197);
        $pdf->Write(0,"N/A");
        //sewerage disposal
        $pdf->SetXY(97, 201);
        $pdf->Write(0,"N/A");
        //surface waterdisposal
        $pdf->SetXY(102, 206);
        $pdf->Write(0,"N/A");
        //refuse disposal
        $pdf->SetXY(91, 211);
        $pdf->Write(0,"N/A");

    //easement details
    $pdf->SetXY(48, 227);
    $pdf->Write(0,"N/A");

        //area of land affected
        $pdf->SetXY(98, 246);
        $pdf->Write(0,"N/A");
        //area covered by building
        $pdf->SetXY(107, 251);
        $pdf->Write(0,"N/A");
        //Percentage of site covered
        $pdf->SetXY(110, 256);
        $pdf->Write(0,"N/A");

    $pdf->Output('regularization_form.pdf', 'D');
