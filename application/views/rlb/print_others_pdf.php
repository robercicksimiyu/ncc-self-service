<?php

    include 'pdf/fpdf.php';
    include 'pdf/fpdi.php';
    //var_dump($this->session->all_userdata());exit;
    // initiate FPDI
    $pdf = new FPDI();
    // Building Structures Variables /////////////////
    $refid  = $this->session->userdata('RefId');
    $planNo  = $this->session->userdata('lrNo');
    $ownerNames  =$this->session->userdata('ownerNames');
    $ownerEmail = $this->session->userdata('architectEmail');
    $ownerPhoneNo =$this->session->userdata('jpwnumber');
    $ownerPostalAddress = $this->session->userdata('ownerAddress');

    $architectRegNo = $this->session->userdata('architectRegNo');
    $architectNames  = $this->session->userdata('ownerAddress');
    $architectEmail = $this->session->userdata('architectEmail');
    $architectPhoneNo  = $this->session->userdata('architectPhoneNo');
    $architectPostalAddress  =$this->session->userdata('architectPostalAddress');

    $engineerRegNo  =$this->session->userdata('engineerRegNo');
    $engineerNames  = $this->session->userdata('engineerNames');
    $engineerEmail  = $this->session->userdata('engineerEmail');
    $engineerPhoneNo  = $this->session->userdata('engineerPhoneNo');
    $engineerPostalAddress  = $this->session->userdata('engineerPostalAddress');

    $currentLandUse  = "Current Land Use";
    $zone  = "Zone";

    $projectDetailedDescription = $this->session->userdata('projectDetailedDescription');
    $projectDetailedDescription = substr($projectDetailedDescription, 0, 150);
    $project2 = $project3 = '';
    // $projectDetailedDescription $array = explode( "\n", wordwrap( $str, $x));
    // Split by 46 first
    // $array = split_str($projectDetailedDescription, 46);
    // if(sizeof($array) > 1){
    //     $project2 = substr($projectDetailedDescription,46,103);
    //     $project3 = substr($projectDetailedDescription,103,150);
    //     $projectDetailedDescription = substr($projectDetailedDescription,0, 46);
    // }
    // $landTenure <select> ***

    $numberofUnits  = $this->session->userdata('numberofUnits');
    $lrNo  = $this->session->userdata('lrNo');
    $plotSize  = $this->session->userdata('plotSize');
    $nearestRoad = $this->session->userdata('nearestRoad');
    $estate  = $this->session->userdata('estate');
    $subcounty  = $this->session->userdata('subcounty');
    $ward  = $this->session->userdata('ward');

    $soilType  = "Type Of Soil";
    $waterSupplier  = "Water Supplier";

    // $sewerageDisposalMethod "Sewerage Disposal"

    $basementArea  = "basementArea";
    $mezzaninefloorArea = "mezzaninefloorArea";
    $floor1Area  = "floor1Area";
    $floor2Area = "floor2Area";
    $floor3Area  = "floor3Area";
    $floor4Area = "floor4Area";
    $Others = "Others";
    $totalArea = "totalArea";

    $projectCost = "Project Cost";
    $inspectionFees  = "Inspection Fees";

    $Foundation  = "Foundation";
    $externalWalls  = "External Walls";
    $mortar  = "Mortar";

    $roofCover  = "Roof Cover";
    $dampProofCourse  = "Damp Proof";
    //////////////////////////////////////////////////////

    $date = date('Y-m-d');
    //$customer = substr($customerName, 0, 17);
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile(APPPATH . 'assets/back/receipts/HFWU16.pdf');
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
    // set the sourcefile

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);

    //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244); //rgb(66, 133, 244)

    $pdf->SetXY(20, 121);
    $pdf->Write(0,$date);

    $pdf->SetXY(45, 90);
    $pdf->Write(0," DIRECTOR OF URBAN PLANNING");

        $pdf->SetXY(119, 133);
        $pdf->Write(0,"Applicant Name");

            $pdf->SetXY(123, 142);
            $pdf->Write(0,"Applicant Address");

                $pdf->SetXY(125, 151);
                $pdf->Write(0,"Profession");

    $pdf->SetXY(70, 170);
    $pdf->Write(0,$ownerNames."    ".$ownerPostalAddress);

    $pdf->SetXY(75, 175);
    $pdf->Write(0,"AS ABOVE");

    $pdf->SetXY(61, 191);
    $pdf->Write(0,$lrNo);

    //acrage
    $pdf->SetXY(43, 201);
    $pdf->Write(0,$this->session->userdata('acreage'));

    //description subdivision
    $pdf->SetXY(55, 236);
    $pdf->Write(0,$this->session->userdata('projectDetailedDescription'));

    //purposes of the land being used
    $pdf->SetXY(24, 264);
    $pdf->Write(0,$this->session->userdata('currentUse'));

    //road nearby
    $pdf->SetXY(68, 276);
    $pdf->Write(0,($this->session->userdata('meansOfAccessContruction')?'Available':'Not Available'));


    // BUILDING STRUCTURES ///////////////////////////////
    // add a page
    $pdf->AddPage();
    // set the sourcefile
    $pdf->setSourceFile(APPPATH . 'assets/back/receipts/HFWU15.pdf');

    // import page 1 ////////////////////////////////
    $tplIdx = $pdf->importPage(1);
    // use the imported page as the template
    $pdf->useTemplate($tplIdx, null, null, 0, 0, true);

    // now write some text above the imported page
    $pdf->SetFont('Arial', '', 11);
    // //$pdf->SetTextColor(44, 62, 80);
    $pdf->SetTextColor(66, 133, 244);

    //$customer = substr($customerName, 0, 17);
    //acrage
    $pdf->SetXY(81, 12);
    $pdf->Write(0,$this->session->userdata('waterSupply'));

    //registered number
    $pdf->SetXY(92, 17);
    $pdf->Write(0,$this->session->userdata('sewerageDisposal'));

    //description subdivision
    $pdf->SetXY(99, 21);
    $pdf->Write(0,$this->session->userdata('surfaceWaterDisposal'));

    //purposes of the land being used
    $pdf->SetXY(87, 26);
    $pdf->Write(0,$this->session->userdata('refuseDisposal'));

    //purposes of the land being used
    $pdf->SetXY(41, 40);
    $pdf->Write(0,$this->session->userdata('easementDetails'));



    //project description
   /* $pdf->SetXY(64, 88);
    $pdf->Write(0,$this->session->userdata('projectDetailedDescription'));

     //project description
    $pdf->SetXY(66, 128);
    $pdf->Write(0,$this->session->userdata('meansOfAccessContruction'));
*/
   /* $pdf->SetXY(37, 113);
    $pdf->Write(0,$this->session->userdata('currentUse'));



    $pdf->SetXY(80,197);
    $pdf->Write(0,$this->session->userdata('waterSupply'));

    //registered number
    $pdf->SetXY(90, 201);
    $pdf->Write(0,$this->session->userdata('sewerageDisposal'));

    //description subdivision
    $pdf->SetXY(97, 206);
    $pdf->Write(0,$this->session->userdata('surfaceWaterDisposal'));

    //purposes of the land being used
    $pdf->SetXY(86, 212);
    $pdf->Write(0,$this->session->userdata('refuseDisposal'));*/





    // add a page



    $pdf->Output('regularization_form.pdf', 'D');
