 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Pay Regularization <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Pay Regularization</a></li>
          <li class="active">Pick Regularization</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">

          <h4 class="panel-title panelx">Which Regularization do you wish to make payments for?</h4>
          <p>Choose the regularization option you would loke to pay for</p>
          <ol>
            <li> Building Structures
            </li>
            <li>Other forms of regularization 
              <ul>
                <li>Amalgamation</li>
                <li>Change of Use</li>
                <li>Extention of Use</li>
                <li>Subdivision of land</li>
              </ul>
            </li>
          </ol>
        </div>
        <div class="panel-body">
        <p></p>
        </div>
          <div id="sub">
            <div class="col-md-6 col-sm-12">
              <p> <a href="<?php echo base_url(); ?>rlb/payRegBuilding"><input type="submit" value="Building Structures" class="btn btn-primary pull-right btn-lg" > </a> </p>
            </div>
            <div class="col-md-6 col-sm-12">
              <p> <a href="<?php echo base_url(); ?>rlb/payReg"><input type="submit" value="Other Regularization" class="btn btn-danger btn-lg"> </a> </p>
            </div>
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Read Carefully and pick the suitable choice</p></li>
              </ol>
        </div>

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    // $('#loader').hide();
    // $('#subcounty').on('change',function(e){
    //   $('#loader').show();
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     $('#loader').hide();
    //     $('#ward').html(data);
    //     jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    //     if(document.getElementById("ward") === null){
    //       $('#sub').hide();
    //     }else{
    //       $('#sub').show();
    //     }

    //   });

    // });
  });
  </script>
