<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Permit<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Regularization</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>
<?php $rescode = $reg['rescode'];?>
<?php if($rescode=="200"): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">

            <div class="panel-body">
              <div class="row">
              <?php echo form_open('rlb/completeRegPayment',array('class' =>"form-block")); ?>
	             <?php if(strcmp($x = substr($reg['Refid'],0,7), 'NCCRF08')==0):?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">BUILDING STRUCTURES DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>Owners Name</b></td>
                        <td><?php echo isset($reg['OwnersName'])?$reg['OwnersName']:''; ?></td>
                        <input type="hidden" name="OwnersName" value="<?php echo isset($reg['OwnersName'])?$reg['OwnersName']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Phone Number</b></td>
                        <td><?php echo isset($reg['PhoneNumber'])?$reg['PhoneNumber']:'';?></td>
                        <input type="hidden" name="Phone" value="<?php echo isset($reg['PhoneNumber'])?$reg['PhoneNumber']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Land Rate Number</b></td>
                        <td><?php echo isset($reg['LrNo'])?$reg['LrNo']:''; ?></td>
                        <input type="hidden" name="LrNo" value="<?php echo isset($reg['LrNo'])?$reg['LrNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>License ID</b></td>
                        <td><?php echo isset($reg['Refid'])?$reg['Refid']:'';?></td>
                        <input type="hidden" name="Refid" value="<?php echo isset($reg['Refid'])?$reg['Refid']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Architects Name</b></td>
                        <td><?php echo isset($reg['ArchitectsName'])?$reg['ArchitectsName']:''; ?></td>
                        <input type="hidden" name="ArchitectsName" value="<?php echo isset($reg['ArchitectsName'])?$reg['ArchitectsName']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Engineers Name</b></td>
                        <td><?php echo isset($reg['EngineersName'])?$reg['EngineersName']:''; ?></td>
                        <input type="hidden" name="EngineersName" value="<?php echo isset($reg['EngineersName'])?$reg['EngineersName']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Invoice No.</b></td>
                        <td><?php echo isset($reg['InvoiceNo'])?$reg['InvoiceNo']:'';?></td>
                        <input type="hidden" name="InvoiceNo" value="<?php echo isset($reg['InvoiceNo'])?$reg['InvoiceNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td><?php echo isset($reg['InvoiceStatus'])?$reg['InvoiceStatus']:'';?></td>
                        <input type="hidden" name="InvoiceStatus" value="<?php echo isset($reg['InvoiceStatus'])?$reg['InvoiceStatus']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Amount</b></td>
                        <td><?php echo isset($reg['Amount'])?$reg['Amount']:'';?></td>
                        <input type="hidden" name="Amount" value="<?php echo isset($reg['Amount'])?$reg['Amount']:'';?>" />
                        <input type="hidden" name="transid" value="<?php echo isset($reg['TransactionID'])?$reg['TransactionID']:'';?>"/>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
		  <?php else: ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">REGULARIZATION DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>Owners Name</b></td>
                        <td><?php echo isset($reg['OwnersName'])?$reg['OwnersName']:''; ?></td>
                        <input type="hidden" name="OwnersName" value="<?php echo isset($reg['OwnersName'])?$reg['OwnersName']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Phone Number</b></td>
                        <td><?php echo isset($reg['PhoneNumber'])?$reg['PhoneNumber']:'';?></td>
                        <input type="hidden" name="Phone" value="<?php echo isset($reg['PhoneNumber'])?$reg['PhoneNumber']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Land Rate Number</b></td>
                        <td><?php echo isset($reg['LrNo'])?$reg['LrNo']:''; ?></td>
                        <input type="hidden" name="LrNo" value="<?php echo isset($reg['LrNo'])?$reg['LrNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>License ID</b></td> 
                        <td><?php echo isset($reg['Refid'])?$reg['Refid']:'';?></td>
                        <input type="hidden" name="Refid" value="<?php echo isset($reg['Refid'])?$reg['Refid']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Invoice No.</b></td>
                        <td><?php echo isset($reg['InvoiceNo'])?$reg['InvoiceNo']:'';?></td>
                        <input type="hidden" name="InvoiceNo" value="<?php echo isset($reg['InvoiceNo'])?$reg['InvoiceNo']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td><?php echo isset($reg['InvoiceStatus'])?$reg['InvoiceStatus']:'';?></td>
                        <input type="hidden" name="InvoiceStatus" value="<?php echo isset($reg['InvoiceStatus'])?$reg['InvoiceStatus']:'';?>" />
                      </tr>
                      <tr>
                        <td><b>Amount</b></td>
                        <td><?php echo isset($reg['Amount'])?$reg['Amount']:'';?></td>
                        <input type="hidden" name="amount" value="<?php echo isset($reg['Amount'])?$reg['Amount']:'';?>" />
                        <input type="hidden" name="transid" value="<?php echo isset($reg['TransactionID'])?$reg['TransactionID']:'';?>" />
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
		  <?php endif; ?>
                  <div class="panel-footer">
                    <div class="row">
                    <?php
                        echo'<div class="col-sm-6">';
                        echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
                            echo'</div>';
                        echo '<input type="submit" id="bill_status"  value="Complete Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('#','Cancel',array('class'=>'btn btn-primary'));
                        ?>
                    </div>
                  </div>
                <?php echo form_close(); ?>
              </div>

        </div>

    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Complete Payment</h4>
            <p></p>
            <p>Kindly enter your e-wallet pin and click complete payment.</p>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $reg['Refid'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
<?php elseif($rescode!="200"): ?>
<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo $reg['message'];?>
                <a href="#" class="btn btn-primary receipt" onclick="goBack()" style="float:right;margin-top:-5px;"><i class="fa fa-mail-reply"></i>Back</a>
                </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel -->
<?php endif;?>
<script>
function goBack() {
    window.history.back();
}
</script>
