<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Building Structures</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">Building Structures</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
         <div class="panel panel-default col-md-10">
            <div class="panel-heading">
              <h4><em>Land Details</em></h4>
            </div>
            <?php echo form_open('rlb/building_structures/step3'); ?>
            <?php if(isset($previousStepData)):?>
                  <?php foreach($previousStepData as $key => $value){
                    echo "<input type='hidden' name='$key' id='$key' value='$value'/>";
                  }?>
            <?php endif;?>
            <div class="panel-body">
              <div class="row">
                <fieldset class="fset">
                  <legend class="fset"> Building / Land Tenure Details</legend>
                  <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('currentLandUse')) ? 'has-error' : ''?>">
                     <label class="control-label"> Current Land Use</label>
                     <select  name="currentLandUse"  id="currentLandUse" class="form-control">
                       <option value="">Select one</option>>
                       <option value="Domestic building"<?php echo set_select('currentLandUse', 'Domestic building'); ?> >Domestic building</option>
                       <option value="Warehouse Class Building"<?php echo set_select('currentLandUse', 'Warehouse Class Building'); ?> >Warehouse Class Building</option>
                       <option value="Public Building"<?php echo set_select('currentLandUse', 'Public Building'); ?>>Public Building</option>
                       <option value="Other"<?php echo set_select('currentLandUse', 'Other'); ?>>Other</option>
                     </select>

                     <small><?php echo form_error('currentLandUse'); ?></small>
                   </div>
                   <div class="col-sm-4 <?=(form_error('zone_zoneid')) ? 'has-error' : ''?>">
                     <label class="control-label">Zone</label>
                     <select class="form-control" name="zone_id" id="zone_id" >
                       <option value="">Select Zone</option>
                       <?php
                       $incr = 0;
                       for($i = 0; $i <= 56; $i++) {
                         echo '<option value="' .$zones[$i + $incr] .'">' .$zones[$i + $incr + 1] .'</option>';
                         $incr ++;
                       }
                       ?>
                      <input type='hidden' id='zone' name='zone' value='' readonly/>
                     </select>
                   </div>
                   <div class="col-sm-4 <?=(form_error('landTenure')) ? 'has-error' : ''?>">
                     <label class="control-label">Land Tenure</label>
                     <select class="form-control" name="landTenure" id="landTenure" >
                       <option value="">Select Land Tenure</option>
                       <option value="1"<?php echo set_select('landTenure', '1'); ?>>Freehold/ Leasehold by commissioner of lands </option>
                       <option value="2"<?php echo set_select('landTenure', '2'); ?>>Leasehold by Kenya Railways Corporation </option>
                       <option value="3"<?php echo set_select('landTenure', '3'); ?>>Leasehold by Nairobi City Council </option>
                       <option value="4"<?php echo set_select('landTenure', '4'); ?>>Affidavit is provided where ownership is in form of share certificate.  </option>
                     </select>
                   </div>
                   <div class="col-sm-12 <?=(form_error('projectDetailedDescription')) ? 'has-error' : ''?>">
                     <label class="control-label"> Project Details Description</label>
                     <textarea rows="3" name="projectDetailedDescription"  id="projectDetailedDescription" class="form-control" placeholder="" ><?=set_value('projectDetailedDescription')?></textarea>
                     <small><?php echo form_error('projectDetailedDescription'); ?></small>
                   </div>
                </div>
                </fieldset>
                <fieldset class="fset">
                  <legend class="fset"> Project location / description</legend>
                  <div class="form-group">
                    <div class="col-sm-3 <?=(form_error('numberofUnits')) ? 'has-error' : ''?>">
                      <label class="control-label">Number Of Units</label>
                      <input type="text" name="numberofUnits"  id="numberofUnits" class="form-control" placeholder="" value="<?=set_value('numberofUnits', '0')?>"  />
                      <small><?php echo form_error('numberofUnits'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('lrNo')) ? 'has-error' : ''?>">
                      <label class="control-label">LR Number</label>
                      <input type="text" name="lrNo"  id="lrNo" class="form-control" placeholder="" value="<?=set_value('lrNo')?>"  />
                      <small><?php echo form_error('lrNo'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('plotSize')) ? 'has-error' : ''?>">
                      <label class="control-label">Plot Size</label>
                      <input type="text" name="plotSize"  id="plotSize" class="form-control" placeholder="" value="<?=set_value('plotSize', '0')?>"  />
                      <small><?php echo form_error('plotSize'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('soilType')) ? 'has-error' : ''?>">
                      <label class="control-label">Soil type</label>
                      <input type="text" name="soilType"  id="soilType" class="form-control" placeholder="" value="<?=set_value('soilType')?>"  />
                      <small><?php echo form_error('soilType'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('nearestRoad')) ? 'has-error' : ''?>">
                      <label class="control-label">Nearest Road</label></label>
                      <input type="text" name="nearestRoad"  id="nearestRoad" class="form-control" placeholder="" value="<?=set_value('nearestRoad')?>"  />
                      <small><?php echo form_error('nearestRoad'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('estate')) ? 'has-error' : ''?>">
                      <label class="control-label">Estate</label>
                      <input type="text" name="estate"  id="estate" class="form-control" placeholder="" value="<?=set_value('estate')?>"  />
                      <small><?php echo form_error('estate'); ?></small>
                    </div>
                    <div class="col-sm-3 <?=(form_error('subcounty')) ? 'has-error' : ''?>">
                      <label class="control-label">Sub County</label>
                      <?php if(!isset($subcounties)){?>
                      <input type="text" name="subcounty"  id="subcounty" class="form-control" placeholder="" value="<?=set_value('subcounty')?>"  />
                      <?php } else {
                          echo '<select class="form-control" name="zonecode" id="zonecode">';
                            foreach($subcounties as $county): ?>
                            <option value="<?php echo $county->ID; ?>"><?php echo $county->Name; ?></option>
                            <?php endforeach;?>
                          <?php echo '</select>';
                          echo '<input type="hidden" id="subcounty" name="subcounty" class="form-control" required/>';
                        }?>
                      <small><?php echo form_error('subcounty'); ?></small>
                    </div>

                    <div class="col-sm-3 <?=(form_error('ward')) ? 'has-error' : ''?>">
                      <label class="control-label">Ward</label>
                      <?php if(!isset($subcounties)){?>
                      <input type="text" name="ward"  id="ward" class="form-control" placeholder="" value="<?=set_value('ward')?>"  />
                      <?php }else{
                          echo '<select class="form-control" name="ward" id="ward">';
                          echo '</select>';
                       }?>
                      <small><?php echo form_error('ward'); ?></small>
                    </div>
                  </div>
                </fieldset>
             </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
          </div><!-- contentpanel -->



     <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->
  <script type="text/javascript">
    $('document').ready(function(){
      if($('#subdiv').val()==='0'){
        $('#permissions').hide();
      }
      if($('#applied').val()==='1' && $('#subdiv').val()==='1'){
        $('#sub_app_no').show();
      }else {
        $('#sub_app_no').hide();
      }
      $('#subdiv').on('change',function(){
        if($(this).val()==='0'){
          $('#permissions').hide();
          $('#sub_app_no').hide();
        }else{
          $('#permissions').show();
            $('#sub_app_no').show();
          if($('#applied').val()==='0'){
            $('#sub_app_no').hide();
          }
        }
      });
      $('#applied').on('change',function(){
        if($(this).val()==='0'){
          $('#sub_app_no').hide();
        }else{
          $('#sub_app_no').show();
        }
      });
  //     $("#subcode").change(function(event) {
	 //    $.ajax({
		// url: '<?php //echo base_url(); ?>sbp/getWard',
		// type: 'POST',
		// dataType: 'json',
		// cache: false,
		// data: $("#subcode").serialize(),
		// beforeSend: function() {
		//     console.log("Trying....");
		// },
		// success: function(data) {
		//     $("#wardcode").empty();
		//     var optgroup = data;
		//     for (var i = 0; i < optgroup.length; i++) {
		//         var id = optgroup[i].ID;
		//         var name = optgroup[i].Name;
		//         $('#wardcode').append($('<option>', {
		//             "value": id
		//         }).text(name));
		//     }
		// },
		// error: function(err) {
		//     console.log(err)
		// }
  //      });
        $("#zone_id").on('change', function (){
          var selectedText = $("#zone_id option:selected").text();
          $("#zone").val(selectedText);
        });

      });
  </script>

      <script type="text/javascript">
      $(document).ready(function(){

      code=$("#zonecode").find('option:selected').text();
      $('#subcounty').val(code);

      jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
      $('#zonecode').on('change',function(){
      code=$(this).find('option:selected').text();
      $('#subcounty').val(code);
      //$('#subcounty').text(code);
      // alert(code);
      });
      // $('#wardcode').on('change',function(){
      //   code=$(this).find('option:selected').text();
      //   $('#wardname').val(code);
      //       //alert(code);
      //     });
      });
      </script>

      <script type="text/javascript">
      jQuery(document).ready(function(){
        $.ajax({
        url: '<?php echo base_url(); ?>sbp/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("Trying....");
        },
        success: function(data) {
            $("#wardcode").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#ward').append($('<option>', {
                    "value": name
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });

      });
      </script>

      <script type="text/javascript">
     $("#zonecode").change(function(event) {
    $.ajax({
        url: '<?php echo base_url(); ?>sbp/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("Trying....");
        },
        success: function(data) {
            $("#ward").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#ward').append($('<option>', {
                    "value": name
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });
});

        </script>
