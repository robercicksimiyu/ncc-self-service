<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Building Structures</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">Building Structures</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
         <div class="panel panel-default col-md-10">
           <div class="panel-heading">
             <h4><em>Participants Details</em></h4>
           </div>
           <?php echo form_open('rlb/building_structures/step2'); ?>
            <div class="panel-body">
              <div class="row">
              <fieldset class="fset">
                <legend class="fset"> Owners Details</legend>
                <div class="form-group">
                  <div class="col-sm-6 <?=(form_error('ownerNames')) ? 'has-error' : ''?>">
                    <label class="control-label">Owner Names</label>
                    <input type="text" name="ownerNames"  id="ownerNames" class="form-control" placeholder="" value="<?=set_value('ownerNames')?>"  />
                    <small><?php echo form_error('ownerNames'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 <?=(form_error('ownerPostalAddress')) ? 'has-error' : ''?>">
                    <label class="control-label">Owner Postal Address</label>
                    <input type="text" name="ownerPostalAddress"  id="ownerPostalAddress" class="form-control" placeholder="" value="<?=set_value('ownerPostalAddress')?>"  />
                    <small><?php echo form_error('ownerPostalAddress'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('ownerPhoneNo')) ? 'has-error' : ''?>">
                    <label class="control-label">Owner Phone No.</label>
                    <input type="text" name="ownerPhoneNo"  id="ownerPhoneNo" class="form-control" placeholder="" value="<?=set_value('ownerPhoneNo')?>"  />
                    <small><?php echo form_error('ownerPhoneNo'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('ownerEmail')) ? 'has-error' : ''?>">
                    <label class="control-label">Owner Email</label>
                    <input type="text" name="ownerEmail"  id="ownerEmail" class="form-control" placeholder="" value="<?=set_value('ownerEmail')?>"  />
                    <small><?php echo form_error('ownerEmail'); ?></small>
                  </div>
                </div>
              </fieldset>
                  <!-- Architect-->
              <fieldset class="fset">
                <legend class="fset"> Architects Details</legend>
                <div class="form-group">
                  <div class="col-sm-6 <?=(form_error('architectNames')) ? 'has-error' : ''?>">
                    <label class="control-label">Architect Names</label>
                    <input type="text" name="architectNames"  id="architectNames" class="form-control" placeholder="" value="<?=set_value('architectNames')?>"  />
                    <small><?php echo form_error('architectNames'); ?></small>
                  </div>
                  <div class="col-sm-6 <?=(form_error('architectRegNo')) ? 'has-error' : ''?>">
                    <label class="control-label">Registration No.</label>
                    <input type="text" name="architectRegNo"  id="architectRegNo" class="form-control" placeholder="" value="<?=set_value('architectRegNo')?>"  />
                    <small><?php echo form_error('architectRegNo'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 <?=(form_error('architectPostalAddress')) ? 'has-error' : ''?>">
                    <label class="control-label">Architect Postal Address</label>
                    <input type="text" name="architectPostalAddress"  id="architectPostalAddress" class="form-control" placeholder="" value="<?=set_value('architectPostalAddress')?>"  />
                    <small><?php echo form_error('architectPostalAddress'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('architectPhoneNo')) ? 'has-error' : ''?>">
                    <label class="control-label">Architect Phone No.</label>
                    <input type="text" name="architectPhoneNo"  id="architectPhoneNo" class="form-control" placeholder="" value="<?=set_value('architectPhoneNo')?>"  />
                    <small><?php echo form_error('architectPhoneNo'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('architectEmail')) ? 'has-error' : ''?>">
                    <label class="control-label">Architect Email</label>
                    <input type="text" name="architectEmail"  id="architectEmail" class="form-control" placeholder="" value="<?=set_value('architectEmail')?>"  />
                    <small><?php echo form_error('architectEmail'); ?></small>
                  </div>
                </div>
              </fieldset>
              <!-- Engineers-->
              <fieldset class="fset">
                <legend class="fset"> Engineers Details</legend>
                <div class="form-group">
                  <div class="col-sm-6 <?=(form_error('engineerNames')) ? 'has-error' : ''?>">
                    <label class="control-label">Engineer Names</label>
                    <input type="text" name="engineerNames"  id="engineerNames" class="form-control" placeholder="" value="<?=set_value('engineerNames')?>"  />
                    <small><?php echo form_error('engineerNames'); ?></small>
                  </div>
                  <div class="col-sm-6 <?=(form_error('engineerRegNo')) ? 'has-error' : ''?>">
                    <label class="control-label">Registration No.</label>
                    <input type="text" name="engineerRegNo"  id="engineerRegNo" class="form-control" placeholder="" value="<?=set_value('engineerRegNo')?>"  />
                    <small><?php echo form_error('engineerRegNo'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 <?=(form_error('engineerPostalAddress')) ? 'has-error' : ''?>">
                    <label class="control-label">Engineer Postal Address</label>
                    <input type="text" name="engineerPostalAddress"  id="engineerPostalAddress" class="form-control" placeholder="" value="<?=set_value('engineerPostalAddress')?>"  />
                    <small><?php echo form_error('engineerPostalAddress'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('engineerPhoneNo')) ? 'has-error' : ''?>">
                    <label class="control-label">Engineer Phone No.</label>
                    <input type="text" name="engineerPhoneNo"  id="ownerPhoneNo" class="form-control" placeholder="" value="<?=set_value('engineerPhoneNo')?>"  />
                    <small><?php echo form_error('engineerPhoneNo'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('engineerEmail')) ? 'has-error' : ''?>">
                    <label class="control-label">Engineer Email</label>
                    <input type="text" name="engineerEmail"  id="engineerEmail" class="form-control" placeholder="" value="<?=set_value('engineerEmail')?>"  />
                    <small><?php echo form_error('engineerEmail'); ?></small>
                  </div>
                </div>
              </fieldset>
              </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->
