<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Building Structures</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
          <li class="active">Building Structures</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-10">
           <div class="panel-heading">
             <h4><em>Project Details</em></h4>
           </div>
           <?php echo form_open('rlb/building_structures/complete', array('id' => 'myform')); ?>
            <div class="panel-body">
            <?php if(isset($previousStepData)):?>
                  <?php foreach($previousStepData as $key => $value){
                    echo "<input type='hidden' name='$key' id='$key' value='$value'/>";
                  }?>
            <?php endif;?>
              <div class="row">
              <fieldset class="fset">
                <legend class="fset">Water, Sewerage BuiltUp and Areas Details</legend>
                <div class="form-group">
                  <div class="col-sm-3 <?=(form_error('waterSupplier')) ? 'has-error' : ''?>">
                    <label class="control-label">Water Supplier</label>
                    <select class="form-control" name="waterSupplier" id="waterSupplier">
                      <option value="">Select one</option>
                      <option value="Borehole"<?php echo set_select('waterSupplier', 'Borehole'); ?>>Borehole</option>
                      <option value="Nairobi City Water and Sewerage Company"<?php echo set_select('waterSupplier', 'Nairobi City Water and Sewerage Company'); ?>>Nairobi City Water and Sewerage Company</option>
                      <option value="Rain water"<?php echo set_select('waterSupplier', 'Rain water'); ?>>Rain water</option>
                      <option value="River"<?php echo set_select('waterSupplier', 'River'); ?>>River</option>
                      <option value="Other Suppliers"<?php echo set_select('waterSupplier', 'Other Suppliers'); ?>>Other Suppliers</option>
                    </select>                    
                    <small><?php echo form_error('waterSupplier'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('sewerageDisposalMethod')) ? 'has-error' : ''?>">
                    <label class="control-label">Sewerage Disposal Method</label>
                    <select name="sewerageDisposalMethod"  id="sewerageDisposalMethod" class="form-control">
                      <option value="">Select One</option>
                      <option value="Nairobi city water and sewerage company"<?php echo set_select('sewerageDisposalMethod', 'Nairobi city water and sewerage company'); ?>>Nairobi city water and sewerage company</option>
                      <option value="Conservative Tank"<?php echo set_select('sewerageDisposalMethod', 'Conservative Tank'); ?>>Conservative Tank</option>
                      <option value="Biodigester"<?php echo set_select('sewerageDisposalMethod', 'Biodigester'); ?>>Biodigester</option>
                      <option value="Pit Latrine"<?php echo set_select('sewerageDisposalMethod', 'Pit Latrine'); ?>>Pit Latrine</option>
		      <option value="Septic tank"<?php echo set_select('sewerageDisposalMethod', 'Septic tank'); ?>>Septic tank</option>
                    </select>                   
                    <small><?php echo form_error('sewerageDisposalMethod'); ?></small>
                  </div>
                   <!--<div class="col-sm-3 <?=(form_error('buildingCategory')) ? 'has-error' : ''?>">
                     <label class="control-label">Building Category</label>
                     <select class="form-control" name="buildingCategory" id="buildingCategory" >
                       <option value="<?php echo set_value('buildingCategory')?>">Select Category</option>
                       <?php
                         //foreach($building_zone as $bzone){
                           //echo " <option value='".$bzone[1]."'>".$bzone[0]."</option>";
                         //}
                       ?>
                     </select>
                   </div>-->
                </div>
                <div class="form-group">
                  <div class="col-sm-3 <?=(form_error('basementArea')) ? 'has-error' : ''?>">
                    <label class="control-label">Basement Area</label>
                    <input type="text" name="basementArea"  id="basementArea" class="form-control areas" placeholder="" value="<?=set_value('basementArea', '0')?>"  />
                    <small><?php echo form_error('basementArea'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('mezzaninefloorArea')) ? 'has-error' : ''?>">
                    <label class="control-label">Mezzanine Floor Area</label>
                    <input type="text" name="mezzaninefloorArea"  id="mezzaninefloorArea" class="form-control areas" placeholder="" value="<?=set_value('mezzaninefloorArea', '0')?>"  />
                    <small><?php echo form_error('mezzaninefloorArea'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('floor1Area')) ? 'has-error' : ''?>">
                    <label class="control-label">Floor Area 1</label>
                    <input type="text" name="floor1Area"  id="floor1Area" class="form-control areas" placeholder="" value="<?=set_value('floor1Area', '0')?>"  />
                    <small><?php echo form_error('floor1Area'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('floor2Area')) ? 'has-error' : ''?>">
                    <label class="control-label">Floor Area 2</label>
                    <input type="text" name="floor2Area"  id="floor2Area" class="form-control areas" placeholder="" value="<?=set_value('floor2Area', '0')?>"  />
                    <small><?php echo form_error('floor2Area'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-3 <?=(form_error('floor3Area')) ? 'has-error' : ''?>">
                    <label class="control-label">Floor Area 3</label>
                    <input type="text" name="floor3Area"  id="floor3Area" class="form-control areas" placeholder="" value="<?=set_value('floor3Area', '0')?>"  />
                    <small><?php echo form_error('floor3Area'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('floor4Area')) ? 'has-error' : ''?>">
                    <label class="control-label">Floor Area 4</label>
                    <input type="text" name="floor4Area"  id="floor4Area" class="form-control areas" placeholder="" value="<?=set_value('floor4Area', '0')?>"  />
                    <small><?php echo form_error('floor4Area'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('Others')) ? 'has-error' : ''?>">
                    <label class="control-label">Area of other floors(SUM)</label>
                    <input type="text" name="Others"  id="Others" class="form-control areas" placeholder="" value="<?=set_value('Others', '0')?>"  />
                    <small><?php echo form_error('Others'); ?></small>
                  </div>
                  <div class="col-sm-3 <?=(form_error('totalArea')) ? 'has-error' : ''?>">
                    <label class="control-label">Total Area</label>
                    <input type="text" name="totalArea"  id="totalArea" class="form-control" placeholder="" value="<?=set_value('totalArea', '0')?>" readonly />
                    <small><?php echo form_error('totalArea'); ?></small>
                  </div>
                </div>
              </fieldset>
              <fieldset class="fset">
                <legend class="fset"> Cost, Fees and Construction Material Details</legend>
                <div class="form-group">
                   <div class="col-sm-4 <?=(form_error('buildingCategory')) ? 'has-error' : ''?>">
                     <label class="control-label">Building Category</label>
                     <select class="form-control" name="buildingCategory" id="buildingCategory"onchange="calc()" >
                       	<option value="">Select Building Category</option>
                            <?php foreach ($building_zone as $key => $category) {
                                echo "<option value='".$category[1]."'".set_select('buildingCategory', $category[1]).">".$category[0]."</option>";
                            }?>
                     </select>
                   </div>
                  <div class="col-sm-4 <?=(form_error('projectCost')) ? 'has-error' : ''?>">
                    <label class="control-label">Project Cost</label>
                    <input type="text" name="projectCost"  id="projectCost" class="form-control" placeholder="" value="<?=set_value('projectCost','0')?>" readonly />
                    <small><?php echo form_error('projectCost'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('inspectionFees')) ? 'has-error' : ''?>">
                    <label class="control-label">Inspection Fees</label>
                    <input type="text" name="inspectionFees"  id="inspectionFees" class="form-control" placeholder="" value="50000" readonly />
                    <small><?php echo form_error('inspectionFees'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 <?=(form_error('Foundation')) ? 'has-error' : ''?>">
                    <label class="control-label">Foundation</label>
                    <input type="text" name="Foundation"  id="Foundation" class="form-control" placeholder="" value="<?=set_value('Foundation')?>"  />
                    <small><?php echo form_error('Foundation'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('externalWalls')) ? 'has-error' : ''?>">
                    <label class="control-label">External Walls</label>
                    <input type="text" name="externalWalls"  id="externalWalls" class="form-control" placeholder="" value="<?=set_value('externalWalls')?>"  />
                    <small><?php echo form_error('externalWalls'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('mortar')) ? 'has-error' : ''?>">
                    <label class="control-label">Mortar</label>
                    <input type="text" name="mortar"  id="mortar" class="form-control" placeholder="" value="<?=set_value('mortar')?>"  />
                    <small><?php echo form_error('mortar'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 <?=(form_error('roofCover')) ? 'has-error' : ''?>">
                    <label class="control-label">Roof Cover</label>
                    <input type="text" name="roofCover"  id="roofCover" class="form-control" placeholder="" value="<?=set_value('roofCover')?>"  />
                    <small><?php echo form_error('roofCover'); ?></small>
                  </div>
                  <div class="col-sm-4 <?=(form_error('dampProofCourse')) ? 'has-error' : ''?>">
                    <label class="control-label">Damp Proof Course</label>
                    <input type="text" name="dampProofCourse"  id="dampProofCourse" class="form-control" placeholder="" value="<?=set_value('dampProofCourse')?>"  />
                    <small><?php echo form_error('dampProofCourse'); ?></small>
                  </div>
                </div>
              </fieldset>
              </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">

          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->

  </div><!-- mainpanel -->

  <script type="text/javascript">
      var $form = $('#myform'),
      $summands = $form.find('input.areas'),
      $sumDisplay = $('#totalArea');
      $category = $('#buildingCategory');
      $sumCost = $('#projectCost');

    $form.delegate('input.areas', 'change', function ()
    {
      var sum = 0;
      $summands.each(function ()
      {
          var value = Number($(this).val());
          if (!isNaN(value)) sum += value;
      });

      $sumDisplay.val(sum);

      var value = Number($('#buildingCategory').val());
      if (!isNaN(value)) sum *= value;

      $sumCost.val(sum);
    });

	function calc() {
		product = 1;
		 var value = Number($('#totalArea').val());
		 var value2 = Number($('#buildingCategory').val());

      		if (!isNaN(value) && !isNaN(value2)) product *= value * value2;
      		$sumCost.val(product);
	}
  </script> 

