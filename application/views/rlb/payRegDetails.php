<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Pay for Regularization<span>Apply Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">Pay for Regularization</a></li>
      <li class="active">Pay</li>
    </ol>
  </div>
</div>
<?php if($reg['ownerNames']!=NULL): ?>
  <div class="contentpanel" >
    <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
      <?php $refid = $reg['RefID'] ;?>
      <div class="panel-body">
        <div class="row">
          <?php echo form_open('rlb/prepareRegPayment',array('class' =>"form-block")); ?>
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
                <tr>
                  <th colspan="2" style="text-align:center;">REGULARIZATION DETAILS</th>
                </tr>
              </thead>
                      <tbody>
                        <tr>
                          <td><b>OWNERS NAME</b></td>
                          <td><?php echo $reg['ownerNames']; ?></td>
                          <input type="hidden" name="ownerNames" value="<?php echo $reg['ownerNames'];?>" />
                        </tr>
                        <!-- <tr>
                          <td><b>CONTACT ADDRESS</b></td>
                          <td><?php echo $reg['ownerAddress'];?></td>
                          <input type="hidden" name="ownerAddress" value="<?php echo $reg['ownerAddress'];?>" />
                        </tr> -->
                        <tr>
                          <td><b>LICENSE ID</b></td>
                          <td><?php echo strtoupper($reg['RefID']);?></td>
                          <input type="hidden" name="RefID" value="<?php echo $reg['RefID'];?>" />
                          <input type="hidden" name="Regtype" value="1" />
                        </tr>
                        <tr>
                          <td><b>INVOICE NUMBER</b></td>
                          <td><?php echo $reg['InvoiceNo'];?></td>
                          <input type="hidden" name="InvoiceNo" value="<?php echo $reg['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                          <td><b>FORM STATUS</b></td>
                          <td><?php echo (isset($reg['form_status'])&&$reg['form_status'])?'PAID':"PENDING";?></td>
                          <input type="hidden" name="form_status" value="<?php echo $reg['form_status'];?>" />
                        </tr>
                        <tr>
                          <td><b>AMOUNT</b></td>
                          <td><?php echo $reg['amount'] ;?></td>
                          <input type="hidden" name="amount" value="<?php echo $reg['amount'];?>" />
                        </tr>
                      </tbody>
                      </table>
                    </div><!-- table-responsive -->

                    <div class="panel-footer">
                      <div class="row">
                        <?php
                        echo '<input type="submit" id="bill_status"  value="Proceed to Make Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('#','Cancel',array('class'=>'btn btn-primary','onClick'=>'goBack()'));
                        ?>
                        <?php echo form_close(); ?>
                      </div>
                    </div>
                  </div>

                </div>

              </div><!-- contentpanel -->



              <div class="panel panel-default col-md-3" >
                <div class="panel-heading" style="text-align:center;">
                  <h4 class="panel-title panelx">Follow these steps</h4>
                  <p>Enter your e-wallet PIN</p>
                  <p>Click Proceed to make payment</li>
                    <li><p>Or Click below to print the Invoice and pay.</p></li>
                  </ul>
                  <p><a href="<?php echo base_url(); ?>rlb/printInvoice/<?php echo $reg['ownerNames'];?>/<?php echo $reg['RefID'];?>/<?php echo $reg['lrNo'];?>/<?php echo $reg['InvoiceNo'];?>/<?php echo $reg['amount'];?>/<?php echo $reg['form_status'];?>"><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a></p>
                </div>
              </div>
        </div>
      </div><!-- mainpanel 1346861-->
    <?php elseif($reg['ownerNames']=NULL):  ?>
    <div class="alert alert-info">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      <?php echo "That Licence ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
              <?php endif; ?>

  <script>
function goBack() {
    window.history.back();
}
</script>
