<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Success</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Regularization Of Land &amp; Buildings</a></li> 
          <li class="active">Success</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
         <div class="panel panel-default col-md-8">
           <div class="panel-heading">
             <h4><?php echo strtoupper($heading).' FORM DETAILS';?></h4>
             <h4>(<em>PLEASE CONFIRM YOUR DETAILS  BEFORE SUBMITTING</em>)</h4>
           </div>
           <?php if(isset($callback) && isset($heading)):?>
           <?php  echo form_open('rlb/'.$callback.'/submit', array('id' => 'myform')); ?>
           <?php   ?>

            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped ">
                  <!-- <thead>
                    <tr>
                      <th colspan="3" style="text-align:center;"></th>
                    </tr>
                  </thead> -->
                  <tbody>
                    <?php if(isset($post_data)):?>
                      <?php
                      //ksort($post_data);
                      foreach($post_data as $key => $value){
			if(isset($fields[strtoupper($key)])){
                        if( $key == 'zone_id' || $key == 'totalArea' || $key == 'inspectionFees'  || $key == 'projectCost'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td>";
                          echo "  <td colspan='3'><input type='text' id='$key' name='$key' value='$value' style='width:100%;' readonly/></td>";
                          echo "</tr>";
                        }else if ($key =="basementArea" || $key =="mezzaninefloorArea" ||$key =="floorArea1" ||$key =="floorArea2" ||$key =="floorArea3" ||$key =="floorArea4" ||$key =="OtherArea"){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td>";
                          echo "  <td colspan='3'><input type='text' id='$key' name='$key' class='areas' value='$value' style='width:100%;'/></td>";
                          echo "</tr>";
                        }else if($key == 'buildingCategory'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' onchange='calc()' >";
                          echo " <option value='$value'>Change ".$fields[strtoupper($key)]."</option>";
                           foreach ($building_zone as $key => $category) {
                                echo "<option value='".$category[1]."'".set_select('buildingCategory', $category[1]).">".$category[0]."</option>";
                              }
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'landTenure'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' onchange='calc()' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='1'>Freehold/ Leasehold by commissioner of lands </option>";
                          echo " <option value='2'>Leasehold by Kenya Railways Corporation </option>";
                          echo " <option value='3'>Leasehold by Nairobi City Council </option>";
                          echo " <option value='4'>Affidavit is provided where ownership is in form of share certificate.</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'surfaceWaterDisposal'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='Nairobi County services'>Nairobi County services</option>";
                          echo " <option value='Private arrangement'>Private arrangement</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'refuseDisposal'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='Nairobi city water and sewerage company'>Nairobi city water and sewerage company</option>";
                          echo " <option value='Nairobi County services'>Nairobi County services</option>";
                          echo " <option value='Private arrangement'>Private arrangement</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'currentUse' || $key == 'currentLandUse'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='Domestic building'>Domestic building</option>";
                          echo " <option value='Warehouse Class Building'>Warehouse Class Building</option>";
                          echo " <option value='Public Building'>Public Building</option>";
                          echo " <option value='Other Use Suppliers'>Other Use Suppliers</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'waterSupply' || $key == 'waterSupplier'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='Borehole'>Borehole</option>";
                          echo " <option value='Nairobi City Water and Sewerage Company'>Nairobi City Water and Sewerage Company</option>";
                          echo " <option value='Rain water'>Rain water</option>";
                          echo " <option value='River'>River</option>";
                          echo " <option value='Other Suppliers'>Other Suppliers</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'sewerageDisposalMethod' || $key == 'sewerageDisposal'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='$key' id='$key' >";
                          echo " <option value='$value'>Change ($value)</option>";
                          echo " <option value='Nairobi city water and sewerage company'>Nairobi city water and sewerage company</option>";
                          echo " <option value='Conservative Tank'>Conservative Tank</option>";
                          echo " <option value='Biodigester'>Biodigester</option>";
                          echo " <option value='Pit Latrine'>Pit Latrine</option>";
                          echo " <option value='Septic tank'>Septic tank</option>";
                          echo " </select>";
                          echo "</td></tr>";
                        }else if($key == 'zone' && isset($zones)){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo " <select class='form-control' name='zone_zoneid' id='zone_zoneid' >";
                          echo " <option value='".$post_data['zone_id']."'>Change Zone ($value)</option>";
                           foreach($zones as $zone){
                               echo "<option value='".$zone[0]."'>".$zone[1]."</option>";
                           }
                          echo " </select>";
                          echo " <input type='hidden' id='zone' name='zone' value='$value' readonly/>";
                          echo "</td></tr>";
                        }else if(isset($subcounties) && $key == 'subcounty'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo '<select class="form-control" name="zonecode" id="zonecode">';
                            foreach($subcounties as $county){ 
                                echo "<option value='$county->ID'>$county->Name</option>";
                            }
                          echo '</select>';
                          echo '<input type="hidden" id="subcounty" name="subcounty" value="$value" class="form-control"/>';

                          echo "</td></tr>";
                        }else if(isset($subcounties) && $key == 'ward'){
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td><td>";
                          echo '<select class="form-control" name="ward" id="ward">';
                          echo '</select>';
                          echo "</td></tr>";
                        }else {
                          echo "<tr>";
                          echo "  <td colspan='1'><b>".$fields[strtoupper($key)]."</b></td>";
                          echo "  <td colspan='3'><input type='text' id='$key' name='$key' value='$value' style='width:100%;'/></td>";
                          echo "</tr>";
                        }
			}else {
			echo "<tr>";
                        echo "  <td colspan='1'><b>".strtoupper($key)."</b></td>";
                        echo "  <td colspan='3'><input type='text' id='$key' name='$key' value='$value' style='width:100%;'/></td>";
                        echo "</tr>";
			}
                      }?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div><!-- table-responsive -->
            </div>
            <div class="panel-footer">
              <div class="row">
                <input type="submit" id="next1"  value="SUBMIT FILLED DETAILS" class="btn btn-primary"/>
              </div>
            </div>
            <?php echo form_close();?>
          <?php endif;?>
    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
      var $form = $('#myform'),
      $summands = $form.find('input.areas'),
      $sumDisplay = $('#totalArea');
      $category = $('#buildingCategory');
      $sumCost = $('#projectCost');

    $form.delegate('input.areas', 'change', function ()
    {
      var sum = 0;
      $summands.each(function ()
      {
          var value = Number($(this).val());
          if (!isNaN(value)) sum += value;
      });

      $sumDisplay.val(sum);

      var value = Number($('#buildingCategory').val());
      if (!isNaN(value)) sum *= value;

      $sumCost.val(sum);
    });

  function calc() {
    product = 1;
     var value = Number($('#totalArea').val());
     var value2 = Number($('#buildingCategory').val());

          if (!isNaN(value) && !isNaN(value2)) product *= value * value2;
          $sumCost.val(product);
  }

  $('document').ready(function(){
    var txt = $("#zone_zoneid option:selected").text();
    var val = $("#zone_zoneid option:selected").val();

    $('#zone_id').val(val);
    $('#zone').val(txt);
    $('select#zone_zoneid').on('change', function() {
        var txt = $("#zone_zoneid option:selected").text();
        var val = $("#zone_zoneid option:selected").val();

        $('#zone_id').val(val);
        $('#zone').val(txt);
      });
    });
  </script>

      <script type="text/javascript">
      jQuery(document).ready(function(){
        $.ajax({
        url: '<?php echo base_url(); ?>rlb/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("Trying....");
        },
        success: function(data) {
            $("#ward").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#ward').append($('<option>', {
                    "value": name
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });

      });
      </script>

      <script type="text/javascript">
     $("#zonecode").change(function(event) {
    $.ajax({
        url: '<?php echo base_url(); ?>rlb/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("Trying....");
        },
        success: function(data) {
            $("#ward").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#ward').append($('<option>', {
                    "value": name
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });
        var txt = $("#zonecode option:selected").text();
        $('#subcounty').val(txt);
});
        
        </script>