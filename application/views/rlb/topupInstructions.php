 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
        <h2><i class="fa fa-home"></i> Regularization of Land &amp; Buildings<span>Success</span></h2>
        <div class="breadcrumb-wrapper">
          <span class="label">You are here:</span>
          <ol class="breadcrumb">
            <li><a href="#">Regularization Of Land &amp; Buildings</a></li>
            <li class="active">Insufficient Funds Top Up Instructions</li>
          </ol>
      </div>
    </div>


    <div class="contentpanel" >

      <div class="panel panel-default col-md-8" style="margin-right:20px">
      <div class="panel-heading">
          <h4 class="panel-title panelx">INSUFFICIENT FUNDS IN YOUR WALLET</h4>
          <p>Please topup your wallet using any of the following channels and then complete the payment</p>
      </div>
      <div class="panel-heading">
          <h4 class="panel-title panelx">STEPI: TO TOPUP YOUR eJijiPAY ACCOUNT</h4>
          <h4 class="panel-title panelx">Option I: M-Pesa</h4>
            <ul>
              <li><p>Go to M-pesa Menu</p></li>
              <li><p>Lipa na M-pesa</p></li>
              <li><p> Select Pay Bill</p></li>
              <li><p> Enter business number: 147147</p></li>
              <li><p>Enter account number which is your phone number e.g. 0722xxxxxx</p></li>
              <li><p>Enter amount</p></li>
              <li><p>Enter M-pesa PIN</p></li>
              <li><p>You will receive a confirmation message from M-pesa.</p></li>
            </ul>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title panelx">OPTION II: Airtel Money</h4>
            <ul>
              <li><p> Go to airtel money</p></li>
              <li><p> Select make payments</p></li>
              <li><p>Select pay Bill</p></li>
              <li><p> Select Other</p></li>
              <li><p> Enter business name: 147147</p></li>
              <li><p> Enter amount and confirm the details</p></li>
              <li><p>Enter airtel money PIN</p></li>
              <li><p>Enter reference which is your phone number e.g. 0722xxxxxx</p></li>
            </ul>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title panelx">OPTION III: Large Amounts:</h4>
            <p>You can either top up online via Visa card or Master Card, visit the following Banks:</p>
            <ul>
              <li><p> Co-operative</p></li>
              <li><p>  Family</p></li>
              <li><p> CFC bank (RTGS)</p></li>
            </ul>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title panelx">OPTION I: Visa</h4>
            <p>Follow this to top up via Visa</p>
            <ul>
              <li><p>Select currency</p></li>
              <li><p>Enter your card number</p></li>
              <li><p>Enter CVV</p></li>
              <li><p> Enter your card expiry date</p></li>
              <li><p>Submit Payment
            </ul>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title panelx">OPTION II: MasterCard</h4>
            <ul>
              <li><p>Select currency</p></li>
              <li><p>Enter your card number</p></li>
              <li><p>Enter CVV</p></li>
              <li><p>Enter your card expiry date</p></li>
              <li><p>Submit payment
            </ul>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title panelx">OPTION III: TOP UP YOUR ACCOUNT VIA RTGS AT CFC STANBIC BANK</h4>
            <p>Follow these simple steps</p>
            <ul>
              <li><p>Bank name: - CfC Stanbic Bank</p></li>
              <li><p>Account name: - JAMBOPAY TRUST</p></li>
              <li><p> Account number: - 0100004299714</p></li>
              <li><p> Branch: - Head office (001)</p></li>
              <li><p>Reference: - 07xxxxxxxxxx (should be the mobile number of the wallet being toppedup).</p></li>
            </ul>
        </div>
        <div class="panel-body">
        <p></p>
        <div class="col-md-6 col-sm-6">
          <p> <a href="<?php echo base_url(); ?>selfservice/wallettopup" target="_blank" ><input type="submit" value="PROCEED TO TOPUP WALLET" class="btn btn-primary " style="float:right;"> </a> </p>
        </div>
        <div class="col-md-6 col-sm-6">
          <p> <?php echo anchor('#','COMPLETE PAYMENT',array('class'=>"btn btn-info",'style'=>"float:left;",'onClick'=>"goBack()"));?></p>
        </div>
          <div id="sub">
          </div>
        </div>
        </div><!-- panel-body -->
        <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->

          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li><p>Read Carefully these topup instructions</p></li>
            <li><p>Top up wallet and then complete payment</p></li>
          </ol>
        </div>

      </div><!-- panel -->

      </div><!-- panel -->

    </div><!-- contentpanel -->


  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  });
    function goBack() {
      window.history.back();
    }
  </script>
