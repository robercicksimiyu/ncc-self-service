 <div class="pageheader">
      <h2><i class="fa fa-th"></i> SR <span>Search Receipt</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li class="active">Enter License Number</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >
      <div class="panel panel-default col-md-8" style="margin-right:20px;">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="minimize">&minus;</a>
          </div>
          <h4 class="panel-title">Fill the details below</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('sbp/printreceiptcheck',array('class' =>"form-block")) ?>
            <div class="form-group">
              <label class="control-label">Business ID</label>
              <input type="text" size="100" class="form-control" id="bizId" name="bizId"  placeholder="Enter Business ID">
            </div>
            <?php 
            $error=$this->uri->segment(3); 
            if(isset($error)&&!empty($error)): ?>
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Receipt not found. Please check your License and search again!
              </div>
              <?php endif; ?>
            <input type="submit" class="btn btn-primary" value="Print Receipt">
            <button type="reset" class="btn btn-default">Reset</button>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
      
    </div><!-- contentpanel -->