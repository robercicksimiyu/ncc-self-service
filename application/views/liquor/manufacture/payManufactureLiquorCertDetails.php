 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Liqour Manufacturer Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Liqour Manufacturer</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
<?php if($liquor['Email']!=NULL): ?>
  <div class="contentpanel" >
    <div >
     <div class="panel panel-default col-md-8" style="margin-right:20px">
      <?php $refid = $liquor['RefID'] ;?>
      <div class="panel-body">
        <div class="row">
          <?php echo form_open('liquor/completeManufactureLiquorCertPayment',array('class' =>"form-block")); ?>
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
                <tr>
                  <th colspan="2" style="text-align:center;">LIQUOR MANUFACTURER CERTIFICATE DETAILS</th>
                </tr>
              </thead>
                      <tbody>
                        <tr>
                          <td><b>BUSINESS NAME</b></td>
                          <td><?php echo $liquor['Firm']; ?></td>
                          <input type="hidden" name="firm" value="<?php echo $liquor['Firm'];?>" />
                        </tr>
                        <tr>
                          <td><b>CONTACT PERSON</b></td>
                          <td><?php echo strtoupper($liquor['Fullname']);?></td>
                          <input type="hidden" name="fullname" value="<?php echo $liquor['Fullname'];?>" />
                          <input type="hidden" name="email" value="<?php echo $liquor['Email'];?>" />
                          <input type="hidden" name="telnumber" value="<?php echo $liquor['TelNumber'];?>" />
                        </tr>
                        <tr>
                          <td><b>LICENSE ID</b></td>
                          <td><?php echo $liquor['RefID'];?></td>
                          <input type="hidden" name="refid" value="<?php echo $liquor['RefID'];?>" />
                        </tr>
                        <tr>
                          <td><b>INVOICE NUMBER</b></td>
                          <td><?php echo $liquor['InvoiceNo'];?></td>
                          <input type="hidden" name="InvoiceNo" value="<?php echo $liquor['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                          <td><b>INSPECTION STATUS</b></td>
                          <td><?php if($liquor['Status']!=0){echo "APPROVED";} else {echo "NOT APPROVED";} ?></td>
                          <input type="hidden" name="InvoiceNo" value="<?php echo $liquor['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                          <td><b>LIQUOR CERTIFICATE FEE</b></td>
                          <td><?php echo $liquor['certAmount'] ;?></td>
                          <input type="hidden" name="certamount" value="<?php echo $liquor['certAmount'];?>" />
                        </tr>
                      </tbody>
                      </table>
                    </div><!-- table-responsive -->

                    <div class="panel-footer">
                      <div class="row">
                        <?php if($liquor['Status']!=0){
                        echo'<div class="col-sm-6">';
                        echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
                        echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
                            echo'</div>';
                        echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('health/completeHealthPermitDetails','Cancel',array('class'=>'btn btn-primary'));
                        }else{
                          echo '<div class="alert alert-info">';
                          echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>';
                          echo "You cannot Proceed to Pay until Inspection has been Approved.";
                          echo '</div>';
                        }
                        ?>
                        <?php echo form_close(); ?>
                      </div>
                    </div>
                  </div>

                </div>

              </div><!-- contentpanel -->

              <div class="panel panel-default col-md-3" >
                <div class="panel-heading" style="text-align:center;">
                  <h4 class="panel-title panelx">Follow these steps</h4>
                  <p>Enter your e-wallet PIN</p>
                  <p>Click Proceed to make payment</p>
                  <p></p>
                </div>
              </div>
        </div>
      </div><!-- mainpanel 1346861-->
    <?php elseif($liquor['Email']=NULL):  ?>
    <div class="alert alert-info">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
              <?php endif; ?>


