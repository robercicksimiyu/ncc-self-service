 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Liqour Manufacturer Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Liqour Manufacturer</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
  

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Fill in the form below </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open_multipart('liquor/displayManufactureLiquorDetails',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")); ?>
          <div class="row">
            <div class=" col-md-6 col-sm-12">
              <br><label class="col-sm-6 control-label">Business ID</label>
              <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
            </div>
          </div>
          <div class="row mb10">
            
            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Contact Person</label>
              <input type="text" class="form-control" id="fullnames" name="fullnames" placeholder="Enter Full names" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Contact Phone No.</label>
              <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter Phone no." required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Contact Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required/>
            </div>            
            <div class="col-md-6 col-sm-12">
              <br><label class="col-md-12 col-sm-12 control-label">Category</label>
              <select class="form-control input-sm mb15 chosen-select" name="category" id="category" data-placeholder="Select Category...">
                <option value="43">Import alcoholic drink licence (wholesale)-1,000,000) litres and above</option>
                <option value="44">Import alcoholic drink licence (wholesale)-Between 500,000 and 999,999 litres</option>
              </select>
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Alcohol Content</label>
              <input type="text" class="form-control" id="alcoholcontent" name="alcoholcontent" placeholder="Enter Alcohol Content" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Mode of Transportation</label>
              <input type="text" class="form-control" id="transportmode" name="transportmode" placeholder="Enter transport mode" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Type & Brand</label>
              <input type="text" class="form-control" id="brand" name="brand" placeholder="Enter Type & Brand" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-md-6 col-sm-12 control-label">Unit Capacity</label>
              <input type="text" class="form-control" id="unitcapacity" name="unitcapacity" placeholder="Enter Unit Capacity" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <label class="col-sm-6 control-label">Purpose</label>
              <input type="text" class="form-control" id="purpose" name="purpose" placeholder="Enter purpose" required />
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Import Cost</label>
              <input type="text" class="form-control" id="importcost" name="importcost" placeholder="Enter Import Cost" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">VAT Certificate</label>
              <input type="file" class="form-control" id="vatcert" multiple name="userfile[]" placeholder="Upload your VAT Cert" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">PIN Certificate</label>
              <input type="file" class="form-control" id="pincert" multiple name="userfile[]"name="vatcert" placeholder="Upload your PIN Cert" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Health Certificate</label>
              <input type="file" class="form-control" id="healthcert" multiple name="userfile[]" placeholder="Upload your Health Cert" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">ID DOCUMENT</label>
              <input type="file" class="form-control" id="iddoc" multiple name="userfile[]" placeholder="Upload your ID Doc" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">NEMA Certificate</label>
              <input type="file" class="form-control" id="nemacert" multiple name="userfile[]" placeholder="Upload your NEMA Cert" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">KEBS Certificate</label>
              <input type="file" class="form-control" id="kebscert" multiple name="userfile[]" placeholder="Upload your Health Cert" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Excise RETURN</label>
              <input type="file" class="form-control" id="excise" multiple name="userfile[]" placeholder="Upload your ID Doc" required/>
            </div>
            <div class="col-md-6 col-sm-12">
              <br><label class="col-sm-5 control-label">Architectural Drawing</label>
              <input type="file" class="form-control" id="archdrawing" multiple name="userfile[]" placeholder="Upload your NEMA Cert" required/>
            </div>
            <!-- <div class="col-sm-5">
              <label class="col-sm-5 control-label">Sub-County</label>
              <select class="form-control chosen-select" id="subcounty" name="subcounty" data-placeholder="Select Sub County...">
                        <option value=""></option>
                        <?php foreach($subcounty as $key=>$value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                        <?php } ?>
                  </select>
            </div>
            <div class="col-sm-5">
              <label class="col-sm-5 control-label">Ward</label>
              <div id="ward">
                <select class="form-control chosen-select" id="ward" name="ward" data-placeholder="Choose Sub County First..." required>
                  <option selected="selected" disabled="disabled" value=""></option>
                </select>
              </div>
            </div> -->
            <!-- <div class="col-sm-2" id="loader">
              <br><label class="col-sm-6 control-label"></label>
              <img src="<?php echo base_url();?>application/assets/back/images/loaders/loader6.gif" alt="" /> Loading...
            </div> -->
          </div>
          <div id="sub">
            <input type="submit" class="btn btn-primary" value="Submit" >
          </div>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
    <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li><p>Enter your Business Identification Number</p></li>
            <li><p>Enter your Email Address</p></li>
            <li><p>Click Submit</p></li>
          </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->




