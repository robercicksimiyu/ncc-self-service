  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Import Liquor Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Liquor Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
<?php $rescode = $liquor['rescode'];?>
<?php if($rescode=="200"): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;"><div class="alert alert-success" style="padding-top:10px;margin-left:20px;">
                          <button type="button" class="close" data-dismiss="success" aria-hidden="true"></button>
                          <?php echo "Your Application has successfully been paid.Await inspection to pay for the Certificate.";?>
                        </div>
                      </th>
                    </tr>
                      <tr>
                        <th colspan="2" style="text-align:center;">PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $liquor['Firm']; ?></td>
                        <input type="hidden" name="firm" value="<?php echo $liquor['Firm'];?>" />
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON</b></td>
                        <td><?php echo strtoupper($liquor['Fullname']);?></td>
                        <input type="hidden" name="fullname" value="<?php echo $liquor['Fullname'];?>" />
                        <input type="hidden" name="email" value="<?php echo $liquor['Email'];?>" />
                        <input type="hidden" name="telnumber" value="<?php echo $liquor['Tel_no'];?>" />
                      </tr>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo $liquor['Refid'];?></td>
                        <input type="hidden" name="refid" value="<?php echo $liquor['Refid'];?>" />
                      </tr>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $liquor['InvoiceNo'];?></td>
                        <input type="hidden" name="InvoiceNo" value="<?php echo $liquor['InvoiceNo'];?>" />
                      </tr>
                      <tr>
                        <td><b>APPLICATION FEE</b></td>
                        <td><?php echo $liquor['Amount'] ;?></td>
                        <input type="hidden" name="amount" value="<?php echo $liquor['Amount'];?>" />
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Payment Complete</h4>
            <p></p>
            <p>Kindly await for inspection in order for you to print the permit.</p>
            <p></p>
            <!-- <p> <a href="<?php echo base_url(); ?>health/printHealthPermitreceipt/<?php echo $fire['Refid'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p> -->
            <p></p>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
<?php elseif($rescode!="200"): ?>
<div class="contentpanel" >
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
      <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="alert alert-danger" style="padding-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo $liquor['rescode'];
                echo anchor('liquor/importLiquorApplicationForm','Back',array('class'=>"btn btn-primary",'style'=>"float:right;"));
                 ?>
                </div>
        </div>
    </div><!-- panel-body -->
  </div><!-- panel -->
</div><!-- contentpanel --> 
<?php endif;?>


