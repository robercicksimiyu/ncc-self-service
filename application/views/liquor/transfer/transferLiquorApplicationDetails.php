  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Liqour Transfer Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Liqour Transfer Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>

<div class="contentpanel" >
    <?php $refid = $liquor['RefID']; ?>
  <div class="panel panel-default col-md-8" style="margin-right:20px;">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
    	<div class="row">
            <?php echo form_open('liquor/completeTransferLiquorInspectionPayment',array('class' =>"form-block")); ?>
    		<div class="table-responsive">
    			<table class="table table-striped mb30">
    				<thead>
    					<tr>
    						<th colspan="2" style="text-align:center;">LIQUOR TRANSFER APPLICATION DETAILS</th>
    					</tr>
    				</thead>
    				<tbody>
                        <tr>
                            <td><b>BUSINESS NAME</b></td>
                            <td><?php echo $liquor['Firm']; ?></td>
                            <input type="hidden" name="firm" value="<?php echo $liquor['Firm'];?>" />
                        </tr>
                        <tr>
                            <td><b>CONTACT PERSON</b></td>
                            <td><?php echo strtoupper($liquor['Fullname']);?></td>
                            <input type="hidden" name="fullname" value="<?php echo $liquor['Fullname'];?>" />
                            <input type="hidden" name="email" value="<?php echo $liquor['Email'];?>" />
                            <input type="hidden" name="telnumber" value="<?php echo $liquor['TelNumber'];?>" />
                        </tr>
    					<tr>
    						<td><b>LICENSE ID</b></td>
    						<td><?php echo $liquor['RefID'];?></td>
                            <input type="hidden" name="refid" value="<?php echo $liquor['RefID'];?>" />
    					</tr>
                        <tr>
                            <td><b>CATEGORY</b></td>
                            <td><?php echo $liquor['Category'];?></td>
                            <input type="hidden" name="category" value="<?php echo $liquor['Category'];?>" />
                        </tr>
                        <tr>
                            <td><b>INVOICE NUMBER</b></td>
                            <td><?php echo $liquor['InvoiceNo'];?></td>
                            <input type="hidden" name="InvoiceNo" value="<?php echo $liquor['InvoiceNo'];?>" />
                        </tr>
                        <tr>
                            <td><b>APPLICATION FEE</b></td>
                            <td><?php echo $liquor['Amount'] ;?></td>
                            <input type="hidden" name="amount" value="<?php echo $liquor['Amount'];?>" />
                        </tr>
                        <tr>
                            <td><b>CERTIFICATE FEE</b></td>
                            <td><?php echo $liquor['certAmount'] ;?></td>
                            <input type="hidden" name="certamount" value="<?php echo $liquor['certAmount'];?>" />
                        </tr>
                        <div class="alert alert-info" style="padding-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    Your Application was successful,proceed to pay the inspection fee.
                  </div>
                </tbody>
            </table>
        </div><!-- table-responsive -->
        <div class="panel-footer">
                      <div class="row">
                        <?php 
                        echo'<div class="col-sm-6">';
                        echo '<input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
                        echo '<input type="hidden" id="refid" name="refid" value=" '.$refid.'" class="form-control" required/>';
                            echo'</div>';
                        echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
                        echo " ";
                        echo anchor('liquor/viewTerms','Cancel',array('class'=>'btn btn-primary'));
                        ?>
                        <?php echo form_close(); ?>
                      </div>
         </div>
    </div>

      <?php echo form_close(); ?>
    </div><!-- panel-body -->
  </div><!-- panel -->
  <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <h4 class="panel-title panelx">Follow these steps</h4>
        <p><ol>
        <li>Confirm your invoice Details</li>
        <li>Enter your wallet PIN to pay the inspection fee</li>
        </ol></p>
        <p></p>
        <p><!-- <a href="<?php echo base_url(); ?>fire/printFirePermitreceipt/<?php echo $refid;?>"><input type="submit" value="Print Invoice" class="btn btn-primary btn-lg" style="line-height:normal"></a> --></p>
    </div>
</div>
</div><!-- contentpanel --> 

