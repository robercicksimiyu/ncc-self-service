  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Print Fire Permit <?php # echo date("Y"); ?> <span>Apply Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Fire Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
<?php if($fire['Email']!=NULL): ?>
    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">HEALTH PERMIT DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $fire['Firm']; ?></td>
                        <input type="hidden" name="firm" value="<?php echo $fire['Firm'];?>" />
                      </tr>
                      <tr>
                        <td><b>CATEGORY</b></td>
                        <td><?php echo $fire['Category']; ?></td>
                        <input type="hidden" name="category" value="<?php echo $fire['Category'];?>" />
                      </tr>
                      <tr>
                        <td><b>BUILDING/STREET</b></td>
                        <td><?php echo $fire['Building']; ?></td>
                        <input type="hidden" name="building" value="<?php echo $fire['Building'];?>" />
                      </tr>
                      <tr>
                        <td><b>LICENSE ID</b></td>
                        <td><?php echo $fire['RefID'];?></td>
                        <input type="hidden" name="refid" value="<?php echo $fire['RefID'];?>" />
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON</b></td>
                        <td><?php echo strtoupper($fire['Fullname']);?></td>
                        <input type="hidden" name="fullname" value="<?php echo $fire['Fullname'];?>" />
                      </tr>
                      <tr>
                        <td><b>EMAIL</b></td>
                        <td><?php echo $fire['Email'];?></td>
                        <input type="hidden" name="email" value="<?php echo $fire['Email'];?>" />
                      </tr>
                      <tr>
                        <td><b>TELEPHONE No.</b></td>
                        <td><?php echo $fire['TelNumber'];?></td>
                        <input type="hidden" name="telnumber" value="<?php echo $fire['TelNumber'];?>" />
                      </tr>
                      <tr>
                        <td><b>APPROVAL STATUS</b></td>
                        <td><?php if($fire['Status']=="0"){echo "PENDING";} elseif($fire['Status']=="1"){echo "APPROVED";} ?></td>
                        <input type="hidden" name="address" value="<?php echo $fire['Status'];?>" />
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $fire['Amount'] ;?></td>
                        <input type="hidden" name="amount" value="<?php echo $fire['Amount'];?>" />
                      </tr>
                        </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title panelx">Print Permit</h4>
        <?php if($fire['Status']=="1"): ?>
            <p></p>
            <p> <a href="<?php echo base_url(); ?>fire/printFire/<?php echo $fire['RefID'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
        <?php elseif($fire['Status']=="0"): ?>
            <p></p>
            <p>Inspection Status is Pending hence Permit Unprintable</p>
        <?php endif; ?>
      </div>
    </div>
   </div>
  </div><!-- mainpanel 1346861-->
  <?php elseif($fire['Email']=NULL):  ?>
    <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "That License ID does not exist" ;?>
                  <!-- Payment Was Successfully Completed<br>
                  Your Business Permit is fully Paid -->
                </div>
  <?php endif; ?>


