 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Unified Business Permit<?php # echo date("Y"); ?> <span>Terms & Conditions</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Unified Business Permit</a></li>
          <li class="active">Fire Certificate Requirements</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-12" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Please read the Fire requirements below</h4>
          
        
        </div>
        <div class="panel-body">
        <table class="table">
          <thead>
            <tr>
               <th>Category</th>
               <th>Requirement</th>
            </tr>           
          </thead>
          <tbody>
            <tr>
              <td>Small shop/Hardware</td>
              <td>Extinguisher</td>
            </tr>
            <tr>
              <td>Large Shops/Hardware</td>
              <td >
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>Fire hose reels provided</li>
                  <li>Escape routes</li>
                  <li>Fire exit signs</li>
                  <li>Fire action notices</li>
                </ol>
              </td>
            </tr>
            <tr>
              <td>Plant Industry</td>
              <td >
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>Fire hose reels</li>
                  <li>Escape routes</li>
                  <li>Fire exit signs</li>
                  <li>Fire action notices</li>
                  <li>Alarm systems  </li>                  
                  <li>Smoke detectors</li>                  
                  <li>Heat detectors</li>                  
                </ol>
              </td>
            </tr>
             <tr>
              <td>Petrol station </td>
              <td>
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>No smoking signage</li>
                  <li>Assembly point  </li>
                  <li>Drainage system of waste provided </li>
                  <li>Switch off engine signage  </li>                                  
                </ol>
              </td>
            </tr>
             <tr>
              <td>Commercial and industry premises/offices </td>
              <td>
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>Fire hose reels</li>
                  <li>Escape routes</li>
                  <li>Fire exit signs</li>
                  <li>Fire action notices</li>
                  <li>Alarm systems  </li>                  
                  <li>Smoke detectors</li>                  
                  <li>Heat detectors</li>                  
                  <li>Fire assembly point  </li>                  
                </ol>
              </td>
            </tr>
            <tr>
              <td>Premises storing dangerous inflammable materials  </td>
              <td>
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>Automatic Fire Extinguishers</li>
                  <li>Air Circulation</li>
                  <li>Fire hose reels</li>
                  <li>Escape routes</li>
                  <li>Fire exit signs</li>
                  <li>Fire action notices</li>
                  <li>Alarm systems  </li>                  
                  <li>Smoke detectors</li>                  
                  <li>Heat detectors</li>                  
                  <li>Training enforced   </li>                  
                </ol>
              </td>
            </tr>
            <tr>
              <td>High-rise buildings –Ground floor to ten and above </td>
              <td>
                <ol>
                  <li>Fire Extinguisher</li>
                  <li>Fire hose reels</li>                 
                  <li>Escape routes</li>
                  <li>Fire exit signs</li>
                  <li>Fire action notices</li>
                  <li>Alarm systems  </li>                  
                  <li>Smoke detectors</li>                  
                  <li>Heat detectors</li>
                  <li>Fire assembly point</li>
                  <li>Dry risers  </li>
                  <li>Landing valves</li>                  
                  <li>Fire delivery hoses </li>                  
                  <li>Fire branches</li>                  
                </ol>
              </td>
            </tr>
          </tbody>
        </table>
        </div>
          
        </div><!-- panel-body -->
       

      </div><!-- panel -->
	        
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    // $('#loader').hide();
    // $('#subcounty').on('change',function(e){
    //   $('#loader').show();
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     $('#loader').hide();
    //     $('#ward').html(data);
    //     jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    //     if(document.getElementById("ward") === null){
    //       $('#sub').hide();
    //     }else{
    //       $('#sub').show();
    //     }

    //   });

    // });
  });
  </script>



