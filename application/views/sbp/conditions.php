 <!--  <div class="mainpanel"> -->

 <style type="text/css">
 .terms-headers{

 }
 </style>
  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Unified Business Permit<?php # echo date("Y"); ?> <span>Terms & Conditions</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Unified Business Permit</a></li>
          <li class="active">Terms & Conditions</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Please read the NCC Terms & Conditions below</h4>

          <ol>
          
            <li><p>A separate application form must be completed in respect of each separate business establishment.</p></li>
            <li><p>The permit is issued under County Government act No. 17 of 2012 laws of Kenya.</p></li>
            <li><p>Any offence under this act shall be dealt with in accordance with any written laws/act of this County and all the relevant county rules/regulations.</p></li>
            <li><p>A permit is a County property./regulations or any written law of the county is contravened.</p></li>
            <li><p>Renewal of this permit shall be done on expiry date.</p></li>
            <li><p>The owner of the business should notify this office once he/she closes the business.</p></li> 
            <li>
              <p>The advertisement signage included in the unified permit is  600mm x 300mm or less in size, any signage above this size should be paid separately. Please contact urban planning department.</p>
            </li>
            <li>
              <p>
                Granting this permit does not exempt the business identified above from complying with the current regulations on fire and safety as established by the Government of Kenya and Nairobi City County
              </p>
              <p>
                <strong><a  target="_blank" href="<?=base_url('sbp/requirements/fire')?>"><span class="text-danger">MUST READ FIRE AND SAFETY REQUIREMENTS</span></a></strong>
              </p>
            </li> 
            <?php if($type=="health"):?>  
                 
              <li>
                <p>
                  Granting this permit does not exempt the business identified above from complying with the current regulations on health and safety as established by the Government of Kenya and Nairobi City County
                </p>
                <p>
                   <strong><a class="text-danger" target="_blank" href="<?=base_url('sbp/requirements/health')?>"><span class="text-danger">MUST READ HEALTH REQUIREMENTS</span></a></strong>
                </p>
              </li>
            <?php endif;?>
          </ol>
        </div>
        <div class="panel-body">
        <p></p>
        </div>
          <div id="sub">
            <div class="col-md-6 col-sm-12">
              <p style="margin-left: 200px;"> <a href="<?php echo base_url(); ?>sbp/enter_acc_no"><input type="submit" value="Agree" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            </div>
            <div class="col-md-6 col-sm-12">
              <p> <a href="<?php echo base_url(); ?>selfservice/home"><input type="submit" value="Disagree" class="btn btn-danger btn-lg" style="line-height:normal"> </a> </p>
            </div>
          </div>
        </div><!-- panel-body -->
        <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Read Carefully and click Agree/Disagree</p></li>
              </ol>
        </div>
       
      </div><!-- panel -->

      </div><!-- panel -->
	        
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    // $('#loader').hide();
    // $('#subcounty').on('change',function(e){
    //   $('#loader').show();
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     $('#loader').hide();
    //     $('#ward').html(data);
    //     jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    //     if(document.getElementById("ward") === null){
    //       $('#sub').hide();
    //     }else{
    //       $('#sub').show();
    //     }

    //   });

    // });
  });
  </script>



