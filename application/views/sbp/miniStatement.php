<!-- <div class="mainpanel"> -->
 <div class="pageheader">
      <h2><i class="fa fa-home"></i> e-Wallet Mini Statement <span>county wallet mini statement</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Nairobi County e-Wallet</a></li>
          <li class="active">View Mini Statement</li>
        </ol>
      </div>
    </div>
   
  <div class="contentpanel" >
      
      <div class="row">
        <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" class="form-horizontal">
             <div class="panel panel-default" >
              <div class="panel-heading">
                <div class="panel-btns">
                  <a href="#" class="minimize"></a>
                </div>
                <h4 class="panel-title panelx">Mini Statement</h4>
                <p>NCC Personal Account Mini Statement</p>
              </div>
              <div class="panel-body" style="">
              
              <table class="table table-striped mb30">
                <thead>
                  <tr><th>Date</th><th>ID</th><th>From</th><th>Amount</th></tr>
                </thead>
                <?php #echo $xml;
                    $RESPONSE_CODE ="0";// $this->session->userdata('RESPONSE_CODE');
                  if($RESPONSE_CODE==0){

                      $statement=$xml;//$this->session->userdata('statement');
                      $data = explode("|",$statement);
                      $value =  array($data);
                      $display = '';
                      foreach ($data as $key => $value) {
                        echo '<tr>';
                        $data2=explode(",", $value);
                        $value2=array($data2);
                        foreach ($data2 as $key => $value2) {
                          echo "<td>".str_replace('JAMBOPAY', 'Nairobi Pay',$value2)."</td>";
                                    //echo "<td>".($xml->STATEMENT)."</td>";
                        }
                        echo '</tr>';

                      }
                    }else{

                      echo "Statement currently unavailable";
                    }
                ?>
              </table>
                 <div class="err-box" style="padding:30px;"></div> 
              </div>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
<!-- panel -->
       </div>
  </div><!-- contentpanel -->

    
</div><!-- mainpanel -->



