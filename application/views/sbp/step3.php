  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-10" style="margin-right:20px">
           <?php #echo $step2data['box'];?>
            <div class="panel-body">
              <div class="row">
                 <?php echo form_open('sbp/step4'); ?>
                      <div class="form-group">
                        <div class="col-sm-8 <?=(form_error('full_names'))?'has-error':''?>">
                          <label class="control-label">Full Names</label>
                          <input type="text" value="<?=set_value('full_names')?>" name="full_names" id="full_names" class="form-control" placeholder="" required />
                           <small><?php echo form_error('full_names'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('owner_box'))?'has-error':''?>">
                          <label class="control-label">P.O Box</label>
                          <input type="text" name="owner_box" value="<?=set_value('owner_box')?>" id="owner_box" class="form-control" placeholder="" required />
                          <small><?php echo form_error('owner_box'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('owner_postal_code'))?'has-error':''?>">
                          <label class="control-label">Postal Code</label>
                          <input type="text" name="owner_postal_code" value="<?=set_value('owner_postal_code')?>" id="owner_postal_code" class="form-control" placeholder="" required />
                          <small><?php echo form_error('owner_postal_code'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('owner_telephone'))?'has-error':''?>">
                          <label class="control-label">Telephone Number</label>
                          <input type="text" name="owner_telephone" id="owner_telephone" class="form-control" value="<?=set_value('owner_telephone')?>" placeholder="" required/>
                          <small><?php echo form_error('owner_telephone'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('owner_telephone2'))?'has-error':''?>">
                          <label class="control-label">Alternative Telephone Number</label>
                          <input type="text" name="owner_telephone2" id="owner_telephone2" class="form-control" value="<?=set_value('owner_telephone2')?>" placeholder=""/>
                          <small><?php echo form_error('owner_telephone2'); ?></small>
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Fax</label>
                          <input type="text" name="owner_fax" value="<?=set_value('owner_fax')?>" id="owner_fax" class="form-control" placeholder=""/>
                        </div>
                      </div>
                          
                          <input type="hidden" name="biz_name"  value="<?php echo $step2data['biz_name'];?>" />
                          <input type="hidden" name="doc_type"  value="<?php echo $step2data['doc_type'];?>" />
                          <input type="hidden" name="doc_no"  value="<?php echo $step2data['doc_no'];?>" />
                          <input type="hidden" name="pin_no"  value="<?php echo $step2data['pin_no'];?>" />
                          <input type="hidden" name="vat_no"  value="<?php echo $step2data['vat_no'];?>" />
                          <input type="hidden" name="box"  value="<?php echo $step2data['box'];?>" />
                          <input type="hidden" name="postal_code"  value="<?php echo $step2data['postal_code'];?>" />
                          <input type="hidden" name="town"  value="<?php echo $step2data['town'];?>" />

                          <input type="hidden" name="tel1"  value="<?php echo $step2data['tel1'];?>" />
                          <input type="hidden" name="tel2"  value="<?php echo $step2data['tel2'];?>" />
                          <input type="hidden" name="fax"  value="<?php echo $step2data['fax'];?>" />
                          <input type="hidden" name="email"  value="<?php echo $step2data['email'];?>" />
                          <input type="hidden" name="address"  value="<?php echo $step2data['address'];?>" />
                          <input type="hidden" name="plot_number"  value="<?php echo $step2data['plot_number'];?>" />
                          <input type="hidden" name="building"  value="<?php echo $step2data['building'];?>" />
                          <input type="hidden" name="floor"  value="<?php echo $step2data['floor'];?>" />
                          <input type="hidden" name="stall_room_no"  value="<?php echo $step2data['stall_room_no'];?>" />

                      <div class="panel-footer">
                        <div class="row">
                          <input type="submit" id="next3"  value="Next" class="btn btn-primary"/>
                        </div>
                      </div>
                  <?php echo form_close();?>
              </div>
              
            
        </div>
      
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->
    
  </div><!-- mainpanel -->



