<!--  <div class="mainpanel"> -->
<div class="pageheader">
  <h2><i class="fa fa-home"></i> Single Business Permit Renewal <span>Renew your Business Permit Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="">Single Business Permit</a></li>
      <li class="active">Check Status</li>
    </ol>
  </div>
</div>

<div class="contentpanel" >
  
  <div class="panel panel-default col-md-8" style="margin-right:20px">
    <div class="panel-heading">
      
      <h4 class="panel-title panelx">Enter Business ID </h4>
      <p>Cross check to make sure you have filled in the correct Business ID</p>
    </div>
    <div class="panel-body">
      <?php echo form_open('sbp/sbp_topup_prepare',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
      <div class="row mb10">
        <div class="col-sm-8">
          <label class="col-sm-6 control-label">Paid By</label>
          <input class="form-control input-sm mb15" name="paidby" id="paidby" placeholder="Paid By" value="<?php echo @$this->session->userdata('name'); ?>">
        </div>
        <div class="col-sm-8">
          <label class="col-sm-6 control-label">Business Id</label>
          <input class="form-control input-sm mb15" name="biz_id" id="biz_id" placeholder="Business Id">
        </div>
        <div class="col-sm-8">
          <label class="col-sm-6 control-label">Year</label>
          <select name="year" class="form-control input-sm mb15">
            <option value="2015 "> 2015</option>
            <option value="2016 "> 2016</option>
            
          </select>
        </div>
        <div class="col-sm-8">
          <label class="col-sm-6 control-label">Business Activity</label>
          <select name="businessactivity" class="form-control input-sm mb15">
            <?php foreach ($businesses as $business): ?>
            <option value="<?= $business->ID ?> "> <?= $business->Name ?></option>
            <?php endforeach ?>
          </select>
        </div>
        
      </div>
      <input type="submit" class="btn btn-primary" value="Check Status" >
      <button type="reset" class="btn btn-default">Reset</button>
      <?php echo form_close(); ?>
      </div><!-- panel-body -->
      </div><!-- panel -->
      <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
            
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
          <ol>
            <li>
              <p>Enter your Business Identification Number</p></li>
              <li>
                <p>Check Status</p></li>
              </ol>
            </div>
            
            </div><!-- panel -->
            
            </div><!-- contentpanel -->
            
            </div><!-- mainpanel -->