<!-- <div class="mainpanel"> -->
<div class="pageheader">
  <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="">Single Business Permit</a></li>
      <li class="active">Check Status</li>
    </ol>
  </div>
</div>
<div class="contentpanel" >
  <div >
    <div class="panel panel-default col-md-8" style="margin-right:20px">
      <div class="panel-body">
        <div class="row">
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
              
                <tr>
                  <th colspan="2" style="text-align:center;">BUSINESS DETAILS</th>
                </tr>
              </thead>
              <tbody>
              
              <?php if (isset($details->ErrorType)): ?>
              <tr>
                <td><b><?php echo $details->ErrorType;?></b></td>
                <td><?php echo $details->Message; ?></td>
              </tr>

                <?php else: ?>
              <tr>
                <td><b>BUSINESS CHANGE ID</b></td>
                <td><?php echo strtoupper($details->ID); ?></td>
              </tr>
              <tr>
                <td><b>BUSINESS ID</b></td>
                <td><?php echo $details->BusinessID; ?></td>
              </tr>


              <tr>
                <td></br></td>
                <td></td>
              </tr>

              <tr>
                <p>BUSINESS HAS BEEN CHANGED SUCCESFULLY CHECK YOUR DETAILS TO CONFIRM </p>
                </tr>
              <?php endif ?>
            
            <tr>
              <td> <a href="/ncc/epaymentsLive/selfservice/home">Home</a></td>
            </tr>             
            </tbody>
          </table>
          </div><!-- table-responsive -->
          
        </div>
        
        <div class="panel-footer">

   
          
         
        
        
      </div>
    </div>
    
    </div><!-- contentpanel -->
    <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li><p>Confirm that all the details displayed match your business</p></li>
        </ol>
      </div>
    </div><!-- mainpanel -->