  <!-- <div class="mainpanel"> -->
    <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Single Business Permit</a></li>
          <li class="active">Print Permit</li>
        </ol>
      </div>
    </div>
    
    <!-- <div class="pageheader">
      <a href="<?php //echo base_url();?>sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>
      <a href="<?php //echo base_url();?>sbp/print_sbp_preview/<?php //echo $sbp['bizzid'];?>"><i class="fa fa-print"></i>Print Permit</a>
    </div> -->


    <div class="contentpanel" >

    <?php if($sbp['rescode']==0): ?>
      
      <div class="row">
        <div class="col-md-9">
          <h5 class="subtitle mb5"></h5>
          <div class="table-responsive">
            <table class="table table-bordered mb30" style="width:100%;">
              <thead>
              <tr>
                <th colspan="2" style="text-align:center;">BUSINESS PERMIT DETAILS</th>
              </tr>
              </thead>
              <tbody >
                <tr style="">
                <td><b>APPLICANT/BUSINESS COMMERCIAL NAME</b></td>
                <td><?php echo strtoupper($sbp['bizname']);?></td>
                </tr>
                <tr>
                <td><b>BID</b></td>
                <td><?php echo $sbp['bizzid'];?></td>
                </tr>
                <tr>
                <td><b>APPROVAL STATUS</b></td>
                <td><?php 
                 if($sbp['approvalstatus']=="0" || $sbp['approvalstatus']=="1"){ echo "WAITING FOR APPROVAL";}
                 elseif($sbp['approvalstatus']=="2"){ echo "APPROVED";}
                 elseif($sbp['approvalstatus']=="3" ){ echo "DECLINED";}
                 elseif($sbp['approvalstatus']=="4" || $sbp['approvalstatus']=="5"){ echo "NOT APPROVED";}
                 #endif;
                 ?></td>
                </tr>
                <tr> 
                <td><b>BUSINESS ACTIVITY</b></td>
                <td><?php echo strtoupper($sbp['bizactivityname']);?></td>
                </tr>
                <tr>
                <td><b>STREET</b></td>
                <td><?php echo strtoupper($sbp['street']) ;?></td>
                </tr>
                <tr>
                <td><b>PAID FEE</b></td>
                <td><?php echo number_format($sbp['paidfee'], 2, '.', ',') ;?></td>
                </tr>
                <tr>
                <td><b>YEAR</b></td>
                <td><?php echo $sbp['year'];?></td>
                <input type="hidden" name="year" id="year" value="<?php echo $sbp['year'];?>" />
                </tr>
                <tr>
                <td><b>RECEIPT NUMBER</b></td>
                <td><?php echo $sbp['receiptno'];?></td>
                </tr>
                <tr>
                <td><b>ISSUE DATE</b></td>
                <td><?php echo $sbp['issuedate'];?></td>
                </tr>
              </tbody>
            </table>
          </div><!-- table-responsive -->
        </div><!-- col-md-6 -->
        
        <h5 class="subtitle mb5"><p></p></h5>
        <div class="panel panel-default col-md-3" >
          <div class="panel-heading" style="text-align:center;">
            <h4 class="panel-title"></h4>
            <p></p>
            <?php if($sbp['approvalstatus']=="2"): ?>
            <p><a href="<?php echo base_url();?>sbp/print_sbp_previews/<?php echo $sbp['bizzid'];?>/<?php echo $sbp['year'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <?php elseif($sbp['approvalstatus']=="0" || $sbp['approvalstatus']=="1" || $sbp['approvalstatus']=="4" || $sbp['approvalstatus']=="5"): ?>
            <p> <a href="<?php echo base_url();?>sbp/print_sbp_previews/<?php echo $sbp['bizzid'];?>/<?php echo $sbp['year'];?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <?php elseif($sbp['approvalstatus']=="3" || $sbp['approvalstatus']=="4"): ?>
            <p><h4>Approval for this business is declined hence not available for printing.</h4></p>
            <p></p>
          <?php endif; ?>
          </div>

        </div>
      </div>

    <?php else: ?>

    <div class="alert alert-danger col-md-6">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $sbp['restext'];?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>sbp/sbp_print">Go Back</a> <?php #echo $this->session->userdata['bill_stat']; ?>
    </div>
  <?php endif; ?>
  </div><!-- contentpanel -->


</div><!-- mainpanel -->

