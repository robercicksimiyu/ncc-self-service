<!--   <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> County E-Wallet <span>Create your county E-wallet</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>admin/indexx">Home</a></li>
          <li class="active">County E-wallet - Register</li>
        </ol>
      </div>
    </div>
  
<!-- <div class="pageheader" style="height:60px">
    <a href="<?php //echo base_url();?>sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php //echo base_url();?>sbp/sbp_print"><i class="fa fa-print"></i>Print Permit</a>
</div> -->

  <div class="contentpanel" >

      <div class="row">
          <form id="form1" class="form-horizontal" method="post">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-btns">
                  <a href="#" class="minimize">&minus;</a>
                </div>
                <h4 class="panel-title">E-Wallet Registration Form</h4>
                <p class="subt">Create Personal Account</p>
              </div>
              <div class="panel-body" style="padding-right:200px;">
              
                <div class="form-group">
                  <label class="col-sm-4 control-label">First Name:</label>
                  <div class="col-sm-8">
                    <input type="text" name="first_name" id="first_name" class="form-control" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Last Name:</label>
                  <div class="col-sm-8">
                    <input type="text" name="last_name" id="last_name" class="form-control" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Email:</label>
                  <div class="col-sm-8">
                    <input type="email" name="email" id="email" class="form-control" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Phone:</label>
                  <div class="col-sm-8">
                    <input type="text" name="express_phone" id="express_phone" class="form-control" placeholder="0715302571" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">E-Wallet Pin:</label>
                  <div class="col-sm-8">
                    <input type="password" name="express_pin" id="express_pin" class="form-control" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">National ID/Passport:</label>
                  <div class="col-sm-8">
                    <input type="text" name="national_id" id="national_id" class="form-control" required/>
                  </div>
                </div>
                <div class="err-box"></div>
              </div><!-- panel-body -->
              <div class="panel-footer" style="padding-left:200px;">
                
                <button type="submit" class="btn btn-primary" name="registersubmit" id="registersubmit">Submit</button>
                <button type="reset" class="btn btn-default">Reset</button>
              
              </div><!-- panel-footer -->
            </div><!-- panel-default -->
          </form>

          
            
      </div><!-- row -->
      
  </div><!-- contentpanel -->
</div>

<script type="text/javascript">
 // console.log("<?php echo base_url();?>components/js/jquery-1.10.2.min.js");
  var base_uri="<?php echo base_url();?>";
  $(document).ready(function() {
      $('#registersubmit').on('click',function(event){

        $.post(base_uri+'sbp/submit_register',
        {
          first_name:$('#first_name').val(),
          last_name:$('#last_name').val(),
          email:$('#email').val(),
          express_phone:$('#express_phone').val(),
          express_pin:$('#express_pin').val(),
          national_id:$('#national_id').val(),
          jp_business:'test@webtribe.co.ke'
        },
        function(data){
          if(data==0){
              msg='Registration was Successful, Please Check Your Email to complete Registration';
              content='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
              content+=''+msg+'</div>';
              $('.panel-title').html('Success');
              $('.subt').html('Registration Was Successful');
              $('.panel-body').html(content);
              $('.err-box').html(content);
              $('.panel-footer').hide();
          }else {

              msg=data;
              content='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
              content+=''+msg+'</div>';
              $('.panel-title').html('Failed');
              $('.subt').html('Fill all the fields with correct data');
              $('.err-box').html(content);

          }

          
        });
        event.preventDefault();
    });
      
   });

</script>