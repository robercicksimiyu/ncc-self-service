  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Single Business Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default col-md-8">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
    

                
                <?php 

                if($isIn == False){
                  echo 
                  '<tr>
                  <div class="alert alert-warning">
                    
                    <strong>SubCounty and Plot Number Mismatch</strong> Choose the correct SubCounty where the plot is located.
                    <br>
                    <a href="http://197.254.58.150/ncc/epaymentsLive/sbp/enter_acc_no" class="backLink" >Go Back</a>
                  </div>
                </tr>';
                }
                 ?>


                 <script type="text/javascript">
                 $(document).ready(function(){
                    $('.backLink').click(function(){
                      parent.history.back();
                      return false;
                    });
                  });
                 </script>
                  
        
                  <div class="table-responsive">
                  <?php echo form_open('sbp/complete_payment'); ?>
                    <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">PLEASE CONFIRM BEFORE COMPLETING THE TRANSACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $sbp['bizname'];?><?php #echo $sbp['transid'];?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS ID</b></td>
                        <td><?php echo $sbp['bid'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON NAME</b></td>
                        <td><?php echo $sbp['ContactPersonName'];?></td>
                      </tr>
                      <tr>
                        <td><b>CONTACT PERSON ID NUMBER</b></td>
                        <td><?php echo $sbp['IDNumber'];?></td>
                      </tr>

                      <tr>
                        <td><b>PLOT NUMBER</b></td>
                        <td><?php echo $sbp['PlotNumber'];?></td>
                      </tr>

                      <tr>
                        <td><b>TELEPHONE</b></td>
                        <td><?php echo $sbp['Telephone1'];?></td>
                      </tr>
                      <tr>
                        <td><b>BUILDING CATEGORY</b></td>
                        <td><?php if($sbp['BuildingCategory']==1){echo "STOREY";}elseif($sbp['BuildingCategory']==0){echo "NON-STOREY";} ?></td>
                      </tr>
                      <tr>
                        <td><b>BUILDING NAME</b></td>
                        <td><?php echo $sbp['Building'];?></td>
                      </tr>
                      <tr>
                        <td><b>SUB-COUNTY</b></td>
                        <td><?php echo $sbp['ZoneName'];?></td>
                      </tr>
                      <tr>
                        <td><b>WARD</b></td>
                        <td><?php echo $sbp['WardName'];?></td>
                      </tr>                      

                      <?php if(isset($sbp['sub_streams'])): foreach($sbp['sub_streams'] as $key=>$stream):?>
                        <tr>
                          <td><?=strtoupper($stream['Description'])?></td>
                          <td><?=number_format($stream['Amount'],2,'.',',')?></td>
                        </tr>                          
                      <?php endforeach;else:?>
                          <td>SBP Amount</td>
                          <td><?=number_format($sbp['annual_amount'],2,'.',',')?></td>
                    <?php endif;?>

                      
                      <tr>
                        <td><b>ACCRUED PENALTIES</b></td>
                        <td><?php echo number_format($sbp['penalty'], 2, '.', ',');?></td>
                      </tr>                      



                      <tr>
                        <td>

                        <input type="hidden" value="<?php echo $sbp['bizname'];?>" name="bizname" id="bizname" />
                        <input type="hidden" value="<?=$sbp['amount']?>" name="amount" id="amount" />
                        <input type="hidden" value="<?php echo $sbp['transid'];?>" name="transid" id="transid" />
                        <input type="hidden" value="<?php echo $sbp['bid'];?>" name="bid" id="bid" />
                        <input type="hidden" value="<?php echo $sbp['year'];?>" name="year" id="year" />
                        <input type="hidden" value="<?php echo $sbp['PlotNumber'];?>" name="plot" id="plot" />

                        <input type="hidden" value="<?php echo $sbp['IDNumber'];?>" name="idnumber" id="idnumber" />
                        <input type="hidden" value="<?php echo $sbp['BuildingCategory'];?>" name="buildingcategory" id="buildingcategory" />
                        <input type="hidden" value="<?php echo $sbp['ContactPersonName'];?>" name="contactpersonname" id="contactpersonname" />
                        <input type="hidden" value="<?php echo $sbp['ZoneName'];?>" name="zonename" id="zonename" />
                        <input type="hidden" value="<?php echo $sbp['WardName'];?>" name="wardname" id="wardname" />
                        <input type="hidden" value="<?php echo $sbp['ZoneCode'];?>" name="zonecode" id="zonecode" />
                        <input type="hidden" value="<?php echo $sbp['WardCode'];?>" name="wardcode" id="wardcode" />
                        <input type="hidden" value="<?php echo $sbp['Building'];?>" name="building" id="building" />
                        <input type="hidden" value="<?php echo $sbp['Floor'];?>" name="floor" id="floor" />
                        <input type="hidden" value="<?php echo $sbp['RoomStallNumber'];?>" name="roomstallnumber" id="roomstallnumber" />
                        
                        </td>
                      </tr>
                      <tr>
                        <td><strong>TOTAL</strong></td>
                        <td>
                        <?php 
                                          
                            echo 'KES. '.number_format( $sbp['amount'], 2, '.', ',');
                            
                          ?>
                      </td>
                      </tr>
                    </tbody>
                  </table>

                  </div><!-- table-responsive -->
        
              </div>
            </div><!-- panel-body -->
           
            <div class="panel-footer">
            <?php
              function setPermitMessage($data)
              {
                $message='';
                if(isset($data['AcceptedHealthReq']) && isset($data['AcceptedFireReq'])){
                    $message="You must comply with the Fire and Health certificates requirements to proceed";
                } else if(isset($data['AcceptedFireReq'])){
                    $message="You must comply with the Fire Certificate requirements to proceed";
                } else if(isset($data['AcceptedHealthReq'])) {
                    $message="You must comply with the Health Certificate requirements to proceed";
                }
                
                return $message;
              }

              $message=setPermitMessage($sbp);
            ?>
            
            <?php if ($isIn == True):?>
              <?php if($message==''):?>
                <div class="col-sm-6">
                    <input type="password" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>
                </div>
                <input class="btn btn-primary" type="submit" value="Make Payment">
                <input class="btn btn-primary" type="button" value="Back" class="backLink">
                </div>
              <?php else:?>
                <div class="alert alert-warning">
                  <p><?=$message?></p>
                </div>
              <?php endif;?>


            <?php endif; ?>
            
            <?php echo form_close();?>
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->