<div class="mainpanel">
    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <?php echo form_open('sbp/confirm_topup_details'); ?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Phone Number</label>
                    <input type="text" name="phoneno" id="phoneno" class="form-control" />
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Amount</label>
                    <input type="text" name="amount" id="amount" class="form-control" />
                  </div>
                </div><!-- col-sm-6 -->
              </div>
              
            </div><!-- panel-body -->
            <div class="panel-footer">
              <input type="submit" class="btn btn-primary" value="Submit Details">
            </div>
            <?php echo form_close();?>
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->