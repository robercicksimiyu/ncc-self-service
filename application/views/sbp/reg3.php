<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Single Business Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
    
     <!-- <div class="pageheader" style="height:60px">
    <a href="<?php #echo base_url();?>index.php/sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php #echo base_url();?>index.php/sbp/sbp_print"><i class="fa fa-print"></i>Print Permit</a>
    </div> -->
    <div class="contentpanel" >

      <?php 

      // $state= $datax['resultcode'];

      // if($state==0):

      ?>
      
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">BUSINESS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php #echo $datax['biz_name'];?></td>
                      </tr>
                      <tr>
                        <td><b>PHYSICAL ADDRESS</b></td>
                        <td><?php #echo $datax['physical_address'];?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS ID</b></td>
                        <td><?php #echo $datax['biz_id'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL NUMBER</b></td>
                        <td><?php #echo $datax['billno'];?></td>
                      </tr>
                      <tr>
                        <td><b>BILL STATUS</b></td>
                        <td><?php #if($datax['bill_stat']=="NEW"){echo "UNPAID";}else{echo "PAID";}  ?></td>
                      </tr>
                      <tr>
                        <td><b>ANNUAL AMOUNT</b></td>
                        <td><?php #echo number_format($datax['annualamount'], 2, '.', ',');?></td>
                      </tr>
                      <tr>
                        <td><b>ACCRUED PENALTIES</b></td>
                        <td><?php #echo number_format($datax['penalty'], 2, '.', ',');?></td>
                      </tr>
                      <tr>
                        <td><b>TOTAL PAYABLE</b></td>
                        <td><?php #if($datax['bill_stat']=="PAID"){echo "0.00";}else{echo number_format($datax['sbpfee'], 2, '.', ',');}?></td>
                      </tr>
                      <!-- <tr>
                        <td><b>TRANSACTION ID</b></td>
                        <td><?php #echo $datax['trans_id'];?></td>
                      </tr> -->
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
            <div class="panel-footer">
              
              <div class="row">
              <?php #if($datax['bill_stat']=="NEW"){
                echo form_open('sbp/confirm_payment');
                #echo'<div class="col-sm-6">';
                #echo '<input type="text" id="jp_pin" placeholder="Enter Your County Wallet Pin" name="jp_pin" class="form-control" required/>';
                #echo'</div>';
                echo '<input type="submit" id="bill_status"  value="Proceed to Complete Payment" class="btn btn-primary">';
                echo " ";
                echo anchor('sbp/enter_acc_no','Cancel',array('class'=>'btn btn-primary'));
                echo form_close();
              // }else{
              //   echo '<div class="alert alert-danger">';
              //    echo ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
              //    echo "The transaction cannot continue since the permit for the above business is PAID!"." "." "." "." ".anchor('sbp/enter_acc_no','Go Back');
              //   echo '</div>';
                  
              #}?>
            </div>
            
            
          </div>
        </div>
      
    </div><!-- contentpanel -->



	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Confirm that all the details displayed match your business</p></li>
		   <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
		    </ol>
        </div>
    
  </div><!-- mainpanel -->
