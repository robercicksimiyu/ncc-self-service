  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
    <?php $var = $step5data['rescode'];?>

      <?php if($step5data['rescode']==0): ?>
                    <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
              <?php echo form_open('sbp/step6'); ?>

                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM BUSINESS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $step5data['details']['BusinessName'];?></td>
                      </tr>
                     
                      <?php if(count($step5data['details']['LineItems'])):?>                        
                        <?php foreach($step5data['details']['LineItems'] as $item):;?>
                        <tr>
                          <td><b><?=$item['Description']?></b></td>
                          <td><?php echo number_format($item['Amount'], 2, '.', ',') ;?></td>
                        </tr>
                      <?php endforeach;?>
                       
                      <?php else:?>
                        <tr>
                          <td><b>ANNUAL AMOUNT</b></td>
                          <td><?php echo number_format($step5data['details']['AnnualSBPAmount'], 2, '.', ',') ;?></td>
                        </tr>
                      <?php endif;?>                                          
                      <tr>
                        <td><b>TOTAL PAYABLE</b></td>
                        <td>
                        <?php
                          if(count($step5data['details']['LineItems'])) {
                              $total=$step5data['details']['Amount'];
                          } else {
                             $total=$step5data['details']['AnnualSBPAmount'] + $regfeez;
                          }                   
                          
                           echo 'KES. '.number_format($total,2,'.',',');
                           ?></td>
                      </tr>
                      
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                  

                    <input type="hidden" name="biz_name"  value="<?php echo $step4data['biz_name'];?>" />
                    <input type="hidden" name="trans_id"  value="<?php echo $step5data['tranidd'];?>" />
                    <input type="hidden" name="doc_type"  value="<?php echo $step4data['doc_type'];?>" />
                    <input type="hidden" name="doc_no"  value="<?php echo $step4data['doc_no'];?>" />
                    <input type="hidden" name="pin_no"  value="<?php echo $step4data['pin_no'];?>" />
                    <input type="hidden" name="vat_no"  value="<?php echo $step4data['vat_no'];?>" />
                    <input type="hidden" name="box"  value="<?php echo $step4data['box'];?>" />
                    <input type="hidden" name="postal_code"  value="<?php echo $step4data['postal_code'];?>" />
                    <input type="hidden" name="town"  value="<?php echo $step4data['town'];?>" />

                    <input type="hidden" name="tel1"  value="<?php echo $step4data['tel1'];?>" />
                    <input type="hidden" name="tel2"  value="<?php echo $step4data['tel2'];?>" />
                    <input type="hidden" name="fax"  value="<?php echo $step4data['fax'];?>" />
                    <input type="hidden" name="email"  value="<?php echo $step4data['email'];?>" />
                    <input type="hidden" name="address"  value="<?php echo $step4data['address'];?>" />
                    <input type="hidden" name="plot_number"  value="<?php echo $step4data['plot_number'];?>" />
                    <input type="hidden" name="building"  value="<?php echo $step4data['building'];?>" />
                    <input type="hidden" name="floor"  value="<?php echo $step4data['floor'];?>" />
                    <input type="hidden" name="stall_room_no"  value="<?php echo $step4data['stall_room_no'];?>" />

                    <input type="hidden" name="full_names"  value="<?php echo $step4data['full_names'];?>" />
                    <input type="hidden" name="owner_box"  value="<?php echo $step4data['owner_box'];?>" />
                    <input type="hidden" name="owner_postal_code"  value="<?php echo $step4data['owner_postal_code'];?>" />
                    <input type="hidden" name="owner_telephone"  value="<?php echo $step4data['owner_telephone'];?>" />
                    <input type="hidden" name="owner_telephone2"  value="<?php echo $step4data['owner_telephone2'];?>" />
                    <input type="hidden" name="owner_fax"  value="<?php echo $step4data['owner_fax'];?>" />

                    <input type="hidden" name="activity_dec"  value="<?php echo $step4data['activity_dec'];?>" />
                    <input type="hidden" name="area"  value="<?php echo $step4data['area'];?>" />
                    <input type="hidden" name="other_details"  value="<?php echo $step4data['other_details'];?>" />
                    <input type="hidden" name="activity_code"  value="<?php echo $step4data['activity_code'];?>" />
                    <input type="hidden" name="sbp_fee"  value="<?php echo $total;?>" />
                    
                    
                    <input type="hidden" name="emp_no"  value="<?php echo $step4data['emp_no'];?>" />
                    <input type="hidden" name="size"  value="<?php echo $step4data['size'];?>" />
                    <input type="hidden" name="zone"  value="<?php echo $step4data['zone'];?>" />
                    <input type="hidden" name="ward"  value="<?php echo $step4data['ward'];?>" />
<!--                     <input type="hidden" name="cust_phone"  value="254712633277" />
 -->                
                  
                    <div class="row">
                    <div class="form-group">
                     <div class="col-sm-4">
                          <label class="control-label">Enter Wallet PIN</label>
                          <input type="password" name="pin" class="form-control" id="pin" placeholder="" required />
                      </div>
                      <div class="col-sm-4">
                        <label class="control-label">&nbsp;</label><br>
                        <input type="submit" id="next5"  value="Next" class="btn btn-primary"/>
                      </div>
                     </div>
                    </div>
                  
                  </div>
                <?php echo form_close();?>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->
       <?php else: ?>
            <div class="alert alert-danger col-md-8">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                      <?php echo $step5data['restext']; ?>
                    </div>
      <?php endif ;?>



  </div><!-- mainpanel -->

