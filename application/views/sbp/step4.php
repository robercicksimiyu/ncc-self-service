  <style type="text/css">
    option:disabled {
        display: none;
    }
  </style>
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-10" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                 <?php echo form_open('sbp/step5'); ?>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Activity Description</label>
                          <input type="text" name="activity_dec" id="activity_dec" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Area(m2)</label>
                          <input type="text" name="area" id="area" class="form-control"  placeholder="" />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Other Details</label>
                          <input type="text" name="other_details" id="other_details" class="form-control" placeholder="" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">No of Employees</label>
                          <input type="text" name="emp_no" id="emp_no" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Sub County</label>
                          <select class="form-control" name="zones" id="zones" required >
                            
                            
                                <?php foreach($counties as $county): ?>
                                        <option value="<?php echo $county->ID; ?>"><?php echo $county->Name; ?></option>
                                <?php endforeach;?>
                       
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Ward</label>
                          <select class="form-control" name="ward" id="ward" required >
                            
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6">
                          <label class="control-label">Major Categories</label>
                          <select class="form-control" name="business_classes" id="business_classes" required>                            
                            <?php
                              foreach($business_classes as $biz_class) {
                                ?>
                                 <option value="<?php echo $biz_class->ID; ?>"><?php echo $biz_class->Name; ?></option>
                               <?php    
                              }
                            ?>
                          </select>
                          
                        </div>
                        <div class="col-sm-6">
                          <label class="control-label">Sub Categories</label>
                          <select class="form-control" name="sub_classes" id="sub_classes" required >
                            
                          </select>                          
                        </div>
                        
                       <!--  <div class="col-sm-4">
                          <label class="control-label">SBP Fee</label>
                          <input type="text" name="sbp_fee" class="form-control" id="sbp" readonly="readonly" required />
                        </div> -->
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Relative Size</label>
                          <select class="form-control" name="size" id="size" required >
                            <option value="">Choose One</option>
                            <?php
                              foreach($step4['relsize'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                               <?php    
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-sm-4 col-sm-offset-2">
                          <div id="exceptions" style="margin-top: 10px;">
                            
                          </div>
                        </div>

                          <input type="hidden" name="biz_name"  value="<?php echo $step3data['biz_name'];?>" />
                          <input type="hidden" name="doc_type"  value="<?php echo $step3data['doc_type'];?>" />
                          <input type="hidden" name="doc_no"  value="<?php echo $step3data['doc_no'];?>" />
                          <input type="hidden" name="pin_no"  value="<?php echo $step3data['pin_no'];?>" />
                          <input type="hidden" name="vat_no"  value="<?php echo $step3data['vat_no'];?>" />
                          <input type="hidden" name="box"  value="<?php echo $step3data['box'];?>" />
                          <input type="hidden" name="postal_code"  value="<?php echo $step3data['postal_code'];?>" />
                          <input type="hidden" name="town"  value="<?php echo $step3data['town'];?>" />

                          <input type="hidden" name="tel1"  value="<?php echo $step3data['tel1'];?>" />
                          <input type="hidden" name="tel2"  value="<?php echo $step3data['tel2'];?>" />
                          <input type="hidden" name="fax"  value="<?php echo $step3data['fax'];?>" />
                          <input type="hidden" name="email"  value="<?php echo $step3data['email'];?>" />
                          <input type="hidden" name="address"  value="<?php echo $step3data['address'];?>" />
                          <input type="hidden" name="plot_number"  value="<?php echo $step3data['plot_number'];?>" />
                          <input type="hidden" name="lr_number"  value="<?php echo $step3data['lr_number'];?>" />
                          <input type="hidden" name="building"  value="<?php echo $step3data['building'];?>" />
                          <input type="hidden" name="floor"  value="<?php echo $step3data['floor'];?>" />
                          <input type="hidden" name="stall_room_no"  value="<?php echo $step3data['stall_room_no'];?>" />

                          <input type="hidden" name="full_names"  value="<?php echo $step3data['full_names'];?>" />
                          <input type="hidden" name="owner_box"  value="<?php echo $step3data['owner_box'];?>" />
                          <input type="hidden" name="owner_postal_code"  value="<?php echo $step3data['owner_postal_code'];?>" />
                          <input type="hidden" name="owner_telephone"  value="<?php echo $step3data['owner_telephone'];?>" />
                          <input type="hidden" name="owner_telephone2"  value="<?php echo $step3data['owner_telephone2'];?>" />
                          <input type="hidden" name="owner_fax"  value="<?php echo $step3data['owner_fax'];?>" />
                      </div>

                      
                      
                      <div class="panel-footer">
                        <div class="row">
                          <input type="submit" id="next4"  value="Next" class="btn btn-primary"/>
                        </div>
                      </div>
                  <?php echo form_close();?>
              </div>
              
            
        </div>
      
    </div><!-- contentpanel -->
  </div><!-- mainpanel -->
<script type="text/javascript">
$(document).ready(function(){
  $("#activity_code option[value='215']").remove();
   $("#activity_code option[value='205']").remove();
   $("#activity_code option[value='210']").remove();
   $("#activity_code option[value='220']").remove();
  $('#activity_amount').hide();
      $('#activity_code').on('change',function(){
          code=$(this).val();

          $("#activity_amount option[value='"+code+"']").attr('selected', 'selected');
          amount=$("#activity_amount option:selected").text();
          $('#sbp').val(amount);
      });

      // $('#ward').on('change',function(){
      //     if($(this).val()==15){
      //         console.log($(this).val());
      //        $('#activity_code').prop('selectedIndex',0);
      //        $('#sbp').val('');
             
      //     }else{
      //       if($("#activity_code option[value='215']").length == 0){
      //         $("#activity_code").append('<option value="215">Semi permanent informal sector trader: Up to 2 persons in verandah or  temporary building</option>');
      //         $("#activity_code").append('<option value="205">1 hawker without motor vehicle</option>');
      //         $("#activity_code").append('<option value="210">Small informal sector trader / service provider e.g.   shoe shiner, shoe repairer, street vendor (newspapers, soda, sweets, cigarettes etc.)</option>');
      //         $("#activity_code").append('<option value="220">Other informal sector</option>');
      //       }
      //     }
      // });
});
</script>
<script type="text/javascript">
      jQuery(document).ready(function(){
        $.ajax({
          url: '<?php echo base_url(); ?>sbp/getWard2',
          type: 'POST',
          dataType: 'json',
          cache: false,
          data: $("#zones").serialize(),
          beforeSend: function() {
              console.log("Trying....");
              dat = $("#zones").serialize();
              console.log(dat);
        },
        success: function(data) {
            $("#ward").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#ward').append($('<option>', {
                    "value": id
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });

           $.ajax({
            url: '<?php echo base_url(); ?>sbp/getSubClasses',
            type: 'POST',
            dataType: 'json',
            cache: false,
            data: $("#business_classes").serialize(),
            beforeSend: function() {
                console.log("Trying....");
                dat = $("#business_classes").serialize();
                console.log(dat);
            },
            success: function(data) {
                $("#sub_classes").empty();
                var optgroup = data;
                for (var i = 0; i < optgroup.length; i++) {
                    var id = optgroup[i].ID;
                    var name = optgroup[i].Name;
                    $('#sub_classes').append($('<option>', {
                        "value": id
                    }).text(name));
                }
                classs_change();
            },
            error: function(err) {
                console.log(err)
            }
        });



      });
      </script>
  <script type="text/javascript">
     $("#zones").change(function(event) {
    $.ajax({
        url: '<?php echo base_url(); ?>sbp/getWard2',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zones").serialize(),
        beforeSend: function() {
            console.log("Trying....");
             dat = $("#zones").serialize();            
            console.log(dat);
        },
        success: function(data) {
           
            $("#ward").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];

                $('#ward').append($('<option>', {
                    "value": id
                }).text(name));
                console.log(id + " " + name);
            }
        },
        error: function(err) {
            console.log(err)
        }
    });
});

  function classs_change(){
      var id = $("#sub_classes").val();
      $.ajax({
        url: '<?php echo base_url(); ?>sbp/getSubClassId',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {'sub_classes':id},           
      success: function(data) {
          $("#exceptions").empty();               
          $("#exceptions").append("<p><strong>Billed Items</strong></p>");
          $.each(data, function(i, item) {             
              
                    $("#exceptions").append("<label>"+item.Description+" - "+item.Amount+" </label></br>");
                       
           });
          
          $('#exceptions').html(text);
          /*var optgroup = data;
          for (var i = 0; i < optgroup.length; i++) {
              var id = optgroup[i].ID;
              var name = optgroup[i].Name;
              $('#ward').append($('<option>', {
                  "value": id
              }).text(name));
          }*/
      },
      error: function(err) {
          console.log(err)
      }
    });
  }

 $("#business_classes").change(function(event) {
    $.ajax({
        url: '<?php echo base_url(); ?>sbp/getSubClasses',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#business_classes").serialize(),
        beforeSend: function() {           
             dat = $("#business_classes").serialize();          
           
        },
        success: function(data) {
           
            $("#sub_classes").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
               // var code = [120,195,215,210,220,205,200,546];

                $('#sub_classes').append($('<option>', {
                    "value": id
                }).text(name));
               
                classs_change();
            }
        },
        error: function(err) {
            console.log(err)
        }
    });
});

 $("#sub_classes").change(function(event){          
           var id = $("#sub_classes").val();           
           $.ajax({
             url: '<?php echo base_url(); ?>sbp/getSubClassId',
             type: 'POST',
             dataType: 'json',
             cache: false,
             data: {'sub_classes':id},           
           success: function(data) {
               $("#exceptions").empty();               
               $("#exceptions").append("<p><strong>Select items that you wish to be excluded</strong></p>");
               $.each(data, function(i, item) { 
                  console.log(item);
                  
                    $("#exceptions").append("<label>"+item.Description+" - "+item.Amount+" </label></br>");
                             
                  
                });
               
               $('#exceptions').html(text);
               /*var optgroup = data;
               for (var i = 0; i < optgroup.length; i++) {
                   var id = optgroup[i].ID;
                   var name = optgroup[i].Name;
                   $('#ward').append($('<option>', {
                       "value": id
                   }).text(name));
               }*/
           },
           error: function(err) {
               console.log(err)
           }
         });
 });




     
     /*$("#ward").change(function(event){
         var code = [120,195,215,210,220,205,200,546];
         var id = $("#ward").val();
          if(id==197){
            $.each(code, function( index, value ) {
               $("#activity_code option[value="+value+"]").attr('disabled','disabled')
            });
          }
          else{
            $.each(code, function( index, value ) {
               $("#activity_code option[value="+value+"]").removeAttr('disabled');
            });
          }
        
        
     });*/
     
        </script>