   <div class="pageheader">
      <h2><i class="fa fa-inbox"></i> HOUSE RENT <span>Confirm Amount</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>index.php/rent/house_rent">House Rent</a></li>
          <li class="active">Confirm Amount To Pay </li>
        </ol>
      </div>
    </div>

    

    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize"></a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                <?php //var_dump($confirm); ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM THE INFORMATION BELOW</th>
                      </tr>
                    </thead>
                    <?php echo form_open('marketstall/completerentpayment'); ?>
                    <tbody>
                      <tr>
                        <td><b>OCCUPANT</b></td>
                        <td><?php echo $confirm['custname'] ?></td>
                        <input type="hidden" value="<?php echo $confirm['custname'] ?>" name="custname">
                      </tr>
                      <tr>
                        <td><b>PHYSICAL ADDRESS</b></td>
                        <td><?php echo $confirm['physicaladdress'] ?></td>
                        <input type="hidden" value="<?php echo $confirm['physicaladdress']; ?>" name="location">
                        <input type="hidden" value="<?php echo $confirm['transactionId']; ?>" name="transid">
                        <input type="hidden" value="<?php echo $confirm['pin']; ?>" name="pin">
                      </tr>
                      <tr>
                        <td><b>MARKET/STALL NUMBER</b></td>
                        <td><?php echo $confirm['houseno'] ?></td>
                        <input type="hidden" value="<?php echo $confirm['houseno'] ?>" name="houseno">
                      </tr>
                      <tr>
                        <td><b>PHONE NUMBER</b></td>
                        <td><?php echo $confirm['phone']; ?></td>
                        <input type="hidden" value="<?php echo $confirm['phone'] ?>" name="phone">
                      </tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo number_format($confirm['amount'],2); ?></td>
                        <input type="hidden" value="<?php echo $confirm['amount'] ?>" name="amount">
                      </tr>
                      <?php //var_dump($confirm['4']) ?>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
              <input type="submit" value="Complete Payment" class="btn btn-primary">
              <?php echo form_close(); ?>
              <?php echo anchor('rent/house_rent','Cancel',array('class'=>"btn btn-default")); ?>
            </div>
          </div>
        </div>
      
    </div><!-- contentpanel -->

  