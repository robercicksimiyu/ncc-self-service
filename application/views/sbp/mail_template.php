<div class="mainpanel">
    <div class="headerbar">
      
      
    </div><!-- headerbar -->
    
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i>Single Business Permit - Self Service</h2>
      <ul class="nav nav-pills nav-stacked nav-bracket" style="width: 60%;text-align: right;">
        <a href="<?php echo base_url();?>index.php/sbp/topup"><span></span></a>&nbsp;&nbsp;
        <a href="<?php echo base_url();?>index.php/sbp/view_balance"><span>Check Balance</span></a>&nbsp;&nbsp;
        <a href="#"><span></span></a>
      </ul>
    </div>

    <div class="contentpanel" >

<div class="row">
        <div class="col-md-9">
          <h5 class="subtitle mb5">Email Notification</h5>
          <div class="table-responsive">
          <table class="table table-bordered mb30">
            <thead>
              <tr>
                <td colspan="4"><img src="<?php echo base_url();?>components/back/images/ncc_logo.png" style="width: 200px;">Nairobi County Council</td>
              </tr>
            </thead>
            <tbody>
              <tr style="background-color: #00431E;color: #FFE100;">
                <td colspan="4">Auto Email From Nairobi County Council</td>
              </tr>
              <tr>
                <td colspan="2"><h4><b>Business Name:</b>&nbsp;&nbsp;<?php echo $this->session->userdata('biz_name');?></h4></td>
              </tr>
              <tr>
                <td colspan="2"></td>
              </tr>
              <tr style="background-color: #EFEFEF;color: #00431E;">
                <th colspan="">Item Details:</th>
                <th colspan="2">Transaction Details:</th>
              </tr>
              <tr>
                <td><b>Item Name:</b>&nbsp;Single Business Permit</td>
                <td><b>Order ID:</b>&nbsp;</td>
              </tr>
              <tr>
                <td><b>Business ID:</b>&nbsp;<?php echo $this->session->userdata('biz_id');?></td>
                <td><b>Bill Status:</b>&nbsp;<?php echo $this->session->userdata('bill_stat');?></td>
              </tr>
              <tr>
                <td><b>SBP FEE:</b>&nbsp;<?php echo $this->session->userdata('sbpfee');?>/=</td>
                <td><b>Date:</b>&nbsp;<?php echo date('l jS \of F Y h:i:s A'); ?></td>
              </tr>
              <tr>
                <td><b>Payment Method:</b>&nbsp;JamboPay</td>
                <td></td>
              </tr>
              

            </tbody>
          </table>
          </div><!-- table-responsive -->
        </div><!-- col-md-6 -->
        
      </div>
      </div>