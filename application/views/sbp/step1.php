<!-- <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
      <div >
         <div class="panel panel-default col-md-10" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                 <?php echo form_open('sbp/step2'); ?>
                  <div class="form-group">
                        <div class="col-sm-8 <?=(form_error('biz_name'))?'has-error':''?>">
                          <label class="control-label">Business Name</label>
                          <input type="text" name="biz_name" id="biz_name" class="form-control" placeholder="" value="<?=set_value('tel1')?>" required />
                          <small><?php echo form_error('biz_name'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('doc_type'))?'has-error':''?>">
                          <label class="control-label">ID Document Type</label>
                          <select class="form-control" name="doc_type" id="doc_type" required>
                           <?php
                              foreach($step1['idtype'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->ID; ?>"><?php echo $value->Name; ?></option>
                               <?php    
                              }
                            ?>
                          </select>
                          <small><?php echo form_error('doc_type'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('doc_no'))?'has-error':''?>">
                          <label class="control-label">ID Document Number</label>
                          <input type="text" name="doc_no" id="doc_no" class="form-control" placeholder="" value="<?=set_value('doc_no')?>" required />
                          <small><?php echo form_error('doc_no'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('pin_no'))?'has-error':''?>">
                          <label class="control-label">KRA Pin Number</label>
                          <input type="text" name="pin_no" id="pin_no" class="form-control" placeholder="" value="<?=set_value('pin_no')?>" required />
                          <small><?php echo form_error('pin_no'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('vat_no'))?'has-error':''?>">
                          <label class="control-label">VAT Number</label>
                          <input type="text" name="vat_no" id="vat_no" value="<?=set_value('vat_no')?>" class="form-control" placeholder=""/>
                          <small><?php echo form_error('vat_no'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('box'))?'has-error':''?>">
                          <label class="control-label">P.O. Box</label>
                          <input type="text" value="<?=set_value('box')?>" name="box" id="box" class="form-control" placeholder="" required />
                          <small><?php echo form_error('box'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('postal_code'))?'has-error':''?>">
                          <label class="control-label">Postal Code</label>
                          <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="" value="<?=set_value('postal_code')?>" required />
                          <small><?php echo form_error('postal_code'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        
                        <div class="col-sm-4 <?=(form_error('town'))?'has-error':''?>">
                          <label class="control-label">Town</label>
                          <input type="text" value="<?=set_value('town')?>" name="town" id="town" class="form-control" placeholder="" required />
                          <small><?php echo form_error('town'); ?></small>
                        </div>
                      </div>
                      <div class="panel-footer">
              
              <div class="row">
              <input type="submit" id="next1"  value="Next" class="btn btn-primary"/>
            </div>
            
            
          </div>
                  <?php echo form_close();?>
              </div>
              
            
        </div>
      
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->
    
  </div><!-- mainpanel -->



