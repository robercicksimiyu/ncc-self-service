  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">
      <div >
         <div class="panel panel-default col-md-10" style="margin-right:20px">
           <?php #echo $step1data['box'];?>
           
            <div class="panel-body">
              <div class="row">
                <?php if($this->session->flashdata('msg')):?>
                <div class="alert alert-warning" role="alert">
                    <?=$this->session->flashdata('msg')?>
                </div>
                <?php endif;?>
                 <?php echo form_open('sbp/step3',array("name"=>"theform")); ?>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('tel1'))?'has-error':''?>">
                          <label class="control-label">Telephone Number</label>
                          <input type="text" value="<?=set_value('tel1')?>" name="tel1" id="tel1" class="form-control" placeholder="" required />
                          <small><?php echo form_error('tel1'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('tel2'))?'has-error':''?>">
                          <label class="control-label">Other Telephone Number</label>
                          <input type="text" value="<?=set_value('tel2')?>" name="tel2" id="tel2" class="form-control" placeholder="" onKeyup="checkform()" />
                          <small><?php echo form_error('tel2'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('fax'))?'has-error':''?>">
                          <label class="control-label">Fax</label>
                          <input type="text" name="fax" value="<?=set_value('fax')?>" id="fax" class="form-control" placeholder=""  />
                          <small><?php echo form_error('fax'); ?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4 <?=(form_error('email'))?'has-error':''?>">
                          <label class="control-label">Email</label>
                          <input type="text" value="<?=set_value('email')?>" name="email" id="email" class="form-control" placeholder="" />
                          <small><?php echo form_error('email'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('address'))?'has-error':''?>">
                          <label class="control-label">Physical Address</label>
                          <input type="text" name="address" value="<?=set_value('address')?>" id="address" class="form-control" placeholder="" required />
                          <small><?php echo form_error('address'); ?></small>
                        </div>
                        <div class="col-sm-4 <?=(form_error('plot_number'))?'has-error':''?>">
                          <label class="control-label">Plot Number</label>
                          <input type="text" name="plot_number" value="<?=set_value('plot_number')?>" id="plot_number" class="form-control" placeholder=""/>
                          <small id="ploterror"><?php echo form_error('plot_number'); ?></small>
                        </div>
                      </div>
                      <div class="form-group <?=(form_error('building'))?'has-error':''?>">
                        <div class="col-sm-4">
                          <label class="control-label">Building Name</label>
                          <input type="text" value="<?=set_value('building')?>" name="building" id="building" class="form-control" placeholder="" required />
                          <small><?php echo form_error('building'); ?></small>
                        </div>          
                        
                                      
                        <div class="col-sm-4">
                          <label class="control-label">Building Type</label>
                          <select id="storey" class="form-control"  value="<?=set_value('storey')?>" name="storey">
                            <option value='1'>Storey</option>
                            <option value='0'>Non-Storey</option>
                         </select> 
                          
                          <div id="floor_no" <?=(form_error('floor'))?'has-error':''?>>
                            <label class="control-label">Floor</label>
                            <input type="text" name="floor" value="<?=set_value('floor')?>" id="floor" class="form-control" placeholder=""/>
                            <small><?php echo form_error('floor'); ?></small>
                          </div>
                        </div>
                        
                        
                        <div class="col-sm-4 <?=(form_error('stall_room_no'))?'has-error':''?>">
                          <label class="control-label">Stall/Room Number</label>
                          <input type="text" name="stall_room_no" value="<?=set_value('stall_room_no')?>" id="stall_room_no" class="form-control" placeholder="" required />
                          <small><?php echo form_error('stall_room_no'); ?></small>
                        </div>
                          <input type="hidden" name="biz_name" value="<?php echo $step1data['biz_name'];?>" />
                          <input type="hidden" name="doc_type"  value="<?php echo $step1data['doc_type'];?>" />
                          <input type="hidden" name="doc_no"  value="<?php echo $step1data['doc_no'];?>" />
                          <input type="hidden" name="pin_no"  value="<?php echo $step1data['pin_no'];?>" />
                          <input type="hidden" name="vat_no"  value="<?php echo $step1data['vat_no'];?>" />
                          <input type="hidden" name="box"  value="<?php echo $step1data['box'];?>" />
                          <input type="hidden" name="postal_code"  value="<?php echo $step1data['postal_code'];?>" />
                          <input type="hidden" name="town"  value="<?php echo $step1data['town'];?>" />

                      </div>
                      
                      <div class="panel-footer">
                        <div class="row">
                          <input type="submit" id="next2" onclick="myFunction()" value="Next" class="btn btn-primary"/>
                        </div>
                      </div>
                  <?php echo form_close();?>
              </div>
              
            
        </div>
      
    </div><!-- contentpanel -->



    <!-- <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
      <ol>
      <li>
          <p>Confirm that all the details displayed match your business</p></li>
       <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
        </div>
        </div> -->
    
  </div><!-- mainpanel -->

  <script>
  function myFunction() {
    var x, text;

    // Get the value of the input field with id="numb"
    x = document.getElementById("plot_number").value;

    // If x is Not a Number or less than one or greater than 10
    if (!isNaN(x) || x < 1) {
      text = "Plot Number Cannot be 0";
    }else {
        text = "";
    }
    document.getElementById("ploterror").innerHTML = text;
  }
  </script>

  <script type="text/javascript">
    $('document').ready(function(){            
          
      $('#storey').on('change',function(){
        if($(this).val()==='0'){          
          $('#floor_no').hide();
        }else{
          $('#floor_no').show();
        }
      });
    });
  </script>

 


