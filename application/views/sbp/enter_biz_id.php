 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Unified Business Permit Renewal <span>Renew your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Unified Business Permit</a></li>
          <li class="active">Check Status</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Enter Business ID </h4>
          <p>Cross check to make sure you have filled in the correct Business ID</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('sbp/display_business_details',array('class' =>"form-block ",'name'=>"frm",'id'=>"frm")) ?>
            <div class="row mb10">
              <?php 
              $curyear = date("Y");
              $lastyear = (int)($curyear) - 1;
              $lastyearbutone = (int)($lastyear) - 1;
              ?>
                      <div class="col-sm-8">
                      <label class="col-sm-4 control-label">Business ID:</label>
                        <input type="text" class="form-control" id="biz_id" name="biz_id" placeholder="Enter Business ID" required />
                      </div>
                      <div class="col-sm-8">
                      <label class="col-sm-6 control-label">Renew permit for the Year</label>
                        <select class="form-control input-sm mb15" name="year" id="year" data-placeholder="Select Year...">
                        <option value="2017">2017</option>
                        <option value="<?php echo $curyear ;?>"><?php echo $curyear ;?></option>
                        <option value="<?php echo $lastyear ;?>"><?php echo $lastyear ;?></option>
                        <option value="<?php echo $lastyearbutone ;?>"><?php echo $lastyearbutone ;?></option>
                        </select>
                      </div>
                      
                      
            </div>
            <input type="submit" class="btn btn-primary" value="Check Status" >
            <button type="reset" class="btn btn-default">Reset</button>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Enter your Business Identification Number</p></li>
		   <li>
          <p>Check Status</p></li>
		    </ol>
        </div>
       
      </div><!-- panel -->
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

<script type="text/javascript">
  $('document').ready(function(){
      $('#year').on('change',function(){
        var year=$(this).val();
        
        if(year < parseInt(2017)) {
          $('#kitchen').hide();
        } else {
          $('#kitchen').show();
        }
      });
  });
</script>
