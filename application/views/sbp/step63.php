  <div class="pageheader">
      <h2><i class="fa fa-home"></i> Register Business <span>Register New Business Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel" >
    <?php //var_dump($this->session->userdata('jpwnumber')); ?>

    <?php $rescode = $alldata['rescode'];$restext = $alldata['restext'];
    if($rescode=='0'):
     ?>
      <div >
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">BUSINESS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>BUSINESS ID</b></td>
                        <td><?php echo $alldata['biz_id'];?></td>
                      </tr>
                      <tr>
                        <td><b>BUSINESS NAME</b></td>
                        <td><?php echo $alldata['biz_name'];?></td>
                      </tr>
                      <tr>
                        <td><b>PHYSICAL ADDRESS</b></td>
                        <td><?php echo $alldata['physical_address'];?></td>
                      </tr>
                      <tr>
                        <td><b>RECEIPT NUMBER</b></td>
                        <td><?php echo $alldata['receiptno'];?></td>
                      </tr>
                      <tr>
                        <td><b>SBP FEE</b></td>
                        <td><?php echo $alldata['sbpfee'];?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->

                  <div class="panel-footer">
                    <div class="row">
                    
                    </div>
                  </div>
              </div>
            
        </div>
      
    </div><!-- contentpanel -->



    <div class="panel panel-default col-md-3" >
      <div class="panel-heading" style="text-align:center;">
        <div class="panel-btns">

        </div>
        <h4 class="panel-title">Print Permit</h4>
            <p></p>
            <p> <a href="<?php echo base_url(); ?>sbp/print_sbp_previews/<?php echo $alldata['biz_id'];?>/<?=date('Y')?>"><input type="submit" value="Print Permit" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
            <p> <a href="<?php echo base_url(); ?>sbp/printnewsbpreceipt/<?php echo $alldata['biz_id'];?>"><input type="submit" value="Print Receipt" class="btn btn-primary btn-lg" style="line-height:normal"> </a> </p>
            <p></p>
      </div>
    </div>
   </div>
 <?php else: ?>
              <div class="alert alert-danger col-md-6">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo $rescode ." | ". $restext; ?>
              </div>
<?php endif;?>
  </div><!-- mainpanel 1346861-->


