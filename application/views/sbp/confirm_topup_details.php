<div class="mainpanel">
    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">
                  
        
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">CONFIRM THE INFORMATION BELOW</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>PHONE NO</b></td>
                        <td><?php echo $confirm['1'] ?></td>
                      </tr>
                      <tr>
                        <td><b>AMOUNT</b></td>
                        <td><?php echo $confirm['2'] ?></td>
                      </tr>
                      <tr>
                        <td><b>TRANSACTION ID</b></td>
                        <td><?php echo $confirm['3'] ?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
            </div><!-- panel-body -->
            <div class="panel-footer">
              <?php echo anchor('en/complete_topup','Confirm Transaction',array('class'=>"btn btn-primary")); ?>
              <?php echo anchor('en/cancel_topup','Cancel Transaction',array('class'=>"btn btn-primary")); ?>
            </div>
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->