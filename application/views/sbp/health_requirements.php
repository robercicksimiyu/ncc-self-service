 <!--  <div class="mainpanel"> -->
  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Unified Business Permit<?php # echo date("Y"); ?> <span>Terms & Conditions</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Unified Business Permit</a></li>
          <li class="active">Fire Certificate Requirements</li>
        </ol>
      </div>
    </div>
	

    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-12" style="margin-right:20px">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Please read the Fire requirements below</h4>
          
        
        </div>
        <div class="panel-body">
          <h3>GENERAL REQUIREMENTS FOR PREMISES <br></h3> 
           <h4>FOOD HYGIENE LICENSE AND HEALTH CERTIFICATE</h4>
           <ol>
            <li>
               <strong>Physical facility requirements</strong>
              <ul>
                  <li>Floors</li>
                  - Are to be durable, smooth, impervious and easy to clean with all angles between floor and walls rounded off to a height of not less than 75mm from the floor.
                  - The material of the floor surface must be suited to the work or process carried out in the premises and the floor drains are recommended in all wet areas
                  - Floor space should be clear of appliances, fittings and stored goods shall be not less than 3m2 per worker in that area or 9.5m2 whichever is the greater.

                  <li>Walls</li>
                  - Internal surfaces to be smooth, dust proof, non- absorbent and easily cleaned without damage to the surface, being painted or treated as the Environmental Health Officer may approve. Minimum height of walls to be 2.40 metres.
                  - Cooking areas also need to be heat resistant and stainless steel sheeting between the cooking equipment and ventilation canopy is recommended.
                  - Wash-up areas- walls surrounding sinks, wash- hand basins, dishwashers and glass washers also need extra protection and providing high bench up-stands or impervious sheeting is recommended.

                  <li>Ceilings</li>
                  - Ceilings or undersides of roof and supports should be smooth, dust proof, easily cleaned and light in colour.

                  <li>Kitchen Space</li>
                  - Should be sufficient for persons working and for easy access for cleaning. 

                    <li>Floors, Walls, Ceilings must be smooth, durable, easily cleanable and non- absorbent.</li>
                    <li>The walls of the preparation and server area shall be lined with glazed tiles or other suitable impervious materials of not less than 1.5 m, to facilitate cleaning.</li>

                  <li>No manhole or overhead sanitary wastepipe shall be sited in the area where food is prepared or cooked. No inspection chamber or grease trap shall be sited within the areas where food is prepared, cooked, stored or served; or other areas where they are likely to give rise to nuisance, health or hygiene hazards during maintenance.</li>
                  <li>At least one sink with a draining board shall be provided in the kitchen or food preparation area. For a large kitchen, additional sinks may be required for the washing of soiled crockery. The numbers to be provided shall be according to the operator’s needs. </li>
                  <li>Sink, a kitchen sink shall be provided with adequate running hot and cold water at all       times</li>

                  <li>The kitchen should not be directly facing the toilets</li>
                  <li>All attachments to walls and ceilings such as lights, vents, fans e.t.c. must be easily cleanable.</li>
                  <li>Outer openings must be protected from the entrance of insects and rodents with closed, tight-fitting windows and tight, self-closing doors. May use screening of 16 meshes to 1inch, air curtains or other effective means. This section does not apply if insects/pests are absent due to weather e.t.c.</li>
                  <li>A private home may not be used as a food establishment.</li>
                  <li>Sleeping rooms must be separated from areas used for food operations by complete partitioning and solid, self-closing doors.</li>
                  <li>The premises must be kept free of unnecessary items, broken equipment and litter.</li>
                  <li>The physical facilities must be kept in a good state of repair.</li>
              </ul>
            </li>
            <li>
              <strong>Equipments, Utensils, Single- Use & Single- Service items.</strong>
              <ul>
                <li>Food contact surfaces of utensils must be safe, durable, non-absorbent, smooth,    easily cleanable and resistant to pitting or chipping e.t.c. </li>
                <li>Non-food contact surfaces exposed to splash, spill or soiling that needs frequent cleaning must be smooth, easily cleanable, non-absorbent, corrosion resistant e.t.c.</li>
                 <li>Equipment and utensils must be durable under normal use.</li>
                <li>Equipment must be kept in a state of good repair. Door seals, hinges, fasteners, kick plates must be kept intact, tight and adjusted properly.</li>
                 <li>A food dispensing utensil shall be available for each item at a buffet, salad bar e.t.c.</li>
                <li>Food dispensing utensil may only be stored in one of the following ways:</li>
                - In the food with the handle above the top of the food.
                - On a clean area if the utensil and the area have been cleaning and sanitized.
                - In running water (as an ice cream dipper well).
                - In a clean protected area (as for an ice scoop) if only used with food that is not potentially hazardous.
                <li>Utensils must be kept in a good state of repair. </li>
                <li>Single use items may not be re-used.</li>
                <li>Must have enough equipment for heating and cooling food.</li>
                <li> Air drying of dishes is required- no towel drying.</li>
              </ul>
            </li> 
            <li>
              <strong>Food handlers/ Personnel</strong>
              <ul>
                <li>Must wash hands as required.</li>
                <li>Nails must be trimmed and filed.</li>
                <li>No nail polish or artificial nails unless wearing gloves.</li>
                <li>Cuts, lesions on hands/ arms covered with impermeable covering.</li>
                <li>No jewellery except for simple wedding band. No watches, bracelets e.t.c.</li>
                <li>Clean clothing.</li>
                <li>No eating, drinking, chewing gum, using tobacco except in designated areas.</li>
                <li>Employee’s cups must have lid and straw.</li>
                <li>Employees experiencing persistent sneezing, coughing, runny nose or other discharges from eyes, nose or mouth may not work with exposed food, clean equipment or utensils or unwrapped single-service articles (napkins, plastic forks).</li>
                3<li>Hair properly restrained- pulled back plus hairnet, hat or scarf.</li>
              </ul>
            </li> 
            <li>
              <strong>Foods</strong>
              <ul>
                <li>Must be from an approved source e.g. KEBS</li>
                <li>Food must be honestly presented.</li>
                <li>Food may not be prepared in a private home.</li>
                <li>Packaging must be in good condition and protect contents from contamination.</li>
                <li>Fish and shellfish must be commercially harvested. </li>
                <li>May NOT touch ready-to-eat food with bare hands. Must use deli tissue, tongs or gloves. </li>
                <li>May not re-use utensil used for tasting without washing it.</li>
                <li>Foods must be protected from cross- contamination.</li>
                <li>Food removed from its original container must be labelled unless it is unmistakably recognizable.</li>
                <li>Raw fruits/vegetables must be washed before they are cut.</li>
                <li>Food shall be cooked to proper internal temperatures.</li>
                <li>Food that is not properly date-marked must be discarded.</li>
                <li>Food may only contact surfaces that are clean and sanitized.</li>
                <li>Returned food may not be offered for re-sale.</li>
              </ul>
            </li>
            <li>
              <strong>Water, Plumbing, Water Fixtures and Waste</strong>
              <ul>
                <li>Drinking water must come from an approved source.</li>
                <li>The plumbing system shall be repaired according to the law and be maintained in good repair.</li>
                <li>The plumbing system must meet all plumbing codes and meet the capacity needs of the food establishment.</li>
                <li>Water source and hot water capacity must be sufficient to meet the needs of the food establishment.</li>
                <li>Water pressure must e adequate.</li>
                <li>A hand washing sink must provide adequate running water.</li>
                <li>A hand washing sink must be kept clean at all times for employee use and may not be used for any other purpose.</li>
                <li>There must be a trash have soap and disposal towels, continuous loop towels or an air dryer can near the hand sink for towel disposal.</li>
                <li>At least one toilet must be provided for both male and female.</li>
                <li>A toilet room used by females must have a covered trash can for sanitary needs.</li>
                <li>A toilet room shall be completely enclosed and have a tight, self-closing door which shall be kept closed.</li>
                <li>A direct connection may not exist between the sewage system and a drain originating from equipment in which food, utensils or portable equipment is placed.</li>
                <li>Changing room/Cloak room shall be provided.  </li>
              </ul>
            </li>
            <li>
              <strong>Workplace Safety</strong>
              <ul>
                <li>Machines properly guarded (where applicable).</li>
                <li>Emergency exits provided</li>
                <li>Emergency exits accessible/free from obstruction </li>
                <li>First aid kit available and equipped</li>
                <li>Electrical wiring properly concealed</li>
                <li>Office accessories in secure places.</li>
                <li>Wall and ceiling fixtures fastened securely. </li>
                <li>Fire-fighting equipment provided and serviced. </li>
                <li>Appropriate signage (warnings)</li>
              </ul>
            </li>  
            <li>
              <strong>Pest and Vermin</strong>
              <ul>
                <li>No Presence or signs of pests/vermins in the premises.</li>
                <li>Proof of fumigation within the last 6 months.</li>
              </ul>
            </li>       
           
          

         
         
         

         
         
          </ol>
        </div>
          
        </div><!-- panel-body -->
       

      </div><!-- panel -->
	        
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

  <script type="text/javascript">
  $(document).ready(function(){
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    // $('#loader').hide();
    // $('#subcounty').on('change',function(e){
    //   $('#loader').show();
    //   $.post('<?php echo base_url();?>health/getWards',
    //   {
    //     subcounty:$('#subcounty').val()
    //   },
    //   function(data){
    //     $('#loader').hide();
    //     $('#ward').html(data);
    //     jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

    //     if(document.getElementById("ward") === null){
    //       $('#sub').hide();
    //     }else{
    //       $('#sub').show();
    //     }

    //   });

    // });
  });
  </script>



