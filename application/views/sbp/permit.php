 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url();?>components/back/images/ncc_logo_medium.png" type="image/png">

  <title>Nairobi County</title>

  <link href="<?php echo base_url();?>components/back/css/style.default.css" rel="stylesheet">
  <link href="<?php echo base_url();?>components/back/css/jquery.datatables.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>components/back/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>components/back/css/bootstrap-timepicker.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>components/back/css/jquery.tagsinput.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>components/back/css/colorpicker.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>components/back/css/dropzone.css" />
  <script src="<?php echo base_url();?>components/back/js/jquery-1.10.2.min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>

  <!-- Preloader -->
  <div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
  </div>

  <section>

    <div class="leftpanel">

      <div class="logopanel">
        <h1><span><img src="<?php echo base_url(); ?>components/back/images/ncc_logo_medium.png">  NCC</span></h1>
      </div><!-- logopanel -->

      <div class="leftpanelinner">    

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
          <div class="media userlogged">
            <img alt="" src="<?php echo base_url();?>components/back/images/photos/loggeduser.png" class="media-object">
            <div class="media-body">
              <h4>John Doe</h4>
              <span>"Life is so..."</span>
            </div>
          </div>
          
          <h5 class="sidebartitle actitle">Account</h5>
          <ul class="nav nav-pills nav-stacked nav-bracket mb30">
            <li><a href="<?php echo base_url();?>components/back/profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>
            <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
            <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
            <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
          </ul>
        </div>

        <h5 class="sidebartitle">Navigation</h5>
        <ul class="nav nav-pills nav-stacked nav-bracket">
          <li><a href="<?php echo base_url();?>selfservice/home"><i class="fa fa-home"></i> <span>Home</span></a></li>
          <!-- <li><a href="<?php //echo base_url();?>sbp/enter_acc_no"><i class="fa fa-barcode"></i> <span>Business Permits</span></a> -->
          <li class="nav-parent">
            <a href=""><i class="fa fa-money"></i> <span>Single Business Permits</span></a>
            <ul class="children">
              <li><a href="<?php echo base_url();?>sbp/enter_acc_no"><i class="fa fa-caret-right"></i>Renew SBP</a></li>
              <li><a href="<?php echo base_url();?>sbp/registersbp"><i class="fa fa-caret-right"></i>Register New Business</a></li>
              <li><a href="<?php echo base_url();?>sbp/sbp_print"><i class="fa fa-caret-right"></i>Print Permit</a></li>
            </ul>
          </li>
        </li>
        <li class="nav-parent">
          <a href=""><i class="fa fa-th"></i> <span>Land Rates</span></a>
          <ul class="children">
            <li><a href="<?php echo base_url();?>lr/enter_plot_no"><i class="fa fa-caret-right"></i> <span>Pay Land Rate</span></a></li>
          <li><a href="<?php echo base_url();?>lr/search"><i class="fa fa-caret-right"></i> <span>Search</span></a></li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-bullseye"></i> <span>AD Manager</span></a></li>
        <li><a href="#"><i class="fa fa-road"></i> <span>Parking Fee</span></a></li>
        <li class="nav-parent">
          <a href=""><i class="fa fa-money"></i> <span>Nairobi County e-Wallet</span></a>
          <ul class="children">
            <li><a href="<?php echo base_url();?>sbp/checkout"><i class="fa fa-caret-right"></i>Top Up</a></li>
            <li><a href="<?php echo base_url();?>sbp/register"><i class="fa fa-caret-right"></i>Register Wallet</a></li>
            <li><a href="<?php echo base_url();?>sbp/view_balance"><i class="fa fa-caret-right"></i> View Balance</a></li>
            <li><a href="<?php echo base_url();?>sbp/view_statement"><i class="fa fa-caret-right"></i> Mini Statement</a></li>
          </ul>
        </li>
      </ul>
      
    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->



  <div class="mainpanel">
   <div class="headerbar">
    <div class="topnav">
      <a class="menutoggle"><i class="fa fa-bars"></i></a>

      <ul class="nav nav-horizontal">
      <!-- <li class="active"><a href="<?php #echo base_url();?>en/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li> -->
        <li><a href="<?php echo base_url();?>sbp/view_balance"><i class="fa fa-caret-right"></i> Wallet Balance</a></li>
        <li><a href="<?php echo base_url();?>sbp/checkout"><i class="fa fa-envelope-o"></i> <span>Top Up Wallet</span></a></li>
        <li><a href="<?php echo base_url();?>sbp/view_statement"><i class="fa fa-caret-right"></i> Mini Statement</a></li>
        <li><a href="<?php echo base_url();?>sbp/sbp_print"><i class="fa fa-print"></i> Print Permit</a></li>
      </ul>
    </div>

    <div class="header-right">
      <ul class="headermenu">
        <li>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>components/images/photos/loggeduser.png" alt="" />
              James Mwema
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
              <li><a href="<?php echo base_url();?>selfservice/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
            </ul>
          </div>
        </li>
        <li>
          <!-- <button id="chatview" class="btn btn-default tp-icon chat-icon">
            <i class="glyphicon glyphicon-comment"></i>
          </button> -->
        </li>
      </ul>
    </div><!-- header-right -->






  </div><!-- headerbar -->
 














  <style type="text/css">
.progress-indicator {
   top:0;
   right:0;
   width:100%;
   height:100%;
   position:fixed;
   text-align:center;
   /* IE filter */
   filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50);
   -moz-opacity:0.5;    /* Mozilla extension */
   -khtml-opacity:0.5;  /* Safari/Conqueror extension */
   opacity:0.5; /* CSS3 */
   z-index:1000;
   background-color:white;
   display:none;
 }

 .progress-indicator img {
   margin-top:75px;
 }

</style>

  <div class="pageheader" style="text-align:center">
      <div class="logopanel">
        <h1><span><img src="<?php echo base_url(); ?>components/back/images/ncc_logo_medium.png">  NCC</span></h1>
      </div>

      <h2><i class="fa fa-home"></i> Register New Business <span>Register new SBP Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url();?>admin/indexx">Home</a></li>
          <li class="active">Register SBP</li>
        </ol>
      </div>
    </div>


    <div class="contentpanel" >
     
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="minimize">&minus;</a>
              </div>
              <h4 class="panel-title">NAIROBI COUNTY SINGLE BUSINESS PERMIT REGISTRATION</h4>
            </div>
            <div class="panel-body panel-body-nopadding">
             
              <!-- BASIC WIZARD -->
              <div id="validationWizard" class="basic-wizard">
               
                <ul class="nav nav-pills nav-justified">
                  <li><a href="#vtab1" data-toggle="tab"><span>Step 1:</span> Payment Details</a></li>
                  <li><a href="#vtab2" data-toggle="tab"><span>Step 2:</span> Complete Payment</a></li>
                  <li><a href="#vtab3" data-toggle="tab"><span>Step 3:</span> Print Receipt</a></li>
                </ul>
               
                <form class="form" id="firstForm"> 
                <div class="tab-content">

                  <div class="progress progress-striped active">
                    <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                 
                  <div class="tab-pane" id="vtab1">
                    <fieldset>
                      <div class="panel-heading">
                        <div class="panel-btns">
                        </div>
                        <h4 class="panel-title">Business Identification and Address</h4>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-8">
                          <label class="control-label">Business Name</label>
                          <input type="text" name="biz_name" id="biz_name" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">ID Document Type</label>
                          <select class="form-control" name="doc_type" id="doc_type" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['idtype'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->IDTypeCode; ?>"><?php echo $value->IDDescription; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">ID Document Number</label>
                          <input type="text" name="doc_no" id="doc_no" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Pin Number</label>
                          <input type="text" name="pin_no" id="pin_no" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">VAT Number</label>
                          <input type="text" name="vat_no" id="vat_no" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">P.O. Box</label>
                          <input type="text" name="box" id="box" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Postal Code</label>
                          <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Town</label>
                          <input type="text" name="town" id="town" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Telephone Number</label>
                          <input type="text" name="tel1" id="tel1" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Other Telephone Number</label>
                          <input type="text" name="tel2" id="tel2" class="form-control" placeholder=""  />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Fax</label>
                          <input type="text" name="fax" id="fax" class="form-control" placeholder=""  />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-8">
                          <label class="control-label">Email</label>
                          <input type="text" name="email" id="email" class="form-control" placeholder="" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Physical Address</label>
                          <input type="text" name="address" id="address" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Plot Number</label>
                          <input type="text" name="plot_number" id="plot_number" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Building Name</label>
                          <input type="text" name="building" id="building" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Floor Number</label>
                          <input type="text" name="floor" id="floor" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Stall/Room Number</label>
                          <input type="text" name="stall_room_no" id="stall_room_no" class="form-control" placeholder="" required />
                        </div>
                      </div>
                    </fieldset>
                    <div class="panel-heading">
                      <div class="panel-btns">
                      </div>
                      <h4 class="panel-title">Owner/Contact Person Details</h4>
                    </div>
                   
                    <fieldset>
                      <div class="form-group">
                        <div class="col-sm-8">
                          <label class="control-label">Full Names</label>
                          <input type="text" name="full_names" id="full_names" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">P.O Box</label>
                          <input type="text" name="owner_box" id="owner_box" class="form-control" placeholder=""  />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Postal Code</label>
                          <input type="text" name="owner_postal_code" id="owner_postal_code" class="form-control" placeholder=""  />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Telephone Number</label>
                          <input type="text" name="owner_telephone" id="owner_telephone" class="form-control" placeholder="" />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Alternative Telephone Number</label>
                          <input type="text" name="owner_telephone2" id="owner_telephone2" class="form-control" placeholder="" />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Fax</label>
                          <input type="text" name="owner_fax" id="owner_fax" class="form-control" placeholder=""  />
                        </div>
                      </div>
                    </fieldset>
                    <div class="panel-heading">
                      <div class="panel-btns">
                      </div>
                      <h4 class="panel-title">Business Classification/Activity Description</h4>
                    </div>
                    <fieldset>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Activity Description</label>
                          <input type="text" name="activity_dec" id="activity_dec" class="form-control" placeholder="" required />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Area(m2)</label>
                          <input type="text" name="area" id="area" class="form-control"  placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Other Details</label>
                          <input type="text" name="other_details" id="other_details" class="form-control" placeholder="" />
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">No of Emp</label>
                          <input type="text" name="emp_no" id="emp_no" class="form-control" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-8">
                          <label class="control-label">Activity Code</label>
                          <select class="form-control" name="activity_code" id="activity_code" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['bizactivity'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->ActivityCode; ?>"><?php echo $value->ActivityDescription; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                          <select class="form-control" name="activity_amount" id="activity_amount" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['bizactivity'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->ActivityCode; ?>"><?php echo $value->AnnualAmount; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                        </div>
                       
                        <div class="col-sm-4">
                          <label class="control-label">SBP Fee</label>
                          <input type="text" name="sbp_fee" class="form-control" id="sbp" placeholder="" required />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <label class="control-label">Zone</label>
                          <select class="form-control" name="zone" id="zone" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['zones'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->ZoneID; ?>"><?php echo $value->ZoneName; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Ward</label>
                          <select class="form-control" name="ward" id="ward" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['wards'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->WardID; ?>"><?php echo $value->WardName; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <label class="control-label">Relative Size</label>
                          <select class="form-control" name="size" id="size" required>
                            <option value="">Choose One</option>
                            <?php
                              foreach($xml['relsize'] as $key=>$value) {
                                ?>
                                 <option value="<?php echo $value->SizeCode; ?>"><?php echo $value->SizeDescription; ?></option>
                               <?php   
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                    </fieldset>
                  </div>
                  <div class="tab-pane" id="vtab2">
                      <div class="table-responsive">
                      <table class="table table-striped mb30">
                        <thead>
                          <tr>
                            <th colspan="2" style="text-align:center;">CONFIRM ACCOUNT DETAILS BELOW</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php //var_dump($confirm['result']); ?>
                          <tr>
                            <td><b>BUSINESS ID</b></td>
                            <td id="bizid"></td>
                          </tr>
                          <tr>
                            <td><b>BUSINESS NAME</b></td>
                            <td id="bizname"></td>
                          </tr>
                          <tr>
                            <td><b>PHYSICAL ADDRESS</b></td>
                            <td id="phyc"></td>
                          </tr>
                          <tr>
                            <td><b>BILL NUMBER</b></td>
                            <td id="bilno"></td>
                          </tr>
                          <tr>
                            <td><b>BILL STATUS</b></td>
                            <td id="billstatus"></td>
                          </tr>
                          <tr>
                            <td><b>ANNUAL AMOUNT</b></td>
                            <td id="annual"></td>
                          </tr>
                           <tr>
                            <td><b>COMMISSION</b></td>
                            <td id="com"></td>
                          </tr>
                          <tr>
                            <td><b>SBP FEE</b></td>
                            <td id="sbpf"></td>
                          </tr>
                        </tbody>
                      </table>
                      </div><!-- table-responsive -->
                      <div class="row">
                        <div class="col-md-6">
                              <div class="ckbox ckbox-success">
                                <input type="checkbox" id="checkboxSuccess"/>
                                <label for="checkboxSuccess">Print Permit</label>
                              </div>
                        </div>
                      </div>
                      <div id="cred">
                        <label class="control-label">Phone</label>
                        <div class="form-group">
                          <div class="col-sm-8">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter Wallet Phone Number" required />
                          </div>
                        </div>
                        <label class="control-label">Pin</label>
                        <div class="form-group">
                          <div class="col-sm-8">
                            <input type="password" name="pin" id="pin" class="form-control" placeholder="Enter Wallet Pin" required />
                          </div>
                        </div>
                      </div>
                  </div>
                 
                  <div class="tab-pane" id="vtab3">
                      
                      <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Please Wait . . .      Transaction in progress
                      </div>

                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Business Registration Complete <a href="" class="btn btn-default print"><i class="fa fa-hdd-o"></i> Print Permit</a>
                      </div>
                      <!--  -->
                  
                  
                </div><!-- tab-content -->
                </form>
               
                <ul class="pager wizard">
                    <li class="previous"><a href="javascript:void(0)">Previous</a></li>
                    <li class="next"><a href="javascript:void(0)">Next</a></li>
                  </ul>
               
              </div><!-- #validationWizard -->
             
            </div><!-- panel-body -->
          </div><!-- panel -->
        </div><!-- col-md-6 -->
     
    </div><!-- contentpanel -->

<script src="<?php echo base_url(); ?>/components/back/js/bootstrap-wizard.min.js"></script>
<script src="<?php echo base_url(); ?>/components/back/js/jquery.validate.min.js"></script>

<script type="text/javascript">
var json='';
  $(document).ready(function(){

      if (!$('#checkboxSuccess').is(':checked')){
        $('#cred').hide();
      }

      $('#checkboxSuccess').on('click',function(){
        //alert('a');
        if (!$('#checkboxSuccess').is(':checked')){
            $('#cred').hide();
         }else{
            $('#cred').show();
         }

     });

      $('.progress-indicator').css( 'display', 'none' );
      //sbp fee
      $('#activity_amount').hide();
      $('#activity_code').on('change',function(){
          code=$(this).val();
          $("#activity_amount option[value='"+code+"']").attr('selected', 'selected');
          amount=$("#activity_amount option:selected").text();
          $('#sbp').val(amount);
      });

      // Progress Wizard
      $('#progressWizard').bootstrapWizard({
        'nextSelector': '.next',
        'previousSelector': '.previous',
        onNext: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        },
        onPrevious: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        },
        onTabShow: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        }
      });

      // With Form Validation Wizard
      var $validator = jQuery("#firstForm").validate({
        highlight: function(element) {
          jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
          jQuery(element).closest('.form-group').removeClass('has-error');
        }
      });
     
      jQuery('#validationWizard').bootstrapWizard({
        tabClass: 'nav nav-pills nav-justified nav-disabled-click',
        onTabClick: function(tab, navigation, index) {
          return false;
        },
        onNext: function(tab, navigation, index) {
          var $valid = jQuery('#firstForm').valid();
          if(!$valid) {
           
            $validator.focusInvalid();
            return false;
          }else {

              if (index==1) {
                $('.progress-indicator').css( 'display', 'inline' );
                setTimeout( function() {
                    $('.progress-indicator > img').attr( 'src',
                    $('.progress-indicator > img').attr('src')+'?reload' );
                }, 50 );
                $.post('<?php echo base_url(); ?>sbp/complete_reg_sbp',
                  {
                    biz_name:$('#biz_name').val(),
                    doc_type:$('#doc_type').val(),
                    doc_no:$('#doc_no').val(),
                    pin_no:$('#pin_no').val(),
                    vat_no:$('#vat_no').val(),
                    box:$('#box').val(),
                    postal_code:$('#postal_code').val(),
                    town:$('#town').val(),
                    tel1:$('#tel1').val(),
                    tel2:$('#tel2').val(),
                    fax:$('#fax').val(),
                    email:$('#email').val(),
                    address:$('#address').val(),
                    plot_number:$('#plot_number').val(),
                    building:$('#building').val(),
                    floor:$('#floor').val(),
                    stall_room_no:$('#stall_room_no').val(),
                    full_names:$('#full_names').val(),
                    owner_box:$('#owner_box').val(),
                    owner_postal_code:$('#owner_postal_code').val(),
                    owner_telephone:$('#owner_telephone').val(),
                    owner_telephone2:$('#owner_telephone2').val(),
                    owner_fax:$('#owner_fax').val(),
                    activity_dec:$('#activity_dec').val(),
                    area:$('#area').val(),
                    other_details:$('#other_details').val(),
                    emp_no:$('#emp_no').val(),
                    activity_code:$('#activity_code').val(),
                    sbp_fee:$('#sbp').val(),
                    zone:$('#zone').val(),
                    ward:$('#ward').val(),
                    size:$('#size').val(),
                    pin:$('#pin').val()
                  },
                  function(data){
                     $('.progress-indicator').css( 'display', 'none' );
                    json=JSON.parse(data);

                    $('#bizid').html(json.biz_id);
                    $('#bizname').html(json.biz_name);
                    $('#phyc').html(json.physical_address);
                    $('#bilno').html(json.bill_no);
                    $('#billstatus').html('NOT PAID');
                    $('#annual').html(json.annualamount);
                    $('#com').html(json.commission);
                    $('#sbpf').html(json.sbpfee);
                   
                  });

              };
              if (index==2) {
                $("a.print").attr("href", "<?php echo base_url(); ?>sbp/print_sbp_preview/"+json.biz_id);

                  $('.alert-info').hide();
              };
          }
        }
      });
  });

</script>

<script type="text/javascript">
  $(document).ready(function(){

  });


 </script>

<?php

/*$string='C:\xampp\htdocs\web\application\views\sbpsbp/qrcode/0016562014031235';

$path=str_replace('\', '/', $string);*/

?>








</section>



<script src="<?php echo base_url();?>components/back/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery-ui-1.10.3.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/modernizr.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/toggles.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/retina.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.cookies.js"></script>

<script src="<?php echo base_url();?>components/back/js/flot/flot.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/morris.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url();?>components/back/js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url();?>components/back/js/custom.js"></script>
<script src="<?php echo base_url();?>components/back/js/dashboard.js"></script>

<script src="<?php echo base_url();?>components/back/js/jquery.autogrow-textarea.js"></script>
<script src="<?php echo base_url();?>components/back/js/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url();?>components/back/js/dropzone.min.js"></script>
<script src="<?php echo base_url();?>components/back/js/colorpicker.js"></script>
<script src="<?php echo base_url();?>components/back/js/jquery.validate.min.js"></script>


<script>
jQuery(document).ready(function(){
    
  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  
  // Tags Input
  jQuery('#tags').tagsInput({width:'auto'});
   
  // Textarea Autogrow
  jQuery('#autoResizeTA').autogrow();
  
  // Color Picker
  if(jQuery('#colorpicker').length > 0) {
   jQuery('#colorSelector').ColorPicker({
      onShow: function (colpkr) {
        jQuery(colpkr).fadeIn(500);
        return false;
      },
      onHide: function (colpkr) {
        jQuery(colpkr).fadeOut(500);
        return false;
      },
      onChange: function (hsb, hex, rgb) {
        jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
        jQuery('#colorpicker').val('#'+hex);
      }
   });
  }
  
  // Color Picker Flat Mode
  jQuery('#colorpickerholder').ColorPicker({
    flat: true,
    onChange: function (hsb, hex, rgb) {
      jQuery('#colorpicker3').val('#'+hex);
    }
  });
   
  // Date Picker
  jQuery('#datepicker').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });
  
  // Spinner
  var spinner = jQuery('#spinner').spinner();
  spinner.spinner('value', 0);
  
  // Input Masks
  jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(999) 999-9999");
  jQuery("#ssn").mask("999-99-9999");
  
  // Time Picker
  jQuery('#timepicker').timepicker({defaultTIme: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});
  jQuery('#timepicker3').timepicker({minuteStep: 15});

  
});
</script>


</body>

</html>
