<!--   <div class="mainpanel"> -->
 <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="">Single Business Permit</a></li>
          <li class="active">Print Business Permit</li>
        </ol>
      </div>
    </div>
    
     <!-- <div class="pageheader" style="height:60px">
    <a href="<?php #echo base_url();?>index.php/sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php #echo base_url();?>index.php/sbp/sbp_print"><i class="fa fa-print"></i>Print Permit</a>
    </div> -->
   
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-8" style="margin-right:20px">
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
            <a href="#" class="minimize">&minus;</a>
          </div>
          <h4 class="panel-title panelx">Enter Business ID</h4>
          <p>Cross check to make sure you have filled in the correct details</p>
        </div>
        <div class="panel-body">
          <?php echo form_open('sbp/print_sbp',array('class' =>"form-block ")) ?>
            <div class="row mb10">
              <?php 
                $curyear = date("Y");
                $lastyear = (int)($curyear) - 1;
                $lastyearbutone = (int)($lastyear) - 1;
               ?>
                      <div class="col-sm-8">
                      <label class="col-sm-4 control-label">Business ID</label>
                        <input type="text" class="form-control" id="biz_idd" name="biz_idd" placeholder="Enter Business ID" required />
                      </div>
                      <div class="col-sm-8">
                      <label class="col-sm-6 control-label">Print permit for the Year</label>
                        <select class="form-control input-sm mb15" name="year" id="year" data-placeholder="Select Year...">
                        <option value="2017">2017</option>
                        <option value="<?php echo $curyear ;?>"><?php echo $curyear ;?></option>
                        <option value="<?php echo $lastyear ;?>"><?php echo $lastyear ;?></option>
                        <option value="<?php echo $lastyearbutone ;?>"><?php echo $lastyearbutone ;?></option>
                      </select>
                      </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Submit">
            <button type="reset" class="btn btn-default">Reset</button>
          <?php echo form_close(); ?>
        </div><!-- panel-body -->
      </div><!-- panel -->
       <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title panelx">Follow these simple steps</h4>
		  <ol>
		  <li><p>Input you Business ID and Click Submit</p></li>
      <li><p>Print Permit for the Year</p></li>
		  <li><p>Click The Print Business Permit Button</p></li>
		   
		    </ol>
        </div>
    
  </div>
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->
  
  