<!-- <div class="mainpanel"> -->
<div class="pageheader">
  <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="">Single Business Permit</a></li>
      <li class="active">Check Status</li>
    </ol>
  </div>
</div>

<!-- <div class="pageheader" style="height:60px">
  <a href="<?php #echo base_url();?>sbp/view_statement"><i class="fa fa-book"></i>Mini Statement</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php #echo base_url();?>sbp/sbp_print"><i class="fa fa-print"></i>Print Permit</a>
</div> -->
<div class="contentpanel" >
  <?php
  //var_dump($datax);
  
  @$bill = $datax['Status'];
  #echo $this->session->userdata('token');
  // if($bal>$datax['sbpfee']){
  //   echo "1";
  // }else{ echo "0";}
  
  $state=(isset($datax['result_code']))?$datax['result_code']:0;
  $prep=$state;

  if(true):
  ?>
  <div >
    <div class="panel panel-default col-md-8" style="margin-right:20px">

    
      
      <div class="panel-body">
         <?php if (isset($datax['ErrorType'])){ ?>
          <div class="alert alert-warning">
            <?=$datax['Message']?>            
          </div>
        <?php } elseif($datax['BillStatus']=='PAID') {?>
           
        <?php } else {?>
        <div class="row">
          <?php echo form_open('sbp/confirm_payment');?>
          <div class="table-responsive">
            <table class="table table-striped mb30">
              <thead>
              
                <tr>
                  <th colspan="2" style="text-align:center;">BUSINESS DETAILS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>BUSINESS NAME</b></td>
                  <td><?php echo $datax['Name'];?></td>
                </tr>
                <tr>
                  <td><b>PHYSICAL ADDRESS</b></td>
                  <td><?php echo $datax['PhysicalAddress'];?></td>
                </tr>
                <tr>
                  <td><b>BUSINESS ID</b></td>
                  <td><?php echo $datax['LicenseID'];?></td>
                </tr>
                <?php if($bill=="NEW"): ?>
                <tr>
                  <td><b>CONTACT PERSON NAME</b></td>
                  <td>
                 <input type="text" id="contactpersoname" name="contactpersonname" class="form-control" required/>                   
                  </td>
                </tr>
                <tr>
                  <td><b>CONTACT PERSON ID NUMBER</b></td>
                  <td><?php if($datax['IDNumberName']!=NULL) {
                    echo $datax['IDNumberName']; } else{
                    echo '<input type="text" id="idnumber" name="idnumber" class="form-control" required/>';
                    }?>
                  </td>
                </tr>
                <tr>
                  <td><b>TELEPHONE</b></td>
                  <td>
                    <input type="text" id="telephone" name="telephone" class="form-control" required/>
                  </td>
                </tr>
               
                  <td><b>BUILDING CATEGORY</b></td>
                  <td><?php if($datax['BuildingCategory']!=0) {
                    echo "<input id='buildingCategoryHidden' type='hidden' value='".$datax["BuildingCategory"]."'>";
                    
                    if($datax['BuildingCategory']==1)
                    {echo "Storey";}
                    else{
                    echo "Non Storey";
                    }
                    }
                    else{
                    echo "<input id='buildingCategoryHidden' type='hidden' value='".$datax['BuildingCategory']."'>";
                    echo '<select class="form-control" name="buildingcategory" id="buildingcategory"><option value="1">Storey</option><option value="0">Non-Storey</option>';
                  echo '</select>';
                  }?>
                 

                </td>
                
              </tr>
              <tr>
                <td><b>BUILDING NAME</b></td>
                <td><?php if($datax['Building']!=NULL) {
                  echo $datax['Building'];
                  }
                  else{
                  echo '<input type="text" id="building" name="building" class="form-control" required/>';
                  }?>
                </td>
              </tr>
              <tr id="floor">
                <td><b>FLOOR</b></td>
                <td><input type='text' name='floor' class='form-control'></td>
              </tr>
              <tr>
                <td><b>ROOM / STALL NO.</b></td>
                <td><?php if($datax['RoomStallNumber']!=NULL) {
                  echo $datax['RoomStallNumber'];
                  }
                  else{
                  echo '<input type="text" id="room_stall_number" name="room_stall_number" class="form-control" required/>';
                  }?>
                </td>
              </tr>
              <tr>
                <td>Major Categories</td>
                <td>
                   <select class="form-control" name="business_classes" id="business_classes" required>                            
                        <?php
                          foreach($business_classes as $biz_class) {
                            ?>
                             <option value="<?php echo $biz_class->ID; ?>"><?php echo $biz_class->Name; ?></option>
                           <?php    
                          }
                        ?>
                      </select>
                </td>
              </tr>
              <tr>
                <td>Sub Categories</td>
                <td>
                   <select class="form-control" name="sub_classes" id="sub_classes" required >
                      
                    </select> 
                </td>
              </tr>
              <tr>
                <td><b>SUB-COUNTY</b></td>
                <td><?php if($datax['ZoneID']!=NULL) {
                  echo $datax['ZoneID'];
                  }
                  else{                    
                  echo '<select class="form-control" name="zonecode" id="zonecode">';
                    foreach($zones['sub_county'] as $county): ?>
                    <option data-name="<?=$county['name']?>" value="<?php echo $county['id']; ?>"><?=$county['name']?></option>
                    <?php endforeach;?>
                  <?php echo '</select>';
                  echo '<input type="hidden" id="zonename" name="zonename" class="form-control" required/>';
                  }?>
                </td>
              </tr>
              <tr>
                <td><b>WARD</b></td>
                <td><?php if($datax['WardID']!=NULL) {
                  echo $datax['WardID'];
                  } else {
                  echo '<select class="form-control" name="wardcode" id="wardcode">';                    
                  echo '</select>';
                  echo '<input type="hidden" id="wardname" name="wardname" class="form-control" required/>';
                  }
                  ?>
                </td>
              </tr>
              <tr>
                <td><b>PLOT No.</b></td>
                <td><?php if($datax['PlotNumber']!=NULL) {
                  echo $datax['PlotNumber']; } else{
                  echo '<input type="text" id="plot" name="plot" class="form-control" required/>';
                  }?>
                </td>
              </tr>
              <?php endif; ?>
              <!-- <tr>
                <td><b>BILL NUMBER</b></td>
                <td><?php echo $datax['bill_no'];?></td>
              </tr> -->
              <!-- <tr>
                <td><b>BILL STATUS</b></td>
                <td><?php if($datax['bill_stat']=="NEW"){echo "UNPAID";}else{echo "PAID";}  ?></td>
              </tr> -->
              <tr>
                <td><b>YEAR</b></td>
                <td><?php echo $datax['Year'];?></td>
              </tr>
              
            </tbody>
          </table>
          </div><!-- table-responsive -->
          
        </div>
        <?php }?>
        <div class="panel-footer">          
          <div class="row">
            <?php if($datax['BillStatus']=="NEW"){            
            echo '<input type="submit" id="bill_status"  value="Update Details" class="btn btn-primary">';
            echo '<input type="hidden" id="bizname" value="'.$datax['Name'].'" name="bizname" class="form-control"/>';           
            echo '<input type="hidden" id="bid" value="'.$datax['LicenseID'].'" name="bid" class="form-control"/>';
            echo '<input type="hidden" id="year" value="'.$datax['Year'].'" name="year" class="form-control"/>';   
            echo '<input type="hidden" id="excludeAdvertisement" value="'.$excludeAdvertisement.'" name="excludeAdvertisement" class="form-control"/>';   
           
           
            echo " ";
            echo anchor('sbp/enter_acc_no','Cancel',array('class'=>'btn btn-primary'));
            echo form_close();
            }elseif($datax['BillStatus']=="PAID"){
            echo '<div class="alert alert-info">';
              echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>';
              echo "The Business is Already Paid";
            echo '</div>';
            }
            ?>
            <?php elseif($datax['BillStatus']=="NEW" && $bal < $datax['license_fee']): ?>
            <div class="alert alert-danger ">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "You Do Not Have Enough Funds to Proceed with Payment . Your Wallet balance is Ksh. ".$bal." ". anchor('selfservice/wallettopup','Top up Now',array('class'=>'btn btn-primary','style'=>'float:right;padding-top:1px;')); ?>
            </div>
            <?php elseif($bill=="PAID"): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "The Business is Already Paid"; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">    Go Back</a>
            </div>
            <?php elseif($state==1070): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "The Given Calender Year not Supported"; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">    Go Back</a>
            </div>
            <?php elseif($state==1110): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "The Business ID Entered is Invalid"; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">    Go Back</a>
            </div>
            <?php elseif($state==44402): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "Service currently unavailable but we will be back soon"; ?>
            </div>
            <?php elseif($state==2021): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "The Phone Number Entered is Invalid"; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">  Go Back</a>
            </div>
            <?php elseif($state==1115): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "No Bill Number Found for the existing Calender Year"; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">    Go Back</a>
            </div>
            <?php elseif($state==55): ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
              <?php echo "Authorization has been denied for this request."; ?><a href="<?php echo base_url();?>sbp/enter_acc_no">    Go Back</a>
            </div>
          <?php endif; ?>



        </div>
        
        
      </div>
    </div>
    
    </div><!-- contentpanel -->
    <div class="panel panel-default col-md-3" >
      <div class="panel-heading">
        <div class="panel-btns">
        </div>
        <h4 class="panel-title">Follow these simple steps</h4>
        <ol>
          <li><p>Confirm that all the details displayed match your business</p></li>
          <li><p>Enter any details required in the input boxes</p></li>
          <li><p>Click Proceed to continue with payment or else Cancel</p></li>
        </ol>
      </div>
      </div><!-- mainpanel -->
     

      <script type="text/javascript">
      jQuery(document).ready(function(){
        $('#buildingcategory').on('change',function(){
          console.log($(this).val());
          if($(this).val()==1){
            $('#floor').show();            
          } else {
            $('#floor').hide();
          }
        })

        $.ajax({
        url: '<?php echo base_url(); ?>sbp/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("Trying....");
        },
        success: function(data) {
            $("#wardcode").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#wardcode').append($('<option>', {
                    "value": id
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }


    });

           $.ajax({
            url: '<?php echo base_url(); ?>sbp/getSubClasses',
            type: 'POST',
            dataType: 'json',
            cache: false,
            data: $("#business_classes").serialize(),
            beforeSend: function() {
                console.log("Trying....");
                dat = $("#business_classes").serialize();
                
            },
            success: function(data) {
                $("#sub_classes").empty();
                var optgroup = data;
                for (var i = 0; i < optgroup.length; i++) {
                    var id = optgroup[i].ID;
                    var name = optgroup[i].Name;
                    $('#sub_classes').append($('<option>', {
                        "value": id
                    }).text(name));
                }
            },
            error: function(err) {
                console.log(err)
            }
        });

           $("#business_classes").change(function(event) {
        $.ajax({
            url: '<?php echo base_url(); ?>sbp/getSubClasses',
            type: 'POST',
            dataType: 'json',
            cache: false,
            data: $("#business_classes").serialize(),
            beforeSend: function() {
                console.log("Trying....");
                 dat = $("#business_classes").serialize();            
                
            },
            success: function(data) {
               
                $("#sub_classes").empty();
                var optgroup = data;
                for (var i = 0; i < optgroup.length; i++) {
                    var id = optgroup[i].ID;
                    var name = optgroup[i].Name;
                   // var code = [120,195,215,210,220,205,200,546];

                    $('#sub_classes').append($('<option>', {
                        "value": id
                    }).text(name));
                    console.log(id + " " + name);
                }
            },
            error: function(err) {
                console.log(err)
            }
        });
    });

      });
      </script>

      <script type="text/javascript">
      //$("#zonename").val($('#zonecode').find(':selected').data('name')); 

     $("#zonecode").change(function(event) {
     $("#zonename").val($(this).find(':selected').data('name'));     
      
    $.ajax({
        url: '<?php echo base_url(); ?>sbp/getWard',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: $("#zonecode").serialize(),
        beforeSend: function() {
            console.log("");
        },
        success: function(data) {
            $("#wardcode").empty();
            var optgroup = data;
            for (var i = 0; i < optgroup.length; i++) {
                var id = optgroup[i].ID;
                var name = optgroup[i].Name;
                $('#wardcode').append($('<option>', {
                    "value": id
                }).text(name));
            }
        },
        error: function(err) {
            console.log(err)
        }
    });

     
       
});
        
        </script>

         <script type="text/javascript">
      $(document).ready(function(){
      jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
        $('#zonename').val($('#zonecode').find(':selected').data('name'));
      });
      </script>