<!-- <div class="mainpanel"> -->
    
    <div class="contentpanel" >
      
      <div >
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="#" class="panel-close">&times;</a>
                <a href="#" class="minimize">&minus;</a>
              </div>
              
            </div>
            <div class="panel-body">
              <div class="row">

                <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $message; ?>
                </div>
                  
              </div>
            </div><!-- panel-body -->
          </div>
        </div>
      
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->