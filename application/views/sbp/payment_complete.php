<!-- <div class="mainpanel"> -->
 <div class="pageheader">
      <h2><i class="fa fa-home"></i> Single Business Permit <span>Pay your Business Permit Online</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">Single Business Permit</a></li>
          <li class="active">Complete Payment</li>
        </ol>
      </div>
    </div>

  <div class="contentpanel" >
      
    <div class="row">
    <?php $response = $complete['rescode'];?>
    <?php $restext = $complete['restext'];?>

    
     <div class="panel panel-default col-md-8" style="margin-right:20px">
          <form id="form1" method="POST" class="form-horizontal">
            <div class="panel panel-default" >
              <div class="panel-heading">
                
                <h4 class="panel-title panelx">Complete Payment <?php #echo $response = $complete['rescode'];?></h4>
              </div>
              <?php if($response=="0"): ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo "Transaction was successfully completed.".anchor('sbp/reprintSbpreceipt/'.$complete['businessid'],'Print Receipt',array('class'=>'btn btn-primary','style'=>'float:right;padding-top:1px;'));
                  ?>
                  <?php echo anchor('sbp/print_sbp_previews/'.$complete['businessid'].'/'.$complete['year'],'Print Permit',array('class'=>'btn btn-primary','style'=>'float:right;padding-top:1px; margin-right:15px;'))?>
              </div>
            <?php else: ?>
              <div class="alert alert-info">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <?php echo  "Error Code ".$response. " : ".$restext; ?>
              </div>
            <?php endif; ?>
            </div><!-- panel-default -->
          </form>
          
      </div><!-- row -->
         
       </div>

  </div>

  <!-- contentpanel -->


    
</div><!-- mainpanel -->

