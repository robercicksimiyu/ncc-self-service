<form id="sbpForm">

	<select id="countiesSelect" name="countiesSelect">
		<?php foreach ($data as $row): ?>
			echo "<option value="<?= $row->ID ?>"> <?= $row->Name ?> </option>";
		<?php endforeach ?>
	</select>

	<select id="wardsSelect">
		

	</select>

</form>


<script type="text/javascript">

	 $("#countiesSelect").change( function( event ){
	    $.ajax({
	      url:'<?php echo base_url(); ?>sbp/getWard',
	      type:'POST',
	      dataType:'json',
	      cache:false,
	      data:$("#sbpForm").serialize(),

	      beforeSend:function(){
	      	console.log("Trying....");
	      },

	      success:function(data){

	        $("#wardsSelect").empty();

	        var optgroup = data;

	        for (var i = 0; i < optgroup.length; i++) {
	          var id = optgroup[i].ID;
	          var name = optgroup[i].Name;
	          $('#wardsSelect').append($('<option>', { "value" : id }).text( name ));
	        }
	      },

	      error:function(err){
	        console.log(err)
	      }
	    });
  });
	
</script>