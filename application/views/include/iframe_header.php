<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url();?>components/back/images/ncc_logo_medium.png" type="image/png">

  <title>Nairobi City County</title>

  <link href="<?php echo base_url();?>application/assets/back/css/style.default.css" rel="stylesheet">
  <link href="<?php echo base_url();?>application/assets/back/css/jquery.datatables.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/bootstrap-timepicker.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/jquery.tagsinput.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/colorpicker.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/dropzone.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>application/assets/back/css/emomentum.css" />
  <script src="<?php echo base_url();?>application/assets/back/js/jquery-1.10.2.min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>


<body>


  <!-- Preloader -->
  

  <section>

    <div class="leftpanel">

      <div class="logopanel">
        <h1><span><img src="<?php echo base_url(); ?>components/back/images/ncc_logo_medium.png">NCC</span></h1>
      </div><!-- logopanel -->

      <div class="leftpanelinner">    

    </div>
  </div>



  <div class="mainpanel">

  
  
