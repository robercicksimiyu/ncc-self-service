<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>components/back/images/ncc_logo_medium.png"
          type="image/png">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/ico">

    <title>Nairobi City County - Self Service Portal</title>

    <link href="<?php echo base_url(); ?>application/assets/back/css/style.default.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>application/assets/back/css/jquery.datatables.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/bootstrap-fileupload.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/jquery.tagsinput.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/colorpicker.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/dropzone.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/assets/back/css/emomentum.css"/>
    <script src="<?php echo base_url(); ?>application/assets/back/js/jquery-1.10.2.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        .btn-item {
            padding: 5px 10px 2px 10px !important;
            margin-top: 10px !important;
            color: #FFFFFF !important;
            background-color: #D82E2F !important;
        }

        .btn-item:hover {
            padding: 5px 10px 2px 10px !important;
            margin-top: 10px !important;
            color: #FFFFFF !important;
            background-color: #D82E2F !important;
        }

        fieldset.fset {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow: 0px 0px 0px 0px #000;
            box-shadow: 0px 0px 0px 0px #000;
        }

        legend.fset {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width: auto;
            padding: 0 10px;
            border-bottom: none;
        }

        ::-webkit-input-placeholder {
            font-style: italic;
        }

        :-moz-placeholder {
            font-style: italic;
        }

        ::-moz-placeholder {
            font-style: italic;
        }

        :-ms-input-placeholder {
            font-style: italic;
        }
    </style>
</head>


<body onload="display_ct();">
<div data-part="breadcrumbs-navbar" id="helper-navbar" class="row fixed-nav-bar top breadcrumbs-navbar">
    <div class="col-xs-12 welcome-nav">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="container">
                    <small class="no-padding no-margin bread">
                        <ol class="breadcrumb" bla="no-margin no-padding">
                            <li><a href="https://epayments.nairobi.go.ke/selfservice/home">Home</a></li>
                            <li class="active">ePayments</li>
                        </ol>
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="leftpanel">

        <div class="logopanel">
            <h1><span><img src="<?php echo base_url(); ?>application/assets/back/images/ncc_logo_medium.png">NCC</span>
            </h1>
        </div><!-- logopanel -->

        <div class="leftpanelinner">

            <h5 class="sidebartitle">Navigation</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket">
                <li><a href="<?php echo base_url(); ?>selfservice/home"><i class="fa fa-home"></i> <span>Home</span></a>
                </li>
                <!-- <li><a href="<?php //echo base_url();?>sbp/enter_acc_no"><i class="fa fa-barcode"></i> <span>Business              Permits</span></a> -->
                <li class="nav-parent">
                    <a href=""><i class="fa fa-money"></i> <span>Unified Business <p
                                    style="text-align:center ">Permits</p></span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>sbp/viewTerms/health"><i class="fa fa-caret-right"></i>Renew
                                UBP</a></li>
                        <li><a href="<?php echo base_url(); ?>sbp/viewTermsRegister"><i class="fa fa-caret-right"></i>Register
                                New Business</a></li>
                        <li><a href="<?php echo base_url(); ?>sbp/sbp_print"><i class="fa fa-caret-right"></i>Print
                                Permit</a></li>
                        <li><a href="<?php echo base_url(); ?>sbp/reprintSReceipt"><i class="fa fa-caret-right"></i>Print
                                Receipt</a></li>
                    </ul>
                </li>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-th"></i> <span>Land Rates</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>lr/enter_plot_no"><i class="fa fa-caret-right"></i> <span>Pay Land Rate          </span></a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>lr/landRsearchdetails"><i class="fa fa-caret-right"></i>
                                <span>Print Receipt</span></a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-home"></i> <span>House Rent & Markets</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>house_rent/houserent"><i class="fa fa-caret-right"></i>
                                <span>House Rent</span></a></li>
                        <li><a href="<?php echo base_url(); ?>marketstall/select_market"><i
                                        class="fa fa-caret-right"></i> <span>Markets</span></a></li>
                        <li><a href="<?php echo base_url(); ?>rents/reprintrentNew"><i class="fa fa-caret-right"></i>
                                <span>Print Rent Receipt</span></a></li>
                    </ul>
                </li>
                <!-- <li><a href="#"><i class="fa fa-bullseye"></i> <span>AD Manager</span></a></li> -->
                <li class="nav-parent">
                    <a href=""><i class="fa fa-road"></i> <span>Parking Fees</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>parking/daily_parking"><i class="fa fa-caret-right"></i>
                                <span>Daily Parking</span></a></li>
                        <li><a href="<?php echo base_url(); ?>parking/parkingTopup"><i class="fa fa-caret-right"></i>
                                <span>Topup Daily Fee</span></a></li>
                        <li><a href="<?php echo base_url(); ?>parking/parkingPenalties"><i
                                        class="fa fa-caret-right"></i> <span>Parking Penalties</span></a></li>
                        <li><a href="<?php echo base_url(); ?>parking/reprintPenaltyReceipt"><i
                                        class="fa fa-caret-right"></i> <span>Clamping Receipt</span></a></li>
                        <li><a href="<?php echo base_url(); ?>parking/seasonal_parking"><i
                                        class="fa fa-caret-right"></i> <span>Seasonal Parking</span></a></li>
                        <li><a href="<?php echo base_url(); ?>parking/reprintDailyReceipt"><i
                                        class="fa fa-caret-right"></i> <span>Print Receipt</span></a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-th"></i> <span>E-Construction</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>econstruction"><i class="fa fa-caret-right"></i> <span>Pay for Permit</span></a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>econstruction/reprintECReceipt"><i
                                        class="fa fa-caret-right"></i> <span>Print Receipt</span></a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-th"></i> <span>Bill Payment</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>billpayment"><i class="fa fa-caret-right"></i> <span>Pay for Bill</span></a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>billpayment/reprintECReceipt"><i
                                        class="fa fa-caret-right"></i> <span>Print Receipt</span></a></li>
                    </ul>
                </li>

                <li class="nav-parent">
                    <a href=""><i class="fa fa-th"></i> <span>Miscellaneous</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>miscellaneous"><i class="fa fa-caret-right"></i> <span>Pay for Miscellaneous</span></a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>miscellaneous/printReceipt"><i
                                        class="fa fa-caret-right"></i> <span>Print Receipt</span></a></li>
                    </ul>
                </li>
                <!-- <li class="nav-parent">
          <a href=""><i class="fa fa-th"></i> <span>Health</span></a>
          <ul class="children">
            <li><a href="<?php echo base_url(); ?>health/foodHygieneApplicationForm"><i class="fa fa-caret-right"></i> <span>Apply Food Hygiene</span></a></li>
            <li><a href="<?php echo base_url(); ?>health/payHygiene"><i class="fa fa-caret-right"></i> <span>Pay for Food Hygiene</span></a></li>
            <li><a href="<?php echo base_url(); ?>health/printHygiene"><i class="fa fa-caret-right"></i> <span>Print Food Hygiene Permit</span></a></li>
          <li><a href="<?php echo base_url(); ?>health/InstitutionHealthApplicationForm"><i class="fa fa-caret-right"></i> <span>Institution Health</span></a></li>
          <li><a href="<?php echo base_url(); ?>health/printInstitution"><i class="fa fa-caret-right"></i> <span>Print Institution Health Recommendation</span></a></li>
          </ul>
        </li> -->
                <li class="nav-parent">
                    <a href=""><i class="fa fa-money"></i> <span>Nairobi County<p
                                    style="text-align:center"> e-Wallet</p></span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>selfservice/wallettopup"><i class="fa fa-caret-right"></i>Top
                                Up</a></li>
                        <li><a href="<?php echo base_url(); ?>selfservice/viewBalance"><i class="fa fa-caret-right"></i>
                                View Balance</a></li>
                        <li><a href="<?php echo base_url(); ?>selfservice/viewStatement"><i
                                        class="fa fa-caret-right"></i> Mini Statement</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-money"></i> <span>Nairobi County<p style="text-align:center">Regularization</p></span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>rlb/building_structures/step1"><i
                                        class="fa fa-caret-right"></i> Building Structures</a></li>
                        <li><a href="<?php echo base_url(); ?>rlb/amalgamation/step1"><i class="fa fa-caret-right"></i>
                                Amalgamation</a></li>
                        <li><a href="<?php echo base_url(); ?>rlb/change_user/step1"><i class="fa fa-caret-right"></i>Change
                                of Use</a></li>
                        <li><a href="<?php echo base_url(); ?>rlb/extention/step1"><i class="fa fa-caret-right"></i>Extension
                                of Use</a></li>
                        <li><a href="<?php echo base_url(); ?>rlb/subdivision/step1"><i class="fa fa-caret-right"></i>
                                Subdivision</a></li>
                        <li><a href="<?php echo base_url(); ?>rlb/payReg"><i class="fa fa-caret-right"></i> Pay
                                regularization</a></li>
                        <!--<li><a href="<?php echo base_url(); ?>rlb/subdivision/large_schemes/step1"><i class="fa fa-caret-right"></i> Subdivision(Large Schemes)</a></li>-->
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href=""><i class="fa fa-money"></i> <span>Nairobi County &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading Zone</span></a>
                    <ul class="children">
                        <li><a href="<?php echo base_url(); ?>transport_infrustructure/renew_register"><i
                                        class="fa fa-caret-right"></i>Application / Renewal</a></li>
                        <!-- <li><a href="<?php echo base_url(); ?>transport_infrustructure/section/banners"><i class="fa fa-caret-right"></i> Banners</a></li>
            <li><a href="<?php echo base_url(); ?>transport_infrustructure/section/bus_shelter"><i class="fa fa-caret-right"></i>Bus shelters</a></li> -->
                        <li><a href="<?php echo base_url(); ?>transport_infrustructure/pay"><i
                                        class="fa fa-caret-right"></i> Pay Loading Zone </a></li>
                        <!-- <li><a href="<?php echo base_url(); ?>transport_infrustructure/print"><i class="fa fa-caret-right"></i> Print receipt</a></li> -->
                    </ul>
                </li>
            </ul>

        </div>
    </div>


    <div class="mainpanel" style="height:500px !important;">
        <div class="headerbar">
            <div class="topnav">
                <a class="menutoggle"><i class="fa fa-bars"></i></a>

                <ul class="nav nav-horizontal">
                    <!-- <li class="active"><a href="<?php #echo base_url();?>en/dashboardname"><i class="fa fa-home"></i> <span>Dashboard</span></a></li> -->
                    <li><a href="<?php echo base_url(); ?>selfservice/viewBalance"><i class="fa fa-caret-right"></i>Wallet
                            Balance<?php #echo $this->session->userdata['balanc']; #echo  $bal ?></a></li>
                    <li><a href="<?php echo base_url(); ?>selfservice/wallettopup"><i class="fa fa-envelope-o"></i>
                            <span>Top Up Wallet</span></a></li>
                    <li><a href="<?php echo base_url(); ?>selfservice/viewStatement"><i class="fa fa-caret-right"></i>
                            Mini Statement</a></li>
                    <li><a href="<?php echo base_url(); ?>sbp/sbp_print"><i class="fa fa-print"></i> Print Permit</a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>selfservice/receiptQuery"><i class="fa fa-caret-right"></i>
                            Validate Receipt</a></li>
                </ul>
            </div>

            <div class="header-right">
                <ul class="headermenu">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <?php
                                $uname = $this->session->userdata('name');
                                $ph = $this->session->userdata('jpwnumber');
                                $phon = substr($ph, -9);
                                $phone = "0" . $phon;
                                if (isset($uname) && !empty($uname)) {

                                    echo $uname . " " . "(" . $phone . ")";
                                } else {

                                    redirect('selfservice/login');
                                }
                                ?>
                                <?php #echo $this->session->userdata['name'];  ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                <li><a href="<?php echo base_url(); ?>user/profile"><i
                                                class="glyphicon glyphicon-log-out"></i>Change PIN</a></li>
                                <li><a href="<?php echo base_url(); ?>selfservice/logout"><i
                                                class="glyphicon glyphicon-user"></i> Log Out</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <!-- <button id="chatview" class="btn btn-default tp-icon chat-icon">
                          <i class="glyphicon glyphicon-comment"></i>
                        </button> -->
                    </li>
                </ul>
            </div><!-- header-right -->


        </div><!-- headerbar -->
  
  
