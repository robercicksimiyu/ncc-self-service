</section>
<div role="nav-menu" data-part="fixed-footer-nav-menu" id="navigation-bar" class="row fixed-nav-bar bottom hidden-xs"> 
  <!--The menu icon-->
  <div class="col-xs-1 hidden-xs hidden-sm text-align-center"> <a href="http://nairobi.go.ke" class="yellow"> <i class="fa fa-home fa-3x yellow"></i> </a> </div>
  <!-- Time and weather-->
  <div class="col-md-2 hidden-xs hidden-sm text-center yellow-bg">
    <h2 class="moment-in-time grey no-padding" id="clockss"></h2>
    <small class="grey">Local time, Nairobi.</small> </div>
  <!-- The navigation menu-->
  <div class="col-sm-6 col-md-5 hidden-xs">
    <div class="row"> 
      <!-- Previous scroller-->
      <div class="col-xs-1">
        <p class="arrow"> <i id="nav-menu-prev" class="fa fa-angle-left fa-2x pull-left white-arrow">&nbsp;</i> </p>
      </div>
      <!-- The nav menu-->
      <div class="col-xs-10 uppercase">
        <div id="nav-menu" class="nav-menu text-align-center">
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/governors-profile/" cla> <i class="fa fa-star-o fa-2x"></i>
            <p style="font-size:12px;">Governor&#039;s Profile</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/services/" cla> <i class="fa fa-globe fa-2x"></i>
            <p style="font-size:12px;">Services</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/history/" cla> <i class="fa fa-file fa-2x"></i>
            <p style="font-size:12px;">History</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/explore-nairobi/" cla> <i class="fa fa-search fa-2x"></i>
            <p style="font-size:12px;">Explore</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://.nairobi.go.ke/home/opportunities/" cla> <i class="fa fa-files-o fa-2x"></i>
            <p style="font-size:12px;">Tenders</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/common-city-laws-and-regulations/" cla> <i class="fa fa-check-square-o fa-2x"></i>
            <p style="font-size:12px;">City ByLaws</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/news-and-updates/" cla> <i class="fa fa-microphone fa-2x"></i>
            <p style="font-size:12px;">News</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/whats-happening/" cla> <i class="fa fa-calendar fa-2x"></i>
            <p style="font-size:12px;">Events</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/downloads/" cla> <i class="fa fa-money fa-2x"></i>
            <p style="font-size:12px;">Downloads</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/visitors/" cla> <i class="fa fa-share-square fa-2x"></i>
            <p style="font-size:12px;">Visitors</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/index.php/home/maps/" cla> <i class="fa fa-map-marker fa-2x"></i>
            <p style="font-size:12px;">Maps</p>
            </a> </div>
          <div class="nav-icon link"> <a href="http://nairobi.go.ke/home/sectors/" cla> <i class="fa fa-hospital-o fa-2x"></i>
            <p style="font-size:12px;">Sectors</p>
            </a> </div>
        </div>
      </div>
      <!-- Next Scroller-->
      <div class="col-xs-1">
        <p class="arrow"> <i id="nav-menu-next" class="fa fa-angle-right fa-2x pull-right white-arrow">&nbsp;</i> </p>
      </div>
    </div>
  </div>
  <!-- Emergency Services and social-icons-->
  <div class="col-sm-6 col-md-4">

    <div class="row inherit height">
      <div class="col-xs-6 col-sm-7 inherit height">
        <div id="social-icons">
          <div class="row text-center">
            <div class="col-xs-4"> <a class="blue" target="_blank" href="https://www.facebook.com/pages/Nairobi-City-County-Official/569756036404745"> <i class="fa fa-facebook-square fa-2x"></i> </a> </div>
            <div class="col-xs-4"> <a class="blue" target="_blank" href="https://twitter.com/county_nairobi"> <i class="fa fa-twitter fa-2x"></i> </a> </div>
            <div class="col-xs-4"> <a class="blue" target="_blank" href="http://nairobi.go.ke"> <i class="fa fa-envelope fa-2x"></i> </a> </div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-5 no-padding no-margin emergency-services-bar">
        <div class="row">
          <div class="col-xs-4"> <i class="fa fa-bell fa-2x fa-borer">&nbsp;</i> </div>
          <div class="col-xs-8">
            <p class="uppercase no-margin text-justify"> <span>Emergency Services</span> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>application/assets/back/js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery.validate.min.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/application.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/execute.js"></script> 
<script src="<?php echo base_url();?>application/assets/back/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery-ui-1.10.3.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/modernizr.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/toggles.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/retina.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.cookies.js"></script>

<script src="<?php echo base_url();?>application/assets/back/js/flot/flot.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/morris.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url();?>application/assets/back/js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url();?>application/assets/back/js/custom.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/dashboard.js"></script>

<script src="<?php echo base_url();?>application/assets/back/js/jquery.autogrow-textarea.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/dropzone.min.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/colorpicker.js"></script>
<script src="<?php echo base_url();?>application/assets/back/js/jquery.validate.min.js"></script>

<script>
jQuery(document).ready(function(){
    
  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  
  // Tags Input
  jQuery('#tags').tagsInput({width:'auto'});
   
  // Textarea Autogrow
  jQuery('#autoResizeTA').autogrow();
  
  // Color Picker
  if(jQuery('#colorpicker').length > 0) {
	 jQuery('#colorSelector').ColorPicker({
			onShow: function (colpkr) {
				jQuery(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				jQuery(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
				jQuery('#colorpicker').val('#'+hex);
			}
	 });
  }
  
  // Color Picker Flat Mode
	jQuery('#colorpickerholder').ColorPicker({
		flat: true,
		onChange: function (hsb, hex, rgb) {
			jQuery('#colorpicker3').val('#'+hex);
		}
	});
   
  // Date Picker
  jQuery('#datepicker').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });
  
  // Spinner
  var spinner = jQuery('#spinner').spinner();
  spinner.spinner('value', 0);
  
  // Input Masks
  jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(999) 999-9999");
  jQuery("#ssn").mask("999-99-9999");
  
  // Time Picker
  jQuery('#timepicker').timepicker({defaultTIme: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});
  jQuery('#timepicker3').timepicker({minuteStep: 15});

  
});
</script>

<script type="text/javascript">
 function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
    }

    function display_ct() {
      var strcount
      var x = new Date()
      //var x1=x.getMonth() + "/" + x.getDate() + "/" + x.getYear(); 
      var x1 =x.getHours( )+ ":" +  (x.getMinutes()<10?'0':'') + x.getMinutes();
      document.getElementById('clockss').innerHTML = x1;

      tt=display_c();
    }
</script>

</body>


</html>
