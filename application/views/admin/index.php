  <!-- <div class="mainpanel"> -->
    <style type="text/css">
      .panel-primary .panel-heading {
          background-color: #428bca;
      }
      .panel-rents .panel-heading{
          background-color: #428bca;
      }
      .panel-parking .panel-heading{
          background-color: #1d2939;
      }
      .panel-miscellaneous .panel-heading{
          background-color: #f0ad4e;
      }
      .panel-wallet .panel-heading{
          background-color: #999967;
      }
      .panel-receipts .panel-heading{
          background-color: #EE6F18;
      }
      .panel-econstruction .panel-heading{
          background-color: #EE6F18;
      }

     



      /*ul .col-xs-4{
        list-style-im
      }*/

      .quiklinks{
        color: #fff !important;
      }
      </style>
  

  <div class="pageheader">
    <h2><i class="fa fa-home"></i>Nairobi City County Self Service Portal <?php #echo $login['name']; ?></h2>
  </div>

  <div class="contentpanel">
    <div class="row">

      <div class="col-sm-6 col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-barcode"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>UBP</h1>
                    <small class="stat-label">Unified Business Permit</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff"; class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('sbp/viewTerms/health','Renew',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('sbp/viewTermsRegister','Register',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('sbp/sbp_print','Permit',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('sbp/reprintSReceipt','Receipt',array('class'=>'quiklinks')); ?></li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 --> 

      <div class="col-sm-6 col-md-4">
          <div class="panel panel-danger panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-th"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>LR</h1>
                    <small class="stat-label">Land Rates</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('lr/enter_plot_no','Pay',array('class'=>'quiklinks')); ?></li>
                      <!-- <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php #echo anchor('lr/search','Search',array('class'=>'quiklinks')); ?></li> -->
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('lr/landRsearchdetails','Receipt',array('class'=>'quiklinks')); ?></li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

      <div class="col-sm-6 col-md-4">
          <div class="panel panel-rents panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-inbox"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>RT</h1>
                    <small class="stat-label">Rents</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('house_rent/houserent','Houses',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('marketstall/select_market','Stalls',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rents/reprintrentNew','Receipt',array('class'=>'quiklinks')); ?></li>
                      <li>&nbsp</li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

      <div class="col-sm-6 col-md-4">
          <div class="panel panel-parking panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-road"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>PF</h1>
                    <small class="stat-label">Parking Fees</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/daily_parking','Daily',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/parkingTopup','Topup',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/parkingPenalties','Penalties',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/seasonal_parking','Seasonal',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/reprintDailyreceipt','Print Receipt',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('parking/reprintPenaltyReceipt','Clamping Receipt',array('class'=>'quiklinks')); ?></li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <!-- <div class="col-sm-6 col-md-4">
          <div class="panel panel-econstruction panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-money"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>e-C</h1>
                    <small class="stat-label">e-Construction</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('econstruction','Pay',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('econstruction/reprintECReceipt','Receipt',array('class'=>'quiklinks')); ?></li>
                      <!-- <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php #echo anchor('selfservice/viewStatement','Statement',array('class'=>'quiklinks')); ?></li>
                      <!-- <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php #echo anchor('parking/parkingReceipt','Receipt',array('class'=>'quiklinks')); ?></li> 
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading
          </div><!-- panel 
        </div><!-- col-sm-6 --> 

        <div class="col-sm-6 col-md-4">
          <div class="panel panel-wallet panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-money"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>e-W</h1>
                    <small class="stat-label">e-Wallet</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('selfservice/wallettopup','Topup',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('selfservice/viewBalance','Balance',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('selfservice/viewStatement','Statement',array('class'=>'quiklinks')); ?></li>
                      <!-- <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php #echo anchor('parking/parkingReceipt','Receipt',array('class'=>'quiklinks')); ?></li> -->
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-4">
          <div class="panel panel-miscellaneous panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-th"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>M</h1>
                    <small class="stat-label">Miscellaneous</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff" class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('miscellaneous','Pay',array('class'=>'quiklinks')); ?></li>
                      <!-- <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php #echo anchor('lr/search','Search',array('class'=>'quiklinks')); ?></li> -->
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('miscellaneous/printReceipt','Receipt',array('class'=>'quiklinks')); ?></li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                      <li>&nbsp</li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

	

      <div class="col-sm-6 col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-money"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>R</h1>
                    <small class="stat-label">Regular -ization</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff"; class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/building_structures/step1','Building Structures',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/amalgamation/step1','Amalgamation',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/change_user/step1','Change of_Use',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/extention/step1','Extension of_Use',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/subdivision/step1','Subdivision',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('rlb/payReg','Pay Regularization',array('class'=>'quiklinks')); ?></li>
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
         <div class="col-sm-6 col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-road"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>L</h1>
                    <small class="stat-label">Loading Zone</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff"; class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('transport_infrustructure/section/loading_zone','Application',array('class'=>'quiklinks')); ?></li> 
                       <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('transport_infrustructure/renew_register','Renew',array('class'=>'quiklinks')); ?></li>                      
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
         <div class="col-sm-6 col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <i style="font-size:;opacity:1" class="fa fa-building-o"></i>
                  </div>
                  <div class="col-xs-4">
                    <h1>E</h1>
                    <small class="stat-label">E-Construction</small>
                  </div>
                  <div class="col-xs-4">
                    <ul style="color:#fff"; class="fa-ul">
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('econstruction','Pay',array('class'=>'quiklinks')); ?></li>
                      <li><span class="fa-li glyphicon glyphicon-hand-right"></span><?php echo anchor('econstruction/reprintECReceipt','Print Receipt',array('class'=>'quiklinks')); ?></li>              
                      
                    </ul>
                  </div>
                </div>
                <div class="mb15"></div>
              </div>
            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        

      
    </div><!-- row -->

  </div><!-- contentpanel -->

</div><!-- mainpanel -->

