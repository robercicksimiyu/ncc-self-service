  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Daily Parking Transactions<span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">transactions</a></li>
          <li class="active"></li>
        </ol>
      </div>
    </div>
  
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-12">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Daily Parking Transactions</h4>
          <p></p>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
          <table class="table" id="table2">
              <thead>
                 <tr>
                    <th>UserID</th>
                    <th>Receipt Number</th>
                    <th>Date</th>
                    <th>Name</th>
                    <!-- <th>Reg No.</th>
                    <th>Vehicle type</th>
                    <th>Zone</th> -->
                    <th>Amount</th>
                    <th>Phone</th>
                    <th>Channel</th>
                 </tr>
              </thead>
              <tbody>
              <?php foreach($trans->result() as $row) { ?>
                 <tr class="odd gradeX">
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->receiptno; ?></td>
                    <td><?php echo $row->issuedate; ?></td>
                    <td><?php echo $row->regno; ?></td>
                    <!-- <td><?php echo $row->regno; ?></td>
                    <td><?php echo $row->category; ?></td>
                    <td><?php echo $row->zone; ?></td> -->
                    <td><?php echo $row->amount; ?></td>
                    <td><?php #echo $row->businessid; ?></td>
                    <td><?php echo $row->channel; ?></td>
                 </tr>
              <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

<script>
  jQuery(document).ready(function() {
    
    jQuery('#table1').dataTable();
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>
  