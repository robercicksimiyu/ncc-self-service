  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Landrates Transactions<span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">transactions</a></li>
          <li class="active"></li>
        </ol>
      </div>
    </div>
  
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-12">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Landrates Transactions</h4>
          <p></p>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
          <table class="table" id="table2">
              <thead>
                 <tr>
                    <th>ID</th>
                    <th>Receipt Number</th>
                    <th>Date</th>
                    <th>Paid By</th>
                    <th>Amount</th>
                    <th>Plot no.</th>
                    <th>Plot Owner</th>
                    <th>Penalties</th>
                    <th>Amount due</th>
                    <th>Phone</th>
                    <th>Channel</th>
                 </tr>
              </thead>
              <tbody>
              <?php foreach($landrates->result() as $row) { ?>
                 <tr class="odd gradeX">
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->receiptno; ?></td>
                    <td><?php echo $row->issuedate; ?></td>
                    <td><?php echo $row->paidby; ?></td>
                    <td><?php echo $row->amount; ?></td>
                    <td><?php echo $row->plotno; ?></td>
                    <td><?php echo $row->plotowner; ?></td>
                    <td><?php echo $row->penalties; ?></td>
                    <td><?php echo $row->amountdue; ?></td>
                    <td><?php echo $row->username; ?></td>
                    <td><?php echo $row->channel; ?></td>
                 </tr>
              <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

<script>
  jQuery(document).ready(function() {
    
    jQuery('#table1').dataTable();
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>
  