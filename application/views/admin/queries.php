  <div class="pageheader">
      <h2><i class="fa fa-home"></i>Queries List<span></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="#">queries</a></li>
          <li class="active"></li>
        </ol>
      </div>
    </div>
  
    <div class="contentpanel" >
      
      <div class="panel panel-default col-md-12">
        <div class="panel-heading">
        
          <h4 class="panel-title panelx">Queries</h4>
          <p></p>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
          <table class="table" id="table2">
              <thead>
                 <tr>
                    <th>QueryID</th>
                    <th>IMEI</th>
                    <th>REG No.</th>
                    <th>Location</th>
                    <th>Time</th>
                    <th>Status</th>
                 </tr>
              </thead>
              <tbody>
              <?php foreach($queries->result() as $row) { ?>
                 <tr class="odd gradeX">
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->imei; ?></td>
                    <td><?php echo $row->reg_no; ?></td>
                    <td><?php echo $row->location; ?></td>
                    <td><?php echo $row->time; ?></td>
                    <td><?php echo $row->status; ?></td>
                 </tr>
              <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- contentpanel -->

    
  </div><!-- mainpanel -->

<script>
  jQuery(document).ready(function() {
    
    jQuery('#table1').dataTable();
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>
  