<div class="pageheader">
  <h2><i class="fa fa-inbox"></i>Advertising Fees<span>Pay for your Advertising Fees online</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="#">AD Manager</a></li>
      <li class="active">Enter Bill No.</li>
    </ol>
  </div>
</div>
    
    <div class="contentpanel" >

      <?php 

      $rescode= $ad['rescode'];

      if($rescode=="0"):

      ?>
      
         <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('admanager/completeADPayment',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">AD DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $ad['invoicenumber'];?></td>
                        <input type="hidden" id="transactionid" name="transactionid" value="<?php echo $ad['TransactionId'];?>"/>
                        <input type="hidden" id="invoicestatus" name="invoicestatus" value="<?php echo $ad['invoicestatus'];?>"/>
                        <input type="hidden" id="invoicenumber" name="invoicenumber" value="<?php echo $ad['invoicenumber'];?>"/>
                        <input type="hidden" id="client" name="client" value="<?php echo $ad['client'];?>"/>
                        <input type="hidden" id="clientaccountnumber" name="clientaccountnumber" value="<?php echo $ad['clientaccountnumber'];?>"/>
                        <input type="hidden" id="feetype" name="feetype" value="<?php echo $ad['feetype'];?>"/>
                        <input type="hidden" id="invoiceamount" name="invoiceamount" value="<?php echo $ad['invoiceamount'];?>"/>
                        <input type="hidden" id="invoiceblocked" name="invoiceblocked" value="<?php echo $ad['invoiceblocked'];?>"/>
                        <input type="hidden" id="signagetype" name="signagetype" value="<?php echo $ad['signagetype'];?>"/>
                        <input type="hidden" id="sitenumber" name="sitenumber" value="<?php echo $ad['sitenumber'];?>"/>
                      </tr>
                      <tr>
                        <td><b>INVOICE STATUS</b></td>
                        <td><?php echo strtoupper($ad['invoicestatus']);?></td>
                      </tr>
                      <tr>
                        <td><b>CLIENT</b></td>
                        <td><?php echo $ad['client'];?></td>
                      </tr>
                      <tr>
                        <td><b>CLIENT ACCOUNT NUMBER</b></td>
                        <td><?php echo strtoupper($ad['clientaccountnumber']);?></td>
                      </tr>
                      <tr>
                        <td><b>FEE TYPE</b></td>
                        <td><?php echo $ad['feetype'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE AMOUNT</b></td>
                        <td><?php echo number_format($ad['invoiceamount'], 2, '.', ',');?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE BLOCKED</b></td>
                        <td><?php echo $ad['invoiceblocked'];?></td>
                      </tr>
                      <tr>
                        <td><b>SIGNAGE TYPE</b></td>
                        <td><?php echo $ad['signagetype'];?></td>
                      </tr>
                      <tr>
                        <td><b>SITE NUMBER</b></td>
                        <td><?php echo $ad['sitenumber'];?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <?php 
                  echo'<div class="col-sm-2">';
                  echo '<h4 class="subtitle mb5">'."Wallet PIN".'</h4>';
                  echo'</div>'; 
                  echo'<div class="col-sm-3">';
                  
                  echo '<input type="password" id="jp_pin" placeholder="Enter Wallet Pin" name="jp_pin" class="form-control" required/>';
                  echo'</div>';
                  #echo '<br/>';
                  ?>
                  <input type="submit" value="Confirm Transaction" class="btn btn-primary">
                  <?php echo form_close(); ?>
                  <?php echo anchor('econstruction','Cancel',array('class'=>"btn btn-default")); ?>
                </div>


              </div>
        </div>
      
    </div><!-- contentpanel -->


	  <div class="panel panel-default col-md-3" >
        <div class="panel-heading">
          <div class="panel-btns">
            <!-- <a href="#" class="panel-close">&times;</a> -->
         
          </div>
          <h4 class="panel-title">Follow these simple steps</h4>
		  <ol>
		  <li>
          <p>Confirm that all the details displayed are correct</p></li>
		   <li>
          <p>Click Proceed to continue with payment or else Cancel</p></li>
		    </ol>
        </div>
    
  </div><!-- mainpanel -->
<?php elseif($rescode=="4440" || $rescode=="44405" || $rescode=="44406"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry! Service is currently unavailable but we will be back soon"; ?>
              </div>
<?php elseif($rescode=="1110"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "You have entered an invalid Invoice Number"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="1097"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Costing for the given Zone Code not found"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="2023"): ?>
              <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <?php echo "Sorry.your Account has been suspended"; ?>
                <a href="<?php echo base_url();?>index.php/econstruction" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-mail-reply"></i>Back</a>
              </div>
<?php elseif($rescode=="2030"): ?>
                      <div class="panel panel-default col-md-8" style="margin-right:20px">
           
            <div class="panel-body">
              <div class="row">

                  <?php echo form_open('construction/completePaymentDaily',array('class' =>"form-block")) ?>
                  <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th colspan="2" style="text-align:center;">constructionELLANEOUS DETAILS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>INVOICE NUMBER</b></td>
                        <td><?php echo $ad['invoicenumber'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE STATUS</b></td>
                        <td><?php echo strtoupper($ad['invoicestatus']);?></td>
                      </tr>
                      <tr>
                        <td><b>CLIENT</b></td>
                        <td><?php echo $ad['client'];?></td>
                      </tr>
                      <tr>
                        <td><b>CLIENT ACCOUNT NUMBER</b></td>
                        <td><?php echo strtoupper($ad['clientaccountnumber']);?></td>
                      </tr>
                      <tr>
                        <td><b>FEE TYPE</b></td>
                        <td><?php echo $ad['feetype'];?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE AMOUNT</b></td>
                        <td><?php echo number_format($ad['invoiceamount'], 2, '.', ',');?></td>
                      </tr>
                      <tr>
                        <td><b>INVOICE BLOCKED</b></td>
                        <td><?php echo $ad['invoiceblocked'];?></td>
                      </tr>
                      <tr>
                        <td><b>SIGNAGE TYPE</b></td>
                        <td><?php echo $ad['signagetype'];?></td>
                      </tr>
                      <tr>
                        <td><b>SITE NUMBER</b></td>
                        <td><?php echo $ad['sitenumber'];?></td>
                      </tr>
                    </tbody>
                  </table>
                  </div><!-- table-responsive -->
        
              </div>
              
              <div class="panel-footer">

                <div class="row">
                  <div class="alert alert-danger col-md-8" style="margin-top:10px;margin-left:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo "Sorry.you do not have sufficient funds in your wallet to continue with the transaction"; ?>
                    <a href="<?php echo base_url();?>index.php/selfservice/wallettopup" class="btn btn-primary receipt" style="float:right;"><i class="fa fa-money"></i>Top Up Wallet</a>
                  </div>
                </div>


              </div>
        </div>
      
    </div> 
<?php endif; ?>