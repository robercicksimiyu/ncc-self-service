<?php
$lang['text_msg_success']	= "Record posted successfully";
$lang['text_msg_exists'] 	= "Record already exists";

$lang['text_fullname'] 		= "Contact Person";
$lang['text_email'] 		= "Email";
$lang['text_phone'] 		= "Phone Number";
$lang['text_duration'] 		= "Duration";
$lang['test_description'] 	= "Description";
$lang['test_subcounty'] 	= "Sub County";
$lang['text_zone'] 			= "Zone";
$lang['text_category'] 		= "Category";
$lang['text_address'] 		= "Address";

$lang['text_bid'] 			= "Business Number";
$lang['text_lr'] 			= "Land Rate Number";
$lang['text_road_name'] 	= "Road Name";
$lang['text_area_location'] = "Area/Location";

/*Environment Module*/
$lang['text_environent_title'] 	= "Environment";
$lang['text_hire_parks'] 		= "Hire of Parks / Grounds";
$lang['hire_parks_form'] 		= "EF02";
$lang['text_landscaping_fees'] 	= "Landscaping Fees";
$lang['landscaping_fees_form'] 	= "EF03";
$lang['text_quarrying_permit'] 	= "Quarrying Permit";
$lang['quarrying_permit_form'] 	= "EF04";
$lang['text_recycling_permit'] 	= "Recycling Permit";
$lang['recycling_permit_form'] 	= "EF05";
$lang['text_tip_charges'] 		= "Tip Charges";
$lang['tip_charges_form'] 		= "EF06";
$lang['text_saleof_flowers'] 	= "Sale of Flowers";
$lang['saleof_flowers_form']	= "EF07";
$lang['text_construction_soil']	= " Soil Transportation";
$lang['construction_soil_form']	= "EF08";
$lang['text_tree_cutting'] 		= "Tree Cutting";
$lang['tree_cutting_form'] 		= "EF09";
$lang['text_waste_collection']	= "Waste collection Permit";
$lang['waste_collection_form'] 	= "EF10";

/*Advertisement Module*/
$lang['text_small_advertisement_title']	= "Small Format Signage Licensing";
$lang['text_large_advertisement_title']	= "Large Format Signage Licensing";

$lang['text_check_business_title']="Do you have a registered business";



/*Traffic*/
$lang['text_commencement_date']= "Commensement Date";
$lang['text_completion_date']= "Completion Date";

$lang['text_carriageway']= "Carriageway";
$lang['text_footpath']= "Footpath";
$lang['text_verge']= "Verge";
$lang['text_Other']= "Other";
$lang['text_length']= "Length";
$lang['text_width']= "Width";
$lang['text_depth']= "Depth";
$lang['text_surface_works']= "Surface Works";

/*/*  "API_Id": "string",
        "Bid": "string",
        "Applicant_name": "string",
        "Address": "string",
        "purpose": "string",
        "Tel_no": "string",
        "commencement_date": "string",
        "completion_date": "string",
        "lrNo": "string",
        "road_name": "string",
        "location_plan": "string",
        "Form_Id": "string",
        "Carriageway_length": "string",
        "Carriageway_width": "string",
        "Carriageway_depth": "string",
        "footpath_length": "string",
        "footpath_width": "string",
        "footpath_depth": "string",
        "verge_length": "string",
        "verge_width": "string",
        "verge_depth": "string",
        "Other_length": "string",
        "Other_width": "string",
        "Other_depth": "string",
        "surface_works": "string",
        "category_id": "string"*/
	

$lang['text_environent_title'] 	= "Environment";



//$lang['waste_collection_form'] 	= "EF10";








/*
Landscaping fees EF03
 Quarrying permit EF04
 Recycling permit EF05
 Sale of flowers EF06
 Tip charges EF07
 Transportation of soil from construction site EF08
 Tree cutting EF09
 Waste collection permit EF10
*/


/*
200 – Record posted successfully
400 – 
401 – Internal server error
402 – Unauthorized access
403 –Record already exists*/