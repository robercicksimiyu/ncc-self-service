<?php
/* start of php file */
class MY_Controller extends CI_Controller {
    public function __construct() {
       parent::__construct();
    }
}

class Authenticated_controller extends MY_Controller {
    public function __construct() {
       parent::__construct();
       $is_logged_in = $this->session->userdata('is_logged_in');
       if(!isset($is_logged_in) || $is_logged_in != true)
       {
       		redirect('selfservice/login');
       } 
   }

}

/**
* Login check of the user is already logged in
*/
class Logincheck_Controller extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(isset($is_logged_in) && $is_logged_in == true) {
			redirect('selfservice/userLogin');
		}
	}
}
/* end of php file */