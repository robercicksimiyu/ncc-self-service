<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class House_rent extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('rent_model');
        $this->load->model('sbp_model');
        $this->load->model('user_model');
    }

    function get_houses(){
        return $this->rent_model->getEstatesHouses();
    }

 
    #-----new-----#
    function houserent(){
        $data['title'] = 'House Rent';
        $data['content'] = 'rent/selectestate';
        $data['estates']=$this->rent_model->getEstatesNew();
        #$data['housetypes']=$this->rent_model->getHouseTypes();
        $this->load->view('include/back_template', $data);
        #var_dump($this->rent_model->getEstates());

    }

    function houseTypes(){
        $this->rent_model->getHouseTypes();
    }

    function getResidence(){
        $this->rent_model->getResidence();
    }

    function preparehousepayment(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmhouserent';
        $data['confirm']=$this->rent_model->prephousepaymentNew();
        $this->load->view('include/back_template', $data);
        #var_dump($data['confirm']);
    }

    function confirm2rent(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmrent';
        $data['confirm']=$this->rent_model->confirm2rent();
        $this->load->view('include/back_template', $data);
    }

    function completerentpayment(){

        $data['title'] = 'Complete Payment';
        $data['content'] = 'rent/completerentpayment';
        $data['bal'] = $this->user_model->viewBalance1();
        $data['message']=$this->rent_model->completeNew();
        $this->load->view('include/back_template', $data);
        #var_dump($this->rent_model->completerentpayment());
    }
    #-----new-----#

    
}