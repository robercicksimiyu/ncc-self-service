<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Liquor extends CI_Controller {
	public $data;

	function __construct() {
		parent::__construct();
		$this->load->model('liquor_model');
		$this->load->model('user_model');
		#$this->load->library('email');
	}

	 function do_upload()
{       
    $this->load->library('upload');

    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
    }
}

private function set_upload_options()
{   
    //upload an image options
    $config = array();
    $config['upload_path'] = './Images/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']      = '0';
    $config['overwrite']     = FALSE;

    return $config;
}

	// public function multiple_upload()
	// {
	// 	$usew_file='vatcert';
	//    $number_of_files_uploaded = count($_FILES[$usew_file]['name']);
	//    $final_files_data=array();
	//    //var_dump($number_of_files_uploaded);
	//    // Faking upload calls to $_FILE
	//    for ($i = 0; $i < $number_of_files_uploaded; $i++) :
	//      $config = array(
	//        'file_name'     => 'random_name',
	//        'allowed_types' => 'jpg|jpeg|png|gif|doc|pdf|docx|odt',
	//        'max_size'      => 3000,
	//        'overwrite'     => FALSE,
	//        'upload_path'   => FCPATH . 'uploads'
	//      );
	//      $this->upload->initialize($config);
	//      if ( ! $this->upload->do_upload($usew_file)) :
	//       print_r($error = array('error' => $this->upload->display_errors())) ;
	//    	//var_dump($error);
	//        //$this->load->view('upload_form', $error);
	//      else :
	//      echo "<pre>";
	//        print_r($final_files_data[] = $this->upload->data()) ;
	//        // Continue processing the uploaded data
	//      endif;
	//    endfor;
	   
	// }

	function test(){
		//var_dump($this->liquor_model->multiple_upload('vatcert'));
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/retail/appform';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function viewTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/retail/retailLiquorConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	// function viewInstitutionTerms(){
	// 	$this->data['title'] = 'Terms & Conditions';
	// 	$this->data['content'] = 'fire/institutionConditions';
	// 	#$this->data['subcounty'] = $this->health_model->getSubcounties();
	// 	$this->load->view('include/back_template', $this->data);
	// }

	function landingPage(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/retail/RetailLiquorLandingPage';
		$this->load->view('include/back_template', $this->data);
	}

	function retailLiquorApplicationForm(){
		$this->data['title'] = 'Liquor Permit Application Form';
		$this->data['content'] = 'liquor/retail/RetailLiquorApplicationForm';
		// $this->liquor_model->getRetailLiquorDetails();
		$this->load->view('include/back_template', $this->data);
	}

	// function firePermitApplicationFormB(){
	// 	$this->data['title'] = 'Fire Permit Application Form';
	// 	$this->data['content'] = 'fire/building';
	// 	#$this->data['subcounty'] = $this->health_model->getSubcounties();
	// 	$this->load->view('include/back_template', $this->data);
	// }

	function displayRetailLiquorDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['liquor'] = $this->liquor_model->getRetailLiquorDetails();
		$this->data['content'] = 'liquor/retail/retailLiquorPermitApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeRetailLiquorInspectionPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeRetailLiquorInspectionPayment();
		$this->data['content'] = 'liquor/retail/completeLiquorInspectionPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	// function displayFirePermitDetailsBuilding(){
	// 	$this->data['title'] = 'Fire Permit Application Details';
	// 	$this->data['fire'] = $this->fire_model->getFireBusinessDetailsBuilding();
	// 	$this->data['content'] = 'fire/firePermitApplicationDetails';
	// 	$this->load->view('include/back_template', $this->data);
	// 	#var_dump($this->health_model->inputFormDetails());
	// }

	// function printFirePermitreceipt($refid){
	// 	$this->fire_model->printFirePermitreceipt($refid);
	// }

	function payRetailLiquorCert(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/retail/payRetailLiquorCert';
		$this->load->view('include/back_template', $this->data);
	}

	function payRetailLiquorCertDetails(){

		$refid = $this->input->post('refid');
		$checkdata = $this->liquor_model->checkLiquorStatus($refid);

		if($checkdata['Status'] == '0'){
			$this->data['title'] = 'Pay';
			$this->data['content'] = 'liquor/retail/payRetailLiquorCertDetails';
			$this->data['liquor'] = $this->liquor_model->payRetailLiquorCertDetails();
			$this->load->view('include/back_template', $this->data);
		} else{
			echo "YOUR CERTIFICATE HAS NOT YE BEEN APPROVED";
			$this->data['title'] = "YOUR CERTIFICATE HAS NOT YE BEEN APPROVED";
			$this->data['content'] = 'liquor/retail/payRetailLiquorCertDetails';
			$this->data['liquor'] = $this->liquor_model->payRetailLiquorCertDetails();
			$this->load->view('include/back_template', $this->data);
		}

	}

	function printRetailLiquorCertDetails(){
		$refid = $this->input->post('refid');
		$checkdata = $this->liquor_model->checkLiquorStatus($refid);

		if($checkdata['Status'] == '0') {

			$this->data['title']   = 'Pay';
			$this->data['content'] = 'liquor/retail/payRetailLiquorCertDetails';
			$this->data['liquor']  = $this->liquor_model->payRetailLiquorCertDetails();
			$this->load->view('include/back_template', $this->data);

		} else {
			echo "YOUR CERTIFICATE HAS NOT YET BEEN APPROVED";
			$this->data['title'] = "YOUR CERTIFICATE HAS NOT YE BEEN APPROVED";
		}
	}


	// Print the retail liquor Certificates, shows the refid form
	function printRetailLiquorCert(){
		$this->data['title']   = 'Pay';
		$this->data['content'] = 'liquor/retail/printRetailLiquor';
		$this->load->view('include/back_template', $this->data);
	}


	// Actual Printing
	function printRetailReceipt(){
		$refid = $this->input->post('refid');
		$this->liquor_model->printLiquorRetailPermitreceipt($refid);
	}

	function completeRetailLiquorCertPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeRetailLiquorCertPayment();
		$this->data['content'] = 'liquor/retail/completeRetailLiquorCertPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}


	// Print Import  form to enter License ID
	function printImportLiquorCert(){
		$this->data['title']   = 'Pay';
		$this->data['content'] = 'liquor/import/printImportLiquor';
		$this->load->view('include/back_template', $this->data);

	}

	// Print Import  form to enter License ID
	function printImportReceipt(){
		$refID = $this->input->post('refid');
		$this->liquor_model->printLiquorImportPermitreceipt($refID);
	}

	// Print Manufacturer  form to enter License ID
	function printManufacturerLiquorCert(){
		$this->data['title']   = 'Pay';
		$this->data['content'] = 'liquor/manufacture/printManufacturerLiquor';
		$this->load->view('include/back_template', $this->data);

	}

	// Print Manufacturer  form to enter License ID
	function printManufacturerReceipt(){
		$refID = $this->input->post('refid');
		$this->liquor_model->printLiquorManufacturerPermitreceipt($refID);
	}



	// Print TemporaryPermit  form to enter License ID
	function printTemporaryLiquorCert(){
		$this->data['title']   = 'Pay';
		$this->data['content'] = 'liquor/temporary/printTemporaryLiquor';
		$this->load->view('include/back_template', $this->data);

	}

	// Print Temporary  form to enter License ID
	function printTemporaryReceipt(){
		$refID = $this->input->post('refid');
		$this->liquor_model->printLiquorTemporaryPermitreceipt($refID);
	}

	// Print TemporaryPermit  form to enter License ID
	function printTransferLiquorCert(){
		$this->data['title']   = 'Pay';
		$this->data['content'] = 'liquor/transfer/printTransferLiquor';
		$this->load->view('include/back_template', $this->data);

	}

	// Print Temporary  form to enter License ID
	function printTransferReceipt(){
		$refID = $this->input->post('refid');
		$this->liquor_model->printLiquorTransferPermitreceipt($refID);
	}

	// function printFireDetails(){
	// 	$this->data['title'] = 'Print';
	// 	$this->data['content'] = 'fire/printFireDetails';
	// 	$this->data['fire'] = $this->fire_model->printFire();
	// 	$this->load->view('include/back_template', $this->data);
	// 	#var_dump($this->health_model->printInstitution());
	// }

	// function printFire($refid){
	// 	$this->fire_model->printFirePermit($refid);
	// }
	function viewImportLiquorTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/import/ImportLiquorConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function importlandingPage(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/import/ImportLiquorLandingPage';
		$this->load->view('include/back_template', $this->data);
	}

	function importLiquorApplicationForm(){
		$this->data['title'] = 'Liquor Permit Application Form';
		$this->data['content'] = 'liquor/import/ImportLiquorApplicationForm';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function displayImportLiquorDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['liquor'] = $this->liquor_model->getImportLiquorDetails();
		$this->data['content'] = 'liquor/import/ImportliquorApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeImportLiquorInspectionPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeImportLiquorInspectionPayment();
		$this->data['content'] = 'liquor/import/completeImportLiquorInspectionPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function payImportLiquorCert(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/import/payImportLiquorCert';
		$this->load->view('include/back_template', $this->data);
	}

	function payImportLiquorCertDetails(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/import/payImportLiquorCertDetails';
		$this->data['liquor'] = $this->liquor_model->payImportLiquorCertDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function completeImportLiquorCertPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeImportLiquorCertPayment();
		$this->data['content'] = 'liquor/import/completeImportLiquorCertPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function viewManufactureLiquorTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/manufacture/ManufactureLiquorConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function manufacturelandingPage(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/manufacture/ManufactureLiquorLandingPage';
		$this->load->view('include/back_template', $this->data);
	}

	function manufactureLiquorApplicationForm(){
		$this->data['title'] = 'Liquor Permit Application Form';
		$this->data['content'] = 'liquor/manufacture/ManufactureLiquorApplicationForm';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function displayManufactureLiquorDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['liquor'] = $this->liquor_model->getManufactureLiquorDetails();
		$this->data['content'] = 'liquor/manufacture/ManufactureliquorApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeManufactureLiquorInspectionPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeManufactureLiquorInspectionPayment();
		$this->data['content'] = 'liquor/manufacture/completeLiquorInspectionPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function payManufactureLiquorCert(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/manufacture/payManufactureLiquorCert';
		$this->load->view('include/back_template', $this->data);
	}

	function payManufactureLiquorCertDetails(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/manufacture/payManufactureLiquorCertDetails';
		$this->data['liquor'] = $this->liquor_model->payManufactureLiquorCertDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function completeManufactureLiquorCertPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeManufactureLiquorCertPayment();
		$this->data['content'] = 'liquor/manufacture/completeManufactureLiquorCertPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function viewTemporaryLiquorTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/temporary/TemporaryLiquorConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function temporarylandingPage(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/temporary/TemporaryLiquorLandingPage';
		$this->load->view('include/back_template', $this->data);
	}

	function temporaryLiquorApplicationForm(){
		$this->data['title'] = 'Liquor Permit Application Form';
		$this->data['content'] = 'liquor/temporary/TemporaryLiquorApplicationForm';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function displaytemporaryLiquorDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['liquor'] = $this->liquor_model->getTemporaryLiquorDetails();
		$this->data['content'] = 'liquor/temporary/TemporaryliquorApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeTemporaryLiquorInspectionPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeTemporaryLiquorInspectionPayment();
		$this->data['content'] = 'liquor/temporary/completeLiquorInspectionPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function payTemporaryLiquorCert(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/temporary/payTemporaryLiquorCert';
		$this->load->view('include/back_template', $this->data);
	}

	function payTemporaryLiquorCertDetails(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/temporary/payTemporaryLiquorCertDetails';
		$this->data['liquor'] = $this->liquor_model->payTemporaryLiquorCertDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function completeTemporaryLiquorCertPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeTemporaryLiquorCertPayment();
		$this->data['content'] = 'liquor/manufacture/completeTemporaryLiquorCertPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function viewTransferLiquorTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'liquor/transfer/TransferLiquorConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	// function temporarylandingPage(){
	// 	$this->data['title'] = 'Terms & Conditions';
	// 	$this->data['content'] = 'liquor/temporary/TemporaryLiquorLandingPage';
	// 	$this->load->view('include/back_template', $this->data);
	// }

	function transferLiquorApplicationForm(){
		$this->data['title'] = 'Liquor Permit Application Form';
		$this->data['content'] = 'liquor/transfer/TransferLiquorApplicationForm';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function displaytransferLiquorDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['liquor'] = $this->liquor_model->getTransferLiquorDetails();
		$this->data['content'] = 'liquor/transfer/TransferliquorApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeTransferLiquorInspectionPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeTransferLiquorInspectionPayment();
		$this->data['content'] = 'liquor/transfer/completeTransferLiquorInspectionPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function payTransferLiquorCert(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/transfer/payTransferLiquorCert';
		$this->load->view('include/back_template', $this->data);
	}

	function payTransferLiquorCertDetails(){
		$this->data['title'] = 'Pay';
		$this->data['content'] = 'liquor/transfer/payTransferLiquorCertDetails';
		$this->data['liquor'] = $this->liquor_model->payTransferLiquorCertDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function completeTransferLiquorCertPayment(){
		$this->data['title'] = 'Liquor Permit Payment Details';
		$this->data['liquor'] = $this->liquor_model->completeTransferLiquorCertPayment();
		$this->data['content'] = 'liquor/transfer/completeTransferLiquorCertPayment';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}


	function invalidRefid() {
		$this->data['title'] = 'Invalid REFID';
		$this->data['content'] = 'liquor/retail/invalidRefid';
		$this->load->view('include/back_template', $this->data);
	}

	function error() {
		$this->data['title'] = 'Invalid REFID';
		$this->data['content'] = 'liquor/retail/error';
		$this->load->view('include/back_template', $this->data);
	}

	// Print Retail Liquor Report
	function printLiquorRetailPermitreceipt($refid) {
		$this->liquor_model->printLiquorRetailPermitreceipt($refid);
	}

	// Print Import Liquor Certificate
	function printLiquorImportPermitreceipt($refid) {
		$this->liquor_model->printLiquorImportPermitreceipt($refid);
	}

	// Print Manufacturer Liquor Certificate
	function printLiquorManufacturerPermitreceipt($refid) {
		$this->liquor_model->printLiquorManufacturerPermitreceipt($refid);
	}

	// Print Temporary Liquor Report
	function printLiquorTemporaryPermitreceipt($refid) {
		$this->liquor_model->printLiquorTemporaryTransferPermitreceipt($refid);
	}


	// Print Transfer Liquor Report
	function printLiquorTransferPermitreceipt($refid) {
		$this->liquor_model->printLiquorTransferPermitreceipt($refid);
	}

	function InvalidBID(){
		$this->data['title'] = 'Invalid BID';
		$this->data['content'] = 'liquor/retail/invalidbid';
		$this->load->view('include/back_template', $this->data);
	}

	
}