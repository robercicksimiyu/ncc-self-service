<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rlb extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('rlb_model');
        $this->load->model('user_model');
	    $this->load->library('session');
    }

    function check_token()
    {
        //print_r($this->rlb_model->GetbuildzoneDetails());exit();
        echo $this->session->userdata('token');
    }

    function getWard(){
        $countyId = $this->input->post('zonecode');
        echo json_encode($this->rlb_model->getWards($countyId));
    }

    function viewTermsRegister()
    {
        $data['title'] = 'Terms & Conditions';
        $data['content'] = 'sbp/conditionsRenew';
        $this->load->view('include/back_template', $data);
    }


    function APIerror()
    {
        $this->data['title'] = 'Regularization Form Details';
        $this->data['content'] = 'rlb/APIerror';
        $this->load->view('include/back_template', $this->data);
    }

    function error()
    {
        $this->data['title'] = 'Error';
        $this->data['content'] = 'rlb/error';
        $this->load->view('include/back_template', $this->data);
    }

    function InvalidRefid()
    {
        $this->data['title'] = 'Print';
        $this->data['content'] = 'rlb/invalidRefid';
        $this->load->view('include/back_template', $this->data);
    }

    function completedForm()
    {
        $this->data['title'] = 'Print';
        $this->data['content'] = 'rlb/structures/printCompleteForm';
        if (isset($_POST)) {
            $data['post_data'] = $_POST;
        }
        $this->load->view('include/back_template', $this->data);
    }

    function printDoc($refid){
        $Form_id = substr($refid, 3, 4);
        $validRefid = substr($refid, 3, 3);
        //Filter by regtype
        if($validRefid != "RF0"){
            redirect("rlb/invalidRefid");
        }elseif ($Form_id == "RF08"){
            //Get building structure details
            $this->rlb_model->printBuildingDoc($refid);
            $this->printForm();
        }else{
            if($Form_id == "RF01" || $Form_id == "RF02" || $Form_id == "RF03"){
                // Get changue of use or extension of use/lease details
                $this->rlb_model->printRegDoc($refid);
                $this->printFormTwo();
            }elseif($Form_id == "RF04" || $Form_id == "RF05" || $Form_id == "RF06" || $Form_id == "RF07") {
                // get amalgamation or subdivision details
                $this->rlb_model->printRegDoc($refid);
                $this->printFormThree();
            }else {
                redirect("rlb/invalidRefid");
            }
        }

    }

    function printForm()
    {
        $this->load->view('rlb/print_pdf');
    }

    function printFormTwo()
    {
       $this->load->view('rlb/print_change_pdf');
    }

    function printFormThree()
    {
       $this->load->view('rlb/print_subdivision_pdf');
    }

    function printReceipt()
    {
    $this->rlb_model->reprintRlbreceipt(
                    $this->input->post("Reference_ID"), $this->input->post("receipt"),
                    $this->input->post("Payee"), $this->input->post("amount"), $this->input->post("form_id")
                );
    }

    function printInvoice($ownerName='',$Reference_ID='', $LrNo='', $Invoice_Number='', $amount='0.00', $form_id ='0')
    {
        $this->rlb_model->printRlbinvoice($ownerName, $Reference_ID, $LrNo, $Invoice_Number, $amount, $form_id );
    }

    function complete_payment(){
        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->rlb_model->completeRegulationPayment();
        $this->data['post_data'] = $this->input->post();
        $this->data['content'] = 'structures/printCompleteForm';
        $this->load->view('include/back_template', $this->data);
    }


    function completion()
    {
        $filter = $this->input->post('completion');
        $this->data['title'] = 'Payment Completed';
        $this->data['content'] = 'rlb/success_one';
        $this->data['complete']= $this->rlb_model->preparePayment();
        //choosing which form to print
        $this->data['others']= ($filter=="sc" || $filter=="ss")? 0 : 1;
        $this->data['others_type'] = $filter;
        if(isset($this->data['complete']["ErrorType"])){
            $this->data['payment_status'] = 0;
            //Load topup instructions when low on funds
            if ($this->data['complete']["ErrorCode"] == 2030){
                $this->data['title'] = 'Top Up Instructions';
                $this->data['content'] = 'rlb/topupInstructions';
              }
        } else {
            $this->data['payment_status'] = 1;
        }
        $this->load->view('include/back_template', $this->data);
    }

    function topup(){
        $this->data['title'] = 'Top Up Instructions';
        $this->data['content'] = 'rlb/topupInstructions';
        $this->load->view('include/back_template', $this->data);
    }

    function pay(){
        $this->data['title'] = 'Choose Regularization';
        $this->data['content'] = 'rlb/conditions';
        $this->load->view('include/back_template', $this->data);
    }

    function payReg(){
        $this->data['title'] = 'Print';
        $this->data['content'] = 'rlb/payReg';
        $this->load->view('include/back_template', $this->data);
    }

    function payRegBuilding(){
        $this->data['title'] = 'Print';
        $this->data['content'] = 'rlb/payRegBuilding';
        $this->load->view('include/back_template', $this->data);
    }

    function payRegDetails(){
        $this->data['title'] = 'Pay Regularization';
        $x = substr(trim($this->input->post('RefId')),0,7);
        if(strcmp($x, 'NCCRF08')==0){
            $this->data['content'] = 'rlb/payRegBuildingDetails';
            $this->data['reg'] = $this->rlb_model->payRegBuildingDetails();
        }else {
            $this->data['content'] = 'rlb/payRegDetails';
            $this->data['reg'] = $this->rlb_model->payRegDetails();
        }
        $this->load->view('include/back_template', $this->data);
    }

    function payRegBuildingDetails(){
        $this->data['title'] = 'Pay Regularization';
        $x = substr(trim($this->input->post('RefId')),0,7);
        if(strcmp($x, 'NCCRF08')==0){
            $this->data['content'] = 'rlb/payRegBuildingDetails';
            $this->data['reg'] = $this->rlb_model->payRegBuildingDetails();
        }else {
            $this->data['content'] = 'rlb/payRegDetails';
            $this->data['reg'] = $this->rlb_model->payRegDetails();
        }
        $this->load->view('include/back_template', $this->data);
    }

    function prepareRegPayment(){
        $this->data['title'] = 'Prepare Payment';
        $this->data['content'] = 'rlb/prepareRegPayment';
        $this->data['reg'] = $this->rlb_model->prepareRegPayment();
        $this->load->view('include/back_template', $this->data);
    }

    function completeRegPayment(){
        $this->data['title'] = 'Complete Payment';
        $this->data['content'] = 'rlb/completeRegDetails';
        $this->data['reg'] = $this->rlb_model->completeRegPayment();
        $this->load->view('include/back_template', $this->data);

    }

    function complete($post_data = null)
    {
        $data['title'] = 'REVIEW DETAILS COMPLETE';
        $data['content'] = 'rlb/success';
        if (isset($post_data)) {
            $data['post_data'] = $post_data;
        }
        $this->load->view('include/back_template', $data);
    }
    function completeDetails($post_data, $callback, $heading)
    {
        $data['title'] = 'REVIEW DETAILS COMPLETE';
        $data['content'] = 'rlb/success';
        $data['callback'] = $callback;
        $data['heading'] = $heading;
	    $data['fields'] = $this->rlb_model->fieldsNames();
        $data['post_data'] = $post_data;
        $data['zones'] = $this->rlb_model->getZoneList();
        $data['subcounties'] = $this->rlb_model->getSubCounties();
        $data['building_zone'] = $this->rlb_model->GetbuildzoneDetails();
        $this->load->view('include/back_template', $data);
    }

    function amalgamation($filter = 'step1', $checksum = null)
    {
        $data['title'] = 'Amalgamation of Land';
        if ($filter == 'step1') {
            $data['title'] = $data['title'].' Step 1';
            $data['zones'] = $this->rlb_model->getZoneList();
            $data['content'] = 'rlb/amalgamation/step1';
        } elseif ($filter == 'step2') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('applicantNames', 'Applicants Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantAddress', 'Applicants Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantProfession', 'Applicants Profession', 'trim|xss_clean');
                $this->form_validation->set_rules('ownerNames', 'Owners Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('ownerAddress', 'Owners Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantInterest', 'Applicants Interest', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('lrNo', 'LR Number', 'trim|xss_clean|required');
                $this->form_validation->set_rules('physicalAddress', 'Physical Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('zone_id', 'Zone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('acreage', 'Acreage', 'trim|xss_clean|required|numeric|callback_zero_check');
                //$this->form_validation->set_rules('applicationNumber', 'Application Number', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->amalgamation('step1');

                    return;
                }
            }
            $data['title'] = $data['title'].' Step 2';
            $data['content'] = 'rlb/amalgamation/step2';
            $data['previousStepData'] = $this->rlb_model->subdivisionStep1();
        } elseif ($filter == 'step3') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('proposedSubdivision', 'Proposed subdivision', 'trim|xss_clean|required');

                $this->form_validation->set_rules('currentUse', 'Original Use', 'trim|xss_clean|required');
                $this->form_validation->set_rules('lastUseDate', 'lastUseDate', 'required|callback_valid_date');
                if (!$this->form_validation->run()) {
                    $this->amalgamation('step2', 1);

                    return;
                }
            }
            $data['title'] = $data['title'].' Step 3';
            $data['content'] = 'rlb/amalgamation/step3';
            $data['previousStepData'] = $this->rlb_model->subdivisionStep2();
        } elseif ($filter == 'complete') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('waterSupply', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('surfaceWaterDisposal', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('sewerageDisposal', 'Sewerage Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('refuseDisposal', 'Refuse Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('easementDetails', 'Easement Details', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->amalgamation('step3', 1);

                    return;
                }
            }
            $this->completeDetails($_POST,'amalgamation','LAND AMALGAMATION');
            return;
        }else if ($filter == 'submit'){
            $data['title'] = 'SUCCESS APPLICATION';
	    $data['content'] = 'rlb/paymentDetails';
	    $data['regDetails'] = $this->rlb_model->postAmalgamation();
        }
        $this->load->view('include/back_template', $data);

        return;
    }

    function building_structures($filter = 'step1', $checksum = null)
    {
        $data['title'] = 'Building Structures';
        if ($filter == 'step1') {
            $data['title'] = 'Building Structures Step 1';
            $data['content'] = 'rlb/structures/step1';
             $data['zones'] = $this->rlb_model->getZoneList();
        } elseif ($filter == 'step2') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_message('min_length[10]', 'field must have a valid phone number.');
                $this->form_validation->set_rules('ownerNames', 'Owners Name', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('ownerEmail', 'Owners Email', 'trim|xss_clean|required|valid_email');
                $this->form_validation->set_rules('ownerPhoneNo', 'Owners Phone No.', 'trim|xss_clean|numeric|required|min_length[10]');
                $this->form_validation->set_rules('ownerPostalAddress', 'Owners Address', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('architectNames', 'Architects Name', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('architectRegNo', 'Architects RegNo', 'trim|xss_clean|required');
                $this->form_validation->set_rules('architectEmail', 'Architects Email', 'trim|xss_clean|required|valid_email');
                $this->form_validation->set_rules('architectPhoneNo', 'Architects Phone No.', 'trim|xss_clean|numeric|required|min_length[10]');
                $this->form_validation->set_rules('architectPostalAddress', 'Architects Address', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('engineerNames', 'Engineers Name', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('engineerRegNo', 'Engineers RegNo', 'trim|xss_clean|required');
                $this->form_validation->set_rules('engineerEmail', 'Engineers Email', 'trim|xss_clean|required|valid_email');
                $this->form_validation->set_rules('engineerPhoneNo', 'Engineers Phone No.', 'trim|xss_clean|numeric|required|min_length[10]');
                $this->form_validation->set_rules('engineerPostalAddress', 'Engineers Address', 'trim|xss_clean|required|callback_zero_check');

                if (!$this->form_validation->run()) {
                    $this->building_structures('step1');

                    return;
                }
            }
            $data['title'] = 'Building Structures Step 2';
            $data['content'] = 'rlb/structures/step2';
            $data['zones'] = $this->rlb_model->getZoneList();
            $data['subcounties'] = $this->rlb_model->getSubCounties();
            $data['previousStepData'] = $this->rlb_model->structuresStep1();
        } elseif ($filter == 'step3') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('currentLandUse', 'Land Use', 'trim|xss_clean|required');
                $this->form_validation->set_rules('zone_id', 'Zone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('projectDetailedDescription', 'Description', 'trim|xss_clean|required');
                $this->form_validation->set_rules('landTenure', 'Land Tenure', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('numberofUnits', 'Units', 'trim|xss_clean|required|numeric|callback_zero_check');
                $this->form_validation->set_rules('lrNo', 'LR Number', 'trim|xss_clean|required');
                $this->form_validation->set_rules('plotSize', 'Size', 'trim|xss_clean|required|numeric|callback_zero_check');
                $this->form_validation->set_rules('nearestRoad', 'Road', 'trim|xss_clean|required');
                $this->form_validation->set_rules('estate', 'Estate', 'trim|xss_clean|required');
                $this->form_validation->set_rules('subcounty', 'SubCounty', 'trim|xss_clean|required');
                $this->form_validation->set_rules('ward', 'Ward', 'trim|xss_clean|required');
                $this->form_validation->set_rules('soilType', 'Soil Type', 'trim|xss_clean|required');

                if (!$this->form_validation->run()) {
                    $this->building_structures('step2', 1);

                    return;
                }
            }
            $data['title'] = 'Building Structures Step 3';
            $data['content'] = 'rlb/structures/step3';
            $data['building_zone'] = $this->rlb_model->GetbuildzoneDetails();
            $data['previousStepData'] = $this->rlb_model->structuresStep2();
        } elseif ($filter == 'complete') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('soilType', 'Soil Type', 'trim|xss_clean|required');
                $this->form_validation->set_rules('waterSupplier', 'Water Supplier', 'trim|xss_clean');
                $this->form_validation->set_rules('sewerageDisposalMethod', 'Sewerage Disposal Method', 'trim|xss_clean');
                $this->form_validation->set_rules('basementArea', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('mezzaninefloorArea', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('floor1Area', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('floor2Area', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('floor3Area', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('floor4Area', 'Area', 'trim|xss_clean|numeric');
                $this->form_validation->set_rules('Others', 'Area', 'trim|xss_clean|required|numeric');
                $this->form_validation->set_rules('totalArea', 'Total Area', 'trim|xss_clean|required|numeric|callback_zero_check');
                $this->form_validation->set_rules('projectCost', 'Project Cost', 'trim|xss_clean|required|numeric|callback_zero_check');
                $this->form_validation->set_rules('inspectionFees', 'Wall Details', 'trim|xss_clean|required|numeric|callback_zero_check');
                $this->form_validation->set_rules('buildingCategory', 'Category', 'trim|xss_clean|required');
                $this->form_validation->set_rules('Foundation', 'Foundation', 'trim|xss_clean|required');
                $this->form_validation->set_rules('externalWalls', 'External Walls', 'trim|xss_clean|required');
                $this->form_validation->set_rules('mortar', 'Mortar', 'trim|xss_clean|required');
                $this->form_validation->set_rules('roofCover', 'Roof Cover', 'trim|xss_clean|required');
                $this->form_validation->set_rules('dampProofCourse', 'Damp Proof Course', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->building_structures('step3', 1);

                    return;
                }
            }
            $this->completeDetails($_POST,'building_structures','BUILDING STRUCTURES');
	    return;
        }else if ($filter == 'submit'){
            $data['title'] = 'SUCCESS APPLICATION';
	    $data['content'] = 'rlb/paymentBuildingDetails';
	    $data['regDetails'] = $this->rlb_model->postBuildingStructures();

        }
        $this->load->view('include/back_template', $data);

        return;
    }

    function change_user($filter = 'step1', $checksum = null)
    {
        $data['title'] = 'Change User';
        if ($filter == 'step1') {
            $data['title'] = 'Change Of User Step 1';
            $data['zones'] = $this->rlb_model->getZoneList();
            $data['content'] = 'rlb/change_user/step1';
        } elseif ($filter == 'step2') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('applicantNames', 'Applicants Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantAddress', 'Applicants Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantProfession', 'Applicants Profession', 'trim|xss_clean');
                $this->form_validation->set_rules('ownerNames', 'Owners Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('ownerAddress', 'Owners Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantInterest', 'Applicants Interest', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('lrNo', 'LR Number', 'trim|xss_clean|required');
                $this->form_validation->set_rules('physicalAddress', 'Physical Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('zone_id', 'Zone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('acreage', 'Acreage', 'trim|xss_clean|required|numeric|callback_zero_check');
                //$this->form_validation->set_rules('applicationNumber', 'Application Number', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->change_user('step1');

                    return;
                }
            }
            $data['title'] = 'Change Of User Step 2';
            $data['content'] = 'rlb/change_user/step2';
            $data['previousStepData'] = $this->rlb_model->changeUserStep1();
        } elseif ($filter == 'step3') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('proposedDevelopment', 'Proposed Development', 'trim|xss_clean|required');

                $this->form_validation->set_rules('currentUse', 'Original Use', 'trim|xss_clean|required');
                $this->form_validation->set_rules('lastUseDate', 'lastUseDate', 'required|callback_valid_date');
                $this->form_validation->set_rules('natureProposedDevelopment', 'Nature Of Proposed Development', 'trim|xss_clean|required');
                $this->form_validation->set_rules('subdivisionInvolved', 'Subdivision Details', 'trim|xss_clean|required');
                if ($this->input->post('subdivisionInvolved') == '1') {
                    $this->form_validation->set_rules('permissionApplied', 'Permissions', 'trim|xss_clean|required');
                    if ($this->input->post('permissionApplied') == '1') {
                        $this->form_validation->set_rules('subdivisionApplicationNo', 'Subdivion Application No', 'trim|xss_clean|required');
                    }
                }
                if (!$this->form_validation->run()) {
                    $this->change_user('step2', 1);

                    return;
                }
            }
            $data['title'] = 'Change Of User Step 3';
            $data['content'] = 'rlb/change_user/step3';
            $data['previousStepData'] = $this->rlb_model->changeUserStep2();
        } elseif ($filter == 'complete') {

            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('landAffected', 'Affected Land', 'trim|xss_clean|required');
                $this->form_validation->set_rules('buildingsCover', 'Buildings Cover %', 'trim|xss_clean|required');
                $this->form_validation->set_rules('currentSiteCover', 'Site Cover %', 'trim|xss_clean|required');
                //$this->form_validation->set_rules('proposedSiteCover', 'Proposed Site Cover %', 'trim|xss_clean|required');
                $this->form_validation->set_rules('wallsDetails', 'Wall Details', 'trim|xss_clean|required');
                $this->form_validation->set_rules('waterSupply', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('surfaceWaterDisposal', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('sewerageDisposal', 'Sewerage Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('refuseDisposal', 'Refuse Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('easementDetails', 'Easement Details', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->change_user('step3', 1);

                    return;
                }
            }
            $this->completeDetails($_POST,'change_user','CHANGE OF USER');
            return;
        }else if ($filter == 'submit'){
            $data['title'] = 'SUCCESS APPLICATION';
    	    $data['content'] = 'rlb/paymentDetails';
    	    $data['regDetails'] = $this->rlb_model->postChangeOfUsers();
        }
        $this->load->view('include/back_template', $data);

        return;
    }

    function extention($filter = 'step1', $checksum = null)
    {
        $data['title'] = 'Extention User';
        if ($filter == 'step1') {
            $data['title'] = 'Extention Of Use Step 1';
            $data['zones'] = $this->rlb_model->getZoneList();
            $data['content'] = 'rlb/extention/step1';
            //$data['step1'] = $this->rlb_model->prepsaveinitialNew();
        } elseif ($filter == 'step2') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('applicantNames', 'Applicants Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantAddress', 'Applicants Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantProfession', 'Applicants Profession', 'trim|xss_clean');
                $this->form_validation->set_rules('ownerNames', 'Owners Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('ownerAddress', 'Owners Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantInterest', 'Applicants Interest', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('lrNo', 'LR Number', 'trim|xss_clean|required');
                $this->form_validation->set_rules('physicalAddress', 'Physical Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('zone_id', 'Zone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('acreage', 'Acreage', 'trim|xss_clean|required|numeric|callback_zero_check');
                //$this->form_validation->set_rules('applicationNumber', 'Application Number', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->change_user('step1');

                    return;
                }
            }
            $data['title'] = 'Extention Of Use Step 2';
            $data['content'] = 'rlb/extention/step2';
            $data['previousStepData'] = $this->rlb_model->changeUserStep1();
        } elseif ($filter == 'step3') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('proposedDevelopment', 'Proposed Development', 'trim|xss_clean|required');

                $this->form_validation->set_rules('currentUse', 'Original Use', 'trim|xss_clean|required');
                $this->form_validation->set_rules('lastUseDate', 'lastUseDate', 'required|callback_valid_date');
                $this->form_validation->set_rules('natureProposedDevelopment', 'Nature Of Proposed Development', 'trim|xss_clean|required');
                $this->form_validation->set_rules('subdivisionInvolved', 'Subdivision Details', 'trim|xss_clean|required');
                if ($this->input->post('subdivisionInvolved') == '1') {
                    $this->form_validation->set_rules('permissionApplied', 'Permissions', 'trim|xss_clean|required');
                    if ($this->input->post('permissionApplied') == '1') {
                        $this->form_validation->set_rules('subdivisionApplicationNo', 'Subdivion Application No', 'trim|xss_clean|required');
                    }
                }
                if (!$this->form_validation->run()) {
                    $this->change_user('step2', 1);

                    return;
                }
            }
            $data['title'] = 'Extention Of Use Step 3';
            $data['content'] = 'rlb/extention/step3';
            $data['previousStepData'] = $this->rlb_model->changeUserStep2();
        } elseif ($filter == 'complete') {

            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('landAffected', 'Affected Land', 'trim|xss_clean|required');
                $this->form_validation->set_rules('buildingsCover', 'Buildings Cover %', 'trim|xss_clean|required');
                $this->form_validation->set_rules('currentSiteCover', 'Site Cover %', 'trim|xss_clean|required');
                //$this->form_validation->set_rules('proposedSiteCover', 'Proposed Site Cover %', 'trim|xss_clean|required');
                $this->form_validation->set_rules('wallsDetails', 'Wall Details', 'trim|xss_clean|required');
                $this->form_validation->set_rules('waterSupply', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('surfaceWaterDisposal', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('sewerageDisposal', 'Sewerage Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('refuseDisposal', 'Refuse Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('easementDetails', 'Easement Details', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->change_user('step3', 1);

                    return;
                }
            }
            $this->completeDetails($_POST,'extention','EXTENTION OF USER');
            return;
        }else if ($filter == 'submit'){
            $data['title'] = 'SUCCESS APPLICATION';
    	    $data['content'] = 'rlb/paymentDetails';
    	    $data['regDetails'] = $this->rlb_model->postExtentionOfUsers();
        }
        $this->load->view('include/back_template', $data);

        return;
    }


    function subdivision($type = 'land', $filter = 'step1', $checksum = null)
    {
        $data['title'] = 'Subdivision';
        if ($type == 'large_schemes') {
            $data['pagetype'] = 'Large Schemes Subdivision';
            $data['no_plots'] = 'show';
            $data['callbackUrl'] = 'rlb/subdivision/large_schemes';
        } elseif ($type == 'ammend') {
            $data['pagetype'] = 'Amend Subdivision';
            $data['callbackUrl'] = 'rlb/subdivision/ammend';
        } else {
            $data['pagetype'] = 'Land Subdivision';
            $data['callbackUrl'] = 'rlb/subdivision/land';
        }

        if ($filter == 'step1') {
            $data['title'] = $data['pagetype'].' Step 1';
            $data['zones'] = $this->rlb_model->getZoneList();
            $data['content'] = 'rlb/subdivision/step1';
        } elseif ($filter == 'step2') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('applicantNames', 'Applicants Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantAddress', 'Applicants Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantProfession', 'Applicants Profession', 'trim|xss_clean');
                $this->form_validation->set_rules('ownerNames', 'Owners Name', 'trim|xss_clean|required');
                $this->form_validation->set_rules('ownerAddress', 'Owners Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('applicantInterest', 'Applicants Interest', 'trim|xss_clean|required|callback_zero_check');
                $this->form_validation->set_rules('lrNo', 'LR Number', 'trim|xss_clean|required');
                $this->form_validation->set_rules('physicalAddress', 'Physical Address', 'trim|xss_clean|required');
                $this->form_validation->set_rules('zone_id', 'Zone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('acreage', 'Acreage', 'trim|xss_clean|numeric|required|callback_zero_check');
                //$this->form_validation->set_rules('applicationNumber', 'Application Number', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->subdivision($type, 'step1');

                    return;
                }
            }
            $data['title'] = $data['pagetype'].' Step 2';
            $data['content'] = 'rlb/subdivision/step2';
            $data['previousStepData'] = $this->rlb_model->subdivisionStep1();
        } elseif ($filter == 'step3') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('proposedSubdivision', 'Proposed subdivision', 'trim|xss_clean|required');
                /*if ($type == 'large_schemes') {
                    $this->form_validation->set_rules('noofSubPlots ', 'No of Sub Plots', 'trim|xss_clean|required|numeric');
                }*/
                $this->form_validation->set_rules('currentUse', 'Original Use', 'trim|xss_clean|required');
                $this->form_validation->set_rules('lastUseDate', 'lastUseDate', 'required|callback_valid_date');
                if (!$this->form_validation->run()) {
                    $this->subdivision($type, 'step2', 1);

                    return;
                }
            }
            $data['title'] = $data['pagetype'].' Step 3';
            $data['content'] = 'rlb/subdivision/step3';
            $data['previousStepData'] = $this->rlb_model->subdivisionStep2();
        } elseif ($filter == 'complete') {
            if ($checksum == null) { // Validate step1 data if checksum = null
                $this->form_validation->set_rules('waterSupply', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('surfaceWaterDisposal', 'Water Supply', 'trim|xss_clean|required');
                $this->form_validation->set_rules('sewerageDisposal', 'Sewerage Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('refuseDisposal', 'Refuse Disposal', 'trim|xss_clean|required');
                $this->form_validation->set_rules('easementDetails', 'Easement Details', 'trim|xss_clean|required');
                if (!$this->form_validation->run()) {
                    $this->subdivision($type, 'step3', 1);

                    return;
                }
            }
            if ($type == 'large_schemes') {
                $this->completeDetails($_POST,'subdivision/large_schemes','SUBDIVISION OF LARGE SCHEMES LAND');
            } elseif ($type == 'ammend') {
                $this->completeDetails($_POST,'subdivision/ammend','AMENDMENT OF LAND');
            } elseif($type == 'land') {
                $this->completeDetails($_POST,'subdivision/land','LAND SUBDIVISION');
            }
            return;
        }else if ($filter == 'submit'){
            $data['title'] = 'SUCCESS APPLICATION';
	    $data['content'] = 'rlb/paymentDetails';
            if ($type == 'large_schemes') {
                $data['regDetails'] = $this->rlb_model->postSubdivisionLargeSchemes();
            } elseif ($type == 'ammend') {
                //Ammendment post
            } elseif($type == 'land') {
                $data['regDetails'] = $this->rlb_model->postLandSubdivison();
            }
        }

        $this->load->view('include/back_template', $data);
    }

    function zero_check($str)
    {
        if ($str == '0') {
            $this->form_validation->set_message('zero_check', 'The %s field can not be zero');

            return false;
        } else {
            return true;
        }
    }

    function valid_date($date)
    {
        $pattern = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';

        if (preg_match($pattern, $date)) {
            return true;
        } else {
            $this->form_validation->set_message('valid_date', ' %s in dd/md/yyyy');

            return false;
        }
    }

}
