
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Health extends CI_Controller {
	public $data;

	function __construct() {
		parent::__construct();
		$this->load->model('health_model');
		$this->load->model('user_model');
		#$this->load->library('email');
	}

	function sendMail(){
  //       // $this->email->from('samsoft12@gmail.com', 'Samuel');
  //       // $this->email->to('samsoft12@gmail.com');
  //       // #$this->email->cc('another@another-example.com');
  //       // #$this->email->bcc('them@their-example.com');
  //       // $this->email->subject('Email Test');
  //       // $this->email->message('Testing the email class.');
  //       // $this->email->send();

  //       // echo $this->email->print_debugger();
		// $admin_email = "samsoft12@gmail.com";
		// $email = "samsoft12@gmail.com";
		// $subject = "Test";
		// $comment = "Test";

  // //send email
		// mail($admin_email, "$subject", $comment, "From:" . $email);
	}

	function foodHygieneApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enter_biz_id';
		$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function getBusinessDetails(){
		var_dump($this->data['health'] = $this->health_model->getSubcounties());
	}

	function getWards(){
		$this->health_model->getWards();
	}

	function displayHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getHygieneBusinessDetails();
		$this->data['content'] = 'health/foodHygieneApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeFHPayment();
		$this->data['content'] = 'health/completeHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function InstitutionHealthApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enterBID';
		$this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
	}

	function displayInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getInstitutionBusinessDetails();
		$this->data['content'] = 'health/foodHygieneInstitutionApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeIHPayment();
		$this->data['content'] = 'health/completeInstitutionHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function printHealthPermit($refid){
		$this->health_model->printHealthPermit($refid);
	}

	function printHealthPermitreceipt($refid){
		$this->health_model->printHealthPermitreceipt($refid);
	}

	function printInstitutionHealthPermit($refid){
		$this->health_model->printInstitutionHealthPermit($refid);
	}

	function printInstitutionHealthPermitreceipt($refid){
		$this->health_model->printInstitutionHealthPermitreceipt($refid);
	}

	function printInstitution(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printInstitution';
		$this->load->view('include/back_template', $this->data);
	}

	function printInstitutionD(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printDetails';
		$this->data['health'] = $this->health_model->printInstitution();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function printHygiene(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printHygiene';
		$this->load->view('include/back_template', $this->data);
	}

	function payHygiene(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payHygiene';
		$this->load->view('include/back_template', $this->data);
	}

	function payHygieneDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payHygieneDetails';
		$this->data['health'] = $this->health_model->payHygieneDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function payInstitution(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payInstitution';
		$this->load->view('include/back_template', $this->data);
	}

	function payInstitutionDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payInstitutionDetails';
		$this->data['health'] = $this->health_model->payInstitutionDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function printHygieneD(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printHygieneDetails';
		$this->data['health'] = $this->health_model->printHygiene();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function error(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/error';
		$this->load->view('include/back_template', $this->data);
	}

	function invalidBID(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidBID';
		$this->load->view('include/back_template', $this->data);
	}

	function APIerror(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/APIerror';
		$this->load->view('include/back_template', $this->data);
	}

	function invalidRefid(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidRefid';
		$this->load->view('include/back_template', $this->data);
	}

}