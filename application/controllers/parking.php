<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parking extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #Accesscontrol_helper::is_logged_in();
        $this->load->model('parking_model');
        $this->load->model('sbp_model');
        $this->load->model('user_model');
    }

    function allDParking(){
        $this->data['title'] = 'Parking Transactions';
        $this->data['trans'] = $this->parking_model->alljpparkingtransactions();
        $this->data['content'] = 'admin/parkingtransactions';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->land_rates_model->allLandrates());
    }

    function parkingPenalties(){
        $this->data['title'] = 'Select Penalty Type';
        $this->data['parkingpenalty'] = $this->parking_model->prepareDailyParkingInitNew();
        $this->data['content'] = 'parking/select_penalty';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->parking_model->prepareDailyParkingInit());
    }

    function parkingTopup(){
        $this->data['title'] = 'Topup Parking Fee';
        $this->data['parkingtopup'] = $this->parking_model->prepareDailyParkingInitNew();
        $this->data['content'] = 'parking/topup';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->parking_model->prepareDailyParkingInit());
    }

    function displayPenaltyDetails(){
        $this->data['title'] = 'Display Penalty Details';
        $this->data['penalty'] = $this->parking_model->prepareDailyParkingPaymentNew();
        $this->data['content'] = 'parking/display_penalty_details';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->parking_model->prepareDailyParkingPayment());
    }

    function daily_parking(){
    	$this->data['title'] = 'Select Location & Vehicle Size';
        $this->data['parking'] = $this->parking_model->prepareDailyParkingInitNew();
    	$this->data['content'] = 'parking/enter_car_reg_daily';
    	$this->load->view('include/back_template', $this->data);
    }

    function display_details_daily(){
        $this->data['title'] = 'Car Details';
        $this->data['prepare'] = $this->parking_model->prepareDailyParkingPaymentNew();
    	$this->data['content'] = 'parking/display_details_daily';
        $this->load->view('include/back_template', $this->data);
        //var_dump($this->parking_model->prepareParkingPayment());
    }

    function complete_payment_daily(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->parking_model->completeDailyParkingNew();
        $this->data['content'] = 'parking/complete_payment_daily';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->parking_model->completeDailyParking());
    }

    function completeChargesPayment(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->parking_model->completeDailyParkingNew();
        $this->data['content'] = 'parking/completeChargesPayment';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->complete());
    }

    function reprintDailyReceipt(){
        $this->data['title'] = 'Car Details';
        $this->data['content'] = 'parking/reprintDreceipt';
        $this->load->view('include/back_template', $this->data);
    }

    function reprintPenaltyReceipt(){
        $this->data['title'] = 'Car Details';
        $this->data['content'] = 'parking/reprintPreceipt';
        $this->load->view('include/back_template', $this->data);
    }

    function CreprintUnavailable(){
        $this->data['title'] = 'Receipt Details Unavailable';
        $this->data['content'] = 'parking/Creceiptunavailable';
        $this->load->view('include/back_template', $this->data);
    }

    function tests(){
        $this->data['parking'] = $this->parking_model->prepareSeasonalParkingInitNew();
        var_dump($this->data['parking']);
    }

    function seasonal_parking(){
        $this->data['title'] = 'Select Duration & Vehicle Size';
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['parking'] = $this->parking_model->prepareSeasonalParkingInitNew();
        #var_dump($this->data['parking']);
        $this->data['content'] = 'parking/enter_car_reg_seasonal';
        $this->load->view('include/back_template', $this->data);
    }

    function confirmSPParking(){
        $data['title'] = 'Parking';
        $data['content'] = 'parking/confirmSPParking';
        $data['bal'] = $this->user_model->viewBalance1();
        $data['parking']=$this->parking_model->confirmSPParking();
        $this->load->view('include/back_template', $data);
    }

    function completeSPPayment(){
        $data['title'] = 'Parking';
        $data['content'] = 'parking/completeSPPayment';
        $data['confirm']=$this->parking_model->completeSPPaymentNew();
        $this->load->view('include/back_template', $data);
        #var_dump($this->parking_model->completeSPPayment());
    }

    function display_details_seasonal(){
        $this->data['title'] = 'Car Details';
        $this->data['prepare'] = $this->parking_model->prepareSeasonalParkingPayment();
        $this->data['content'] = 'parking/display_details_seasonal';
        $this->load->view('include/back_template', $this->data);
        //var_dump($this->parking_model->prepareParkingPayment());
    }

    function reprintUnavailable(){
        $this->data['title'] = 'Receipt Details Unavailable';
        $this->data['content'] = 'parking/receiptunavailable';
        $this->load->view('include/back_template', $this->data);
    }

    function complete_payment_seasonal(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->parking_model->completeSeasonalParking();
        $this->data['content'] = 'parking/complete_payment_seasonal';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->complete());
    }

   
    function getVehiclesDetails(){
        $this->parking_model->getVehiclesDetails();
    }

    function countVehicles(){
        $this->parking_model->countVehicles();
    }
    function getVehiclesSum(){
        $this->parking_model->getVehiclesSum();
    }

    function addVehicle(){
        $this->parking_model->addVehicleNew();
    }

    function resetVehicle(){
        $this->parking_model->resetVehicle();
    }

    function printSeasonal(){
        $this->parking_model->printSeasonal();
    }

    function printDreceipt(){
        $this->parking_model->printDreceiptNew();
    }

    function reprintPreceipt(){
        $this->parking_model->printPenaltyReceipt();
    }

    function printCreceipt(){
        $this->parking_model->printCreceipt();
    }

    function removeSRecord(){
         $this->parking_model->removeSRecord();
    }

    function reprintDreceipt(){
        $this->parking_model->reprintDreceiptNew();
    }

    function reprintManualDReceipt(){
        $this->data['title'] = 'Car Details';
        $this->data['content'] = 'parking/reprintManualDreceipt';
        $this->data['parking'] = $this->parking_model->prepareDailyParkingInit();
        $this->load->view('include/back_template', $this->data);
    }

    function reprintManualSReceipt(){
        $this->data['title'] = 'Car Details';
        $this->data['content'] = 'parking/reprintManualSreceipt';
        $this->data['parking'] = $this->parking_model->prepareSeasonalParkingInit();
        $this->load->view('include/back_template', $this->data);
    }

    function reprintManualDReceiptError(){
        $this->data['title'] = 'Car Details';
        $this->data['content'] = 'parking/reprintDetailsSaved';
        $this->load->view('include/back_template', $this->data);
    }

    function insertManualDReceipt(){
       var_dump($this->parking_model->insertReceiptDetails()) ;
    }

    function insertManualSReceipt(){
        $this->parking_model->insertSReceiptDetails();
    }

    /*function testManualStaff()
    {
        $token=$this->session->userdata('token');  

        $username= $this->session->userdata('jpwnumber');
        $issuedate='2016-06-08';
        $regno='KBN371Y';
        
        $stream = "parking";
        $key0 = "PlateNumber";
        $value0 = $regno;
        $key1 = "TransactionStatus";
        $value1 = "1";
        $key2 = "StartDate";
        $value2 = $issuedate;
        $key3 = "EndDate";
        $value3 = $issuedate;

        $url = REST_URL."gettransactions?stream=$stream&[0].Key=$key0&[0].Value=$value0&[1].Key=$key1&[1].Value=$value1";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        #curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer '.$token,
            'app_key: '.APP_KEY
        ));
        $result = curl_exec( $ch );        
        $res = json_decode($result);
        var_dump($res);
    }*/


}