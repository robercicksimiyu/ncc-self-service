<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Garbage extends CI_Controller {
    public $data;

function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('gabage_model');
        $this->load->model('user_model');
        $this->load->model('sbp_model');
    }

    function index(){
        $this->data['title'] = 'Gabage Collection';        
        $this->data['content'] = 'gabage/form_information';
        $this->data['categories'] = $this->gabage_model->get_categories();
        $this->data['subcounties'] = $this->sbp_model->getBusinessZones();        
        $this->load->view('include/back_template', $this->data); # var_dump($this->land_rates_model->preparePayment());
    }

    public function sub_categories($category_id)
    {
        echo json_encode($this->gabage_model->get_sub_categories($category_id));
        return;
    }

    public function confirm()
    {
        //var_dump($this->input->post());exit;
        $this->form_validation->set_rules('bid','Business No','required');       
        $this->form_validation->set_rules('id_number','ID Number','required');
        if($this->form_validation->run()){
           
            $this->data['invoice_details']=$this->gabage_model->save_business_details($this->input->post('bid'));          
            
            if(count($this->data['invoice_details'])) {
                 $this->data['title'] = 'Gabage Collection';
                 $this->data['subcounties'] = $this->sbp_model->getBusinessZones();        
                 $this->data['content'] = 'gabage/displayInvoiceDetails';
                 $this->load->view('include/back_template', $this->data);
                return;
            }
            
        }
        //$this->session=->set_flashdata
        redirect('garbage');
        return;
    }

    function get_business_details($bid)
    {      
        echo $this->gabage_model->get_business_details($bid);
        return;
    }

    function renewal()
    {
        $this->form_validation->set_rules('bizId','Business Number','required');
        if($this->form_validation->run()) {
            $this->data['invoice_details']=$this->gabage_model->renewal();
            //var_dump($this->data);exit;
             $this->data['title'] = 'Gabage Collection';        
             $this->data['content'] = 'gabage/displayInvoiceDetails';
             $this->load->view('include/back_template', $this->data);
            return;
        }
        $this->data['title'] = 'Gabage Collection';        
        $this->data['content'] = 'gabage/renewal';
        $this->load->view('include/back_template', $this->data);
    }

    function payment()
    {
        $this->data['trans_details']=$this->gabage_model->make_payments();        
         $this->data['title'] = 'Gabage Collection'; 
         $this->data['content'] = 'gabage/payment_complete';
         $this->load->view('include/back_template', $this->data); 
    }


    function printreceipt($invoice_number=null)
    {
        $this->form_validation->set_rules('bid','Business Number','required');
        if($this->form_validation->run()){
            $this->gabage_model->printReceipt();
        } else if($invoice_number!=null) {            
            $this->gabage_model->printReceipt($invoice_number);
        }        
        $this->data['trans_details']=$this->gabage_model->make_payments();        
        $this->data['title'] = 'Gabage Collection'; 
        $this->data['content'] = 'gabage/payment_complete';
        $this->load->view('include/back_template', $this->data); 
    }



    function displayConstructionDetails(){
        $this->data['title'] = 'Display Construction Details';
        $this->data['construction'] = $this->econstruction_model->prepareConstructionNew();
        $this->data['content'] = 'econstruction/displayInvoiceDetails';
        $this->load->view('include/back_template', $this->data);#var_dump($this->econstruction_model->prepareConstruction());
    }

    function completeConstructionPayment(){
        $this->data['title'] = 'Complete Construction Payment';
        $this->data['construction'] = $this->econstruction_model->completeConstructionPayment();
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['content'] = 'econstruction/completeConstructionPayment';
        $this->load->view('include/back_template', $this->data);  #var_dump($this->econstruction_model->completeConstructionPayment());
    }


    function reprintECReceipt(){
        $data['title'] = 'E-Construction';
        $data['content'] = 'econstruction/reprintECReceipt';
        $this->load->view('include/back_template', $data);
    }

    function checkECReceipt(){
        $rno=$this->econstruction_model->checkECprintReceipt();
        $this->econstruction_model->printEreceiptsearch($rno);
    }

    function econstructionreceiptprint($receiptno){
        $this->econstruction_model->PrintEConstruction($receiptno);
    }

    function test_smt()
    {
        $this->load->model('business');
        var_dump($this->business->get_business_sub_classes(1));
    }


}
