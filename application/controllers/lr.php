<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lr extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('land_rates_model');
        $this->load->model('user_model');

    }

    function allLandrates(){
        $this->data['title'] = 'Parking Transactions';
        $this->data['landrates'] = $this->land_rates_model->allLandrates();
        $this->data['content'] = 'admin/allLandrates';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->land_rates_model->allLandrates());
    }

    function enter_plot_no(){
    	$this->data['title'] = 'Enter Plot Number';
    	$this->data['content'] = 'lr/enter_plot_no';
    	$this->load->view('include/back_template', $this->data);
    }

    function display_plot_details(){
        $this->data['title'] = 'Plot Details';
        $this->data['datax'] = $this->land_rates_model->preparePaymentNew();
    	$this->data['content'] = 'lr/display_plot_details';
        $this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
    }

    function confirm_land_rates(){


        $data['title'] = 'Land Rate Details';
        $data['content'] = 'en/confirm_land_rates';
        $data['confirm']=$this->land_rates_model->Lprepare();
        $this->load->view('include/back_template', $data);
        
    }

    function confirm_details(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'lr/confirm_payment';
        $data['confirm']=$this->land_rates_model->confirm_payment();
        $this->load->view('include/back_template', $data);
    }

    function complete_payment(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->land_rates_model->completeNew();
        $this->data['content'] = 'lr/complete_payment';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->land_rates_model->complete());
    }
    
    function is_logged_in() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('admin/login/view');
        }
    }

    function return_result(){
        $this->data['title'] = 'Return';
        $this->data['content'] = 'sbp/return';
        $this->load->view('include/back_template',$this->data);
    }

    function send_mail(){
        $this->data['title'] = 'Email';
        $this->data['content'] = 'sbp/mail_template';
        $this->load->view('include/back_template',$this->data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('admin/login/view');
    }

    function search(){
        $this->data['title'] = 'Search';
        $this->data['content'] = 'lr/enter_receipt_no';
        $this->load->view('include/back_template',$this->data);
    }

    function display_search_details(){
        $this->data['title'] = 'Display';
        $this->data['search'] = $this->land_rates_model->PrepareSearchPayment();
        $this->data['content'] = 'lr/confirm_search';
        $this->load->view('include/back_template',$this->data);
        #var_dump($this->land_rates_model->PrepareSearchPayment());
    }

    function complete_search_payment(){
        $this->data['title'] = 'Display';
        $this->data['search'] = $this->land_rates_model->CompleteSearchPayment();
        $this->data['content'] = 'lr/search_results';
        $this->load->view('include/back_template',$this->data);
        //var_dump($this->land_rates_model->CompleteSearchPayment());
    }

    function printlandreceipt(){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =dirname(__FILE__).'\receipt.pdf';

        $receiptno=$this->session->flashdata('receiptnum');
        $date=date('Y-m-d',time()); 
        $phoneno=$this->session->flashdata('mobile');
        $total_amount_due=$this->session->flashdata('amount_due');
        #$balance_due=$this->session->flashdata('balance');
        $plot_owner=$this->session->flashdata('plot_owner');
        #$cashier=$this->session->userdata('name');

        $amount=$this->session->flashdata('kshs');
        $amount = str_replace( ',', '', $amount);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY**";
        }else $amount_in_words="NOT AVAILABLE";

        $description= "LAND RATES, PLOT NUMBER ".$this->session->flashdata('plotno'); 


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

        $page->drawText($receiptno, 150, 548);
        $page->drawText($date, 480, 548);
        $page->drawText($phoneno, 250, 505); 
        $page->drawText($amount, 480, 505);
        $page->drawText($amount_in_words, 150, 470); 
        $page->drawText($description, 150, 437);
        $page->drawText($plot_owner, 170, 403);
        #$page->drawText($cashier, 127, 13);
        $page->drawText($total_amount_due, 450, 375);
        $page->drawText($amount, 450, 345);
        #$page->drawText($balance_due, 450, 315);   

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>dirname(__FILE__).'\SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 555,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        redirect('lr/enter_plot_no');
    }

   function landRsearchdetails(){

        $data['title'] = 'Land Search Details';
        $data['content'] = 'lr/reprintlr';
        $data['confirm']=$this->land_rates_model->CompleteSearchPayment();
        //$data['bal']=$this->wallet_model->floatbal();
        $this->load->view('include/back_template', $data);
    }

    function checklandreceipt(){
        $rno=$this->land_rates_model->checkLprintReceiptNew();
        $this->landreceiptprint($rno);
    }

    function landreceiptprint($receiptno){
        $this->land_rates_model->lprintlandreceipt($receiptno);
    }

    function printdirect($receiptno){
        $this->land_rates_model->printlandreceiptNew($receiptno);
    }

    function printreceiptmanual(){
        $this->land_rates_model->pdf4();
    }

    function ManualLandReceiptInsert(){

        $data['title'] = 'Land Search Details';
        $data['content'] = 'lr/reprintManualLandReceipt';
        #$data['confirm']=$this->land_rates_model->CompleteSearchPayment();
        //$data['bal']=$this->wallet_model->floatbal();
        $this->load->view('include/back_template', $data);
    }

    function insertManualReceipt(){
        $this->land_rates_model->insertReceiptDetails();
    }

}