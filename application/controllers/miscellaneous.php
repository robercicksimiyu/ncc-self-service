<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Miscellaneous extends CI_Controller {
    public $data;

function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('miscellaneous_model');
        $this->load->model('user_model');
    }

    function index(){
        $this->data['title'] = 'Miscellaneous';
        $this->data['content'] = 'miscellaneous/selectMisc';
        $this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
    }

    function displayMiscDetails(){
        $this->data['title'] = 'Display Construction Details';
        $this->data['misc'] = $this->miscellaneous_model->preparemiscellaneous();
        $this->data['content'] = 'miscellaneous/displayMiscDetails';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->econstruction_model->prepareConstruction());
    }

    function completeMiscPayment(){
        $this->data['title'] = 'Complete Miscellaneous Payment';
        $this->data['misc'] = $this->miscellaneous_model->completeMiscellaneousPayment();
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['content'] = 'miscellaneous/completeMiscPayment';
        $this->load->view('include/back_template', $this->data);
    }

    function printECReceipt(){
        $this->econstruction_model->PrintEConstruction();
    }

    function printReceipt(){
        $data['title'] = 'Miscellaneous';
        $data['content'] = 'miscellaneous/printReceipt';
        $this->load->view('include/back_template', $data);
    }

    function checkMiscReceipt(){
        $billno=$this->input->post('billno');
        $this->miscellaneous_model->printmiscellaneousreceiptNew($billno);
    }

    function miscellaneousreceiptprint($billno){
        $this->miscellaneous_model->printmiscellaneousreceiptNew($billno);
    }



}