<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
    }

	function get_wards(){        
        $countyId = $this->input->post('zone');        
        echo json_encode($this->common_model->get_wards($countyId));
    }

    function requirements($type)
    {
        $this->data['Title']="Requirements";
        if($type=="health") {
            $this->data['content']='sbp/health_requirements';
        } elseif($type=="fire") {
             $this->data['content']='sbp/fire_requirements';
        }
        $this->load->view('include/back_template', $this->data);
    }
    
    function test_new_token(){
         $tosend = array(
          'grant_type'=>"wallet",
          'username'=>"254712633277",
          'password' => '1111'    
        );

        foreach ( $tosend as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode ('&', $post_items);

        $url = "http://192.168.6.10/JamboPayServices/token";
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        var_dump($res->access_token);
    }


	/*function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('sbp_model');
        $this->load->model('user_model');
    }*/


    // TEST SBP ADDS
    function testSBP(){
        $this->data['title'] = 'Terms & Conditions';
        $this->data['content'] = 'sbp/testsbp';
        $this->data['data'] = $this->sbp_model->getSubCounties();
        $this->load->view('include/back_template', $this->data);
        
    }
    
   

    function getSubClasses(){
         $classId = $this->input->post('business_classes');
          echo json_encode($this->sbp_model->getBusinessSubClasses($classId));
    }

    function getWard(){
        $countyId = $this->input->post('zonecode');
        echo json_encode($this->sbp_model->getWards($countyId));
    }

    
    function viewTerms($type=""){
        $this->data['title'] = 'Terms & Conditions';
        $this->data['type'] = $type;
        $this->data['content'] = 'sbp/conditions';
        $this->load->view('include/back_template', $this->data);
    }

    function viewTermsRegister(){
        $this->data['title'] = 'Terms & Conditions';
        $this->data['content'] = 'sbp/conditionsRenew';
        $this->load->view('include/back_template', $this->data);
    }

    function wallettopup(){
        $this->data['title'] = 'Business ID';
        $this->data['content'] = 'sbp/check_bal';
        $this->data['xml'] =$this->sbp_model->top();
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->top());
    }

    // SBP topup
    function sbp_topup_change(){
        $this->details = $this->sbp_model->sbp_topup_init();
        $this->data['title'] = 'SBP TopUp';
        $this->data['businesses'] = $this->details['bizactivity']; 
        $this->data['content'] = 'sbp/topup';
        $this->load->view('include/back_template', $this->data);
    }

    function sbp_topup_prepare(){
        $this->data['changeDetails'] = $this->sbp_model->sbp_topup_change_api();
        $this->data['title'] = 'Business Details';
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['datax'] = $this->sbp_model->preparePaymentNew();
        $this->data['zones'] =$this->sbp_model->prepsaveinitialNew();
        $this->data['counties'] = $this->sbp_model->getSubCounties();
        $this->data['content'] = 'sbp/display_biz_details';
        $this->load->view('include/back_template', $this->data);
        // $this->data['content'] = 'sbp/topup_return'; 
        // $this->load->view('include/back_template', $this->data);
    }



    function new_login(){
        $this->data['title'] = 'Login';
        $this->data['content'] = 'en/register';
        $this->load->view('include/no_template', $this->data);
    }

    function get_wallet_bal(){
        $data['title'] = 'View Balance';
        $data['bal'] = $this->user_model->show_bal();
        $data['content'] = 'sbp/balance_form';
        $this->load->view('include/back_template', $data);
        #var_dump($data['bal']);
    }   
  
    function enter_acc_no(){
        #$this->is_logged_in();
    	$this->data['title'] = 'Business ID';
    	$this->data['content'] = 'sbp/enter_biz_id';
    	$this->load->view('include/back_template', $this->data);
    }

    function display_business_details(){
        $this->data['title'] = 'Business Details';

        $this->data['bal'] = $this->user_model->viewBalance1(); 
        if($this->input->post('year') > 2016){
			 $this->data['datax'] = $this->sbp_model->getBusinessDetails($this->input->post('biz_id'),$this->input->post('year'));
			 //var_dump($this->data);exit;
             //$this->data['datax'] = $this->sbp_model->testpreparePaymentNew();
              $this->data['business_classes'] = $this->sbp_model->getBusinessClasses($this->data['datax']['ActivityID']); 
        } else {            
             $this->data['datax'] = $this->sbp_model->preparePaymentNew();             
        }  
        //exit;
        //$this->data['datax'] = $this->sbp_model->testpreparePaymentNew();
       
       // var_dump($this->data['business_classes']);exit;
        $this->data['datax']['advert_fee'] =4200;
        $this->data['zones'] =$this->sbp_model->testprepsaveinitialNew();        
    	$this->data['content'] = 'sbp/display_biz_details';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->preparePayment());
    }

    function complete_payment(){
        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->sbp_model->completeNew();
        $this->data['content'] = 'sbp/payment_complete';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->complete());
    }

    function generatebarcode($businessId){
        #$theCode ="1254789";
        $this->load->library('zend');
        #$rendererOptions = array();
        $this->zend->load('Zend/Barcode');
        $image= Zend_Barcode::render('code128', 'image', array('text' => $businessId), array());
        #var_dump($image);
        @$data['bc'] = imagejpeg($image,"barcodes/".$businessId.".jpg", 100);
        $data['content'] = 'sbp/bar';
        $this->load->view('include/back_template', $data);
    }

    function qrcode($receiptno){
        $this->load->library('ciqrcode');
        header("Content-Type: image/png");
        $params['data'] = $receiptno;
        #$params['size'] = 100;
        #$params['savename'] ='qrcode/image.png';
        $data['qr'] = $this->ciqrcode->generate($params);
        $data['content'] = 'sbp/bar';
        $this->load->view('include/no_template', $data);
    }

    function sbp_print(){
        $data['title'] = 'Print';
        $data['content'] = 'sbp/print_sbp1';
        $this->load->view('include/back_template', $data);
    }

    function print_sbp(){
        #$this->is_logged_in();
        $data['title'] = 'Print Business Permit';
        $data['sbp'] =$this->sbp_model->print_sbp();
        $data['datax'] = $this->sbp_model->preparePayment();
        $data['content'] = 'sbp/print_sbp';
        $this->load->view('include/back_template', $data);
    }

    function sbp_print_preview(){
        $data['title'] = 'Print';
        $data['content'] = 'sbp/print_sbp1';
        $this->load->view('include/back_template', $data);
    }

    function print_sbp_preview($businessId){
        #$this->is_logged_in();
        $this->load->library('pdf');
        $this->data['title'] = 'Print Business Permit';
        $this->data['content'] = 'sbp/print_pdf';
        $this->data['sbp'] =$this->sbp_model->print_sbp_preview($businessId);

        $this->pdf->load_view('include/no_template', $this->data);
        $this->pdf->render();
        #$dompdf->load_html();
        $this->pdf->stream('NCC_SBP.pdf',array("Attachement"=>false));
    }

    function confirm_payment(){
                
        $this->data['isIn'] = $this->sbp_model->confirmPlotCounty();
        $this->data['sbp'] = $this->sbp_model->confirm();
        $this->data['content'] = 'sbp/confirm_payment';
        $this->load->view('include/back_template', $this->data);        
    }

    function confirm_payment2(){

        var_dump($this->input->post());exit;
        $this->data['title'] = 'Confirm Payment';
        $this->data['content'] = 'sbp/complete_payment';
        $this->load->view('include/back_template', $this->data);
    }

    function not_found(){

         $data['title'] = 'Service Unavailable';
         $data['content'] = 'en/404';
         $this->load->view('include/back_template', $data);
    }

    function jp_checkout(){
        $this->data['title'] = 'Checkout';
        $this->data['content'] = 'sbp/jp_express_checkout';
        $this->load->view('include/back_template',$this->data);
    }

    function is_logged_in() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('selfservice/login/view');
        }
    }

    function wtopup(){
        $data['title'] = 'Submit Registration';
         $data['xml'] =$this->sbp_model->checkout();

    }

    function return_result(){
        $this->data['title'] = 'Return';
        $this->data['content'] = 'admin/index';
        $this->load->view('include/back_template',$this->data);
    }

    function success(){
        $this->data['title'] = 'Top up Successful';
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['content'] = 'sbp/success';
        $this->load->view('include/iframe_template',$this->data);
    }

    function topup_fail(){
        $this->data['title'] = 'Top up Fail!';
        $this->data['content'] = 'sbp/return';
        $this->load->view('include/iframe_template',$this->data);
    }

    function send_mail(){
        $this->data['title'] = 'Email';
        $this->data['content'] = 'sbp/mail_template';
        $this->load->view('include/back_template',$this->data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('selfservice/login/view');
    }

    function topup(){
        $data['title'] = 'Top Up';
        $data['content'] = 'sbp/topup_stp1';
        $this->load->view('include/back_template', $data);
    }

    function confirm_topup_details(){
        $data['title'] = 'Confirm Details';
        $data['content'] = 'sbp/confirm_topup_details';
        $data['confirm']=$this->sbp_model->confirm_topup();
        $this->load->view('include/back_template', $data);
    }

    function complete_topup(){
        $data['title'] = 'Complete Topup';
        $data['content'] = 'sbp/complete_topup';
        $data['message'] =$this->sbp_model->complete_topup();
        $this->load->view('include/back_template', $data);
    }

    function register(){

        $data['title'] = 'Register';
        $data['content'] = 'sbp/register';
        $this->load->view('include/back_template', $data);
    }

    function submit_register(){

        $data['title'] = 'Submit Registration';
       // $data['content'] = 'en/register';
        $data['xml'] =$this->sbp_model->register_user();
        //$this->load->view('include/back_template', $data);
    }


    function view_balance(){
        $data['title'] = 'View Balance';
        $data['bal'] = $this->user_model->show_bal();
        $data['content'] = 'sbp/balance_form';
        $this->load->view('include/back_template', $data);
    }

    function submit_balance(){

         $data['title'] = 'Submit Registration';
         $data['xml'] =$this->sbp_model->check_balance();
    }

    function submit_checkout(){

         #$data['title'] = 'Submit Checkout';
         $data['xml'] =$this->sbp_model->submit_checkout();
         #var_dump($this->sbp_model->submit_checkout());
    }

    // function jp_checkout(){
    //     $this->data['title'] = 'Checkout';
    //     $this->data['content'] = 'sbp/jp_express_checkout';
    //     $this->load->view('include/back_template',$this->data);
    // }


    function view_statement(){
        $data['title'] = 'View Ministatement';
        $data['content'] = 'sbp/view_ministatement';
        $data['xml'] =$this->sbp_model->mini_statement();
        $this->load->view('include/back_template', $data);
        #var_dump($data['xml']);
        #$this->load->view('include/back_template', $data);
    }

    function submit_ministatement(){

        $data['title'] = 'View Ministatement';
        //$data['content'] = 'en/balance_form';
        $data['xml'] =$this->sbp_model->mini_statement();
        //$this->load->view('include/back_template', $data);
        var_dump($data['xml']);
    }

    function final_pdf($invid = "") {

        $this->data['single_past_invoices'] = $this->agent_model->get_single_past_invoices($invid);

        //echo "<pre>";print_r( $this->data);echo "</pre>";exit; 

        $this->load->library('pdf');
        $this->pdf->load_view('agent/past_invoice_single', $this->data);
        $this->pdf->render();
        $this->pdf->stream('pdf_report.pdf');  //adding  array("Attachment" => 0) make //the pdf viewable on the browser $ without it it woill be downloaded straight away
    }

    function barcode(){
        $data['title'] = 'Barcode';
        $data['content'] = 'sbp/barcode/test';
        $this->load->view('include/no_template', $data);
    }

    function complete_reg_sbp(){
       $data['xml'] =$this->sbp_model->register_sbp();
       //var_dump($data['xml']);
    }

    function pay_sbp(){
       $data['xml'] =$this->sbp_model->pay_sbp();
    }

    function registersbp(){
        $data['title'] = 'Register SBP';
        $data['content'] = 'sbp/reg_new_business';
        $data['xml'] =$this->sbp_model->prepsaveinitial();
        #$data['res'] =$this->sbp_model->register_sbp();
        #$data['bal'] = $this->sbp_model->show_bal();
        $this->load->view('include/back_template', $data);
    }

    function reg1(){
        $data['title'] = 'Register SBP';
        $data['content'] = 'sbp/reg1';
        $data['xml'] =$this->sbp_model->prepsaveinitial();
        $this->load->view('include/back_template', $data);
    }

    function reg2(){
        $data['title'] = 'Register SBP';
        $data['content'] = 'sbp/reg2';
        $data['xml'] =$this->sbp_model->register_sbp();
        $this->load->view('include/back_template', $data);
        #var_dump($data['xml']);
    }

    function reg3(){
        $data['title'] = 'Register SBP';
        $data['content'] = 'sbp/reg3';
        $data['xml'] =$this->sbp_model->register_sbp1();
        $this->load->view('include/back_template', $data);
    }

   
    function dumper(){
        var_dump($this->sbp_model->register_sbp());
    }

    function checkout(){

    	$data['xml'] =$this->sbp_model->top();
        #$url = 'http://41.212.9.57:1500/jpweb/ncc/walletexpress.aspx';
        #$xml=simplexml_load_string($url);
        #$statement=$xml->url;
        #print_r($data); 
        #header("location: $statement");

        #var_dump($data);
        #header("location: $data");
    }

    function h(){
    	$x=$this->uri->segment(3);
    	header("location: $x");
    }

    function login(){
        $status = $this->user_model->login();
        $text = $status['response'];
        $name = $status['name'];
        if($text=="OK"){
            $data['title'] = 'Home';
            $data['login'] =$name;
            $data['content'] = 'admin/index';
            $this->load->view('include/back_template', $data);
        }else{
            #$this ->session->set_flashdata($text);
            redirect('selfservice/login/view');
        }
        #print_r($text);
    }

    /*function print_sbp_previews($businessId){
        $this->load->library('zend');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');
        $a=$this->load->library('amount_to_words');

        $confirm_details=$this->session->userdata('confirm_details');
        $biz_details=$this->session->userdata('biz_details');
        $vals=$this->sbp_model->print_sbp_preview($businessId);
        
        $approvalstatus=$vals['approvalstatus'];#var_dump($approvalstatus); die();
         if($vals['year'] > 2016){
                 $fileName =APPPATH.'assets/back/receipts/unified_business_permit.pdf';
            } else {
                if ($approvalstatus==1 || $approvalstatus==0) {
                     $fileName =APPPATH.'assets/back/receipts/NCCProvisionalPermit.pdf';
                    
                 }elseif ($approvalstatus==4 || $approvalstatus==5) {
                     $fileName =APPPATH.'assets/back/receipts/NCCPermitNotApproved.pdf';
                 }elseif($approvalstatus==2){
                     $fileName =APPPATH.'assets/back/receipts/NCCPermit.pdf';
                 }
            }
       
       
        $permit_no=$vals['bizzid'];
        $permit_year=$vals['year'];
        $biz_name= $vals['bizname']; //"GRANDFAM PROPERTIES LTD";
        $pin_no=$vals['pinno'];
        $biz_id=$vals['bizzid'];
        $activity=$vals['biztype'];
        $description=$vals['bizactivityname'];
        $activity_code=$vals['activitycode'];
        $amount=$vals['paidfee'];        
        $amount = str_replace( ',', '', $amount);
        $amount1 = number_format($amount,2);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS **";
        }else $amount_in_words="NOT AVAILABLE";
        $box=$vals['pobox'];
        $plot_no=$vals['plotnumber'];
        $road_street=$vals['street'];
        $building=$vals['building'];
        $floor=$vals['floor'];
        $issue_date=$vals['issuedate'];
        $roomstallnumber=$vals['roomstallnumber'];
        $barcode_text=$vals['receiptno'];        
       
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        // Draw something on a page
        // Set font
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 


        //$page->drawText($permit_no, 473, 697); 
        $page->drawText($permit_year, 473, 674);
        $page->drawText($biz_name, 35, 580);
        $page->drawText($pin_no, 110, 552);
        $page->drawText($biz_id, 460, 580);
        $page->drawText($activity, 35, 480);
        $page->drawText($description, 35, 456);
        $page->drawText($activity_code, 485, 473);
        $page->drawText($amount1, 255, 409);
        $page->drawText($amount_in_words, 115, 388);
        $page->drawText($box, 115, 317);
        $page->drawText($plot_no, 340, 317);
        $page->drawText($road_street, 120, 295);
        $page->drawText($building, 120, 273);
        $page->drawText($roomstallnumber, 340, 273);
        $page->drawText($floor, 520, 273);
        $page->drawText(substr($issue_date,0,10), 118, 225);

               
        if(isset($sbp_type)&&!empty($sbp_type)){
            // /$page->drawText(substr($issue_date,0,10), 118, 205);
            $barcodeOptions = array('text' => $barcode_text,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 629,
                'leftOffset' =>285
                );
        }else{
            //$page->drawText(substr($issue_date,0,10), 118, 224);
            $barcodeOptions = array('text' => $barcode_text,'barHeight' => 40,'factor'=>2.5,'font' =>APPPATH.'assets/back/fonts/SWANSEBI.TTF');
            $rendererOptions = array(
                'topOffset' => 610,
                'leftOffset' =>285
                );
        }
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 14); 
        $page->drawText('Trade Licence', 610, 285);
        $details=$this->session->userdata('bid_prepared_details');
        $permits=json_decode($vals['sub_streams'],true);

        $i=0;
        if($permit_year > 2016) {
            $qrUrl = 'https://api.qrserver.com/v1/create-qr-code/?size=70x70&data='.$vals['bizzid'];
                    $qrFileName = APPPATH.'assets/tmp/'.$vals['receiptno'].'.png';
                    copy($qrUrl, $qrFileName);
                    
            $image = Zend_Pdf_Image::imageWithPath($qrFileName); 
            $page->drawImage($image, 450, 130, 535, 215); 
            foreach($permits as $key => $permit){
                $page->drawText($permit['Name'], 260, 200-$i);
                $i+=20;
            }
        } else {
            $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();
        }


        
        
       

        $pdfData = $pdf->render(); 

        header("Content-Disposition: inline; filename=sbp.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
        //Zend_Pdf::parse($pdfData);
        //Zend_Pdf::load($pdfData);
    }*/

    function print_sbp_previews($businessId)
    {
         $vals=$this->sbp_model->print_sbp_preview($businessId);
         //var_dump($vals);exit;
        $this->sbp_model->unified_print_permit($businessId,$vals['year']);
    }

    function print_sbp_previewsProvisional($businessId){
        $this->load->library('zend');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');
        $a=$this->load->library('amount_to_words');
        #$sbp_type=$this->uri->segment(4);
        // if ($sbp_type==1) {
            $fileName =APPPATH.'assets/back/receipts/NCCProvisionalPermit.pdf';
        // }else{
        //     $fileName =APPPATH.'assets/back/receipts/NCCPermit.pdf';
        // }
       
        $vals=$this->sbp_model->print_sbp_preview($businessId);

        $permit_no=$vals['sbpnumber'];
        $permit_year=$vals['year'];
        $biz_name= $vals['bizname']; //"GRANDFAM PROPERTIES LTD";
        $pin_no=$vals['pinno'];
        $biz_id=$vals['bizzid'];
        $activity=$vals['biztype'];
        $description=$vals['bizactivityname'];
        $activity_code=$vals['activitycode'];;
        $amount=$vals['paidfee'];
        #$amount_in_words="**TWENTY SEVEN THOUSAND FIVE HUNDRED AND SIXTY SEVEN SHILLINGS**";
        $amount = str_replace( ',', '', $amount);
        $amount1 = number_format($amount,2);
        if( is_numeric( $amount) ) {
            $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS **";
        }else $amount_in_words="NOT AVAILABLE";
        $box=$vals['pobox'];
        $plot_no=$vals['plotnumber'];
        $road_street=$vals['street'];
        $issue_date=$vals['issuedate'];
        $barcode_text=$vals['receiptno'];
        /*$barcodeOptions = array('text' => 'PETER MUNYASI');
        $imagePath=dirname(__FILE__).'\barcode.png';*/
       
        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        // Draw something on a page
        // Set font
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $page->drawText($permit_no, 473, 697); 
        $page->drawText($permit_year, 473, 674);
        $page->drawText(strtoupper($biz_name), 35, 580);
        $page->drawText($pin_no, 110, 552);
        $page->drawText($biz_id, 460, 580);
        $page->drawText($activity, 35, 480);
        $page->drawText($description, 35, 456);
        $page->drawText($activity_code, 485, 473);
        $page->drawText($amount1, 255, 409);
        $page->drawText($amount_in_words, 115, 388);
        $page->drawText($box, 115, 317);  
        $page->drawText($plot_no, 340, 317);
        $page->drawText($road_street, 120, 295);
        $page->drawText($issue_date, 120, 248);
        $barcodeOptions = array('text' => $barcode_text,'barHeight' => 40,'factor'=>2.5,'font' =>dirname(__FILE__).'/SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 590,
            'leftOffset' =>285
            );
        /*$pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();*/

        $pdfData = $pdf->render();

        header("Content-Disposition: inline; filename=result.pdf");
        header("Content-type: application/x-pdf");
        echo $pdfData;
    }

        function printreceipt($businessId){
        $this->load->library('zend');
        $a=$this->load->library('amount_to_words');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =dirname(__FILE__).'/receipt.pdf';

        $vals=$this->sbp_model->print_sbp_preview($businessId);

        $receiptno=$vals['receiptno'];
        $date=date('Y-m-d',time()); 
        $date = substr($date,0,10);
        $stype=$this->uri->segment(5);
        $phoneno=$this->uri->segment(4);
        $balance_due="0.00";
        if(isset($stype)&&!empty($stype)){
           $amount = str_replace( ',', '', $vals['paidfee']);
           $amount=$amount+200; 
           $description= "'".strtoupper($vals['bizname'])."'"." NEW BUSINESS PERMIT, BUSINESS ID ".$vals['bizzid'];
           //
            if( is_numeric( $amount) ) {
                $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS ONLY **";
            }else $amount_in_words="NOT AVAILABLE"; 
       } else {
            $amount=$vals['paidfee'];
             $description= "'".strtoupper($vals['bizname'])."'"." BUSINESS PERMIT, BUSINESS ID ".$vals['bizzid']; 
             $amount = str_replace( ',', '', $amount);
            if( is_numeric( $amount) ) {
                $amount_in_words="** ".strtoupper($this->amount_to_words->convert_number($amount))." SHILLINGS **";
            }else $amount_in_words="NOT AVAILABLE";
        }
        $amount =number_format(str_replace( ',', '', $amount),2);
        
        


        $pdf = Zend_Pdf::load($fileName);
        $page=$pdf->pages[0];

        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 
        $fire_health_text="This permit includes trading license, fire certificate, health certificate and advertisement";
        $page->drawText($receiptno, 150, 548);
        $page->drawText($date, 480, 548);
        $page->drawText($phoneno, 250, 505); 
        $page->drawText($amount, 480, 505);
        $page->drawText($amount_in_words, 150, 470); 
        $page->drawText($description, 150, 437);
        $page->drawText($fire_health_text, 265, 705);
        $page->drawText($amount, 450, 375);
        $page->drawText($amount, 450, 345);
        $page->drawText($balance_due, 450, 315);

        $barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>dirname(__FILE__).'\SWANSEBI.TTF');
        $rendererOptions = array(
            'topOffset' => 555,
            'leftOffset' =>295
            );
        $pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


        $pdfData = $pdf->render(); 
        header("Content-Disposition: inline; filename=receipt.pdf"); 
        header("Content-type: application/x-pdf"); 
        echo $pdfData;
    }

    function reg_new(){
        $data['title'] = 'Register SBP Step 1';
        $data['content'] = 'sbp/step1';
        $data['step1'] =$this->sbp_model->prepsaveinitialNew();
        $this->load->view('include/back_template', $data);
    }

    function step2($checksum=NULL){
        if($checksum==NULL){           
            $this->form_validation->set_rules('biz_name','Business Name','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('doc_no','ID Document Number','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('doc_no','ID Document Number','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('box','P.O. Box','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('postal_code','Postal Code','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('pin_no','KRA Pin','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('town','Town','trim|xss_clean|required|callback_zero_check');
            if(!$this->form_validation->run()){
                /*$this->session->set_flashdata("msg",validation_errors());
                redirect('sbp/step2');*/
                $this->reg_new();
                return;
            }

        }
        $data['title'] = 'Register SBP Step 2';
        $data['content'] = 'sbp/step2';
        $data['step1data'] =$this->sbp_model->step1();
        $this->load->view('include/back_template', $data);
        #var_dump($data['step1data']);
    }

    function step3($checksum=NULL){
        if($checksum==NULL){
            $this->form_validation->set_message('min_length[10]', 'field must have a valid phone number.');
            $this->form_validation->set_rules('tel1','Telephone','trim|xss_clean|required|min_length[10]');
            $this->form_validation->set_rules('email','Email','trim|xss_clean|required|valid_email');
            $this->form_validation->set_rules('address','Physical Address','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('building','Building Name','trim|xss_clean|required');
            $this->form_validation->set_rules('plot_number','Plot Number','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('lr_number','Land Rate Number','trim|xss_clean|required|callback_zero_check');
            $this->form_validation->set_rules('stall_room_no','Stall/Room Number','trim|xss_clean|required');
            if($this->input->post('storey')==1){
                $this->form_validation->set_rules('floor','Floor Number','trim|xss_clean|required');
            } 
            if($this->input->post('tel2')!=''){
                $this->form_validation->set_rules('tel2','Other Telephone','trim|xss_clean|required|min_length[10]');
            }        
            
            if(!$this->form_validation->run()){
                /*$this->session->set_flashdata("msg",validation_errors());
                redirect('sbp/step2');*/
                $this->step2(1);
                return;
            }
        }      
        
        $data['title'] = 'Register SBP Step 3';
        $data['content'] = 'sbp/step3';
        $data['step2data'] =$this->sbp_model->step2();
        $this->load->view('include/back_template', $data);
    }

    public function zero_check($str)
    {
        if ($str == '0')
        {
            $this->form_validation->set_message('zero_check', 'The %s field can not be zero');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function step4(){
        $this->form_validation->set_rules('full_names','Full Names','trim|xss_clean|required');
        $this->form_validation->set_rules('owner_postal_code','Postal Code','trim|xss_clean|required|callback_zero_check');
        $this->form_validation->set_rules('owner_telephone','Telephone Number','trim|xss_clean|required|callback_zero_check');
        if($this->input->post('owner_telephone2')!=''){
            $this->form_validation->set_rules('owner_telephone2','Alternative Telephone','trim|xss_clean|required|callback_zero_check');
        }

        if($this->input->post('owner_box')!=''){
            $this->form_validation->set_rules('owner_box','PO Box','trim|xss_clean|required|callback_zero_check');
        }        
        if(!$this->form_validation->run()){            
            $this->step3(1);
            return;
        }

        $data['title'] = 'Register SBP Step 4';
        $data['content'] = 'sbp/step4';
        $data['counties'] = $this->sbp_model->getSubCounties();  
        $data['business_classes'] = $this->sbp_model->getBusinessClasses();         
        $data['step4'] =$this->sbp_model->prepsaveinitialNew();
        $data['step3data'] =$this->sbp_model->step3();
        $this->load->view('include/back_template', $data);
    }

    function getClasses()
    {
        echo json_encode($this->sbp_model->getBusinessClasses());
    }

    function step5(){        
        $data['title'] = 'Register SBP Step 5';
        $data['content'] = 'sbp/step5';
        $data['step5data'] =$this->sbp_model->prepareRegistration();
        /*$data['fire_data'] =$this->sbp_model->testgetFireBusinessDetails();   
        $data['health_data']=$this->sbp_model->testgetHygieneBusinessDetails();*/
        $data['step4data'] =$this->sbp_model->step4();
        $this->load->view('include/back_template', $data);
    }



    function step6(){
        $data['title'] = 'Register SBP Step 6';
        $data['step5data'] =$this->sbp_model->step5();
       
        $data['alldata'] =$this->sbp_model->completeRegistration();
        $data['content'] = 'sbp/step6';
        $this->load->view('include/back_template', $data);
        // var_dump($data['step5data']);
        // var_dump($data['alldata']);
    }

    function step7(){
        $this->sbp_model->reg_prepare();
    }

    function reprintSbpreceipt($businessid,$receiptno){
        $this->sbp_model->reprintSbpreceiptNew($businessid,$receiptno);
    }

    function printreceiptcheck(){
        $bizId=$this->sbp_model->bussinessidcheckNew();
        $this->sbp_model->reprintSbpreceiptNew($bizId);
    }

    function printnewsbpreceipt($businessid){
        $businessid = $this->uri->segment(3);
        $this->sbp_model->printnewsbpreceipt($businessid);
    }

    function reprintSReceipt(){
        $data['title'] = 'Reprint Receipt';
        $data['content'] = 'sbp/reprintSReceipt';
        $this->load->view('include/back_template', $data);
    }

    function insertManualSbpReceipt(){
        $this->data['title'] = 'SBP Details';
        $this->data['content'] = 'sbp/insertManualSbpReceipt';
        $this->load->view('include/back_template', $this->data);
    }

    function insertManualReceipt(){
        $this->sbp_model->insertReceiptDetails();
    }

    function check_sessions()
    {
        /*$_POST['biz_id']='1270014';
        $_POST['year']='2017';
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['datax'] = $this->sbp_model->testpreparePaymentNew();
        $this->data['datax']['advert_fee'] =4200;
        $this->data['zones'] =$this->sbp_model->testprepsaveinitialNew();
        $this->data['fire_invoice']=$this->sbp_model->testgetFireBusinessDetails();
        $this->data['health_details']=$this->sbp_model->testgetHygieneBusinessDetails();
         echo "<pre>";var_dump($this->data);exit;*/
       
    }

    function getSubClassId()
    {
        //$_POST['sub_classes']=103;
        echo $this->sbp_model->getSubActivityCharges();
    }

}
