<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class En extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('agent_portal_model');
       # $this->load->library("nuSoap_lib");
    }

     function permit(){

        $data['title'] = '';
        $data['content'] = 'sbp/permit';
        $this->load->view('sbp/permit', $data);
    }


    function index() {

        $data['title'] = 'Login';
        $data['content'] = 'en/index';
        $this->load->view('en/index');
    }

    function api_access_error(){
        $data['content'] = 'en/api_access_error';
        $this->load->view('include/back_template', $data);
    }

    function test(){
        $data['content'] = 'en/test';
        $this->load->view('include/back_template', $data);
    }

    function dashboard(){

        $data['title'] = 'Dashboard';
        $data['content'] = 'en/dashboard';
        $this->load->view('include/back_template', $data);

    }

    function sbp(){

        $data['title'] = 'Account Number';
        $data['content'] = 'en/accountno';
        $this->load->view('include/back_template', $data);
    }

    function ac_details(){

        $data['title'] = 'Account Details';
        $data['content'] = 'en/ac_details';
        $data['confirm']=$this->agent_portal_model->preparepayment();
        $this->load->view('include/back_template', $data);
    }

    function confirm_payment(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'en/confirm_payment_details';
        $data['confirm']=$this->agent_portal_model->confirm_payment();
        $this->load->view('include/back_template', $data);
    }

    function complete_payment(){

        $data['title'] = 'Complete Payment';
        $data['content'] = 'en/complete_payment';
        $data['confirm']=$this->agent_portal_model->complete_payment();

        //print_r($data['confirm']=$this->agent_portal_model->complete_payment());
        $this->load->view('include/back_template', $data);
    }

    function topup(){
        $data['title'] = 'Top Up';
        $data['content'] = 'en/topup_stp1';
        $this->load->view('include/back_template', $data);
    }

    function confirm_topup_details(){
        $data['title'] = 'Confirm Details';
        $data['content'] = 'en/confirm_topup_details';
        $data['confirm']=$this->agent_portal_model->confirm_topup();
        //var_dump($this->agent_portal_model->confirm_topup());
        $this->load->view('include/back_template', $data);
    }

    function complete_topup(){
        $data['title'] = 'Complete Topup';
        $data['content'] = 'en/complete_topup';
        $data['message'] =$this->agent_portal_model->complete_topup();
        //var_dump($data['message'] =$this->agent_portal_model->complete_topup());
        $this->load->view('include/back_template', $data);
    }

    function register(){

        $data['title'] = 'Register';
        $data['content'] = 'en/register';
        $this->load->view('include/back_template', $data);
    }

    function submit_register(){

        $data['title'] = 'Submit Registration';
       // $data['content'] = 'en/register';
        $data['xml'] =$this->agent_portal_model->register_user();
        //$this->load->view('include/back_template', $data);
    }

    function view_balance(){
        $data['title'] = 'View Balance';
        $data['content'] = 'en/balance_form';
        $this->load->view('include/back_template', $data);
    }

    function submit_balance(){

         $data['title'] = 'Submit Registration';
         $data['xml'] =$this->agent_portal_model->check_balance();
    }

    function mini_statement(){
        $data['title'] = 'View Ministatement';
        $data['content'] = 'en/view_ministatement';
        $this->load->view('include/back_template', $data);
    }

    function submit_ministatement(){

        $data['title'] = 'View Ministatement';
        //$data['content'] = 'en/balance_form';
        $data['xml'] =$this->agent_portal_model->mini_statement();
        //$this->load->view('include/back_template', $data);
    }


    /*
    *
    *LAnd rates
    */

    function landrates(){

        $data['title'] = 'View Balance';
        $data['content'] = 'en/balance_form';
        $this->load->view('include/back_template', $data);

    }

    function ts_checkout(){
        $data['content'] = 'form';
        $this->load->view('include/back_template', $data);
    }


    /*parking*/

    function parking_daily(){
        $data['title'] = 'Parking';
        $data['content'] = 'en/parking_daily';
        $this->load->view('include/back_template', $data);

    }
}

