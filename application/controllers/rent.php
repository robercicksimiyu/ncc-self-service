<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rent extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model('rent_model');
    }

    function select_estate(){
    	$this->data['title'] = 'Select Estate';
    	$this->data['estates'] = $this->rent_model->getEstates();
    	$this->data['content'] = 'rent/select_estate';
    	$this->load->view('include/back_template', $this->data);
    	// var_dump($this->rent_model->getEstates());
    }

    function select_estate_house(){
    	$this->data['title'] = 'Get Estate House';
    	$this->data['esthouse'] = $this->rent_model->getEstateHouse();
    	$this->data['content'] = 'rent/select_est_house';
    	$this->load->view('include/back_template',$this->data);
    	#var_dump($this->rent_model->getEstateHouse());
    }

    function get_houses(){
        return $this->rent_model->getEstatesHouses();
    }

    function dump_site(){
        var_dump($this->rent_model->getEstatesOnly());
    }
    #-----new-----#
    function house_rent(){
        $data['title'] = 'House Rent';
        $data['content'] = 'rent/selectestate';
        $data['estates']=$this->rent_model->getEstates();
        $this->load->view('include/back_template', $data);
        #var_dump($this->rent_model->getEstates());

    }

    // function get_houses(){
    //     return $this->house_rent_model->getEstatesHouses();
    // }

    function preparehousepayment(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmhouserent';
        $data['confirm']=$this->rent_model->prephousepayment();
        $this->load->view('include/back_template', $data);
        #var_dump($data['confirm']);
    }

    function confirm2rent(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmrent';
        $data['confirm']=$this->rent_model->confirm2rent();
        $this->load->view('include/back_template', $data);
    }

    function completerentpayment(){

        // $data['title'] = 'Complete Payment';
        // $data['content'] = 'rent/completerentpayment';
        // $data['message']=$this->rent_model->completerentpayment();
        // $this->load->view('include/back_template', $data);
        var_dump($this->rent_model->completerentpayment());
    }
    #-----new-----#

    function pdf4(){
$this->load->library('zend');
        $this->zend->load('Zend/Pdf');
        $this->zend->load('Zend/Barcode');

        $fileName =dirname(__FILE__).'\LR_receipt.pdf';

        $receiptno="1111221336/000002168";
$date=date('Y-m-d',time()); 
$phoneno="STEPHEN K. MUNYAO & ANOTHER";
$penalties="0.00";
$total_amount_due="9,177.00";
$balance_due="-23.00"; 
$amount="9,200.00"; 
$amount_in_words="** NINE THOUSAND TWO HUNDRED SHILLINGS ONLY **"; 
$description="Plot number BLOCK 78/226"; 
$plot_owner="STEPHEN K. MUNYAO & ANOTHER";
$cashier="irene nderi";


$pdf = Zend_Pdf::load($fileName);
$page=$pdf->pages[0];

$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 10); 

$page->drawText($receiptno, 150, 528);
$page->drawText($date, 450, 528);
$page->drawText($phoneno, 250, 478); 
$page->drawText($amount, 480, 478);
$page->drawText($amount_in_words, 150, 427); 
$page->drawText($description, 150, 376);
$page->drawText($plot_owner, 170, 325);
$page->drawText($cashier, 127, 38);
$page->drawText($penalties, 450, 274);
$page->drawText($total_amount_due, 450, 252);
$page->drawText($amount, 450, 230);
$page->drawText($balance_due, 450, 209);   

$barcodeOptions = array('text' => $receiptno,'barHeight' => 40,'factor'=>2.5,'font' =>dirname(__FILE__).'\SWANSEBI.TTF');
$rendererOptions = array(
'topOffset' => 600,
'leftOffset' =>295
);
$pdf = Zend_Barcode::factory('code128', 'pdf', $barcodeOptions, $rendererOptions)->setResource($pdf)->draw();


$pdfData = $pdf->render(); 
header("Content-Disposition: inline; filename=receipt.pdf"); 
header("Content-type: application/x-pdf"); 
echo $pdfData;
}
}