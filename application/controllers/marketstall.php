<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Marketstall extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('marketstalls_model');
        $this->load->model('rent_model');
        $this->load->model('user_model');
    }

    function get_houses(){
        return $this->marketstalls_model->getEstatesHouses();
    }

    function dump_site(){
        var_dump($this->marketstalls_model->getEstatesOnly());
    }
    #-----new-----#
    function select_market(){
        $data['title'] = 'House Rent';
        $data['content'] = 'rent/selectmarket';
        $data['estates']=$this->marketstalls_model->getEstatesNew();
        $this->load->view('include/back_template', $data);
        #var_dump($this->rent_model->getEstates());

    }

    function preparemarketpayment(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmmarketrent';
        $data['confirm']=$this->marketstalls_model->prephousepaymentNew();
        $this->load->view('include/back_template', $data);
        #var_dump($data['confirm']);
    }

    function confirm2rent(){

        $data['title'] = 'Confirm Details';
        $data['content'] = 'rent/confirmrentmarket';
        $data['confirm']=$this->marketstalls_model->confirm2rent();
        $this->load->view('include/back_template', $data);
    }

    function completerentpayment(){

        $data['title'] = 'Complete Payment';
        $data['content'] = 'rent/completemarketpayment';
        $data['bal'] = $this->user_model->viewBalance1();
        $data['message']=$this->marketstalls_model->completeNew();
        $this->load->view('include/back_template', $data);
        #var_dump($this->rent_model->completerentpayment());
    }
    #-----new-----#

    
}