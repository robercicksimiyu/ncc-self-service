
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement extends CI_Controller {
	public $data;

	function __construct() {
		parent::__construct();
		$this->load->model('transport_model');
		
    	$this->load->model('advert_model');
   		$this->load->model('rlb_model');
   		$this->load->model('common_model');
		#$this->load->library('email');
	}

	function index()
	{
		$this->data['title']= $this->lang->line('text_check_business_title');
		$this->data['content']="advertisement/verify_premise";
		$this->load->view('include/back_template', $this->data);
	}

	function get_category_fee_cat_id($id)
	{
		echo json_encode($this->advert_model->get_category_fees_by_cat_id($id));
	}

	function small()
	{
		$this->data['title']= $this->lang->line('text_small_advertisement_title');
		if($this->input->post()) {
			$this->data['invoice_inforamtion']=$this->advert_model->small_format_application();
			$this->data['content']="advertisement/confirm_details";
			$this->load->view('include/back_template', $this->data);
			return;
		}		
		
		$this->data['content']="advertisement/application_form";
		$this->data['subcounties']=$this->common_model->getZones();		
		$this->data['categories']=$this->advert_model->get_categories();
		$this->data['dimensions']=$this->advert_model->get_dimensions();
		
		$this->load->view('include/back_template', $this->data);
	}

	function get_fees($id)
	{
		return trim($this->advert_model->get_category_fee($id));
	}

	function pay()
	{
		//var_dump($this->input->post());exit;
		$this->data['payment_details']=$this->advert_model->complete_payment();
		$this->data['content']="advertisement/print_receipt";
		$this->load->view('include/back_template', $this->data);
		
	}

	function receipt($invoice_no)
	{
		$this->advert_model->printReceipt($invoice_no);
	}

	

	function section($type="loading_zone",$premise=false){
		$this->data['premise']=$premise;
		if($type=="loading_zone") {
			$this->data['form'] = 'TF01';
			$this->data['title']="Loading Zone";
			$this->data['category']=197;
			$this->data['content'] = 'transport/application_form';
		} elseif($type=="banners") {
			$this->data['form'] = 'TF02';
			$this->data['title']="Banners";
			$this->data['hoisted_location']=$this->transport_model->get_hoisted_location();				
			$this->data['category']=196;
			$this->data['content'] = 'transport/banner_form';
		} else {
			$this->data['form'] = 'TF03';
			$this->data['title']="Bus shelters";
			$this->data['category']=197;
			$this->data['content'] = 'transport/bus_shelter_form';
		}
		$this->data['subcounties'] = json_decode(json_encode($this->sbp_model->getBusinessZones()));	
		$this->data['type']=$type;
		
	    
		$this->load->view('include/back_template', $this->data);
	}

	 function renew_register()
	 {
	 	$this->data['title'] = 'Loading Zones';
		$this->data['content'] = 'transport/loading_zone/renew_register';			
		$this->load->view('include/back_template', $this->data);

	 }

	 public function settle()
	 {
	 	$this->data['payment_info']=$this->transport_model->complete_payment();
	 }

	 public function billing()
	 {
	 	$this->data['invoice_details']=$this->transport_model->get_renewal_bill();
	 	$this->data['renewal']=true;
	 	$this->data['invoice_details']['invoice_no']=$this->transport_model->generate_random_invoice_numbers();	 
	 	$this->data['content'] = 'transport/loading_zone/details';			
		$this->load->view('include/back_template', $this->data);
	 }

	 public function renew()
	 {
	 	$this->data['title']="Loading Zones Renewal";
	 	$this->data['businesses']=$this->transport_model->get_renew_businesses();	 	
	 	$this->data['content'] = 'transport/loading_zone/renew';			
		$this->load->view('include/back_template', $this->data);
	 }

	function confirm()
	{
		
		if(count($this->input->post()) > 1) {
			$this->load->model('sbp_model');
			$this->data['invoice_details']=$this->transport_model->confirm_details();
			$this->data['hoisted_location']=$this->transport_model->get_hoisted_location();				
			$this->data['subcounties'] = $this->sbp_model->getBusinessZones();		
			$this->data['title'] = 'Loading Zones';
			$this->data['content'] = 'transport/confirm_details';			
			$this->load->view('include/back_template', $this->data);
		} else {
			redirect('transport_infrustructure/renew_register');
		}
		
	}


	function approve()
	{
		if(count($this->input->post()) > 1) {
			$this->data['details']=$this->transport_model->process_application();
			$this->data['content'] = 'transport/application_details';			
			$this->load->view('include/back_template', $this->data);
		} else {
			redirect('transport_infrustructure/renew_register');
		}
	}


	function payment()
	{
		$this->data['payment_details']=$this->transport_model->complete_payment();
		$this->data['title'] = 'Loading Zones';
		$this->data['response'] = 0;
		$this->data['content'] = 'transport/print_receipt';
		$this->load->view('include/back_template', $this->data);		
		
	}

	/*function receipt($ref_id)
	{
		$this->advert_model->printReceipt($ref_id);
	}*/

	function invoice()
	{
		$this->data['title']="Invoice Details";
		$this->data['invoice_details']=$this->transport_model->getInvoiceDetails($this->input->post('RefId'));		
		$this->data['content'] = 'transport/loading_zone/invoice_details';
		$this->load->view('include/back_template', $this->data);
	}

	/*function pay()
	{
		
		$this->data['content'] = 'transport/pay';
		$this->load->view('include/back_template', $this->data);
	
	}*/

	function complete_payment()
	{
		$this->data['payment_details']=$this->transport_model->invoicePay();			
		$this->data['title'] = 'Loading Zones';
		$this->data['response'] = 0;
		$this->data['content'] = 'transport/print_receipt';
		$this->load->view('include/back_template', $this->data);
	}

	function viewTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'health/conditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function institutionTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'health/institutionConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function sendMail(){
  //       // $this->email->from('samsoft12@gmail.com', 'Samuel');
  //       // $this->email->to('samsoft12@gmail.com');
  //       // #$this->email->cc('another@another-example.com');
  //       // #$this->email->bcc('them@their-example.com');
  //       // $this->email->subject('Email Test');
  //       // $this->email->message('Testing the email class.');
  //       // $this->email->send();

  //       // echo $this->email->print_debugger();
		// $admin_email = "samsoft12@gmail.com";
		// $email = "samsoft12@gmail.com";
		// $subject = "Test";
		// $comment = "Test";

  // //send email
		// mail($admin_email, "$subject", $comment, "From:" . $email);
	}

	function foodHygieneApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enter_biz_id';
		$this->data['subcounties'] = $this->rlb_model->getSubcounties();
    $this->data['categories'] = $this->sbp_model->prepsaveinitialNew();
    $this->data['zones'] = $this->rlb_model->getZoneList();
		$this->load->view('include/back_template', $this->data);
	}

	function getBusinessDetails(){
    $this->load->model('sbp_model');
		$this->data=$this->health_model->preparePayment();
    echo $this->data;
		// var_dump($this->data['health'] = $this->health_model->getSubcounties());
	}

	function getWards(){
		$this->health_model->getWards();
	}

	function displayHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getHygieneBusinessDetails();
		$this->data['content'] = 'health/foodHygieneApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeFHPayment();
		$this->data['content'] = 'health/completeHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function InstitutionHealthApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enterBID';
    $this->data['categories'] = $this->sbp_model->prepsaveinitialNew();
    $this->data['subcounties'] = $this->rlb_model->getSubcounties();
    $this->data['zones'] = $this->rlb_model->getZoneList();
		$this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
	}

	function displayInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getInstitutionBusinessDetails();
		$this->data['content'] = 'health/foodHygieneInstitutionApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeIHPayment();
		$this->data['content'] = 'health/completeInstitutionHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function printHealthPermit($refid){
		$this->health_model->printHealthPermit($refid);
	}

	function printHealthPermitreceipt($refid){
		$this->health_model->printHealthPermitreceipt($refid);
	}

	function printHealthPermitPayment($refid){
		$this->health_model->printHealthPermitPayment($refid);
	}

	function printInstitutionHealthPermit($refid){
		$this->health_model->printInstitutionHealthPermit($refid);
	}

	function printInstitutionHealthPermitreceipt($refid){
		$this->health_model->printInstitutionHealthPermitreceipt($refid);
	}

	function printInstitution(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printInstitution';
		$this->load->view('include/back_template', $this->data);
	}

	function printInstitutionD(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printDetails';
		$this->data['health'] = $this->health_model->printInstitution();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function printHygiene(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printHygiene';
		$this->load->view('include/back_template', $this->data);
	}

	function payHygiene(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payHygiene';
		$this->load->view('include/back_template', $this->data);
	}

	function payHygieneDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payHygieneDetails';
		$this->data['health'] = $this->health_model->payHygieneDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function payInstitution(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payInstitution';
		$this->load->view('include/back_template', $this->data);
	}

	function payInstitutionDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/payInstitutionDetails';
		$this->data['health'] = $this->health_model->payInstitutionDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function printHygieneD(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printHygieneDetails';
		$this->data['health'] = $this->health_model->printHygiene();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function error($error_type=''){
		switch ($error_type) {
			case 'details':
				$this->data['error_msg']="No details found for this business. Please try again";
				break;
			
			default:
				$this->data['error_msg']="An error accured while processing your request. Please try again";
				break;
		}
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/error';		
		$this->load->view('include/back_template', $this->data);
	}

	function invalidBID(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidBID';
		$this->load->view('include/back_template', $this->data);
	}

	function APIerror(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/APIerror';
		$this->load->view('include/back_template', $this->data);
	}

	function invalidRefid(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidRefid';
		$this->load->view('include/back_template', $this->data);
	}

	/*public function testSms()
	{
	  $toSend=array(
	    "SenderName"=>"KASNEB",
	    "Mobile"=>"0717803383",
	    "Message"=>"Ready for the exams",
	    );

	  //echo hash('sha256', 'Pass@1234');
	  $url="http://192.168.11.225/SMSService/API/SMS";
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	  curl_setopt($ch, CURLOPT_POST, true);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($toSend));
	  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	      'DeveloperKey: 084049D0-AB72-4EE2-9EDE-0C25C1D1268C',
	      'Password: '.hash('sha256', 'Pass@1234')
	  ));
	  $result = curl_exec($ch);

	}*/

}
