<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fire extends CI_Controller {
	public $data;

	function __construct() {
		parent::__construct();
		$this->load->model('fire_model');
		$this->load->model('user_model');
    $this->load->model('sbp_model');
    $this->load->model('rlb_model');
	}

	function viewTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'fire/conditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function viewInstitutionTerms(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'fire/institutionConditions';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function landingPage(){
		$this->data['title'] = 'Terms & Conditions';
		$this->data['content'] = 'fire/landingPage';
		$this->load->view('include/back_template', $this->data);
	}

	function firePermitApplicationForm(){
		$this->data['title'] = 'Fire Permit Application Form';
		$this->data['content'] = 'fire/enter_biz_id';
    $this->data['categories'] = $this->sbp_model->prepsaveinitialNew();
    $this->data['subcounties'] = $this->rlb_model->getSubcounties();
    $this->data['zones'] = $this->rlb_model->getZoneList();
		$this->load->view('include/back_template', $this->data);
	}

	function firePermitApplicationFormB(){
		$this->data['title'] = 'Fire Permit Application Form';
		$this->data['content'] = 'fire/building';
		#$this->data['subcounty'] = $this->health_model->getSubcounties();
		$this->load->view('include/back_template', $this->data);
	}

	function displayFirePermitDetails(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['fire'] = $this->fire_model->getFireBusinessDetails();
		$this->data['content'] = 'fire/firePermitApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function displayFirePermitDetailsBuilding(){
		$this->data['title'] = 'Fire Permit Application Details';
		$this->data['fire'] = $this->fire_model->getFireBusinessDetailsBuilding();
		$this->data['content'] = 'fire/firePermitApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function printFirePermitreceipt($refid){
		$this->fire_model->printFirePermitreceipt($refid);
	}

	function prepareFirePayment(){
		$this->data['title'] = 'Prepare Payment';
		$this->data['content'] = 'fire/prepareFirePayment';
		$this->data['fire'] = $this->fire_model->prepareFirePayment();
		$this->load->view('include/back_template', $this->data);
	}

	function completeFirePayment(){
		$this->data['title'] = 'Complete Payment';
		$this->data['content'] = 'fire/completeFirePermitDetails';
		$this->data['fire'] = $this->fire_model->completeFirePayment();
		$this->load->view('include/back_template', $this->data);
	}

	function payFire(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/payFire';
		$this->load->view('include/back_template', $this->data);
	}

	function payFireDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/payFireDetails';
		$this->data['fire'] = $this->fire_model->payFireDetails();
		$this->load->view('include/back_template', $this->data);
	}

	function completeFirePermitDetails(){
		$this->data['title'] = 'Fire Permit Payment Details';
		$this->data['fire'] = $this->fire_model->completeFPPayment();
		$this->data['content'] = 'fire/completeFirePermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function printFirePermit(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/printFire';
		$this->load->view('include/back_template', $this->data);
	}

	function printFireDetails(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/printFireDetails';
		$this->data['fire'] = $this->fire_model->printFire();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function printFire($refid){
		$this->fire_model->printFirePermit($refid);
	}

	function getBusinessDetails()
	{
		$this->load->model('sbp_model');
		$this->data=$this->fire_model->preparePayment();
    echo $this->data;
	}


	function InvalidBid(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/invalidBID';
		$this->load->view('include/back_template', $this->data);
	}

	function InvalidRefid(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'fire/invalidRefid';
		$this->load->view('include/back_template', $this->data);
	}

	function error(){
		$this->data['title'] = 'Error';
		$this->data['content'] = 'fire/error';
		$this->load->view('include/back_template', $this->data);
	}












}
