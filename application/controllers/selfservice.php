<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Selfservice extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('queries_model');
        #$this->is_logged_in();
    }

    public function index() {
        redirect('selfservice/login');
    }

    function register(){
        $data['title'] = 'Register';
        $data['content'] = 'en/registerFeedback';
        $data['register'] =$this->user_model->RegisterNewUser();
        $this->load->view('include/no_template', $data);
    }

    function registerNew(){
        $data['title'] = 'Register';
        $data['content'] = 'en/registerUser';
        $this->load->view('include/no_template', $data);
    }

    // function registerNewUser(){
    //     $data['title'] = 'View Balance';
    //     #$data['bal'] = $this->user_model->viewBalance1();
    //     $data['content'] = 'en/registerUser';
    //     $this->load->view('include/back_template', $data);
    // }

    function registerUserNew(){

        $status = $this->user_model->registerNew();
      var_dump($status); die();
        $text = $status['response'];
        if($text=="OK"){
            redirect('selfservice/login');
        }else{
            $this ->session->set_flashdata('msg',$text);
            redirect('selfservice/registerNew');
        }

    }

    function forgotPassword(){
        $data['title'] = 'Forgot Password';
        $data['content'] = 'en/forgotpassword';
        $this->load->view('include/no_template', $data);
    }

    function inchwara(){
        $this->user_model->inchwara();
    }

    function usertopup(){
        var_dump($this->user_model->topup());
    }

    function home(){
    	#$this->is_logged_in();
    	$this->data['title'] = 'Home';
        $this->data['content'] = 'admin/index';
    	$this->load->view('include/back_template', $this->data);
    }

    function view_balance(){
        $data['title'] = 'View Balance';
        $data['bal'] = $this->user_model->viewStatement();
        //var_dump($data['bal']); die();
        $data['content'] = 'sbp/balance_form';
        $this->load->view('include/back_template', $data);
    }
    function viewBalance(){
        $data['title'] = 'View Balance';
        $data['bal'] = $this->user_model->viewBalance1();
        $data['content'] = 'sbp/balance_form';
        $this->load->view('include/back_template', $data);
    }
    function submit_balance(){

         $data['title'] = 'Submit Registration';
         $data['xml'] =$this->user_model->check_balance();
    }

    function view_statement(){
        $data['title'] = 'View Ministatement';
        $data['content'] = 'sbp/view_ministatement';
        $data['xml'] =$this->user_model->mini_statement();
        $this->load->view('include/back_template', $data);
        #var_dump($data['xml']);
        #$this->load->view('include/back_template', $data);
    }
    function viewStatement(){
        $data['title'] = 'View Ministatement';
        $data['content'] = 'sbp/miniStatement.php';
        $data['xml'] =$this->user_model->viewStatement();
        $this->load->view('include/back_template', $data);
        #var_dump($data['xml']);
        #$this->load->view('include/back_template', $data);
    }

    function submit_ministatement(){

        $data['title'] = 'View Ministatement';
        $data['xml'] =$this->user_model->mini_statement();
        var_dump($data['xml']);
    }

    function wallettopup(){
        $this->data['title'] = 'Business ID';
        $this->data['content'] = 'sbp/check_bal';
        $this->data['xml'] =$this->user_model->topup();
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->top());
    }

    function get_wallet_bal(){
        $data['title'] = 'View Balance';
        $data['bal'] = $this->user_model->show_bal();
        $data['content'] = 'sbp/balance_form';
        $this->load->view('include/back_template', $data);
        #var_dump($data['bal']);
    }

    function changeWalletPin(){
        var_dump($this->user_model->changeWalletPin());
    }


    function log(){
        $status = $this->user_model->login();
        $text = $status['response'];
        $name = $status['name'];
        #$bal = $status['balance'];
        if($text=="OK"){
            $data['title'] = 'Home';
            $data['login'] = $name;
            #$data['bal'] = $bal;
            $data['content'] = 'admin/index';
            $this->load->view('include/back_template', $data);
        }else{
            $this ->session->set_flashdata('msg','Invalid Login Details, Please try again!');
            redirect('selfservice/login');
        }
        #print_r($text);
    }
    function userLogin(){
        $status = $this->user_model->userLoginNew(); #var_dump($status); die();
        $text = $status['RESPONSE_CODE'];
        $name = $status['name'];
        $bal = $status['balanc'];
        if($text=="0"){
            $data['title'] = 'Home';
            $data['login'] = $name;
            $data['bal'] = $bal;
            $data['content'] = 'admin/index';
            $this->load->view('include/back_template', $data);
        }else{
            $this ->session->set_flashdata('msg','Invalid Login Details, Please try again!');
            redirect('selfservice/login');
        }
        #print_r($text);
    }
    function newLogin(){
        $status = $this->user_model->newLogin();
        $text = $status['response'];
        $name = $status['name'];
        #$bal = $status['balance'];
        if($text=="OK"){
            $data['title'] = 'Home';
            $data['login'] = $name;
            #$data['bal'] = $bal;
            $data['content'] = 'admin/index';
            $this->load->view('include/back_template', $data);
        }else{
            $this ->session->set_flashdata('msg','Invalid Login Details, Please try again!');
            redirect('selfservice/login');
        }
        #print_r($text);
    }
    function _load_view() {
        $this->load->view('include/no_template', $this->data);
    }


    // function registerNew(){
    //     $data['title'] = 'Register';
    //     $data['content'] = 'en/registerUser';
    //     $this->load->view('include/no_template', $data);
    // }

     function login(){
        $data['title'] = 'Register';
        $data['content'] = 'en/loginn';
        $this->load->view('include/no_template', $data);
    }

    // function new_login(){
    //     $this->data['title'] = 'Login';
    //     $this->data['content'] = 'en/registernew';
    //     $this->load->view('include/no_template', $this->data);
    // }

    function submit_register(){

        $data['title'] = 'Submit Registration';
       // $data['content'] = 'en/register';
        $data['xml'] =$this->user_model->register_user();
        //$this->load->view('include/back_template', $data);
    }

    function add_user(){
    	#$data['edit'] = $edit;
        $data['users'] = $this->input->post();

        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone No.', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() == true && $this->user_model->add_user()){
        	
        }redirect('selfservice/login/view');
    }

    function is_logged_in() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect('selfservice/login');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('selfservice/login');
    }

    function receiptQuery(){
        $data['title'] = 'Receipt Query';
        $data['content'] = 'en/receiptQuery';
        $this->load->view('include/back_template', $data);
    }

    function receiptQueryDetails(){
        $data['title'] = 'Receipt Query';
        $data['receiptDetails'] = $this->queries_model->receiptQueryDetails();
        $data['content'] = 'en/receiptQueryDetails';
        $this->load->view('include/back_template', $data);
    }

    function printReceiptQueryDetails($transid){
        $this->queries_model->printReceiptQueryDetails($transid);
    }
}