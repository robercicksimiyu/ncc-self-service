<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parking extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #Accesscontrol_helper::is_logged_in();
        $this->load->model('parking_model');
        $this->load->model('sbp_model');
    }

    function daily_parking(){
    	$this->data['title'] = 'Select Location & Vehicle Size';
        $this->data['parking'] = $this->parking_model->prepareDailyParkingInit();
    	$this->data['content'] = 'parking/enter_car_reg_daily';
    	$this->load->view('include/back_template', $this->data);
    }

    function display_details_daily(){
        $this->data['title'] = 'Car Details';
        $this->data['prepare'] = $this->parking_model->prepareDailyParkingPayment();
    	$this->data['content'] = 'parking/display_details_daily';
        $this->load->view('include/back_template', $this->data);
        //var_dump($this->parking_model->prepareParkingPayment());
    }

    function complete_payment_daily(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->parking_model->completeDailyParking();
        $this->data['content'] = 'parking/complete_payment_daily';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->complete());
    }

    function seasonal_parking(){
        $this->data['title'] = 'Select Duration & Vehicle Size';
        $this->data['parking'] = $this->parking_model->prepareSeasonalParkingInit();
        $this->data['content'] = 'parking/enter_car_reg_seasonal';
        $this->load->view('include/back_template', $this->data);
    }

    function confirmSPParking(){
        $data['title'] = 'Parking';
        $data['content'] = 'parking/confirmSPParking';
        $data['bal'] = $this->sbp_model->show_bal();
        $data['parking']=$this->parking_model->confirmSPParking();
        $this->load->view('include/back_template', $data);
    }

    function completeSPPayment(){
        $data['title'] = 'Parking';
        $data['content'] = 'parking/completeSPPayment';
        $data['confirm']=$this->parking_model->completeSPPayment();
        $this->load->view('include/back_template', $data);
        #var_dump($this->parking_model->completeSPPayment());
    }

    function display_details_seasonal(){
        $this->data['title'] = 'Car Details';
        $this->data['prepare'] = $this->parking_model->prepareSeasonalParkingPayment();
        $this->data['content'] = 'parking/display_details_seasonal';
        $this->load->view('include/back_template', $this->data);
        //var_dump($this->parking_model->prepareParkingPayment());
    }

    function complete_payment_seasonal(){

        $this->data['title'] = 'Payment Completed';
        $this->data['complete'] = $this->parking_model->completeSeasonalParking();
        $this->data['content'] = 'parking/complete_payment_seasonal';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->sbp_model->complete());
    }

    function getVehiclesDetails(){
        $this->parking_model->getVehiclesDetails();
    }

    function countVehicles(){
        $this->parking_model->countVehicles();
    }
    function getVehiclesSum(){
        $this->parking_model->getVehiclesSum();
    }

    function addVehicle(){
        $this->parking_model->addVehicle();
    }

    function resetVehicle(){
        $this->parking_model->resetVehicle();
    }

    function printSeasonal(){
        $this->parking_model->printSeasonal();
    }

    function printDreceipt(){
        $this->parking_model->printDreceipt();
    }


}