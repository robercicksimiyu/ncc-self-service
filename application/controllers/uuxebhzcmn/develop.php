<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 5/16/2017
 * Time: 11:17 AM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Develop extends CI_Controller
{
    public $data;

    function __construct()
    {
        parent::__construct();
        $this->load->model('develop_model');#$this->load->library('email');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
    }

    public function _example_output($output = null)
    {
        $this->load->view('develop/example.php',(array)$output);
    }

    function index()
    {
        $this->data['forms']=$this->develop_model->getFormValidation();

        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('datatables');
            $crud->set_table('form_validation');
            $crud->set_subject('Forms');
            $crud->required_fields('form_key');
            $crud->columns('form_key','field','title','rules');

            $output = $crud->render();

            $this->_example_output($output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }

    }
}