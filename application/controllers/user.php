<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
         $this->load->model('user_model');
        $this->load->model('sbp_model');
    }

   function profile(){
       /* $data['title'] = 'View Profile';
        $data['content'] = 'user/profile';*/
        $data['title'] = 'Register SBP';
        $data['content'] = 'user/profile';
        $data['xml'] =$this->sbp_model->prepsaveinitial();
        #$data['res'] =$this->sbp_model->register_sbp();
        #$data['bal'] = $this->sbp_model->show_bal();
        $this->load->view('include/back_template', $data);
    }

    function completeEdit(){
        $data['title'] = 'View Profile';
        $data['content'] = 'user/completeEdit';
        /*$data['bal']=$this->agent_portal_model->floatbal();
        $data['sbp_s']=$this->db_model->sbp_summary();
        $data['users']=$this->agents_model->listbranches();
        $data['summary'] = $this->db_model->userreports();
        $data['msg']= $this->agents_model->completeEdit();*/
        $this->load->view('include/back_template', $data);
    }
    function changePassword(){
        $data['data'] =$this->user_model->changePin();
    }

    function changePin(){
        $data['data'] =$this->user_model->changePassword();
    }
   


}

?>