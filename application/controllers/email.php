<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('email');
	}

	function sendEmail(){
		$this->load->library('email');
		$this->email->from('samuel@deveint.com', 'Samuel Kahara');
		$this->email->to('samuel@deveint.com'); 
		#$this->email->cc('another@another-example.com'); 
		#$this->email->bcc('them@their-example.com'); 
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');	
		
		if(!$this->email->send()){
			echo $this->email->print_debugger();
		}else{
			echo $this->email->print_debugger();
		}
		#echo $this->email->print_debugger();
	}

}