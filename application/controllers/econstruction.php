<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Econstruction extends CI_Controller {
    public $data;

function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('econstruction_model');
        $this->load->model('user_model');
    }

    function index(){
        $this->data['title'] = 'e-Construction';
        $this->data['content'] = 'econstruction/enterInvoice';
        $this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
    }

    function displayConstructionDetails(){
        $this->data['title'] = 'Display Construction Details';
        $this->data['construction'] = $this->econstruction_model->prepareConstructionNew();
        $this->data['content'] = 'econstruction/displayInvoiceDetails';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->econstruction_model->prepareConstruction());
    }

    function completeConstructionPayment(){
        $this->data['title'] = 'Complete Construction Payment';
        $this->data['construction'] = $this->econstruction_model->completeConstructionPayment();
        $this->data['bal'] = $this->user_model->viewBalance1();
        $this->data['content'] = 'econstruction/completeConstructionPayment';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->econstruction_model->completeConstructionPayment());
    }

    function printECReceipt(){
        $this->econstruction_model->PrintEConstruction();
    }

    function reprintECReceipt(){
        $data['title'] = 'E-Construction';
        $data['content'] = 'econstruction/reprintECReceipt';
        $this->load->view('include/back_template', $data);
    }

    function checkECReceipt(){
        $rno=$this->econstruction_model->checkECprintReceipt();
        $this->econstruction_model->printEreceiptsearchNew($rno);
    }

    function econstructionreceiptprint($receiptno){
        $this->econstruction_model->PrintEConstruction($receiptno);
    }



}
