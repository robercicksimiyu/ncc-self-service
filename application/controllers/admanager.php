<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admanager extends CI_Controller {
	 public $data=[];

    function __construct() {
        parent::__construct();
        #$this->is_logged_in();        
       $this->load->model('admanager_model');
    }

    function index(){
    	$this->data['title'] = 'AD Manager';
    	$this->data['content'] = 'admanager/enterBillNo';
        $this->load->view('include/back_template', $this->data);
    }

    function displayADDetails(){
        $this->data['title'] = 'Display AD Details';
        $this->data['ad'] = $this->admanager_model->prepareAD();
        $this->data['content'] = 'admanager/displayADDetails';
        $this->load->view('include/back_template', $this->data);
        //var_dump($this->admanager_model->prepareAD());
    }

    function completeADPayment(){
        $this->data['title'] = 'lCompete Construction Payment';
        $this->data['ad'] = $this->admanager_model->completeADPayment();
        $this->data['content'] = 'admanager/completeADPayment';
        $this->load->view('include/back_template', $this->data);
        #var_dump($this->econstruction_model->completeConstructionPayment());
    }


}