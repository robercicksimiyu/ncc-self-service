<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class walletCreations extends CI_Controller {
	public $data;

	function __construct() {
        parent::__construct();
        #$this->is_logged_in();
        $this->load->model('user_model');

    }

    function index(){
    	$this->data['title'] = 'Registrations';
        $this->data['regs'] = $this->user_model->registrations();
    	$this->data['content'] = 'admin/registrations';
        $this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
    }
}