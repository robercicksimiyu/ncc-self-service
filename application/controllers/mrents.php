<?php
/**
* 
*/
class Mrents extends CI_Controller {

    function __construct() {
        parent::__construct();
        #$this->load->model('db_model');
        //$this->load->model('land_rates_model');
        #$this->load->model('agent_portal_model');
        //$this->load->model('parking_model');
        #$this->load->model('users_model');
        //$this->load->model('miscellaneous_model');
        $this->load->model('marketstalls_model');
        $this->load->model('cess_model');
    }
    function reprintrent(){
    	$data['title'] = 'House Rent';
    	$data['estates']=$this->rent_model->getEstates();
        $data['cess'] = $this->cess_model->dcinitial();
        #$data['bal']=$this->agent_portal_model->floatbal();
        #$data['lr_t']=$this->db_model->lr_total();
        #$data['lnd']=$this->db_model->landrate_summary();
        #$data['sbp_t']=$this->db_model->sbp_total();
        #$data['sbp_s']=$this->db_model->sbp_summary();
        $data['content'] = 'rent/printReceipts';
        #$data['summary'] = $this->db_model->userreports();
        $this->load->view('include/back_template', $data);
    }

    function reprintrentmanual(){
        $this->houserents_model->printrentreceiptmanual();
     }

    function printreceiptcheck(){
        $rno=$this->marketstall_model->housecheckNew();
        $this->marketstalls_model->printrentreceiptsearchNew($rno['hNo'],$rno['eID']);

    }

    function printdirect($receiptno){
        $this->marketstalls_model->printrentreceiptNew($receiptno);
    }
    
    function ManualRentReceiptInsert(){

        $data['title'] = 'Land Search Details';
        $data['content'] = 'rent/reprintManualRentReceipt';
        $data['estates']=$this->marketstalls_model->getEstates();
        #$data['confirm']=$this->land_rates_model->CompleteSearchPayment();
        //$data['bal']=$this->wallet_model->floatbal();
        $this->load->view('include/back_template', $data);
    }

    function insertManualReceipt(){
        $this->marketstalls_model->insertReceiptDetails();
    }
}
?>