<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Health extends CI_Controller {
	public $data;

	function __construct() {
		parent::__construct();
		$this->load->model('health_model');
		$this->load->model('user_model');
	}

	function foodHygieneApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enter_biz_id';
		$this->load->view('include/back_template', $this->data);
	}

	function getBusinessDetails(){
		var_dump($this->data['health'] = $this->health_model->getBusinessDetails());
	}


	function displayHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getHygieneBusinessDetails();
		$this->data['content'] = 'health/foodHygieneApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeHealthPayment();
		$this->data['content'] = 'health/completeHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function InstitutionHealthApplicationForm(){
		$this->data['title'] = 'Food Hygiene Application Form';
		$this->data['content'] = 'health/enterBID';
		$this->load->view('include/back_template', $this->data);
       # var_dump($this->land_rates_model->preparePayment());
	}

	function displayInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->getInstitutionBusinessDetails();
		$this->data['content'] = 'health/foodHygieneInstitutionApplicationDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->inputFormDetails());
	}

	function completeInstitutionHealthPermitDetails(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['health'] = $this->health_model->completeInstitutionHealthPayment();
		$this->data['content'] = 'health/completeInstitutionHealthPermitDetails';
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->completeHealthPayment());
	}

	function printHealthPermit($refid){
		$this->health_model->printHealthPermit($refid);
	}

	function printHealthPermitreceipt($refid){
		$this->health_model->printHealthPermitreceipt($refid);
	}

	function printInstitutionHealthPermit($refid){
		$this->health_model->printInstitutionHealthPermit($refid);
	}

	function printInstitutionHealthPermitreceipt($refid){
		$this->health_model->printInstitutionHealthPermitreceipt($refid);
	}

	function printInstitution(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printInstitution';
		$this->load->view('include/back_template', $this->data);
	}

	function printInstitutionD(){
		$this->data['title'] = 'Print';
		$this->data['content'] = 'health/printDetails';
		$this->data['health'] = $this->health_model->printInstitution();
		$this->load->view('include/back_template', $this->data);
		#var_dump($this->health_model->printInstitution());
	}

	function error(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/error';
		$this->load->view('include/back_template', $this->data);
	}

	function invalidBID(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidBID';
		$this->load->view('include/back_template', $this->data);
	}

	function APIerror(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/APIerror';
		$this->load->view('include/back_template', $this->data);
	}

	function invalidRefid(){
		$this->data['title'] = 'Food Hygiene Application Details';
		$this->data['content'] = 'health/invalidRefid';
		$this->load->view('include/back_template', $this->data);
	}

}