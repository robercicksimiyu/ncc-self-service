-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 18, 2017 at 06:17 PM
-- Server version: 5.5.45
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epaymentslive`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_stands`
--

CREATE TABLE `banner_stands` (
  `id` int(11) NOT NULL,
  `street` varchar(128) NOT NULL,
  `location` varchar(128) NOT NULL,
  `num` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_stands`
--

INSERT INTO `banner_stands` (`id`, `street`, `location`, `num`) VALUES
(1, 'Limuru Road', 'Stima Plaza', '3'),
(2, 'Limuru Road', '3rdParklands', '3'),
(3, 'Limuru Road', 'City Park', '3'),
(4, 'Ngong Road', 'Uchumi Hyper', '3'),
(5, 'Ngong Road', '3No. Nakumat Prestige', '9'),
(6, 'Ngong Road', 'Hurliqiun Grounds', '3'),
(7, 'Ngong Road', 'K.S.T.C - University', '3'),
(8, 'Ngong Road', 'Nakumat Junction', '3'),
(9, 'Ngong Road', 'Karen Shopping Centre', '3'),
(10, 'Langata Road', 'Nyayo Stadium', '3'),
(11, 'Langata Road', 'Barnados', '3'),
(12, 'Langata Road', 'Carnivore', '3'),
(13, 'Langata Road', 'Catholic University', '3'),
(14, 'James Gichuru', 'Lavington Shopping Centre', '3'),
(15, 'James Gichuru', 'Kingara/James Gichuru', '3'),
(16, 'Lusaka Road', 'Toyota Kenya', '3'),
(17, 'Lusaka Road', 'D.T. Dobie', '3'),
(18, 'Jogoo Road', 'City Stadium', '3'),
(19, 'Jogoo Road', 'Uchumi', '3'),
(20, 'Argwings Kodhek', 'D.O.D', '3'),
(21, 'Argwings Kodhek', 'Yaya Centre', '3'),
(22, 'Gitanga Road', 'Valley Arcade', '3'),
(23, 'Chiromo Road', 'Delta', '3'),
(24, 'Chiromo Road', 'Consolata Shrine', '3'),
(25, 'Chiromo Road', '2No.The Mall', '6'),
(26, 'Waiyaki Way', 'Matatu Park', '3'),
(27, 'Waiyaki Way', 'St. Marks', '3'),
(28, 'Landhies Road', 'Kenya Meat Commission', '3'),
(29, 'Landhies Road', 'N.C.C Depot', '3'),
(30, 'Parklands', 'Sarit Centre', '3'),
(31, 'Parklands', 'Parklands Police Station', '3'),
(32, 'Lower Kabete', 'Sarit Centre', '3'),
(33, 'Peponi Road', 'Nakumat Okay', '3'),
(34, 'Ring Road Westlands', 'Vishwal Oshwal', '3'),
(35, 'Ring Road Westlands', 'Uchumi Westlands', '3'),
(36, 'Valley Road', 'Silver Spring', '3'),
(37, 'Milimani Road', 'National Security', '3'),
(38, 'Kiambu Road', 'C.I.D Headquarters', '3');

-- --------------------------------------------------------

--
-- Table structure for table `bid_gabage_info`
--

CREATE TABLE `bid_gabage_info` (
  `id` int(11) NOT NULL,
  `contact_person` varchar(128) DEFAULT NULL,
  `phone_number` varchar(128) DEFAULT NULL,
  `id_number` varchar(128) DEFAULT NULL,
  `bid` varchar(128) DEFAULT NULL,
  `bid_name` varchar(128) DEFAULT NULL,
  `bid_location` varchar(128) DEFAULT NULL,
  `bid_lr_no` varchar(128) DEFAULT NULL,
  `zone` varchar(128) DEFAULT NULL,
  `ward` varchar(128) DEFAULT NULL,
  `building` varchar(256) DEFAULT NULL,
  `bid_activity` text,
  `bid_activity_id` varchar(128) NOT NULL,
  `payment_type_id` bigint(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `building_structure`
--

CREATE TABLE `building_structure` (
  `id` int(11) NOT NULL,
  `phone_number` varchar(243) NOT NULL,
  `details` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business_activities`
--

CREATE TABLE `business_activities` (
  `ID` int(11) NOT NULL,
  `Name` varchar(625) NOT NULL,
  `Amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business_sizes`
--

CREATE TABLE `business_sizes` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business_transaction`
--

CREATE TABLE `business_transaction` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `receiptno` varchar(255) DEFAULT NULL,
  `paidamount` double NOT NULL,
  `businessname` varchar(128) DEFAULT NULL,
  `businessid` varchar(128) DEFAULT NULL,
  `year` varchar(8) DEFAULT NULL,
  `for` varchar(255) DEFAULT NULL,
  `restext` varchar(255) DEFAULT NULL,
  `rescode` varchar(10) DEFAULT NULL,
  `details` text,
  `transaction_date` date DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business_trans_stream`
--

CREATE TABLE `business_trans_stream` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(128) NOT NULL,
  `streams` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dailyparking`
--

CREATE TABLE `dailyparking` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `regno` varchar(20) NOT NULL,
  `category` varchar(30) NOT NULL,
  `zone` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dailyparkingtest`
--

CREATE TABLE `dailyparkingtest` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `regno` varchar(20) NOT NULL,
  `category` varchar(30) NOT NULL,
  `zone` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `econstruction`
--

CREATE TABLE `econstruction` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `invoiceno` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `epaymentsseasonalparkingtemp`
--

CREATE TABLE `epaymentsseasonalparkingtemp` (
  `id` int(11) NOT NULL,
  `durationDesc` varchar(20) NOT NULL,
  `durationCode` int(5) NOT NULL,
  `registrationNo` varchar(15) NOT NULL,
  `vehicleDesc` varchar(30) NOT NULL,
  `vehicleCode` int(5) NOT NULL,
  `transid` int(20) NOT NULL,
  `amount` float NOT NULL,
  `time` datetime NOT NULL,
  `user` varchar(30) NOT NULL,
  `status` int(1) NOT NULL,
  `complete` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fhygiene`
--

CREATE TABLE `fhygiene` (
  `id` int(11) NOT NULL,
  `RefID` varchar(200) DEFAULT NULL,
  `issueDate` datetime NOT NULL,
  `Fullname` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `TelNumber` varchar(200) NOT NULL,
  `Firm` varchar(200) NOT NULL,
  `InvoiceNo` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `certificate` varchar(200) DEFAULT NULL,
  `total` varchar(200) NOT NULL,
  `plotno` varchar(200) DEFAULT NULL,
  `nature` varchar(200) NOT NULL,
  `Amount` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fireinspection`
--

CREATE TABLE `fireinspection` (
  `id` int(11) NOT NULL,
  `RefID` varchar(200) DEFAULT NULL,
  `issueDate` datetime NOT NULL,
  `Fullname` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `TelNumber` varchar(200) NOT NULL,
  `Firm` varchar(200) NOT NULL,
  `InvoiceNo` varchar(200) NOT NULL,
  `Status` varchar(200) NOT NULL,
  `Owner` varchar(200) DEFAULT NULL,
  `Building` varchar(200) DEFAULT NULL,
  `Category` varchar(200) NOT NULL,
  `Amount` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `food_hygiene`
--

CREATE TABLE `food_hygiene` (
  `FH_Id` int(6) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `licencedParty` varchar(255) NOT NULL,
  `Occupation` varchar(50) NOT NULL,
  `PremiseOwner` varchar(100) NOT NULL,
  `PlotNo` varchar(50) NOT NULL,
  `LRNo` varchar(50) NOT NULL,
  `TelNum` varchar(50) NOT NULL,
  `FrontingOn` varchar(100) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `BuildingName` varchar(50) NOT NULL,
  `FloorNumber` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_validation`
--

CREATE TABLE `form_validation` (
  `pk_form_validation_id` int(10) UNSIGNED NOT NULL,
  `form_key` varchar(20) DEFAULT NULL,
  `field` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `rules` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_validation`
--

INSERT INTO `form_validation` (`pk_form_validation_id`, `form_key`, `field`, `title`, `rules`) VALUES
(1, 'banners', 'Fullname', 'Contact Person', 'required\r\n'),
(2, 'banners', 'Tel_no', 'Contact Phone No.', 'required|is_not[0]\r\n'),
(3, 'banners', 'Email', 'Contact Email', 'required\r\n'),
(5, 'banners', 'Description', 'Description', 'required\r\n'),
(6, 'banners', 'Location', 'Location', 'required\r\n'),
(7, 'banners', 'Week_no', 'Week No.', 'required\r\n'),
(8, 'banners', 'Artwork', 'Art Work', 'required|file_allowed_type[pdf]\r\n'),
(9, 'banners', 'Banner_no', 'Number of Banners', 'required|is_natural_no_zero\r\n'),
(10, 'banners', 'Start_date', 'Start Date', 'required|callback_check_start_date'),
(11, 'banners', 'Bid', 'Business ID', 'callback_bid_check\r\n'),
(12, 'banners', 'HoistedLocation', 'Hoisted Location', 'required'),
(13, 'bus_shelter', 'Fullname', 'Contact Person', 'required'),
(14, 'bus_shelter', 'Tel_no', 'Contact Phone No.', 'required|is_not[0]\r\n'),
(15, 'bus_shelter', 'Email', 'Contact Email', 'required|valid_email'),
(16, 'bus_shelter', 'Busshelter_slots', 'Bus Shelter Slots', 'required|is_natural_no_zero'),
(17, 'bus_shelter', 'userfile[]', 'Actual Drawings', 'required|file_allowed_type[pdf]\r\n'),
(18, 'bus_shelter', 'Description', 'Description', 'required'),
(19, 'bus_shelter', 'zones', 'Sub County', 'required'),
(20, 'bus_shelter', 'ward', 'Ward', 'required\r\n'),
(21, 'bus_shelter', 'Mapped_location', 'Mapped Location', 'required'),
(22, 'bus_shelter', 'Location', 'Location', 'required');

-- --------------------------------------------------------

--
-- Table structure for table `gabage_categories`
--

CREATE TABLE `gabage_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `amount` double NOT NULL,
  `parent` int(11) NOT NULL,
  `has_sub_category` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gabage_invoices`
--

CREATE TABLE `gabage_invoices` (
  `id` int(11) NOT NULL,
  `invoice_number` varchar(128) NOT NULL,
  `ref_number` varchar(128) NOT NULL,
  `bid` varchar(128) NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT '0',
  `time_billed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gabage_payment`
--

CREATE TABLE `gabage_payment` (
  `id` int(11) NOT NULL,
  `amount_paid` double NOT NULL,
  `invoice_number` varchar(128) NOT NULL,
  `paid_by` varchar(128) NOT NULL,
  `receipt_number` varchar(128) NOT NULL,
  `transaction_id` varchar(128) NOT NULL,
  `business_name` varchar(128) NOT NULL,
  `phone_number` varchar(128) NOT NULL,
  `checksum` varchar(256) NOT NULL,
  `transaction_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gabage_payment_type`
--

CREATE TABLE `gabage_payment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `healthinstitution`
--

CREATE TABLE `healthinstitution` (
  `id` int(11) NOT NULL,
  `RefID` varchar(200) DEFAULT NULL,
  `issueDate` datetime NOT NULL,
  `Fullname` varchar(200) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `PlotNo` varchar(200) NOT NULL,
  `Mobile` varchar(200) NOT NULL,
  `Purpose` varchar(200) NOT NULL,
  `Nature` varchar(200) NOT NULL,
  `Address` varchar(200) NOT NULL,
  `Status` varchar(200) NOT NULL,
  `Amount` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `healthinvoice`
--

CREATE TABLE `healthinvoice` (
  `id` int(11) NOT NULL,
  `lid` varchar(200) NOT NULL,
  `issuedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fullnames` varchar(200) NOT NULL,
  `businessname` varchar(200) NOT NULL,
  `physicallocation` varchar(200) NOT NULL,
  `plotno` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hire_parks_category`
--

CREATE TABLE `hire_parks_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hrents`
--

CREATE TABLE `hrents` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `estateid` varchar(30) NOT NULL,
  `houseno` varchar(30) NOT NULL,
  `houseowner` varchar(50) NOT NULL,
  `arrears` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hrentstest`
--

CREATE TABLE `hrentstest` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `estateid` varchar(30) NOT NULL,
  `houseno` varchar(30) NOT NULL,
  `houseowner` varchar(50) NOT NULL,
  `arrears` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` int(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `id_types`
--

CREATE TABLE `id_types` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `institutionhealth`
--

CREATE TABLE `institutionhealth` (
  `id` int(11) NOT NULL,
  `RefID` varchar(200) DEFAULT NULL,
  `issueDate` datetime NOT NULL,
  `Fullname` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `TelNumber` varchar(200) NOT NULL,
  `Firm` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `certificate` varchar(200) NOT NULL,
  `Occupation` varchar(200) NOT NULL,
  `Owner` varchar(200) NOT NULL,
  `PlotNo` varchar(200) NOT NULL,
  `LRNO` varchar(200) NOT NULL,
  `Frontingon` varchar(200) NOT NULL,
  `Building` varchar(200) NOT NULL,
  `FloorNo` varchar(200) NOT NULL,
  `Amount` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `landrates`
--

CREATE TABLE `landrates` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `paidby` varchar(30) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `plotno` varchar(30) NOT NULL,
  `plotowner` varchar(30) NOT NULL,
  `penalties` varchar(20) NOT NULL,
  `amountdue` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `landratestest`
--

CREATE TABLE `landratestest` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `paidby` varchar(30) NOT NULL,
  `amount` float DEFAULT NULL,
  `plotno` varchar(30) NOT NULL,
  `plotowner` varchar(30) NOT NULL,
  `penalties` varchar(50) DEFAULT NULL,
  `amountdue` float DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `learning_institution`
--

CREATE TABLE `learning_institution` (
  `LI_Id` int(6) NOT NULL,
  `ApplicantName` varchar(200) NOT NULL,
  `Institution` varchar(255) NOT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `PlotNo` varchar(50) DEFAULT NULL,
  `Road` varchar(50) DEFAULT NULL,
  `Mobile` varchar(50) NOT NULL,
  `InspectionPurpose` text,
  `PremiseNature` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loading_zones_refid`
--

CREATE TABLE `loading_zones_refid` (
  `id` int(11) NOT NULL,
  `form_id` varchar(8) NOT NULL,
  `year` varchar(8) NOT NULL,
  `bid` varchar(128) NOT NULL,
  `ref_id` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loading_zone_businesses`
--

CREATE TABLE `loading_zone_businesses` (
  `id` int(11) NOT NULL,
  `sno` int(11) NOT NULL,
  `business_no` varchar(128) NOT NULL,
  `client` varchar(255) NOT NULL,
  `street_road` varchar(325) NOT NULL,
  `amount` double NOT NULL,
  `expiry_date` varchar(128) NOT NULL,
  `zones` varchar(11) NOT NULL,
  `pending_value` varchar(128) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loading_zone_payment`
--

CREATE TABLE `loading_zone_payment` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(128) NOT NULL,
  `receipt_no` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `miscellaneous`
--

CREATE TABLE `miscellaneous` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `billno` varchar(50) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `description` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstalls`
--

CREATE TABLE `mstalls` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `marketid` varchar(20) NOT NULL,
  `stallno` varchar(20) NOT NULL,
  `stallowner` varchar(30) NOT NULL,
  `arrears` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstallstest`
--

CREATE TABLE `mstallstest` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `marketid` varchar(20) NOT NULL,
  `stallno` varchar(20) NOT NULL,
  `stallowner` varchar(30) NOT NULL,
  `arrears` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newsbp`
--

CREATE TABLE `newsbp` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `businessid` varchar(30) NOT NULL,
  `businessowner` varchar(100) NOT NULL,
  `businessname` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_registrations`
--

CREATE TABLE `new_registrations` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `nationalid` int(30) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE `queries` (
  `id` int(11) NOT NULL,
  `imei` varchar(100) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `time` datetime DEFAULT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registration_fee`
--

CREATE TABLE `registration_fee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regularization_results`
--

CREATE TABLE `regularization_results` (
  `id` int(11) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reg_building_structure`
--

CREATE TABLE `reg_building_structure` (
  `id` int(11) NOT NULL,
  `ownerEmail` varchar(128) DEFAULT NULL,
  `ownerPhoneNo` int(64) DEFAULT NULL,
  `ownerPostalAddress` int(255) DEFAULT NULL,
  `architectRegNo` int(11) DEFAULT NULL,
  `architectNames` int(128) DEFAULT NULL,
  `architectEmail` int(128) DEFAULT NULL,
  `architectPhoneNo` int(64) DEFAULT NULL,
  `architectPostalAddress` int(255) DEFAULT NULL,
  `engineerRegNo` int(64) DEFAULT NULL,
  `engineerNames` int(128) DEFAULT NULL,
  `engineerEmail` int(128) DEFAULT NULL,
  `engineerPhoneNo` int(64) DEFAULT NULL,
  `engineerPostalAddress` int(255) DEFAULT NULL,
  `currentLandUse` int(255) DEFAULT NULL,
  `zone` int(128) DEFAULT NULL,
  `zone_id` int(64) DEFAULT NULL,
  `projectDetailedDescription` text,
  `landTenure` varchar(16) DEFAULT NULL,
  `numberofUnits` varchar(64) DEFAULT NULL,
  `lrNo` varchar(124) DEFAULT NULL,
  `plotSize` varchar(128) DEFAULT NULL,
  `nearestRoad` varchar(128) DEFAULT NULL,
  `estate` varchar(128) DEFAULT NULL,
  `subcounty` varchar(128) DEFAULT NULL,
  `ward` varchar(128) DEFAULT NULL,
  `soilType` varchar(128) DEFAULT NULL,
  `waterSupplier` varchar(128) DEFAULT NULL,
  `sewerageDisposalMethod` varchar(128) DEFAULT NULL,
  `basementArea` varchar(128) DEFAULT NULL,
  `mezzaninefloorArea` varchar(128) DEFAULT NULL,
  `floor1Area` varchar(128) DEFAULT NULL,
  `floor2Area` varchar(128) DEFAULT NULL,
  `floor3Area` varchar(128) DEFAULT NULL,
  `floor4Area` varchar(128) DEFAULT NULL,
  `Others` varchar(128) DEFAULT NULL,
  `totalArea` varchar(128) DEFAULT NULL,
  `buildingCategory` varchar(128) DEFAULT NULL,
  `projectCost` double DEFAULT NULL,
  `inspectionFees` double DEFAULT NULL,
  `Foundation` varchar(128) DEFAULT NULL,
  `externalWalls` varchar(128) DEFAULT NULL,
  `mortar` varchar(128) DEFAULT NULL,
  `roofCover` varchar(128) DEFAULT NULL,
  `dampProofCourse` varchar(128) DEFAULT NULL,
  `jpwnumber` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `retailliquor`
--

CREATE TABLE `retailliquor` (
  `id` int(11) NOT NULL,
  `RefID` varchar(200) DEFAULT NULL,
  `issueDate` datetime NOT NULL,
  `Fullname` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `TelNumber` varchar(200) NOT NULL,
  `Firm` varchar(200) NOT NULL,
  `InvoiceNo` varchar(200) NOT NULL,
  `Status` varchar(200) NOT NULL,
  `Owner` varchar(200) DEFAULT NULL,
  `Building` varchar(200) DEFAULT NULL,
  `Category` varchar(200) NOT NULL,
  `Amount` varchar(200) NOT NULL,
  `certAmount` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rlb_logs`
--

CREATE TABLE `rlb_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `log` text NOT NULL,
  `time_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sbp`
--

CREATE TABLE `sbp` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `paidby` varchar(55) NOT NULL,
  `amount` float NOT NULL,
  `businessid` varchar(30) NOT NULL,
  `penalties` float NOT NULL,
  `annualamount` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sbptest`
--

CREATE TABLE `sbptest` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `businessid` varchar(30) NOT NULL,
  `penalties` float NOT NULL,
  `annualamount` float NOT NULL,
  `amountdue` float NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `channel` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sbp_fire_details`
--

CREATE TABLE `sbp_fire_details` (
  `id` int(11) NOT NULL,
  `phone_number` varchar(64) NOT NULL,
  `business_id` varchar(128) NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  `date_requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sbp_health_details`
--

CREATE TABLE `sbp_health_details` (
  `id` int(11) NOT NULL,
  `phone_number` varchar(64) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `business_id` varchar(255) NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  `date_requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sbp_logs`
--

CREATE TABLE `sbp_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `log` text NOT NULL,
  `time_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seasonalparking`
--

CREATE TABLE `seasonalparking` (
  `id` int(11) NOT NULL,
  `receiptno` varchar(100) NOT NULL,
  `issuedate` datetime NOT NULL,
  `from` varchar(30) NOT NULL,
  `regno` varchar(20) NOT NULL,
  `category` varchar(30) NOT NULL,
  `duration` varchar(30) NOT NULL,
  `amount` float NOT NULL,
  `expirydate` datetime NOT NULL,
  `username` varchar(20) NOT NULL,
  `cashiername` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `t_unique` varchar(30) NOT NULL,
  `channel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_county`
--

CREATE TABLE `sub_county` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transport_forms`
--

CREATE TABLE `transport_forms` (
  `id` int(11) NOT NULL,
  `form_id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `category` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wards`
--

CREATE TABLE `wards` (
  `ID` int(11) NOT NULL,
  `Name` varchar(325) NOT NULL,
  `ZoneID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `ID` int(11) NOT NULL,
  `Name` varchar(325) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_stands`
--
ALTER TABLE `banner_stands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bid_gabage_info`
--
ALTER TABLE `bid_gabage_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `building_structure`
--
ALTER TABLE `building_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_transaction`
--
ALTER TABLE `business_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_trans_stream`
--
ALTER TABLE `business_trans_stream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dailyparking`
--
ALTER TABLE `dailyparking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dailyparkingtest`
--
ALTER TABLE `dailyparkingtest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `econstruction`
--
ALTER TABLE `econstruction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `epaymentsseasonalparkingtemp`
--
ALTER TABLE `epaymentsseasonalparkingtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fhygiene`
--
ALTER TABLE `fhygiene`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fireinspection`
--
ALTER TABLE `fireinspection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_hygiene`
--
ALTER TABLE `food_hygiene`
  ADD PRIMARY KEY (`FH_Id`);

--
-- Indexes for table `form_validation`
--
ALTER TABLE `form_validation`
  ADD PRIMARY KEY (`pk_form_validation_id`);

--
-- Indexes for table `gabage_categories`
--
ALTER TABLE `gabage_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gabage_invoices`
--
ALTER TABLE `gabage_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `payment_type_id` (`payment_type_id`),
  ADD KEY `ref_number` (`ref_number`);

--
-- Indexes for table `gabage_payment`
--
ALTER TABLE `gabage_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gabage_payment_type`
--
ALTER TABLE `gabage_payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthinstitution`
--
ALTER TABLE `healthinstitution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthinvoice`
--
ALTER TABLE `healthinvoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hire_parks_category`
--
ALTER TABLE `hire_parks_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_id` (`category_id`);

--
-- Indexes for table `hrents`
--
ALTER TABLE `hrents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hrentstest`
--
ALTER TABLE `hrentstest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutionhealth`
--
ALTER TABLE `institutionhealth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landrates`
--
ALTER TABLE `landrates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landratestest`
--
ALTER TABLE `landratestest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `learning_institution`
--
ALTER TABLE `learning_institution`
  ADD PRIMARY KEY (`LI_Id`);

--
-- Indexes for table `loading_zones_refid`
--
ALTER TABLE `loading_zones_refid`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_id` (`ref_id`);

--
-- Indexes for table `loading_zone_businesses`
--
ALTER TABLE `loading_zone_businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loading_zone_payment`
--
ALTER TABLE `loading_zone_payment`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `miscellaneous`
--
ALTER TABLE `miscellaneous`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `billno` (`billno`);

--
-- Indexes for table `mstalls`
--
ALTER TABLE `mstalls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstallstest`
--
ALTER TABLE `mstallstest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsbp`
--
ALTER TABLE `newsbp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_registrations`
--
ALTER TABLE `new_registrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queries`
--
ALTER TABLE `queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_fee`
--
ALTER TABLE `registration_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regularization_results`
--
ALTER TABLE `regularization_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reg_building_structure`
--
ALTER TABLE `reg_building_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retailliquor`
--
ALTER TABLE `retailliquor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rlb_logs`
--
ALTER TABLE `rlb_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sbp`
--
ALTER TABLE `sbp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sbptest`
--
ALTER TABLE `sbptest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sbp_fire_details`
--
ALTER TABLE `sbp_fire_details`
  ADD UNIQUE KEY `sbp_fire_id` (`id`);

--
-- Indexes for table `sbp_health_details`
--
ALTER TABLE `sbp_health_details`
  ADD KEY `sbp_health_id` (`id`);

--
-- Indexes for table `sbp_logs`
--
ALTER TABLE `sbp_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seasonalparking`
--
ALTER TABLE `seasonalparking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_county`
--
ALTER TABLE `sub_county`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport_forms`
--
ALTER TABLE `transport_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_stands`
--
ALTER TABLE `banner_stands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `bid_gabage_info`
--
ALTER TABLE `bid_gabage_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `building_structure`
--
ALTER TABLE `building_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `business_transaction`
--
ALTER TABLE `business_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `business_trans_stream`
--
ALTER TABLE `business_trans_stream`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dailyparking`
--
ALTER TABLE `dailyparking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dailyparkingtest`
--
ALTER TABLE `dailyparkingtest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `econstruction`
--
ALTER TABLE `econstruction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `epaymentsseasonalparkingtemp`
--
ALTER TABLE `epaymentsseasonalparkingtemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fhygiene`
--
ALTER TABLE `fhygiene`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fireinspection`
--
ALTER TABLE `fireinspection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `food_hygiene`
--
ALTER TABLE `food_hygiene`
  MODIFY `FH_Id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_validation`
--
ALTER TABLE `form_validation`
  MODIFY `pk_form_validation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `gabage_categories`
--
ALTER TABLE `gabage_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gabage_invoices`
--
ALTER TABLE `gabage_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gabage_payment`
--
ALTER TABLE `gabage_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gabage_payment_type`
--
ALTER TABLE `gabage_payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `healthinstitution`
--
ALTER TABLE `healthinstitution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `healthinvoice`
--
ALTER TABLE `healthinvoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hire_parks_category`
--
ALTER TABLE `hire_parks_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hrents`
--
ALTER TABLE `hrents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hrentstest`
--
ALTER TABLE `hrentstest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institutionhealth`
--
ALTER TABLE `institutionhealth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `landrates`
--
ALTER TABLE `landrates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `landratestest`
--
ALTER TABLE `landratestest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `learning_institution`
--
ALTER TABLE `learning_institution`
  MODIFY `LI_Id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loading_zones_refid`
--
ALTER TABLE `loading_zones_refid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loading_zone_businesses`
--
ALTER TABLE `loading_zone_businesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loading_zone_payment`
--
ALTER TABLE `loading_zone_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `miscellaneous`
--
ALTER TABLE `miscellaneous`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstalls`
--
ALTER TABLE `mstalls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstallstest`
--
ALTER TABLE `mstallstest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsbp`
--
ALTER TABLE `newsbp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `new_registrations`
--
ALTER TABLE `new_registrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `queries`
--
ALTER TABLE `queries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registration_fee`
--
ALTER TABLE `registration_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `regularization_results`
--
ALTER TABLE `regularization_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retailliquor`
--
ALTER TABLE `retailliquor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rlb_logs`
--
ALTER TABLE `rlb_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sbp`
--
ALTER TABLE `sbp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sbp_fire_details`
--
ALTER TABLE `sbp_fire_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sbp_health_details`
--
ALTER TABLE `sbp_health_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sbp_logs`
--
ALTER TABLE `sbp_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seasonalparking`
--
ALTER TABLE `seasonalparking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_county`
--
ALTER TABLE `sub_county`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transport_forms`
--
ALTER TABLE `transport_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
